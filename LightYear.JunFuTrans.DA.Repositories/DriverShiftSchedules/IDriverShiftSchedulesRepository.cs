﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.DriverShiftSchedules
{
    public interface IDriverShiftSchedulesRepository
    {
        DriverShiftArrangement GetAllById(int id);

        List<DriverShiftArrangement> GetAllByStationAndDate(string scode, DateTime date);

        List<DriverShiftArrangement> GetAllByStationScode(string scode);

        void Delete(int id);

        void Insert(DriverShiftArrangement driverShiftArrangement);

        void Update(DriverShiftArrangement driverShiftArrangement);

        int GetDriversCount(string scode);

        int GetTakeOffDriversCount(string scode, DateTime date);
    }
}
