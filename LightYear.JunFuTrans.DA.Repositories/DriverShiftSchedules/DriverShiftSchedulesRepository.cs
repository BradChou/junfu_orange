﻿using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.DriverShiftSchedules
{
    public class DriverShiftSchedulesRepository : IDriverShiftSchedulesRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public JunFuDbContext JunFuDbContext { get; private set; }

        public DriverShiftSchedulesRepository(JunFuTransDbContext junFuTransDbContext, JunFuDbContext junFuDbContext)
        {
            JunFuTransDbContext = junFuTransDbContext;
            JunFuDbContext = junFuDbContext;
        }

        public DriverShiftArrangement GetAllById(int id)
        {
            var data = JunFuTransDbContext.DriverShiftArrangements.Find(id);        

            return data;
        }

        public List<DriverShiftArrangement> GetAllByStationScode (string scode)
        {
            var data = JunFuTransDbContext.DriverShiftArrangements
                       .Where(s => s.StationScode == scode)
                       .ToList();

            return data;
        }

        public List<DriverShiftArrangement> GetAllByStationAndDate(string scode, DateTime date)
        {
            var data = JunFuTransDbContext.DriverShiftArrangements
                       .Where(s => s.StationScode == scode)
                       .Where(s => s.TakeOffDate == date)
                       .ToList();
            return data;
        }

        public void Delete(int id)
        {
            var data = JunFuTransDbContext.DriverShiftArrangements.Find(id);

            if (data != null)
            {
                this.JunFuTransDbContext.DriverShiftArrangements.Remove(data);

                this.JunFuTransDbContext.SaveChanges();
            }
        }
        
        public void Insert(DriverShiftArrangement driverShiftArrangement)
        {
            this.JunFuTransDbContext.DriverShiftArrangements.Add(driverShiftArrangement);

            this.JunFuTransDbContext.SaveChanges();
        }

        public void Update(DriverShiftArrangement driverShiftArrangement)
        {

            this.JunFuTransDbContext.DriverShiftArrangements.Attach(driverShiftArrangement);

            var entry = this.JunFuTransDbContext.Entry(driverShiftArrangement);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            this.JunFuTransDbContext.SaveChanges();
        }

        public int GetDriversCount(string scode)
        {
            var data = (from tb in JunFuDbContext.TbDrivers
                        where tb.Station == scode
                        select tb.DriverCode).Count();
            return data;
        }

        public int GetTakeOffDriversCount(string scode, DateTime date) //當天請假的司機數量
        {
            var data = (from tb in JunFuTransDbContext.DriverShiftArrangements
                        where tb.StationScode == scode & tb.TakeOffDate == date
                        select tb.TakeOffDriverCode).Count();
            return data;
        }
    }
}
