﻿using LightYear.JunFuTrans.BL.BE.PalletACSectionShouldPay;
using LightYear.JunFuTrans.DA.JunFuDb;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace LightYear.JunFuTrans.DA.Repositories.PalletACSectionShouldPay
{
    public class PalletACSectionShouldPayRepository : IPalletACSectionShouldPayRepository
    {
        public JunFuDbContext JunFuDbContext { get; set; }

        public PalletACSectionShouldPayRepository(JunFuDbContext junFuDbContext)
        {
            JunFuDbContext = junFuDbContext;
        }

        /// <summary>
        /// 取得營運中的區配商
        /// </summary>
        /// <returns></returns>
        public List<SuppliersEntity> GetSuppliers()
        {
            var notCount = new[] { "Z01" };

            var data = (from d in JunFuDbContext.TbDrivers
                        join s in JunFuDbContext.TbSuppliers on d.CheckoutVendor equals s.SupplierCode
                        orderby d.CheckoutVendor
                        where d.CheckoutVendor.Length > 0 && s.ActiveFlag.Equals(true) && !notCount.Contains(d.CheckoutVendor)
                        select new { SupplierCode = d.CheckoutVendor, SupplierName = s.SupplierName }).Distinct();
            var output = data.Select(a => new SuppliersEntity { SupplierCode = a.SupplierCode, SupplierName = a.SupplierCode + " " + a.SupplierName }).ToList();
            return output;
        }

        /// <summary>
        /// 取得最新C段浮動油價表折扣
        /// </summary>
        /// <returns></returns>
        public OilPriceBalance GetCSectionFloatingOilPriceDiscount()
        {
            short latestStepDistance = JunFuDbContext.OilPriceUpdateLogs.OrderByDescending(a => a.CreateDate).FirstOrDefault().StepDistance;
            return JunFuDbContext.OilPriceBalances.Where(a => Convert.ToInt32(a.StepDistance).Equals(Convert.ToInt32(latestStepDistance))).FirstOrDefault();
        }

        /// <summary>
        /// 取得C段各客戶原始運價
        /// </summary>
        /// <returns></returns>
        public List<CSectionOriginalShippingFee> GetCSectionOriginalShippingFee()
        {
            var SupplierFreights = JunFuDbContext.SupplierFreights.ToList();
            var latest = SupplierFreights.GroupBy(a => a.SupplierNo).Where(a => a.Key.Length > 1).Select(a => new { SupplierNo = a.Key, CreateDate = a.Max(b => b.CreateDate) }).ToList();

            var output = (from la in latest
                          join S in SupplierFreights on new { SupplierNo = la.SupplierNo, CreateDate = la.CreateDate } equals new { SupplierNo = S.SupplierNo, CreateDate = S.CreateDate }
                          join sd in JunFuDbContext.SupplierFreightDetails on S.Id equals sd.SupplierFreightId
                          join su in JunFuDbContext.TbSuppliers on S.SupplierNo equals su.SupplierNo
                          orderby S.Id
                          select new CSectionOriginalShippingFee
                          {
                              SupplierNo = S.SupplierNo,
                              SupplierArea = sd.SupplyArea,
                              ReceiveArea = sd.ReceiveArea,
                              C1 = sd.C1 ?? 0,
                              C2 = sd.C2 ?? 0,
                              C3 = sd.C3 ?? 0,
                              C4 = sd.C4 ?? 0,
                              C5 = sd.C5 ?? 0,
                              C6 = sd.C6 ?? 0
                          }).ToList();
            return output;
        }

        /// <summary>
        /// 各配達區分是否需要付款資料表
        /// </summary>
        /// <returns></returns>
        public List<ArriveOptionIsPaymentRequired> GetArriveOptionIsPaymentRequired()
        {
            var NoPaymentRequireds = new[] { "1", "2" }; //客戶不在、約定再配

            List<ArriveOptionIsPaymentRequired> paymentRequireds = new List<ArriveOptionIsPaymentRequired>();

            Regex regex = new Regex("^[0-9]$");

            var data = JunFuDbContext.TbItemCodes.Where(a => a.CodeSclass.Equals("AO")).ToList();

            foreach (var item in data)
            {
                // if (regex.IsMatch(item.CodeId))
                //{
                ArriveOptionIsPaymentRequired paymentRequired = new ArriveOptionIsPaymentRequired();
                paymentRequired.CodeId = item.CodeId;
                paymentRequired.CodeName = item.CodeName;
                if (NoPaymentRequireds.Contains(paymentRequired.CodeId))
                    paymentRequired.PaymentRequired = false;
                paymentRequireds.Add(paymentRequired);
                // }
            }

            paymentRequireds.Add(data.Where(a => a.CodeId.Equals("10")).Select(a => new ArriveOptionIsPaymentRequired { CodeId = a.CodeId, CodeName = a.CodeName, PaymentRequired = true }).FirstOrDefault());

            return paymentRequireds;
        }

        /// <summary>
        /// 取得非系統補集貨的第一筆司機和所屬結帳區配商(A段)
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public List<ACSectionSupplierAndDriver> GetASectionSupplierAndDriver(DateTime start, DateTime end)
        {
            //   var NotCountingCustomerCode = new[] { "0000049201", "0000049001", "0000049102", "B050000001", "000000101" };

            var firstPickUp = from s in JunFuDbContext.TtDeliveryScanLogs
                              join t in JunFuDbContext.TcDeliveryRequests on s.CheckNumber equals t.CheckNumber
                              where t.PrintDate >= start && t.PrintDate < end && t.ShipDate != null && t.CancelDate == null
                              && t.LessThanTruckload.Equals(false) && t.CheckNumber.Length > 0 && t.PricingType != "05"
                              && s.ScanItem == "5" && s.DriverCode != "Z010474" // 系統補集貨司機
                                                                                //   && !NotCountingCustomerCode.Contains(t.CustomerCode)
                              group s by t.CheckNumber into g
                              select new
                              {
                                  CheckNumber = g.Key,
                                  ScanDate = g.Min(a => a.ScanDate)
                              };
            var pickUpDriver = (from f in firstPickUp
                                join t in JunFuDbContext.TtDeliveryScanLogs on new { CheckNumber = f.CheckNumber, ScanDate = f.ScanDate } equals new { CheckNumber = t.CheckNumber, ScanDate = t.ScanDate }
                                join d in JunFuDbContext.TbDrivers on t.DriverCode equals d.DriverCode
                                join su in JunFuDbContext.TbSuppliers on d.CheckoutVendor equals su.SupplierCode
                                where !string.IsNullOrEmpty(d.CheckoutVendor)
                                select new { CheckNumber = f.CheckNumber, DriverCodeAndName = t.DriverCode + "-" + d.DriverName, SupplierCodeAndName = d.CheckoutVendor + "-" + su.SupplierName }
                               ).Distinct().ToList();

            var outputPickUpDriver = pickUpDriver.Select(a => new ACSectionSupplierAndDriver { CheckNumber = a.CheckNumber, DriverCodeAndName = a.DriverCodeAndName, SupplierCodeAndName = a.SupplierCodeAndName }).ToList();

            return outputPickUpDriver;
        }

        public List<ACSectionSupplierAndDriver> GetASectionSupplierAndDriverByCheckNumber(string checkNumber)
        {
            //   var NotCountingCustomerCode = new[] { "0000049201", "0000049001", "0000049102", "B050000001", "000000101" };

            var firstPickUp = (from s in JunFuDbContext.TtDeliveryScanLogs
                               join t in JunFuDbContext.TcDeliveryRequests on s.CheckNumber equals t.CheckNumber
                               where t.ShipDate != null && t.CancelDate == null && t.LessThanTruckload.Equals(false)
                               && t.CheckNumber.Length > 0 && t.PricingType != "05"
                               && s.ScanItem == "5" && s.DriverCode != "Z010474" // 系統補集貨司機
                                                                                 //  && !NotCountingCustomerCode.Contains(t.CustomerCode) 
                               && s.CheckNumber == checkNumber
                               group s by new { s.CheckNumber, s.DriverCode } into g
                               orderby g.Min(a => a.ScanDate) ascending
                               select new
                               {
                                   CheckNumber = g.Key.CheckNumber,
                                   DriverCode = g.Key.DriverCode,
                               }).Take(1);

            var pickUpDriver = (from f in firstPickUp
                                join d in JunFuDbContext.TbDrivers on f.DriverCode equals d.DriverCode
                                join su in JunFuDbContext.TbSuppliers on d.CheckoutVendor equals su.SupplierCode
                                where !string.IsNullOrEmpty(d.CheckoutVendor) && d.ActiveFlag == true
                                select new ACSectionSupplierAndDriver { CheckNumber = f.CheckNumber, DriverCodeAndName = f.DriverCode + "-" + d.DriverName, SupplierCodeAndName = d.CheckoutVendor + "-" + su.SupplierName }
                               ).ToList();
            return pickUpDriver;
        }

        public List<ACSectionSupplierAndDriver> GetASectionSupplierAndDriverByTaskId(string taskId)
        {
            List<ACSectionSupplierAndDriver> aCs = new List<ACSectionSupplierAndDriver>();

            //  var NotCountingCustomerCode = new[] { "0000049201", "0000049001", "0000049102", "B050000001", "000000101" };

            var searchCheckNumbersByTaskId = (from t in JunFuDbContext.TcDeliveryRequests
                                              join taskDetail in JunFuDbContext.TtDispatchTaskDetails on Convert.ToInt32(t.RequestId) equals Convert.ToInt32(taskDetail.RequestId ?? 0)
                                              join task in JunFuDbContext.TtDispatchTasks on taskDetail.TId ?? 0 equals task.TId
                                              where t.ShipDate != null && t.CancelDate == null && t.LessThanTruckload.Equals(false)
                                                    && t.CheckNumber.Length > 0 && t.PricingType != "05"
                                                    // && !NotCountingCustomerCode.Contains(t.CustomerCode)
                                                    && task.TaskId == taskId
                                              select t.CheckNumber).ToList();

            foreach (var checkNumber in searchCheckNumbersByTaskId)
            {
                aCs.AddRange(GetASectionSupplierAndDriverByCheckNumber(checkNumber));
            }

            return aCs;
        }

        public List<AcRemoteSection> GetACRemoteSections()
        {
            return JunFuDbContext.AcRemoteSections.ToList();
        }

        public List<PalletAcSectionShouldPay> GetPalletAcSectionShouldPay()
        {
            return JunFuDbContext.PalletAcSectionShouldPays.ToList();
        }

        public List<PalletAcSectionShouldPay> GetPalletAcSectionShouldPayByRequestIds(List<decimal> requestIds)
        {
            List<int> ids = new List<int>();

            requestIds.ForEach(a => ids.Add(Convert.ToInt32(a)));

            var output = JunFuDbContext.PalletAcSectionShouldPays.Where(a => ids.Contains(a.RequestId ?? 0)).ToList();

            return output.Count > 0 ? output : null;
        }

        public List<PalletACSectionShouldPayEntity> GetACSectionShouldPayDetails(DateTime start, DateTime end)
        {
            //  var NotCountingCustomerCode = new[] { "0000049201", "0000049001", "0000049102", "B050000001", "000000101" };
            var AOCode = GetArriveOptionIsPaymentRequired().Select(a => a.CodeId).ToList();

            var data = (from t in JunFuDbContext.TcDeliveryRequests
                        join a in JunFuDbContext.TbAccounts on t.Cuser equals a.AccountCode
                        join c1 in JunFuDbContext.TbCustomers on t.CustomerCode equals c1.CustomerCode
                        join s in JunFuDbContext.TbSuppliers on t.AreaArriveCode equals s.SupplierCode
                        //join taskDetail in JunFuDbContext.TtDispatchTaskDetails on Convert.ToInt32(t.RequestId) equals Convert.ToInt32(taskDetail.RequestId ?? 0) into taskDetailTemp
                        //from taskDetail in taskDetailTemp.DefaultIfEmpty()
                        //join task in JunFuDbContext.TtDispatchTasks on taskDetail.TId ?? 0 equals task.TId into taskTemp
                        //from task in taskTemp.DefaultIfEmpty()
                        join d in (from x in JunFuDbContext.TbDrivers where !string.IsNullOrEmpty(x.CheckoutVendor) select x) on t.LatestArriveOptionDriver equals d.DriverCode into temp
                        from d in temp.DefaultIfEmpty()
                        join s1 in (from y in JunFuDbContext.TbSuppliers where !string.IsNullOrEmpty(y.SupplierCode) select y) on d.CheckoutVendor equals s1.SupplierCode into temp1
                        from s1 in temp1.DefaultIfEmpty()
                        where t.PrintDate >= start && t.PrintDate < end && /*t.ShipDate != null && */t.CancelDate == null
                              && t.LessThanTruckload.Equals(false) && t.CheckNumber.Length > 0 && t.PricingType != "05"

                              && !string.IsNullOrEmpty(t.AreaArriveCode)
                              //          && !NotCountingCustomerCode.Contains(t.CustomerCode)
                              && (AOCode.Contains(t.LatestArriveOption) || t.LatestArriveOption == null)
                        select new PalletACSectionShouldPayEntity
                        {
                            RequestId = t.RequestId,
                            TaskId = "",
                            UserName = a.UserName,
                            PrintDate = t.PrintDate,
                            CustomerCode = t.CustomerCode + "" + c1.CustomerName,
                            CheckNumber = t.CheckNumber,
                            SendCity = t.SendCity + t.SendArea,
                            SendContact = t.SendContact,
                            ReceiveContact = t.ReceiveContact,
                            ReceiveCity = t.ReceiveCity + t.ReceiveArea,
                            Plates = t.Plates,
                            Pieces = t.Pieces,
                            AShipDate = t.ShipDate,
                            SpecialNeed = t.InvoiceDesc,
                            ArriveStation = s.SupplierName,
                            SendStation = t.SupplierName,
                            ArriveOption = t.LatestArriveOption,
                            CDriver = t.LatestArriveOptionDriver + "-" + d.DriverName,
                            CSectionSupplier = d.CheckoutVendor + "-" + s1.SupplierName,
                            CSectionSupplierNo = s1.SupplierNo,
                            SendStationCode = t.SendStationScode ?? "", //發送站代碼
                            CSectionPayment = t.ReceiveAddress.Contains("站址") ? 0 : 1, // 0代表0元，1只是區別出要算錢
                            ReceiveAddress = t.ReceiveCity + t.ReceiveArea + t.ReceiveAddress
                        }).ToList();
            return data;
        }

        public List<PalletACSectionShouldPayEntity> GetACSectionShouldPayDetailsByCheckNumber(string checkNumber)
        {
            // var NotCountingCustomerCode = new[] { "0000049201", "0000049001", "0000049102", "B050000001", "000000101" };
            var AOCode = GetArriveOptionIsPaymentRequired().Select(a => a.CodeId).ToList();

            var data = (from t in JunFuDbContext.TcDeliveryRequests
                        join a in JunFuDbContext.TbAccounts on t.Cuser equals a.AccountCode
                        join c1 in JunFuDbContext.TbCustomers on t.CustomerCode equals c1.CustomerCode
                        join s in JunFuDbContext.TbSuppliers on t.AreaArriveCode equals s.SupplierCode
                        //join taskDetail in JunFuDbContext.TtDispatchTaskDetails on Convert.ToInt32(t.RequestId) equals Convert.ToInt32(taskDetail.RequestId ?? 0) into taskDetailTemp
                        //from taskDetail in taskDetailTemp.DefaultIfEmpty()
                        //join task in JunFuDbContext.TtDispatchTasks on taskDetail.TId ?? 0 equals task.TId into taskTemp
                        //from task in taskTemp.DefaultIfEmpty()                        
                        join d in (from x in JunFuDbContext.TbDrivers where !string.IsNullOrEmpty(x.CheckoutVendor) select x) on t.LatestArriveOptionDriver equals d.DriverCode into temp
                        from d in temp.DefaultIfEmpty()
                        join s1 in (from y in JunFuDbContext.TbSuppliers where !string.IsNullOrEmpty(y.SupplierCode) select y) on d.CheckoutVendor equals s1.SupplierCode into temp1
                        from s1 in temp1.DefaultIfEmpty()
                        where /*t.ShipDate != null && */t.CancelDate == null && t.LessThanTruckload.Equals(false)
                              && t.CheckNumber.Length > 0 && t.PricingType != "05"
                              && !string.IsNullOrEmpty(t.AreaArriveCode)
                              //            && !NotCountingCustomerCode.Contains(t.CustomerCode) 
                              && (AOCode.Contains(t.LatestArriveOption) || t.LatestArriveOption == null)
                              && t.CheckNumber == checkNumber
                        select new PalletACSectionShouldPayEntity
                        {
                            RequestId = t.RequestId,
                            TaskId = "",
                            UserName = a.UserName,
                            PrintDate = t.PrintDate,
                            CustomerCode = t.CustomerCode + "" + c1.CustomerName,
                            CheckNumber = t.CheckNumber,
                            SendCity = t.SendCity + t.SendArea,
                            SendContact = t.SendContact,
                            ReceiveContact = t.ReceiveContact,
                            ReceiveCity = t.ReceiveCity + t.ReceiveArea,
                            Plates = t.Plates,
                            Pieces = t.Pieces,
                            AShipDate = t.ShipDate,
                            SpecialNeed = t.InvoiceDesc,
                            ArriveStation = s.SupplierName,
                            SendStation = t.SupplierName,
                            ArriveOption = t.LatestArriveOption,
                            CDriver = t.LatestArriveOptionDriver + "-" + d.DriverName,
                            CSectionSupplier = d.CheckoutVendor + "-" + s1.SupplierName,
                            CSectionSupplierNo = s1.SupplierNo,
                            ASectionPayment = t.CustomerCode.Substring(0, 3).Equals(t.SendStationScode) ? 0 : 1, // 0代表0元，1只是區別出要算錢
                            CSectionPayment = t.ReceiveAddress.Contains("站址") ? 0 : 1, // 0代表0元，1只是區別出要算錢
                            ReceiveAddress = t.ReceiveCity + t.ReceiveArea + t.ReceiveAddress,
                            SendStationCode = t.SendStationScode ?? ""
                        }).ToList();
            return data;
        }

        public List<PalletACSectionShouldPayEntity> GetACSectionShouldPayDetailsByTaskId(string taskId)
        {
            //   var NotCountingCustomerCode = new[] { "0000049201", "0000049001", "0000049102", "B050000001", "000000101" };
            var AOCode = GetArriveOptionIsPaymentRequired().Select(a => a.CodeId).ToList();

            var data = (from t in JunFuDbContext.TcDeliveryRequests
                        join a in JunFuDbContext.TbAccounts on t.Cuser equals a.AccountCode
                        join c1 in JunFuDbContext.TbCustomers on t.CustomerCode equals c1.CustomerCode
                        join s in JunFuDbContext.TbSuppliers on t.AreaArriveCode equals s.SupplierCode
                        join taskDetail in JunFuDbContext.TtDispatchTaskDetails on Convert.ToInt32(t.RequestId) equals Convert.ToInt32(taskDetail.RequestId ?? 0) into taskDetailTemp
                        from taskDetail in taskDetailTemp.DefaultIfEmpty()
                        join task in JunFuDbContext.TtDispatchTasks on taskDetail.TId ?? 0 equals task.TId into taskTemp
                        from task in taskTemp.DefaultIfEmpty()
                        join d in (from x in JunFuDbContext.TbDrivers where !string.IsNullOrEmpty(x.CheckoutVendor) select x) on t.LatestArriveOptionDriver equals d.DriverCode into temp
                        from d in temp.DefaultIfEmpty()
                        join s1 in (from y in JunFuDbContext.TbSuppliers where !string.IsNullOrEmpty(y.SupplierCode) select y) on d.CheckoutVendor equals s1.SupplierCode into temp1
                        from s1 in temp1.DefaultIfEmpty()
                        where t.ShipDate != null && t.CancelDate == null && t.LessThanTruckload.Equals(false)
                              && t.CheckNumber.Length > 0 && t.PricingType != "05"

                              && !string.IsNullOrEmpty(t.AreaArriveCode)
                              //         && !NotCountingCustomerCode.Contains(t.CustomerCode) 
                              && (AOCode.Contains(t.LatestArriveOption) || t.LatestArriveOption == null)
                              && task.TaskId == taskId
                        select new PalletACSectionShouldPayEntity
                        {
                            RequestId = t.RequestId,
                            TaskId = task.TaskId,
                            UserName = a.UserName,
                            PrintDate = t.PrintDate,
                            CustomerCode = t.CustomerCode + "" + c1.CustomerName,
                            CheckNumber = t.CheckNumber,
                            SendCity = t.SendCity + t.SendArea,
                            SendContact = t.SendContact,
                            ReceiveContact = t.ReceiveContact,
                            ReceiveCity = t.ReceiveCity + t.ReceiveArea,
                            Plates = t.Plates,
                            Pieces = t.Pieces,
                            AShipDate = t.ShipDate,
                            SpecialNeed = t.InvoiceDesc,
                            ArriveStation = s.SupplierName,
                            SendStation = t.SupplierName,
                            ArriveOption = t.LatestArriveOption,
                            CDriver = t.LatestArriveOptionDriver + "-" + d.DriverName,
                            CSectionSupplier = d.CheckoutVendor + "-" + s1.SupplierName,
                            CSectionSupplierNo = s1.SupplierNo,
                            ASectionPayment = t.CustomerCode.Substring(0, 3).Equals(t.SendStationScode) ? 0 : 1, // 0代表0元，1只是區別出要算錢
                            CSectionPayment = t.ReceiveAddress.Contains("站址") ? 0 : 1, // 0代表0元，1只是區別出要算錢
                            ReceiveAddress = t.ReceiveCity + t.ReceiveArea + t.ReceiveAddress,
                            SendStationCode = t.SendStationScode ?? ""
                        }).ToList();
            return data;
        }

        public int Insert(PalletAcSectionShouldPay palletAcSectionShouldPay)
        {
            this.JunFuDbContext.PalletAcSectionShouldPays.Add(palletAcSectionShouldPay);

            this.JunFuDbContext.SaveChanges();

            return palletAcSectionShouldPay.Id;
        }

        public void Update(PalletAcSectionShouldPay palletAcSectionShouldPay)
        {
            this.JunFuDbContext.PalletAcSectionShouldPays.Attach(palletAcSectionShouldPay);

            var entry = this.JunFuDbContext.Entry(palletAcSectionShouldPay);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            this.JunFuDbContext.SaveChanges();
        }

        public List<CSectionInfo> GetCSectionInfoByCheckNumber(List<PalletACSectionShouldPayEntity> data)
        {
            var checkNumberList = data.Select(x => x.CheckNumber).Distinct();
            var temp = from a in JunFuDbContext.TcDeliveryRequests.Where(x => checkNumberList.Contains(x.CheckNumber))
                       join b in JunFuDbContext.TtDeliveryScanLogs on a.CheckNumber equals b.CheckNumber
                       join c in JunFuDbContext.TbDrivers on b.DriverCode equals c.DriverCode into bc
                       from c in bc.DefaultIfEmpty()
                       where b.ScanItem == "3"
                       select new CSectionInfo
                       {
                           CheckNumber = b.CheckNumber,
                           Driver = $"{b.DriverCode}-{c.DriverName}",
                           ScanDate = b.ScanDate
                       };

            var result = temp.AsEnumerable().GroupBy(x => x.CheckNumber)
                                     .Select(x => new { x.Key, CInfo = x, Count = x.Count() })
                                     .SelectMany(x => x.CInfo.OrderBy(x => x.ScanDate)
                                                       .Select((item, index) => new CSectionInfo
                                                       {
                                                           CheckNumber = item.CheckNumber,
                                                           Driver = item.Driver,
                                                           ScanDate = item.ScanDate,
                                                           ScanCount = index + 1
                                                       }).Where(y => y.ScanCount == x.Count)
                                     ).Distinct().ToList();

            return result.ToList();
        }
    }
}
