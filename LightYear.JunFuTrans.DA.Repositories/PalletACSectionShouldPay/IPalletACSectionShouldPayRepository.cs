﻿using LightYear.JunFuTrans.BL.BE.PalletACSectionShouldPay;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.PalletACSectionShouldPay
{
    public interface IPalletACSectionShouldPayRepository
    {
        List<SuppliersEntity> GetSuppliers();
        OilPriceBalance GetCSectionFloatingOilPriceDiscount();
        List<CSectionOriginalShippingFee> GetCSectionOriginalShippingFee();
        List<CSectionInfo> GetCSectionInfoByCheckNumber(List<PalletACSectionShouldPayEntity> checkNumber);
        List<ArriveOptionIsPaymentRequired> GetArriveOptionIsPaymentRequired();
        List<ACSectionSupplierAndDriver> GetASectionSupplierAndDriver(DateTime start, DateTime end);
        List<ACSectionSupplierAndDriver> GetASectionSupplierAndDriverByCheckNumber(string checkNumber);
        List<ACSectionSupplierAndDriver> GetASectionSupplierAndDriverByTaskId(string taskId);
        List<PalletACSectionShouldPayEntity> GetACSectionShouldPayDetails(DateTime start, DateTime end);
        List<PalletACSectionShouldPayEntity> GetACSectionShouldPayDetailsByCheckNumber(string checkNumber);
        List<PalletACSectionShouldPayEntity> GetACSectionShouldPayDetailsByTaskId(string taskId);
        List<AcRemoteSection> GetACRemoteSections();
        List<PalletAcSectionShouldPay> GetPalletAcSectionShouldPay();
        List<PalletAcSectionShouldPay> GetPalletAcSectionShouldPayByRequestIds(List<decimal> requestIds);
        void Update(PalletAcSectionShouldPay palletAcSectionShouldPay);
        int Insert(PalletAcSectionShouldPay palletAcSectionShouldPay);
    }
}
