﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.DA.Repositories.JunFuOrangeLogs
{
    public interface IJunFuOrangeLogRepository
    {
        List<JunFuOrangeLog> GetAll();
        List<JunFuOrangeLog> GetPublicAll();
        Task InsertAsync(JunFuOrangeLog junFuOrangeLog);
        void Insert(JunFuOrangeLog junFuOrangeLog);
    }
}
