﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.DA.Repositories.JunFuOrangeLogs
{
    public class JunFuOrangeLogRepository : IJunFuOrangeLogRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public JunFuOrangeLogRepository(JunFuTransDbContext junFuTransDbContext)
        {
            JunFuTransDbContext = junFuTransDbContext;
        }
        public List<JunFuOrangeLog> GetAll()
        {
            return JunFuTransDbContext.JunFuOrangeLogs.Where(p => true).ToList();
        }
        public List<JunFuOrangeLog> GetPublicAll()
        {
            return this.GetAll().ToList();
        }
        public async Task InsertAsync(JunFuOrangeLog junFuOrangeLog)
        {
            if (string.IsNullOrEmpty(junFuOrangeLog.ResponseBody))
            {
                junFuOrangeLog.ResponseBody = "";
            }
            try
            {
                this.JunFuTransDbContext.JunFuOrangeLogs.Add(junFuOrangeLog);
                await this.JunFuTransDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                var a = e.Message;
                throw;
            }

            return;
        }
        public void Insert(JunFuOrangeLog junFuOrangeLog)
        {
            if (string.IsNullOrEmpty(junFuOrangeLog.ResponseBody))
            {
                junFuOrangeLog.ResponseBody = "";
            }
            try
            {
                this.JunFuTransDbContext.JunFuOrangeLogs.Add(junFuOrangeLog);
                this.JunFuTransDbContext.SaveChanges();
            }
            catch (Exception e)
            {
                var a = e.Message;
                throw;
            }
            return;
        }
    }
}
