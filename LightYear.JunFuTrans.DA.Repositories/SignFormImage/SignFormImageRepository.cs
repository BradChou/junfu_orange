﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;
using System.Linq;
using Microsoft.Extensions.Configuration;
using LightYear.JunFuTrans.BL.BE.SignForm;

namespace LightYear.JunFuTrans.DA.Repositories.SignFormImage
{
    public class SignFormImageRepository : ISignFormImageRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }
        public IConfiguration Configuration { get; set; }
        public SignFormImageRepository(JunFuDbContext junFuDbContext, IConfiguration configuration)
        {
            JunFuDbContext = junFuDbContext;
            Configuration = configuration;
        }

        public IEnumerable<SignFormEntity> GetDataByDatePeriodAndCustomerCode(DateTime deliveryDateStart, DateTime deliveryDateEnd, string customerCode, string stationScode)
        {
            var data = from request in JunFuDbContext.TcDeliveryRequests
                       join checkPhoto in JunFuDbContext.CheckPhotoToS3File on request.CheckNumber equals checkPhoto.FileName into t3
                       from checkPhoto in t3.DefaultIfEmpty()
                       join itemCodes in JunFuDbContext.TbItemCodes on request.LatestArriveOption equals itemCodes.CodeId into t4
                       from itemCodes in t4.DefaultIfEmpty()
                       join drivers in JunFuDbContext.TbDrivers on request.LatestScanDriverCode.ToUpper() equals drivers.DriverCode into t5
                       from drivers in t5.DefaultIfEmpty()
                       join station in JunFuDbContext.TbStations on request.AreaArriveCode equals station.StationScode into t6
                       from station in t6.DefaultIfEmpty()
                       where request.CustomerCode == customerCode && request.PrintDate >= deliveryDateStart && request.PrintDate < deliveryDateEnd.AddDays(1) && request.AreaArriveCode == stationScode && itemCodes.CodeSclass == "AO"
                       orderby request.RequestId
                       select new SignFormEntity
                       {
                           RequestId = request.RequestId,
                           CheckNumber = request.CheckNumber,
                           DeliverDriver = request.LatestScanDriverCode.ToUpper() + drivers.DriverName,
                           ArriveOption = itemCodes.CodeName,
                           SignPaperPath = checkPhoto.S3Path == null ? "" : checkPhoto.S3Path.Replace(Configuration.GetValue<string>("S3SignPaperPhotoIP"), Configuration.GetValue<string>("S3Url")),
                           SignPaperString = checkPhoto.S3Path == null ? "下載空白簽單" : "查看簽單",
                           ArriveDate = request.LatestScanDate == null? "" : ((DateTime)request.LatestScanDate).ToString("yyyy-MM-dd HH:mm:ss"),
                           AreaArriveStation = station.StationName,
                           IsSignPaperExist = !(checkPhoto.S3Path == null),
                           IsReturn = !(request.DeliveryType == "D")
                       };

            return data.Distinct();
        }

        public IEnumerable<SignFormEntity> GetDataByDatePeriod(DateTime deliveryDateStart, DateTime deliveryDateEnd, string stationScode)
        {
            var data = from request in JunFuDbContext.TcDeliveryRequests
                       join checkPhoto in JunFuDbContext.CheckPhotoToS3File on request.CheckNumber equals checkPhoto.FileName into t3
                       from checkPhoto in t3.DefaultIfEmpty()
                       join itemCodes in JunFuDbContext.TbItemCodes on request.LatestArriveOption equals itemCodes.CodeId into t4
                       from itemCodes in t4.DefaultIfEmpty()
                       join drivers in JunFuDbContext.TbDrivers on request.LatestScanDriverCode.ToUpper() equals drivers.DriverCode into t5
                       from drivers in t5.DefaultIfEmpty()
                       join station in JunFuDbContext.TbStations on request.AreaArriveCode equals station.StationScode into t6
                       from station in t6.DefaultIfEmpty()
                       where request.PrintDate >= deliveryDateStart && request.PrintDate < deliveryDateEnd.AddDays(1) && request.AreaArriveCode == stationScode && itemCodes.CodeSclass == "AO"
                       orderby request.RequestId
                       select new SignFormEntity
                       {
                           RequestId = request.RequestId,
                           CheckNumber = request.CheckNumber,
                           DeliverDriver = request.LatestScanDriverCode.ToUpper() + drivers.DriverName,
                           ArriveOption = itemCodes.CodeName,
                           SignPaperPath = checkPhoto.S3Path == null ? "" : checkPhoto.S3Path.Replace(Configuration.GetValue<string>("S3SignPaperPhotoIP"), Configuration.GetValue<string>("S3Url")),
                           SignPaperString = checkPhoto.S3Path == null ? "下載空白簽單" : "查看簽單",
                           ArriveDate = request.LatestScanDate == null ? "" : ((DateTime)request.LatestScanDate).ToString("yyyy-MM-dd HH:mm:ss"),
                           AreaArriveStation = station.StationName,
                           IsSignPaperExist = !(checkPhoto.S3Path == null),
                           IsReturn = !(request.DeliveryType == "D")
                       };

            return data.Distinct();
        }

        public IEnumerable<SignFormEntity> GetDataByCustomerCode(DateTime deliveryDateStart, DateTime deliveryDateEnd, string customerCode)
        {
            var data = from request in JunFuDbContext.TcDeliveryRequests
                       join checkPhoto in JunFuDbContext.CheckPhotoToS3File on request.CheckNumber equals checkPhoto.FileName into t3
                       from checkPhoto in t3.DefaultIfEmpty()
                       join itemCodes in JunFuDbContext.TbItemCodes on request.LatestArriveOption equals itemCodes.CodeId into t4
                       from itemCodes in t4.DefaultIfEmpty()
                       join drivers in JunFuDbContext.TbDrivers on request.LatestScanDriverCode.ToUpper() equals drivers.DriverCode into t5
                       from drivers in t5.DefaultIfEmpty()
                       join station in JunFuDbContext.TbStations on request.AreaArriveCode equals station.StationScode into t6
                       from station in t6.DefaultIfEmpty()
                       where request.CustomerCode == customerCode && request.PrintDate >= deliveryDateStart && request.PrintDate < deliveryDateEnd.AddDays(1) && itemCodes.CodeSclass == "AO"
                       orderby request.RequestId
                       select new SignFormEntity
                       {
                           RequestId = request.RequestId,
                           CheckNumber = request.CheckNumber,
                           DeliverDriver = request.LatestScanDriverCode.ToUpper() + drivers.DriverName,
                           ArriveOption = itemCodes.CodeName,
                           SignPaperPath = checkPhoto.S3Path == null ? "" : checkPhoto.S3Path.Replace(Configuration.GetValue<string>("S3SignPaperPhotoIP"), Configuration.GetValue<string>("S3Url")),
                           SignPaperString = checkPhoto.S3Path == null ? "下載空白簽單" : "查看簽單",
                           ArriveDate = request.LatestScanDate == null ? "" : ((DateTime)request.LatestScanDate).ToString("yyyy-MM-dd HH:mm:ss"),
                           AreaArriveStation = station.StationName,
                           IsSignPaperExist = !(checkPhoto.S3Path == null),
                           IsReturn = !(request.DeliveryType == "D")
                       };
            return data.Distinct();
        }
    }
}
