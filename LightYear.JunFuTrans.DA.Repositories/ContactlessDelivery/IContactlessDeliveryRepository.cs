﻿using LightYear.JunFuTrans.BL.BE.ContactlessDelivery;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.ContactlessDelivery
{
    public interface IContactlessDeliveryRepository
    {
        List<ContactlessDeliveryEntity> CheckReceiveContactTel(string checknumber, string tel);
        List<ContactlessDeliveryEntity> CheckDeliveryDriverCode(string checknumber, string driverCode);
        List<ContactlessDeliveryEntity> GetReceiveContactDataByChecknumber(string checknumber);
        bool InsertContactlessDeliveryCheckNumberIntoScanLog(string driverCode, string checkNumber, int pieces);
        DateTime? CheckIfScanOrNot(string checknumber);
        string FindLatestDeliveryDriver(string checknumber);
        string CheckLatestDeliveryDriverNullOrNot(string checknumber);
    }
}
