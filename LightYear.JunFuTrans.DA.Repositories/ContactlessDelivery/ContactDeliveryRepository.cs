﻿using LightYear.JunFuTrans.BL.BE.ContactlessDelivery;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.ContactlessDelivery
{
    public class ContactDeliveryRepository : IContactlessDeliveryRepository
    {
        public JunFuDbContext JunFuDbContext {get; private set;}

        public ContactDeliveryRepository(JunFuDbContext junFuDbContext)
        {
            JunFuDbContext = junFuDbContext;
        }

        public List<ContactlessDeliveryEntity> CheckReceiveContactTel(string checknumber, string tel)
        {
            return (from a in JunFuDbContext.TcDeliveryRequests
                        where a.CheckNumber.Equals(checknumber)
                        && ((!string.IsNullOrEmpty(a.ReceiveTel1) && a.ReceiveTel1.Substring(a.ReceiveTel1.Length - 3, 3).Equals(tel))
                            || (!string.IsNullOrEmpty(a.ReceiveTel2) && a.ReceiveTel2.Substring(a.ReceiveTel2.Length - 3, 3).Equals(tel)))
                        select new ContactlessDeliveryEntity
                        {
                            CheckNumber = a.CheckNumber,
                            SendContact = a.SendContact,
                            ReceiveContact = a.ReceiveContact,
                            ReceiveTel1 = a.ReceiveTel1 ?? "",
                            ReceiveTel2 = a.ReceiveTel2 ?? "",
                            Pieces = a.Pieces ?? 0,
                            CollectionMoney = a.CollectionMoney ?? 0,
                            Time = DateTime.Now.ToString("yyyy/MM/dd HH:mm")
                        }).ToList();
        }

        public List<ContactlessDeliveryEntity> CheckDeliveryDriverCode(string checknumber, string driverCode)
        {
            return (from a in JunFuDbContext.TcDeliveryRequests
                    where a.CheckNumber.Equals(checknumber)
                    && (!string.IsNullOrEmpty(a.LatestDeliveryDriver) && a.LatestDeliveryDriver.Substring(a.LatestDeliveryDriver.Length - 3, 3).Equals(driverCode))
                    select new ContactlessDeliveryEntity
                    {
                        CheckNumber = a.CheckNumber,
                        SendContact = a.SendContact,
                        ReceiveContact = a.ReceiveContact,
                        ReceiveTel1 = a.ReceiveTel1 ?? "",
                        ReceiveTel2 = a.ReceiveTel2 ?? "",
                        Pieces = a.Pieces ?? 0,
                        CollectionMoney = a.CollectionMoney ?? 0,
                        Time = DateTime.Now.ToString("yyyy/MM/dd HH:mm")
                    }).ToList();
        }

        public List<ContactlessDeliveryEntity> GetReceiveContactDataByChecknumber(string checknumber)
        {
            return (from a in JunFuDbContext.TcDeliveryRequests
                    where a.CheckNumber.Equals(checknumber)
                    select new ContactlessDeliveryEntity
                    {
                        CheckNumber = a.CheckNumber,
                        SendContact = a.SendContact,
                        ReceiveContact = a.ReceiveContact,
                        ReceiveTel1 = a.ReceiveTel1 ?? "",
                        ReceiveTel2 = a.ReceiveTel2 ?? "",
                        Pieces = a.Pieces ?? 0,
                        CollectionMoney = a.CollectionMoney ?? 0,
                        Time = DateTime.Now.ToString("yyyy/MM/dd HH:mm")
                    }).ToList();
        }

        public bool InsertContactlessDeliveryCheckNumberIntoScanLog(string driverCode, string checkNumber, int pieces)
        {
            TtDeliveryScanLog entity = new TtDeliveryScanLog() { DriverCode = driverCode, CheckNumber = checkNumber, OptionCategory = "1", ScanDate = DateTime.Now, Cdate = DateTime.Now, Pieces = pieces, ScanItem = "3", ArriveOption = "3" };
            JunFuDbContext.TtDeliveryScanLogs.Add(entity);
            try
            {
                JunFuDbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public DateTime? CheckIfScanOrNot(string checknumber)
        {
            return JunFuDbContext.TtDeliveryScanLogs
                .Where(a => a.ScanItem.Equals("3") && a.ArriveOption.Equals("3")
                && a.OptionCategory.Equals("1") && a.CheckNumber.Equals(checknumber)).FirstOrDefault()?.ScanDate;
        }

        public string FindLatestDeliveryDriver(string checknumber)
        {
            return (JunFuDbContext.TcDeliveryRequests
                   .Where(a => a.CheckNumber.Equals(checknumber))
                   .Select(a => a.LatestDeliveryDriver).FirstOrDefault()) ?? "Z010007"; //無接觸配達工號
        }

        public string CheckLatestDeliveryDriverNullOrNot(string checknumber)
        {
            return (JunFuDbContext.TcDeliveryRequests
                   .Where(a => a.CheckNumber.Equals(checknumber))
                   .Select(a => a.LatestDeliveryDriver).FirstOrDefault());
        }
    }
}
