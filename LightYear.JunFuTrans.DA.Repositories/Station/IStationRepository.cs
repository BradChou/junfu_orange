﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.DA.Repositories.Station
{
    public interface IStationRepository
    {
        TbStation GetById(int id);

        TbStation GetByScode(string scode);

        List<TbStation> GetAll();

        List<TbStation> GetStartWith(string startWith);

        List<StationArea> GetAreaStation();

        string GetStationNameByScode(string scode);

        string GetStationNameByStationCode(string stationCode);

        string GetStationScodeByName(string stationName);

        string GetStationSCodeByStationCode(string stationCode);

        string GetStationCodeByStationScode(string stationScode);

        string GetStationCodeByName(string stationName);

        TbStation GetByStationCode(string stationCode);

        Dictionary<string, TbStation> GetTbStations(List<string> stationCodes);

        Dictionary<string, TbStation> GetTbStationsByIds(List<string> ids);

        List<TbStation> GetListStationNameByListStationScode(List<string> stationScode);

        IEnumerable<TbStation> GetStationsByScodes(IEnumerable<string> stationScodes);

        List<TbStation> GetStationByArea(string areaName);

        List<string> GetAreaList();
    }
}
