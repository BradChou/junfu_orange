﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.BL.BE.DeliveryRequest;

namespace LightYear.JunFuTrans.DA.Repositories.Station
{
    public interface IOrgAreaRepository
    {
        public void Update(OrgArea orgArea);

        public List<OrgArea> GetByFSEAddressEntity(FSEAddressEntity fSEAddressEntity);

        public List<OrgArea> GetByStationSCode(string stationSCode);

        public void BatchUpdateSD(List<int> ids, string sdValue);

        public void BatchUpdateMD(List<int> ids, string mdValue);

        public void BatchUpdatePutOrder(List<int> ids, string poValue);

        public List<CityAreaEntity> GetCityAreas(string road);
    }
}
