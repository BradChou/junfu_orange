﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;

namespace LightYear.JunFuTrans.DA.Repositories.Station
{
    public class StationAreaRepository : IStationAreaRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public JunFuDbContext JunFuDbContext { get; private set; }

        public StationAreaRepository(JunFuTransDbContext junFuTransDbContext, JunFuDbContext junFuDbContext)
        {
            JunFuTransDbContext = junFuTransDbContext;
            JunFuDbContext = junFuDbContext;
        }

        public List<string> GetAreaList()
        {
            return JunFuTransDbContext.StationAreas.Where(s => s.Area != null).Select(s => s.Area).Distinct().ToList();
        }

        public StationArea GetStationAreaById(int id)
        {
            return JunFuTransDbContext.StationAreas.Find(id);
        }

        public int Insert(StationArea stationArea)
        {
            JunFuTransDbContext.Add(stationArea);
            JunFuTransDbContext.SaveChanges();
            return stationArea.Id;
        }

        public int Update(StationArea stationArea)
        {
            JunFuTransDbContext.StationAreas.Attach(stationArea);
            var entry = JunFuTransDbContext.Entry(stationArea);
            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            JunFuTransDbContext.SaveChanges();

            return stationArea.Id;
        }

        public void Delete(int id)
        {
            var station = JunFuTransDbContext.StationAreas.Find(id);
            if (station != null)
            {
                JunFuTransDbContext.StationAreas.Remove(station);
                JunFuTransDbContext.SaveChanges();
            }
        }

        public StationArea GetByStationScode(string stationScode)
        {
            return JunFuTransDbContext.StationAreas.FirstOrDefault(s => s.StationScode == stationScode);
        }

        public List<StationArea> GetStationsByAreaName(string areaName)
        {
            return JunFuTransDbContext.StationAreas.Where(s => s.Area.Equals(areaName)).ToList();
        }

        public List<JFDepartmentDropDownListEntity> GetJFDepartment()
        {
            return JunFuDbContext.TbItemCodes.Where(a => a.CodeBclass == "6" && a.CodeSclass == "jf_dept")
                .Select(s => new JFDepartmentDropDownListEntity(s.CodeId, s.CodeName)).ToList();
        }
        public List<JFDepartmentDropDownListEntity> GetFeetype()
        {
            return JunFuDbContext.TbItemCodes.Where(a => a.CodeBclass == "6" && a.CodeSclass == "fee_type" && a.ActiveFlag == true)
                .Select(s => new JFDepartmentDropDownListEntity(s.CodeId, s.CodeName)).ToList();
        }
    }
}
