﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.Station
{
    public class StationRepository : IStationRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public StationRepository(JunFuDbContext junFuDbContext, JunFuTransDbContext junFuTransDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
            this.JunFuTransDbContext = junFuTransDbContext;
        }

        /// <summary>
        /// 取得站所資料
        /// </summary>
        /// <param name="id">站所序號</param>
        /// <returns>站所</returns>
        public TbStation GetById(int id)
        {
            var data = from tbStation in this.JunFuDbContext.TbStations where tbStation.Id.Equals(id) select tbStation;

            List<TbStation> stations = data.ToList();

            if (stations.Count > 0)
            {
                return stations[0];
            }
            else
            {
                return null;
            }
        }

        public TbStation GetByScode(string scode)
        {
            return JunFuDbContext.TbStations.SingleOrDefault(s => s.StationScode == scode);
        }
        public TbStation GetByStationCode(string stationCode)
        {
            return JunFuDbContext.TbStations.SingleOrDefault(s => s.StationCode == stationCode);
        }

        public List<TbStation> GetAll()                                         //站所依照區屬排序
        {
            var data = JunFuDbContext.TbStations.ToList();
            data.Sort((x,y)=>x.BusinessDistrict is null || y.BusinessDistrict is null ? (x.BusinessDistrict is null ? "infinite".CompareTo(y.BusinessDistrict) : x.BusinessDistrict.CompareTo("infinite")) : x.BusinessDistrict.CompareTo(y.BusinessDistrict));
            return data;
        }

        public string GetStationNameByScode(string scode)
        {
            return JunFuDbContext.TbStations.Where(s => s.StationScode == scode).Select(s=> s.StationName).FirstOrDefault();            
        }

        public string GetStationNameByStationCode(string stationCode)
        {
            return JunFuDbContext.TbStations.Where(s => s.StationCode == stationCode).Select(s => s.StationName).FirstOrDefault();
        }
        public List<TbStation> GetListStationNameByListStationScode(List<string> stationScode)
        {
            var data = (from t in JunFuDbContext.TbStations where stationScode.Contains(t.StationScode) 
                        select new TbStation {StationName = t.StationName }).ToList();
            return data;
        }


        public string GetStationScodeByName(string stationName)
        {
            return JunFuDbContext.TbStations.Where(s => s.StationName.Equals(stationName)).Select(s => s.StationScode).FirstOrDefault();
        }

        public string GetStationCodeByName(string stationName)
        {
            return JunFuDbContext.TbStations.Where(s => s.StationName.Equals(stationName)).Select(s => s.StationCode).FirstOrDefault();
        }

        public List<TbStation> GetStartWith(string startWith)
        {
            var data = from s in this.JunFuDbContext.TbStations where (s.StationCode.StartsWith(startWith) || s.StationScode.StartsWith(startWith) || s.StationName.StartsWith(startWith)) orderby s.StationScode select s;
            return data.ToList();
        }

        public List<StationArea> GetAreaStation()
        {
            return JunFuTransDbContext.StationAreas.ToList();
        }

        public string GetStationSCodeByStationCode(string stationCode)
        {
            return JunFuDbContext.TbStations.Where(s => s.StationCode.Equals(stationCode)).Select(s => s.StationScode).FirstOrDefault();
        }

        public string GetStationCodeByStationScode(string stationScode)
        {
            return JunFuDbContext.TbStations.Where(s => s.StationScode.Equals(stationScode)).Select(s => s.StationCode).FirstOrDefault();
        }

        public Dictionary<string, TbStation> GetTbStations(List<string> stationCodes)
        {
            var data = (from s in this.JunFuDbContext.TbStations where stationCodes.Contains(s.StationScode) select s).ToDictionary(p => p.StationScode);

            return data;
        }

        public Dictionary<string, TbStation> GetTbStationsByIds(List<string> ids)
        {
            var data = (from s in this.JunFuDbContext.TbStations where ids.Contains(s.Id.ToString()) select s).ToDictionary(p => p.Id.ToString());

            return data;
        }

        public IEnumerable<TbStation> GetStationsByScodes(IEnumerable<string> stationScodes)
        {
            return JunFuDbContext.TbStations.Where(s => stationScodes.Contains(s.StationScode));
        }

        public List<TbStation> GetStationByArea(string areaName)
        {
            return JunFuDbContext.TbStations.Where(s => s.BusinessDistrict.Equals(areaName)).ToList();
        }

        public List<string> GetAreaList()
        {
            return JunFuDbContext.TbStations.Select(s => s.BusinessDistrict).Distinct().ToList();
        }
    }
}
