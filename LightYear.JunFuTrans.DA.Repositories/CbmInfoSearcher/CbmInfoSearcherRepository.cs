﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.BL.BE.CbmInfo;
using System.Linq;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.DA.Repositories.CbmInfoSearcher
{
    public class CbmInfoSearcherRepository : ICbmInfoSearcherRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }
        public CbmInfoSearcherRepository(JunFuDbContext junFuDbContext, JunFuTransDbContext junFuTransDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
            this.JunFuTransDbContext = junFuTransDbContext;
        }

        public TcDeliveryRequest[] GetCbmInfoByCheckNumber(string checkNumber)
        {
            var data = this.JunFuDbContext.TcDeliveryRequests.Where(x => x.CheckNumber == checkNumber);
            return Filter(data.ToArray());
        }

        public TcDeliveryRequest[] GetCbmInfoByDateAndCustomerCode(DateTime? start, DateTime? end, string customerCode)
        {
            var data = this.JunFuDbContext.TcDeliveryRequests.Where(x => x.PrintDate < end && x.PrintDate > start && x.CustomerCode == customerCode);
            return Filter(data.ToArray());
        }
        public TcDeliveryRequest[] GetCbmInfoByDateAndStation(DateTime? start, DateTime? end, string stationCode)
        {
            var data = this.JunFuDbContext.TcDeliveryRequests.Where(x => x.PrintDate < end && x.PrintDate > start && x.SupplierCode == stationCode);
            return Filter(data.ToArray());
        }
        public TcDeliveryRequest[] GetCbmInfoByCustomerCode(string customerCode)
        {
            var data = this.JunFuDbContext.TcDeliveryRequests.Where(x => x.CustomerCode == customerCode);
            return Filter(data.ToArray());
        }
        public TcDeliveryRequest[] GetCbmInfoByStation(string stationCode)
        {
            var data = this.JunFuDbContext.TcDeliveryRequests.Where(x => x.SupplierCode == stationCode);
            return Filter(data.ToArray());
        }
        public TcDeliveryRequest[] GetCbmInfoByDate(DateTime? start, DateTime? end)
        {
            var data = this.JunFuDbContext.TcDeliveryRequests.Where(x => x.PrintDate < end && x.PrintDate > start);
            return Filter(data.ToArray());
        }

        private TcDeliveryRequest[] Filter(TcDeliveryRequest[] input)
        {
            return input.Where(x => !x.CheckNumber.StartsWith("880") && !x.CheckNumber.StartsWith("600") && !x.CheckNumber.StartsWith("500")
                        && x.LatestScanItem != null && !x.LatestScanItem.Equals("5") && !x.LatestScanItem.Equals("6") 
                        && x.LessThanTruckload == true).ToArray();
        }
    }
}
