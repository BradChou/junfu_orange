﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.TMS
{
    public interface ILtWarehouseRepository
    {
        List<LtWarehouse> GetLtWarehouses();

        Dictionary<int, List<int>> GetWarehouseStationMapping();

        List<LtWarehouseBoxCount> GetBoxCountsByPrintDate(DateTime printDate);

        bool InsertBoxCountEntity(LtWarehouseBoxCount entity);

        bool UpdateBoxCountEntity(LtWarehouseBoxCount entity);
    }
}
