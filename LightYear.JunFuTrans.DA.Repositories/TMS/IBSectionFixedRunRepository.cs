﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.DA.Repositories.TMS
{
    public class BSectionRelayStop
    {
        public BSectionRelayStop(int Id, string Name, DateTime ArriveAt, DateTime DispatchAt, int RunFlowTypeId)
        {
            this.Id = Id;
            this.Name = Name;
            this.ArriveAt = ArriveAt;
            this.DispatchAt = DispatchAt;
            this.RunFlowTypeId = RunFlowTypeId;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime ArriveAt { get; set; }

        public DateTime DispatchAt { get; set; }

        public int RunFlowTypeId { get; set; }
    }

    public class BSectionFixedRunData
    {
        public BSectionFixedRunData(int carTypeID, int runTypeID, bool isActive, bool isLtl, int fromID, int toID, bool doesSundayDispatch, bool doesMondayDispatch, bool doesTuesdayDispatch, bool doesWednesdayDispatch, bool doesThursdayDispatch, bool doesFridayDispatch, bool doesSaturdayDispatch, DateTime goStartAt, DateTime goEndAt, DateTime backStartAt, DateTime backEndAt, List<BSectionRelayStop> goRelays, List<BSectionRelayStop> backRelays)
        {
            this.CarTypeId = carTypeID;
            this.RunTypeId = runTypeID;
            this.IsActive = isActive;
            this.IsLtl = isLtl;
            this.FromId = fromID;
            this.ToId = toID;
            this.DoesSundayDispatch = doesSundayDispatch;
            this.DoesMondayDispatch = doesMondayDispatch;
            this.DoesTuesdayDispatch = doesTuesdayDispatch;
            this.DoesWednesdayDispatch = doesWednesdayDispatch;
            this.DoesThursdayDispatch = doesThursdayDispatch;
            this.DoesFridayDispatch = doesFridayDispatch;
            this.DoesSaturdayDispatch = doesSaturdayDispatch;
            this.GoStartAt = goStartAt;
            this.GoEndAt = goEndAt;
            this.BackStartAt = backStartAt;
            this.BackEndAt = backEndAt;
            this.GoRelays = goRelays;
            this.BackRelays = backRelays;
        }

        public int CarTypeId { get; set; }
        public int RunTypeId { get; set; }
        public bool IsActive { get; set; }
        public bool IsLtl { get; set; }
        public int FromId { get; set; }
        public int ToId { get; set; }
        public bool DoesSundayDispatch { get; set; }
        public bool DoesMondayDispatch { get; set; }
        public bool DoesTuesdayDispatch { get; set; }
        public bool DoesWednesdayDispatch { get; set; }
        public bool DoesThursdayDispatch { get; set; }
        public bool DoesFridayDispatch { get; set; }
        public bool DoesSaturdayDispatch { get; set; }
        public DateTime GoStartAt { get; set; }
        public DateTime GoEndAt { get; set; }
        public DateTime BackStartAt { get; set; }
        public DateTime BackEndAt { get; set; }
        public List<BSectionRelayStop> GoRelays { get; set; }
        public List<BSectionRelayStop> BackRelays { get; set; }
    }

    public enum OperationStatus
    {
        Normal,
        Stopping,
        All
    }
    public interface IBSectionFixedRunRepository
    {
        /// <summary>
        /// 取得固定班次列表
        /// </summary>
        /// <param name="operationStatus">運行狀態</param>
        /// <returns></returns>
        List<BSectionFixedRun> GetBSectionFixedRunList(OperationStatus operationStatus);

        /// <summary>
        /// 依照班次取得經停點列表
        /// </summary>
        /// <param name="runId">班次ID</param>
        /// <returns></returns>
        List<BSectionRelayStop> GetBSectionRelayStopList(int runId);

        /// <summary>
        /// 依照站點ID取得站點
        /// </summary>
        /// <param name="id">站點ID</param>
        /// <returns></returns>
        BSectionStop GetStopById(int Id);

        /// <summary>
        /// 依照車種ID取得車種
        /// </summary>
        /// <param name="id">車種ID</param>
        /// <returns></returns>
        CarType GetCarTypeById(int Id);

        /// <summary>
        /// 取得車種資料
        /// </summary>
        /// <returns></returns>
        List<CarType> GetCarTypes();

        /// <summary>
        /// 取得流向資料
        /// </summary>
        /// <returns></returns>
        List<RunFlowType> GetRunFlowTypes();

        /// <summary>
        /// 依照班次流向ID取得班次流向
        /// </summary>
        /// <param name="id">流向ID</param>
        /// <returns></returns>
        RunFlowType GetRunFlowTypeById(int Id);

        /// <summary>
        /// 依照屬性取得最新班次流水號
        /// </summary>
        /// <param name="isLtl">是否有零擔</param>
        /// <returns></returns>
        int GetMaxNumberByAttribute(bool isLtl);

        /// <summary>
        /// 新增班次資料
        /// </summary>
        /// <param name="bSectionFixedRun">班次資料</param>
        /// <returns></returns>
        BSectionFixedRun InsertBSectionFixedRun(BSectionFixedRun bSectionFixedRun);

        /// <summary>
        /// 更新班次資料
        /// </summary>
        /// <param name="bSectionFixedRun">班次資料</param>
        /// <returns></returns>
        BSectionFixedRun UpdateBSectionFixedRun(BSectionFixedRun bSectionFixedRun);

        /// <summary>
        /// 新增班次及經停點對映
        /// </summary>
        /// <param name="bSectionMiddleStopMapping">對映資料</param>
        /// <returns></returns>
        BSectionMiddleStopMapping InsertBSectionMiddleStopMapping(BSectionMiddleStopMapping bSectionMiddleStopMapping);

        /// <summary>
        /// 刪除班次所有經停點
        /// </summary>
        /// <param name="id">班次 id</param>
        /// <returns></returns>
        int DeleteAllBSectionMiddleStopMappingsByRunID(int id);

        /// <summary>
        /// 取得B段停靠點資料
        /// </summary>
        /// <returns></returns>
        List<BSectionStop> GetBSectionStops();

        /// <summary>
        /// 依照停靠點 ID 取得區配商資料
        /// </summary>
        /// <returns></returns>
        List<TbSupplier> GetSuppliersByStationID(int id);

        Dictionary<int, List<decimal>> GetMappingByStopIDs(List<int> ids);

        /// <summary>
        /// 取得區配商資料
        /// </summary>
        /// <returns></returns>
        List<TbSupplier> GetSuppliers();

        /// <summary>
        /// 建立B段停靠點
        /// </summary>
        /// <returns></returns>
        BSectionStop CreateNewBSectionStop(BSectionStop newStopData);

        /// <summary>
        /// 建立B段停靠點及區配商對映
        /// </summary>
        /// <returns></returns>
        BSectionStopSupplierMapping CreateNewStopSupplierMapping(int stopID, int supplierID);

        /// <summary>
        /// 刪除停靠點的區配商
        /// </summary>
        /// <param name="id">停靠點 ID</param>
        /// <returns></returns>
        int DeleteAllBSectionStopSupplierMappingByStopId(int id);

        /// <summary>
        /// 更新停靠點資料
        /// </summary>
        /// <param name="newStopData">停靠點資料</param>
        /// <returns></returns>
        BSectionStop UpdateBSectionStop(BSectionStop newStopData);


    }
}
