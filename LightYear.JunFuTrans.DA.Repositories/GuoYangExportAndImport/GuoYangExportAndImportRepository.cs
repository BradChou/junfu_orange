﻿using LightYear.JunFuTrans.BL.BE.GuoYangExportAndImport;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.GuoYangExportAndImport
{
    public class GuoYangExportAndImportRepository : IGuoYangExportAndImportRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }

        public GuoYangExportAndImportRepository(JunFuDbContext junFuDbContext)
        {
            JunFuDbContext = junFuDbContext;
        }

        public List<FrontendEntity> GetByDateAndStationScode(DateTime start, DateTime end, string scode)
        {
            var data = JunFuDbContext.TtTransferLogs.Where(b => (b.DriverCode == "pp990" || b.DriverCode == "PP990") && b.StationCode == "29" && b.Transfer == "2");

            var request =  (scode == "-1") ? //全部站所
                           (from da in data
                           join re in JunFuDbContext.TcDeliveryRequests on da.CheckNumber equals re.CheckNumber
                           orderby da.CheckNumber ascending
                           where re.PrintDate > start && re.PrintDate < end.AddDays(1)
                           && re.LatestArriveOption == null
                           select new FrontendEntity
                           {
                               CheckNumber = da.CheckNumber,
                               PrintDate = re.PrintDate,
                               Pieces = re.Pieces
                           }).Distinct() :
                           (from da in data
                           join re in JunFuDbContext.TcDeliveryRequests on da.CheckNumber equals re.CheckNumber
                           orderby da.CheckNumber ascending
                           where re.SendStationScode == scode && re.PrintDate > start && re.PrintDate < end.AddDays(1)
                           && re.LatestArriveOption == null
                           select new FrontendEntity
                           {
                               CheckNumber = da.CheckNumber,
                               PrintDate = re.PrintDate,
                               Pieces = re.Pieces
                           }).Distinct();

            return request.ToList();           
        }

        public List<FrontendEntity> GetByDate(DateTime start, DateTime end)
        {
            var data = JunFuDbContext.TtTransferLogs.Where(b => (b.DriverCode == "pp990" || b.DriverCode == "PP990") && b.StationCode == "29" && b.Transfer == "2");

            var request = (from da in data
                           join re in JunFuDbContext.TcDeliveryRequests on da.CheckNumber equals re.CheckNumber
                           orderby da.CheckNumber ascending
                           where re.PrintDate > start && re.PrintDate < end.AddDays(1)
                           && re.LatestArriveOption == null
                           select new FrontendEntity
                           {
                               CheckNumber = da.CheckNumber,
                               PrintDate = re.PrintDate,
                               Pieces = re.Pieces
                           }).Distinct();

            return request.ToList();
        }
    }
}
