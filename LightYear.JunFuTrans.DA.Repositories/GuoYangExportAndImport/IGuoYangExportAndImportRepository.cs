﻿using LightYear.JunFuTrans.BL.BE.GuoYangExportAndImport;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.GuoYangExportAndImport
{
    public interface IGuoYangExportAndImportRepository
    {
        List<FrontendEntity> GetByDateAndStationScode(DateTime start, DateTime end, string scode);
    }
}
