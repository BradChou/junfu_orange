﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Linq;
using LightYear.JunFuTrans.BL.BE.PickUpRequest;

namespace LightYear.JunFuTrans.DA.Repositories.PickUpRequest
{
    public class PickupRequestForApiuserRepository : IPickupRequestForApiuserRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }

        public PickupRequestForApiuserRepository(JunFuDbContext junFuDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
        }

        public PickupRequestForApiuser Insert(PickupRequestForApiuser pickupRequestForApiuser)
        {
            JunFuDbContext.PickupRequestForApiusers.Add(pickupRequestForApiuser);

            JunFuDbContext.SaveChanges();

            return pickupRequestForApiuser;
        }

        public List<PickUpInfoShowEntity> GetByTimePeriodAndStation(DateTime start, DateTime end, string stationCode)
        {
            return (from pra in JunFuDbContext.PickupRequestForApiusers
                    join dr in JunFuDbContext.TcDeliveryRequests on pra.CheckNumber equals dr.CheckNumber
                    join ts in JunFuDbContext.TbStations on dr.AreaArriveCode equals ts.StationScode
                    where pra.SupplierCode == stationCode && pra.RequestDate >= start && pra.RequestDate <= end && dr.CancelDate == null
                    select new PickUpInfoShowEntity
                    {
                       ReassignMd = pra.ReassignMd,
                       PickupRequestId = pra.Id,
                       RequestId = Convert.ToInt32(dr.RequestId),
                       NotifyDate = pra.RequestDate??new DateTime(),
                       CustomerCode = pra.CustomerCode,
                       CheckNumber = pra.CheckNumber,
                       AssignedMd = pra.Md,
                       AssignedSd = pra.Sd,
                       ShouldPickUpPieces = Convert.ToInt32(pra.Pieces),
                       PutOrder = pra.Putorder,
                       PickUpStation = pra.SupplierCode,
                       ArriveStation=ts.StationName,
                       ReceiveAddress=dr.ReceiveCity+dr.ReceiveArea+dr.ReceiveAddress
                    }
                    ).ToList();
        }

        public List<PickUpInfoShowEntity> GetByTimePeriodAndCustomerCode(DateTime start, DateTime end, string customerCode)
        {
            var s= (from pra in JunFuDbContext.PickupRequestForApiusers
                    join dr in JunFuDbContext.TcDeliveryRequests on pra.CheckNumber equals dr.CheckNumber
                    join ts in JunFuDbContext.TbStations on dr.AreaArriveCode equals ts.StationScode
                    //join prb in JunFuDbContext.PickUpRequestLogs on pra.CheckNumber equals prb.CheckNumber
                    where pra.CustomerCode == customerCode && pra.RequestDate >= start && pra.RequestDate <= end && dr.CancelDate == null
                    select new PickUpInfoShowEntity
                    {
                        ReassignMd = pra.ReassignMd,
                        PickupRequestId = pra.Id,
                        RequestId = Convert.ToInt32(dr.RequestId),
                        NotifyDate = pra.RequestDate ?? new DateTime(),
                        CustomerCode = pra.CustomerCode,
                        CheckNumber = pra.CheckNumber,
                        AssignedMd = pra.Md,
                        AssignedSd = pra.Sd,
                        ShouldPickUpPieces = Convert.ToInt32(pra.Pieces),
                        PutOrder = pra.Putorder,
                        PickUpStation = pra.SupplierCode,
                        ArriveStation = ts.StationName,
                        ReceiveAddress = dr.ReceiveCity + dr.ReceiveArea + dr.ReceiveAddress
                    }
                    ).ToList();
            return s;
        }

        public List<PickUpInfoShowEntity> GetByTimePeriodAndCustomerCodeAndStation(DateTime start, DateTime end, string customerCode, string stationCode)
        {
            var s = (from pra in JunFuDbContext.PickupRequestForApiusers
                    join dr in JunFuDbContext.TcDeliveryRequests on pra.CheckNumber equals dr.CheckNumber
                    join ts in JunFuDbContext.TbStations on dr.AreaArriveCode equals ts.StationScode
                    where pra.CustomerCode == customerCode && pra.RequestDate >= start && pra.RequestDate <= end && dr.CancelDate == null &&
                    pra.SupplierCode == stationCode
                    select new PickUpInfoShowEntity
                    {
                    ReassignMd = pra.ReassignMd,
                    PickupRequestId = pra.Id,
                    RequestId = Convert.ToInt32(dr.RequestId),
                    NotifyDate = pra.RequestDate ?? new DateTime(),
                    CustomerCode = pra.CustomerCode,
                    CheckNumber = pra.CheckNumber,
                    AssignedMd = pra.Md,
                    AssignedSd = pra.Sd,
                    ShouldPickUpPieces = Convert.ToInt32(pra.Pieces),
                    PutOrder = pra.Putorder,
                    PickUpStation = pra.SupplierCode,
                    ArriveStation = ts.StationName,
                    ReceiveAddress = dr.ReceiveCity + dr.ReceiveArea + dr.ReceiveAddress
                    }
                    ).ToList();
            return s;
        }

        public List<PickUpInfoShowEntity> GetByTimePeriod(DateTime start, DateTime end)
        {
            return 
            (from pra in JunFuDbContext.PickupRequestForApiusers
             join dr in JunFuDbContext.TcDeliveryRequests on pra.CheckNumber equals dr.CheckNumber
             join ts in JunFuDbContext.TbStations on dr.AreaArriveCode equals ts.StationScode
             where pra.RequestDate >= start && pra.RequestDate <= end
             select new PickUpInfoShowEntity
             {
                 ReassignMd = pra.ReassignMd,
                 PickupRequestId = pra.Id,
                 RequestId = Convert.ToInt32(dr.RequestId),
                 NotifyDate = pra.RequestDate ?? new DateTime(),
                 CustomerCode = pra.CustomerCode,
                 CheckNumber = pra.CheckNumber,
                 AssignedMd = pra.Md,
                 AssignedSd = pra.Sd,
                 ShouldPickUpPieces = Convert.ToInt32(pra.Pieces),
                 PutOrder = pra.Putorder,
                 PickUpStation = pra.SupplierCode,
                 ArriveStation = ts.StationName,
                 ReceiveAddress = dr.ReceiveCity + dr.ReceiveArea + dr.ReceiveAddress
             }
             ).ToList();
        }
    }
}
