﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.PickUpRequest
{
    public class PickUpRequestRepository : IPickUpRequestRepository
    {
        public JunFuDbContext JunFuDbContext { get; set; }

        public PickUpRequestRepository(JunFuDbContext dbContext)
        {
            JunFuDbContext = dbContext;
        }

        public IEnumerable<PickUpRequestLog> GetPickUpRequestByStationAndRequestTimePeriod(string supplierStationCode, DateTime requestTimeStart, DateTime requestTimeEnd)  //篩選:時間、站所
        {
            return JunFuDbContext.PickUpRequestLogs.Where(p => p.Cdate >= requestTimeStart && p.Cdate < requestTimeEnd)
                    .Where(p => p.RequestCustomerCode.StartsWith(supplierStationCode));
        }

        public IEnumerable<PickUpRequestLog> GetPickUpRequestByStationAndRequestTimePeriodAndCustomerCode(string supplierStationCode, DateTime requestTimeStart, DateTime requestTimeEnd, string customerCode) //篩選:時間、客代
        {
            return GetPickUpRequestByRequestTimePeriod(requestTimeStart, requestTimeEnd).Where(x => x.RequestCustomerCode == customerCode);
        }

        public IEnumerable<PickUpRequestLog> GetPickUpRequestByRequestTimePeriod(DateTime requestTimeStart, DateTime requestTimeEnd) //篩選:時間
        {
            return JunFuDbContext.PickUpRequestLogs.Where(p => p.Cdate >= requestTimeStart && p.Cdate < requestTimeEnd);
        }

        public long Insert(PickUpRequestLog pickUpRequestLog)
        {
            pickUpRequestLog.Cdate = DateTime.Now;

            pickUpRequestLog.Udate = DateTime.Now;

            JunFuDbContext.PickUpRequestLogs.Add(pickUpRequestLog);

            JunFuDbContext.SaveChanges();

            return pickUpRequestLog.PickUpId;
        }

        public IEnumerable<TbItemCode> GetTbItemCodesAllowToEdit()
        {
            //return JunFuDbContext.TbItemCodes.Where(x => x.CodeSclass == "RO" && new string[4] { "16", "21", "18", "19" }.Contains(x.CodeId));     //此四個號碼是議題#789指定
            string[] itemCodesArray = new string[8] { "16", "6", "12", "18", "8", "7", "9", "22" }; //此八個號碼是議題#816指定
            TbItemCode[] result = new TbItemCode[8];
            for (var i = 0; i < itemCodesArray.Length; i++)
            {
                result[i] = JunFuDbContext.TbItemCodes.Where(x => x.CodeSclass == "RO" && x.CodeId == itemCodesArray[i]).FirstOrDefault();
            }
            return result;
        }

        public bool InsertIntoScanLogRo(IEnumerable<TtDeliveryScanLog> scanLogs)
        {
            JunFuDbContext.TtDeliveryScanLogs.AddRange(scanLogs);
            JunFuDbContext.SaveChanges();
            return true;
        }

        public bool UpdateDeliveryRequests(IEnumerable<string> checkNumbers, string accountCode)
        {
            IEnumerable<TcDeliveryRequest> requests = JunFuDbContext.TcDeliveryRequests.Where(x => checkNumbers.Contains(x.CheckNumber));
            foreach (var r in requests)
            {
                r.OrangeR11Uuser = accountCode;
            }
            JunFuDbContext.TcDeliveryRequests.AttachRange(requests);
            JunFuDbContext.SaveChanges();
            return true;
        }

        public bool UpdatePickupRequest(PickUpRequestLog log)
        {
            var dataInDb = JunFuDbContext.PickUpRequestLogs.FirstOrDefault(p => p.PickUpId == log.PickUpId);

            dataInDb.ReassignMd = log.ReassignMd;
            dataInDb.MdUuser = log.MdUuser;

            JunFuDbContext.SaveChanges();
            return true;
        }

        public bool UpdatePickupRequestForApi(PickupRequestForApiuser log)
        {
            var dataInDb = JunFuDbContext.PickupRequestForApiusers.FirstOrDefault(p => p.Id == log.Id);

            dataInDb.ReassignMd = log.ReassignMd;
            dataInDb.MdUuser = log.MdUuser;

            JunFuDbContext.SaveChanges();
            return true;
        }

        public IEnumerable<TtDeliveryScanLog> GetPickUpInfoByCheckNumber(IEnumerable<string> checkNumbers)
        {
            return JunFuDbContext.TtDeliveryScanLogs.Where(x => checkNumbers.Contains(x.CheckNumber) && x.ScanItem == "5");
        }

        public TbStation[] GetAllStations()
        {
            return JunFuDbContext.TbStations.ToArray();
        }
    }
}
