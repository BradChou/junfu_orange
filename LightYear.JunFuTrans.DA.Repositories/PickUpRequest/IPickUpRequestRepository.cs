﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.PickUpRequest
{
    public interface IPickUpRequestRepository
    {
        /// <summary>
        /// 從發送站跟客戶送出"派員收件"的時間
        /// </summary>
        /// <param name="supplierStationCode"></param>
        /// <param name="requestTimeStart"></param>
        /// <param name="requestTimeEnd"></param>
        /// <returns></returns>
        IEnumerable<PickUpRequestLog> GetPickUpRequestByStationAndRequestTimePeriod(string supplierStationCode, DateTime requestTimeStart, DateTime requestTimeEnd);

        IEnumerable<PickUpRequestLog> GetPickUpRequestByStationAndRequestTimePeriodAndCustomerCode(string supplierStationCode, DateTime requestTimeStart, DateTime requestTimeEnd, string customerCode);

        IEnumerable<PickUpRequestLog> GetPickUpRequestByRequestTimePeriod(DateTime requestTimeStart, DateTime requestTimeEnd);

        Int64 Insert(PickUpRequestLog pickUpRequestLog);

        IEnumerable<TbItemCode> GetTbItemCodesAllowToEdit();

        bool InsertIntoScanLogRo(IEnumerable<TtDeliveryScanLog> scanLogs);

        bool UpdateDeliveryRequests(IEnumerable<string> requests, string accountCode);

        bool UpdatePickupRequest(PickUpRequestLog log);

        bool UpdatePickupRequestForApi(PickupRequestForApiuser log);

        IEnumerable<TtDeliveryScanLog> GetPickUpInfoByCheckNumber(IEnumerable<string> checkNumbers);

        TbStation[] GetAllStations();
    }
}
