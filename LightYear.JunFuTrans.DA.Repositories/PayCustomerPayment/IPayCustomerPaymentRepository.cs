﻿using LightYear.JunFuTrans.BL.BE.PayCustomerPayment;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.PayCustomerPayment
{
    public interface IPayCustomerPaymentRepository
    {
        void InsertBatch(List<JunFuDb.PayCustomerPayment> Entities);

        List<FrontendEntity> GetAll(DateTime start, DateTime end, string station, string customer);

        void Update(JunFuDb.PayCustomerPayment entity);

        List<JunFuDb.PayCustomerPayment> IsInsertPayCustomerPaymentOrNot(List<string> checknumbers);
    }
}
