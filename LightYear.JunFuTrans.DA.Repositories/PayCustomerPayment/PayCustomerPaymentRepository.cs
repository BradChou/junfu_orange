﻿using LightYear.JunFuTrans.BL.BE.PayCustomerPayment;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.PayCustomerPayment
{
    public class PayCustomerPaymentRepository : IPayCustomerPaymentRepository
    {
        public JunFuDbContext JunFuDbContext { get; set; }

        public PayCustomerPaymentRepository(JunFuDbContext junFuDbContext)
        {
            JunFuDbContext = junFuDbContext;
        }

        public void InsertBatch(List<JunFuDb.PayCustomerPayment> Entities)
        {
            this.JunFuDbContext.PayCustomerPayments.AddRange(Entities);

            this.JunFuDbContext.SaveChanges();
        }

        public List<FrontendEntity> GetAll(DateTime start, DateTime end, string station = null, string customer = null)
        {
            // with nolock in linq 
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required
             , new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
            {
                if (customer == null)
                {
                    var data = (station == null || station == "-1")?
                                ((from dr in JunFuDbContext.TcDeliveryRequests
                                  join p in JunFuDbContext.PayCustomerPayments on dr.CheckNumber equals p.CheckNumber into ps
                                  from p in ps.DefaultIfEmpty()
                                  where dr.CollectionMoney != 0 & dr.CollectionMoney != null & dr.LessThanTruckload.Equals(true)
                              & dr.LatestArriveOptionDate < end & dr.LatestArriveOptionDate >= start & dr.CheckNumber.Length > 0
                              & dr.DeliveryType != "R" & dr.LatestArriveOption == "3"
                                  select new FrontendEntity
                                  {
                                      SendStation = dr.SupplierCode,
                                      CheckNumber = dr.CheckNumber,
                                      AreaArriveCode = dr.AreaArriveCode,
                                      Customer = dr.CustomerCode,
                                      Payment = dr.CollectionMoney ?? 0,
                                      IsPaid = (p.IsPaid == null || p.IsPaid == false) ? "未支付" : "已支付"
                                  }).Distinct().ToList()) :
                               (from dr in JunFuDbContext.TcDeliveryRequests
                                join p in JunFuDbContext.PayCustomerPayments on dr.CheckNumber equals p.CheckNumber into ps
                                from p in ps.DefaultIfEmpty()
                                where dr.CollectionMoney != 0 & dr.CollectionMoney != null & dr.LessThanTruckload.Equals(true)
                                & dr.LatestArriveOptionDate < end & dr.LatestArriveOptionDate >= start & dr.CheckNumber.Length > 0
                                & dr.DeliveryType != "R" & dr.LatestArriveOption == "3" && dr.SupplierCode == station
                                select new FrontendEntity
                                {
                                    SendStation = dr.SupplierCode,
                                    CheckNumber = dr.CheckNumber,
                                    AreaArriveCode = dr.AreaArriveCode,
                                    Customer = dr.CustomerCode,
                                    Payment = dr.CollectionMoney ?? 0,
                                    IsPaid = (p.IsPaid == null || p.IsPaid == false) ? "未支付" : "已支付"
                                }).Distinct().ToList();
                    return data;
                }
                else
                {
                    var data = station == null ?
                               ((from dr in JunFuDbContext.TcDeliveryRequests
                                 join p in JunFuDbContext.PayCustomerPayments on dr.CheckNumber equals p.CheckNumber into ps
                                 from p in ps.DefaultIfEmpty()
                                 where dr.CollectionMoney != 0 & dr.CollectionMoney != null & dr.LessThanTruckload.Equals(true)
                             & dr.LatestArriveOptionDate < end & dr.LatestArriveOptionDate >= start & dr.CheckNumber.Length > 0
                             & dr.DeliveryType != "R" & dr.LatestArriveOption == "3" && dr.CustomerCode == customer
                                 select new FrontendEntity
                                 {
                                     SendStation = dr.SupplierCode,
                                     CheckNumber = dr.CheckNumber,
                                     AreaArriveCode = dr.AreaArriveCode,
                                     Customer = dr.CustomerCode,
                                     Payment = dr.CollectionMoney ?? 0,
                                     IsPaid = (p.IsPaid == null || p.IsPaid == false) ? "未支付" : "已支付"
                                 }).Distinct().ToList()) :
                              (from dr in JunFuDbContext.TcDeliveryRequests
                               join p in JunFuDbContext.PayCustomerPayments on dr.CheckNumber equals p.CheckNumber into ps
                               from p in ps.DefaultIfEmpty()
                               where dr.CollectionMoney != 0 & dr.CollectionMoney != null & dr.LessThanTruckload.Equals(true)
                               & dr.LatestArriveOptionDate < end & dr.LatestArriveOptionDate >= start & dr.CheckNumber.Length > 0
                               & dr.DeliveryType != "R" & dr.LatestArriveOption == "3" && dr.SupplierCode == station && dr.CustomerCode == customer
                               select new FrontendEntity
                               {
                                   SendStation = dr.SupplierCode,
                                   CheckNumber = dr.CheckNumber,
                                   AreaArriveCode = dr.AreaArriveCode,
                                   Customer = dr.CustomerCode,
                                   Payment = dr.CollectionMoney ?? 0,
                                   IsPaid = (p.IsPaid == null || p.IsPaid == false) ? "未支付" : "已支付"
                               }).Distinct().ToList();
                    return data;
                }
            }
        }

        public void Update(JunFuDb.PayCustomerPayment entity)
        {
            this.JunFuDbContext.PayCustomerPayments.Attach(entity);

            var entry = this.JunFuDbContext.Entry(entity);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            entry.Property(x => x.CreateDate).IsModified = false;

            this.JunFuDbContext.SaveChanges();
        }

        public List<JunFuDb.PayCustomerPayment> IsInsertPayCustomerPaymentOrNot(List<string> checknumbers)
        {
            var data = (from dp in JunFuDbContext.PayCustomerPayments
                        where checknumbers.Contains(dp.CheckNumber)
                        select dp).ToList();
            if (data.Count > 0)
            {
                return data;
            }
            return null;
        }
    }
}
