﻿using LightYear.JunFuTrans.BL.BE.TodayPickUp;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.TodayPickUp
{
    public interface ITodayPickUpRepository
    {
        List<TtDeliveryScanLog> TodayPickUpRepositoryEntities(TodayPickUpInputEntity todayPickUpInputEntity);
        string GetDriverNameByDriverCode(string driverCode);
        string GetDriverStationByScode(string scode);
    }
}
