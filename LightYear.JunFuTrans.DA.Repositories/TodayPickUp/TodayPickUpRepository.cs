﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.BL.BE.TodayPickUp;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.TodayPickUp
{
    public class TodayPickUpRepository :ITodayPickUpRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }

        public TodayPickUpRepository(JunFuDbContext junFuDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
        }
        public List<TtDeliveryScanLog> TodayPickUpRepositoryEntities(TodayPickUpInputEntity todayPickUpInputEntity)
        {
            DateTime today = DateTime.Parse(DateTime.Now.ToString().Split(' ')[0]);
            var data = from logs in JunFuDbContext.TtDeliveryScanLogs where logs.DriverCode.Equals(todayPickUpInputEntity.DriverCode) & logs.ScanItem.Equals("5") & logs.ScanDate < today.AddDays(1).AddHours(5) & logs.ScanDate > today.AddHours(5)
                       orderby logs.ScanDate descending
                       select logs;

            return data.ToList();
        }
        public string GetDriverNameByDriverCode(string driverCode)
        {
            var data = from driver in JunFuDbContext.TbDrivers
                       where driverCode == driver.DriverCode
                       select driver.DriverName;

            return data.First();
        }
        public string GetDriverStationByScode(string scode)
        {
            var data = from station in JunFuDbContext.TbStations
                       where scode == station.StationScode
                       select station.StationName;

            return data.First();
        }
    }
}
