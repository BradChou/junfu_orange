﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.CbmSize
{
    public class CbmSizeRepository : ICbmSizeRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }
        public CbmSizeRepository(JunFuDbContext junFuDbContext)
        {
            JunFuDbContext = junFuDbContext;
        }
        public TcCbmSize[] GetAllCbmSize()
        {
            return JunFuDbContext.TcCbmSizes.ToArray();
        }
    }
}
