﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.DA.Repositories.ConstantData
{
    public interface IRegionRepository
    {
        /// <summary>
        /// 取得城市資料
        /// </summary>
        /// <returns></returns>
        List<TbPostCity> GetCityList();

        /// <summary>
        /// 取得個各城市行政區資料
        /// </summary>
        /// <returns></returns>
        List<TbPostCityArea> GetDistrictList();
    }
}
