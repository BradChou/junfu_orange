﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;
using System.Linq;
using System.Security.Cryptography;
using LightYear.JunFuTrans.BL.BE.DeliveryRate;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;

namespace LightYear.JunFuTrans.DA.Repositories.ConstantData
{

    public class RegionRepository : IRegionRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }

        public RegionRepository(JunFuDbContext junFuDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
        }

        public List<TbPostCity> GetCityList()
        {
            List<TbPostCity> citys = (from city in this.JunFuDbContext.TbPostCities select city).ToList();

            return citys;
        }

        public List<TbPostCityArea> GetDistrictList()
        {
            List<TbPostCityArea> districts = (from district in this.JunFuDbContext.TbPostCityAreas select district).ToList();

            return districts;
        }
    }
}
