﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.BL.BE.SignPaperPhoto;

namespace LightYear.JunFuTrans.DA.Repositories.SignPaperPhotoPackage
{
    public interface ISignPaperPhotoPackageRepository
    {
        IEnumerable<SignPaperPhotoOutputEntity> GetRequestsByAccountCodeAndPrintDateAndShipDate(string customerCode, DateTime printDateStart, DateTime printDateEnd, DateTime shipDateStart, DateTime shipDateEnd);

        IEnumerable<SignPaperPhotoOutputEntity> GetRequestsByCustomerCodeAndShipDate(string customerCode, DateTime shipDateStart, DateTime shipDateEnd);

        IEnumerable<SignPaperPhotoOutputEntity> GetRequestsByCustomerCodeAndPrintDate(string customerCode, DateTime printDateStart, DateTime printDateEnd);

        IEnumerable<CheckNumberPackageProcess> GetCheckNumberPackageProcesses(string loginAccountCode);

        public IEnumerable<CheckNumberWithPhoto> GetCaterpillarPhotoFileName(IEnumerable<string> checkNumbers);

        public CheckPhotoToS3File GetTaiChungSchedulePhotoById(string checkNumber);
    }
}
