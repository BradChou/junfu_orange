﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.Account
{
    public class EmpRepository : IEmpRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }

        public EmpRepository(JunFuDbContext junFuDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
        }

        public TbEmp GetByEmpCode(string empCode)
        {
            var data = from tbEmp in JunFuDbContext.TbEmps where tbEmp.EmpCode.Equals(empCode) select tbEmp;

            if (data.Count() > 0)
            {
                return data.FirstOrDefault();
            }
            else
            {
                return null;
            }
        }

        public Dictionary<string, TbEmp> GetEmpsByEmpCodes(List<string> empCodes)
        {
            var data = (from emp in this.JunFuDbContext.TbEmps where empCodes.Contains(emp.EmpCode) select emp).ToLookup(p => p.EmpCode).ToDictionary(p => p.Key, p => p.First());

            return data;
        }
    }
}
