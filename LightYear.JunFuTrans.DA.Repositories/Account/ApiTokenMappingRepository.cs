﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.Account
{
    public class ApiTokenMappingRepository : IApiTokenMappingRepository
    {
        public JunFuTransDbContext JunFuTransDbContext;

        public ApiTokenMappingRepository(JunFuTransDbContext junFuTransDbContext)
        {
            this.JunFuTransDbContext = junFuTransDbContext;
        }

        public void Delete(int id)
        {
            var apiTokenMapping = this.JunFuTransDbContext.ApiTokenMappings.Find(id);

            if (apiTokenMapping != null)
            {
                this.JunFuTransDbContext.ApiTokenMappings.Remove(apiTokenMapping);

                this.JunFuTransDbContext.SaveChanges();
            }
        }

        public List<ApiTokenMapping> GetAllApiTokenMappings()
        {
            var data = from apiTokenMapping in this.JunFuTransDbContext.ApiTokenMappings orderby apiTokenMapping.Id descending select apiTokenMapping;

            return data.ToList();
        }

        public ApiTokenMapping GetApiTokenMapping(int id)
        {
            ApiTokenMapping apiTokenMapping = this.JunFuTransDbContext.ApiTokenMappings.Find(id);

            return apiTokenMapping;
        }

        public List<ApiTokenMapping> GetApiTokenMappingsByToken(string token)
        {
            var data = from apiTokenMapping in this.JunFuTransDbContext.ApiTokenMappings where apiTokenMapping.Token.Equals(token) orderby apiTokenMapping.Id descending select apiTokenMapping;

            return data.ToList();
        }

        public int Insert(ApiTokenMapping apiTokenMapping)
        {
            apiTokenMapping.CreateDate = DateTime.Now;

            apiTokenMapping.UpdateDate = DateTime.Now;

            this.JunFuTransDbContext.ApiTokenMappings.Add(apiTokenMapping);

            this.JunFuTransDbContext.SaveChanges();

            return apiTokenMapping.Id;
        }

        public void Update(ApiTokenMapping apiTokenMapping)
        {
            apiTokenMapping.UpdateDate = DateTime.Now;

            this.JunFuTransDbContext.ApiTokenMappings.Attach(apiTokenMapping);

            var entry = this.JunFuTransDbContext.Entry(apiTokenMapping);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            entry.Property(x => x.CreateDate).IsModified = false;

            this.JunFuTransDbContext.SaveChanges();
        }
    }
}
