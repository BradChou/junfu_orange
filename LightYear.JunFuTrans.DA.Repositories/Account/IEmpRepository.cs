﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.DA.Repositories.Account
{
    public interface IEmpRepository
    {
        TbEmp GetByEmpCode(string empCode);

        Dictionary<string, TbEmp> GetEmpsByEmpCodes(List<string> empCodes);
    }
}
