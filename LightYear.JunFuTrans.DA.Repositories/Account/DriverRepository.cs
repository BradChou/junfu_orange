﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.Account
{
    public class DriverRepository : IDriverRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }

        public DriverRepository(JunFuDbContext junFuDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
        }

        public TbDriver GetDriverByAccountAndPassword(string account, string password)
        {
            var data = from tbDriver in JunFuDbContext.TbDrivers where tbDriver.DriverCode.Equals(account) && tbDriver.LoginPassword.Equals(password) select tbDriver;

            if (data.Count() > 0)
            {
                return data.FirstOrDefault();
            }
            else
            {
                return null;
            }
        }

        public TbDriver GetByDriverCode(string driverCode)
        {
            var data = from tbDriver in JunFuDbContext.TbDrivers where tbDriver.DriverCode.Equals(driverCode) select tbDriver;

            if (data.Count() > 0)
            {
                return data.FirstOrDefault();
            }
            else
            {
                return null;
            }
        }

        public List<TbDriver> GetAccountsStartWith(string startWith, int num)
        {
            var data = from m in this.JunFuDbContext.TbDrivers where m.DriverCode.StartsWith(startWith) || m.DriverName.StartsWith(startWith) orderby m.DriverCode select m;

            var output = data.Take(num);

            return output.ToList();
        }

        public List<TbDriver> GetNumAccounts(int num)
        {
            var data = this.JunFuDbContext.TbDrivers.OrderByDescending(u => u.DriverId).Take(num).OrderBy(o => o.DriverCode);

            return data.ToList();
        }

        public TbDriver GetById(decimal id)
        {
            var data = this.JunFuDbContext.TbDrivers.Find(id);

            return data;
        }

        public List<TbDriver> GetDriversByStation(string stationScode)
        {
            return JunFuDbContext.TbDrivers.Where(d => d.Station.Equals(stationScode)).ToList();
        }

        public Dictionary<string, TbDriver> GetDriversByDriverCodes(List<string> driverCodes)
        {
            var data = (from driver in this.JunFuDbContext.TbDrivers where driverCodes.Contains(driver.DriverCode) select driver).ToDictionary(p => p.DriverCode.ToUpper());

            return data;
        }

        public TbDriver GetByEmpCode(string empCode)
        {
            var data = from tbDriver in JunFuDbContext.TbDrivers where tbDriver.EmpCode.Equals(empCode) select tbDriver;

            if (data.Count() > 0)
            {
                return data.FirstOrDefault();
            }
            else
            {
                return null;
            }
        }

        public string GetDriverNameByDriverCode(string driverCode)
        {
            return JunFuDbContext.TbDrivers.Where(d => d.DriverCode.Equals(driverCode)).Select(d => d.DriverName).FirstOrDefault();
        }

        public Dictionary<string, string> GetDriverNameByDriverCode(IEnumerable<string> driverCode)
        {
            return JunFuDbContext.TbDrivers.Where(d => driverCode.Contains(d.DriverCode))
                .Select(d => new { DriverCode = d.DriverCode, DriverName = d.DriverName }).ToDictionary(d => d.DriverCode, d => d.DriverName);
        }
    }
}
