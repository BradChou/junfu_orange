﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.Account
{
    public class FunctionUserMappingRepository : IFunctionUserMappingRepository
    {
        public JunFuTransDbContext JunFuTransDbContext;

        public FunctionUserMappingRepository(JunFuTransDbContext junFuTransDbContext)
        {
            this.JunFuTransDbContext = junFuTransDbContext;
        }

        public void Delete(int id)
        {
            var systemFunctionUserMappings = this.JunFuTransDbContext.SystemFunctionUserMappings.Find(id);

            if (systemFunctionUserMappings != null)
            {
                this.JunFuTransDbContext.SystemFunctionUserMappings.Remove(systemFunctionUserMappings);

                this.JunFuTransDbContext.SaveChanges();
            }
        }

        public List<SystemFunctionUserMapping> GetAllSystemFunctionUserMappings()
        {
            var data = from systemFunctionUserMapping in this.JunFuTransDbContext.SystemFunctionUserMappings orderby systemFunctionUserMapping.Id descending select systemFunctionUserMapping;

            return data.ToList();
        }

        public SystemFunctionUserMapping GetSystemFunctionUserMapping(int id)
        {
            var data = from systemFunctionUserMapping in this.JunFuTransDbContext.SystemFunctionUserMappings where systemFunctionUserMapping.Id.Equals(id) select systemFunctionUserMapping;

            if (data.Count() == 0)
            {
                return null;
            }

            return data.FirstOrDefault();
        }

        public int Insert(SystemFunctionUserMapping systemFunctionUserMapping)
        {
            systemFunctionUserMapping.CreateDate = DateTime.Now;

            systemFunctionUserMapping.UpdateDate = DateTime.Now;

            this.JunFuTransDbContext.SystemFunctionUserMappings.Add(systemFunctionUserMapping);

            this.JunFuTransDbContext.SaveChanges();

            return systemFunctionUserMapping.Id;
        }

        public void Update(SystemFunctionUserMapping systemFunctionUserMapping)
        {
            systemFunctionUserMapping.UpdateDate = DateTime.Now;

            this.JunFuTransDbContext.SystemFunctionUserMappings.Attach(systemFunctionUserMapping);

            var entry = this.JunFuTransDbContext.Entry(systemFunctionUserMapping);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            entry.Property(x => x.CreateDate).IsModified = false;

            this.JunFuTransDbContext.SaveChanges();
        }

        public List<SystemFunctionUserMapping> GetAllSystemFunctionUserMappingsByUserIdAndUserType(int userId, int userType)
        {
            var data = from systemFunctionUserMapping in this.JunFuTransDbContext.SystemFunctionUserMappings where systemFunctionUserMapping.UserId.Equals(userId) && systemFunctionUserMapping.UserType.Equals(userType) select systemFunctionUserMapping;

            return data.ToList();
        }

        public void DeleteByUserIdAndAccountType(int userId, int accountType)
        {
            var query = this.JunFuTransDbContext.SystemFunctionUserMappings.Where(o => o.UserId == userId && o.UserType == accountType);

            this.JunFuTransDbContext.SystemFunctionUserMappings.RemoveRange(query);

            this.JunFuTransDbContext.SaveChanges();
        }
    }
}
