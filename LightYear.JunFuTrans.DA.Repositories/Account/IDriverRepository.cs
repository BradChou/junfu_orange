﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.DA.Repositories.Account
{
    public interface IDriverRepository
    {
        public TbDriver GetDriverByAccountAndPassword(string account, string password);

        public TbDriver GetByDriverCode(string driverCode);

        public TbDriver GetByEmpCode(string empCode);

        public List<TbDriver> GetAccountsStartWith(string startWith, int num);

        public List<TbDriver> GetNumAccounts(int num);

        public TbDriver GetById(decimal id);

        List<TbDriver> GetDriversByStation(string stationScode);

        Dictionary<string, TbDriver> GetDriversByDriverCodes(List<string> driverCodes);

        string GetDriverNameByDriverCode(string driverCode);

        Dictionary<string, string> GetDriverNameByDriverCode(IEnumerable<string> driverCode);
    }
}
