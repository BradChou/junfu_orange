﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.Account
{
    public class FunctionRoleMappingRepository : IFunctionRoleMappingRepository
    {
        public JunFuTransDbContext JunFuTransDbContext;

        public FunctionRoleMappingRepository(JunFuTransDbContext junFuTransDbContext)
        {
            this.JunFuTransDbContext = junFuTransDbContext;
        }

        public void Delete(int id)
        {
            var systemFunctionRoleMappings = this.JunFuTransDbContext.SystemFunctionRoleMappings.Find(id);

            if (systemFunctionRoleMappings != null)
            {
                this.JunFuTransDbContext.SystemFunctionRoleMappings.Remove(systemFunctionRoleMappings);

                this.JunFuTransDbContext.SaveChanges();
            }
        }

        public List<SystemFunctionRoleMapping> GetAllSystemFunctionRoleMappings()
        {
            var data = from systemFunctionRoleMapping in this.JunFuTransDbContext.SystemFunctionRoleMappings orderby systemFunctionRoleMapping.Id descending select systemFunctionRoleMapping;

            return data.ToList();
        }

        public List<SystemFunctionRoleMapping> GetAllSystemFunctionRoleMappingsByRoleId(int roleId)
        {
            var data = from systemFunctionRoleMapping in this.JunFuTransDbContext.SystemFunctionRoleMappings where systemFunctionRoleMapping.RoleId.Equals(roleId) select systemFunctionRoleMapping;

            return data.ToList();
        }

        public SystemFunctionRoleMapping GetSystemFunctionRoleMapping(int id)
        {
            var data = from systemFunctionRoleMapping in this.JunFuTransDbContext.SystemFunctionRoleMappings where systemFunctionRoleMapping.Id.Equals(id) select systemFunctionRoleMapping;

            if (data.Count() == 0)
            {
                return null;
            }

            return data.FirstOrDefault();
        }

        public int Insert(SystemFunctionRoleMapping systemFunctionRoleMapping)
        {
            systemFunctionRoleMapping.CreateDate = DateTime.Now;

            systemFunctionRoleMapping.UpdateDate = DateTime.Now;

            this.JunFuTransDbContext.SystemFunctionRoleMappings.Add(systemFunctionRoleMapping);

            this.JunFuTransDbContext.SaveChanges();

            return systemFunctionRoleMapping.Id;
        }

        public void Update(SystemFunctionRoleMapping systemFunctionRoleMapping)
        {
            systemFunctionRoleMapping.UpdateDate = DateTime.Now;

            this.JunFuTransDbContext.SystemFunctionRoleMappings.Attach(systemFunctionRoleMapping);

            var entry = this.JunFuTransDbContext.Entry(systemFunctionRoleMapping);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            entry.Property(x => x.CreateDate).IsModified = false;

            this.JunFuTransDbContext.SaveChanges();
        }


        public void DeleteByRoleId(int roleId)
        {
            var query = this.JunFuTransDbContext.SystemFunctionRoleMappings.Where(o => o.RoleId == roleId);

            this.JunFuTransDbContext.SystemFunctionRoleMappings.RemoveRange(query);

            this.JunFuTransDbContext.SaveChanges();
        }
    }
}
