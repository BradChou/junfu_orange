﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Security.Cryptography;

namespace LightYear.JunFuTrans.DA.Repositories.Account
{
    public class AccountsRepository : IAccountsRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }

        public AccountsRepository(JunFuDbContext junFuDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
        }

        public TbAccount GetByAccountCode(string account)
        {
            var data = from tbAccount in JunFuDbContext.TbAccounts where tbAccount.AccountCode.Equals(account) select tbAccount;

            if (data.Count() > 0)
            {
                return data.FirstOrDefault();
            }
            else
            {
                return null;
            }
        }

        public TbAccount GetByAccountAndPassword(string account, string password)
        {
            var data = from tbAccount in JunFuDbContext.TbAccounts where tbAccount.AccountCode.Equals(account) && tbAccount.Password.Equals(password) select tbAccount;

            if (data.Count() > 0)
            {
                return data.FirstOrDefault();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 取得最新的幾個帳號
        /// </summary>
        /// <returns></returns>
        public List<TbAccount> GetNumAccounts(int num)
        {
            var data = this.JunFuDbContext.TbAccounts.OrderByDescending(u => u.AccountId).Take(num).OrderBy(o => o.AccountCode);

            return data.ToList();
        }

        /// <summary>
        /// 取得棧板最新的幾個帳號
        /// </summary>
        /// <param name="num"></param>
        /// <param name="managerType">1:俊富管理者, 2:俊富一般員工, 3: 俊富自營, 4:區配商, 5:區配商自營</param>
        /// <returns></returns>
        public List<TbAccount> GetJFNumAccounts(int num, string managerType = "1")
        {
            var data = this.JunFuDbContext.TbAccounts.Where(u => u.AccountType == false && u.ManagerType == managerType)
                .Take(num).OrderBy(o => o.AccountCode);

            return data.ToList();
        }

        /// <summary>
        /// 取得帳號代碼開頭的帳號
        /// </summary>
        /// <param name="startWith"></param>
        /// <returns></returns>
        public List<TbAccount> GetAccountsStartWith(string startWith, int num)
        {
            var data = from m in this.JunFuDbContext.TbAccounts where m.AccountCode.StartsWith(startWith) || m.UserName.StartsWith(startWith) orderby m.AccountCode select m;

            var output = data.Take(num);

            return output.ToList();
        }

        /// <summary>
        /// 取得區配商或區配商自營的帳號
        /// </summary>
        /// <returns></returns>
        public List<TbAccount> GetSuppliersAccount()
        {
            var data = (from m in this.JunFuDbContext.TbAccounts where (m.ManagerType.Equals("4") || m.ManagerType.Equals("5")) && m.ActiveFlag.Equals(true) && m.AccountType.Equals(false)
                       select m).ToList();

            return data;
        }

        public List<TbAccount> GetJFAccountsStartWith(string startWith, int num, string managerType = "1")
        {
            var data = from m in this.JunFuDbContext.TbAccounts where (m.AccountType == false && m.ManagerType == managerType)
                       && (m.AccountCode.StartsWith(startWith) || m.UserName.StartsWith(startWith))
                       orderby m.AccountCode select m;

            var output = data.Take(num);

            return output.ToList();
        }

        public TbAccount GetById(decimal id)
        {
            var data = this.JunFuDbContext.TbAccounts.Find(id);

            return data;
        }
        public List<TbAccount> GetPublicAll()
        {
            var data = this.JunFuDbContext.TbAccounts.Where(p => p.ActiveFlag == true).ToList();

            return data;
        }
    }
}
