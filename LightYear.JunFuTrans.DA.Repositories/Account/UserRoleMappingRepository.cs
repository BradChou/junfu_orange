﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.Account
{
    public class UserRoleMappingRepository : IUserRoleMappingRepository
    {
        public JunFuTransDbContext JunFuTransDbContext;

        public UserRoleMappingRepository(JunFuTransDbContext junFuTransDbContext)
        {
            this.JunFuTransDbContext = junFuTransDbContext;
        }

        public void Delete(int id)
        {
            var userRoleMappings = this.JunFuTransDbContext.UserRoleMappings.Find(id);

            if (userRoleMappings != null)
            {
                this.JunFuTransDbContext.UserRoleMappings.Remove(userRoleMappings);

                this.JunFuTransDbContext.SaveChanges();
            }
        }

        public void DeleteByUserIdAndAccountType(int userId, int accountType)
        {
            var query = this.JunFuTransDbContext.UserRoleMappings.Where(o => o.UserId == userId && o.UserType == accountType);

            this.JunFuTransDbContext.UserRoleMappings.RemoveRange(query);

            this.JunFuTransDbContext.SaveChanges();
        }

        public void DeleteByRoleId(int roleId)
        {
            var query = this.JunFuTransDbContext.UserRoleMappings.Where(o => o.RoleId == roleId);

            this.JunFuTransDbContext.UserRoleMappings.RemoveRange(query);

            this.JunFuTransDbContext.SaveChanges();
        }

        public List<UserRoleMapping> GetAllUserRoleMappings()
        {
            var data = from userRoleMappings in this.JunFuTransDbContext.UserRoleMappings orderby userRoleMappings.Id descending select userRoleMappings;

            return data.ToList();
        }

        public List<UserRoleMapping> GetAllUserRoleMappingsByUserIdAndUserType(int userId, int userType)
        {
            var data = from userRoleMappings in this.JunFuTransDbContext.UserRoleMappings where userRoleMappings.UserId.Equals(userId) && userRoleMappings.UserType.Equals(userType) select userRoleMappings;

            if (data.Count() == 0)
            {
                return new List<UserRoleMapping>();
            }

            return data.ToList();
        }

        public List<UserRoleMapping> GetAllUserRoleMappingsByRoleId(int roleId)
        {
            var data = from userRoleMappings in this.JunFuTransDbContext.UserRoleMappings where userRoleMappings.RoleId.Equals(roleId) select userRoleMappings;

            if (data.Count() == 0)
            {
                return new List<UserRoleMapping>();
            }

            return data.ToList();
        }

        public UserRoleMapping GetUserRoleMapping(int id)
        {
            var data = from userRoleMappings in this.JunFuTransDbContext.UserRoleMappings where userRoleMappings.Id.Equals(id) select userRoleMappings;

            if (data.Count() == 0)
            {
                return null;
            }

            return data.FirstOrDefault();
        }

        public int Insert(UserRoleMapping userRoleMapping)
        {
            userRoleMapping.CreateDate = DateTime.Now;

            userRoleMapping.UpdateDate = DateTime.Now;

            this.JunFuTransDbContext.UserRoleMappings.Add(userRoleMapping);

            this.JunFuTransDbContext.SaveChanges();

            return userRoleMapping.Id;
        }

        public void Update(UserRoleMapping userRoleMapping)
        {
            userRoleMapping.UpdateDate = DateTime.Now;

            this.JunFuTransDbContext.UserRoleMappings.Attach(userRoleMapping);

            var entry = this.JunFuTransDbContext.Entry(userRoleMapping);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            entry.Property(x => x.CreateDate).IsModified = false;

            this.JunFuTransDbContext.SaveChanges();
        }
    }
}
