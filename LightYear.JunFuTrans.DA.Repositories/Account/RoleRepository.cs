﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.Account
{
    public class RoleRepository : IRoleRepository
    {
        public JunFuTransDbContext JunFuTransDbContext;

        public RoleRepository(JunFuTransDbContext junFuTransDbContext)
        {
            this.JunFuTransDbContext = junFuTransDbContext;
        }

        public void Delete(int id)
        {
            var role = this.JunFuTransDbContext.Roles.Find(id);

            if (role != null)
            {
                this.JunFuTransDbContext.Roles.Remove(role);

                this.JunFuTransDbContext.SaveChanges();
            }
        }

        public List<Role> GetAllRoles()
        {
            var data = from role in this.JunFuTransDbContext.Roles orderby role.Id descending select role;

            return data.ToList();
        }

        public List<Role> GetJFAllRoles()
        {
            var data = from role in this.JunFuTransDbContext.Roles
                       where role.Company == "2"
                       orderby role.Id descending select role;

            return data.ToList();
        }

        public Role GetRole(int id)
        {
            var data = from role in this.JunFuTransDbContext.Roles where role.Id.Equals(id) select role;

            if (data.Count() == 0)
            {
                return null;
            }

            return data.FirstOrDefault();
        }

        public int Insert(Role role)
        {
            role.CreateDate = DateTime.Now;

            role.UpdateDate = DateTime.Now;

            this.JunFuTransDbContext.Roles.Add(role);

            this.JunFuTransDbContext.SaveChanges();

            return role.Id;
        }

        public void Update(Role role)
        {

            role.UpdateDate = DateTime.Now;

            this.JunFuTransDbContext.Roles.Attach(role);

            var entry = this.JunFuTransDbContext.Entry(role);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            entry.Property(x => x.CreateDate).IsModified = false;

            this.JunFuTransDbContext.SaveChanges();
        }
    }
}
