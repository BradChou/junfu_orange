﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.DA.Repositories.Account
{
    public interface IRoleRepository
    {
        int Insert(Role role);

        void Update(Role role);

        List<Role> GetAllRoles();

        List<Role> GetJFAllRoles();

        Role GetRole(int id);

        void Delete(int id);
    }
}
