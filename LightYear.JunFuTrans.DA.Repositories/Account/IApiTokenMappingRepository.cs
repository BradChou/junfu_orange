﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.DA.Repositories.Account
{
    public interface IApiTokenMappingRepository
    {
        int Insert(ApiTokenMapping apiTokenMapping);

        void Update(ApiTokenMapping apiTokenMapping);

        List<ApiTokenMapping> GetAllApiTokenMappings();

        ApiTokenMapping GetApiTokenMapping(int id);

        void Delete(int id);

        List<ApiTokenMapping> GetApiTokenMappingsByToken(string token);
    }
}
