﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.DA.Repositories.Account
{
    public interface IFunctionRoleMappingRepository
    {
        int Insert(SystemFunctionRoleMapping systemFunctionRoleMapping);

        void Update(SystemFunctionRoleMapping systemFunctionRoleMapping);

        List<SystemFunctionRoleMapping> GetAllSystemFunctionRoleMappingsByRoleId(int roleId);

        List<SystemFunctionRoleMapping> GetAllSystemFunctionRoleMappings();

        SystemFunctionRoleMapping GetSystemFunctionRoleMapping(int id);

        void Delete(int id);

        void DeleteByRoleId(int roleId);
    }
}
