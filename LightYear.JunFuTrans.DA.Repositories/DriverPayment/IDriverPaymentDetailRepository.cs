﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.DriverPayment
{
    public interface IDriverPaymentDetailRepository
    {
        int Insert(DriverPaymentDetail driverPaymentDetail);

        List<DriverPaymentDetail> GetDetailBySheetId(int paymentId);

        IEnumerable<DriverPaymentDetail> GetDetialsBySheetIds(IEnumerable<int> ids);

        List<DriverPaymentDetail> GetDetailBySheetIds(List<int> paymentIds);

        Dictionary<string, string> GetTicketLastFiveByCheckNubmers(IEnumerable<string> checkNumber);
    }
}
