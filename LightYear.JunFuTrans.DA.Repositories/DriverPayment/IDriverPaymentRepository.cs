﻿using LightYear.JunFuTrans.BL.BE.DriverToStationPayment;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.DriverPayment
{
    public interface IDriverPaymentRepository
    {
        int Insert(DriverPaymentSheet driverPaymentSheet);

        DriverPaymentSheet GetPaymentSheetById(int paymentId);

        IEnumerable<DriverPaymentSheet> GetPaymentSheetByDriverAndDate(string driverCode, DateTime date);

        IEnumerable<DriverPaymentSheet> GetTodaySheetsByDriver(string driverCode);

        List<DriverPaymentSheet> GetListByTimePeriodAndStation(DateTime start, DateTime end, string stationName);

        List<DriverPaymentSheet> GetListByTimeAndStation(DateTime date, string stationScode);

        int GetPreviousPaymentBalanceBeforeTime(DateTime time, string driverCode);

        List<DriverPaymentSheet> GetListByTime(DateTime start, DateTime end);

        int UpdateActualPaidMoney(string driverCode, DateTime date, int actualPaidMoney, int paymentBalance, int accumulatedBalance);

        int SavePaymentBalanceData(DateTime date, Dictionary<string, PaymentBalanceEntity> driverCodeWithPaymentBalance);

        List<DriverPaymentSheet> GetListByTimePeriodAndStationScode(DateTime start, DateTime end, string scode);

        List<DriverPaymentSheet> GetListByTimePeriod(DateTime start, DateTime end);
    }
}
