﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.DriverPayment
{
    public class DriverPaymentDetailRepository : IDriverPaymentDetailRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; set; }

        public DriverPaymentDetailRepository(JunFuTransDbContext dbContext)
        {
            JunFuTransDbContext = dbContext;
        }

        public int Insert(DriverPaymentDetail driverPaymentDetail)
        {
            JunFuTransDbContext.DriverPaymentDetails.Add(driverPaymentDetail);
            JunFuTransDbContext.SaveChanges();
            return driverPaymentDetail.Id;
        }

        public List<DriverPaymentDetail> GetDetailBySheetId(int paymentId)
        {
            return JunFuTransDbContext.DriverPaymentDetails.Where(p => p.PaymentSheetId == paymentId).ToList();
        }

        public IEnumerable<DriverPaymentDetail> GetDetialsBySheetIds(IEnumerable<int> ids)
        {
            return JunFuTransDbContext.DriverPaymentDetails.Where(p => ids.Contains(p.PaymentSheetId));
        }

        public List<DriverPaymentDetail> GetDetailBySheetIds(List<int> paymentIds)
        {
            return JunFuTransDbContext.DriverPaymentDetails.Where(p => paymentIds.Contains(p.PaymentSheetId)).ToList();
        }

        public Dictionary<string, string> GetTicketLastFiveByCheckNubmers(IEnumerable<string> checkNumber)
        {
            var paymentDetail = (JunFuTransDbContext.DriverPaymentDetails.Where(d => checkNumber.Contains(d.CheckNumber))
              .Select(d => new { d.CheckNumber, d.TicketOrRemittanceLastFive })).Distinct();

            return paymentDetail.ToDictionary(d => d.CheckNumber, d => d.TicketOrRemittanceLastFive);
        }
    }
}
