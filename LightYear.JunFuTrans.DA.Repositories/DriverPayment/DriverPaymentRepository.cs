﻿using LightYear.JunFuTrans.BL.BE.DriverToStationPayment;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.DriverPayment
{
    public class DriverPaymentRepository : IDriverPaymentRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public JunFuDbContext JunFuDbContext { get; set; }

        public DriverPaymentRepository(JunFuTransDbContext junFuTransDbContext, JunFuDbContext junFuDbContext)
        {
            JunFuTransDbContext = junFuTransDbContext;
            JunFuDbContext = junFuDbContext;
        }

        public int Insert(DriverPaymentSheet driverPaymentSheet)
        {
            driverPaymentSheet.CreateDate = DateTime.Now;

            JunFuTransDbContext.DriverPaymentSheets.Add(driverPaymentSheet);

            JunFuTransDbContext.SaveChanges();

            return driverPaymentSheet.Id;
        }

        public DriverPaymentSheet GetPaymentSheetById(int paymentId)
        {
            return JunFuTransDbContext.DriverPaymentSheets.SingleOrDefault(p => p.Id == paymentId);
        }

        public IEnumerable<DriverPaymentSheet> GetPaymentSheetByDriverAndDate(string driverCode, DateTime date)
        {
            return JunFuTransDbContext.DriverPaymentSheets.Where(s => s.DriverCode == driverCode && s.CreateDate >= date && s.CreateDate < date.AddDays(1)
            && s.IsValid != false);
        }

        public List<DriverPaymentSheet> GetListByTimePeriodAndStation(DateTime start, DateTime end, string stationName)
        {
            return JunFuTransDbContext.DriverPaymentSheets.Where(p => p.StationName == stationName)
                .Where(p => p.CreateDate >= start).Where(p => p.CreateDate < end.AddDays(1) && p.IsValid != false)
                .ToList();
        }

        public List<DriverPaymentSheet> GetListByTime(DateTime start, DateTime end)
        {
            return JunFuTransDbContext.DriverPaymentSheets
                .Where(p => p.CreateDate >= start).Where(p => p.CreateDate < end.AddDays(1) && p.IsValid != false)
                .ToList();
        }

        public IEnumerable<DriverPaymentSheet> GetTodaySheetsByDriver(string driverCode)
        {
            return JunFuTransDbContext.DriverPaymentSheets.Where(d => d.CreateDate > DateTime.Today && d.DriverCode == driverCode && d.IsValid != false);
        }

        public List<DriverPaymentSheet> GetListByTimeAndStation(DateTime date, string stationScode)
        {
            return JunFuTransDbContext.DriverPaymentSheets.Where(d => d.CreateDate >= date && d.CreateDate < date.AddDays(1) && d.IsValid != false)
                .Where(d => d.StationScode == stationScode).ToList();
        }

        public int GetPreviousPaymentBalanceBeforeTime(DateTime time, string driverCode)
        {
            var last = JunFuTransDbContext.DriverPaymentSheets.Where(s => s.DriverCode == driverCode && s.CreateDate < time && s.PaymentBalance != null)
                .OrderByDescending(s => s.CreateDate).FirstOrDefault();

            return last == null ? 0 : last.AccumulatedPaymentBalance ?? 0;
        }

        public int UpdateActualPaidMoney(string driverCode, DateTime date, int actualPaidMoney, int paymentBalance, int accumulatedBalance)
        {
            var paymentSheetInDb = JunFuTransDbContext.DriverPaymentSheets.Where(s => s.DriverCode == driverCode
                && s.CreateDate >= date && s.CreateDate < date.AddDays(1));
            foreach (var e in paymentSheetInDb)
            {
                e.AcutalPayAmount = actualPaidMoney;
                e.PaymentBalance = paymentBalance;
                e.AccumulatedPaymentBalance = accumulatedBalance;
            }

            JunFuTransDbContext.SaveChanges();
            return paymentSheetInDb.Count();
        }

        public int SavePaymentBalanceData(DateTime date, Dictionary<string, PaymentBalanceEntity> driverCodeWithPaymentBalance)
        {
            // Update
            var paymentSheets = JunFuTransDbContext.DriverPaymentSheets.Where(s => driverCodeWithPaymentBalance.Keys.Contains(s.DriverCode)
                && s.CreateDate >= date && s.CreateDate < date.AddDays(1));

            foreach (var p in paymentSheets)
            {
                p.PaymentBalance = driverCodeWithPaymentBalance[p.DriverCode].CurrentPaymentBalance;
                p.AccumulatedPaymentBalance = driverCodeWithPaymentBalance[p.DriverCode].AccumulatedPaymentBalance;
            }

            // Insert
            var notInDb = driverCodeWithPaymentBalance.Keys.Except(paymentSheets.Select(p => p.DriverCode));
            if (notInDb.Any())
            {
                List<DriverPaymentSheet> newRecord = new List<DriverPaymentSheet>();
                foreach (var p in notInDb)
                {
                    newRecord.Add(new DriverPaymentSheet
                    {
                        DriverCode = p,
                        CreateDate = DateTime.Now,
                        PaymentBalance = driverCodeWithPaymentBalance[p].CurrentPaymentBalance,
                        AccumulatedPaymentBalance = driverCodeWithPaymentBalance[p].AccumulatedPaymentBalance,
                        IsValid = false
                    });
                }
                JunFuTransDbContext.DriverPaymentSheets.AddRange(newRecord);
            }            

            JunFuTransDbContext.SaveChanges();
            return driverCodeWithPaymentBalance.Count;
        }

        public List<DriverPaymentSheet> GetListByTimePeriodAndStationScode(DateTime start, DateTime end, string scode)
        {
            return JunFuTransDbContext.DriverPaymentSheets.Where(p => p.StationScode == scode)
               .Where(p => p.CreateDate >= start).Where(p => p.CreateDate < end).Where(p => p.IsValid != false)
               .ToList();
        }

        public List<DriverPaymentSheet> GetListByTimePeriod(DateTime start, DateTime end)
        {
            return JunFuTransDbContext.DriverPaymentSheets
                .Where(p => p.CreateDate >= start).Where(p => p.CreateDate < end).Where(p => p.IsValid != false)
                .ToList();
        }
    }
}
