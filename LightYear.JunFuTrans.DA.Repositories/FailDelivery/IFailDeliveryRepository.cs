﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.PerformanceReport;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.BL.BE.FailDelivery
{
    public interface IFailDeliveryRepository
    {
        List<FailDeliveryEntity> FailDeliveryEntities(FailDeliveryInputEntity failDeliveryInputEntity);
        List<StationEntity> GetAllStations();
        List<CustomerShortNameEntity> GetCustomerShortNameEntities();
        List<TbCustomer> GetCustomersByStation(string station);
    }
}
