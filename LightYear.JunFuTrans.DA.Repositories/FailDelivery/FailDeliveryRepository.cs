﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.FailDelivery;
using LightYear.JunFuTrans.BL.BE.PerformanceReport;
using LightYear.JunFuTrans.DA.JunFuDb;
using System.Linq;
using System.Text.RegularExpressions;

namespace LightYear.JunFuTrans.DA.Repositories.FailDelivery
{
    public class FailDeliveryRepository : IFailDeliveryRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }

        public FailDeliveryRepository(JunFuDbContext junFuDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
        }
        public List<FailDeliveryEntity> FailDeliveryEntities(FailDeliveryInputEntity failDeliveryInputEntity)
        {
            var data = from scanLog in this.JunFuDbContext.TtDeliveryScanLogs
                       join request in this.JunFuDbContext.TcDeliveryRequests on scanLog.CheckNumber equals request.CheckNumber
                       where request.ShipDate > failDeliveryInputEntity.Start.AddHours(5) &
                             request.ShipDate < failDeliveryInputEntity.End.AddHours(5).AddDays(1) &
                             (request.CustomerCode == failDeliveryInputEntity.CustomerCode || failDeliveryInputEntity.CustomerCode.Equals("All") || (request.CustomerCode.StartsWith(failDeliveryInputEntity.AccountStation) & failDeliveryInputEntity.CustomerCode.Equals("Partial"))) &
                             (request.SupplierCode == failDeliveryInputEntity.SupplierCode || failDeliveryInputEntity.SupplierCode.Equals("All"))
                       orderby request.CheckNumber
                       select new FailDeliveryEntity()
                       {
                           ShipDate = request.ShipDate,
                           SupplierCode = request.SupplierCode,
                           ArriveCode= request.AreaArriveCode,
                           CheckNumber= request.CheckNumber,
                           SupplierPieces= request.Pieces,
                           ArrivePieces= request.Pieces,
                           ReceiveContact= request.ReceiveContact,
                           AbnormalRemarks = failDeliveryInputEntity.AbnormalRemarks,
                           CustomerCode = request.CustomerCode,
                           ExceptionOption = scanLog.ExceptionOption
                       };

            return data.ToList();
        }
        /*public List<FailDeliveryEntity> FailDeliveryEntitiesAllStationAllAccount(FailDeliveryInputEntity failDeliveryInputEntity)
        {
            var data = from scanLog in this.JunFuDbContext.TtDeliveryScanLogs
                       join request in this.JunFuDbContext.TcDeliveryRequests on scanLog.CheckNumber equals request.CheckNumber
                       where scanLog.ExceptionOption.Equals("3") &
                             request.ShipDate > failDeliveryInputEntity.Start.AddHours(5) &
                             request.ShipDate < failDeliveryInputEntity.End.AddHours(5).AddDays(1)
                       orderby request.CheckNumber
                       select new FailDeliveryEntity()
                       {
                           ShipDate = request.ShipDate,
                           SupplierCode = request.SupplierCode,
                           ArriveCode = request.AreaArriveCode,
                           CheckNumber = request.CheckNumber,
                           SupplierPieces = request.Pieces,
                           ArrivePieces = request.Pieces,
                           ReceiveContact = request.ReceiveContact,
                           AbnormalRemarks = failDeliveryInputEntity.AbnormalRemarks,
                           CustomerCode = request.CustomerCode
                       };

            return data.ToList();
        }
        public List<FailDeliveryEntity> FailDeliveryEntitiesOneStationAllAccount(FailDeliveryInputEntity failDeliveryInputEntity)
        {
            var data = from scanLog in this.JunFuDbContext.TtDeliveryScanLogs
                       join request in this.JunFuDbContext.TcDeliveryRequests on scanLog.CheckNumber equals request.CheckNumber
                       where request.ShipDate > failDeliveryInputEntity.Start.AddHours(5) &
                             request.ShipDate < failDeliveryInputEntity.End.AddHours(5).AddDays(1) &
                             request.SupplierCode == failDeliveryInputEntity.SupplierCode
                       orderby request.CheckNumber
                       select new FailDeliveryEntity()
                       {
                           ShipDate = request.ShipDate,
                           SupplierCode = request.SupplierCode,
                           ArriveCode = request.AreaArriveCode,
                           CheckNumber = request.CheckNumber,
                           SupplierPieces = request.Pieces,
                           ArrivePieces = request.Pieces,
                           ReceiveContact = request.ReceiveContact,
                           AbnormalRemarks = failDeliveryInputEntity.AbnormalRemarks,
                           CustomerCode = request.CustomerCode
                       };

            return data.ToList();
        }
        public List<FailDeliveryEntity> FailDeliveryEntitiesLoginStationAllAccount(FailDeliveryInputEntity failDeliveryInputEntity)
        {
            var data = from scanLog in this.JunFuDbContext.TtDeliveryScanLogs
                       join request in this.JunFuDbContext.TcDeliveryRequests on scanLog.CheckNumber equals request.CheckNumber
                       where scanLog.ExceptionOption.Equals("3") &
                             request.ShipDate > failDeliveryInputEntity.Start.AddHours(5) &
                             request.ShipDate < failDeliveryInputEntity.End.AddHours(5).AddDays(1) &
                             request.CustomerCode.StartsWith(failDeliveryInputEntity.AccountStation)
                       orderby request.CheckNumber
                       select new FailDeliveryEntity()
                       {
                           ShipDate = request.ShipDate,
                           SupplierCode = request.SupplierCode,
                           ArriveCode = request.AreaArriveCode,
                           CheckNumber = request.CheckNumber,
                           SupplierPieces = request.Pieces,
                           ArrivePieces = request.Pieces,
                           ReceiveContact = request.ReceiveContact,
                           AbnormalRemarks = failDeliveryInputEntity.AbnormalRemarks,
                           CustomerCode = request.CustomerCode
                       };

            return data.ToList();
        }
        public List<FailDeliveryEntity> FailDeliveryEntitiesOneAccount(FailDeliveryInputEntity failDeliveryInputEntity)
        {
            var data = from scanLog in this.JunFuDbContext.TtDeliveryScanLogs
                       join request in this.JunFuDbContext.TcDeliveryRequests on scanLog.CheckNumber equals request.CheckNumber
                       where scanLog.ExceptionOption.Equals("3") &
                             request.ShipDate > failDeliveryInputEntity.Start.AddHours(5) &
                             request.ShipDate < failDeliveryInputEntity.End.AddHours(5).AddDays(1) &
                             request.CustomerCode == failDeliveryInputEntity.CustomerCode
                       orderby request.CheckNumber
                       select new FailDeliveryEntity()
                       {
                           ShipDate = request.ShipDate,
                           SupplierCode = request.SupplierCode,
                           ArriveCode = request.AreaArriveCode,
                           CheckNumber = request.CheckNumber,
                           SupplierPieces = request.Pieces,
                           ArrivePieces = request.Pieces,
                           ReceiveContact = request.ReceiveContact,
                           AbnormalRemarks = failDeliveryInputEntity.AbnormalRemarks,
                           CustomerCode = request.CustomerCode
                       };

            return data.ToList();
        }*/
        public List<StationEntity> GetAllStations()
        {
            var data = from station in JunFuDbContext.TbStations
                       orderby station.Id
                       select new StationEntity()
                       {
                           Name = station.StationName,
                           Code = station.StationCode,
                           Scode = station.StationScode
                       };
            return data.ToList();
        }
        public List<CustomerShortNameEntity> GetCustomerShortNameEntities()
        {
            var data = from customers in JunFuDbContext.TbCustomers
                       where customers.CustomerCode.StartsWith("F")
                       orderby customers.CustomerCode
                       select new CustomerShortNameEntity()
                       {
                           CustomerCode = customers.CustomerCode,
                           CustomerShortName = customers.CustomerShortname
                       };
            return data.ToList();
        }

        public List<TbCustomer> GetCustomersByStation(string station)
        {
            var data = from customers in JunFuDbContext.TbCustomers
                       where (customers.CustomerCode.StartsWith("F") & station.Equals("-1")) || (customers.CustomerCode.StartsWith(station) & !station.Equals("-1"))
                       orderby customers.CustomerCode
                       select customers;
            
            return data.ToList();
        }
    }
}
