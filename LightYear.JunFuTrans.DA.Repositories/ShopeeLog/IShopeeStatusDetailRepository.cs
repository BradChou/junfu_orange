﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.DA.Repositories.ShopeeLog
{
    interface IShopeeStatusDetailRepository
    {
        long Insert(ShopeeStatusDetail shopeeStatusDetail);

        void InsertDatas(List<ShopeeStatusDetail> shopeeCbminfos);
    }
}
