﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.DA.Repositories.ShopeeLog
{
    public interface IShopeeCbminfoRepository
    {
        long Insert(ShopeeCbminfo shopeeCbminfo);

        void InsertDatas(List<ShopeeCbminfo> shopeeCbminfos);
    }
}
