﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.ShopeeLog
{
    public class ShopeePickupInfoRepository : IShopeePickupInfoRepository
    {
        public JunFuTransDbContext JunFuTransDbContext;

        public ShopeePickupInfoRepository(JunFuTransDbContext junFuTransDbContext)
        {
            JunFuTransDbContext = junFuTransDbContext;
        }

        public long  Insert(ShopeePickupInfo shopeePickupInfo)
        {
            JunFuTransDbContext.Add(shopeePickupInfo);
            JunFuTransDbContext.SaveChanges();
            return shopeePickupInfo.ShopeePickupInfoId;
        }

        public void InsertDatas(List<ShopeePickupInfo> shopeeCbminfos)
        {
            JunFuTransDbContext.ShopeePickupInfoes.AddRange(shopeeCbminfos);
            JunFuTransDbContext.SaveChanges();
        }
    }
}
