﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.ShopeeLog
{
    public class ShopeeStatusDetailRepository : IShopeeStatusDetailRepository
    {
        public JunFuTransDbContext JunFuTransDbContext;

        public ShopeeStatusDetailRepository(JunFuTransDbContext junFuTransDbContext)
        {
            JunFuTransDbContext = junFuTransDbContext;
        }

        public long Insert(ShopeeStatusDetail shopeeStatusDetail)
        {
            JunFuTransDbContext.Add(shopeeStatusDetail);
            JunFuTransDbContext.SaveChanges();
            return shopeeStatusDetail.ShopeeStatusDetailId;
        }

        public void InsertDatas(List<ShopeeStatusDetail> shopeeCbminfos)
        {
            JunFuTransDbContext.ShopeeStatusDetails.AddRange(shopeeCbminfos);
            JunFuTransDbContext.SaveChanges();
        }
    }
}
