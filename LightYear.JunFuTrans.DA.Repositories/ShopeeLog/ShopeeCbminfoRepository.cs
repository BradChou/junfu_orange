﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.ShopeeLog
{
    public class ShopeeCbminfoRepository : IShopeeCbminfoRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public ShopeeCbminfoRepository(JunFuTransDbContext junFuTransDbContext)
        {
            JunFuTransDbContext = junFuTransDbContext;
        }

        public long Insert(ShopeeCbminfo shopeeCbminfo)
        {
            shopeeCbminfo.CreateDatetime = DateTime.Now;
            shopeeCbminfo.UpdateDatetime = DateTime.Now;

            JunFuTransDbContext.Add(shopeeCbminfo);
            JunFuTransDbContext.SaveChanges();
            return shopeeCbminfo.ShopeeCbminfoId;
        }

        public void InsertDatas(List<ShopeeCbminfo> shopeeCbminfos)
        {
            JunFuTransDbContext.ShopeeCbminfoes.AddRange(shopeeCbminfos);
            JunFuTransDbContext.SaveChanges();
        }
    }
}
