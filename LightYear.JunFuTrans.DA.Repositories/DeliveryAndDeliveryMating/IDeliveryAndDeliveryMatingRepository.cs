﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.DeliveryAndDeliveryMating;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryAndDeliveryMating
{
    public interface IDeliveryAndDeliveryMatingRepository
    {
        List<ArriveOptionEntity> GetArriveOptions();
        List<DriversEntity> GetAllDrivers();
        List<StationEntity> GetAllStations();
        int GetCountPieces(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption);
        int GetCountArriveOptions(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption);
        List<DeliveryAndDeliveryMatingEntity> GetAllByDriverCode(DateTime start, DateTime end, string driverCode, string arriveOption);
        List<DeliveryAndDeliveryMatingEntity> GetAllByStationScode(DateTime start, DateTime end, string stationscode, string arriveOption);
        List<DriversEntity> GetDriversByStation(string stationscode);
        IEnumerable<TbItemCode> GetAllTbItemCodesWhichSclassAO();
    }
}
