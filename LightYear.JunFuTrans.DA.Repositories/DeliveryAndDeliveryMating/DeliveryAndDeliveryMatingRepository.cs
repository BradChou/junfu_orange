﻿using LightYear.JunFuTrans.BL.BE.DeliveryAndDeliveryMating;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Transactions;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryAndDeliveryMating
{
    public class DeliveryAndDeliveryMatingRepository : IDeliveryAndDeliveryMatingRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }

        public DeliveryAndDeliveryMatingRepository(JunFuDbContext junFuDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
        }
       
        public List<DeliveryAndDeliveryMatingEntity> GetAllByDriverCode(DateTime start, DateTime end, string driverCode, string arriveOption)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required
            , new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
            {
                var delivery = (arriveOption.Equals("all")) ? //是否為全部配達區分
                            JunFuDbContext.TcDeliveryRequests
                            .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                   && a.LatestDeliveryDriver == driverCode)
                            .Select(a => new DeliveryAndDeliveryMatingEntity
                            {
                                CheckNumber = a.CheckNumber,
                                SubpoenaCategory = a.SubpoenaCategory,
                                Pieces = a.Pieces,
                                ArriveOption = a.LatestScanArriveOption,
                                ScanDate = a.LatestScanDate,
                                ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                                DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                                CollectionMoney = a.CollectionMoney ?? 0
                            }).Distinct().ToList() :
                            JunFuDbContext.TcDeliveryRequests
                            .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                   && a.LatestDeliveryDriver == driverCode && a.LatestScanArriveOption == arriveOption)
                            .Select(a => new DeliveryAndDeliveryMatingEntity
                            {
                                CheckNumber = a.CheckNumber,
                                SubpoenaCategory = a.SubpoenaCategory,
                                Pieces = a.Pieces,
                                ArriveOption = a.LatestScanArriveOption,
                                ScanDate = a.LatestScanDate,
                                ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                                DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                                CollectionMoney = a.CollectionMoney ?? 0
                            }).Distinct().ToList();

                return delivery;
            }
        }

        public List<DeliveryAndDeliveryMatingEntity> GetAllByStationScode(DateTime start, DateTime end, string stationscode, string arriveOption)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required
               , new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
            {
                var specificDrivers = JunFuDbContext.TbDrivers.Where(a => a.Station == stationscode).Select(a => a.DriverCode).ToList();

                var delivery = (arriveOption.Equals("all")) ? //是否為全部配達區分
                            JunFuDbContext.TcDeliveryRequests
                            .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                   && specificDrivers.Contains(a.LatestDeliveryDriver))
                            .Select(a => new DeliveryAndDeliveryMatingEntity
                            {
                                CheckNumber = a.CheckNumber,
                                SubpoenaCategory = a.SubpoenaCategory,
                                Pieces = a.Pieces,
                                ArriveOption = a.LatestScanArriveOption,
                                ScanDate = a.LatestScanDate,
                                ReceiveAddress = a.ReceiveCity+a.ReceiveArea+a.ReceiveAddress,
                                DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                                CollectionMoney = a.CollectionMoney ?? 0                                
                            }).Distinct().ToList() :
                            JunFuDbContext.TcDeliveryRequests
                            .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                   && specificDrivers.Contains(a.LatestDeliveryDriver) && a.LatestScanArriveOption == arriveOption)
                            .Select(a => new DeliveryAndDeliveryMatingEntity
                            {
                                CheckNumber = a.CheckNumber,
                                SubpoenaCategory = a.SubpoenaCategory,
                                Pieces = a.Pieces,
                                ArriveOption = a.LatestScanArriveOption,
                                ScanDate = a.LatestScanDate,
                                ReceiveAddress = a.ReceiveCity+a.ReceiveArea+a.ReceiveAddress,
                                DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                                CollectionMoney = a.CollectionMoney ?? 0
                            }).Distinct().ToList();

                return delivery;
            }
        }

        public int GetCountPieces(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption)
        {
            var count = (driverCode.Equals("all")) ? GetAllByStationScode(start, end, stationscode, arriveOption).Count() : GetAllByDriverCode(start, end, driverCode, arriveOption).Count();
            return count;
        }

        public int GetCountArriveOptions(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption)
        {
            int count = 0;

            if (driverCode.Equals("all"))
            {
                var specificDrivers = JunFuDbContext.TbDrivers.Where(a => a.Station == stationscode).Select(a => a.DriverCode).ToList();

                        count = (arriveOption.Equals("all")) ? //是否為全部配達區分
                                JunFuDbContext.TcDeliveryRequests
                                .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                       && specificDrivers.Contains(a.LatestDeliveryDriver) && a.LatestScanArriveOption != null)
                                .Select(a => new DeliveryAndDeliveryMatingEntity
                                {
                                    CheckNumber = a.CheckNumber,
                                    SubpoenaCategory = a.SubpoenaCategory,
                                    Pieces = a.Pieces,
                                    ArriveOption = a.LatestScanArriveOption,
                                    ScanDate = a.LatestScanDate,
                                    ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                                    DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                                    CollectionMoney = a.CollectionMoney ?? 0
                                }).Distinct().ToList().Count() :
                                JunFuDbContext.TcDeliveryRequests
                                .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                       && specificDrivers.Contains(a.LatestDeliveryDriver) && a.LatestScanArriveOption == arriveOption && a.LatestScanArriveOption != null)
                                .Select(a => new DeliveryAndDeliveryMatingEntity
                                {
                                    CheckNumber = a.CheckNumber,
                                    SubpoenaCategory = a.SubpoenaCategory,
                                    Pieces = a.Pieces,
                                    ArriveOption = a.LatestScanArriveOption,
                                    ScanDate = a.LatestScanDate,
                                    ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                                    DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                                    CollectionMoney = a.CollectionMoney ?? 0
                                }).Distinct().ToList().Count();
            }
            else
            {
                     count = (arriveOption.Equals("all")) ? //是否為全部配達區分
                             JunFuDbContext.TcDeliveryRequests
                             .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                    && a.LatestDeliveryDriver == driverCode && a.LatestScanArriveOption != null)
                             .Select(a => new DeliveryAndDeliveryMatingEntity
                             {
                                 CheckNumber = a.CheckNumber,
                                 SubpoenaCategory = a.SubpoenaCategory,
                                 Pieces = a.Pieces,
                                 ArriveOption = a.LatestScanArriveOption,
                                 ScanDate = a.LatestScanDate,
                                 ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                                 DriverNameCode = a.LatestScanDriverCode,
                                 CollectionMoney = a.CollectionMoney ?? 0
                             }).Distinct().ToList().Count() :
                             JunFuDbContext.TcDeliveryRequests
                             .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                    && a.LatestDeliveryDriver == driverCode && a.LatestScanArriveOption == arriveOption && a.LatestScanArriveOption != null)
                             .Select(a => new DeliveryAndDeliveryMatingEntity
                             {
                                 CheckNumber = a.CheckNumber,
                                 SubpoenaCategory = a.SubpoenaCategory,
                                 Pieces = a.Pieces,
                                 ArriveOption = a.LatestScanArriveOption,
                                 ScanDate = a.LatestScanDate,
                                 ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                                 DriverNameCode = a.LatestScanDriverCode,
                                 CollectionMoney = a.CollectionMoney ?? 0
                             }).Distinct().ToList().Count();
            }
            return count;
        }

        public List<ArriveOptionEntity> GetArriveOptions()
        { 
            var data = (from c in JunFuDbContext.TbItemCodes where c.CodeId.CompareTo("9") <= 0 & c.CodeId != "10" & c.CodeId != "11" & c.CodeSclass == "AO" 
                       select new ArriveOptionEntity { CodeName = c.CodeName, CodeId = c.CodeId});
            return data.ToList();
        }

        public List<DriversEntity> GetAllDrivers()
        {
            var data = (from d in JunFuDbContext.TbDrivers where d.DriverCode.StartsWith("Z") && d.DriverCode.Length == 7 select new DriversEntity { DriverCode = d.DriverCode, DriverName = d.DriverCode+"-"+d.DriverName});
            return data.ToList();
        }

        public List<DriversEntity> GetDriversByStation(string stationscode)
        {
            var data = (from d in JunFuDbContext.TbDrivers where d.DriverCode.StartsWith("Z") && d.DriverCode.Length == 7 && d.Station == stationscode select new DriversEntity { DriverCode = d.DriverCode, DriverName = d.DriverCode + "-" + d.DriverName });
            return data.ToList();
        }

        public List<StationEntity> GetAllStations()
        {
            var data = (from s in JunFuDbContext.TbStations select new StationEntity { Name = s.StationScode + "-" + s.StationName, Scode = s.StationScode});
            return data.ToList();
        }

        public IEnumerable<TbItemCode> GetAllTbItemCodesWhichSclassAO() 
        {
            return JunFuDbContext.TbItemCodes.Where(x=>x.CodeSclass == "AO");
        }
    }
}
