﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryRate
{
    public interface IDeliveryRateRepository
    {
        int GetOrderNumBetweenDate(DateTime start, DateTime end, string stationScode, string customerCode);

        int GetMatingNumBeforeTime(DateTime start, DateTime end, byte timeInDay, string stationScode, string customerCode);

        int GetNotWriteOffNum(DateTime start, DateTime end, string stationScode, string customerCode);

        int GetScanNumber(DateTime start, DateTime end, string stationScode, string customerCode);

        int GetNotDeliveryNum(DateTime start, DateTime end, string stationScode, string customerCode);


        int GetOrderNumByDriver(DateTime start, DateTime end, string driverCode);

        int GetMatingNumByDriver(DateTime start, DateTime end, byte timeInDay, string driverCode);

        int GetNotWriteOffNumByDriver(DateTime start, DateTime end, string driverCode);

        int GetScanNumberByDriver(DateTime start, DateTime end, string driverCode);

        int GetNotDeliveryNumByDriver(DateTime start, DateTime end, string driverCode);
    }
}
