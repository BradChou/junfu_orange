﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.BL.BE.DeliveryException;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryException
{
    public interface IDeliveryExceptionRepository
    {
        int InsertDeliveryException(ExceptionReport exception);

        void UpdateDeliveryException(ExceptionReport exception);

        void DeleteDeliveryException(string checkNumber);

        List<ExceptionReport> GetDeliveryExceptionsProcessing(string station);
        
        List<ExceptionReport> GetDeliveryExceptionsPending(string station);

        List<JunFuDb.TbStation> GetTbStations();

        int InsertDeliveryExceptionLog(ExceptionReportLog log);

        List<TcDeliveryRequest> GetDeliveryExceptionsInfoByCN(string checkNumber);

        bool IsException(string checkNumber);

        string GetStationNameByCode(string code);

        List<WriteOffCheckList> GetAllWriteOffCheckList();

        List<WriteOffCheckList> GetAllErrorWriteOffCheckList();

        List<WriteOffCheckList> GetAllNormalWriteOffCheckList();

        WriteOffCheckList GetWriteOffCheckList(int id);

        Dictionary<int, WriteOffCheckList> GetWriteOffCheckListsByIds(List<int?> ids);

        List<ExceptionReportAddScanLogEntity> GetExceptions(DateTime start, DateTime end, string station);

        List<ExceptionReportAddScanLogEntity> GetExceptionsByAllStationScode(DateTime start, DateTime end);

        List<ExceptionReportAddScanLogEntity> GetExceptionsByArea(DateTime start, DateTime end, string area);

        List<ExceptionReportAddScanLogEntity> GetExceptionFromScanLogsByCk(string checknumber);
    }
}
