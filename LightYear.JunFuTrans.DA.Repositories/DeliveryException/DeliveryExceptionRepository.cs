﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using LightYear.JunFuTrans.DA.Repositories.Station;
using System.Linq.Expressions;
using LightYear.JunFuTrans.BL.BE.DeliveryException;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryException
{
    public class DeliveryExceptionRepository : IDeliveryExceptionRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }
        public JunFuDbContext JunFuDbContext { get; private set; }

        public DeliveryExceptionRepository(JunFuTransDbContext dbContext, JunFuDbContext context)
        {
            JunFuTransDbContext = dbContext;
            JunFuDbContext = context;
        }

        public int InsertDeliveryException(ExceptionReport exception)
        {
            this.JunFuTransDbContext.Add(exception);

            this.JunFuTransDbContext.SaveChanges();

            return exception.Id;
        }

        public void UpdateDeliveryException(ExceptionReport exception)
        {
            this.JunFuTransDbContext.Attach(exception);

            var entry = this.JunFuTransDbContext.Entry(exception);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            this.JunFuTransDbContext.SaveChanges();
        }

        public void DeleteDeliveryException(string checkNumber)
        {
            var data = from exceptionReport in this.JunFuTransDbContext.ExceptionReports where exceptionReport.CheckNumber.Equals(checkNumber) select exceptionReport.Id;

            var exc = this.JunFuTransDbContext.ExceptionReports.Find(data.First());

            if (exc != null)
            {
                this.JunFuTransDbContext.ExceptionReports.Remove(exc);

                this.JunFuTransDbContext.SaveChanges();
            }
        }

        public List<ExceptionReport> GetDeliveryExceptionsProcessing(string station)
        {
            var data = from exceptionReport in this.JunFuTransDbContext.ExceptionReports where exceptionReport.HandleStation.Equals(station) && exceptionReport.HandleResult != null && exceptionReport.HandleResult != "" orderby exceptionReport.ReportStation descending select exceptionReport;

            return data.ToList();
        }
        public List<ExceptionReport> GetDeliveryExceptionsPending(string station)
        {
            var data = from exceptionReport in this.JunFuTransDbContext.ExceptionReports where exceptionReport.ReportStation.Equals(station) && exceptionReport.HandleResult.Equals(null) || exceptionReport.HandleResult.Equals("") orderby exceptionReport.ReportStation descending select exceptionReport;

            return data.ToList();
        }

        public List<JunFuDb.TbStation> GetTbStations()
        {
            var data = from station in this.JunFuDbContext.TbStations where station.ActiveFlag.Equals(true) orderby station.Id select station;

            return data.ToList();
        }

        public int InsertDeliveryExceptionLog(ExceptionReportLog log)
        {
            this.JunFuTransDbContext.Add(log);

            this.JunFuTransDbContext.SaveChanges();

            return log.Id;
        }

        public bool IsException(string checkNumber)
        {
            var data = from exceptionReport in this.JunFuTransDbContext.ExceptionReports where exceptionReport.CheckNumber.Equals(checkNumber) orderby exceptionReport.Id select exceptionReport;

            if (data.FirstOrDefault() != null){
                return true;
            }  else {
                return false;
            }
        }

        public List<TcDeliveryRequest> GetDeliveryExceptionsInfoByCN(string checkNumber)
        {
            var data = from info in this.JunFuDbContext.TcDeliveryRequests where info.CheckNumber.Equals(checkNumber) orderby info.CheckNumber select info;

            return data.ToList();
        }

        public string GetStationNameByCode(string code)
        {
            var data = from info in this.JunFuDbContext.TbStations where info.StationCode.Equals(code) orderby info.Id select info;

            return data.FirstOrDefault().StationName;
        }

        public List<WriteOffCheckList> GetAllWriteOffCheckList()
        {
            var data = from writeOffCheckList in this.JunFuDbContext.WriteOffCheckLists orderby writeOffCheckList.Id select writeOffCheckList;

            return data.ToList();
        }

        public List<WriteOffCheckList> GetAllErrorWriteOffCheckList()
        {
            var data = from writeOffCheckList in this.JunFuDbContext.WriteOffCheckLists where writeOffCheckList.Type.Equals("異常") orderby writeOffCheckList.Id select writeOffCheckList;

            return data.ToList();
        }

        public List<WriteOffCheckList> GetAllNormalWriteOffCheckList()
        {
            var data = from writeOffCheckList in this.JunFuDbContext.WriteOffCheckLists where writeOffCheckList.Type.Equals("正常") orderby writeOffCheckList.Id select writeOffCheckList;

            return data.ToList();
        }

        public WriteOffCheckList GetWriteOffCheckList(int id)
        {
            var data = this.JunFuDbContext.WriteOffCheckLists.Find(id);

            return data;
        }

        public Dictionary<int, WriteOffCheckList> GetWriteOffCheckListsByIds(List<int?> ids)
        {
            var data = (from writeOffCheckList in this.JunFuDbContext.WriteOffCheckLists where ids.Contains(writeOffCheckList.Id) orderby writeOffCheckList.Id select writeOffCheckList).ToDictionary(p => p.Id);

            return data;
        }
        public List<ExceptionReportAddScanLogEntity> GetExceptions(DateTime start, DateTime end, string station)
        {
            var exceptionOption = JunFuDbContext.TbItemCodes.Where(a => a.CodeSclass == "EO" && a.CodeId != "0").Select(a=>a.CodeId).ToList();

            var scanLog = (from deliveryScanLog in JunFuDbContext.TtDeliveryScanLogs
                         join deliveryRequest in JunFuDbContext.TcDeliveryRequests on deliveryScanLog.CheckNumber equals deliveryRequest.CheckNumber
                         join ts in JunFuDbContext.TbStations on deliveryRequest.SendStationScode equals ts.StationScode
                         join tc in JunFuDbContext.TbStations on deliveryRequest.AreaArriveCode equals tc.StationScode
                         join tb in JunFuDbContext.TbDrivers on deliveryScanLog.DriverCode equals tb.DriverCode
                         join ta in JunFuDbContext.TbStations on tb.Station equals ta.StationScode
                         orderby deliveryScanLog.ScanDate ascending 
                           where exceptionOption.Contains(deliveryScanLog.ExceptionOption) // && !deliveryScanLog.ExceptionOption.Equals("")
                         //&& deliveryScanLog.ExceptionOption != null
                         && deliveryRequest.LessThanTruckload.Equals(true)
                         && deliveryRequest.LatestDestDate >= start && deliveryRequest.LatestDestDate < end
                         && tb.Station == station
                         select new ExceptionReportAddScanLogEntity
                         {
                             CheckNumber = deliveryScanLog.CheckNumber,
                             //HandlerCode = deliveryScanLog.DriverCode,
                             CDate = deliveryScanLog.ScanDate,
                             ExceptionStatus = deliveryScanLog.ExceptionOption,
                             SendStation = ts.StationScode + " " + ts.StationName,
                             DesStation = tc.StationScode + " " + tc.StationName,
                             ReportName = tb.DriverName,
                             ReportStation = ta.StationScode + " " + ta.StationName,
                             FilePath = ""
                         }).Distinct().ToList();

            //var exceptionReport = JunFuTransDbContext.ExceptionReports.Where(a => a.Cdate >= start && a.Cdate < end
            //                       && a.ReportStation.Equals(station))
            //                    .Select(a => new ExceptionReportAddScanLogEntity {
            //                        CheckNumber = a.CheckNumber
            //                       ,CDate = a.Cdate
            //                       ,ExceptionStatus =a.ReportMatter
            //                       ,ReportName = a.ReportName
            //                       ,ReportStation = a.ReportStation
            //                       ,OtherMatters = a.OtherMatters
            //                       ,SketchMatter = a.SketchMatter
            //                       ,SendStation = a.Sendstation
            //                       ,DesStation = a.Desstation
            //                       ,FilePath = a.PicTop ?? ""
            //                    }).ToList();
            var exceptionReport = (from ta in JunFuTransDbContext.ExceptionReports
                                   join tb in JunFuTransDbContext.StationAreas on ta.ReportStation equals tb.StationScode into temp3
                                   from tb in temp3.DefaultIfEmpty()
                                   join tc in JunFuTransDbContext.StationAreas on ta.Sendstation equals tc.StationName into temp2
                                   from tc in temp2.DefaultIfEmpty()
                                   join td in JunFuTransDbContext.StationAreas on ta.Desstation equals td.StationName into temp1
                                   from td in temp1.DefaultIfEmpty()
                                   where ta.Cdate >= start && ta.Cdate < end &&  ta.ReportStation.Equals(station)
                                   select new ExceptionReportAddScanLogEntity
                                   {
                                       CheckNumber = ta.CheckNumber,
                                       CDate = ta.Cdate,
                                       ReportStation = ta.ReportStation + " " + tb.StationName,
                                       ExceptionStatus = ta.ReportMatter,
                                       ReportName = ta.ReportName,
                                       OtherMatters = ta.OtherMatters,
                                       SendStation = tc.StationScode + " " + ta.Sendstation,
                                       DesStation = td.StationScode + " " + ta.Desstation,
                                       SketchMatter = ta.SketchMatter,
                                       FilePath = ta.PicTop ?? "",
                                   }).Distinct().ToList();

            var fullOuterJoin = scanLog.Union(exceptionReport);

            return fullOuterJoin.ToList();
        }

        public List<ExceptionReportAddScanLogEntity> GetExceptionsByAllStationScode(DateTime start, DateTime end)//全選
        {
            var exceptionOption = JunFuDbContext.TbItemCodes.Where(a => a.CodeSclass == "EO" && a.CodeId != "0").Select(a => a.CodeId).ToList();

            var scanLog = (from deliveryScanLog in JunFuDbContext.TtDeliveryScanLogs
                       join deliveryRequest in JunFuDbContext.TcDeliveryRequests on deliveryScanLog.CheckNumber equals deliveryRequest.CheckNumber
                       join ts in JunFuDbContext.TbStations on deliveryRequest.SendStationScode equals ts.StationScode
                       join tc in JunFuDbContext.TbStations on deliveryRequest.AreaArriveCode equals tc.StationScode                         
                       join tb in JunFuDbContext.TbDrivers on deliveryScanLog.DriverCode equals tb.DriverCode
                       join ta in JunFuDbContext.TbStations on tb.Station equals ta.StationScode
                       orderby deliveryScanLog.ScanDate ascending
                       where exceptionOption.Contains(deliveryScanLog.ExceptionOption) && deliveryRequest.LessThanTruckload.Equals(true)
                       && deliveryRequest.LatestDestDate >= start && deliveryRequest.LatestDestDate < end
                       select new ExceptionReportAddScanLogEntity
                       {
                           CheckNumber = deliveryScanLog.CheckNumber,
                           //HandlerCode = deliveryScanLog.DriverCode,
                           CDate = deliveryScanLog.ScanDate,
                           SendStation = ts.StationScode + " " + ts.StationName,
                           DesStation = tc.StationScode + " " + tc.StationName,
                           ExceptionStatus = deliveryScanLog.ExceptionOption,
                           ReportName = tb.DriverName,
                           ReportStation = ta.StationScode + " " + ta.StationName,
                           FilePath = ""
                       }).Distinct().ToList();

            //var exceptionReport = JunFuTransDbContext.ExceptionReports.Where(a => a.Cdate >= start && a.Cdate < end)
            //                     .Select(a => new ExceptionReportAddScanLogEntity {
            //                         CheckNumber = a.CheckNumber
            //                        ,CDate = a.Cdate
            //                        ,ExceptionStatus =a.ReportMatter                                 
            //                        ,ReportName = a.ReportName
            //                        ,ReportStation = a.ReportStation
            //                        ,OtherMatters = a.OtherMatters
            //                        ,SendStation = a.Sendstation
            //                        ,DesStation = a.Desstation
            //                        ,SketchMatter = a.SketchMatter
            //                        ,FilePath = a.PicTop ?? ""
            //                     }).ToList();
            var exceptionReport = (from ta in JunFuTransDbContext.ExceptionReports
                                   join tb in JunFuTransDbContext.StationAreas on ta.ReportStation equals tb.StationScode into temp3
                                   from tb in temp3.DefaultIfEmpty()
                                   join tc in JunFuTransDbContext.StationAreas on ta.Sendstation equals tc.StationName into temp2
                                   from tc in temp2.DefaultIfEmpty()
                                   join td in JunFuTransDbContext.StationAreas on ta.Desstation equals td.StationName into temp1
                                   from td in temp1.DefaultIfEmpty()
                                   where ta.Cdate >= start && ta.Cdate < end 
                                   select new ExceptionReportAddScanLogEntity
                                   {
                                       CheckNumber = ta.CheckNumber,
                                       CDate = ta.Cdate,
                                       ReportStation = ta.ReportStation + " " + tb.StationName,
                                       ExceptionStatus = ta.ReportMatter,
                                       ReportName = ta.ReportName,
                                       OtherMatters = ta.OtherMatters,
                                       SendStation = tc.StationScode + " " + ta.Sendstation,
                                       DesStation = td.StationScode + " " + ta.Desstation,
                                       FilePath = ta.PicTop ?? "",
                                   }).Distinct().ToList();

            var fullOuterJoin = scanLog.Union(exceptionReport);

            return fullOuterJoin.ToList();
        }

        public List<ExceptionReportAddScanLogEntity> GetExceptionsByArea(DateTime start, DateTime end, string area) //區屬
        {
            var exceptionOption = JunFuDbContext.TbItemCodes.Where(a => a.CodeSclass == "EO" && a.CodeId != "0").Select(a => a.CodeId).ToList();

            var areaoutput = (from stationarea in this.JunFuTransDbContext.StationAreas where stationarea.Area == area select stationarea.StationScode).ToList();

            var scanLog = (from deliveryScanLog in JunFuDbContext.TtDeliveryScanLogs
                        join deliveryRequest in JunFuDbContext.TcDeliveryRequests on deliveryScanLog.CheckNumber equals deliveryRequest.CheckNumber
                        join tb in JunFuDbContext.TbDrivers on deliveryScanLog.DriverCode equals tb.DriverCode
                        join tc in JunFuDbContext.TbStations on deliveryRequest.AreaArriveCode equals tc.StationScode
                        join ts in JunFuDbContext.TbStations on deliveryRequest.SendStationScode equals ts.StationScode
                        join ta in JunFuDbContext.TbStations on tb.Station equals ta.StationScode
                        orderby deliveryScanLog.ScanDate ascending
                        where exceptionOption.Contains(deliveryScanLog.ExceptionOption) && deliveryRequest.LessThanTruckload.Equals(true)
                        && deliveryRequest.LatestDestDate >= start && deliveryRequest.LatestDestDate < end
                        && areaoutput.Contains(tb.Station)
                        select new ExceptionReportAddScanLogEntity
                        {
                            CheckNumber = deliveryScanLog.CheckNumber,
                            //HandlerCode = deliveryScanLog.DriverCode,
                            CDate = deliveryScanLog.ScanDate,
                            ExceptionStatus = deliveryScanLog.ExceptionOption,
                            SendStation = ts.StationScode + " " + ts.StationName,
                            DesStation = tc.StationScode + " " + tc.StationName,
                            ReportName = tb.DriverName,
                            ReportStation = ta.StationScode + " " + ta.StationName,
                            FilePath = ""
                        }).Distinct().ToList();

            //var exceptionReport = JunFuTransDbContext.ExceptionReports.Where(a => a.Cdate >= start && a.Cdate < end && areaoutput.Contains(a.ReportStation))
            //                     .Select(a => new ExceptionReportAddScanLogEntity {
            //                         CheckNumber = a.CheckNumber
            //                        ,CDate = a.Cdate
            //                        ,ExceptionStatus =a.ReportMatter
            //                        ,ReportName = a.ReportName
            //                        ,ReportStation = a.ReportStation
            //                        ,OtherMatters = a.OtherMatters
            //                        ,SendStation = a.Sendstation
            //                        ,DesStation = a.Desstation
            //                        ,SketchMatter = a.SketchMatter
            //                        ,FilePath = a.PicTop ?? ""
            //                     }).ToList();
            var exceptionReport = (from ta in JunFuTransDbContext.ExceptionReports
                                   join tb in JunFuTransDbContext.StationAreas on ta.ReportStation equals tb.StationScode into temp3
                                   from tb in temp3.DefaultIfEmpty()
                                   join tc in JunFuTransDbContext.StationAreas on ta.Sendstation equals tc.StationName into temp2
                                   from tc in temp2.DefaultIfEmpty()
                                   join td in JunFuTransDbContext.StationAreas on ta.Desstation equals td.StationName into temp1
                                   from td in temp1.DefaultIfEmpty()
                                   where ta.Cdate >= start && ta.Cdate < end && areaoutput.Contains(ta.ReportStation)
                                   select new ExceptionReportAddScanLogEntity
                                   {
                                       CheckNumber = ta.CheckNumber,
                                       CDate = ta.Cdate,
                                       ReportStation = ta.ReportStation + " " + tb.StationName,
                                       ExceptionStatus = ta.ReportMatter,
                                       ReportName = ta.ReportName,
                                       OtherMatters = ta.OtherMatters,
                                       SendStation = tc.StationScode + " " + ta.Sendstation,
                                       DesStation = td.StationScode + " " + ta.Desstation,
                                       SketchMatter = ta.SketchMatter,
                                       FilePath = ta.PicTop ?? "",
                                   }).Distinct().ToList();

            var fullOuterJoin = scanLog.Union(exceptionReport);

            return fullOuterJoin.ToList();
        }

        public List<ExceptionReportAddScanLogEntity> GetExceptionFromScanLogsByCk(string checknumber)
        {
            var exceptionOption = JunFuDbContext.TbItemCodes.Where(a => a.CodeSclass == "EO" && a.CodeId != "0").Select(a => a.CodeId).ToList();

            var scanLog = (from deliveryScanLog in JunFuDbContext.TtDeliveryScanLogs
                        join deliveryRequest in JunFuDbContext.TcDeliveryRequests on deliveryScanLog.CheckNumber equals deliveryRequest.CheckNumber
                        join tb in JunFuDbContext.TbDrivers on deliveryScanLog.DriverCode equals tb.DriverCode
                        join tc in JunFuDbContext.TbStations on deliveryRequest.AreaArriveCode equals tc.StationScode
                        join ts in JunFuDbContext.TbStations on deliveryRequest.SendStationScode equals ts.StationScode
                        join ta in JunFuDbContext.TbStations on tb.Station equals ta.StationScode
                        orderby deliveryScanLog.ScanDate ascending
                        where exceptionOption.Contains(deliveryScanLog.ExceptionOption) && deliveryRequest.LessThanTruckload.Equals(true)
                        && deliveryScanLog.CheckNumber == checknumber
                        select new ExceptionReportAddScanLogEntity
                        {
                            CheckNumber = deliveryScanLog.CheckNumber,
                            //HandlerCode = deliveryScanLog.DriverCode,
                            CDate = deliveryScanLog.ScanDate,
                            ExceptionStatus = deliveryScanLog.ExceptionOption,
                            SendStation = ts.StationScode + " " + ts.StationName,
                            DesStation = tc.StationScode + " " + tc.StationName,
                            ReportName = tb.DriverName,
                            ReportStation = ta.StationScode + " " + ta.StationName,
                            FilePath = ""
                        }).Distinct().ToList();

            //var exceptionReport = JunFuTransDbContext.ExceptionReports.Where(a => a.CheckNumber.Equals(checknumber))
                                    
            //                     .Select(a => new ExceptionReportAddScanLogEntity {
            //                         CheckNumber = a.CheckNumber
            //                        ,CDate = a.Cdate
            //                        ,ExceptionStatus =a.ReportMatter
            //                        ,ReportName = a.ReportName
            //                        ,ReportStation = a.ReportStation
            //                        ,OtherMatters = a.OtherMatters
            //                        ,SendStation = a.Sendstation
            //                        ,DesStation = a.Desstation
            //                        ,SketchMatter = a.SketchMatter
            //                        ,FilePath = a.PicTop??""
            //                     }).ToList();

            var exceptionReport = ( from ta  in JunFuTransDbContext.ExceptionReports
                                    join tb in JunFuTransDbContext.StationAreas on ta.ReportStation equals tb.StationScode into temp3
                                    from tb in temp3.DefaultIfEmpty()
                                    join tc in JunFuTransDbContext.StationAreas on ta.Sendstation equals tc.StationName into temp2
                                    from tc in temp2.DefaultIfEmpty()
                                    join td in JunFuTransDbContext.StationAreas on ta.Desstation equals td.StationName into temp1
                                    from td in temp1.DefaultIfEmpty()
                                    where ta.CheckNumber.Equals(checknumber)
                                    select new ExceptionReportAddScanLogEntity
                                    {
                                      CheckNumber = ta.CheckNumber,
                                      CDate = ta.Cdate,
                                      ReportStation = ta.ReportStation + " " + tb.StationName,
                                      ExceptionStatus = ta.ReportMatter,
                                      ReportName = ta.ReportName,
                                      OtherMatters = ta.OtherMatters,
                                      SendStation =  tc.StationScode + " " +ta.Sendstation,
                                      DesStation =  td.StationScode + " " + ta.Desstation,
                                      SketchMatter = ta.SketchMatter,
                                      FilePath = ta.PicTop ?? "",
                                    }).Distinct().ToList();

            var fullOuterJoin = scanLog.Union(exceptionReport);

            return fullOuterJoin.ToList();
        }
    }
}
