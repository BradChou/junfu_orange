﻿using LightYear.JunFuTrans.BL.BE.DriverCollectionMoneyWriteOff;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System.Transactions;

namespace LightYear.JunFuTrans.DA.Repositories.DriverCollectionMoneyWriteOff
{
    public class DriverCollectionMoneyWriteOffRepository: IDriverCollectionMoneyWriteOffRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public DriverCollectionMoneyWriteOffRepository(JunFuDbContext junFuDbContext, JunFuTransDbContext junFuTransDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
            this.JunFuTransDbContext = junFuTransDbContext;
        }

        public List<DriverCollectionMoneyWriteOffEntity> GetAllByDateAndStation(string scode , DateTime start, DateTime end)
        {
            // with nolock in linq 
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required
            , new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
            {
                var data = (from dr in JunFuDbContext.TcDeliveryRequests
                            join ta in JunFuDbContext.TbCustomers on dr.CustomerCode equals ta.CustomerCode into temp3
                            from ta in temp3.DefaultIfEmpty()
                            where dr.CollectionMoney != 0 & dr.CollectionMoney != null & dr.LessThanTruckload.Equals(true)
                            & dr.LatestArriveOptionDate < end & dr.LatestArriveOptionDate >= start & dr.CheckNumber.Length > 0
                            & dr.DeliveryType != "R" & dr.AreaArriveCode == scode
                            & dr.LatestArriveOption == "3" 
                            select new DriverCollectionMoneyWriteOffEntity
                            {
                                ArriveStationName = dr.AreaArriveCode,
                                CheckNumber = dr.CheckNumber,
                                CustomerCode = dr.CustomerCode,
                                CustomerName = ta.CustomerName,
                                ReceiveContact = dr.ReceiveContact,
                                CollectionMoney = dr.CollectionMoney,
                                DriverCode = dr.LatestArriveOptionDriver,
                                ScanDate = dr.LatestArriveOptionDate
                            }).Distinct().ToList();
                return data;
            }
        }

        public List<DriverCollectionMoneyWriteOffEntity> GetAllByDate(DateTime start, DateTime end)
        {
            // with nolock in linq 
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required
             , new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
            {
                var data = (from dr in JunFuDbContext.TcDeliveryRequests
                            join ta in JunFuDbContext.TbCustomers on dr.CustomerCode equals ta.CustomerCode into temp3
                            from ta in temp3.DefaultIfEmpty()
                            where dr.CollectionMoney != 0 & dr.CollectionMoney != null & dr.LessThanTruckload.Equals(true)
                            & dr.LatestArriveOptionDate < end & dr.LatestArriveOptionDate >= start & dr.CheckNumber.Length > 0
                            & dr.DeliveryType != "R" & dr.LatestArriveOption == "3"
                            select new DriverCollectionMoneyWriteOffEntity
                            {
                                ArriveStationName = dr.AreaArriveCode,
                                CheckNumber = dr.CheckNumber,
                                CustomerCode = dr.CustomerCode,
                                CustomerName = ta.CustomerName,
                                ReceiveContact = dr.ReceiveContact,
                                CollectionMoney = dr.CollectionMoney,
                                DriverCode = dr.LatestArriveOptionDriver,
                                ScanDate = dr.LatestArriveOptionDate
                            }).Distinct().ToList();
                return data;
            }
        }
          
        public Dictionary<string,DriverPaymentDetail> GetIsDriverPaymentSheetOrNot(List<string> checknumbers)
        {
            var data = (from dp in JunFuTransDbContext.DriverPaymentDetails
                        where checknumbers.Contains(dp.CheckNumber)
                        select dp).ToDictionary(s => s.CheckNumber);
            return data;
        }

        public bool IsDriverPaymentSheetOrNot(string checknumber)
        {
            bool result = false;

            var data = (from dp in JunFuTransDbContext.DriverPaymentDetails
                        where dp.CheckNumber == checknumber
                        select dp).ToList();
            if (data.Count > 0)
            {
                result = true;
                return result;
            }
            return result;
        }

    }
}
