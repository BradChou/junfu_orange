﻿using LightYear.JunFuTrans.BL.BE.DriverCollectionMoneyWriteOff;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.DriverCollectionMoneyWriteOff
{
    public interface IDriverCollectionMoneyWriteOffRepository
    {
        List<DriverCollectionMoneyWriteOffEntity> GetAllByDateAndStation(string scode, DateTime start, DateTime end);

        List<DriverCollectionMoneyWriteOffEntity> GetAllByDate(DateTime start, DateTime end);

        Dictionary<string, DriverPaymentDetail> GetIsDriverPaymentSheetOrNot(List<string> checknumbers);

        bool IsDriverPaymentSheetOrNot(string checknumber);

    }
}
