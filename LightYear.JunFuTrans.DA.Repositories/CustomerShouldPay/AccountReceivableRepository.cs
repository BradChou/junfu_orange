﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.CustomerShouldPay
{
    public class AccountReceivableRepository : IAccountReceivableRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; set; }

        public AccountReceivableRepository(JunFuTransDbContext dbContext)
        {
            JunFuTransDbContext = dbContext;
        }

        public int Insert(AccountReceivable data)
        {
            DateTime now = DateTime.Now;
            data.CreateDate = now;
            data.UpdateDate = now;

            JunFuTransDbContext.AccountReceivables.Add(data);
            JunFuTransDbContext.SaveChanges();

            return data.Id;
        }

        public void BulkInsert(List<AccountReceivable> data)
        {
            DateTime now = DateTime.Now;
            foreach(var d in data)
            {
                d.CreateDate = now;
                d.UpdateDate = now;
            }

            JunFuTransDbContext.AccountReceivables.AddRange(data);
            JunFuTransDbContext.SaveChanges();

        }

        public void UpdateElectricReceipts(List<AccountReceivable> data)
        {
            DateTime now = DateTime.Now;
            foreach (var e in data)
            {
                e.UpdateDate = now;
                var dataInDb = JunFuTransDbContext.AccountReceivables.FirstOrDefault(d => d.InvoiceId == e.InvoiceId);
                if (dataInDb == null)
                    continue;

                if (dataInDb.ElectronicReceipt == null || dataInDb.ElectronicReceipt.Length == 0)
                    dataInDb.ElectronicReceipt = e.ElectronicReceipt;
            }

            JunFuTransDbContext.SaveChanges();
        }

        public void UpdateInvoiceId(List<AccountReceivable> data)
        {
            DateTime now = DateTime.Now;
            foreach(var e in data)
            {
                e.UpdateDate = now;
                var dataInDb = JunFuTransDbContext.AccountReceivables.FirstOrDefault(d => d.Id == e.Id);
                if (dataInDb == null)
                    continue;

                if(dataInDb.InvoiceId == null || dataInDb.InvoiceId.Length == 0)
                    dataInDb.InvoiceId = e.InvoiceId;
            }

            JunFuTransDbContext.SaveChanges();
        }

        public int UpdatePaidInfo(AccountReceivable data)
        {
            data.UpdateDate = DateTime.Now;

            var dataInDb = JunFuTransDbContext.AccountReceivables.FirstOrDefault(a => a.Id == data.Id);
            dataInDb.IsPaid = data.IsPaid;
            dataInDb.PaidDate = data.PaidDate;
            dataInDb.UpdateDate = DateTime.Now;

            JunFuTransDbContext.SaveChanges();

            return data.Id;
        }

        public AccountReceivable GetByCustomerAndShipDate(string customerCode, DateTime shipDateStart)
        {
            return JunFuTransDbContext.AccountReceivables.Where(a => a.CustomerCode == customerCode && a.ShipDateStart == shipDateStart).FirstOrDefault();
        }

        public IEnumerable<AccountReceivable> GetByCustomersAndShipDateBatch(IEnumerable<string> customerCodes, DateTime shipDateStart)
        {
            return JunFuTransDbContext.AccountReceivables.Where(a => customerCodes.Contains(a.CustomerCode) && a.ShipDateStart == shipDateStart);
        }
    }
}
