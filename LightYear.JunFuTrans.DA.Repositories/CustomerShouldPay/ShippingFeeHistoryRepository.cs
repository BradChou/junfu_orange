﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.CustomerShouldPay
{
    public class ShippingFeeHistoryRepository : IShippingFeeHistoryRepository
    {
        private JunFuDbContext JunFuDbContext { get; }

        public ShippingFeeHistoryRepository(JunFuDbContext dbContext)
        {
            JunFuDbContext = dbContext;
        }

        public CustomerShippingFeeHistory GetByCustomerCode(string customerCode)
        {
            return JunFuDbContext.CustomerShippingFeeHistories.Where(c => c.CustomerCode == customerCode).LastOrDefault();
        }

        public void InsertShippingFeeHistory(CustomerShippingFeeHistory entity)
        {
            JunFuDbContext.CustomerShippingFeeHistories.Add(entity);
            JunFuDbContext.SaveChanges();
        }
    }
}
