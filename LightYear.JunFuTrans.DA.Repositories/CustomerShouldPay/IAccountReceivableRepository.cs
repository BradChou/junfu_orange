﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.DA.Repositories.CustomerShouldPay
{
    public interface IAccountReceivableRepository
    {
        int Insert(AccountReceivable data);

        int UpdatePaidInfo(AccountReceivable data);

        void BulkInsert(List<AccountReceivable> data);

        void UpdateInvoiceId(List<AccountReceivable> data);

        AccountReceivable GetByCustomerAndShipDate(string customerCode, DateTime shipDateStart);

        IEnumerable<AccountReceivable> GetByCustomersAndShipDateBatch(IEnumerable<string> customerCodes, DateTime shipDateStart);

        void UpdateElectricReceipts(List<AccountReceivable> data);
    }
}
