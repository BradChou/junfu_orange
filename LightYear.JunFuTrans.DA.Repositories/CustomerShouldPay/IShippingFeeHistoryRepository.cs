﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.CustomerShouldPay
{
    public interface IShippingFeeHistoryRepository
    {
        CustomerShippingFeeHistory GetByCustomerCode(string customerCode);

        void InsertShippingFeeHistory(CustomerShippingFeeHistory entity);
    }
}
