﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.CustomerShouldPay
{
    public class CustomerShippingFeeRepository : ICustomerShippingFeeRepository
    {
        public JunFuDbContext JunFuDbContext { get; set; }

        public CustomerShippingFeeRepository(JunFuDbContext junFuDbContext)
        {
            JunFuDbContext = junFuDbContext;
        }

        public CustomerShippingFee GetCustomerShippingFeeByCustomerCode(string customerCode)
        {
            return JunFuDbContext.CustomerShippingFees.FirstOrDefault(c => c.CustomerCode == customerCode);
        }

        public IEnumerable<CustomerShippingFee> GetCustomerShippingFeeByCustomerCodes(IEnumerable<string> customerCode)
        {
            return JunFuDbContext.CustomerShippingFees.Where(c => customerCode.Contains(c.CustomerCode));
        }

        public IEnumerable<CustomerShippingFee> GetAll()
        {
            return JunFuDbContext.CustomerShippingFees.ToList();
        }

        public IEnumerable<CustomerShippingFee> GetByStationCode(string stationCode)
        {
            return JunFuDbContext.CustomerShippingFees.Where(c => c.CustomerCode.StartsWith(stationCode.ToUpper())).ToList();
        }

        public void UpdateShippingFee(CustomerShippingFee entity)
        {
            var dataInDb = JunFuDbContext.CustomerShippingFees.Where(c => c.CustomerCode == entity.CustomerCode)
                .OrderByDescending(c => c.Id).Take(1).FirstOrDefault();

            dataInDb.No1Bag = entity.No1Bag;
            dataInDb.No2Bag = entity.No2Bag;
            dataInDb.No3Bag = entity.No3Bag;
            dataInDb.No4Bag = entity.No4Bag;
            dataInDb.S60Cm = entity.S60Cm;
            dataInDb.S90Cm = entity.S90Cm;
            dataInDb.S110Cm = entity.S110Cm;
            dataInDb.S120Cm = entity.S120Cm;
            dataInDb.S150Cm = entity.S150Cm;
            dataInDb.S180 = entity.S180;
            dataInDb.Holiday = entity.Holiday;
            dataInDb.PieceRate = entity.PieceRate;
            dataInDb.LianYun = entity.LianYun;
            dataInDb.EffectiveDate = entity.EffectiveDate;

            JunFuDbContext.SaveChanges();
        }

        public List<TbCalendar> GetAllTbCalendars()
        {
            return JunFuDbContext.TbCalendars.ToList();
        }

        public CustomerShippingFee GetCustomerShippingFeeHistoryByCustomerCode(string customerCode)
        {
            return JunFuDbContext.CustomerShippingFeeHistories.OrderByDescending(a => a.EffectiveDate)
                   .Where(a => a.CustomerCode.Equals(customerCode))
                   .Select(a => new CustomerShippingFee { Id = a.Id, CustomerCode = a.CustomerCode, StationName = a.StationName, CustomerName = a.CustomerName, No1Bag = a.No1Bag, No2Bag = a.No2Bag, No3Bag = a.No3Bag, No4Bag = a.No4Bag, S60Cm = a.S60Cm, S90Cm = a.S90Cm, S110Cm = a.S110Cm, S120Cm = a.S120Cm, S150Cm = a.S150Cm, S180 = a.S180, Holiday = a.Holiday, PieceRate = a.PieceRate, LianYun = a.LianYun }).FirstOrDefault();
                   
        }
    }
}
