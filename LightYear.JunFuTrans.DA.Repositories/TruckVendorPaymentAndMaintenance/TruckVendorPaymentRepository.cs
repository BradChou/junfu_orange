﻿using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance
{
    public class TruckVendorPaymentRepository : ITruckVendorPaymentRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }
        public JunFuDbContext JunFuDbContext { get; set; }

        public TruckVendorPaymentRepository(JunFuTransDbContext junFuTransDbContext, JunFuDbContext junFuDbContext)
        {
            JunFuTransDbContext = junFuTransDbContext;
            JunFuDbContext = junFuDbContext;
        }
        public List<TruckVendorPaymentLog> GetAll()
        {
            return JunFuTransDbContext.TruckVendorPaymentLogs.Where(p => true).ToList();
        }
        public List<TruckVendorPaymentLog> GetPublicAll()
        {
            return this.GetAll().ToList();
        }
        public int Insert(TruckVendorPaymentLog truckVendorPaymentLog)
        {
            this.JunFuTransDbContext.TruckVendorPaymentLogs.Add(truckVendorPaymentLog);

            this.JunFuTransDbContext.SaveChanges();

            return truckVendorPaymentLog.Id;
        }

        public List<int> InsertBatch(List<TruckVendorPaymentLog> truckVendorPaymentLogs)
        {
            this.JunFuTransDbContext.TruckVendorPaymentLogs.AddRange(truckVendorPaymentLogs);

            this.JunFuTransDbContext.SaveChanges();

            return truckVendorPaymentLogs.Select(a => a.Id).ToList();
        }

        public void Update(TruckVendorPaymentLog truckVendorPaymentLog)
        {

            this.JunFuTransDbContext.TruckVendorPaymentLogs.Attach(truckVendorPaymentLog);

            var entry = this.JunFuTransDbContext.Entry(truckVendorPaymentLog);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            entry.Property(x => x.RegisteredDate).IsModified = false;
            entry.Property(x => x.CreateDate).IsModified = false;

            this.JunFuTransDbContext.SaveChanges();
        }
        public void UpdateBatch(List<TruckVendorPaymentLog> truckVendorPaymentLogs)
        {
            JunFuTransDbContext.UpdateRange(truckVendorPaymentLogs);
            JunFuTransDbContext.SaveChanges();
        }

        public void UpdateVendorPayment(TruckVendorPaymentLog truckVendorPaymentLog)
        {

            this.JunFuTransDbContext.TruckVendorPaymentLogs.Attach(truckVendorPaymentLog);

            var entry = this.JunFuTransDbContext.Entry(truckVendorPaymentLog);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            entry.Property(x => x.CreateDate).IsModified = false;
            entry.Property(x => x.CreateUser).IsModified = false;
            entry.Property(x => x.ImportRandomCode).IsModified = false;

            this.JunFuTransDbContext.SaveChanges();
        }

        public void Delete(TruckVendorPaymentLog truckVendorPaymentLog)
        {
            this.JunFuTransDbContext.TruckVendorPaymentLogs.Attach(truckVendorPaymentLog);
            this.JunFuTransDbContext.TruckVendorPaymentLogs.Remove(truckVendorPaymentLog);

            this.JunFuTransDbContext.SaveChanges();
        }

        public List<TruckVendorPaymentLog> GetAllByMonthAndTruckVendor(DateTime start, DateTime end, string vendor)
        {
            var data = this.GetAll().Where(a => a.RegisteredDate >= start && a.RegisteredDate < end && a.VendorName == vendor && a.IsActive == true && a.IsChecked == true).ToList();
            return data;
        }
        public List<TruckVendorPaymentLog> GetAllByMonthAndTruckVendorCostData(DateTime start, DateTime end, string vendor)
        {
            var data = this.GetAll().Where(a => a.RegisteredDate >= start && a.RegisteredDate < end && a.Payable == vendor && a.IsActive == true && a.IsChecked == true).ToList();
            return data;
        }
        public List<TruckVendorPaymentLog> GetAllByMonthAndTruckVendor_Old(DateTime start, DateTime end, string vendor)
        {
            var data = this.GetAll().Where(a => a.RegisteredDate >= start && a.RegisteredDate < end && a.VendorName == vendor).ToList();
            return data;
        }

        public List<TruckVendor> GetTruckVendors()
        {
            return JunFuTransDbContext.TruckVendors.Where(a => a.IsActive.Equals(true)).ToList();
        }

        public TruckVendor GetTruckVendorByTruckVendorAccountCode(string accountCode)
        {
            var data = JunFuTransDbContext.TruckVendors.Where(a => a.IsActive.Equals(true) && a.AccountCode.Equals(accountCode));

            if (data.Count() > 0)
            {
                return data.FirstOrDefault();
            }
            else
            {
                return null;
            }
        }

        public string GetVenderNameById(int id)
        {
            return JunFuTransDbContext.TruckVendors.Where(a => a.IsActive.Equals(true) && a.Id == id).Select(a => a.VendorName).FirstOrDefault();
        }

        public List<DropDownListEntity> GetFeeTypes()
        {
            return JunFuDbContext.TbItemCodes.Where(a => a.CodeBclass == "6" && a.CodeSclass == "fee_type" && a.ActiveFlag.Equals(true)).OrderBy(a => Convert.ToInt32(a.CodeId))
                  .Select(a => new DropDownListEntity { Id = a.CodeId, Name = a.CodeName }).ToList();
        }
        public List<DropDownListEntity> GetCompanies()
        {
            return JunFuDbContext.TbItemCodes.Where(a => a.CodeBclass == "6" && a.CodeSclass == "CD" && a.ActiveFlag.Equals(true)).OrderBy(a => Convert.ToInt32(a.CodeId))
                  .Select(a => new DropDownListEntity { Id = a.CodeId, Name = a.CodeName }).ToList();
        }
        public List<DropDownListEntity> GetDepartments()
        {
            return JunFuTransDbContext.TruckDepartment.Where(a => a.IsActive.Equals(true)).OrderBy(a => a.DepartmentCode)
                  .Select(a => new DropDownListEntity { Id = a.DepartmentCode, Name = a.DepartmentCode + "-" + a.DepartmentName }).ToList();
        }

        public List<DisbursementEntity> GetDisbursementByMonthAndTruckVendor(DateTime start, DateTime end, string vendor)
        {
            var data = (from log in JunFuTransDbContext.TruckVendorPaymentLogs
                        orderby log.FeeType
                        where log.RegisteredDate >= start && log.RegisteredDate < end && log.VendorName == vendor && log.IsActive == true
                        && log.IsChecked == true
                        select new DisbursementEntity
                        {
                            FeeType = log.FeeType,
                            Payment = log.Payment ?? 0,
                            Cost = 0,
                            //VendorName = g.Key.VendorName,
                            //Date = new DateTime (g.Key.Year,g.Key.Month,02)
                        }).ToList();
            var dataCost = (from log in JunFuTransDbContext.TruckVendorPaymentLogs
                        orderby log.FeeType
                        where log.RegisteredDate >= start && log.RegisteredDate < end && log.Payable == vendor && log.IsActive == true
                        && log.IsChecked == true
                        select new DisbursementEntity
                        {
                            FeeType = log.FeeType,
                            Payment = 0,
                            Cost = log.Cost,
                            //VendorName = g.Key.VendorName,
                            //Date = new DateTime (g.Key.Year,g.Key.Month,02)
                        }).ToList();
            data.AddRange(dataCost);
            var feetype = JunFuDbContext.TbItemCodes.Where(a => a.CodeBclass == "6" && a.CodeSclass == "fee_type" && a.ActiveFlag.Equals(true)).OrderBy(a => Convert.ToInt32(a.CodeId))
                  .Select(a => new DisbursementEntity { FeeType = a.CodeId, Payment = 0 }).ToList();

            data.AddRange(feetype);
            var output = data.GroupBy(a => a.FeeType).Select(a => new DisbursementEntity { FeeType = a.Key, Payment = a.Sum(x => x.Payment), Cost = a.Sum(x => x.Cost) }).OrderBy(a => Convert.ToInt32(a.FeeType)).ToList();

            return output;
        }
        public List<DisbursementEntity> GetDisbursementByMonthAndTruckVendor_Old(DateTime start, DateTime end, string vendor)
        {
            var data = (from log in JunFuTransDbContext.TruckVendorPaymentLogs
                        orderby log.FeeType
                        where log.RegisteredDate >= start && log.RegisteredDate < end && log.VendorName == vendor
                        select new DisbursementEntity
                        {
                            FeeType = log.FeeType,
                            Payment = log.Payment ?? 0,
                            Cost = 0,
                            //VendorName = g.Key.VendorName,
                            //Date = new DateTime (g.Key.Year,g.Key.Month,02)
                        }).ToList();

            var feetype = JunFuDbContext.TbItemCodes.Where(a => a.CodeBclass == "6" && a.CodeSclass == "fee_type" && a.ActiveFlag.Equals(true)).OrderBy(a => Convert.ToInt32(a.CodeId))
                  .Select(a => new DisbursementEntity { FeeType = a.CodeId, Payment = 0 }).ToList();

            data.AddRange(feetype);
            var output = data.GroupBy(a => a.FeeType).Select(a => new DisbursementEntity { FeeType = a.Key, Payment = a.Sum(x => x.Payment), Cost = a.Sum(x => x.Cost) }).OrderBy(a => Convert.ToInt32(a.FeeType)).ToList();

            return output;
        }
        public List<TbItemCode> GetFeeTypeTbItemCodes()
        {
            return JunFuDbContext.TbItemCodes.Where(a => a.CodeBclass == "6" && a.CodeSclass == "fee_type" && a.ActiveFlag.Equals(true)).OrderBy(a => Convert.ToInt32(a.CodeId))
                  .ToList();
        }

        public PaymentEntity GetIncomeAndExpenditure(DateTime start, DateTime end, string vendor)
        {
            //var total = GetDisbursementByMonthAndTruckVendor(start, end, vendor).Sum(a => a.Payment);

            //var incomeCodeId = JunFuDbContext.TbItemCodes.Where(a => a.CodeBclass == "6" && a.CodeSclass == "fee_type" && a.ActiveFlag.Equals(true) && a.Memo.Contains("收入")).OrderBy(a => Convert.ToInt32(a.CodeId))
            //      .Select(a => a.CodeId).ToList();

            //var income = GetDisbursementByMonthAndTruckVendor(start, end, vendor).Where(a => incomeCodeId.Contains(a.FeeType)).Sum(a => a.Payment);

            //int expenditure = total - income;

            var income = GetDisbursementByMonthAndTruckVendor(start, end, vendor).Sum(a => a.Cost);
            var expenditure = GetDisbursementByMonthAndTruckVendor(start, end, vendor).Sum(a => a.Payment);

            PaymentEntity paymentEntity = new PaymentEntity()
            {
                Income = income.ToString(),
                Expenditure = expenditure.ToString(),
                Remainder = (income - expenditure).ToString()
            };
            return paymentEntity;
        }
        public PaymentEntity GetIncomeAndExpenditure_Old(DateTime start, DateTime end, string vendor)
        {
            var total = GetDisbursementByMonthAndTruckVendor_Old(start, end, vendor).Sum(a => a.Payment);

            var incomeCodeId = JunFuDbContext.TbItemCodes.Where(a => a.CodeBclass == "6" && a.CodeSclass == "fee_type" && a.ActiveFlag.Equals(true) && a.Memo.Contains("收入")).OrderBy(a => Convert.ToInt32(a.CodeId))
                  .Select(a => a.CodeId).ToList();

            var income = GetDisbursementByMonthAndTruckVendor_Old(start, end, vendor).Where(a => incomeCodeId.Contains(a.FeeType)).Sum(a => a.Payment);

            int expenditure = total - income;

            //var income = GetDisbursementByMonthAndTruckVendor(start, end, vendor).Sum(a => a.Cost);
            //var expenditure = GetDisbursementByMonthAndTruckVendor(start, end, vendor).Sum(a => a.Payment);

            PaymentEntity paymentEntity = new PaymentEntity()
            {
                Income = income.ToString(),
                Expenditure = expenditure.ToString(),
                Remainder = (income - expenditure).ToString()
            };
            return paymentEntity;
        }
        public List<TruckVendorPaymentLog> GetLatestUploadData(string ImportRandomCode = "")
        {
            var dataRandomCodeNotNull = this.GetAll().Where(s => s.ImportRandomCode != null && !s.ImportRandomCode.Equals(""));
            return dataRandomCodeNotNull.Where(s => s.ImportRandomCode == ImportRandomCode).ToList();
        }

        public List<TruckVendorPaymentLog> GetAllByMonth(DateTime start, DateTime end)
        {
            var data = this.GetAll().Where(a => a.RegisteredDate >= start && a.RegisteredDate < end).ToList();
            return data;
        }

        public List<TruckVendorPaymentLog> GetPaymentByTimePeriodAndFeeType(DateTime start, DateTime end, string[] feeTypes)
        {
            var data = this.GetAll().Where(a => a.CreateDate >= start && a.CreateDate < end
                && feeTypes.Contains(a.FeeType)).ToList();
            return data;
        }

        public List<TruckVendorPaymentLog> GetPaymentByFeeType(string[] feeTypes)
        {
            var data = this.GetAll().Where(a => feeTypes.Contains(a.FeeType)).ToList();
            return data;
        }

        public List<TruckVendorPaymentLog> GetPaymentByTimePeriod(DateTime start, DateTime end)
        {
            var data = this.GetAll().Where(a => a.CreateDate >= start && a.CreateDate < end).ToList();
            return data;
        }

        public List<TruckVendorPaymentLog> GetPaymentBySearchModel(List<JFCreateUserDropDownListEntity> users, TruckVendorPaymentLogSearchEntity search, ERPSearchType userCondition, ERPSearchType checkAccountCondition, ERPSearchType feeTypeCondition, ERPSearchType vendorCondition, ERPSearchType payableCondition)
        {
            var result = this.GetPublicAll().Where(a => a.CreateDate >= search.startUpload && a.CreateDate <= search.endUpload).ToList();
            var userList = new List<string>();
            if (users.Count > 0)
            {
                foreach (var user in users)
                {
                    userList.Add(user.Id);
                }
            }

            switch (userCondition)
            {
                case ERPSearchType.UserDepend:
                    result = result.Where(p => p.CreateUser == search.createUser).ToList();
                    break;
                case ERPSearchType.DeptAllUser:
                    //result = result.Where(p => feeTypes.Contains(p.FeeType)).ToList();
                    result = result.Where(p => userList.Contains(p.CreateUser)).ToList();
                    break;
                case ERPSearchType.AllDept:
                    break;
                default:
                    break;
            }
            switch (checkAccountCondition)
            {
                case ERPSearchType.HasCheckAccounted:
                case ERPSearchType.HasNotCheckAccounted:
                    result = result.Where(p => p.IsChecked == search.hasCheckAccount).ToList();
                    break;
                case ERPSearchType.IgnoreCheckAccount:
                    break;
                default:
                    break;
            }
            switch (feeTypeCondition)
            {
                case ERPSearchType.FeeTypeDepend:
                    result = result.Where(p => p.FeeType == search.feeType).ToList();
                    break;
                case ERPSearchType.AllFeeType:
                    break;
                default:
                    break;
            }
            switch (vendorCondition)
            {
                case ERPSearchType.VendorDepend:
                    result = result.Where(p => p.VendorName == search.vendor).ToList();
                    break;
                case ERPSearchType.AllVendor:
                    break;
                default:
                    break;
            }
            switch (payableCondition)
            {
                case ERPSearchType.PayableDepend:
                    result = result.Where(p => p.Payable == search.payble).ToList();
                    break;
                case ERPSearchType.AllPayable:
                    break;
                default:
                    break;
            }
            if (!string.IsNullOrEmpty(search.noteSearch))
            {
                result = result.Where(p => p.Memo.ToLower().Contains(search.noteSearch.ToLower())).ToList();
            }
            return result;
        }

        public void DeleteBatch(List<TruckVendorPaymentLog> truckVendorPaymentLogs)
        {
            JunFuTransDbContext.UpdateRange(truckVendorPaymentLogs);
            JunFuTransDbContext.SaveChanges();
        }
        public TruckVendorPaymentLog GetById(int id)
        {
            var result = this.GetAll().FirstOrDefault(p => p.Id == id);
            return result;
        }
        public List<TruckVendorPaymentLog> GetByIds(int[] ids)
        {
            var result = new List<TruckVendorPaymentLog>();
            result = this.GetAll().Where(p => ids.Contains(p.Id)).ToList();
            return result;
        }
    }
}
