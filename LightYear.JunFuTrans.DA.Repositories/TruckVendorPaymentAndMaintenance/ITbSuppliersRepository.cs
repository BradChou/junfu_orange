﻿using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance
{
    public interface ITbSuppliersRepository
    {
        public List<TbSupplier> GetAll();
        public List<VW_tbSupplier> GetVWAll();
        public List<TbSupplier> GetPublicAll();
        public TbSupplier GetSupplierById(int id);
        public VW_tbSupplier GetVWSupplierById(int id);
        public List<JFDepartmentDropDownListEntity> GetSuppliers();
    }
}
