﻿using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance
{
    public class TruckVendorMaintenanceRepository : ITruckVendorMaintenanceRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }
        public JunFuDbContext JunFuDbContext { get; set; }

        public TruckVendorMaintenanceRepository(JunFuTransDbContext junFuTransDbContext, JunFuDbContext junFuDbContext)
        {
            JunFuTransDbContext = junFuTransDbContext;
            JunFuDbContext = junFuDbContext;
        }

        public List<TruckVendor> GetTruckVendorsForMaintenance()
        {
            return JunFuTransDbContext.TruckVendors.ToList();
        }

        public void Insert(TruckVendor truckVendor)
        {
            this.JunFuTransDbContext.TruckVendors.Add(truckVendor);

            this.JunFuTransDbContext.SaveChanges();
        }

        public void Update(TruckVendor truckVendor)
        {
            this.JunFuTransDbContext.TruckVendors.Attach(truckVendor);

            var entry = this.JunFuTransDbContext.Entry(truckVendor);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            this.JunFuTransDbContext.SaveChanges();
        }

        public List<TbItemCode> GetIncomeFeeTypeTbItemCodes()
        {
            return JunFuDbContext.TbItemCodes.Where(a => a.CodeBclass == "6" && a.CodeSclass == "fee_type" 
                   && a.Memo.Contains("收入")).OrderBy(a => Convert.ToInt32(a.CodeId))
                  .ToList();
        }

        public List<TbItemCode> GetFeeTypeTbItemCodes()
        {
            return JunFuDbContext.TbItemCodes.Where(a => a.CodeBclass == "6" && a.CodeSclass == "fee_type")
                  .ToList();
        }

        public List<TbItemCode> GetExpenditureFeeTypeTbItemCodes()
        {
            return JunFuDbContext.TbItemCodes.Where(a => a.CodeBclass == "6" && a.CodeSclass == "fee_type"
                   && a.Memo.Contains("支出")).OrderBy(a => Convert.ToInt32(a.CodeId))
                  .ToList();
        }

        public decimal InsertFeeType(TbItemCode tbItemCode)
        {
            this.JunFuDbContext.TbItemCodes.Add(tbItemCode);
            this.JunFuDbContext.SaveChanges();
            return tbItemCode.Seq;
        }

        public void UpdateFeeType(TbItemCode tbItemCode)
        {
            this.JunFuDbContext.TbItemCodes.Attach(tbItemCode);

            var entry = this.JunFuDbContext.Entry(tbItemCode);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            this.JunFuDbContext.SaveChanges();
        }

        public List<TbItemCode> GetCompanyTbItemCodes()
        {
            return JunFuDbContext.TbItemCodes.Where(a => a.CodeBclass == "6" && a.CodeSclass == "CD").OrderBy(a => Convert.ToInt32(a.CodeId))
                  .ToList();
        }

        public void InsertCompany(TbItemCode tbItemCode)
        {
            this.JunFuDbContext.TbItemCodes.Add(tbItemCode);
            this.JunFuDbContext.SaveChanges();
        }

        public void UpdateCompany(TbItemCode tbItemCode)
        {
            this.JunFuDbContext.TbItemCodes.Attach(tbItemCode);

            var entry = this.JunFuDbContext.Entry(tbItemCode);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            this.JunFuDbContext.SaveChanges();
        }

        public List<TbItemMappingCode> GetJFDeptFeeTypeMappingTbItemCodes()
        {
            return JunFuDbContext.TbItemMappingCodes.Where(a => a.MappingBclass == "1" && a.MappingSclass == "jf_dept_fee_type").OrderBy(a => Convert.ToInt32(a.MappingId)).ToList();
        }
        
        public List<TbItemCode> GetJFDepartment()
        {
            return JunFuDbContext.TbItemCodes.Where(a => a.CodeBclass == "6" && a.CodeSclass == "jf_dept").OrderBy(a => Convert.ToInt32(a.CodeId))
                  .ToList();
        }

        public string GetLastFeeCodeId()
        {
            return JunFuDbContext.TbItemCodes.Where(i => i.CodeBclass == "6" && i.CodeSclass == "fee_type" && i.CodeId != "99")
                    .AsEnumerable().OrderByDescending(i => int.Parse(i.CodeId)).First().CodeId;
        }

        public int GetMappingIdByFeeType(int feeCode)
        {
            var mappingId = JunFuDbContext.TbItemMappingCodes.Where(i => i.MappingBId == feeCode.ToString()).FirstOrDefault();

            return mappingId == null ? 0 : mappingId.MappingId;
        }

        public string GetDeptNameByCodeId(string codeId)
        {
            return JunFuDbContext.TbItemCodes.FirstOrDefault(i => i.CodeBclass == "6" && i.CodeSclass == "jf_dept" && i.CodeId == codeId)?.CodeName;
        }
    }
}
