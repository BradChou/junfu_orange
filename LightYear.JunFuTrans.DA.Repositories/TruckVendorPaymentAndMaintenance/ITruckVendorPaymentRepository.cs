﻿using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance
{
    public interface ITruckVendorPaymentRepository
    {
        public List<TruckVendorPaymentLog> GetPublicAll();
        List<TruckVendorPaymentLog> GetAllByMonthAndTruckVendor(DateTime start, DateTime end, string vendor);
        List<TruckVendorPaymentLog> GetAllByMonthAndTruckVendorCostData(DateTime start, DateTime end, string vendor);
        List<TruckVendorPaymentLog> GetAllByMonthAndTruckVendor_Old(DateTime start, DateTime end, string vendor);
        void Update(TruckVendorPaymentLog truckVendorPaymentLog);
        public void UpdateBatch(List<TruckVendorPaymentLog> truckVendorPaymentLogs);

        void UpdateVendorPayment(TruckVendorPaymentLog truckVendorPaymentLog);

        void Delete(TruckVendorPaymentLog truckVendorPaymentLog);
        public void DeleteBatch(List<TruckVendorPaymentLog> truckVendorPaymentLogs);

        int Insert(TruckVendorPaymentLog truckVendorPaymentLog);
        List<TruckVendor> GetTruckVendors();
        List<DropDownListEntity> GetFeeTypes();
        List<DropDownListEntity> GetCompanies();
        List<DropDownListEntity> GetDepartments();
        List<DisbursementEntity> GetDisbursementByMonthAndTruckVendor(DateTime start, DateTime end, string vendor);
        List<DisbursementEntity> GetDisbursementByMonthAndTruckVendor_Old(DateTime start, DateTime end, string vendor);
        List<TbItemCode> GetFeeTypeTbItemCodes();
        PaymentEntity GetIncomeAndExpenditure(DateTime start, DateTime end, string vendor);
        PaymentEntity GetIncomeAndExpenditure_Old(DateTime start, DateTime end, string vendor);
        List<TruckVendorPaymentLog> GetLatestUploadData(string ImportRandomCode = "");
        List<int> InsertBatch(List<TruckVendorPaymentLog> truckVendorPaymentLogs);
        TruckVendor GetTruckVendorByTruckVendorAccountCode(string accountCode);

        string GetVenderNameById(int id);

        List<TruckVendorPaymentLog> GetAllByMonth(DateTime start, DateTime end);

        List<TruckVendorPaymentLog> GetPaymentByTimePeriodAndFeeType(DateTime start, DateTime end, string[] feeTypes);

        List<TruckVendorPaymentLog> GetPaymentByFeeType(string[] feeTypes);

        List<TruckVendorPaymentLog> GetPaymentByTimePeriod(DateTime start, DateTime end);
        List<TruckVendorPaymentLog> GetPaymentBySearchModel(List<JFCreateUserDropDownListEntity> users, TruckVendorPaymentLogSearchEntity search, ERPSearchType userCondition, ERPSearchType checkAccountCondition, ERPSearchType feeTypeCondition, ERPSearchType vendorCondition, ERPSearchType ERPSearchType);
        TruckVendorPaymentLog GetById(int id);
        List<TruckVendorPaymentLog> GetByIds(int[] ids);
    }
}
