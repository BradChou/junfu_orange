﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance
{
    public interface IVW_TruckVendorPaymentLogDSERPＭergeDataRepository
    {
        List<VW_TruckVendorPaymentLogDSERPＭergeData> GetAll();
        List<VW_TruckVendorPaymentLogDSERPＭergeData> GetPublicAll();
        List<VW_TruckVendorPaymentLogDSERPＭergeData> GetByDownloadId(string downloadId);
    }
}
