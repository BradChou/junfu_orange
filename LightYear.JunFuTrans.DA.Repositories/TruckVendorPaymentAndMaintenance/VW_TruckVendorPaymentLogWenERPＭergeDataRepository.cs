﻿using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance
{
    public class VW_TruckVendorPaymentLogWenERPＭergeDataRepository : IVW_TruckVendorPaymentLogWenERPＭergeDataRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }
        public JunFuDbContext JunFuDbContext { get; set; }

        public VW_TruckVendorPaymentLogWenERPＭergeDataRepository(JunFuTransDbContext junFuTransDbContext, JunFuDbContext junFuDbContext)
        {
            JunFuTransDbContext = junFuTransDbContext;
            JunFuDbContext = junFuDbContext;
        }
        public List<VW_TruckVendorPaymentLogWenERPＭergeData> GetAll()
        {
            return JunFuTransDbContext.VW_TruckVendorPaymentLogWenERPＭergeData.Where(p => true).ToList();
        }
        public List<VW_TruckVendorPaymentLogWenERPＭergeData> GetPublicAll()
        {
            return this.GetAll();
        }

        public List<VW_TruckVendorPaymentLogWenERPＭergeData> GetMergeDataByIId(string[] aryIds)
        {
            var result = this.GetPublicAll();
            result = result.Where(p => aryIds.Contains(p.InvoicingNumber)).ToList();
            return result;
        }


    }
}
