﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance
{
    public interface ITbItemCodesRepository
    {
        List<string> GetFeeTypesCodeByDeptCodeId(string id);

        string GetDepartmentFromFeeType(string id);

        Dictionary<string, string> GetDutyDeptMapping();

        string GetFeeTypeNameById(string id);

        string GetDepartmentNameById(string id);

        string GetCompanyNameById(string id);

        string GetNameByCodeId(string id);
    }
}
