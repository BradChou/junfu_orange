﻿using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance
{
    public interface ITruckVendorMaintenanceRepository
    {
        List<TruckVendor> GetTruckVendorsForMaintenance();
        void Insert(TruckVendor truckVendor);
        void Update(TruckVendor truckVendor);

        List<TbItemCode> GetIncomeFeeTypeTbItemCodes();

        List<TbItemCode> GetFeeTypeTbItemCodes();

        List<TbItemCode> GetExpenditureFeeTypeTbItemCodes();
        decimal InsertFeeType(TbItemCode tbItemCode);
        void UpdateFeeType(TbItemCode tbItemCode);

        List<TbItemCode> GetCompanyTbItemCodes();
        void InsertCompany(TbItemCode tbItemCode);
        void UpdateCompany(TbItemCode tbItemCode);
        public List<TbItemMappingCode> GetJFDeptFeeTypeMappingTbItemCodes();

        public List<TbItemCode> GetJFDepartment();

        public string GetLastFeeCodeId();

        public int GetMappingIdByFeeType(int feeCode);

        public string GetDeptNameByCodeId(string codeId);
    }
}
