﻿using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance
{
    public class TruckDepartmentRepository : ITruckDepartmentRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }
        public JunFuDbContext JunFuDbContext { get; set; }
        public TruckDepartmentRepository(JunFuTransDbContext junFuTransDbContext, JunFuDbContext junFuDbContext)
        {
            JunFuTransDbContext = junFuTransDbContext;
            JunFuDbContext = junFuDbContext;
        }

        public List<TruckDepartment> GetAll()
        {
            return JunFuTransDbContext.TruckDepartment.Where(p => true).ToList();
        }
        public List<TruckDepartment> GetPublicAll()
        {
            return this.GetAll().Where(p => p.IsActive == true).ToList();
        }
    }
}
