﻿using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance
{
    public class DSERPMergeDataSnapShotRepository : IDSERPMergeDataSnapShotRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }
        public JunFuDbContext JunFuDbContext { get; set; }
        public DSERPMergeDataSnapShotRepository(JunFuTransDbContext junFuTransDbContext, JunFuDbContext junFuDbContext)
        {
            JunFuTransDbContext = junFuTransDbContext;
            JunFuDbContext = junFuDbContext;
        }
        public List<DSERPMergeDataSnapShot> GetAll()
        {
            return JunFuTransDbContext.DSERPMergeDataSnapShot.Where(p => true).ToList();
        }

        public List<DSERPMergeDataSnapShot> GetPublicAll()
        {
            return this.GetAll().Where(p => p.IsActive == true).ToList();
        }
        public int Insert(DSERPMergeDataSnapShot truckVendorPaymentLogDSERP)
        {
            this.JunFuTransDbContext.DSERPMergeDataSnapShot.Add(truckVendorPaymentLogDSERP);

            this.JunFuTransDbContext.SaveChanges();

            return truckVendorPaymentLogDSERP.Id;
        }

        public List<int> InsertBatch(List<DSERPMergeDataSnapShot> truckVendorPaymentLogDSERP)
        {
            this.JunFuTransDbContext.DSERPMergeDataSnapShot.AddRange(truckVendorPaymentLogDSERP);

            this.JunFuTransDbContext.SaveChanges();

            return truckVendorPaymentLogDSERP.Select(a => a.Id).ToList();
        }
    }
}
