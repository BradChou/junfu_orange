﻿using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;
namespace LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance
{
    public interface ITruckVendorPaymentLogDSERPRepository
    {
        public List<TruckVendorPaymentLogDSERP> GetAll();
        public List<TruckVendorPaymentLogDSERP> GetPublicAll();
        public int Insert(TruckVendorPaymentLogDSERP truckVendorPaymentLogDSERP);
        public List<int> InsertBatch(List<TruckVendorPaymentLogDSERP> truckVendorPaymentLogDSERP);
        public void Delete(TruckVendorPaymentLogDSERP truckVendorPaymentLogDSERP);
        public void DeleteBatch(List<TruckVendorPaymentLogDSERP> truckVendorPaymentLogDSERP);
        public List<TruckVendorPaymentLogDSERP> GetDSERPEntityBySearchModel(TruckVendorPaymentLogDSERPSearchEntity search, string[] feeTypes, ERPSearchType dateCondition, ERPSearchType userCondition, ERPSearchType downlodCondition);
        public List<TruckVendorPaymentLogDSERP> GetDSERPEntityBySearchModel(TruckVendorPaymentLogDSERPSearchEntity search, List<JFCreateUserDropDownListEntity> users, ERPSearchType dateCondition, ERPSearchType userCondition, ERPSearchType downlodCondition, ERPSearchType feeTypeCondition);
        public List<TruckVendorPaymentLogDSERP> GetByIds(int[] ids);
        public TruckVendorPaymentLogDSERP GetByTruckVendorLogId(int truckVendorLogId);
        public List<TruckVendorPaymentLogDSERP> GetByTruckVendorLogIds(int[] truckVendorLogIds);
        public TruckVendorPaymentLogDSERP GetById(int id);
        public void Update(TruckVendorPaymentLogDSERP truckVendorPaymentLogDSERP);
        public void UpdateBatch(List<TruckVendorPaymentLogDSERP> truckVendorPaymentDSERPLogs);

    }
}

