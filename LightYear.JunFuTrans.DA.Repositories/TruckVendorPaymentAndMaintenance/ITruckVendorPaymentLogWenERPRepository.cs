﻿using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;
namespace LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance
{
    public interface ITruckVendorPaymentLogWenERPRepository
    {
        public List<TruckVendorPaymentLogWenERP> GetAll();
        public List<TruckVendorPaymentLogWenERP> GetPublicAll();
        public int Insert(TruckVendorPaymentLogWenERP truckVendorPaymentLogWenERP);
        public List<int> InsertBatch(List<TruckVendorPaymentLogWenERP> truckVendorPaymentLogWenERP);
        public void DeleteBatch(List<TruckVendorPaymentLogWenERP> truckVendorPaymentLogWenERP);
        public List<TruckVendorPaymentLogWenERP> GetByTimePeriodAndFeeType(DateTime sDate, DateTime eDate, string[] feeTypes);
        public List<TruckVendorPaymentLogWenERP> GetByTimePeriod(DateTime sDate, DateTime eDate);
        public List<TruckVendorPaymentLogWenERP> GetByFeeType(string[] feeTypes);
        public List<TruckVendorPaymentLogWenERP> GetWenERPEntityBySearchModel(TruckVendorPaymentLogWenERPSearchEntity search, string[] feeTypes, ERPSearchType dateCondition, ERPSearchType userCondition, ERPSearchType downlodCondition);
        public TruckVendorPaymentLogWenERP GetById(int id);
        public List<TruckVendorPaymentLogWenERP> GetByIds(int[] ids);
        public List<TruckVendorPaymentLogWenERP> GetByTruckVendorLogIds(int[] truckVendorLogIds);
        public void Update(TruckVendorPaymentLogWenERP truckVendorPaymentLogWenERP);
        public TruckVendorPaymentLogWenERP GetByTruckVendorPaymentLogId(int truckVendorPaymentLogId);
        public int GetSerialNumber(DateTime registeredDate);
    }
}

