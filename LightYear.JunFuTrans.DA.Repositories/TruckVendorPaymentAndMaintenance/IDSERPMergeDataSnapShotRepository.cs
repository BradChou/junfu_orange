﻿using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;
namespace LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance
{
    public interface IDSERPMergeDataSnapShotRepository
    {
        public List<DSERPMergeDataSnapShot> GetAll();
        public List<DSERPMergeDataSnapShot> GetPublicAll();
        public int Insert(DSERPMergeDataSnapShot truckVendorPaymentLogDSERP);
        public List<int> InsertBatch(List<DSERPMergeDataSnapShot> truckVendorPaymentLogDSERP);

    }
}

