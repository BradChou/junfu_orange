﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance
{
    public interface IVW_TruckVendorPaymentLogWenERPＭergeDataRepository
    {
        List<VW_TruckVendorPaymentLogWenERPＭergeData> GetAll();
        List<VW_TruckVendorPaymentLogWenERPＭergeData> GetPublicAll();
        List<VW_TruckVendorPaymentLogWenERPＭergeData> GetMergeDataByIId(string[] aryIds);
    }
}
