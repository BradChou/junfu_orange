﻿using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance
{
    public class TbSuppliersRepository : ITbSuppliersRepository
    {
        public JunFuDbContext DbContext { get; set; }

        public TbSuppliersRepository(JunFuDbContext dbContext)
        {
            DbContext = dbContext;
        }
        public List<TbSupplier> GetAll()
        {
            return DbContext.TbSuppliers.Where(p => true).ToList();
        }
        public List<VW_tbSupplier> GetVWAll()
        {
            return DbContext.VW_tbSupplier.Where(p => true).ToList();
        }
        public List<TbSupplier> GetPublicAll()
        {
            var result = new List<TbSupplier>();
            result = this.GetAll().ToList();
            result = this.GetAll().Where(p => p.ActiveFlag.Equals(true)).ToList();
            return result;
        }
        public List<VW_tbSupplier> GetVWPublicAll()
        {
            var result = new List<VW_tbSupplier>();
            result = this.GetVWAll().ToList();
            return result;
        }
        public TbSupplier GetSupplierById(int id)
        {
            var result = new TbSupplier();
            result = this.GetPublicAll().FirstOrDefault(p => p.SupplierId == id);
            return result;
        }
        public VW_tbSupplier GetVWSupplierById(int id)
        {
            var result = new VW_tbSupplier();
            result = this.GetVWPublicAll().FirstOrDefault(p => p.supplier_id == id);
            return result;
        }
        public List<JFDepartmentDropDownListEntity> GetSuppliers()
        {
            return this.GetVWAll().Where(a => !string.IsNullOrEmpty(a.IDNO) && !string.IsNullOrEmpty(a.vendorName))
                .Select(s => new JFDepartmentDropDownListEntity(s.supplier_id.ToString(), s.vendorName)).ToList();
        }
    }
}
