﻿using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance
{
    public class TruckVendorPaymentLogWenERPRepository : ITruckVendorPaymentLogWenERPRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }
        public JunFuDbContext JunFuDbContext { get; set; }
        public TruckVendorPaymentLogWenERPRepository(JunFuTransDbContext junFuTransDbContext, JunFuDbContext junFuDbContext)
        {
            JunFuTransDbContext = junFuTransDbContext;
            JunFuDbContext = junFuDbContext;
        }
        public List<TruckVendorPaymentLogWenERP> GetAll()
        {
            return JunFuTransDbContext.TruckVendorPaymentLogWenERP.Where(p => true).ToList();
        }

        public List<TruckVendorPaymentLogWenERP> GetPublicAll()
        {
            return this.GetAll().Where(p => p.IsActive == true).ToList();
        }

        public int Insert(TruckVendorPaymentLogWenERP truckVendorPaymentLogWenERP)
        {
            this.JunFuTransDbContext.TruckVendorPaymentLogWenERP.Add(truckVendorPaymentLogWenERP);

            this.JunFuTransDbContext.SaveChanges();

            return truckVendorPaymentLogWenERP.Id;
        }

        public List<int> InsertBatch(List<TruckVendorPaymentLogWenERP> truckVendorPaymentLogWenERP)
        {
            this.JunFuTransDbContext.TruckVendorPaymentLogWenERP.AddRange(truckVendorPaymentLogWenERP);

            this.JunFuTransDbContext.SaveChanges();

            return truckVendorPaymentLogWenERP.Select(a => a.Id).ToList();
        }
        public void DeleteBatch(List<TruckVendorPaymentLogWenERP> truckVendorPaymentLogWenERP)
        {
            this.JunFuTransDbContext.TruckVendorPaymentLogWenERP.UpdateRange(truckVendorPaymentLogWenERP);

            this.JunFuTransDbContext.SaveChanges();

            return;
        }
        public List<TruckVendorPaymentLogWenERP> GetByFeeType(string[] feeTypes)
        {
            var data = JunFuTransDbContext.TruckVendorPaymentLogWenERP.Where(a => feeTypes.Contains(a.FeeType)).ToList();
            return data;
        }
        public List<TruckVendorPaymentLogWenERP> GetByTimePeriod(DateTime sDate, DateTime eDate)
        {
            var data = JunFuTransDbContext.TruckVendorPaymentLogWenERP.Where(a => a.CreateDate >= sDate && a.CreateDate < eDate).ToList();
            return data;
        }
        public List<TruckVendorPaymentLogWenERP> GetByTimePeriodAndFeeType(DateTime sDate, DateTime eDate, string[] feeTypes)
        {
            var data = JunFuTransDbContext.TruckVendorPaymentLogWenERP.Where(a => a.CreateDate >= sDate && a.CreateDate < eDate
                && feeTypes.Contains(a.FeeType)).ToList();
            return data;
        }
        public List<TruckVendorPaymentLogWenERP> GetWenERPEntityBySearchModel(TruckVendorPaymentLogWenERPSearchEntity search, string[] feeTypes, ERPSearchType dateCondition, ERPSearchType userCondition, ERPSearchType downlodCondition)
        {
            var result = this.GetPublicAll();
            //單別&日期
            switch (dateCondition)
            {
                case ERPSearchType.OnlyUploadDate:
                    result = result.Where(p => p.CreateDate >= search.startUpload && p.CreateDate < search.endUpload && p.OrderType == search.orderType).ToList();
                    break;
                case ERPSearchType.OnlyRegisterDate:
                    result = result.Where(p => p.RegisteredDate >= search.startRegister && p.RegisteredDate < search.endRegister && p.OrderType == search.orderType).ToList();
                    break;
                case ERPSearchType.BothDate:
                    result = result.Where(p => p.CreateDate >= search.startUpload && p.CreateDate < search.endUpload && p.RegisteredDate >= search.startRegister && p.RegisteredDate < search.endRegister && p.OrderType == search.orderType).ToList();
                    break;
                default:
                    break;
            }
            switch (userCondition)
            {
                case ERPSearchType.UserDepend:
                    result = result.Where(p => p.CreateUser == search.createUser && feeTypes.Contains(p.FeeType)).ToList();
                    break;
                case ERPSearchType.DeptAllUser:
                    result = result.Where(p => feeTypes.Contains(p.FeeType)).ToList();
                    break;
                case ERPSearchType.AllDept:
                    break;
                default:
                    break;
            }
            switch (downlodCondition)
            {
                case ERPSearchType.HasDownload:
                    result = result.Where(p => p.HasDownload == search.hasDownload).ToList();
                    break;
                case ERPSearchType.HasNotDownload:
                    result = result.Where(p => p.HasDownload == search.hasDownload).ToList();
                    break;
                case ERPSearchType.IgnoreDownload:
                    break;
                default:
                    break;
            }
            return result;
        }

        public TruckVendorPaymentLogWenERP GetById(int id)
        {
            var result = new TruckVendorPaymentLogWenERP();
            result = this.GetPublicAll().FirstOrDefault(p => p.Id == id);
            return result;
        }
        public List<TruckVendorPaymentLogWenERP> GetByIds(int[] ids)
        {
            var result = new List<TruckVendorPaymentLogWenERP>();
            result = this.GetPublicAll().Where(p => ids.Contains(p.Id)).ToList();
            return result;
        }

        public List<TruckVendorPaymentLogWenERP> GetByTruckVendorLogIds(int[] truckVendorLogIds)
        {
            var result = new List<TruckVendorPaymentLogWenERP>();
            result = this.GetPublicAll().Where(p => truckVendorLogIds.Contains(p.TruckVendorPaymentLogId)).ToList();
            return result;
        }


        public void Update(TruckVendorPaymentLogWenERP truckVendorPaymentLogWenERP)
        {

            this.JunFuTransDbContext.TruckVendorPaymentLogWenERP.Attach(truckVendorPaymentLogWenERP);

            var entry = this.JunFuTransDbContext.Entry(truckVendorPaymentLogWenERP);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            entry.Property(x => x.RegisteredDate).IsModified = false;
            entry.Property(x => x.CreateDate).IsModified = false;
            entry.Property(x => x.CreateUser).IsModified = false;

            this.JunFuTransDbContext.SaveChanges();
        }
        public TruckVendorPaymentLogWenERP GetByTruckVendorPaymentLogId(int truckVendorPaymentLogId)
        {
            var result = new TruckVendorPaymentLogWenERP();
            result = this.GetPublicAll().FirstOrDefault(p => p.TruckVendorPaymentLogId == truckVendorPaymentLogId);
            return result;
        }
        public int GetSerialNumber(DateTime registeredDate)
        {
            //var stringFormat = "00000";
            var data = this.GetPublicAll().Where(p => p.RegisteredDate.Year == registeredDate.Year && p.RegisteredDate.Month == registeredDate.Month);
            if (data.Count() == 0)
            {
                return 0;
            }
            var num = int.Parse(data.Max(p => p.SerialNumber));
            if (num > 99999)
            {
                return 0;
            }
            return num;
        }
    }
}
