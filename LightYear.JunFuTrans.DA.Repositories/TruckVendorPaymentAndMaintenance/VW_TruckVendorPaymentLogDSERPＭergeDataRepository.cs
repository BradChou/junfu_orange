﻿using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance
{
    public class VW_TruckVendorPaymentLogDSERPＭergeDataRepository : IVW_TruckVendorPaymentLogDSERPＭergeDataRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }
        public JunFuDbContext JunFuDbContext { get; set; }

        public VW_TruckVendorPaymentLogDSERPＭergeDataRepository(JunFuTransDbContext junFuTransDbContext, JunFuDbContext junFuDbContext)
        {
            JunFuTransDbContext = junFuTransDbContext;
            JunFuDbContext = junFuDbContext;
        }
        public List<VW_TruckVendorPaymentLogDSERPＭergeData> GetAll()
        {
            return JunFuTransDbContext.VW_TruckVendorPaymentLogDSERPＭergeData.Where(p => true).ToList();
        }
        public List<VW_TruckVendorPaymentLogDSERPＭergeData> GetPublicAll()
        {
            return this.GetAll();
        }
        public List<VW_TruckVendorPaymentLogDSERPＭergeData> GetByDownloadId(string downloadId)
        {
            return this.GetPublicAll().Where(p=>p.DownloadRandomCode == downloadId).ToList();
        }
    }
}
