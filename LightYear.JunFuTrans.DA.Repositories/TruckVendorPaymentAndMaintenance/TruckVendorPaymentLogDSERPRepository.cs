﻿using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance
{
    public class TruckVendorPaymentLogDSERPRepository : ITruckVendorPaymentLogDSERPRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }
        public JunFuDbContext JunFuDbContext { get; set; }
        public TruckVendorPaymentLogDSERPRepository(JunFuTransDbContext junFuTransDbContext, JunFuDbContext junFuDbContext)
        {
            JunFuTransDbContext = junFuTransDbContext;
            JunFuDbContext = junFuDbContext;
        }
        public List<TruckVendorPaymentLogDSERP> GetAll()
        {
            return JunFuTransDbContext.TruckVendorPaymentLogDSERP.Where(p => true).ToList();
        }

        public List<TruckVendorPaymentLogDSERP> GetPublicAll()
        {
            return this.GetAll().Where(p => p.IsActive == true).ToList();
        }
        public int Insert(TruckVendorPaymentLogDSERP truckVendorPaymentLogDSERP)
        {
            this.JunFuTransDbContext.TruckVendorPaymentLogDSERP.Add(truckVendorPaymentLogDSERP);

            this.JunFuTransDbContext.SaveChanges();

            return truckVendorPaymentLogDSERP.Id;
        }

        public List<int> InsertBatch(List<TruckVendorPaymentLogDSERP> truckVendorPaymentLogDSERP)
        {
            this.JunFuTransDbContext.TruckVendorPaymentLogDSERP.AddRange(truckVendorPaymentLogDSERP);

            this.JunFuTransDbContext.SaveChanges();

            return truckVendorPaymentLogDSERP.Select(a => a.Id).ToList();
        }
        public void Delete(TruckVendorPaymentLogDSERP truckVendorPaymentLogDSERP)
        {
            this.JunFuTransDbContext.TruckVendorPaymentLogDSERP.Update(truckVendorPaymentLogDSERP);

            this.JunFuTransDbContext.SaveChanges();

            return;
        }
        public void DeleteBatch(List<TruckVendorPaymentLogDSERP> truckVendorPaymentLogDSERP)
        {
            this.JunFuTransDbContext.TruckVendorPaymentLogDSERP.UpdateRange(truckVendorPaymentLogDSERP);

            this.JunFuTransDbContext.SaveChanges();

            return;
        }

        public List<TruckVendorPaymentLogDSERP> GetDSERPEntityBySearchModel(TruckVendorPaymentLogDSERPSearchEntity search, string[] feeTypes, ERPSearchType dateCondition, ERPSearchType userCondition, ERPSearchType downlodCondition)
        {
            var result = this.GetPublicAll();
            //單別&日期
            switch (dateCondition)
            {
                case ERPSearchType.OnlyUploadDate:
                    result = result.Where(p => p.UploadDate >= search.startUpload && p.UploadDate < search.endUpload && p.VoucherCode == search.voucherCode && p.Company == search.company).ToList();
                    break;
                case ERPSearchType.OnlyRegisterDate:
                    result = result.Where(p => p.RegisteredDate >= search.startRegister && p.RegisteredDate < search.endRegister && p.VoucherCode == search.voucherCode && p.Company == search.company).ToList();
                    break;
                case ERPSearchType.BothDate:
                    result = result.Where(p => p.UploadDate >= search.startUpload && p.UploadDate < search.endUpload && p.RegisteredDate >= search.startRegister && p.RegisteredDate < search.endRegister && p.VoucherCode == search.voucherCode && p.Company == search.company).ToList();
                    break;
                default:
                    break;
            }

            switch (userCondition)
            {
                case ERPSearchType.UserDepend:
                    result = result.Where(p => p.CreateUser == search.createUser && feeTypes.Contains(p.FeeType)).ToList();
                    break;
                case ERPSearchType.DeptAllUser:
                    result = result.Where(p => feeTypes.Contains(p.FeeType)).ToList();
                    break;
                case ERPSearchType.AllDept:
                    break;
                default:
                    break;
            }
            switch (downlodCondition)
            {
                case ERPSearchType.HasDownload:
                    result = result.Where(p => p.HasDownload == search.hasDownload).ToList();
                    break;
                case ERPSearchType.HasNotDownload:
                    result = result.Where(p => p.HasDownload == search.hasDownload).ToList();
                    break;
                case ERPSearchType.IgnoreDownload:
                    break;
                default:
                    break;
            }
            return result;
        }

        public List<TruckVendorPaymentLogDSERP> GetDSERPEntityBySearchModel(TruckVendorPaymentLogDSERPSearchEntity search, List<JFCreateUserDropDownListEntity> users, ERPSearchType dateCondition, ERPSearchType userCondition, ERPSearchType downlodCondition, ERPSearchType feeTypeCondition)
        {
            var result = this.GetPublicAll();
            var userList = new List<string>();
            if (users.Count>0)
            {
                foreach (var user in users)
                {
                    userList.Add(user.Id);
                }
            }
            //單別&日期
            switch (dateCondition)
            {
                case ERPSearchType.OnlyUploadDate:
                    result = result.Where(p => p.UploadDate >= search.startUpload && p.UploadDate < search.endUpload && p.VoucherCode == search.voucherCode && p.Company == search.company).ToList();
                    break;
                case ERPSearchType.OnlyRegisterDate:
                    result = result.Where(p => p.RegisteredDate >= search.startRegister && p.RegisteredDate < search.endRegister && p.VoucherCode == search.voucherCode && p.Company == search.company).ToList();
                    break;
                case ERPSearchType.BothDate:
                    result = result.Where(p => p.UploadDate >= search.startUpload && p.UploadDate < search.endUpload && p.RegisteredDate >= search.startRegister && p.RegisteredDate < search.endRegister && p.VoucherCode == search.voucherCode && p.Company == search.company).ToList();
                    break;
                default:
                    break;
            }

            switch (userCondition)
            {
                case ERPSearchType.UserDepend:
                    result = result.Where(p => p.CreateUser == search.createUser).ToList();
                    break;
                case ERPSearchType.DeptAllUser:
                    //result = result.Where(p => feeTypes.Contains(p.FeeType)).ToList();
                    result = result.Where(p => userList.Contains(p.CreateUser)).ToList();
                    break;
                case ERPSearchType.AllDept:
                    break;
                default:
                    break;
            }
            switch (downlodCondition)
            {
                case ERPSearchType.HasDownload:
                    result = result.Where(p => p.HasDownload == search.hasDownload).ToList();
                    break;
                case ERPSearchType.HasNotDownload:
                    result = result.Where(p => p.HasDownload == search.hasDownload).ToList();
                    break;
                case ERPSearchType.IgnoreDownload:
                    break;
                default:
                    break;
            }
            switch (feeTypeCondition)
            {
                case ERPSearchType.FeeTypeDepend:
                    result = result.Where(p => p.FeeType == search.feeType).ToList();
                    break;
                case ERPSearchType.AllFeeType:
                    break;
                default:
                    break;
            }
            return result;
        }
        public TruckVendorPaymentLogDSERP GetById(int id)
        {
            var result = new TruckVendorPaymentLogDSERP();
            result = this.GetPublicAll().FirstOrDefault(p => p.Id == id);
            return result;
        }
        public List<TruckVendorPaymentLogDSERP> GetByIds(int[] ids)
        {
            var result = new List<TruckVendorPaymentLogDSERP>();
            result = this.GetPublicAll().Where(p => ids.Contains(p.Id)).ToList();
            return result;
        }
        public List<TruckVendorPaymentLogDSERP> GetByTruckVendorLogIds(int[] truckVendorLogIds)
        {
            var result = new List<TruckVendorPaymentLogDSERP>();
            result = this.GetPublicAll().Where(p => truckVendorLogIds.Contains(p.TruckVendorPaymentLogId)).ToList();
            return result;
        }
        public TruckVendorPaymentLogDSERP GetByTruckVendorLogId(int truckVendorLogId)
        {
            var result = new TruckVendorPaymentLogDSERP();
            result = this.GetPublicAll().FirstOrDefault(p => p.TruckVendorPaymentLogId == truckVendorLogId);
            return result;
        }
        public void Update(TruckVendorPaymentLogDSERP truckVendorPaymentLogDSERP)
        {

            this.JunFuTransDbContext.TruckVendorPaymentLogDSERP.Attach(truckVendorPaymentLogDSERP);

            var entry = this.JunFuTransDbContext.Entry(truckVendorPaymentLogDSERP);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            entry.Property(x => x.RegisteredDate).IsModified = false;
            entry.Property(x => x.CreateDate).IsModified = false;
            entry.Property(x => x.UploadDate).IsModified = false;
            entry.Property(x => x.CreateUser).IsModified = false;

            this.JunFuTransDbContext.SaveChanges();
        }
        public void UpdateBatch(List<TruckVendorPaymentLogDSERP> truckVendorPaymentDSERPLogs)
        {
            JunFuTransDbContext.UpdateRange(truckVendorPaymentDSERPLogs);
            JunFuTransDbContext.SaveChanges();
        }
    }
}
