﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance
{
    public class FeeTypeRelateRepository : IFeeTypeRelateRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public FeeTypeRelateRepository(JunFuTransDbContext junFuTransDbContext)
        {
            JunFuTransDbContext = junFuTransDbContext;
        }
        public List<FeeTypeRelate> GetAll()
        {
            return JunFuTransDbContext.FeeTypeRelates.Where(p => true).ToList();
        }
        public List<FeeTypeRelate> GetPublicAll()
        {
            return this.GetAll().Where(p => p.IsActive == true).ToList();
        }
        public List<FeeTypeRelate> GetHasInvoice()
        {
            return this.GetPublicAll().Where(p => p.HasInvoice == true).ToList();
        }
    }
}
