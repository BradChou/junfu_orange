﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance
{
    public interface ITruckCompanyRelateRepository
    {
        List<TruckCompanyRelate> GetAll();
        List<TruckCompanyRelate> GetPublicAll();
    }
}
