﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance
{
    public class TruckCompanyRelateRepository : ITruckCompanyRelateRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public TruckCompanyRelateRepository(JunFuTransDbContext junFuTransDbContext)
        {
            JunFuTransDbContext = junFuTransDbContext;
        }
        public List<TruckCompanyRelate> GetAll()
        {
            return JunFuTransDbContext.TruckCompanyRelates.Where(p => true).ToList();
        }
        public List<TruckCompanyRelate> GetPublicAll()
        {
            return this.GetAll().Where(p => p.IsActive == true).ToList();
        }
    }
}
