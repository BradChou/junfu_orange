﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System.Linq;
namespace LightYear.JunFuTrans.DA.Repositories.MenuBar
{
    public class MenuFunctionRepository : IMenuFunctionRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public MenuFunctionRepository(JunFuTransDbContext junFuTransDbContext)
        {
            this.JunFuTransDbContext = junFuTransDbContext;
        }
        public List<SystemFunction> GetAllSystemFucntionBySubCategoryId(int subCategoryId)
        {
            var data = from system_function in this.JunFuTransDbContext.SystemFunctions where system_function.SubCategoryId.Equals(subCategoryId) orderby system_function.Id ascending select system_function;

            List<SystemFunction> systemFunctions = data.ToList();

            return systemFunctions;

        }
    }
}
