﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.DA.Repositories.MenuBar
{
    public interface IMenuFunctionRepository
    {
        List<SystemFunction> GetAllSystemFucntionBySubCategoryId(int subCategoryId);
    }
}
