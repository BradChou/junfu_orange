﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.MenuBar
{
    public class MenuCategoryRepository : IMenuCategoryRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public MenuCategoryRepository(JunFuTransDbContext junFuTransDbContext)
        {
            this.JunFuTransDbContext = junFuTransDbContext;
        }

        public List<SystemCategory> GetAllMenuBarCategory()
        {
            var data = from system_category in this.JunFuTransDbContext.SystemCategories select system_category;

            List<SystemCategory> menuBarCategories = data.ToList();
            
            return menuBarCategories;
        }

        public List<SystemCategory> GetJFAllMenuBarCategory()
        {
            var data = from system_category in this.JunFuTransDbContext.SystemCategories where system_category.Company == "2" select system_category;

            List<SystemCategory> menuBarCategories = data.ToList();

            return menuBarCategories;
        }
    }
}
