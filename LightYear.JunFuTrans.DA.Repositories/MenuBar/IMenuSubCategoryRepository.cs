﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.MenuBar
{
    public interface IMenuSubCategoryRepository
    {
        List<SystemSubCategory> GetSubCategoryByCategoryId(int categoryId);
    }
}
