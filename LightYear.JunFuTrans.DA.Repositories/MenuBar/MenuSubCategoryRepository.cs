﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.DA.Repositories.MenuBar
{
    public class MenuSubCategoryRepository : IMenuSubCategoryRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public MenuSubCategoryRepository(JunFuTransDbContext junFuTransDbContext)
        {
            this.JunFuTransDbContext = junFuTransDbContext;
        }
        public List<SystemSubCategory> GetSubCategoryByCategoryId(int categoryId)
        {
            var data = from system_sub_category in this.JunFuTransDbContext.SystemSubCategories where system_sub_category.CategoryId.Equals(categoryId) orderby system_sub_category.Id ascending select system_sub_category;

            List<SystemSubCategory> subCategories = data.ToList();

            return subCategories;
        }
    }
}
