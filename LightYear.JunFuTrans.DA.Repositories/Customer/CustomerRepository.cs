﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.Customer
{
    public class CustomerRepository:ICustomerRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }

        public CustomerRepository(JunFuDbContext junFuDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
        }
        
        public TbCustomer GetByCustomerCode(string customerCode)
        {
            var data = from TbCustomer in this.JunFuDbContext.TbCustomers where TbCustomer.CustomerCode.Equals(customerCode) select TbCustomer;

            if (data.Count()>0)
            {
                return data.FirstOrDefault();
            }
            else
            {
                return null;
            }
        }

        public List<TbCustomer> GetAll()
        {
            return JunFuDbContext.TbCustomers.Distinct().ToList();
        }

        public List<TbCustomer> GetCustomersByStation(string stationCode)
        {
            return JunFuDbContext.TbCustomers.Where(c => c.CustomerCode.StartsWith(stationCode)).Distinct().ToList();
        }

        public string GetCustomerCodeByName(string customerName)
        {
            return JunFuDbContext.TbCustomers.Where(c => c.CustomerName.Equals(customerName)).Select(c => c.CustomerCode).FirstOrDefault();
        }

        public List<TbCustomer> GetByStationCodeAndStartWith(string stationCode, string startWith, int nums)
        {
            var data = from s in this.JunFuDbContext.TbCustomers where s.SupplierCode.Equals(stationCode) && ( s.CustomerCode.StartsWith(startWith) || s.CustomerName.StartsWith(startWith) )
                       && s.CustomerCode.StartsWith("F") && !s.CustomerCode.StartsWith("F0")
                       orderby s.CustomerCode select s;

            var output = data.Take(nums);

            return output.ToList();
        }

        public List<TbCustomer> GetStartWith(string startWith, int nums)
        {
            var data = from s in this.JunFuDbContext.TbCustomers where (s.CustomerCode.StartsWith(startWith) || s.CustomerName.StartsWith(startWith))
                       && s.CustomerCode.StartsWith("F") && !s.CustomerCode.StartsWith("F0")
                       orderby s.CustomerCode select s;

            var output = data.Take(nums);

            return output.ToList();
        }

        public Dictionary<string, TbCustomer> GetCustomersByCustomerCodes(List<string> customerCodes)
        {
            var data = (from customer in this.JunFuDbContext.TbCustomers where customer.StopShippingCode.Equals("0") && customerCodes.Contains(customer.CustomerCode) select customer).ToLookup(p => p.CustomerCode).ToDictionary(p => p.Key.ToUpper(), p => p.First());

            return data;
        }

        public Dictionary<string, string> GetCustomerNameByCustomerCode(IEnumerable<string> customerCodes)
        {
            return JunFuDbContext.TbCustomers.Where(c => customerCodes.Contains(c.CustomerCode)).ToDictionary(c => c.CustomerCode, c => c.CustomerName);
        }
    }
}
