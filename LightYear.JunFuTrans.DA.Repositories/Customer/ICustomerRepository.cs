﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.DA.Repositories.Customer
{
    public interface ICustomerRepository
    {
        TbCustomer GetByCustomerCode(string customerCode);

        List<TbCustomer> GetAll();

        List<TbCustomer> GetCustomersByStation(string stationCode);

        string GetCustomerCodeByName(string customerName);

        List<TbCustomer> GetByStationCodeAndStartWith(string stationCode, string startWith, int nums);

        List<TbCustomer> GetStartWith(string startWith, int nums);

        Dictionary<string, TbCustomer> GetCustomersByCustomerCodes(List<string> customerCodes);

        Dictionary<string, string> GetCustomerNameByCustomerCode(IEnumerable<string> customerCodes);
    }
}
