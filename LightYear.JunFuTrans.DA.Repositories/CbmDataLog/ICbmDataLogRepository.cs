﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.BL.BE.CbmDataLog;

namespace LightYear.JunFuTrans.DA.Repositories.CbmDataLog
{
    public interface ICbmDataLogRepository
    {
        bool InsertIntoCbmDataLogBulky(List<JunFuDb.CbmDataLog> data);
        bool LogSchedule(CbmDataLogRecord logData);
        bool UpdateSchdule(CbmDataLogRecord logData);
        CbmDataLogRecord FindUncompletedScheduleLogByFileName(string fileName, string dataSource);
        bool IsAlreadyCompletedScheduleLogByFileName(string fileName, string dataSource);
        bool CbmSpecialDataInsertIntoScanLog(TtDeliveryScanLog scanLog);
        IEnumerable<JunFuDb.CbmDataLog> GetAbnormalData(CbmDataLogInputEntity input);
        bool UpdateCbmDataLog(List<JunFuDb.CbmDataLog> updateData);
        int FindCbmDataLogId(string checkNumber);
        JunFuDb.CbmDataLog GetCbmDataLogById(int id);
        decimal FindMaxDeliveryRequestIdByCheckNumber(string checkNumber);
        TcDeliveryRequest FindDeliveryRequestById(decimal id);
        bool UpdateDeliveryRequest(List<TcDeliveryRequest> updateData);
    }
}
