﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;
using System.Linq;
using LightYear.JunFuTrans.BL.BE.CbmDataLog;

namespace LightYear.JunFuTrans.DA.Repositories.CbmDataLog
{
    public class CbmDataLogRepository : ICbmDataLogRepository
    {
        JunFuDbContext JunFuDbContext;
        public CbmDataLogRepository(JunFuDbContext junFuDbContext)
        {
            JunFuDbContext = junFuDbContext;
        }
        public bool InsertIntoCbmDataLogBulky(List<JunFuDb.CbmDataLog> data)
        {
            try
            {
                JunFuDbContext.CbmDataLogs.AddRange(data);
                JunFuDbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool LogSchedule(CbmDataLogRecord logData)
        {
            try
            {
                JunFuDbContext.CbmDataLogRecords.Add(logData);
                JunFuDbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateSchdule(CbmDataLogRecord logData)
        {
            try
            {
                JunFuDbContext.CbmDataLogRecords.Attach(logData);
                /*var entry = JunFuDbContext.Entry(logData);
                entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;*/
                JunFuDbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public CbmDataLogRecord FindUncompletedScheduleLogByFileName(string fileName, string dataSource)
        {
            return JunFuDbContext.CbmDataLogRecords.Where(x => x.FileName == fileName && x.CompleteDate == null && x.DataSource == dataSource).OrderByDescending(x => x.Cdate).FirstOrDefault();
        }

        public bool IsAlreadyCompletedScheduleLogByFileName(string fileName, string dataSource)
        {
            return JunFuDbContext.CbmDataLogRecords.Where(x => x.FileName == fileName && x.IsSuccess == true && x.DataSource == dataSource).OrderByDescending(x => x.CompleteDate).Count() > 0;
        }

        public bool CbmSpecialDataInsertIntoScanLog(TtDeliveryScanLog scanLog)
        {
            try
            {
                JunFuDbContext.TtDeliveryScanLogs.Add(scanLog);
                JunFuDbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public IEnumerable<JunFuDb.CbmDataLog> GetAbnormalData(CbmDataLogInputEntity input)
        {
            return JunFuDbContext.CbmDataLogs.Where(x => x.ScanTime < input.CbmScanEndDate.AddHours(10) && x.ScanTime > input.CbmScanStartDate.AddDays(-1).AddHours(10) && ((x.CheckNumber.Length == 12 && x.S3PicUri == "") || (x.CheckNumber.Length < 12 && x.S3PicUri != "")) && x.IsLog == false).OrderBy(x => x.FileName).ThenBy(x => x.FileRow);
        }

        public bool UpdateCbmDataLog(List<JunFuDb.CbmDataLog> updateData)
        {
            try
            {
                JunFuDbContext.CbmDataLogs.AttachRange(updateData);
                /*var entry = JunFuDbContext.Entry(updateData);
                entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;*/
                JunFuDbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public int FindCbmDataLogId(string checkNumber)
        {
            return JunFuDbContext.CbmDataLogs.Where(x => x.CheckNumber == checkNumber && x.IsLog != true && x.S3PicUri == "").FirstOrDefault()?.Id ?? 0;
        }

        public JunFuDb.CbmDataLog GetCbmDataLogById(int id)
        {
            return JunFuDbContext.CbmDataLogs.Where(x=>x.Id == id).FirstOrDefault();
        }

        public decimal FindMaxDeliveryRequestIdByCheckNumber(string checkNumber)
        {
            return JunFuDbContext.TcDeliveryRequests.Where(x => x.CheckNumber == checkNumber).OrderByDescending(x=>x.RequestId).FirstOrDefault()?.RequestId ?? 0;
        }

        public TcDeliveryRequest FindDeliveryRequestById(decimal id)
        {
            return JunFuDbContext.TcDeliveryRequests.Where(x => x.RequestId == id).FirstOrDefault();
        }

        public bool UpdateDeliveryRequest(List<TcDeliveryRequest> updateData)
        {
            try
            {
                JunFuDbContext.TcDeliveryRequests.AttachRange(updateData);
                /*var entry = JunFuDbContext.Entry(updateData);
                entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;*/
                JunFuDbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
