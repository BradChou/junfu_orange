﻿using LightYear.JunFuTrans.BL.BE.DriverDispatch;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace LightYear.JunFuTrans.DA.Repositories.DriverDispatch
{
    public class DriverDispatchRepository : IDriverDispatchRepository
    {
        public JunFuDbContext JunFuDbContext { get; set; }

        public DriverDispatchRepository(JunFuDbContext junFuDbContext)
        {
            JunFuDbContext = junFuDbContext;
        }

        /// <summary>
        /// 取得未集貨狀態
        /// </summary>
        /// <returns></returns>
        public List<TbItemCode> GetPickUpOption()
        {
            return JunFuDbContext.TbItemCodes.Where(a => a.CodeBclass.Equals("5") && a.CodeSclass.Equals("RO") && a.Memo.Equals("未集貨狀態")).ToList();
        }

        /// <summary>
        /// 取得取件狀態
        /// </summary>
        /// <returns></returns>
        public List<TbItemCode> GetAllPickUpOption()
        {
            return JunFuDbContext.TbItemCodes.Where(a => a.CodeBclass.Equals("5") && a.CodeSclass.Equals("RO")).ToList();
        }

        public void UpdateMDSD(string customerCode, string checkNumber, string md, string sd)
        {

            var pickups = JunFuDbContext.PickupRequestForApiusers.Where(m => m.CustomerCode.Equals(customerCode) && m.CheckNumber.Equals(checkNumber)).FirstOrDefault();

            if (pickups != null)
            {
                if (md.Equals("") && sd.Equals(""))
                {
                    return;
                }

                if (!md.Equals(""))
                {
                    pickups.ReassignMd = md;
                }

                if (!sd.Equals(""))
                {
                    pickups.Sd = sd;
                }

                JunFuDbContext.SaveChanges();
            }
        }


        /// <summary>
        /// 一般-取得司機正物流派遣明細
        /// </summary>
        /// <param name="driverCode"></param>
        /// <returns></returns>
        public List<DriverDispatchEntity> GetGeneralDispatchPickUpList(string driverCode)
        {
            var todayFourPM = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 16, 00, 00);

            var driver = JunFuDbContext.TbDrivers.Where(a => a.DriverCode.Equals(driverCode)).FirstOrDefault();

            var DriverForCustomer = new List<DriverDispatchEntity>();

            if (driver.DriverType.Equals("MD"))
            {
                DriverForCustomer = (from c in JunFuDbContext.TbCustomers
                                     join requestLog in JunFuDbContext.PickUpRequestLogs on c.CustomerCode equals requestLog.RequestCustomerCode
                                     where requestLog.ReassignMd == driver.DriverTypeCode && c.SupplierCode.EndsWith(driver.Station)
                                     select new DriverDispatchEntity
                                     {
                                         DriverCode = driver.DriverCode,
                                         CustomerCode = c.CustomerCode,
                                         CustomerName = c.CustomerName,
                                         CustomerTel = c.Telephone + c.Fax,
                                         CustomerAddress = c.ShipmentsCity + c.ShipmentsArea + c.ShipmentsRoad
                                     }).ToList();
            }
            else if (driver.DriverType.Equals("SD"))
            {
                DriverForCustomer = JunFuDbContext.TbCustomers
                                    .Where(a => a.AssignedSd.Equals(driver.DriverTypeCode)
                                    && a.SupplierCode.Equals("F" + driver.Station))
                                    .Select(a => new DriverDispatchEntity
                                    {
                                        DriverCode = driver.DriverCode,
                                        CustomerCode = a.CustomerCode,
                                        CustomerName = a.CustomerName,
                                        CustomerTel = a.Telephone + a.Fax,
                                        CustomerAddress = a.ShipmentsCity + a.ShipmentsArea + a.ShipmentsRoad
                                    }).ToList();
            }

            var shouldPickUp = (from d in DriverForCustomer
                                join p in JunFuDbContext.PickUpRequestLogs on d.CustomerCode equals p.RequestCustomerCode
                                where p.Cdate <= todayFourPM && p.Cdate > todayFourPM.AddDays(-1) && d.DriverCode == driverCode
                                group p by new
                                {
                                    d.CustomerCode,
                                    d.CustomerName,
                                    d.DriverCode,
                                    d.CustomerTel,
                                    d.CustomerAddress,
                                    Cdate = p.Cdate.Value.Date,
                                    //p.PickUpPieces
                                } into g
                                select new
                                {
                                    DriverCode = g.Key.DriverCode,
                                    DispatchDate = g.Key.Cdate,
                                    CustomerName = g.Key.CustomerName,
                                    CustomerCode = g.Key.CustomerCode,
                                    CustomerTel = g.Key.CustomerTel,
                                    CustomerAddress = g.Key.CustomerAddress,
                                    ShouldPickUpCount = g.Sum(s => s.PickUpPieces ?? 0),
                                }).ToList();

            var shouldPickUpCustomer = shouldPickUp.Select(a => a.CustomerCode).ToList();

            var actualPickUp = (from tc in JunFuDbContext.TcDeliveryRequests
                                where tc.ShipDate <= DateTime.Now && tc.ShipDate > DateTime.Now.Date 
                                && tc.ShipDate != null && tc.CancelDate == null
                                && shouldPickUpCustomer.Contains(tc.CustomerCode)
                                group tc by new { CustomerCode = tc.CustomerCode, ShipDate = tc.ShipDate.Value.Date } into g
                                select new
                                {
                                    ShipDate = g.Key.ShipDate,
                                    CustomerCode = g.Key.CustomerCode,
                                    ActualPickUpCount = g.Sum(a => a.Pieces ?? 0)
                                }).ToList();

            var output = actualPickUp.Count.Equals(0) ? (
                  (from s in shouldPickUp
                       //join a in actualPickUp on s.CustomerCode equals a.CustomerCode into p
                       //from a in p.DefaultIfEmpty()
                   select new DriverDispatchEntity
                   {
                       DriverCode = s.DriverCode,
                       PickUpType = "一般",
                       DispatchDate = s.DispatchDate.ToString("yyyy/MM/dd"),
                       CustomerName = s.CustomerName,
                       CustomerCode = s.CustomerCode,
                       CustomerTel = s.CustomerTel,
                       CustomerAddress = s.CustomerAddress,
                       ShouldPickUpCount = s.ShouldPickUpCount,
                       ActualPickUpCount = 0,
                   }).ToList()) :
            (from s in shouldPickUp
             join a in actualPickUp on s.CustomerCode equals a.CustomerCode into p
             from a in p.DefaultIfEmpty(new { ShipDate = default(DateTime), CustomerCode = default(string) ,ActualPickUpCount = default(int)})
             select new DriverDispatchEntity
             {
                 DriverCode = s.DriverCode,
                 PickUpType = "一般",
                 DispatchDate = s.DispatchDate.ToString("yyyy/MM/dd"),
                 CustomerName = s.CustomerName,
                 CustomerCode = s.CustomerCode,
                 CustomerTel = s.CustomerTel,
                 CustomerAddress = s.CustomerAddress,
                 ShouldPickUpCount = s.ShouldPickUpCount,
                 ActualPickUpCount = a.ActualPickUpCount
             }).ToList();

            //output.ForEach(a => a.DriverMDSD = sendDriverSDMD);
            return output;
        }

        /// <summary>
        /// 一般-取得單次派遣託運單號明細
        /// </summary>
        /// <param name="dispatchJsonEntity"></param>
        /// <returns></returns>
        public List<CheckNumberListWithinDispathEntity> GetGeneralCheckNumberListWithinDispath(DispatchJsonEntity dispatchJsonEntity)
        {
            using (var txn = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
            { IsolationLevel = IsolationLevel.ReadUncommitted }))
            {
                var todayFourPM = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 16, 00, 00);

                var pickUpedOutput = (from p in JunFuDbContext.PickUpRequestLogs
                                      join tc in JunFuDbContext.TcDeliveryRequests on p.RequestCustomerCode equals tc.CustomerCode
                                      join tt in JunFuDbContext.TtDeliveryScanLogs on tc.LatestPickUpScanLogId equals Convert.ToInt32(tt.LogId)
                                      where tc.CustomerCode.Equals(dispatchJsonEntity.CustomerCode)
                                            && p.Cdate <= todayFourPM && p.Cdate > todayFourPM.AddDays(-1)
                                            && tc.ShipDate == null && tc.LatestPickUpScanLogId != null
                                            && tc.CancelDate == null
                                            && (tc.SendCity + tc.SendArea + tc.SendAddress).Equals(dispatchJsonEntity.SendAddress)
                                      select new
                                      {
                                          CheckNumber = tc.CheckNumber,
                                          SendContact = tc.SendContact,
                                          Pieces = tc.Pieces ?? 0,
                                          IsShip = tc.ShipDate == null ? false : true,
                                          ShipCategory = tt.ReceiveOption == null ? "" : tt.ReceiveOption
                                      }).Distinct().ToList();

                var notPickUpOutput = (from p in JunFuDbContext.PickUpRequestLogs
                                       join tc in JunFuDbContext.TcDeliveryRequests on p.RequestCustomerCode equals tc.CustomerCode
                                       where tc.CustomerCode.Equals(dispatchJsonEntity.CustomerCode)
                                        && p.Cdate <= todayFourPM && p.Cdate > todayFourPM.AddDays(-1)
                                        && tc.ShipDate == null && tc.LatestPickUpScanLogId == null
                                        && tc.CancelDate == null
                                        && (tc.SendCity + tc.SendArea + tc.SendAddress).Equals(dispatchJsonEntity.SendAddress)
                                       select new
                                       {
                                           CheckNumber = tc.CheckNumber,
                                           SendContact = tc.SendContact,
                                           Pieces = tc.Pieces ?? 0,
                                           IsShip = tc.ShipDate == null ? false : true,
                                           ShipCategory = ""
                                       }).Distinct().ToList();

                var Union = pickUpedOutput.Union(notPickUpOutput).Select(tc => new CheckNumberListWithinDispathEntity
                {
                    CheckNumber = tc.CheckNumber,
                    SendContact = tc.SendContact,
                    Pieces = tc.Pieces,
                    IsShip = tc.IsShip,
                    ShipCategory = tc.ShipCategory
                }).ToList();

                return Union;
            }
        }

        /// <summary>
        /// 甲配-取得單次派遣託運單號明細
        /// </summary>
        /// <param name="dispatchJsonEntity"></param>
        /// <returns></returns>
        public List<CheckNumberListWithinDispathEntity> GetTheThirdPlatformCheckNumberListWithinDispath(DispatchJsonEntity dispatchJsonEntity)
        {
            var todayFivePM = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 17, 00, 00);
            
            // D-3日的17:00~D-1日的17:00 頭尾都包含
            var yesterdayFivePM = todayFivePM.AddDays(-1);
            var threeDaysAgoFivePM = todayFivePM.AddDays(-3); 

            var pickUpedOutput = (from p in JunFuDbContext.PickupRequestForApiusers
                                  join tc in JunFuDbContext.TcDeliveryRequests on p.CheckNumber equals tc.CheckNumber
                                  join tt in JunFuDbContext.TtDeliveryScanLogs on tc.LatestPickUpScanLogId equals Convert.ToInt32(tt.LogId)
                                  where p.CustomerCode.Equals(dispatchJsonEntity.CustomerCode)
                                        && p.RequestDate <= yesterdayFivePM && p.RequestDate >= threeDaysAgoFivePM
                                        && tc.ShipDate == null && tc.LatestPickUpScanLogId != null
                                        && tc.CancelDate == null
                                        && (tc.SendCity + tc.SendArea + tc.SendAddress).Equals(dispatchJsonEntity.SendAddress)
                                        && (p.CustomerCode == "F2900210002" || p.CustomerCode == "F1300600002")
                                        && p.ReassignMd == dispatchJsonEntity.MD
                                  select new
                                  {
                                      CheckNumber = tc.CheckNumber,
                                      SendContact = tc.SendContact,
                                      Pieces = tc.Pieces ?? 0,
                                      IsShip = tc.ShipDate == null ? false : true,
                                      ShipCategory = tt.ReceiveOption == null ? "" : tt.ReceiveOption
                                  }).Distinct().ToList();

            var notPickUpOutput = (from p in JunFuDbContext.PickupRequestForApiusers
                                   join tc in JunFuDbContext.TcDeliveryRequests on p.CheckNumber equals tc.CheckNumber
                                   where p.CustomerCode.Equals(dispatchJsonEntity.CustomerCode)
                                         && p.RequestDate <= yesterdayFivePM && p.RequestDate >= threeDaysAgoFivePM
                                         && tc.ShipDate == null && tc.LatestPickUpScanLogId == null
                                         && tc.CancelDate == null
                                         && (tc.SendCity + tc.SendArea + tc.SendAddress).Equals(dispatchJsonEntity.SendAddress)
                                         && (p.CustomerCode == "F2900210002" || p.CustomerCode == "F1300600002")
                                         && p.ReassignMd == dispatchJsonEntity.MD
                                   select new
                                   {
                                       CheckNumber = tc.CheckNumber,
                                       SendContact = tc.SendContact,
                                       Pieces = tc.Pieces ?? 0,
                                       IsShip = tc.ShipDate == null ? false : true,
                                       ShipCategory = ""
                                   }).Distinct().ToList();

            var Union = pickUpedOutput.Union(notPickUpOutput).Select(tc => new CheckNumberListWithinDispathEntity
            {
                CheckNumber = tc.CheckNumber,
                SendContact = tc.SendContact,
                Pieces = tc.Pieces,
                IsShip = tc.IsShip,
                ShipCategory = tc.ShipCategory
            }).ToList();

            return Union;
        }

        /// <summary>
        /// 甲配-取得司機正物流派遣明細
        /// </summary>
        /// <param name="driverCode"></param>
        /// <returns></returns>
        public List<DriverDispatchEntity> GetTheThirdPlatformDispatchPickUpList(string driverCode)
        {
            var todayFivePM = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 17, 00, 00);

            // D-3日的17:00~D-1日的17:00 頭尾都包含
            var yesterdayFivePM = todayFivePM.AddDays(-1);
            var threeDaysAgoFivePM = todayFivePM.AddDays(-3);

            var driver = JunFuDbContext.TbDrivers.Where(a => a.DriverCode.Equals(driverCode)).FirstOrDefault();

            List<DriverDispatchEntity> checkNumbers = new List<DriverDispatchEntity>();

            if (driver.DriverType.Equals("MD"))
            {
                checkNumbers = (from requestLog in JunFuDbContext.PickupRequestForApiusers
                                join dr in JunFuDbContext.TcDeliveryRequests on requestLog.CheckNumber equals dr.CheckNumber
                                where requestLog.ReassignMd == driver.DriverTypeCode && requestLog.SupplierCode.EndsWith(driver.Station)
                                && requestLog.RequestDate <= yesterdayFivePM && requestLog.RequestDate >= threeDaysAgoFivePM
                                && dr.CancelDate == null
                                select new DriverDispatchEntity
                                {
                                    DriverCode = driver.DriverCode,
                                    DispatchDate = (requestLog.RequestDate ?? new DateTime(1911, 01, 01)).ToString("yyyy/MM/dd"),
                                    CustomerCode = dr.CustomerCode,
                                    CustomerName = dr.SendContact,
                                    CustomerTel = dr.SendTel,
                                    CustomerAddress = dr.SendCity + dr.SendArea + dr.SendAddress,
                                    PickUpType = "甲配",
                                    ShouldPickUpCount = requestLog.Pieces ?? 0
                                }).ToList();
            }
            else if (driver.DriverType.Equals("SD"))
            {
                checkNumbers = (from a in JunFuDbContext.PickupRequestForApiusers
                                join tc in JunFuDbContext.TcDeliveryRequests on a.CheckNumber equals tc.CheckNumber
                                where a.Sd.Equals(driver.DriverTypeCode) && a.SupplierCode.Equals("F" + driver.Station)
                                && a.RequestDate <= yesterdayFivePM && a.RequestDate >= threeDaysAgoFivePM
                                && tc.CancelDate == null
                                && (a.CustomerCode == "F2900210002" || a.CustomerCode == "F1300600002")
                                select new DriverDispatchEntity
                                {
                                    DriverCode = driver.DriverCode,
                                    DispatchDate = (a.RequestDate ?? new DateTime(1911, 01, 01)).ToString("yyyy/MM/dd"),
                                    CustomerCode = a.CustomerCode,
                                    CustomerName = tc.SendContact,
                                    CustomerTel = tc.SendTel,
                                    CustomerAddress = tc.SendCity + tc.SendArea + tc.SendAddress,
                                    PickUpType = "甲配",
                                    ShouldPickUpCount = a.Pieces ?? 0
                                }).ToList();
            }

            //應收
            var shouldPickUp = checkNumbers.GroupBy(a => new
            {
                a.DriverCode,
                a.CustomerCode,
                a.CustomerName,
                a.CustomerTel,
                a.CustomerAddress,
                a.DispatchDate
            })
                               .Select(g => new
                               {
                                   DriverCode = g.Key.DriverCode,
                                   DispatchDate = g.Key.DispatchDate,
                                   CustomerCode = g.Key.CustomerCode,
                                   CustomerName = g.Key.CustomerName,
                                   CustomerTel = g.Key.CustomerTel,
                                   CustomerAddress = g.Key.CustomerAddress,
                                   PickUpType = "甲配",
                                   ShouldPickUpCount = g.Sum(c => c.ShouldPickUpCount)
                               });


            //實收
            var actualPickUp = (from tc in JunFuDbContext.TcDeliveryRequests
                                where tc.ShipDate <= DateTime.Now && tc.ShipDate > DateTime.Now.Date
                                 && tc.ShipDate != null && tc.CancelDate == null
                                 && (tc.CustomerCode == "F2900210002" || tc.CustomerCode == "F1300600002")
                                group tc by new
                               {
                                   CustomerCode = tc.CustomerCode,
                                   CustomerName = tc.SendContact,
                                   //CustomerTel = tc.SendTel,
                                   CustomerAddress = tc.SendCity + tc.SendArea + tc.SendAddress,
                                   ShipDate = tc.ShipDate.Value.Date,
                               } into g
                               select new
                               {
                                   ShipDate = g.Key.ShipDate,
                                   CustomerCode = g.Key.CustomerCode,
                                   CustomerName = g.Key.CustomerName,
                                   //CustomerTel = g.Key.CustomerTel,
                                   CustomerAddress = g.Key.CustomerAddress,
                                   ActualPickUpCount = g.Sum(c => c.Pieces ?? 0)
                               });

            var output = (from a in shouldPickUp
                          join b in actualPickUp on
                          new { CustomerCode = a.CustomerCode, CustomerName = a.CustomerName/*, CustomerTel = a.CustomerTel*/, CustomerAddress = a.CustomerAddress }
                          equals
                          new { CustomerCode = b.CustomerCode, CustomerName = b.CustomerName/*, CustomerTel = b.CustomerTel*/, CustomerAddress = b.CustomerAddress }
                          into temp
                          from b in temp.DefaultIfEmpty(new { ShipDate = default(DateTime), CustomerCode = default(string), CustomerName = default(string) ,CustomerAddress = default(string), ActualPickUpCount = default(int) })
                          select new DriverDispatchEntity
                          {
                              DriverCode = a.DriverCode,
                              PickUpType = a.PickUpType,
                              DispatchDate = a.DispatchDate,
                              CustomerName = a.CustomerName,
                              CustomerCode = a.CustomerCode,
                              CustomerTel = a.CustomerTel,
                              CustomerAddress = a.CustomerAddress,
                              ShouldPickUpCount = a.ShouldPickUpCount,
                              ActualPickUpCount = b.ActualPickUpCount,
                          }).ToList();

            return output;
        }

        public Driver GetDriverMDSD(string driver)
        {
            var data = JunFuDbContext.TbDrivers.Where(a => a.DriverCode.Equals(driver))
                   .Select(a => new Driver { DriverType = a.DriverType, DriverTypeCode = a.DriverTypeCode })
                   .FirstOrDefault();

            if (data.DriverType == null || data.DriverTypeCode == null)
            {
                return new Driver { DriverType = "", DriverTypeCode = "" };
            }

            return data;
        }
    }
}
