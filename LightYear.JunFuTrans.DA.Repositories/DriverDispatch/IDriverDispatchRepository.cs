﻿using LightYear.JunFuTrans.BL.BE.DriverDispatch;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.DriverDispatch
{
    public interface IDriverDispatchRepository
    {
        List<TbItemCode> GetPickUpOption();
        List<DriverDispatchEntity> GetGeneralDispatchPickUpList(string driverCode);
        List<DriverDispatchEntity> GetTheThirdPlatformDispatchPickUpList(string driverCode);
        List<CheckNumberListWithinDispathEntity> GetTheThirdPlatformCheckNumberListWithinDispath(DispatchJsonEntity dispatchJsonEntity);
        List<CheckNumberListWithinDispathEntity> GetGeneralCheckNumberListWithinDispath(DispatchJsonEntity dispatchJsonEntity);
        Driver GetDriverMDSD(string driver);
        List<TbItemCode> GetAllPickUpOption();

        void UpdateMDSD(string customerCode, string checkNumber, string md, string sd);
    }
}
