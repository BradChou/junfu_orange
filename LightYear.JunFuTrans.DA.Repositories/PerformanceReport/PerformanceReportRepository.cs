﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.Repositories.PerformanceReport;
using LightYear.JunFuTrans.BL.BE.PerformanceReport;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.PerformanceReport
{
    public class PerformanceReportRepository : IPerformanceReportRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }

        public PerformanceReportRepository(JunFuDbContext junFuDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
        }

        public List<PerformanceReportRepositoryEntity> PerformanceReportRepositoryBySendStationList(FrontendInputEntity frontendInput)
        {
            var data = from requests in this.JunFuDbContext.TcDeliveryRequests
                       join accounts in this.JunFuDbContext.TbCustomers on requests.CustomerCode equals accounts.CustomerCode
                       where requests.ShipDate > frontendInput.Start.AddHours(5) & requests.ShipDate < frontendInput.End.AddDays(1).AddHours(5) & (requests.SupplierCode == frontendInput.SendStation || frontendInput.SendStation == "*")
                       orderby accounts.CustomerCode
                       select new PerformanceReportRepositoryEntity()
                       {
                           AccountCode = requests.CustomerCode,
                           UserName = accounts.CustomerName,
                           ShipDate = requests.ShipDate,
                           DetailEntity = new PerformanceReportDetailEntity()
                           {
                               CustomerCode = requests.CustomerCode,
                               CheckNumber = requests.CheckNumber,
                               ReceiveContact = requests.ReceiveContact,
                               TEL = requests.ReceiveTel1,
                               City = requests.ReceiveCity,
                               Region = requests.ReceiveArea,
                               Address = requests.ReceiveAddress,
                               ShipDate = requests.ShipDate
                           }
                       };

            return data.ToList();
        }

        public List<PerformanceReportRepositoryEntity> PerformanceReportRepositoryByAccountCodeList(FrontendInputEntity frontendInput)
        {
            var data = from requests in this.JunFuDbContext.TcDeliveryRequests
                       join accounts in this.JunFuDbContext.TbCustomers on requests.CustomerCode equals accounts.CustomerCode
                       where requests.ShipDate > frontendInput.Start.AddHours(5) & requests.ShipDate < frontendInput.End.AddDays(1).AddHours(5) & (requests.CustomerCode == frontendInput.AccountCode || frontendInput.AccountCode == "*")
                       orderby accounts.CustomerCode
                       select new PerformanceReportRepositoryEntity()
                       {
                           AccountCode = requests.CustomerCode,
                           UserName = accounts.CustomerName,
                           ShipDate = requests.ShipDate,
                           DetailEntity = new PerformanceReportDetailEntity()
                           {
                               CustomerCode = requests.CustomerCode,
                               CheckNumber = requests.CheckNumber,
                               ReceiveContact = requests.ReceiveContact,
                               TEL = requests.ReceiveTel1,
                               City = requests.ReceiveCity,
                               Region = requests.ReceiveArea,
                               Address = requests.ReceiveAddress,
                               ShipDate = requests.ShipDate
                           }
                       };

            return data.ToList();
        }

        public List<AccountEntity> GetAccountEntities(string station)
        {
            var data = from accounts in this.JunFuDbContext.TbCustomers
                       where accounts.CustomerCode != null & accounts.CustomerCode.StartsWith(station) & accounts.CustomerCode.StartsWith("F")
                       orderby accounts.CustomerCode
                       select new AccountEntity()
                       {
                           AccountCode = accounts.CustomerCode,
                           AccountName = accounts.CustomerName
                       };

            return data.ToList();
        }

        public List<AccountEntity> MasterGetAccountEntities(string station)       //總公司專用
        {
            var data = from accounts in this.JunFuDbContext.TbCustomers
                       where accounts.CustomerCode != null & accounts.CustomerCode.StartsWith("F")
                       orderby accounts.CustomerCode
                       select new AccountEntity()
                       {
                           AccountCode = accounts.CustomerCode,
                           AccountName = accounts.CustomerName
                       };

            return data.ToList();
        }

        public TbCustomer GetByAccountCode(string accountCode)
        {
            var data = from account in JunFuDbContext.TbCustomers where account.CustomerCode.Equals(accountCode) select account;

            if (data.Count() > 0)
            {
                return data.FirstOrDefault();
            }
            else
            {
                return null;
            }
        }
    }
}
