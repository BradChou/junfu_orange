﻿using LightYear.JunFuTrans.DA.LightYearTest;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.StudentTest
{
    public class TestDataRepository : ITestDataRepository
    {
        public LightYearTestDbContext LightYearTestDbContext { get; private set; }

        public TestDataRepository(LightYearTestDbContext lightYearTestDbContext)
        {
            this.LightYearTestDbContext = lightYearTestDbContext;
        }

        public void Delete(int id)
        {
            var testData = this.LightYearTestDbContext.TestDatas.Find(id);

            if (testData != null)
            {
                this.LightYearTestDbContext.TestDatas.Remove(testData);

                this.LightYearTestDbContext.SaveChanges();
            }
        }

        public TestData GetTestData(int id)
        {
            var data = from testData in this.LightYearTestDbContext.TestDatas where testData.Id.Equals(id) select testData;

            if (data.Count() == 0)
            {
                return null;
            }

            return data.FirstOrDefault();
        }

        public List<TestData> GetTestDatas()
        {
            var data = from testData in this.LightYearTestDbContext.TestDatas orderby testData.Id descending select testData;

            return data.ToList();
        }

        public int Insert(TestData testData)
        {
            this.LightYearTestDbContext.TestDatas.Add(testData);

            this.LightYearTestDbContext.SaveChanges();

            return testData.Id;
        }

        public void Update(TestData testData)
        {
            this.LightYearTestDbContext.TestDatas.Attach(testData);

            var entry = this.LightYearTestDbContext.Entry(testData);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            this.LightYearTestDbContext.SaveChanges();
        }
    }
}
