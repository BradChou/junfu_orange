﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.LightYearTest;

namespace LightYear.JunFuTrans.DA.Repositories.StudentTest
{
    public interface IStudentRepository
    {
        int Insert(Student student);

        void Update(Student student);

        List<Student> GetStudents();

        Student GetStudent(int id);

        void Delete(int id);

    }
}
