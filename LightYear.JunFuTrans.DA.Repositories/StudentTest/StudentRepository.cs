﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.LightYearTest;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.StudentTest
{
    public class StudentRepository : IStudentRepository
    {
        public LightYearTestDbContext LightYearTestDbContext { get; private set; }

        public StudentRepository(LightYearTestDbContext lightYearTestDbContext)
        {
            this.LightYearTestDbContext = lightYearTestDbContext;
        }

        public Student GetStudent(int id)
        {
            var data = from student in this.LightYearTestDbContext.Students where student.StudentId.Equals(id) select student;

            if (data.Count() == 0)
            {
                return null;
            }

            return data.FirstOrDefault();
        }

        public List<Student> GetStudents()
        {
            var data = from student in this.LightYearTestDbContext.Students orderby student.StudentId descending select student;

            return data.ToList();
        }

        public int Insert(Student student)
        {
            this.LightYearTestDbContext.Students.Add(student);

            this.LightYearTestDbContext.SaveChanges();

            return student.StudentId;
        }

        public void Update(Student student)
        {
            this.LightYearTestDbContext.Students.Attach(student);

            var entry = this.LightYearTestDbContext.Entry(student);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            this.LightYearTestDbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            var student = this.LightYearTestDbContext.Students.Find(id);

            if (student != null)
            {
                this.LightYearTestDbContext.Students.Remove(student);

                this.LightYearTestDbContext.SaveChanges();
            }
        }
    }
}
