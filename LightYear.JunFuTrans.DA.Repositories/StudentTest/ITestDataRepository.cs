﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.LightYearTest;

namespace LightYear.JunFuTrans.DA.Repositories.StudentTest
{
    public interface ITestDataRepository
    {
        int Insert(TestData testData);

        void Update(TestData testData);

        List<TestData> GetTestDatas();

        TestData GetTestData(int id);

        void Delete(int id);
    }
}
