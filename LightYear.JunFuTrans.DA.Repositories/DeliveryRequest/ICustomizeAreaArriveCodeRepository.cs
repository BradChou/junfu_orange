﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryRequest
{
    public interface ICustomizeAreaArriveCodeRepository
    {
        List<CustomizeAreaArriveCode> GetAll();
    }
}
