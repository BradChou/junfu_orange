﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryRequest
{
    public interface ICheckNumberSDMappingRepository
    {
        long Insert(CheckNumberSdMapping checkNumberSdMapping);

        void Update(CheckNumberSdMapping checkNumberSdMapping);

        Dictionary<long, CheckNumberSdMapping> CheckExistByRequestIds(List<decimal> requestIds);

        void InsertMutiData(List<CheckNumberSdMapping> checkNumberSdMappings);
    }
}
