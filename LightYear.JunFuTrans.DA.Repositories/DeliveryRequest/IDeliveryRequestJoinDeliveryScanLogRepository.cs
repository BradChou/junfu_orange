﻿using LightYear.JunFuTrans.BL.BE.DeliveryRate;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryRequest
{
    public interface IDeliveryRequestJoinDeliveryScanLogRepository
    {
        IEnumerable<DeliveryDetailEntity> GetDeliveryDetailByDriverCodeAndDeliveryDatePeriod(string driverCode, DateTime deliveryDateStart, DateTime deliveryDateEnd);

        IEnumerable<DeliveryRequestJoinScanLogForMatingRate> GetJoinedEntitiesByStationAndShipDatePeriod(IEnumerable<string> stationScode, DateTime start, DateTime end);

        IEnumerable<DeliveryRequestJoinScanLogForMatingRate> GetJoinedEntitiesByCustomerAndShipDatePeriod(string customerCode, DateTime start, DateTime end);

        /// <summary>
        /// 已集貨的訂單
        /// </summary>
        /// <param name="customerCode"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        IEnumerable<TcDeliveryRequest> GetRequestsAlreadyPickUpByCustomerAndMonth(string customerCode, int year, int month);

        IEnumerable<TcDeliveryRequest> GetRequestsAlreadyPickUpByCustomerListAndMonth(IEnumerable<string> customerCodes, int year, int month);

        IEnumerable<DeliveryRequestJoinScanLogForMatingRate> GetJoinedEntitiesByShipDatePeriod(DateTime start, DateTime end);

        IEnumerable<DeliveryRequestJoinScanLogForMatingRate> GetJoinedEntitiesByAreaAndShipDatePeriod(string areaName, DateTime start, DateTime end);

        /// <summary>
        /// 到著站時間內有掃配送的
        /// </summary>
        /// <param name="stationScode"></param>
        /// <param name="deliveryDateStart"></param>
        /// <param name="deliveryDateEnd"></param>
        /// <returns></returns>
        IEnumerable<DeliveryRequestJoinScanLogForMatingRate> GetJoinedEntityByStationListAndDeliveryDate(IEnumerable<string> stationScode, DateTime deliveryDateStart, DateTime deliveryDateEnd);

        /// <summary>
        /// 時間內有掃配送的某客戶訂單
        /// </summary>
        /// <param name="customerCode"></param>
        /// <param name="deliveryDateStart"></param>
        /// <param name="deliveryDateEnd"></param>
        /// <returns></returns>
        IEnumerable<DeliveryRequestJoinScanLogForMatingRate> GetJoinedEntityByCustomerAndDeliveryDate(string customerCode, DateTime deliveryDateStart, DateTime deliveryDateEnd);


        /// <summary>
        /// 全部站所時間內有掃配送的
        /// </summary>
        /// <param name="stationScode"></param>
        /// <param name="deliveryDateStart"></param>
        /// <param name="deliveryDateEnd"></param>
        /// <returns></returns>
        IEnumerable<DeliveryRequestJoinScanLogForMatingRate> GetJoinedEntityByDeliveryDate(DateTime deliveryDateStart, DateTime deliveryDateEnd);

        Dictionary<Tuple<DateTime, string>, int> GetPickUpAmountByCustomersAndDate(IEnumerable<string> customerCodes, DateTime start, DateTime end);
    }
}
