﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryRequest
{
    public class CheckNumberPreheadRepository : ICheckNumberPreheadRepository
    {
        public JunFuDbContext DbContext { get; set; }

        public CheckNumberPreheadRepository(JunFuDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public List<CheckNumberPrehead> Get()
        {
            return DbContext.CheckNumberPreheads.ToList();
        }

        public string GetPrefixByProductType(string productType)
        {
            return DbContext.CheckNumberPreheads.FirstOrDefault(c => c.ProductType == productType).CheckNumberPrehead_;
        }
    }
}
