﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;
using System.Linq;
using System.Security.Cryptography;
using LightYear.JunFuTrans.BL.BE.DeliveryRate;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryRequest
{
    public class DeliveryRequestRepository : IDeliveryRequestRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public DeliveryRequestRepository(JunFuDbContext junFuDbContext, JunFuTransDbContext junFuTransDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
            this.JunFuTransDbContext = junFuTransDbContext;
        }

        /// <summary>
        /// 從條碼號取得代收金額
        /// </summary>
        /// <param name="check_number">宅運單號</param>
        /// <returns></returns>
        public double GetCollectionMoney(string checkNumber)
        {
            var data = from tcDeliveryRequest in this.JunFuDbContext.TcDeliveryRequests where tcDeliveryRequest.CheckNumber.Equals(checkNumber) select tcDeliveryRequest;

            List<TcDeliveryRequest> deliveryRequests = data.ToList();

            if (deliveryRequests.Count > 0)
            {
                return deliveryRequests[0].CollectionMoney ?? 0;
            }
            else
            {
                return -1;
            }
        }

        public Dictionary<string, int> GetCollectionMoneyBatch(IEnumerable<string> checkNumbers)
        {
            var temp = JunFuDbContext.TcDeliveryRequests.Where(r => checkNumbers.Contains(r.CheckNumber))
                .Select(r => new { CheckNumber = r.CheckNumber, Id = r.RequestId }).ToList();

            // remove duplicate check number
            var ids = (from t in temp
                       group t by t.CheckNumber into g
                       select g.Max(r => r.Id)).ToList();

            var deliveryRequests = JunFuDbContext.TcDeliveryRequests.Where(r => ids.Contains(r.RequestId))
                .Select(r => new { CheckNumber = r.CheckNumber, CollectionMoney = r.CollectionMoney }).ToList();

            return deliveryRequests.ToDictionary(data => data.CheckNumber, data => (data.CollectionMoney ?? 0));
        }

        /// <summary>
        /// 從條碼取得相關資訊
        /// </summary>
        /// <param name="checkNumber">條碼號</param>
        /// <returns></returns>
        public TcDeliveryRequest GetByCheckNumber(string checkNumber)
        {
            var data = from tcDeliveryRequest in this.JunFuDbContext.TcDeliveryRequests where tcDeliveryRequest.CheckNumber.Equals(checkNumber) select tcDeliveryRequest;

            List<TcDeliveryRequest> deliveryRequests = data.ToList();

            if (deliveryRequests.Count > 0)
            {
                return deliveryRequests[0];
            }
            else
            {
                return null;
            }
        }
        public TcDeliveryRequest GetNotLessThanTruckloadByCheckNumber(string checkNumber)
        {
            var data = from tcDeliveryRequest in this.JunFuDbContext.TcDeliveryRequests 
                       where tcDeliveryRequest.LessThanTruckload.Equals(true)
                       & tcDeliveryRequest.CheckNumber.Equals(checkNumber)  
                       select tcDeliveryRequest;

            //多筆正常配交且應到實到站所相同的貨號
            var kickout = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                           join s in JunFuDbContext.TbDrivers on deliveryRequest.LatestArriveOptionDriver equals s.DriverCode
                           where (deliveryRequest.LatestArriveOptionDate > deliveryRequest.DeliveryCompleteDate && deliveryRequest.LatestArriveOption == "3" && deliveryRequest.AreaArriveCode == s.Station)
                           && deliveryRequest.CheckNumber.Equals(checkNumber)
                           && deliveryRequest.LessThanTruckload.Equals(true)
                           select deliveryRequest).FirstOrDefault();

            List<TcDeliveryRequest> deliveryRequests = data.ToList();

            if (kickout!= null)
            {
                return null;
            }
            if (deliveryRequests.Count > 0 )
            {
                return deliveryRequests[0];
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<TcDeliveryRequest> GetByCheckNumberList(IEnumerable<string> checkNumbers)
        {
            return JunFuDbContext.TcDeliveryRequests.Where(r => checkNumbers.Contains(r.CheckNumber)).ToLookup(p => p.CheckNumber).ToDictionary(p => p.Key.ToUpper(), p => p.Last()).Values;
        }

        /// <summary>
        /// 從託運單號陣列取得零擔DelievetRequest
        /// </summary>
        /// <param name="checkNumber">託運單號陣列</param>
        /// <returns></returns>
        public List<TcDeliveryRequest> GetNotLessThanTruckloadByCheckNumberList(List<string> checkNumbers)
        {
            return JunFuDbContext.TcDeliveryRequests.Where(r => checkNumbers.Contains(r.CheckNumber) & r.LessThanTruckload.Equals(true)).ToList();
        }
        public Dictionary<string, TcDeliveryRequest> GetDictionaryNotLessThanTruckloadByCheckNumberList(List<string> checkNumbers)
        {
            return JunFuDbContext.TcDeliveryRequests.Where(r => checkNumbers.Contains(r.CheckNumber) & r.LessThanTruckload.Equals(true)).ToDictionary(p => p.CheckNumber);
        }
        /// <summary>
        /// 查詢時間內收到的數量
        /// </summary>
        /// <param name="startDate">首刷件啟時間</param>
        /// <param name="endDate">首刷件迄止時間</param>
        /// <returns></returns>
        public int GetAllCount(DateTime startDate, DateTime endDate)
        {
            var count = (from deliveryRequerst in this.JunFuDbContext.TcDeliveryRequests where deliveryRequerst.CheckNumber != "" && deliveryRequerst.ShipDate > startDate && deliveryRequerst.ShipDate < endDate select deliveryRequerst).Count();

            return count;
        }

        /// <summary>
        /// 查詢客戶在時間內寄送的數量
        /// </summary>
        /// <param name="startDate">首刷件啟時間</param>
        /// <param name="endDate">首刷件迄止時間</param>
        /// <param name="customerCode">客戶代碼</param>
        /// <returns></returns>
        public int GetAllCountWithCustomer(DateTime startDate, DateTime endDate, string customerCode)
        {
            var count = (from deliveryRequerst in this.JunFuDbContext.TcDeliveryRequests where deliveryRequerst.CheckNumber != "" && deliveryRequerst.ShipDate > startDate && deliveryRequerst.ShipDate < endDate && deliveryRequerst.CustomerCode.Equals(customerCode) select deliveryRequerst).Count();

            return count;
        }

        public Dictionary<TcDeliveryRequest, string> GetDPlusOneDeliveryDictionary(string station, DateTime deliveryDate)
        {
            DateTime ShipStartDate = deliveryDate.AddHours(-19);
            DateTime ScanDeadLine = deliveryDate.AddDays(1);

            var  data = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                        join destinationDriver in this.JunFuDbContext.TbDrivers on deliveryRequest.LatestDestDriver equals destinationDriver.DriverCode into main_table
                        from main in main_table.DefaultIfEmpty()
                        where deliveryRequest.ShipDate >= ShipStartDate
                        && deliveryRequest.ShipDate < ShipStartDate.AddDays(1)
                        && deliveryRequest.LessThanTruckload.Equals(true)
                        && deliveryRequest.DeliveryType.Equals("D")
                        && deliveryRequest.CancelDate == null
                        && deliveryRequest.AreaArriveCode.Equals(station)
                        select new 
                        { 
                                deliveryRequest,actualDestinationStation=main.Station
                        }
                        ).ToDictionary(p=>p.deliveryRequest,q=>q.actualDestinationStation);

            return data;
        }

        public int GetCountByArriveCodeAndDate(string station, DateTime checkDate)
        {
            var count = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests where deliveryRequest.CheckNumber != "" && deliveryRequest.ShipDate > checkDate && deliveryRequest.ShipDate < checkDate.AddDays(1) && deliveryRequest.AreaArriveCode.Equals(station) select deliveryRequest).Count();

            return count;
        }

        public List<TcDeliveryRequest> GetListByArriveCodeAndDate(string station, DateTime checkDate)
        {
            var data = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests where deliveryRequest.CheckNumber != "" && deliveryRequest.ShipDate > checkDate && deliveryRequest.ShipDate < checkDate.AddDays(1) && deliveryRequest.AreaArriveCode.Equals(station) orderby deliveryRequest.ShipDate select deliveryRequest).ToList();

            return data;
        }

        public int GetSuccessCountByArriveCodeAndDate(string station, DateTime checkDate, int days)
        {
            var count = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                         join deliveryScanLogs in this.JunFuDbContext.TtDeliveryScanLogs on deliveryRequest.CheckNumber equals deliveryScanLogs.CheckNumber
                         where deliveryRequest.CheckNumber != "" && deliveryRequest.AreaArriveCode.Equals(station) && deliveryRequest.ShipDate > checkDate && deliveryRequest.ShipDate < checkDate.AddDays(1) &&
                        deliveryScanLogs.ScanDate < checkDate.AddDays((days + 1)) && (deliveryScanLogs.ScanItem.Equals("3") || deliveryScanLogs.ScanItem.Equals("4")) && deliveryScanLogs.ArriveOption.Equals("3")
                         select deliveryRequest).Distinct().Count();

            return count;
        }

        public List<TcDeliveryRequest> GetSuccessListByArriveCodeAndDate(string station, DateTime checkDate, int days)
        {
            var data = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                        join deliveryScanLogs in this.JunFuDbContext.TtDeliveryScanLogs on deliveryRequest.CheckNumber equals deliveryScanLogs.CheckNumber
                        where deliveryRequest.CheckNumber != "" && deliveryRequest.AreaArriveCode.Equals(station) && deliveryRequest.ShipDate > checkDate && deliveryRequest.ShipDate < checkDate.AddDays(1) &&
                        deliveryScanLogs.ScanDate < checkDate.AddDays((days + 1)) && (deliveryScanLogs.ScanItem.Equals("3") || deliveryScanLogs.ScanItem.Equals("4")) && deliveryScanLogs.ArriveOption.Equals("3")
                        orderby deliveryRequest.ShipDate
                        select deliveryRequest).Distinct().ToList();

            return data;
        }

        public List<string> GetCheckNumberListByCustomerCodeAndDateZone(string customerCode, DateTime start, DateTime end)
        {
            var data = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests where deliveryRequest.CheckNumber != "" && deliveryRequest.CustomerCode.Equals(customerCode) && deliveryRequest.ShipDate > start && deliveryRequest.ShipDate < end orderby deliveryRequest.CheckNumber select deliveryRequest.CheckNumber).ToList();

            return data;
        }

        public List<string> GetSuccessCheckNumberListByCustomerCodeAndDateZone(string customerCode, DateTime start, DateTime end)
        {
            var data = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                        join deliveryScanLogs in this.JunFuDbContext.TtDeliveryScanLogs on deliveryRequest.CheckNumber equals deliveryScanLogs.CheckNumber
                        where deliveryRequest.CheckNumber != "" && deliveryRequest.CustomerCode.Equals(customerCode) && deliveryRequest.ShipDate > start && deliveryRequest.ShipDate < end
                        && (deliveryScanLogs.ScanItem.Equals("3") || deliveryScanLogs.ScanItem.Equals("4")) && deliveryScanLogs.ArriveOption.Equals("3")
                        orderby deliveryRequest.CheckNumber
                        select deliveryRequest.CheckNumber).Distinct().ToList();

            return data;
        }

        public List<string> GetCheckNumberListByActArriveCodeAndDateZone(string station, DateTime start, DateTime end)
        {
            var data = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests where deliveryRequest.CheckNumber != "" && deliveryRequest.AreaArriveCode.Equals(station) && deliveryRequest.ShipDate > start && deliveryRequest.ShipDate < end orderby deliveryRequest.CheckNumber select deliveryRequest.CheckNumber).ToList();

            return data;
        }

        public List<string> GetSuccessCheckNumberListByActArriveCodeAndDateZone(string station, DateTime start, DateTime end)
        {
            var data = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                        join deliveryScanLogs in this.JunFuDbContext.TtDeliveryScanLogs on deliveryRequest.CheckNumber equals deliveryScanLogs.CheckNumber
                        where deliveryRequest.CheckNumber != "" && deliveryRequest.AreaArriveCode.Equals(station) && deliveryRequest.ShipDate > start && deliveryRequest.ShipDate < end
                        && (deliveryScanLogs.ScanItem.Equals("3") || deliveryScanLogs.ScanItem.Equals("4")) && deliveryScanLogs.ArriveOption.Equals("3")
                        orderby deliveryRequest.CheckNumber
                        select deliveryRequest.CheckNumber).Distinct().ToList();

            return data;
        }

        public Dictionary<string, DateTime?> GetSuccessCheckNumberAndTimeListByCustomerCodeAndDateZone(string customerCode, DateTime start, DateTime end)
        {
            var checkNumberList = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                                   where deliveryRequest.CheckNumber != "" && deliveryRequest.LessThanTruckload.Equals(true) && deliveryRequest.CustomerCode.Equals(customerCode) && deliveryRequest.ShipDate > start && deliveryRequest.ShipDate < end
                                   orderby deliveryRequest.CheckNumber
                                   select deliveryRequest.CheckNumber).ToList();

            var data = (from scanLog in this.JunFuDbContext.TtDeliveryScanLogs
                        where checkNumberList.Contains(scanLog.CheckNumber) && (scanLog.ScanItem.Equals("3") || scanLog.ScanItem.Equals("4")) && scanLog.ArriveOption.Equals("3")
                        group scanLog by scanLog.CheckNumber into g
                        select new { CheckNumber = g.Key, ScanDate = g.Min(p => p.ScanDate) }).ToDictionary(p => p.CheckNumber, q => q.ScanDate);

            return data;
        }

        public Dictionary<string, DateTime?> GetLastScanDateListByCustomerCodeAndDateZone(string customerCode, DateTime start, DateTime end)
        {
            var checkNumberList = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                                   where deliveryRequest.CheckNumber != "" && deliveryRequest.LessThanTruckload.Equals(true) && deliveryRequest.CustomerCode.Equals(customerCode) && deliveryRequest.ShipDate > start && deliveryRequest.ShipDate < end
                                   orderby deliveryRequest.CheckNumber
                                   select deliveryRequest.CheckNumber).ToList();

            var data = (from scanLog in this.JunFuDbContext.TtDeliveryScanLogs
                        where checkNumberList.Contains(scanLog.CheckNumber)
                        group scanLog by scanLog.CheckNumber into g
                        select new { CheckNumber = g.Key, ScanDate = g.Max(p => p.ScanDate) }).ToDictionary(p => p.CheckNumber, q => q.ScanDate);

            return data;
        }

        public Dictionary<string, DateTime?> GetSuccessCheckNumberAndTimeListByActArriveCodeAndDateZone(string station, DateTime start, DateTime end)
        {
            var checkNumberList = (station == "-1") ? //全部站所
                                  (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                                   where deliveryRequest.CheckNumber != "" && deliveryRequest.LessThanTruckload.Equals(true) && deliveryRequest.ShipDate > start && deliveryRequest.ShipDate < end
                                   orderby deliveryRequest.CheckNumber
                                   select deliveryRequest.CheckNumber).ToList() :
                                  (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                                   where deliveryRequest.CheckNumber != "" && deliveryRequest.LessThanTruckload.Equals(true) && deliveryRequest.AreaArriveCode.Equals(station) && deliveryRequest.ShipDate > start && deliveryRequest.ShipDate < end
                                   orderby deliveryRequest.CheckNumber
                                   select deliveryRequest.CheckNumber).ToList();

            var data = (from scanLog in this.JunFuDbContext.TtDeliveryScanLogs
                        where checkNumberList.Contains(scanLog.CheckNumber) && (scanLog.ScanItem.Equals("3") || scanLog.ScanItem.Equals("4")) && scanLog.ArriveOption.Equals("3")
                        group scanLog by scanLog.CheckNumber into g
                        select new { CheckNumber = g.Key, ScanDate = g.Min(p => p.ScanDate) }).ToDictionary(p => p.CheckNumber, q => q.ScanDate);

            return data;
        }

        public Dictionary<string, DateTime?> GetLastScanDateListByActArriveCodeAndDateZone(string station, DateTime start, DateTime end)
        {
            var checkNumberList = (station == "-1") ? //全部站所
                                  (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                                   where deliveryRequest.CheckNumber != "" && deliveryRequest.LessThanTruckload.Equals(true) && deliveryRequest.ShipDate > start && deliveryRequest.ShipDate < end
                                   orderby deliveryRequest.CheckNumber
                                   select deliveryRequest.CheckNumber).ToList() :
                                  (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                                   where deliveryRequest.CheckNumber != "" && deliveryRequest.LessThanTruckload.Equals(true) && deliveryRequest.AreaArriveCode.Equals(station) && deliveryRequest.ShipDate > start && deliveryRequest.ShipDate < end
                                   orderby deliveryRequest.CheckNumber
                                   select deliveryRequest.CheckNumber).ToList();

            var data = (from scanLog in this.JunFuDbContext.TtDeliveryScanLogs
                        where checkNumberList.Contains(scanLog.CheckNumber)
                        group scanLog by scanLog.CheckNumber into g
                        select new { CheckNumber = g.Key, ScanDate = g.Max(p => p.ScanDate) }).ToDictionary(p => p.CheckNumber, q => q.ScanDate);

            return data;
        }

        public List<TcDeliveryRequest> GetDeliveryRequestsByCheckNumberList(List<string> checkNumbers)
        {
            var data = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests where checkNumbers.Contains(deliveryRequest.CheckNumber) select deliveryRequest).ToList();

            return data;
        }

        public Dictionary<string, TtDeliveryScanLog> GetLastScanLog(List<string> checkNumbers)
        {
            var logIds = (from scanLog in this.JunFuDbContext.TtDeliveryScanLogs
                          where checkNumbers.Contains(scanLog.CheckNumber) && scanLog.DriverCode != "Z010474"
                          group scanLog by scanLog.CheckNumber into g
                          select g.Max(p => p.LogId)).ToList();

            var data = (from scanLog in this.JunFuDbContext.TtDeliveryScanLogs
                        where logIds.Contains(scanLog.LogId)
                        select scanLog).ToDictionary(p => p.CheckNumber);

            return data;
        }

        public List<TcDeliveryRequest> GetReturnDeliveryCheckNumberLisyBySendStationCodeAndDateZone(string station, DateTime start, DateTime end)
        {
            var data = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests where deliveryRequest.CheckNumber != "" && deliveryRequest.DeliveryType.Equals("R") && deliveryRequest.SendStationScode.Equals(station) && deliveryRequest.Cdate >= start && deliveryRequest.Cdate <= end orderby deliveryRequest.CheckNumber select deliveryRequest).Distinct().ToList();

            return data;
        }

        public List<TcDeliveryRequest> GetReturnDeliveryCheckNumberListByCustomerCodeAndDateZone(string customerCode, DateTime start, DateTime end)
        {
            var data = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests where deliveryRequest.CheckNumber != "" && deliveryRequest.DeliveryType.Equals("R") && deliveryRequest.CustomerCode.Equals(customerCode) && deliveryRequest.Cdate >= start && deliveryRequest.Cdate <= end orderby deliveryRequest.CheckNumber select deliveryRequest).Distinct().ToList();
            return data;
        }

        public List<TcDeliveryRequest> GetAllReturnDeliveryCheckNumberLisyByDateZone(DateTime start, DateTime end)
        {
            var data = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests where deliveryRequest.CheckNumber != "" && deliveryRequest.DeliveryType.Equals("R") && deliveryRequest.Cdate >= start && deliveryRequest.Cdate <= end orderby deliveryRequest.CheckNumber select deliveryRequest).Distinct().ToList();
            return data;
        }
        public List<TcDeliveryRequest> GetErrorDoneDeliveryRequestsByScanDateArea(DateTime startDate, DateTime endDate, string areaName)
        {
            var checkNumberinWriteOff = (from writeoff in JunFuTransDbContext.WriteOffs
                                         where (writeoff.WriteOffCheckListId == 1 || writeoff.WriteOffCheckListId == 2 || writeoff.WriteOffCheckListId == 17)
                                         select writeoff.CheckNumber).ToList();

            var data = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                        where deliveryRequest.LatestScanDate > deliveryRequest.DeliveryCompleteDate && deliveryRequest.LatestScanDate > startDate && deliveryRequest.LatestScanDate < endDate && deliveryRequest.CheckNumber != ""
                        && deliveryRequest.LessThanTruckload.Equals(true)
                        select deliveryRequest.CheckNumber).Distinct().ToList();

            var kickout = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                           join s in JunFuDbContext.TbDrivers on deliveryRequest.LatestArriveOptionDriver equals s.DriverCode
                           where (deliveryRequest.LatestArriveOptionDate > deliveryRequest.DeliveryCompleteDate && deliveryRequest.LatestArriveOption == "3" && deliveryRequest.AreaArriveCode == s.Station)
                           && deliveryRequest.LatestScanDate > startDate && deliveryRequest.LatestScanDate < endDate
                           && deliveryRequest.CheckNumber != ""
                           && deliveryRequest.LessThanTruckload.Equals(true)
                           select deliveryRequest.CheckNumber).Distinct().ToList();

            var answer = data.Except(checkNumberinWriteOff);

            var answerCheckNumber = answer.Except(kickout);

            var area = (from stationarea in this.JunFuTransDbContext.StationAreas where stationarea.Area == areaName select stationarea.StationScode).ToList();

            var output = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                          where deliveryRequest.LatestScanDate > deliveryRequest.DeliveryCompleteDate && deliveryRequest.LatestScanDate > startDate && deliveryRequest.LatestScanDate < endDate && deliveryRequest.CheckNumber != ""
                          && deliveryRequest.LessThanTruckload.Equals(true)
                          && answerCheckNumber.Contains(deliveryRequest.CheckNumber)
                          && area.Contains(deliveryRequest.AreaArriveCode)
                          select deliveryRequest).Distinct().ToList();

            return output;

        }
        public List<TcDeliveryRequest> GetErrorDoneDeliveryRequestsByScanDateStationScode(DateTime startDate, DateTime endDate, string scode)
        {
            var checkNumberinWriteOff = (from writeoff in JunFuTransDbContext.WriteOffs
                                         where (writeoff.WriteOffCheckListId == 1 || writeoff.WriteOffCheckListId == 2 || writeoff.WriteOffCheckListId == 17)
                                         select writeoff.CheckNumber).ToList();

            var data = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                        where deliveryRequest.LatestScanDate > deliveryRequest.DeliveryCompleteDate && deliveryRequest.LatestScanDate > startDate && deliveryRequest.LatestScanDate < endDate && deliveryRequest.CheckNumber != ""
                        && deliveryRequest.LessThanTruckload.Equals(true)
                        select deliveryRequest.CheckNumber).Distinct().ToList();

            var kickout = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                          join s in JunFuDbContext.TbDrivers on deliveryRequest.LatestArriveOptionDriver equals s.DriverCode
                          where (deliveryRequest.LatestArriveOptionDate > deliveryRequest.DeliveryCompleteDate && deliveryRequest.LatestArriveOption == "3" && deliveryRequest.AreaArriveCode == s.Station)
                          && deliveryRequest.LatestScanDate > startDate && deliveryRequest.LatestScanDate < endDate
                          && deliveryRequest.CheckNumber != ""
                          && deliveryRequest.LessThanTruckload.Equals(true)
                          select deliveryRequest.CheckNumber).Distinct().ToList();

            var answer = data.Except(checkNumberinWriteOff);

            var answerCheckNumber = answer.Except(kickout);

            var output = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                          where deliveryRequest.LatestScanDate > deliveryRequest.DeliveryCompleteDate && deliveryRequest.LatestScanDate > startDate && deliveryRequest.LatestScanDate < endDate && deliveryRequest.CheckNumber != ""
                          && deliveryRequest.LessThanTruckload.Equals(true)
                          && answerCheckNumber.Contains(deliveryRequest.CheckNumber)
                          && deliveryRequest.AreaArriveCode == scode
                          select deliveryRequest).Distinct().ToList();

            return output;

        }


        public List<TcDeliveryRequest> GetAllAreaErrorDoneDeliveryRequestsByScanDate(DateTime startDate, DateTime endDate)
        {
            var checkNumberinWriteOff = (from writeoff in JunFuTransDbContext.WriteOffs
                                         where (writeoff.WriteOffCheckListId == 1 || writeoff.WriteOffCheckListId == 2 || writeoff.WriteOffCheckListId == 17)
                                         select writeoff.CheckNumber).ToList();

            var data = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                        where deliveryRequest.LatestScanDate > deliveryRequest.DeliveryCompleteDate && deliveryRequest.LatestScanDate > startDate && deliveryRequest.LatestScanDate < endDate && deliveryRequest.CheckNumber != ""
                        && deliveryRequest.LessThanTruckload.Equals(true)
                        select deliveryRequest.CheckNumber).Distinct().ToList();

            var kickout = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                           join s in JunFuDbContext.TbDrivers on deliveryRequest.LatestArriveOptionDriver equals s.DriverCode
                           where (deliveryRequest.LatestArriveOptionDate > deliveryRequest.DeliveryCompleteDate && deliveryRequest.LatestArriveOption == "3" && deliveryRequest.AreaArriveCode == s.Station)
                           && deliveryRequest.LatestScanDate > startDate && deliveryRequest.LatestScanDate < endDate
                           && deliveryRequest.CheckNumber != ""
                           && deliveryRequest.LessThanTruckload.Equals(true)
                           select deliveryRequest.CheckNumber).Distinct().ToList();

            var answer = data.Except(checkNumberinWriteOff);

            var answerCheckNumber = answer.Except(kickout);

            var area = (from stationarea in this.JunFuTransDbContext.StationAreas select stationarea.StationScode).ToList();

            var output = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                          where deliveryRequest.LatestScanDate > deliveryRequest.DeliveryCompleteDate && deliveryRequest.LatestScanDate > startDate && deliveryRequest.LatestScanDate < endDate && deliveryRequest.CheckNumber != ""
                          && deliveryRequest.LessThanTruckload.Equals(true)
                          && answerCheckNumber.Contains(deliveryRequest.CheckNumber)
                          && area.Contains(deliveryRequest.AreaArriveCode)
                          select deliveryRequest).Distinct().ToList();

            return output;

        }

        public List<TcDeliveryRequest> GetErrorDoneDeliveryRequestsByDriverCode(string driverCode)
        {
            var checkNumberinWriteOff = (from writeoff in JunFuTransDbContext.WriteOffs
                                         where (writeoff.WriteOffCheckListId == 1 || writeoff.WriteOffCheckListId == 2 || writeoff.WriteOffCheckListId == 17)
                                         select writeoff.CheckNumber).ToList();

            var data = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                        join deliveryScanLogs in this.JunFuDbContext.TtDeliveryScanLogs
                        on new { deliveryRequest.CheckNumber, scanDate = deliveryRequest.LatestScanDate } equals
                           new { deliveryScanLogs.CheckNumber, scanDate = deliveryScanLogs.ScanDate }
                        where deliveryRequest.LatestScanDate > deliveryRequest.DeliveryCompleteDate && deliveryScanLogs.DriverCode == driverCode && deliveryScanLogs.DriverCode.Equals(driverCode) && deliveryRequest.CheckNumber != "" && deliveryRequest.LessThanTruckload.Equals(true)
                        select deliveryRequest.CheckNumber).Distinct().ToList();

            var kickout = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                           join s in JunFuDbContext.TbDrivers on deliveryRequest.LatestArriveOptionDriver equals s.DriverCode
                           where (deliveryRequest.LatestArriveOptionDate > deliveryRequest.DeliveryCompleteDate && deliveryRequest.LatestArriveOption == "3" && deliveryRequest.AreaArriveCode == s.Station)
                           && deliveryRequest.LatestArriveOptionDriver == driverCode
                           && deliveryRequest.CheckNumber != ""
                           && deliveryRequest.LessThanTruckload.Equals(true)
                           select deliveryRequest.CheckNumber).Distinct().ToList();

            var answer = data.Except(checkNumberinWriteOff);

            var answerCheckNumber = answer.Except(kickout);

            var output = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                          join deliveryScanLogs in this.JunFuDbContext.TtDeliveryScanLogs
                          on new { deliveryRequest.CheckNumber, scanDate = deliveryRequest.LatestScanDate } equals
                             new { deliveryScanLogs.CheckNumber, scanDate = deliveryScanLogs.ScanDate }
                          where deliveryRequest.LatestScanDate > deliveryRequest.DeliveryCompleteDate && deliveryScanLogs.DriverCode == driverCode && deliveryScanLogs.DriverCode.Equals(driverCode) && deliveryRequest.CheckNumber != "" && deliveryRequest.LessThanTruckload.Equals(true)
                          && answerCheckNumber.Contains(deliveryRequest.CheckNumber)
                          select deliveryRequest).Distinct().ToList();
            return output;
        }
        public Dictionary<string, TbStation> GetAreaArriveCodeStation(List<string> checkNumber)
        {
            var data = from dr in JunFuDbContext.TcDeliveryRequests
                       join t in JunFuDbContext.TbStations on dr.AreaArriveCode equals t.StationScode
                       where checkNumber.Contains(dr.CheckNumber)
                       select t;
            return data.ToDictionary(p => p.StationScode);

        }

        public IEnumerable<TcDeliveryRequest> GetAllRequestBetweenDates(DateTime start, DateTime end)
        {
            return JunFuDbContext.TcDeliveryRequests.Where(r => r.ShipDate > start && r.ShipDate < end.AddDays(1))
                .Where(r => r.LessThanTruckload == true);
        }

        public IEnumerable<TcDeliveryRequest> GetRequestByArriveCodeAndTimePeriod(string stationScode, DateTime start, DateTime end)
        {
            return JunFuDbContext.TcDeliveryRequests
                .Where(r => r.ShipDate > start && r.ShipDate < end.AddDays(1))
                .Where(r => r.AreaArriveCode.Equals(stationScode)).Where(r => r.LessThanTruckload == true);
        }

        public IEnumerable<TcDeliveryRequest> GetRequestsByCustomerCodeAndTimePeriod(string customerCode, DateTime start, DateTime end)
        {
            return JunFuDbContext.TcDeliveryRequests
                .Where(r => r.ShipDate > start && r.ShipDate < end.AddDays(1))
                .Where(r => r.CustomerCode.Equals(customerCode)).Where(r => r.LessThanTruckload == true);
        }

        public IEnumerable<TcDeliveryRequest> GetRequestsBySupplyStationAndTimePeriod(string stationScode, DateTime start, DateTime end)
        {
            return JunFuDbContext.TcDeliveryRequests.Where(r => r.SendStationScode == stationScode)
                .Where(r => r.ShipDate > start && r.ShipDate < end.AddDays(1));
        }

        public List<string> GetCheckNumbersByIds(List<decimal> ids)
        {
            var data = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests where ids.Contains(deliveryRequest.RequestId) select deliveryRequest.CheckNumber).ToList();

            return data;
        }

        public IEnumerable<TcDeliveryRequest> GetNonFinishByCustomerCodeAndTimePeriod(string customerCode, DateTime start, DateTime end)
        {
            return JunFuDbContext.TcDeliveryRequests
                .Where(r => r.DeliveryCompleteDate == null && r.ShipDate > start && r.ShipDate < end && r.IsWriteOff != true && r.LatestDestDate != null)
                .Where(r => r.CustomerCode.Equals(customerCode)).Where(r => r.LessThanTruckload == true && r.CancelDate == null);
        }

        public IEnumerable<TcDeliveryRequest> GetErrorFinishByCustomerCodeAndTimePeriod(string customerCode, DateTime start, DateTime end)
        {
            var kickout = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                           join s in JunFuDbContext.TbDrivers on deliveryRequest.LatestArriveOptionDriver equals s.DriverCode
                           where (deliveryRequest.LatestArriveOptionDate > deliveryRequest.DeliveryCompleteDate && deliveryRequest.LatestArriveOption == "3" && deliveryRequest.AreaArriveCode == s.Station)
                           && deliveryRequest.ShipDate > start && deliveryRequest.ShipDate < end
                           && deliveryRequest.CheckNumber != ""
                           && deliveryRequest.LessThanTruckload.Equals(true)
                           && deliveryRequest.CustomerCode.Equals(customerCode)
                           select deliveryRequest.CheckNumber).Distinct();

            return JunFuDbContext.TcDeliveryRequests
                .Where(r => r.DeliveryCompleteDate < r.LatestScanDate && r.ShipDate > start && r.ShipDate < end && r.IsWriteOff != true && r.LatestDestDate != null && !kickout.Contains(r.CheckNumber))
                .Where(r => r.CustomerCode.Equals(customerCode)).Where(r => r.LessThanTruckload == true).Where(r => r.CancelDate == null);
        }

        public IEnumerable<TcDeliveryRequest> GetNonFinishByActArriveCodeAndTimePeriod(string stationScode, DateTime start, DateTime end)
        {
            var data = (stationScode == "-1") ? JunFuDbContext.TcDeliveryRequests.Where(r => r.DeliveryCompleteDate == null && r.ShipDate > start && r.ShipDate < end && r.LessThanTruckload == true && r.CancelDate == null && r.IsWriteOff != true && r.LatestDestDate != null)
                : JunFuDbContext.TcDeliveryRequests.Where(r => r.DeliveryCompleteDate == null && r.ShipDate > start && r.ShipDate < end && r.AreaArriveCode.Equals(stationScode) && r.LessThanTruckload == true && r.CancelDate == null && r.IsWriteOff != true && r.LatestDestDate != null);

            return data;
        }

        public IEnumerable<TcDeliveryRequest> GetErrorFinishByActArriveCodeAndTimePeriod(string stationScode, DateTime start, DateTime end)
        {
            var kickout = (from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                          join s in JunFuDbContext.TbDrivers on deliveryRequest.LatestArriveOptionDriver equals s.DriverCode
                          where (deliveryRequest.LatestArriveOptionDate > deliveryRequest.DeliveryCompleteDate && deliveryRequest.LatestArriveOption == "3" && deliveryRequest.AreaArriveCode == s.Station)
                          && deliveryRequest.ShipDate > start && deliveryRequest.ShipDate < end
                          && deliveryRequest.CheckNumber != ""
                          && deliveryRequest.LessThanTruckload.Equals(true)
                          && deliveryRequest.AreaArriveCode.Equals(stationScode)
                          select deliveryRequest.CheckNumber).Distinct();

            var data = (stationScode == "-1") ? JunFuDbContext.TcDeliveryRequests.Where(r => r.DeliveryCompleteDate < r.LatestScanDate && r.ShipDate > start && r.ShipDate < end && r.LessThanTruckload == true && r.CancelDate == null && r.IsWriteOff != true && r.LatestDestDate != null && !kickout.Contains(r.CheckNumber))
                : JunFuDbContext.TcDeliveryRequests.Where(r => r.DeliveryCompleteDate < r.LatestScanDate && r.ShipDate > start && r.ShipDate < end && r.AreaArriveCode.Equals(stationScode) && r.LessThanTruckload == true && r.CancelDate == null && r.IsWriteOff != true && r.LatestDestDate != null && !kickout.Contains(r.CheckNumber));

            return data;
        }

        public List<TcDeliveryRequest> GetAllByRequestIds(List<decimal> requestIds)
        {
            var data = (from deliveryRequest in JunFuDbContext.TcDeliveryRequests where requestIds.Contains(deliveryRequest.RequestId) select deliveryRequest).ToList();

            return data;
        }

        /// <summary>
        /// 沒有掃配達的
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public IEnumerable<TcDeliveryRequest> GetNotMatingByShipDatePeriod(DateTime start, DateTime end)
        {
            return JunFuDbContext.TcDeliveryRequests.Where(r => r.ShipDate > start && r.ShipDate < end.AddDays(1) && r.LessThanTruckload == true && r.LatestDeliveryDate == null && r.CancelDate == null);
        }

        public IEnumerable<TcDeliveryRequest> GetNotMatingByStationListAndShipDatePeriod(IEnumerable<string> stationScode, DateTime start, DateTime end)
        {
            return JunFuDbContext.TcDeliveryRequests.Where(r => r.ShipDate > start && r.ShipDate < end.AddDays(1) && r.LessThanTruckload == true 
            && r.LatestDeliveryDate == null && r.CancelDate == null && stationScode.Contains(r.AreaArriveCode));
        }

        public IEnumerable<TcDeliveryRequest> GetNotMatingByCustomerAndShipDatePeriod(string customerCode, DateTime start, DateTime end)
        {
            return JunFuDbContext.TcDeliveryRequests.Where(r => r.ShipDate > start && r.ShipDate < end.AddDays(1) && r.LessThanTruckload == true 
            && r.LatestDeliveryDate == null && r.CancelDate == null && r.CustomerCode == customerCode);
        }

        List<TcDeliveryRequest> IDeliveryRequestRepository.GetDepotDeliveryList(string station, DateTime checkDate, DateTime shipStartDate)
        {
            DateTime now = DateTime.Now;
            DateTime shipEndDate = checkDate.AddHours(-19);

            var data = from deliveryRequest in this.JunFuDbContext.TcDeliveryRequests
                       where deliveryRequest.ShipDate >= shipStartDate
                       && deliveryRequest.ShipDate < shipEndDate
                       && ((!deliveryRequest.LatestScanArriveOption.Equals("3")) || (deliveryRequest.LatestScanArriveOption == null))
                       &&((!deliveryRequest.LatestScanItem.Equals("5")) && (!deliveryRequest.LatestScanItem.Equals("6")) && (!deliveryRequest.LatestScanItem.Equals("7"))) //只抓最新貨態為到著，配送，配達
                       && (deliveryRequest.LatestDestDate != null || deliveryRequest.LatestDeliveryDate != null || deliveryRequest.LatestArriveOptionDate != null)
                       && deliveryRequest.LatestScanDate < now
                       && deliveryRequest.AreaArriveCode.Equals(station)
                       && deliveryRequest.LessThanTruckload.Equals(true)
                       && deliveryRequest.CancelDate == null
                       select deliveryRequest;

            return data.ToList();
        }

        public void CallUpdateProcedure(string customerCode)
        {
            try
            {
                var param = new SqlParameter("@customer_code", customerCode);
                JunFuDbContext.Database.ExecuteSqlCommand("exec customer_shipping_fee_updated @customer_code", param);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public List<TcDeliveryRequest> GetPalletByPrintDate(DateTime printDate)
        {
            return JunFuDbContext.TcDeliveryRequests
                .Where(r => r.LessThanTruckload == false 
                && r.PrintDate >= printDate && r.PrintDate < printDate.AddDays(1)).ToList();
        }

        public List<TcDeliveryRequest> GetByShipDatePeriod(DateTime shipDateStart, DateTime shipDateEnd)
        {
            return JunFuDbContext.TcDeliveryRequests.Where(r => r.ShipDate >= shipDateStart && r.ShipDate < shipDateEnd
            && r.CancelDate == null).ToList();
        }
    }
}
