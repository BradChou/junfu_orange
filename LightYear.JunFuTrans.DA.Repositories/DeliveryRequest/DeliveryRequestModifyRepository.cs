﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;
using System.Linq;
using System.Security.Cryptography;
using LightYear.JunFuTrans.BL.BE.DeliveryRate;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using Microsoft.International.Converters.TraditionalChineseToSimplifiedConverter;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryRequest
{
    public class DeliveryRequestModifyRepository : IDeliveryRequestModifyRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public DeliveryRequestModifyRepository(JunFuDbContext junFuDbContext, JunFuTransDbContext junFuTransDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
            this.JunFuTransDbContext = junFuTransDbContext;
        }

        public TcDeliveryRequest Insert(TcDeliveryRequest tcDeliveryRequest)
        {
            tcDeliveryRequest.Cdate = DateTime.Now;
            tcDeliveryRequest.Udate = DateTime.Now;

            JunFuDbContext.TcDeliveryRequests.Add(tcDeliveryRequest);

            JunFuDbContext.SaveChanges();

            return tcDeliveryRequest;
        }

        public TcDeliveryRequest Update(TcDeliveryRequest tcDeliveryRequest)
        {
            this.JunFuDbContext.TcDeliveryRequests.Attach(tcDeliveryRequest);

            var entry = this.JunFuDbContext.Entry(tcDeliveryRequest);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            entry.Property(x => x.CancelDate).IsModified = false;
            entry.Property(x => x.PrintDate).IsModified = false;
            entry.Property(x => x.Cdate).IsModified = false;
            entry.Property(x => x.SendStationScode).IsModified = false;
            entry.Property(x => x.ReturnCheckNumber).IsModified = false;
            entry.Property(x => x.ShipDate).IsModified = false;
            entry.Property(x => x.DeliveryDate).IsModified = false;
            entry.Property(x => x.DeliveryCompleteDate).IsModified = false;
            entry.Property(x => x.LatestDestDate).IsModified = false;
            entry.Property(x => x.LatestDestDriver).IsModified = false;
            entry.Property(x => x.LatestDeliveryDate).IsModified = false;
            entry.Property(x => x.LatestDeliveryDriver).IsModified = false;
            entry.Property(x => x.SupplierDate).IsModified = false;
            entry.Property(x => x.LatestArriveOptionDate).IsModified = false;
            entry.Property(x => x.LatestArriveOptionDriver).IsModified = false;
            entry.Property(x => x.LatestArriveOption).IsModified = false;
            entry.Property(x => x.LatestScanDate).IsModified = false;
            entry.Property(x => x.LatestScanItem).IsModified = false;
            entry.Property(x => x.LatestScanArriveOption).IsModified = false;
            entry.Property(x => x.LatestScanDriverCode).IsModified = false;
            entry.Property(x => x.AreaArriveCode).IsModified = false;

            this.JunFuDbContext.SaveChanges();

            return tcDeliveryRequest;
        }

        public List<TcDeliveryRequest> SaveList(List<TcDeliveryRequest> tcDeliveryRequests)
        {
            throw new NotImplementedException();
        }

        public TcDeliveryRequest SelectById(long requestId)
        {
            var data = from TcDeliveryRequest in this.JunFuDbContext.TcDeliveryRequests where TcDeliveryRequest.RequestId.Equals(requestId) select TcDeliveryRequest;

            if(data.Count() == 0)
            {
                return null;
            }

            return data.FirstOrDefault();
        }

        public List<TcDeliveryRequest> SelectByCheckNumber(string checkNumber)
        {
            var data = from TcDeliveryRequest in this.JunFuDbContext.TcDeliveryRequests where TcDeliveryRequest.CheckNumber.Equals(checkNumber) select TcDeliveryRequest;

            if (data.Count() == 0)
            {
                return null;
            }

            return data.ToList();
        }

        public List<TcDeliveryRequest> SelectByCheckNumbers(List<string> checkNumbers)
        {
            var data = from TcDeliveryRequest in this.JunFuDbContext.TcDeliveryRequests where checkNumbers.Contains(TcDeliveryRequest.CheckNumber) select TcDeliveryRequest;

            if (data.Count() == 0)
            {
                return null;
            }

            return data.ToList();
        }

        /// <summary>
        /// 使用預存程序切出地址
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public LyAddrFormatReturnModel GetAddressCityInfo(string address)
        {
            string realAddress = ChineseConverter.Convert(address, ChineseConversionDirection.SimplifiedToTraditional);

            var returnInfo = this.JunFuDbContext.LyAddrFormat(realAddress);

            if (returnInfo != null && returnInfo.Count > 0)
            {
                return returnInfo[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 查詢零擔配送商
        /// </summary>
        /// <param name="city"></param>
        /// <param name="area"></param>
        /// <returns></returns>
        public LyGetDistributorReturnModel GetDistributor(string city, string area)
        {
            var returnInfo = this.JunFuDbContext.LyGetDistributor(city, area);

            if( returnInfo != null && returnInfo.Count > 0 )
            {
                return returnInfo[0];
            }
            else
            {
                return null;
            }
        }

        public LyGetLtShipFeeByRequestIdReturnModel GetShipFee(long requestId)
        {
            var returnInfo = this.JunFuDbContext.LyGetLtShipFeeByRequestId(requestId);

            if (returnInfo != null && returnInfo.Count > 0)
            {
                return returnInfo[0];
            }
            else
            {
                return null;
            }
        }

        public TcDeliveryRequest GetTcDeliveryRequestByCustomerCodeAndOrderNumber(string customerCode, string orderNumber)
        {
            var data = from TcDeliveryRequest in this.JunFuDbContext.TcDeliveryRequests where TcDeliveryRequest.OrderNumber.Equals(orderNumber) && TcDeliveryRequest.CustomerCode.Equals(customerCode) && TcDeliveryRequest.CheckNumber != null && TcDeliveryRequest.CheckNumber != "" select TcDeliveryRequest;

            if (data.Count() == 0)
            {
                return null;
            }

            return data.FirstOrDefault();
        }

    }
}
