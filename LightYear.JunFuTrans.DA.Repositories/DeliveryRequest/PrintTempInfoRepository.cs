﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryRequest
{
    public class PrintTempInfoRepository : IPrintTempInfoRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public PrintTempInfoRepository(JunFuTransDbContext junFuTransDbContext)
        {
            this.JunFuTransDbContext = junFuTransDbContext;
        }

        public void Delete(string infoKey)
        {
            var data = (from printTemp in this.JunFuTransDbContext.PrintTempInfoes where printTemp.InfoKey.Equals(infoKey) select printTemp).FirstOrDefault();

            if( data != null )
            {
                this.JunFuTransDbContext.PrintTempInfoes.Remove(data);

                this.JunFuTransDbContext.SaveChanges();
            }

        }

        public void Insert(PrintTempInfo printTempInfo)
        {
            this.JunFuTransDbContext.PrintTempInfoes.Add(printTempInfo);

            this.JunFuTransDbContext.SaveChanges();
        }

        public PrintTempInfo GetByInfoKey(string infoKey)
        {
            var data = (from printTemp in this.JunFuTransDbContext.PrintTempInfoes where printTemp.InfoKey.Equals(infoKey) select printTemp).FirstOrDefault();

            return data;
        }
    }
}
