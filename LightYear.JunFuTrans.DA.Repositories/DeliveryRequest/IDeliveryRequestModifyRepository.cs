﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryRequest
{
    public interface IDeliveryRequestModifyRepository
    {
        TcDeliveryRequest Insert(TcDeliveryRequest tcDeliveryRequest);

        TcDeliveryRequest Update(TcDeliveryRequest tcDeliveryRequest);

        List<TcDeliveryRequest> SaveList(List<TcDeliveryRequest> tcDeliveryRequests);

        TcDeliveryRequest SelectById(long requestId);

        List<TcDeliveryRequest> SelectByCheckNumber(string checkNumber);

        LyAddrFormatReturnModel GetAddressCityInfo(string address);

        LyGetDistributorReturnModel GetDistributor(string city, string area);

        LyGetLtShipFeeByRequestIdReturnModel GetShipFee(long requestId);

        List<TcDeliveryRequest> SelectByCheckNumbers(List<string> checkNumbers);

        /// <summary>
        /// 以客戶代碼及訂單編號查詢資料
        /// </summary>
        /// <param name="customerCode">客戶代碼</param>
        /// <param name="orderNumber">訂單編號</param>
        /// <returns>託運資料</returns>
        TcDeliveryRequest GetTcDeliveryRequestByCustomerCodeAndOrderNumber(string customerCode, string orderNumber);
    }
}
