﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryRequest
{
    public interface ICheckNumberPreheadRepository
    {
        List<CheckNumberPrehead> Get();

        string GetPrefixByProductType(string productType);
    }
}
