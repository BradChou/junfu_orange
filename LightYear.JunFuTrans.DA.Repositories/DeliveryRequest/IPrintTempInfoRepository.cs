﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryRequest
{
    public interface IPrintTempInfoRepository
    {

        void Insert(PrintTempInfo printTempInfo);

        PrintTempInfo GetByInfoKey(string infoKey);

        void Delete(string infoKey);
    }
}
