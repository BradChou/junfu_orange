﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryRequest
{
    public class CustomizeAreaArriveCodeRepository : ICustomizeAreaArriveCodeRepository
    {
        public JunFuDbContext DbContext { get; set; }

        public CustomizeAreaArriveCodeRepository(JunFuDbContext junFuDbContext)
        {
            DbContext = junFuDbContext;
        }

        public List<CustomizeAreaArriveCode> GetAll()
        {
            return DbContext.CustomizeAreaArriveCodes.ToList();
        }
    }
}
