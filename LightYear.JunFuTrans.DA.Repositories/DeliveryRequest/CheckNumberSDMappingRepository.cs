﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryRequest
{
    public class CheckNumberSDMappingRepository : ICheckNumberSDMappingRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public CheckNumberSDMappingRepository(JunFuTransDbContext junFuTransDbContext)
        {
            this.JunFuTransDbContext = junFuTransDbContext;
        }

        public long Insert(CheckNumberSdMapping checkNumberSdMapping)
        {
            checkNumberSdMapping.Cdate = DateTime.Now;

            checkNumberSdMapping.Udate = DateTime.Now;

            this.JunFuTransDbContext.CheckNumberSdMappings.Add(checkNumberSdMapping);

            this.JunFuTransDbContext.SaveChanges();

            return checkNumberSdMapping.Id;
        }

        public void InsertMutiData(List<CheckNumberSdMapping> checkNumberSdMappings)
        {
            foreach(CheckNumberSdMapping checkNumberSdMapping in checkNumberSdMappings)
            {
                checkNumberSdMapping.Cdate = DateTime.Now;

                checkNumberSdMapping.Udate = DateTime.Now;

                this.JunFuTransDbContext.CheckNumberSdMappings.Add(checkNumberSdMapping);
            }

            this.JunFuTransDbContext.SaveChanges();
        }

        public void Update(CheckNumberSdMapping checkNumberSdMapping)
        {
            checkNumberSdMapping.Udate = DateTime.Now;

            this.JunFuTransDbContext.CheckNumberSdMappings.Attach(checkNumberSdMapping);

            var entry = this.JunFuTransDbContext.Entry(checkNumberSdMapping);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            entry.Property(x => x.CheckNumber).IsModified = false;
            entry.Property(x => x.Cdate).IsModified = false;
            entry.Property(x => x.RequestId).IsModified = false;

            this.JunFuTransDbContext.SaveChanges();
        }

        public Dictionary<long, CheckNumberSdMapping> CheckExistByRequestIds(List<decimal> requestIds)
        {
            var data = (from checkNumberSDMapping in this.JunFuTransDbContext.CheckNumberSdMappings where requestIds.Contains(checkNumberSDMapping.RequestId) select checkNumberSDMapping).ToLookup(p => p.RequestId).ToDictionary(a => a.Key, a => a.First());

            return data;
        }
    }
}
