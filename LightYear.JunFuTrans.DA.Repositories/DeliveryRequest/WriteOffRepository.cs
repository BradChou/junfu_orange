﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.PickUpAndDeliveryMating;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryRequest
{
    public class WriteOffRepository : IWriteOffRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public WriteOffRepository(JunFuTransDbContext junFuTransDbContext)
        {
            this.JunFuTransDbContext = junFuTransDbContext;
        }

        public WriteOff GetWriteOff(string checkNumber)
        {
            var data = this.JunFuTransDbContext.WriteOffs.Find(checkNumber);

            return data;
        }
        public WriteOff GetWriteOffNormal(string checkNumber)
        {
            var data = from writeoff in JunFuTransDbContext.WriteOffs
                        where (writeoff.WriteOffCheckListId == 1 || writeoff.WriteOffCheckListId == 2 || writeoff.WriteOffCheckListId == 17)
                       & writeoff.CheckNumber == checkNumber
                       select writeoff;

            List<WriteOff> writeOffs = data.ToList();

            if (writeOffs.Count > 0)
            {
                return writeOffs[0];
            }
            else
            {
                return null;
            }
        }

        public Dictionary<string, WriteOff> GetWriteOffs(List<string> checkNumbers)
        {
            var data = (from writeOff in this.JunFuTransDbContext.WriteOffs where checkNumbers.Contains(writeOff.CheckNumber) select writeOff).ToDictionary(p => p.CheckNumber);

            return data;
        }

        public Dictionary<string, WriteOff> GetWriteOffsNormal(List<string> checkNumbers)
        {
            var data = (from writeOff in this.JunFuTransDbContext.WriteOffs where checkNumbers.Contains(writeOff.CheckNumber)
                        & (writeOff.WriteOffCheckListId == 1 || writeOff.WriteOffCheckListId == 2 || writeOff.WriteOffCheckListId == 17)
                        select writeOff).ToDictionary(p => p.CheckNumber);

            return data;
        }
        public List<WriteOff> GetWriteOffList(List<string> checkNumbers)
        {
            var data = (from writeOff in this.JunFuTransDbContext.WriteOffs where checkNumbers.Contains(writeOff.CheckNumber) select writeOff).ToList();

            return data;
        }

        public void Insert(WriteOff writeOff)
        {
            writeOff.Cdate = DateTime.Now;

            writeOff.Udate = DateTime.Now;

            this.JunFuTransDbContext.WriteOffs.Add(writeOff);

            this.JunFuTransDbContext.SaveChanges();
        }

        public void Update(WriteOff writeOff)
        {
            writeOff.Udate = DateTime.Now;

            this.JunFuTransDbContext.WriteOffs.Attach(writeOff);

            var entry = this.JunFuTransDbContext.Entry(writeOff);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            this.JunFuTransDbContext.SaveChanges();
        }

        public bool isClosedNormally(string checkNumber)
        {
            bool result = false;
            WriteOff writeOff = GetWriteOff(checkNumber);
            if (writeOff != null)
            {
                int? writeOffCheckListId = writeOff.WriteOffCheckListId;
                result = (writeOffCheckListId == 1 || writeOffCheckListId == 2 || writeOffCheckListId == 17);
            }
            return result;
        }
    }
}
