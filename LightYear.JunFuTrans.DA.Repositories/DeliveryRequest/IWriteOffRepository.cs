﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryRequest
{
    public interface IWriteOffRepository
    {
        void Insert(WriteOff writeOff);

        void Update(WriteOff writeOff);

        WriteOff GetWriteOff(string checkNumber);

        Dictionary<string, WriteOff> GetWriteOffs(List<string> checkNumbers);

        Dictionary<string, WriteOff> GetWriteOffsNormal(List<string> checkNumbers);

        List<WriteOff> GetWriteOffList(List<string> checkNumbers);

        WriteOff GetWriteOffNormal(string checkNumber);

        bool isClosedNormally(string checkNumber);

    }
}
