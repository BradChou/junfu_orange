﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.Api;
using LightYear.JunFuTrans.BL.BE.DriverToStationPayment;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.BL.BE.SignPaperPhoto;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryRequest
{
    public interface IDeliveryScanLogRepository
    {
        List<TtDeliveryScanLog> GetByCheckNumber(string checkNumber);

        IEnumerable<TtDeliveryScanLog> GetByCheckNumbers(IEnumerable<string> checkNumber);

        IEnumerable<string> RemoveNotPickUpCheckNumber(IEnumerable<string> checkNumbers);

        //List<string> GetDriverTodayDeliverySuccessNum(string driverCode);

        List<string> GetDriverDeliverySuccessByDriverAfterTime(string driverCode, DateTime time);

        List<TtDeliveryScanLog> GetDeliveryScanLogsByScanItem(List<string> checkNumbers, string scanItem);

        IEnumerable<TtDeliveryScanLog> GetDeliverySuccessByDriversAndDate(IEnumerable<string> drivers, DateTime date);

        SignPaperPhotoAndDateTimeAndType CatchLatestScanNormalDeliveryTime(string checkNumber);

        List<ApiScanLogEntity> GetApiScanLogByCustomerCodeAndTimeZone(string customerCode, DateTime start, DateTime end, bool isCheckNumberNull = true);

        Dictionary<string, string> GetItemCode(string codeSClass);

        List<TtDeliveryScanLog> GetLastPickUpScan(string customerCode, DateTime start);

        List<TcDeliveryRequest> GetLastNotPickUpScan(string customerCode, DateTime start);

        Dictionary<string, TcDeliveryRequest> GetByCheckNumberList(IEnumerable<string> checkNumbers);

        List<TcDeliveryRequest> GetMeasuredInfo(string customerCode, DateTime start, DateTime end);

        List<TbItemCode> GetReceiveOptionCode();

    }
}
