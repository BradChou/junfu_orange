﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using LightYear.JunFuTrans.BL.BE.DriverToStationPayment;
using System.Security.Cryptography;
using LightYear.JunFuTrans.BL.BE.Api;
using LightYear.JunFuTrans.BL.BE.SignPaperPhoto;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryRequest
{
    public class DeliveryScanLogRepository : IDeliveryScanLogRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }

        public DeliveryScanLogRepository(JunFuDbContext junFuDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
        }

        public List<TtDeliveryScanLog> GetByCheckNumber(string checkNumber)
        {
            var data = from ttDeliveryScanLog in this.JunFuDbContext.TtDeliveryScanLogs where ttDeliveryScanLog.CheckNumber.Equals(checkNumber) orderby ttDeliveryScanLog.ScanDate descending select ttDeliveryScanLog;

            List<TtDeliveryScanLog> deliveryScanLogs = data.ToList();

            return deliveryScanLogs;
        }

        public List<string> GetDriverDeliverySuccessByDriverAfterTime(string driverCode, DateTime time)
        {
            var temp = (from sl in JunFuDbContext.TtDeliveryScanLogs
                        where sl.DriverCode == driverCode && sl.ScanDate > time && sl.ScanItem == "3" && !sl.CheckNumber.StartsWith("102") && !sl.CheckNumber.StartsWith("99")
                        select new { CheckNumber = sl.CheckNumber, ArriveOption = sl.ArriveOption, ScanDate = sl.ScanDate });

            // 取最新掃讀狀態
            var latestScan = from t in temp
                             group t by t.CheckNumber into g
                             select new { CheckNumber = g.Key, ScanDate = g.Max(s => s.ScanDate) };

            var latestScanArriveOption = from l in latestScan
                                         join t in temp on new { l.CheckNumber, l.ScanDate } equals new { t.CheckNumber, t.ScanDate }
                                         select new { CheckNumber = l.CheckNumber, ScanDate = l.ScanDate, ArriveOption = t.ArriveOption };

            return (from j in latestScanArriveOption where j.ArriveOption == "3" select j.CheckNumber).Distinct().ToList();
        }

        public List<TtDeliveryScanLog> GetDeliveryScanLogsByScanItem(List<string> checkNumbers, string scanItem)
        {
            var data = (from deliveryScanLog in JunFuDbContext.TtDeliveryScanLogs where checkNumbers.Contains(deliveryScanLog.CheckNumber) && deliveryScanLog.ScanItem.Equals(scanItem) orderby deliveryScanLog.ScanDate descending select deliveryScanLog);

            return data.ToList();
        }

        public IEnumerable<TtDeliveryScanLog> GetByCheckNumbers(IEnumerable<string> checkNumber)
        {
            return JunFuDbContext.TtDeliveryScanLogs.Where(s => checkNumber.Contains(s.CheckNumber));
        }

        public IEnumerable<string> RemoveNotPickUpCheckNumber(IEnumerable<string> checkNumbers)
        {
            return JunFuDbContext.TtDeliveryScanLogs.Where(s => checkNumbers.Contains(s.CheckNumber))
                .Where(s => s.ScanItem == "5").Select(s => s.CheckNumber).Distinct();
        }

        public IEnumerable<TtDeliveryScanLog> GetDeliverySuccessByDriversAndDate(IEnumerable<string> drivers, DateTime date)
        {
            return JunFuDbContext.TtDeliveryScanLogs.Where(s => drivers.Contains(s.DriverCode) && s.ScanDate >= date && s.ScanDate < date.AddDays(1)
                && s.ScanItem == "3" && s.ArriveOption == "3" && !s.CheckNumber.StartsWith("99") && !s.CheckNumber.StartsWith("102"));
        }

        public SignPaperPhotoAndDateTimeAndType CatchLatestScanNormalDeliveryTime(string checkNumber)
        {
            var destData = JunFuDbContext.TtDeliveryScanLogs.Where(s => s.CheckNumber == checkNumber && s.ArriveOption == "3");
            if (destData.Where(s => s.ScanItem == "4").Count() > 0)
            {
                var result = new SignPaperPhotoAndDateTimeAndType
                {
                    CheckNumber = checkNumber,
                    ScanDate = destData.Where(s => s.ScanItem == "4").Max(s => s.Cdate).GetValueOrDefault(),
                    SignPaperPhotoPath = destData.Where(s => s.ScanItem == "4").FirstOrDefault().SignFormImage
                };
                return result;
            }
            else
            {
                var result = new SignPaperPhotoAndDateTimeAndType
                {
                    CheckNumber = checkNumber,
                    ScanDate = destData.Where(s => s.ScanItem == "3").Max(s => s.Cdate).GetValueOrDefault(),
                    SignPaperPhotoPath = destData.Where(s => s.ScanItem == "3").FirstOrDefault().SignFormImage
                };
                return result;
            }
        }

        public List<ApiScanLogEntity> GetApiScanLogByCustomerCodeAndTimeZone(string customerCode, DateTime start, DateTime end, bool isCheckNumberNull = true)
        {
            if( isCheckNumberNull )
            {

                var data = from scan in JunFuDbContext.TtDeliveryScanLogs
                           join request in JunFuDbContext.TcDeliveryRequests on scan.CheckNumber equals request.CheckNumber
                           where request.CustomerCode.Equals(customerCode) && scan.ScanDate > start && scan.ScanDate < end && request.ShipDate != null
                           orderby scan.ScanDate
                           select new ApiScanLogEntity { checkNumber = scan.CheckNumber, scanDate = (DateTime)scan.ScanDate, deliveryType = request.DeliveryType, scanName = scan.ScanItem, supplierName = request.SupplierName, status = string.Empty, itemCodes = scan.ArriveOption, driverCode = scan.DriverCode, driverName = string.Empty, stationCode = request.AreaArriveCode, stationName = string.Empty, returnCheckNumber = request.ReturnCheckNumber };

                return data.ToList();
            }
            else
            {
                var data = from scan in JunFuDbContext.TtDeliveryScanLogs
                           join request in JunFuDbContext.TcDeliveryRequests on scan.CheckNumber equals request.CheckNumber
                           where request.CustomerCode.Equals(customerCode) && scan.ScanDate > start && scan.ScanDate < end && request.ShipDate != null && !request.CheckNumber.Equals(string.Empty)
                           orderby scan.ScanDate
                           select new ApiScanLogEntity { checkNumber = scan.CheckNumber, scanDate = (DateTime)scan.ScanDate, deliveryType = request.DeliveryType, scanName = scan.ScanItem, supplierName = request.SupplierName, status = string.Empty, itemCodes = scan.ArriveOption, driverCode = scan.DriverCode, driverName = string.Empty, stationCode = request.AreaArriveCode, stationName = string.Empty, returnCheckNumber = request.ReturnCheckNumber };

                return data.ToList();
            }
        }

        public Dictionary<string, string> GetItemCode(string codeSClass)
        {
            var data = from itemCode in JunFuDbContext.TbItemCodes
                       where itemCode.CodeSclass.Equals(codeSClass) && itemCode.ActiveFlag.Equals(true)
                       select itemCode;

            return data.ToDictionary(p => p.CodeId, p => p.CodeName);
        }

        public List<TtDeliveryScanLog> GetLastPickUpScan(string customerCode, DateTime start)
        {
            var data = from scan in JunFuDbContext.TtDeliveryScanLogs
                       join request in JunFuDbContext.TcDeliveryRequests on scan.CheckNumber equals request.CheckNumber
                       where request.CustomerCode.Equals(customerCode) && scan.ScanItem.Equals("5") && scan.ScanDate > start
                       orderby scan.ScanDate
                       select scan;

            return data.ToList();
        }

        public List<TcDeliveryRequest> GetLastNotPickUpScan(string customerCode, DateTime start)
        {
            var data = from request in JunFuDbContext.TcDeliveryRequests
                       where request.LatestScanDate == null && request.CustomerCode.Equals(customerCode) && request.Cdate > start
                       orderby request.Cdate
                       select request;

            return data.ToList();
        }

        public Dictionary<string, TcDeliveryRequest> GetByCheckNumberList(IEnumerable<string> checkNumbers)
        {
            return JunFuDbContext.TcDeliveryRequests.Where(r => checkNumbers.Contains(r.CheckNumber)).ToLookup(p => p.CheckNumber).ToDictionary(p => p.Key.ToUpper(), p => p.Last());
        }

        public List<TcDeliveryRequest> GetMeasuredInfo(string customerCode, DateTime start, DateTime end)
        {
            var data = (from scan in JunFuDbContext.TtDeliveryScanLogs
                       join request in JunFuDbContext.TcDeliveryRequests on scan.CheckNumber equals request.CheckNumber
                       where request.CustomerCode.Equals(customerCode) && (scan.ScanItem.Equals("7") || (request.ShipDate != null && scan.ScanItem == "5")) && scan.ScanDate > start && scan.ScanDate <= end && request.CheckNumber.Length > 0
                        orderby request.LatestScanDate
                       select  request).Distinct();

            return data.ToList();
        }

        public List<TbItemCode> GetReceiveOptionCode()
        {
            var data = from codes in JunFuDbContext.TbItemCodes
                       where codes.CodeBclass.Equals("5") && codes.CodeSclass.Equals("RO") && codes.ActiveFlag.Equals(true)
                       orderby codes.CodeId
                       select codes;

            return data.ToList();
        }

        enum ScanItem
        {
            // 掃描項目 1.到著2.配送3.4.配達5.集貨6.卸集7.發送
            Destination = 1,
            Delivery = 2,
            DeliveryMating = 3,
            PickUp = 5,
            DropOff = 6
        }

        enum ArriveOption
        {
            // 配達項目 1.客戶不在2.約定再配3.正常配交4.拒收5.地址錯誤6.查無此人
            NotAtHome = 1,
            MaybeTomorrow = 2,
            DeliverySuccess = 3,
            DeliveryDeny = 4,
            WrongAddress = 5,
            WrongPerson = 6
        }
    }
}
