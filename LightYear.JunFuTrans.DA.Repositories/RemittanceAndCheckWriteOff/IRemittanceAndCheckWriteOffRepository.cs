﻿using LightYear.JunFuTrans.BL.BE.RemittanceAndCheckWriteOff;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.RemittanceAndCheckWriteOff
{
    public interface IRemittanceAndCheckWriteOffRepository
    {
        List<RemittanceAndCheckWriteOffEntity> GetAllByDate(DateTime start, DateTime end);
        List<RemittanceAndCheckWriteOffEntity> GetAllByDateAndStation(DateTime start, DateTime end, string scode);
        void InsertPaymentWriteOffByRequestId(List<int> requestId);
        void UpdatePaymentWriteOffByRequestId(List<int> requestId, int writeoff);
        string GetLastFiveNumberByCheckNumber(string checkNumber);
        bool? IsPaymentWriteOffOrNot(decimal requestId);
        List<PaymentWriteOff> GetPaymentWriteOffByRequestId(List<int> requestId);
        DateTime? GetWriteOffDateByRequestId(decimal requestId);

    }
}
