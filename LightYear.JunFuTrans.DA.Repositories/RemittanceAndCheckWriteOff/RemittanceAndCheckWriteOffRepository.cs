﻿using LightYear.JunFuTrans.BL.BE.RemittanceAndCheckWriteOff;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.RemittanceAndCheckWriteOff
{
    public class RemittanceAndCheckWriteOffRepository : IRemittanceAndCheckWriteOffRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }
        public JunFuDbContext JunFuDbContext { get; private set; }

        public RemittanceAndCheckWriteOffRepository(JunFuTransDbContext junFuTransDbContext, JunFuDbContext junFuDbContext)
        {
            JunFuTransDbContext = junFuTransDbContext;
            JunFuDbContext = junFuDbContext;
        }

        public void InsertPaymentWriteOffByRequestId(List<int> requestId)
        {
            for (int i = 0; i < requestId.Count; i++)
            {
                PaymentWriteOff paymentWriteOff = new PaymentWriteOff
                {
                    RequestId = requestId[i],
                    IsWriteOff = true,
                    WriteOffDate = DateTime.Now
                };

                this.JunFuTransDbContext.PaymentWriteOffs.Add(paymentWriteOff);

                this.JunFuTransDbContext.SaveChanges();
            }
        }

        public void UpdatePaymentWriteOffByRequestId(List<int> requestId , int writeoff)
        {
            List<PaymentWriteOff> paymentWriteOffs = GetPaymentWriteOffByRequestId(requestId);

            foreach (PaymentWriteOff paymentWriteOff in paymentWriteOffs)
            {
                paymentWriteOff.WriteOffDate = DateTime.Now;

                if (writeoff == 1)
                {
                    paymentWriteOff.IsWriteOff = true;
                }
                else
                {
                    paymentWriteOff.IsWriteOff = false;
                }

                this.JunFuTransDbContext.PaymentWriteOffs.Attach(paymentWriteOff);

                var entry = this.JunFuTransDbContext.Entry(paymentWriteOff);

                entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

                this.JunFuTransDbContext.SaveChanges();
            }
                
        }

        public List<PaymentWriteOff> GetPaymentWriteOffByRequestId(List<int> requestId)
        {
            List<decimal> ids = new List<decimal>();

            for (int i = 0; i < requestId.Count; i++)
            {
                decimal id = Convert.ToDecimal(requestId[i]);
                ids.Add(id);
            }
            var data = JunFuTransDbContext.PaymentWriteOffs.Where(s => ids.Contains(s.RequestId));
            return data.ToList();
        }

        public List<RemittanceAndCheckWriteOffEntity> GetAllByDate(DateTime start, DateTime end)
        {
            var data = (from dr in JunFuDbContext.TcDeliveryRequests
                       join tt in JunFuDbContext.TbDrivers on dr.LatestArriveOptionDriver equals tt.DriverCode
                       join tb in JunFuDbContext.TbStations on tt.Station equals tb.StationScode
                       orderby dr.LatestArriveOptionDate ascending
                       where (dr.InvoiceDesc == "支票" || dr.InvoiceDesc == "匯款" || dr.PaymentMethod == "支票" || dr.PaymentMethod == "匯款")
                       && dr.LatestArriveOptionDate > start && dr.LatestArriveOptionDate <= end.AddDays(1)
                       select new RemittanceAndCheckWriteOffEntity
                       {
                           RequestId = dr.RequestId,
                           Station = tt.Station + tb.StationName,
                           PaymentMethod = dr.InvoiceDesc ?? dr.PaymentMethod,
                           CheckNumber = dr.CheckNumber,
                           Payment = dr.CollectionMoney,
                           CustomerCodeAndName =  dr.CustomerCode + dr.SendContact,
                           LatestArriveOptionDate = dr.LatestArriveOptionDate,
                       }).ToList();

            return data;
        }

        public List<RemittanceAndCheckWriteOffEntity> GetAllByDateAndStation(DateTime start, DateTime end, string scode)
        {
            var data = (from dr in JunFuDbContext.TcDeliveryRequests
                       join tt in JunFuDbContext.TbDrivers on dr.LatestArriveOptionDriver equals tt.DriverCode
                       join tb in JunFuDbContext.TbStations on tt.Station equals tb.StationScode
                       orderby dr.LatestArriveOptionDate ascending
                       where (dr.InvoiceDesc == "支票" || dr.InvoiceDesc == "匯款" || dr.PaymentMethod == "支票" || dr.PaymentMethod == "匯款")
                       && dr.LatestArriveOptionDate > start && dr.LatestArriveOptionDate <= end.AddDays(1)
                       && tt.Station == scode
                       select new RemittanceAndCheckWriteOffEntity
                       {
                           RequestId = dr.RequestId,
                           Station = tt.Station + tb.StationName,
                           PaymentMethod = dr.InvoiceDesc ?? dr.PaymentMethod,
                           CheckNumber = dr.CheckNumber,
                           Payment = dr.CollectionMoney,
                           CustomerCodeAndName = dr.CustomerCode + dr.SendContact,
                           LatestArriveOptionDate = dr.LatestArriveOptionDate,
                       }).ToList();

            return data;
        }

        public bool? IsPaymentWriteOffOrNot(decimal requestId)
        {
            var data = (from dp in JunFuTransDbContext.PaymentWriteOffs
                        where dp.RequestId == requestId
                        select dp.IsWriteOff).ToList();

            return data.FirstOrDefault();
        }

        public string GetLastFiveNumberByCheckNumber(string checkNumber)
        {
            var data = (from dp in JunFuTransDbContext.DriverPaymentDetails
                        where checkNumber == dp.CheckNumber
                        select dp.TicketOrRemittanceLastFive);
            return data.FirstOrDefault();
        }

        public DateTime? GetWriteOffDateByRequestId(decimal requestId)
        {
            var data = (from dp in JunFuTransDbContext.PaymentWriteOffs
                        where dp.RequestId == requestId
                        select dp.WriteOffDate);
            return data.FirstOrDefault();
        }

    }
}
