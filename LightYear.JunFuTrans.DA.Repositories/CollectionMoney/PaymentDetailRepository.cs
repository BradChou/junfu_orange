﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.DA.Repositories.CollectionMoney
{
    public class PaymentDetailRepository : IPaymentDetailRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public PaymentDetailRepository(JunFuTransDbContext junFuTransDbContext)
        {
            this.JunFuTransDbContext = junFuTransDbContext;
        }

        public void Delete(int id)
        {
            var paymentDetail = this.JunFuTransDbContext.PaymentDetails.Find(id);

            if (paymentDetail != null)
            {
                //執行假刪除
                paymentDetail.UpdateDate = DateTime.Now;

                paymentDetail.IsDelete = true;

                this.JunFuTransDbContext.PaymentDetails.Attach(paymentDetail);

                var entry = this.JunFuTransDbContext.Entry(paymentDetail);

                entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

                this.JunFuTransDbContext.SaveChanges();
            }
        }

        public List<PaymentDetail> GetByBarcode(string barcode)
        {
            var data = from paymentDetail in this.JunFuTransDbContext.PaymentDetails where paymentDetail.Barcode.Equals(barcode) && !paymentDetail.IsDelete select paymentDetail;

            return data.ToList();
        }

        public PaymentDetail GetById(int id)
        {
            var paymentDetail = this.JunFuTransDbContext.PaymentDetails.Find(id);

            return paymentDetail;
        }

        public List<PaymentDetail> GetByPaymentId(int id)
        {
            var data = from paymentDetail in this.JunFuTransDbContext.PaymentDetails where paymentDetail.PaymentId.Equals(id) && !paymentDetail.IsDelete select paymentDetail;

            return data.ToList();
        }

        public int Insert(PaymentDetail paymentDetail)
        {
            paymentDetail.CreateDate = DateTime.Now;

            paymentDetail.UpdateDate = DateTime.Now;

            this.JunFuTransDbContext.PaymentDetails.Add(paymentDetail);

            this.JunFuTransDbContext.SaveChanges();

            return paymentDetail.PaymentDetailId;
        }

        public void Update(PaymentDetail paymentDetail)
        {
            paymentDetail.UpdateDate = DateTime.Now;

            this.JunFuTransDbContext.PaymentDetails.Add(paymentDetail);

            var entry = this.JunFuTransDbContext.Entry(paymentDetail);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            this.JunFuTransDbContext.SaveChanges();
        }
    }
}
