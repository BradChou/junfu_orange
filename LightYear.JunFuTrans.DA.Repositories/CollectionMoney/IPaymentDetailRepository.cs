﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuTransDb;
namespace LightYear.JunFuTrans.DA.Repositories.CollectionMoney
{
    public interface IPaymentDetailRepository
    {
        int Insert(PaymentDetail paymentDetail);

        void Update(PaymentDetail paymentDetail);

        void Delete(int id);

        PaymentDetail GetById(int id);

        List<PaymentDetail> GetByPaymentId(int id);

        List<PaymentDetail> GetByBarcode(string barcode);

    }
}
