﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.DA.Repositories.CollectionMoney
{
    public interface IPaymentRepository
    {
        int Insert(Payment payment);

        void Update(Payment payment);

        List<Payment> GetList(DateTime startDate, DateTime endDate);

        List<Payment> GetByReceiveNumber(string receiveNumber);

        Payment GetByPaymentNumber(string paymentNumber);

        Payment GetById(int id);

        void Delete(int id);
    }
}
