﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.DA.Repositories.CollectionMoney
{
    public class PaymentRepository : IPaymentRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public PaymentRepository(JunFuTransDbContext junFuTransDbContext)
        {
            this.JunFuTransDbContext = junFuTransDbContext;
        }

        public void Delete(int id)
        {
            var payment = this.JunFuTransDbContext.Payments.Find(id);

            if (payment != null)
            {
                //執行假刪除
                payment.UpdateDate = DateTime.Now;

                payment.IsDelete = true;

                this.JunFuTransDbContext.Payments.Attach(payment);

                var entry = this.JunFuTransDbContext.Entry(payment);

                entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

                this.JunFuTransDbContext.SaveChanges();
            }
        }

        public Payment GetById(int id)
        {
            var payment = this.JunFuTransDbContext.Payments.Find(id);

            return payment;
        }

        public Payment GetByPaymentNumber(string paymentNumber)
        {
            var data = from payment in this.JunFuTransDbContext.Payments where payment.PaymentNumber.Equals(paymentNumber) && !payment.IsDelete select payment;

            return data.FirstOrDefault();
        }

        public List<Payment> GetByReceiveNumber(string receiveNumber)
        {
            var data = from payment in this.JunFuTransDbContext.Payments where payment.ReceiveNumber.Equals(receiveNumber) && !payment.IsDelete select payment;

            return data.ToList();
        }

        public List<Payment> GetList(DateTime startDate, DateTime endDate)
        {
            var data = from payment in this.JunFuTransDbContext.Payments where payment.PaymentDate < startDate && payment.PaymentDate > endDate && !payment.IsDelete select payment;

            return data.ToList();
        }

        public int Insert(Payment payment)
        {
            payment.CreateDate = DateTime.Now;

            payment.UpdateDate = DateTime.Now;

            //組成精算編號
            string paymentNumber = GetNextPaymentNumber();

            payment.PaymentNumber = paymentNumber;

            this.JunFuTransDbContext.Payments.Add(payment);

            this.JunFuTransDbContext.SaveChanges();

            return payment.PaymentId;
        }

        public void Update(Payment payment)
        {
            payment.UpdateDate = DateTime.Now;

            this.JunFuTransDbContext.Payments.Add(payment);

            var entry = this.JunFuTransDbContext.Entry(payment);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            this.JunFuTransDbContext.SaveChanges();
        }

        public int GetCount(DateTime startDate, DateTime endDate)
        {
            var q = this.JunFuTransDbContext.Payments.Count(p => p.PaymentDate > startDate && p.PaymentDate < endDate);

            return q;
        }

        public string GetNextPaymentNumber()
        {
            DateTime startDateTime = DateTime.Today;

            DateTime endDateTime = startDateTime.AddDays(1);

            int currentCount = GetCount(startDateTime, endDateTime);

            int dateNumber = currentCount + 10001;

            string currentNumber = string.Format("{0}{1}", DateTime.Today.ToString("yyyyMMdd"), dateNumber.ToString().Substring(1));

            return currentNumber;

        }
    }
}
