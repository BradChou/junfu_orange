﻿using LightYear.JunFuTrans.BL.BE.PickUpAndDeliveryMating;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Transactions;

namespace LightYear.JunFuTrans.DA.Repositories.PickUpAndDeliveryMating
{
    public class PickUpAndDeliveryMatingRepository : IPickUpAndDeliveryMatingRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }

        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public PickUpAndDeliveryMatingRepository(JunFuDbContext junFuDbContext, JunFuTransDbContext junFuTransDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
            this.JunFuTransDbContext = junFuTransDbContext;
        }

        #region 已集貨明細
        public List<PickUpAndDeliveryMatingEntity> GetPickUpByMonthStationScode(DateTime month, string scode)
        {
            var data = (from r in JunFuDbContext.TcDeliveryRequests
                        join sl in JunFuDbContext.TtDeliveryScanLogs on r.CheckNumber equals sl.CheckNumber
                        join d in JunFuDbContext.TbDrivers on sl.DriverCode equals d.DriverCode
                        join s in JunFuDbContext.TbStations on d.Station equals s.StationScode
                        //join c in JunFuDbContext.TbCustomers on r.CustomerCode equals c.CustomerCode
                        where d.Station == scode && r.LatestPickUpScanLogId == sl.LogId
                        && sl.ScanDate >= month && sl.ScanDate < month.AddMonths(1)
                        && r.LessThanTruckload == true && sl.ScanItem == "5"
                        && (r.ShipDate ?? (DateTime)SqlDateTime.MinValue).Month == (sl.ScanDate ?? (DateTime)SqlDateTime.MinValue).Month
                        && !r.CheckNumber.StartsWith("201")
                        select new PickUpAndDeliveryMatingEntity
                        {
                            ScanItem = "已集貨",
                            DriverCode = sl.DriverCode.ToUpper(),
                            DriverFullName = d.DriverName,
                            CheckNumber = r.CheckNumber,
                            OrderNumber = r.OrderNumber,
                            ScanDate = sl.ScanDate,
                            Pieces = r.Pieces ?? 1,
                            StationName = s.StationName,
                            CustomerCode = r.CustomerCode
                        }).Distinct().ToList();

            var removeDuplicateEntity = RemoveDuplicateCustomerCode(data);
            var checkNumberFilter = CheckNumberFilter(removeDuplicateEntity);

            return checkNumberFilter;
        }

        public List<PickUpAndDeliveryMatingEntity> GetPickUpByMonthAndDriver(DateTime month, string driverCode)
        {
            var data = (from r in JunFuDbContext.TcDeliveryRequests
                        join sl in JunFuDbContext.TtDeliveryScanLogs on r.CheckNumber equals sl.CheckNumber
                        join d in JunFuDbContext.TbDrivers on sl.DriverCode equals d.DriverCode
                        join s in JunFuDbContext.TbStations on d.Station equals s.StationScode
                        where sl.DriverCode == driverCode && r.LatestPickUpScanLogId == sl.LogId
                        && sl.ScanDate >= month && sl.ScanDate < month.AddMonths(1)
                        && r.LessThanTruckload == true && sl.ScanItem == "5"
                        && (r.ShipDate ?? (DateTime)SqlDateTime.MinValue).Month == (sl.ScanDate ?? (DateTime)SqlDateTime.MinValue).Month
                        && !r.CheckNumber.StartsWith("201")
                        select new PickUpAndDeliveryMatingEntity
                        {
                            ScanItem = "已集貨",
                            DriverCode = sl.DriverCode.ToUpper(),
                            DriverFullName = d.DriverName,
                            CheckNumber = r.CheckNumber,
                            OrderNumber = r.OrderNumber,
                            ScanDate = sl.ScanDate,
                            Pieces = r.Pieces ?? 1,
                            StationName = s.StationName,
                            CustomerCode = r.CustomerCode
                        }).Distinct().ToList();

            var removeDuplicateEntity = RemoveDuplicateCustomerCode(data);
            var checkNumberFilter = CheckNumberFilter(removeDuplicateEntity);

            return checkNumberFilter;
        }

        public List<PickUpAndDeliveryMatingEntity> GetPickUpByMonthAndArea(DateTime month, IEnumerable<string> stations)
        {
            var data = (from r in JunFuDbContext.TcDeliveryRequests
                        join sl in JunFuDbContext.TtDeliveryScanLogs on r.CheckNumber equals sl.CheckNumber
                        join d in JunFuDbContext.TbDrivers on sl.DriverCode equals d.DriverCode
                        join s in JunFuDbContext.TbStations on d.Station equals s.StationScode                        
                        where stations.Contains(d.Station) && r.LatestPickUpScanLogId == sl.LogId
                        && sl.ScanDate >= month && sl.ScanDate < month.AddMonths(1)
                        && r.LessThanTruckload == true && sl.ScanItem == "5"
                        && (r.ShipDate ?? (DateTime)SqlDateTime.MinValue).Month == (sl.ScanDate ?? (DateTime)SqlDateTime.MinValue).Month
                        && !r.CheckNumber.StartsWith("201")
                        select new PickUpAndDeliveryMatingEntity
                        {
                            ScanItem = "已集貨",
                            DriverCode = sl.DriverCode.ToUpper(),
                            DriverFullName = d.DriverName,
                            CheckNumber = r.CheckNumber,
                            OrderNumber = r.OrderNumber,
                            ScanDate = sl.ScanDate,
                            Pieces = r.Pieces ?? 1,
                            StationName = s.StationName,
                            CustomerCode = r.CustomerCode
                        }).Distinct().ToList();

            var removeDuplicateEntity = RemoveDuplicateCustomerCode(data);
            var checkNumberFilter = CheckNumberFilter(removeDuplicateEntity);

            return checkNumberFilter;
        }
        #endregion

        #region 已配達明細
        public List<PickUpAndDeliveryMatingEntity> GetDeliveryMatingByMonthStationScode(DateTime month, string scode)
        { //可選站所

            var deliveryCheck = from r in JunFuDbContext.TcDeliveryRequests
                                join tb in JunFuDbContext.TbDrivers on r.LatestArriveOptionDriver equals tb.DriverCode
                                join s in JunFuDbContext.TbStations on tb.Station equals s.StationScode
                                join c in JunFuDbContext.TbCustomers on r.CustomerCode equals c.CustomerCode
                                where r.LatestArriveOptionDate >= month && r.LatestArriveOptionDate < month.AddMonths(1)
                                && (r.LatestArriveOption == "3" || r.LatestArriveOption == "D" || r.LatestArriveOption == "E")
                                && r.LessThanTruckload.Equals(true) 
                                && tb.Station == scode
                                && (r.DeliveryCompleteDate ?? (DateTime)SqlDateTime.MinValue).Month == (r.LatestArriveOptionDate ?? (DateTime)SqlDateTime.MinValue).Month
                                select new { r.CheckNumber, r.PrintDate } into slct
                                group slct by slct.CheckNumber into g
                                select new { CheckNumber = g.Key, PrintDate = g.Max(sl => sl.PrintDate) };

            // 抓 print_date 最大值，去重複貨號
            var delivery = (from d in deliveryCheck
                            join r in JunFuDbContext.TcDeliveryRequests on new { d.CheckNumber, d.PrintDate } equals new { r.CheckNumber, r.PrintDate }
                            join dr in JunFuDbContext.TbDrivers on r.LatestArriveOptionDriver equals dr.DriverCode
                            join s in JunFuDbContext.TbStations on dr.Station equals s.StationScode
                            select new PickUpAndDeliveryMatingEntity
                            {
                                ScanItem = "已配達",
                                DriverCode = r.LatestArriveOptionDriver.ToUpper(),
                                DriverFullName = dr.DriverName,
                                ScanDate = r.LatestArriveOptionDate,
                                CheckNumber = r.CheckNumber,
                                OrderNumber = r.OrderNumber,
                                Pieces = r.Pieces ?? 1,
                                StationName = s.StationName,
                                CustomerCode = r.CustomerCode
                            }).Distinct().ToList();

            var removeDuplicateEntity = RemoveDuplicateCustomerCode(delivery);
            var checkNumberFilter = CheckNumberFilter(removeDuplicateEntity);

            return checkNumberFilter;
        }

        public List<PickUpAndDeliveryMatingEntity> GetDeliveryMatingByMonthAndDriver(DateTime month, string driverCode)
        { //可選站所
            var deliveryCheck = from r in JunFuDbContext.TcDeliveryRequests
                                join d in JunFuDbContext.TbDrivers on r.LatestArriveOptionDriver equals d.DriverCode
                                join s in JunFuDbContext.TbStations on d.Station equals s.StationScode
                                join c in JunFuDbContext.TbCustomers on r.CustomerCode equals c.CustomerCode
                                where r.LatestArriveOptionDate >= month && r.LatestArriveOptionDate < month.AddMonths(1) && d.DriverCode == driverCode
                                && (r.LatestArriveOption == "3" || r.LatestArriveOption == "D" || r.LatestArriveOption == "E")
                                && r.LessThanTruckload.Equals(true)
                                && (r.DeliveryCompleteDate ?? (DateTime)SqlDateTime.MinValue).Month == (r.LatestArriveOptionDate ?? (DateTime)SqlDateTime.MinValue).Month
                                select new { r.CheckNumber, r.PrintDate } into slct
                                group slct by slct.CheckNumber into g
                                select new { CheckNumber = g.Key, PrintDate = g.Max(sl => sl.PrintDate) };

            // 抓 print_date 最大值，去重複貨號
            var delivery = (from d in deliveryCheck
                            join r in JunFuDbContext.TcDeliveryRequests on new { d.CheckNumber, d.PrintDate } equals new { r.CheckNumber, r.PrintDate }
                            join dr in JunFuDbContext.TbDrivers on r.LatestArriveOptionDriver equals dr.DriverCode
                            join s in JunFuDbContext.TbStations on dr.Station equals s.StationScode
                            select new PickUpAndDeliveryMatingEntity
                            {
                                ScanItem = "已配達",
                                DriverCode = r.LatestArriveOptionDriver.ToUpper(),
                                DriverFullName = dr.DriverName,
                                ScanDate = r.LatestArriveOptionDate,
                                CheckNumber = r.CheckNumber,
                                OrderNumber = r.OrderNumber,
                                Pieces = r.Pieces ?? 1,
                                StationName = s.StationName,
                                CustomerCode = r.CustomerCode
                            }).Distinct().ToList();

            var removeDuplicateEntity = RemoveDuplicateCustomerCode(delivery);
            var checkNumberFilter = CheckNumberFilter(removeDuplicateEntity);

            return checkNumberFilter;
        }

        public List<PickUpAndDeliveryMatingEntity> GetDeliveryMatingByMonthAndStations(DateTime month, IEnumerable<string> stations)
        {
            var deliveryCheck = from r in JunFuDbContext.TcDeliveryRequests
                                join tb in JunFuDbContext.TbDrivers on r.LatestArriveOptionDriver equals tb.DriverCode
                                where r.LatestArriveOptionDate >= month && r.LatestArriveOptionDate < month.AddMonths(1)
                                && (r.LatestArriveOption == "3" || r.LatestArriveOption == "D" || r.LatestArriveOption == "E")
                                && r.LessThanTruckload.Equals(true) && stations.Contains(tb.Station)
                                && (r.DeliveryCompleteDate ?? (DateTime)SqlDateTime.MinValue).Month == (r.LatestArriveOptionDate ?? (DateTime)SqlDateTime.MinValue).Month
                                select new { r.CheckNumber, r.PrintDate, r.OrderNumber } into slct
                                group slct by slct.CheckNumber into g
                                select new { CheckNumber = g.Key, PrintDate = g.Max(sl => sl.PrintDate), OrderNumber = g.Max(r => r.OrderNumber) };

            // 抓 print_date 最大值，去重複貨號
            var delivery = (from d in deliveryCheck
                            join r in JunFuDbContext.TcDeliveryRequests on new { d.CheckNumber, d.PrintDate } equals new { r.CheckNumber, r.PrintDate }
                            join dr in JunFuDbContext.TbDrivers on r.LatestArriveOptionDriver equals dr.DriverCode
                            join s in JunFuDbContext.TbStations on dr.Station equals s.StationScode                            
                            select new PickUpAndDeliveryMatingEntity
                            {
                                ScanItem = "已配達",
                                DriverCode = r.LatestArriveOptionDriver.ToUpper(),
                                DriverFullName = dr.DriverName,
                                ScanDate = r.LatestArriveOptionDate,
                                CheckNumber = r.CheckNumber,
                                OrderNumber = r.OrderNumber,
                                Pieces = r.Pieces ?? 1,
                                StationName = s.StationName,
                                CustomerCode = r.CustomerCode
                            }).Distinct().ToList();

            var removeDuplicateEntity = RemoveDuplicateCustomerCode(delivery);
            var checkNumberFilter = CheckNumberFilter(removeDuplicateEntity);

            return checkNumberFilter;
        }
        #endregion

        private List<PickUpAndDeliveryMatingEntity> CheckNumberFilter(List<PickUpAndDeliveryMatingEntity> input)
        {
            var requestFilter = (from r in input
                                 where !r.CheckNumber.StartsWith("100") && !r.CheckNumber.StartsWith("600") && !r.CheckNumber.StartsWith("980")
                                 && (!r.CheckNumber.StartsWith("990") || r.CustomerCode != "F3500010002") 
                                 && !(r.CheckNumber.StartsWith("990") && r.OrderNumber != null && r.OrderNumber.ToUpper().StartsWith("TH"))
                                 select r).ToList();

            return requestFilter;
        }

        private List<PickUpAndDeliveryMatingEntity> RemoveDuplicateCustomerCode(List<PickUpAndDeliveryMatingEntity> data)
        {
            var customers = from d in data select d.CustomerCode;
            var customerIds = from c in JunFuDbContext.TbCustomers
                              where customers.Contains(c.CustomerCode)
                              group c by c.CustomerCode into g
                              select new
                              {
                                  CustomerCode = g.Key,
                                  CustomerId = g.Max(c => c.CustomerId)
                              };

            var result = from d in data
                         join cid in customerIds on d.CustomerCode equals cid.CustomerCode
                         join c in JunFuDbContext.TbCustomers on cid.CustomerId equals c.CustomerId
                         select new PickUpAndDeliveryMatingEntity
                         {
                             ScanItem = d.ScanItem,
                             DriverCode = d.DriverCode.ToUpper(),
                             DriverFullName = d.DriverFullName,
                             CheckNumber = d.CheckNumber,
                             OrderNumber = d.OrderNumber,
                             ScanDate = d.ScanDate,
                             Pieces = d.Pieces,
                             StationName = d.StationName,
                             CustomerCode = d.CustomerCode,
                             CustomerName = c.CustomerShortname
                         };

            return result.ToList();
        }

        #region 總表
        public List<PickUpAndDeliveryMatingFirstEntity> GetDriverAllCountByMonthStationScode(DateTime month, string scode)
        {   
            //已集貨
            using (var txn = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
            { IsolationLevel = IsolationLevel.ReadUncommitted }))
            {
                var pickUp = (from ds in JunFuDbContext.TtDeliveryScanLogs
                              join r in JunFuDbContext.TcDeliveryRequests on ds.CheckNumber equals r.CheckNumber
                              join d in JunFuDbContext.TbDrivers on ds.DriverCode equals d.DriverCode
                              where ds.ScanDate >= month && ds.ScanDate < month.AddMonths(1) && r.LessThanTruckload == true
                              && ds.ScanItem == "5" && r.LatestPickUpScanLogId == ds.LogId && d.Station == scode
                              && (r.ShipDate??(DateTime)SqlDateTime.MinValue).Month == (ds.ScanDate ?? (DateTime)SqlDateTime.MinValue).Month
                              && !ds.CheckNumber.StartsWith("100") && !ds.CheckNumber.StartsWith("600") && !ds.CheckNumber.StartsWith("980")
                              && (!ds.CheckNumber.StartsWith("990") || r.CustomerCode != "F3500010002") && (!ds.CheckNumber.StartsWith("990") || r.OrderNumber == null || !r.OrderNumber.ToUpper().StartsWith("TH"))
                              && !r.CheckNumber.StartsWith("201")
                              select new { d.Station, d.DriverCode, d.DriverName, ds.CheckNumber, r.Pieces }).Distinct();

                //已配達
                var deliveryCheck = from r in JunFuDbContext.TcDeliveryRequests
                                    join tb in JunFuDbContext.TbDrivers on r.LatestArriveOptionDriver equals tb.DriverCode
                                    where r.LatestArriveOptionDate >= month && r.LatestArriveOptionDate < month.AddMonths(1)
                                    && (r.LatestArriveOption == "3" || r.LatestArriveOption == "D" || r.LatestArriveOption == "E")
                                    && r.LessThanTruckload.Equals(true) && tb.Station == scode 
                                    && (r.DeliveryCompleteDate ?? (DateTime)SqlDateTime.MinValue).Month == (r.LatestArriveOptionDate ?? (DateTime)SqlDateTime.MinValue).Month
                                    && !r.CheckNumber.StartsWith("100") && !r.CheckNumber.StartsWith("600") && !r.CheckNumber.StartsWith("980")
                                    && (!r.CheckNumber.StartsWith("990") || r.CustomerCode != "F3500010002") && (!r.CheckNumber.StartsWith("990") || r.OrderNumber == null || !r.OrderNumber.ToUpper().StartsWith("TH"))
                                    group r by r.CheckNumber into g
                                    select new { CheckNumber = g.Key, PrintDate = g.Max(sl => sl.PrintDate)};

                // 抓 print_date 最大值，去重複貨號
                var delivery = (from d in deliveryCheck
                               join r in JunFuDbContext.TcDeliveryRequests on new { d.CheckNumber, d.PrintDate } equals new { r.CheckNumber, r.PrintDate }
                               join dr in JunFuDbContext.TbDrivers on r.LatestArriveOptionDriver equals dr.DriverCode
                               select new { dr.Station, r.LatestArriveOptionDriver, dr.DriverName, r.LatestArriveOptionDate, r.CheckNumber, r.Pieces }).Distinct();

                var deliveryCount = delivery.GroupBy(d => new { d.Station, LatestArriveOptionDriver = d.LatestArriveOptionDriver.ToUpper(), d.DriverName })
                                   .Select(a => new { a.Key.Station, DriverCode = a.Key.LatestArriveOptionDriver.ToUpper(), a.Key.DriverName, Count = a.Sum(p => p.Pieces) }).ToList();

                var pickUpCount = pickUp.GroupBy(p => new { p.Station, DriverCode = p.DriverCode.ToUpper(), p.DriverName })
                                    .Select(a => new { a.Key.Station, DriverCode = a.Key.DriverCode.ToUpper(), a.Key.DriverName, Count = a.Sum(p => p.Pieces) }).ToList();

                var leftOuterJoin = (from dc in deliveryCount
                                     join pc in pickUpCount
                                     on dc.DriverCode equals pc.DriverCode
                                     into temp
                                     from pc in temp.DefaultIfEmpty(new { Station = default(string), dc.DriverCode, dc.DriverName, Count = default(int?) })
                                     select new
                                     {
                                         StationScode = dc.Station,
                                         DriverCode = dc.DriverCode,
                                         DriverName = dc.DriverName,
                                         CountDeliveryMating = dc.Count,
                                         CountPickUp = pc.Count,
                                     }).ToList();

                var rightOuterJoin = (from pc in pickUpCount
                                      join dc in deliveryCount
                                      on pc.DriverCode equals dc.DriverCode
                                      into temp
                                      from dc in temp.DefaultIfEmpty(new
                                      {
                                          Station = default(string),
                                          pc.DriverCode,
                                          pc.DriverName,
                                          Count = default(int?)
                                      })
                                      select new
                                      {
                                          StationScode = pc.Station,
                                          DriverCode = pc.DriverCode,
                                          DriverName = pc.DriverName,
                                          CountDeliveryMating = dc.Count,
                                          CountPickUp = pc.Count,
                                      }).ToList();

                var fullOuterJoin = leftOuterJoin.Union(rightOuterJoin);
                var output = fullOuterJoin.Select(s => new PickUpAndDeliveryMatingFirstEntity
                {
                    StationScode = s.StationScode,
                    DriverCode = s.DriverCode,
                    DriverName = s.DriverName,
                    CountDeliveryMating = s.CountDeliveryMating ?? 0,
                    CountPickUp = s.CountPickUp ?? 0,

                }).ToList();

                return output;
            }
        }

        public List<PickUpAndDeliveryMatingFirstEntity> GetDriverAllCountByMonth(DateTime month)
        {   //已集貨
            using (var txn = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
            { IsolationLevel = IsolationLevel.ReadUncommitted }))
            {
                var pickUp = (from ds in JunFuDbContext.TtDeliveryScanLogs
                              join r in JunFuDbContext.TcDeliveryRequests on ds.CheckNumber equals r.CheckNumber
                              join d in JunFuDbContext.TbDrivers on ds.DriverCode equals d.DriverCode
                              where ds.ScanDate >= month && ds.ScanDate < month.AddMonths(1) && r.LessThanTruckload == true
                              && ds.ScanItem == "5" && r.LatestPickUpScanLogId == ds.LogId
                              && (r.ShipDate ?? (DateTime)SqlDateTime.MinValue).Month == (ds.ScanDate ?? (DateTime)SqlDateTime.MinValue).Month
                              && !ds.CheckNumber.StartsWith("100") && !ds.CheckNumber.StartsWith("600") && !ds.CheckNumber.StartsWith("980")
                              && (!ds.CheckNumber.StartsWith("990") || r.CustomerCode != "F3500010002") && (!ds.CheckNumber.StartsWith("990") || !r.OrderNumber.ToUpper().StartsWith("TH"))
                              && !r.CheckNumber.StartsWith("201")
                              select new { d.Station, d.DriverCode, d.DriverName, ScanDate = ds.ScanDate, ds.CheckNumber }).Distinct();

                //已配達
                var deliveryCheck = from ds in JunFuDbContext.TcDeliveryRequests
                                    join r in JunFuDbContext.TcDeliveryRequests on ds.CheckNumber equals r.CheckNumber
                                    join tb in JunFuDbContext.TbDrivers on ds.LatestArriveOptionDriver equals tb.DriverCode
                                    where ds.LatestArriveOptionDate >= month && ds.LatestArriveOptionDate < month.AddMonths(1)
                                    && (ds.LatestArriveOption == "3" || ds.LatestArriveOption == "D" || ds.LatestArriveOption == "E")
                                    && ds.LessThanTruckload.Equals(true)
                                    && (r.DeliveryCompleteDate ?? (DateTime)SqlDateTime.MinValue).Month == (r.LatestArriveOptionDate ?? (DateTime)SqlDateTime.MinValue).Month
                                    && !ds.CheckNumber.StartsWith("100") && !ds.CheckNumber.StartsWith("600") && !ds.CheckNumber.StartsWith("980")
                                    && (!ds.CheckNumber.StartsWith("990") || r.CustomerCode != "F3500010002") && (!ds.CheckNumber.StartsWith("990") || !r.OrderNumber.ToUpper().StartsWith("TH"))
                                    select new { r.CheckNumber, r.PrintDate } into slct
                                    group slct by slct.CheckNumber into g
                                    select new { CheckNumber = g.Key, PrintDate = g.Max(sl => sl.PrintDate) };
                // 抓 print_date 最大值，去重複貨號
                var delivery = (from d in deliveryCheck
                               join r in JunFuDbContext.TcDeliveryRequests on new { d.CheckNumber, d.PrintDate } equals new { r.CheckNumber, r.PrintDate }
                               join dr in JunFuDbContext.TbDrivers on r.LatestArriveOptionDriver equals dr.DriverCode
                               select new { dr.Station, r.LatestArriveOptionDriver, dr.DriverName, r.LatestArriveOptionDate, r.CheckNumber }).Distinct();

                var deliveryCount = delivery.GroupBy(d => new { d.Station, LatestArriveOptionDriver = d.LatestArriveOptionDriver.ToUpper(), d.DriverName })
                                    .Select(a => new { a.Key.Station, DriverCode = a.Key.LatestArriveOptionDriver.ToUpper(), a.Key.DriverName, Count = a.Count() }).ToList();

                var pickUpCount = pickUp.GroupBy(p => new { p.Station, DriverCode = p.DriverCode.ToUpper(), p.DriverName })
                                    .Select(a => new { a.Key.Station, DriverCode = a.Key.DriverCode.ToUpper(), a.Key.DriverName, Count = a.Count() }).ToList();

                var leftOuterJoin = (from dc in deliveryCount
                                     join pc in pickUpCount
                                     on dc.DriverCode equals pc.DriverCode
                                     into temp
                                     from pc in temp.DefaultIfEmpty(new { Station = default(string), dc.DriverCode, dc.DriverName, Count = default(int) })
                                     select new
                                     {
                                         StationScode = dc.Station,
                                         DriverCode = dc.DriverCode,
                                         DriverName = dc.DriverName,
                                         CountDeliveryMating = dc.Count,
                                         CountPickUp = pc.Count,
                                     }).ToList();

                var rightOuterJoin = (from pc in pickUpCount
                                      join dc in deliveryCount
                                      on pc.DriverCode equals dc.DriverCode
                                      into temp
                                      from dc in temp.DefaultIfEmpty(new
                                      {
                                          Station = default(string),
                                          pc.DriverCode,
                                          pc.DriverName,
                                          Count = default(int)
                                      })
                                      select new
                                      {
                                          StationScode = dc.Station,
                                          DriverCode = dc.DriverCode,
                                          DriverName = dc.DriverName,
                                          CountDeliveryMating = dc.Count,
                                          CountPickUp = pc.Count,
                                      }).ToList();

                var fullOuterJoin = leftOuterJoin.Union(rightOuterJoin);
                var output = fullOuterJoin.Select(s => new PickUpAndDeliveryMatingFirstEntity
                {

                    StationScode = s.StationScode,
                    DriverCode = s.DriverCode,
                    DriverName = s.DriverName,
                    CountDeliveryMating = s.CountDeliveryMating,
                    CountPickUp = s.CountPickUp,

                }).ToList();

                return output;
            }
        }

        public List<PickUpAndDeliveryMatingFirstEntity> GetDriverAllCountByMonthStations(DateTime month, IEnumerable<string> stations)
        {   //已集貨
            using (var txn = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
            { IsolationLevel = IsolationLevel.ReadUncommitted }))
            {
                var pickUp = (from ds in JunFuDbContext.TtDeliveryScanLogs
                              join r in JunFuDbContext.TcDeliveryRequests on ds.CheckNumber equals r.CheckNumber
                              join d in JunFuDbContext.TbDrivers on ds.DriverCode equals d.DriverCode
                              where ds.ScanDate >= month && ds.ScanDate < month.AddMonths(1) && r.LessThanTruckload == true
                              && ds.ScanItem == "5" && r.LatestPickUpScanLogId == ds.LogId
                              && (r.ShipDate ?? (DateTime)SqlDateTime.MinValue).Month == (ds.ScanDate ?? (DateTime)SqlDateTime.MinValue).Month
                              && !ds.CheckNumber.StartsWith("100") && !ds.CheckNumber.StartsWith("600") && !ds.CheckNumber.StartsWith("980")
                              && (!ds.CheckNumber.StartsWith("990") || r.CustomerCode != "F3500010002") && (!ds.CheckNumber.StartsWith("990") || !r.OrderNumber.ToUpper().StartsWith("TH"))
                              && !r.CheckNumber.StartsWith("201")
                              && stations.Contains(d.Station)
                              select new { d.Station, d.DriverCode, d.DriverName, ScanDate = ds.ScanDate, ds.CheckNumber, r.Pieces }).Distinct();

                //已配達
                var deliveryCheck = from ds in JunFuDbContext.TcDeliveryRequests
                                    join r in JunFuDbContext.TcDeliveryRequests on ds.CheckNumber equals r.CheckNumber
                                    join tb in JunFuDbContext.TbDrivers on ds.LatestArriveOptionDriver equals tb.DriverCode
                                    where ds.LatestArriveOptionDate >= month && ds.LatestArriveOptionDate < month.AddMonths(1)
                                    && (ds.LatestArriveOption == "3" || ds.LatestArriveOption == "D" || ds.LatestArriveOption == "E")
                                    && ds.LessThanTruckload.Equals(true) && stations.Contains(tb.Station)
                                    && (r.DeliveryCompleteDate ?? (DateTime)SqlDateTime.MinValue).Month == (r.LatestArriveOptionDate ?? (DateTime)SqlDateTime.MinValue).Month
                                    && !ds.CheckNumber.StartsWith("100") && !ds.CheckNumber.StartsWith("600") && !ds.CheckNumber.StartsWith("980")
                                    && (!ds.CheckNumber.StartsWith("990") || r.CustomerCode != "F3500010002") && (!ds.CheckNumber.StartsWith("990") || !r.OrderNumber.ToUpper().StartsWith("TH"))
                                    select new { r.CheckNumber, r.PrintDate } into slct
                                    group slct by slct.CheckNumber into g
                                    select new { CheckNumber = g.Key, PrintDate = g.Max(sl => sl.PrintDate) };

                // 抓 print_date 最大值，去重複貨號
                var delivery = (from d in deliveryCheck
                               join r in JunFuDbContext.TcDeliveryRequests on new { d.CheckNumber, d.PrintDate } equals new { r.CheckNumber, r.PrintDate }
                               join dr in JunFuDbContext.TbDrivers on r.LatestArriveOptionDriver equals dr.DriverCode
                               select new { dr.Station, r.LatestArriveOptionDriver, dr.DriverName, r.LatestArriveOptionDate, r.CheckNumber, r.Pieces }).Distinct();

                var deliveryCount = delivery.GroupBy(d => new { d.Station, LatestArriveOptionDriver = d.LatestArriveOptionDriver.ToUpper(), d.DriverName })
                                   .Select(a => new { a.Key.Station, DriverCode = a.Key.LatestArriveOptionDriver.ToUpper(), a.Key.DriverName, Count = a.Sum(r => r.Pieces) }).ToList();

                var pickUpCount = pickUp.GroupBy(p => new { p.Station, DriverCode = p.DriverCode.ToUpper(), p.DriverName })
                                    .Select(a => new { a.Key.Station, DriverCode = a.Key.DriverCode.ToUpper(), a.Key.DriverName, Count = a.Sum(r => r.Pieces) }).ToList();

                var leftOuterJoin = (from dc in deliveryCount
                                     join pc in pickUpCount
                                     on dc.DriverCode equals pc.DriverCode
                                     into temp
                                     from pc in temp.DefaultIfEmpty(new { Station = default(string), dc.DriverCode, dc.DriverName, Count = default(int?) })
                                     select new
                                     {
                                         StationScode = dc.Station,
                                         DriverCode = dc.DriverCode,
                                         DriverName = dc.DriverName,
                                         CountDeliveryMating = dc.Count,
                                         CountPickUp = pc.Count,
                                     }).ToList();

                var rightOuterJoin = (from pc in pickUpCount
                                      join dc in deliveryCount
                                      on pc.DriverCode equals dc.DriverCode
                                      into temp
                                      from dc in temp.DefaultIfEmpty(new
                                      {
                                          Station = default(string),
                                          pc.DriverCode,
                                          pc.DriverName,
                                          Count = default(int?)
                                      })
                                      select new
                                      {
                                          StationScode = pc.Station,
                                          DriverCode = pc.DriverCode,
                                          DriverName = pc.DriverName,
                                          CountDeliveryMating = dc.Count,
                                          CountPickUp = pc.Count,
                                      }).ToList();

                var fullOuterJoin = leftOuterJoin.Union(rightOuterJoin);
                var output = fullOuterJoin.Select(s => new PickUpAndDeliveryMatingFirstEntity
                {

                    StationScode = s.StationScode,
                    DriverCode = s.DriverCode,
                    DriverName = s.DriverName,
                    CountDeliveryMating = s.CountDeliveryMating ?? 0,
                    CountPickUp = s.CountPickUp ?? 0,

                }).ToList();

                return output;
            }
        } 
        #endregion
    }
}
