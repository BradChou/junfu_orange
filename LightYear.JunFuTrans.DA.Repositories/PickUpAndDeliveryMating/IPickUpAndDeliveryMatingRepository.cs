﻿using LightYear.JunFuTrans.BL.BE.PickUpAndDeliveryMating;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.PickUpAndDeliveryMating
{
    public interface IPickUpAndDeliveryMatingRepository
    {
        List<PickUpAndDeliveryMatingEntity> GetPickUpByMonthStationScode(DateTime month, string scode);

        List<PickUpAndDeliveryMatingEntity> GetPickUpByMonthAndDriver(DateTime month, string driverCode);

        List<PickUpAndDeliveryMatingEntity> GetPickUpByMonthAndArea(DateTime month, IEnumerable<string> stations);

        List<PickUpAndDeliveryMatingEntity> GetDeliveryMatingByMonthStationScode(DateTime month, string scode);

        List<PickUpAndDeliveryMatingEntity> GetDeliveryMatingByMonthAndDriver(DateTime month, string driverCode);

        List<PickUpAndDeliveryMatingFirstEntity> GetDriverAllCountByMonthStationScode(DateTime month, string scode);

        List<PickUpAndDeliveryMatingFirstEntity> GetDriverAllCountByMonth(DateTime month);

        List<PickUpAndDeliveryMatingEntity> GetDeliveryMatingByMonthAndStations(DateTime month, IEnumerable<string> stations);

        List<PickUpAndDeliveryMatingFirstEntity> GetDriverAllCountByMonthStations(DateTime month, IEnumerable<string> stations);
    }
}
