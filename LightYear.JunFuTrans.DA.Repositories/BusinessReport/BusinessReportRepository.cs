﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using LightYear.JunFuTrans.BL.BE.BusinessReport;

namespace LightYear.JunFuTrans.DA.Repositories.BusinessReport
{
    public class BusinessReportRepository : IBusinessReportRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }
        public BusinessReportRepository(JunFuDbContext dbContext)
        {
            JunFuDbContext = dbContext;
        }
        public List<BusinessReportRepositoryEntity> PieceList(DateTime start, DateTime end, string areaArriveCode)
        {
            var data = from requests in this.JunFuDbContext.TcDeliveryRequests
                       where requests.ShipDate > start.AddHours(5) & requests.ShipDate < end.AddHours(5).AddDays(1) &  requests.CheckNumber.Length.Equals(12) & requests.SupplierCode != "*9聯運" & (areaArriveCode.Equals("All") || requests.AreaArriveCode.Equals(areaArriveCode))
                       orderby requests.SupplierCode
                       select new BusinessReportRepositoryEntity()
                       {
                           CheckNumber = requests.CheckNumber,
                           AreaArriveCode = requests.AreaArriveCode,
                           Pieces = requests.Pieces,
                           ShipDate = requests.ShipDate,
                           SupplierCode = requests.SupplierCode,
                           ScanItem = "5",                                              //trigger造成有shipdate必定有集貨日，故改寫死為集貨
                           CustomerCode = requests.CustomerCode
                       };

            return data.ToList();
        }
        public List<TbStation> StationEntities()
        {
            var data = from station in this.JunFuDbContext.TbStations where station.ActiveFlag.Equals(true) 
                       orderby station.StationScode ascending
                       where !station.StationName.StartsWith("F") && !station.StationName.EndsWith("倉")
                       select station;

            return data.ToList();
        }
    }
}
