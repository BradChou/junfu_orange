﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;
using System.Linq;
using LightYear.JunFuTrans.BL.BE.Intermodal;

namespace LightYear.JunFuTrans.DA.Repositories.Intermodal
{
    public class IntermodalRepository : IIntermodalRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }
        public IntermodalRepository(JunFuDbContext junFuDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
        }

        public IEnumerable<TcDeliveryRequest> GetTcDeliveryRequestsByRequestIds(List<string> requestIds)
        {
            return JunFuDbContext.TcDeliveryRequests.Where(x => requestIds.Contains(x.RequestId.ToString()) && x.AreaArriveCode == "99" && x.DeliveryType == "D");
        }

        public void InsertGuoYangTable(IEnumerable<GuoYangRequest> guoYangRequests)
        {
            JunFuDbContext.GuoYangRequests.AddRange(guoYangRequests);
        }

        public IEnumerable<GuoYangRequest> GetGuoYangRequestsByCheckNumbers(IEnumerable<string> checkNumbers)
        {
            return JunFuDbContext.GuoYangRequests.Where(x => checkNumbers.Contains(x.FseCheckNumber));
        }

        public bool InsertIntoGuoYangTable(List<GuoYangRequest> requests)
        {
            bool isSuccess = true;
            try
            {
                JunFuDbContext.GuoYangRequests.AddRange(requests);
            }
            catch
            {
                isSuccess = false;
            }
            return isSuccess;
        }

        public GuoYangCheckNumberStartAndEndEntity GetNextGuoYangCheckNumberBulky(int amount)      //一次取幾單
        {
            int priority = JunFuDbContext.IntermodalTransportationChecknumbers.Where(x => x.CompanyId == 2 && x.ActiveFlag == true).Min(x => x.Priority) ?? -1;
            var companyDbEntity = JunFuDbContext.IntermodalTransportationChecknumbers.Where(x => x.CompanyId == 2 && x.ActiveFlag == true && x.Priority == priority).FirstOrDefault();

            return new GuoYangCheckNumberStartAndEndEntity
            {
                Start = long.Parse(companyDbEntity.NumberStart),
                End = long.Parse(companyDbEntity.NumberEnd)
            };
        }

        public bool BulkyInsertIntoGuoYangTable(IEnumerable<GuoYangRequest> guoYangRequests)
        {
            try
            {
                JunFuDbContext.GuoYangRequests.AddRange(guoYangRequests);
                JunFuDbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool BulkyUpdateGuoYang(IEnumerable<GuoYangRequest> guoYangRequests)
        {
            try
            {
                JunFuDbContext.GuoYangRequests.AttachRange(guoYangRequests);
                foreach (var g in guoYangRequests)
                {
                    var entry = this.JunFuDbContext.Entry(g);
                    entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                }
                JunFuDbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public int InsertGuoYangSchedule(GuoYangScheduleLog log)
        {
            try
            {
                JunFuDbContext.GuoYangScheduleLogs.Add(log);
                JunFuDbContext.SaveChanges();
                return log.Id;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public GuoYangScheduleLog GetLastScheduleLogForAssignStep(int step)
        {
            return JunFuDbContext.GuoYangScheduleLogs.Where(x => x.Step == step && x.EndTime == null).OrderByDescending(x => x.StartTime).FirstOrDefault();
        }

        public int UpdateScheduleLogForAssignStep(GuoYangScheduleLog log)
        {
            try
            {
                JunFuDbContext.GuoYangScheduleLogs.Append(log);
                JunFuDbContext.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public bool InsertGuoYangReturnContent(IEnumerable<GuoYangReturnContent> contents)
        {
            try
            {
                JunFuDbContext.GuoYangReturnContents.AddRange(contents);
                JunFuDbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public IEnumerable<GuoYangRequest> SelectGuoYangRequest(IEnumerable<string> guoYangCheckNumbers)
        {
            return JunFuDbContext.GuoYangRequests.Where(x => guoYangCheckNumbers.Contains(x.GuoyangCheckNumber) && x.IsClosed != true);
        }

        public IEnumerable<GuoYangRequest> SelectGuoYangRequestNeedToSendToday(string driverCode)
        {
            var gy = JunFuDbContext.GuoYangRequests.Where(x => x.IsUploaded == false);
            var sl = JunFuDbContext.TtDeliveryScanLogs.Where(x => x.DriverCode == driverCode && x.ScanItem == "7");
            List<GuoYangRequest> resultInEntities = new List<GuoYangRequest>();
            var resultInObj = gy.Join(sl, gyang => gyang.FseCheckNumber, slog => slog.CheckNumber, (gyang, slog) => new
            {
                Id = gyang.Id,
                FseCheckNumber = gyang.FseCheckNumber,
                GuoyangCheckNumber = gyang.GuoyangCheckNumber,
                Cdate = gyang.Cdate,
                ActiveFlag = gyang.ActiveFlag,
                ScheduleId = gyang.ScheduleId,
                ReceiveContact = gyang.ReceiveContact,
                ReceiveAddress = gyang.ReceiveAddress,
                ReceiveTel = gyang.ReceiveTel,
                EMail = gyang.EMail,
                DetailCheckNumber = gyang.DetailCheckNumber,
                CollectionMoney = gyang.CollectionMoney,
                Remark = gyang.Remark,
                IsEcho = gyang.IsEcho,
                IsClosed = gyang.IsClosed,
                IsUploaded = gyang.IsUploaded,
                UploadFileName = gyang.UploadFileName,
                RequestId = gyang.RequestId
            }).Distinct().ToList();
            foreach (var o in resultInObj)
            {
                resultInEntities.Add(new GuoYangRequest
                {
                    Id = o.Id,
                    FseCheckNumber = o.FseCheckNumber,
                    GuoyangCheckNumber = o.GuoyangCheckNumber,
                    Cdate = o.Cdate,
                    ActiveFlag = o.ActiveFlag,
                    ScheduleId = o.ScheduleId,
                    ReceiveContact = o.ReceiveContact,
                    ReceiveAddress = o.ReceiveAddress,
                    ReceiveTel = o.ReceiveTel,
                    EMail = o.EMail,
                    DetailCheckNumber = o.DetailCheckNumber,
                    CollectionMoney = o.CollectionMoney,
                    Remark = o.Remark,
                    IsEcho = o.IsEcho,
                    IsClosed = o.IsClosed,
                    IsUploaded = o.IsUploaded,
                    UploadFileName = o.UploadFileName,
                    RequestId = o.RequestId
                });
            }

            return resultInEntities;
        }

        public bool InsertIntoApiReturn(IEnumerable<GuoYangApiReturnStatus> entities)
        {
            try
            {
                JunFuDbContext.GuoYangApiReturnStatus.AddRange(entities);
                JunFuDbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }

        }

        public string[] GetActiveRquestFromGuoYangRequest()
        {
            return JunFuDbContext.GuoYangRequests.Where(x => x.IsEcho == true && x.IsUploaded == true && x.IsClosed == false).Select(x => x.GuoyangCheckNumber).ToArray();
        }

        public GuoYangStatus[] GetGuoYangStatuses()
        {
            return JunFuDbContext.GuoYangStatus.ToArray();
        }

        public bool InsertIntoTtDeliveryScanLog(IEnumerable<TtDeliveryScanLog> logs)
        {
            JunFuDbContext.TtDeliveryScanLogs.AddRange(logs);
            JunFuDbContext.SaveChanges();
            return true;
        }

        public List<GuoYangApiReturnStatus> GetTheLatestApiReturnStatus(string[] checkNumbers)
        {
            var statusContainsInCheckNumbersArray = JunFuDbContext.GuoYangApiReturnStatus.Where(s => checkNumbers.Contains(s.ApiPostNo)).ToArray();
            var groupedCheckNumbers = statusContainsInCheckNumbersArray.GroupBy(x => x.ApiPostNo).ToArray();
            List<GuoYangApiReturnStatus> result = new List<GuoYangApiReturnStatus>();

            for (var i = 0; i < groupedCheckNumbers.Length; i++)
            {
                var checkNumbersTheNewestStatus = groupedCheckNumbers[i].Max(x => x.ApiNowDate);
                result.Add(groupedCheckNumbers[i].Where(x => x.ApiNowDate == checkNumbersTheNewestStatus).FirstOrDefault());
            }

            return result;
        }

        public bool IsTodayHoliday()
        {
            DateTime now = DateTime.Now;
            var today = JunFuDbContext.TbCalendars.Where(c => c.Year == now.Year && c.Month == now.Month && c.Day == now.Day && c.KindOfDay != "WORKDAY");
            return today.Count() > 0 ? true : false;
        }
    }
}
