﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.BL.BE.Intermodal;

namespace LightYear.JunFuTrans.DA.Repositories.Intermodal
{
    public interface IIntermodalRepository
    {

        IEnumerable<TcDeliveryRequest> GetTcDeliveryRequestsByRequestIds(List<string> requestIds);

        void InsertGuoYangTable(IEnumerable<GuoYangRequest> guoYangRequests);

        IEnumerable<GuoYangRequest> GetGuoYangRequestsByCheckNumbers(IEnumerable<string> checkNumbers);

        GuoYangCheckNumberStartAndEndEntity GetNextGuoYangCheckNumberBulky(int amount);

        bool BulkyInsertIntoGuoYangTable(IEnumerable<GuoYangRequest> guoYangRequests);

        bool BulkyUpdateGuoYang(IEnumerable<GuoYangRequest> guoYangRequests);

        int InsertGuoYangSchedule(GuoYangScheduleLog log);

        GuoYangScheduleLog GetLastScheduleLogForAssignStep(int step);

        int UpdateScheduleLogForAssignStep(GuoYangScheduleLog log);

        bool InsertGuoYangReturnContent(IEnumerable<GuoYangReturnContent> contents);

        IEnumerable<GuoYangRequest> SelectGuoYangRequest(IEnumerable<string> guoYangCheckNumbers);

        IEnumerable<GuoYangRequest> SelectGuoYangRequestNeedToSendToday(string driverCode);

        bool InsertIntoApiReturn(IEnumerable<GuoYangApiReturnStatus> entities);

        string[] GetActiveRquestFromGuoYangRequest();

        GuoYangStatus[] GetGuoYangStatuses();

        bool InsertIntoTtDeliveryScanLog(IEnumerable<TtDeliveryScanLog> logs);

        List<GuoYangApiReturnStatus> GetTheLatestApiReturnStatus(string[] checkNumbers);

        bool IsTodayHoliday();
    }
}
