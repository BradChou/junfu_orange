﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.BusinessReport;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.Mappers.BusinessReport
{
    public class AreaStationMappingProfile : AutoMapper.Profile
    {
        public AreaStationMappingProfile()
        {
            this.CreateMap<StationArea, AreaStationEntity>()
                .ForMember(a => a.Area, s => s.MapFrom(a => a.Area))
                .ForMember(a => a.StationCode, s => s.MapFrom(a => a.StationCode))
                .ForMember(a => a.StationName, s => s.MapFrom(a => a.StationName));

            this.CreateMap<AreaStationEntity, StationArea>()
                .ForMember(s => s.Area, a => a.MapFrom(s => s.Area))
                .ForMember(s => s.StationCode, a => a.MapFrom(s => s.StationCode))
                .ForMember(s => s.StationName, a => a.MapFrom(s => s.StationName));
        }
    }
}
