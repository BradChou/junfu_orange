﻿using LightYear.JunFuTrans.BL.BE.RemittanceAndCheckWriteOff;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.Mappers.RemittanceAndCheckWriteOff
{
    public class RemittanceAndCheckWriteOffMappingProfile : AutoMapper.Profile
    {
        public RemittanceAndCheckWriteOffMappingProfile()
        {
            CreateMap<PaymentWriteOff, RemittanceAndCheckWriteOffEntity>()
                .ForMember(i => i.RequestId, s => s.MapFrom(i => i.RequestId))
                .ForMember(i => i.IsWriteOff, s => s.MapFrom(i => i.IsWriteOff));

            CreateMap<RemittanceAndCheckWriteOffEntity, PaymentWriteOff>()
               .ForMember(i => i.RequestId, s => s.MapFrom(i => i.RequestId))
               .ForMember(i => i.IsWriteOff, s => s.MapFrom(i => i.IsWriteOff));
        }
    }
}
