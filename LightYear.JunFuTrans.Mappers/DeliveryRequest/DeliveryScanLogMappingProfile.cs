﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.DeliveryRequest;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.Mappers.DeliveryRequest
{
    public class DeliveryScanLogMappingProfile : AutoMapper.Profile
    {
        public DeliveryScanLogMappingProfile()
        {
            this.CreateMap<TtDeliveryScanLog, DeliveryScanLogEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.LogId))
                .ForMember(i => i.DriverCode, s => s.MapFrom(i => i.DriverCode))
                .ForMember(i => i.ScanDate, s => s.MapFrom(i => i.ScanDate))
                .ForMember(i => i.ScanStatus, s => s.MapFrom(i => i.ScanItem))
                .ForMember(i => i.CreateDate, s => s.MapFrom(i => i.Cdate))
                .ForMember(i => i.SignFormImage, s => s.MapFrom(i => i.SignFormImage))
                .ForMember(i => i.SignFieldImage, s => s.MapFrom(i => i.SignFieldImage));
        }
    }
}
