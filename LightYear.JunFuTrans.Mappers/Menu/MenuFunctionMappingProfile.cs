﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.Menu;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.Mappers.Menu
{
    public class MenuFunctionMappingProfile:AutoMapper.Profile
    {
        public MenuFunctionMappingProfile()
        {
            this.CreateMap<SystemFunction, SystemFunctionEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.SubCategoryId, s => s.MapFrom(i => i.SubCategoryId))
                .ForMember(i => i.FunctionName, s => s.MapFrom(i => i.FunctionName))
                .ForMember(i => i.FunctionEnName, s => s.MapFrom(i => i.FunctionEnName))
                .ForMember(i => i.Url, s => s.MapFrom(i => i.FunctionUrl))
                .ForMember(i => i.IsMenu, s => s.MapFrom(i => i.IsMenu))
                .ForMember(i => i.DisplaySort, s => s.MapFrom(i => i.DisplaySort));
                
        }
    }
}
