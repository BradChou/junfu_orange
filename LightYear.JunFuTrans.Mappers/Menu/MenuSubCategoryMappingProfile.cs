﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.Menu;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.Mappers.Menu
{
    class MenuSubCategoryMappingProfile : AutoMapper.Profile
    {
        public MenuSubCategoryMappingProfile()
        {
            this.CreateMap<SystemSubCategory, SystemSubCategoryEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.CatetoryId, s => s.MapFrom(i => i.CategoryId))
                .ForMember(i => i.SubCategoryName, s => s.MapFrom(i => i.SubCategoryName))
                .ForMember(i => i.SubCategoryEnName, s => s.MapFrom(i => i.SubCategoryEnName))
                .ForMember(i => i.IsMenu, s => s.MapFrom(i => i.IsMenu))
                .ForMember(i => i.DisplaySort, s => s.MapFrom(i => i.DisplaySort));

        }
    }
}
