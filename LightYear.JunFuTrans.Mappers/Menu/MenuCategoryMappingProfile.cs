﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.Menu;
using LightYear.JunFuTrans.DA.JunFuTransDb;
namespace LightYear.JunFuTrans.Mappers.Menu
{
    class MenuCategoryMappingProfile:AutoMapper.Profile
    {
        public MenuCategoryMappingProfile()
        {
            this.CreateMap<SystemCategory, SystemCategoryEntity>()
            .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
            .ForMember(i => i.CategoryName, s => s.MapFrom(i => i.CategoryName))
            .ForMember(i => i.CategoryEnName, s => s.MapFrom(i => i.CategoryEnName))
            .ForMember(i => i.IsMenu, s => s.MapFrom(i => i.IsMenu))
            .ForMember(i => i.DisplaySort, s => s.MapFrom(i => i.DisplaySort));
                
        }
    }
}
