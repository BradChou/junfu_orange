﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.FailDelivery;
using LightYear.JunFuTrans.DA.JunFuDb;
using AutoMapper;

namespace LightYear.JunFuTrans.Mappers.FailDelivery
{
    public class FailDeliveryMappingProfile : Profile
    {
        public FailDeliveryMappingProfile()
        {
            this.CreateMap<TbCustomer, CustomerDownListEntity>()
                .ForMember(c => c.CustomerCode, t => t.MapFrom(c => c.CustomerCode))
                .ForMember(c => c.CustomerName, t => t.MapFrom(c => c.CustomerName));


            this.CreateMap<CustomerDownListEntity, TbCustomer>()
                .ForMember(t => t.CustomerCode, c => c.MapFrom(t => t.CustomerCode))
                .ForMember(t => t.CustomerName, c => c.MapFrom(t => t.CustomerName));

        }
    }
}
