﻿using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.Mappers.TruckVendor
{
    public class TruckVendorMappingProfile : AutoMapper.Profile
    {
        public TruckVendorMappingProfile()
        {
            CreateMap<TruckVendorPaymentLog, TruckVendorPaymentLogFrontEndShowEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.UploadDate, s => s.MapFrom(i => i.CreateDate))
                //.ForMember(i => i.DutyDept, s => s.MapFrom(i => i.))
                .ForMember(i => i.RegisterDate, s => s.MapFrom(i => i.RegisteredDate))
                .ForMember(i => i.FeeType, s => s.MapFrom(i => i.FeeType))
                .ForMember(i => i.VendorName, s => s.MapFrom(i => i.VendorName))
                .ForMember(i => i.CarNumber, s => s.MapFrom(i => i.CarLicense))
                .ForMember(i => i.Payment, s => s.MapFrom(i => i.Payment))
                .ForMember(i => i.Memo, s => s.MapFrom(i => i.Memo))
                .ForMember(i => i.CompanyType, s => s.MapFrom(i => i.Company));

            CreateMap<TruckVendorPaymentLogFrontEndShowEntity, TruckVendorPaymentLog>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.CreateDate, s => s.MapFrom(i => i.UploadDate))
                .ForMember(i => i.RegisteredDate, s => s.MapFrom(i => i.RegisterDate))
                .ForMember(i => i.FeeType, s => s.MapFrom(i => i.FeeType))
                .ForMember(i => i.VendorName, s => s.MapFrom(i => i.VendorName))
                .ForMember(i => i.CarLicense, s => s.MapFrom(i => i.CarNumber))
                .ForMember(i => i.Payment, s => s.MapFrom(i => i.Payment))
                .ForMember(i => i.Memo, s => s.MapFrom(i => i.Memo))
                .ForMember(i => i.Company, s => s.MapFrom(i => i. CompanyType));
        }
    }
}
