﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.CollectionMoney;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.Mappers.CollectionMoney
{
    public class CollectionMoneyMappingProfile : AutoMapper.Profile
    {
        public CollectionMoneyMappingProfile()
        {
            //from CollectionMoneyEntity to Payment
            this.CreateMap<CollectionMoneyEntity, Payment>()
                .ForMember(i => i.ReceiveNumber, s => s.MapFrom(i => i.BankNumber))
                .ForMember(i => i.PaymentDate, s => s.MapFrom(i => DateTime.Now));

            //from CollectionMoneyDetailEntity to PaymentDetail
            this.CreateMap<CollectionMoneyDetailEntity, PaymentDetail>()
                .ForMember(i => i.Barcode, s => s.MapFrom(i => i.CheckNumber))
                .ForMember(i => i.ReceiveAmount, s => s.MapFrom(i => i.Amount))
                .ForMember(i => i.ActualAmount, s => s.MapFrom(i => i.ActualAmount));
        }
    }
}
