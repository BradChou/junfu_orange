﻿using LightYear.JunFuTrans.BL.BE.StationArea;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.Mappers.StationWithArea
{
    public class StationAreaMappingProfile : AutoMapper.Profile
    {
        public StationAreaMappingProfile()
        {
            CreateMap<StationArea, StationAreaEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.Area, s => s.MapFrom(i => i.Area))
                .ForMember(i => i.ShowName, s => s.MapFrom(i => i.StationName + "(" + i.StationCode + ")"));

            CreateMap<StationAreaEntity, StationArea>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.Area, s => s.MapFrom(i => i.Area))
                .ForMember(i => i.StationCode, s => s.MapFrom(i => i.StationCode))
                .ForMember(i => i.StationScode, s => s.MapFrom(i => i.StationSCode))
                .ForMember(i => i.StationName, s => s.MapFrom(i => i.StationName))
                .ForMember(i => i.CreateDate, s => s.MapFrom(i => i.CreateDate))
                .ForMember(i => i.UpdateDate, s => s.MapFrom(i => i.UpdateDate))
                .ForMember(i => i.UpdateUser, s => s.MapFrom(i => i.UpdateUser));

            CreateMap<TbStation, StationAreaEntity>()
                .ForMember(i => i.StationName, s => s.MapFrom(i => i.StationName))
                .ForMember(i => i.StationCode, s => s.MapFrom(i => i.StationCode))
                .ForMember(i => i.StationSCode, s => s.MapFrom(i => i.StationScode))
                .ForMember(i => i.UpdateDate, s => s.MapFrom(i => i.Udate))
                .ForMember(i => i.UpdateUser, s => s.MapFrom(i => i.Uuser))
                .ForMember(i => i.ShowName, s => s.MapFrom(i => i.StationName + "(" + i.StationCode + ")"));

            CreateMap<OrgArea, Post5CodeMappingEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.Office, s => s.MapFrom(i => i.Office))
                .ForMember(i => i.Zip3A, s => s.MapFrom(i => i.Zip3A))
                .ForMember(i => i.ZipCode, s => s.MapFrom(i => i.Zipcode))
                .ForMember(i => i.City, s => s.MapFrom(i => i.City))
                .ForMember(i => i.Area, s => s.MapFrom(i => i.Area))
                .ForMember(i => i.Road, s => s.MapFrom(i => i.Road))
                .ForMember(i => i.Scoop, s => s.MapFrom(i => i.Scoop))
                .ForMember(i => i.StationName, s => s.MapFrom(i => i.StationName))
                .ForMember(i => i.StationScode, s => s.MapFrom(i => i.StationScode))
                .ForMember(i => i.MDNo, s => s.MapFrom(i => i.MdNo))
                .ForMember(i => i.SDNo, s => s.MapFrom(i => i.SdNo))
                .ForMember(i => i.PutOrder, s => s.MapFrom(i => i.PutOrder));

            CreateMap<Post5CodeMappingEntity, OrgArea>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.Office, s => s.MapFrom(i => i.Office))
                .ForMember(i => i.Zip3A, s => s.MapFrom(i => i.Zip3A))
                .ForMember(i => i.Zipcode, s => s.MapFrom(i => i.ZipCode))
                .ForMember(i => i.City, s => s.MapFrom(i => i.City))
                .ForMember(i => i.Area, s => s.MapFrom(i => i.Area))
                .ForMember(i => i.Road, s => s.MapFrom(i => i.Road))
                .ForMember(i => i.Scoop, s => s.MapFrom(i => i.Scoop))
                .ForMember(i => i.StationName, s => s.MapFrom(i => i.StationName))
                .ForMember(i => i.StationScode, s => s.MapFrom(i => i.StationScode))
                .ForMember(i => i.MdNo, s => s.MapFrom(i => i.MDNo))
                .ForMember(i => i.SdNo, s => s.MapFrom(i => i.SDNo))
                .ForMember(i => i.PutOrder, s => s.MapFrom(i => i.PutOrder));
        }
    }
}
