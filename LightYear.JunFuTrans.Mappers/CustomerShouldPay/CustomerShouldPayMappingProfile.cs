﻿using LightYear.JunFuTrans.BL.BE.CustomerShouldPay;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.Mappers.CustomerShouldPay
{
    public class CustomerShouldPayMappingProfile : AutoMapper.Profile
    {
        public CustomerShouldPayMappingProfile()
        {
            CreateMap<TcDeliveryRequest, CustomerPaymentDetailEntity>()
                .ForMember(i => i.ShipDate, s => s.MapFrom(i => i.ShipDate))
                .ForMember(i => i.CheckNumber, s => s.MapFrom(i => i.CheckNumber))
                .ForMember(i => i.OrderId, s => s.MapFrom(i => i.OrderNumber))
                .ForMember(i => i.SupplyStationScode, s => s.MapFrom(i => i.SendStationScode))
                .ForMember(i => i.AreaArriveCode, s => s.MapFrom(i => i.AreaArriveCode))
                .ForMember(i => i.ReceiveMan, s => s.MapFrom(i => i.ReceiveContact))
                .ForMember(i => i.PickUpGoodTypeByDriver, s => s.MapFrom(i => int.Parse(i.PickUpGoodType)))
                .ForMember(i => i.Weight, s => s.MapFrom(i => i.CbmWeight))
                .ForMember(i => i.SideLengthSum, s => s.MapFrom(i => i.CbmHeight + i.CbmLength + i.CbmWidth))
                .ForMember(i => i.Length, s => s.MapFrom(i => i.CbmLength))
                .ForMember(i => i.Width, s => s.MapFrom(i => i.CbmWidth))
                .ForMember(i => i.Height, s => s.MapFrom(i => i.CbmHeight))
                .ForMember(i => i.Price, s => s.MapFrom(i => i.Freight))
                .ForMember(i => i.SubpoenaCategory, s => s.MapFrom(i => i.SubpoenaCategory))
                .ForMember(i => i.SubpoenaCategoryShowName, s => s.MapFrom(i => GetCategoryShowName(i.SubpoenaCategory)));

            // from CustomerPaymentEntity to AccountReceivable
            CreateMap<CustomerPaymentEntity, AccountReceivable>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.CustomerCode, s => s.MapFrom(i => i.CustomerCode))
                .ForMember(i => i.CustomerName, s => s.MapFrom(i => i.CustomerName))
                .ForMember(i => i.ShipDateStart, s => s.MapFrom(i => i.ShipDateStart))
                .ForMember(i => i.ShipDateEnd, s => s.MapFrom(i => i.ShipDateEnd))
                .ForMember(i => i.InvoiceId, s => s.MapFrom(i => i.InvoiceId))
                .ForMember(i => i.Peices, s => s.MapFrom(i => i.Peices))
                .ForMember(i => i.IsPaid, s => s.MapFrom(i => i.IsPaid))
                .ForMember(i => i.ElectronicReceipt, s => s.MapFrom(i => i.ElectricReceipt))
                .ForMember(i => i.CreateDate, s => s.MapFrom(i => i.CreateDate))
                .ForMember(i => i.UpdateDate, s => s.MapFrom(i => i.UpdateTime))
                .ForMember(i => i.PaidDate, s => s.MapFrom(i => i.PaidDate));

            CreateMap<AccountReceivable, CustomerPaymentEntity>()
                .ForMember(i => i.CustomerCode, s => s.MapFrom(i => i.CustomerCode))
                .ForMember(i => i.CustomerName, s => s.MapFrom(i => i.CustomerName))
                .ForMember(i => i.ShipDateStart, s => s.MapFrom(i => i.ShipDateStart))
                .ForMember(i => i.ShipDateEnd, s => s.MapFrom(i => i.ShipDateEnd))
                .ForMember(i => i.InvoiceId, s => s.MapFrom(i => i.InvoiceId))
                .ForMember(i => i.Peices, s => s.MapFrom(i => i.Peices))
                .ForMember(i => i.IsPaid, s => s.MapFrom(i => i.IsPaid))
                .ForMember(i => i.ElectricReceipt, s => s.MapFrom(i => i.ElectronicReceipt))
                .ForMember(i => i.CreateDate, s => s.MapFrom(i => i.CreateDate))
                .ForMember(i => i.UpdateTime, s => s.MapFrom(i => i.UpdateDate))
                .ForMember(i => i.PaidDate, s => s.MapFrom(i => i.PaidDate));

            CreateMap<CustomerShippingFee, CustomerShippingFeeEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.CustomerCode, s => s.MapFrom(i => i.CustomerCode))
                .ForMember(i => i.CustomerName, s => s.MapFrom(i => i.CustomerName))
                .ForMember(i => i.S60, s => s.MapFrom(i => i.S60Cm))
                .ForMember(i => i.S90, s => s.MapFrom(i => i.S90Cm))
                .ForMember(i => i.S120, s => s.MapFrom(i => i.S120Cm))
                .ForMember(i => i.S110, s => s.MapFrom(i => i.S110Cm))
                .ForMember(i => i.S150, s => s.MapFrom(i => i.S150Cm))
                .ForMember(i => i.S180, s => s.MapFrom(i => i.S180))
                .ForMember(i => i.StationName, s => s.MapFrom(i => i.StationName))
                .ForMember(i => i.NoOne, s => s.MapFrom(i => i.No1Bag))
                .ForMember(i => i.NoTwo, s => s.MapFrom(i => i.No2Bag))
                .ForMember(i => i.NoThree, s => s.MapFrom(i => i.No3Bag))
                .ForMember(i => i.NoFour, s => s.MapFrom(i => i.No4Bag))
                .ForMember(i => i.HolidayPrice, s => s.MapFrom(i => i.Holiday))
                .ForMember(i => i.PieceRate, s => s.MapFrom(i => i.PieceRate))
                .ForMember(i => i.LianYun, s => s.MapFrom(i => i.LianYun))
                .ForMember(i => i.EffectiveDate, s => s.MapFrom(i => i.EffectiveDate));

            CreateMap<CustomerShippingFeeEntity, CustomerShippingFee>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.CustomerCode, s => s.MapFrom(i => i.CustomerCode))
                .ForMember(i => i.CustomerName, s => s.MapFrom(i => i.CustomerName))
                .ForMember(i => i.S60Cm, s => s.MapFrom(i => i.S60))
                .ForMember(i => i.S90Cm, s => s.MapFrom(i => i.S90))
                .ForMember(i => i.S120Cm, s => s.MapFrom(i => i.S120))
                .ForMember(i => i.S110Cm, s => s.MapFrom(i => i.S110))
                .ForMember(i => i.S150Cm, s => s.MapFrom(i => i.S150))
                .ForMember(i => i.S180, s => s.MapFrom(i => i.S180))
                .ForMember(i => i.StationName, s => s.MapFrom(i => i.StationName))
                .ForMember(i => i.No1Bag, s => s.MapFrom(i => i.NoOne))
                .ForMember(i => i.No2Bag, s => s.MapFrom(i => i.NoTwo))
                .ForMember(i => i.No3Bag, s => s.MapFrom(i => i.NoThree))
                .ForMember(i => i.No4Bag, s => s.MapFrom(i => i.NoFour))
                .ForMember(i => i.Holiday, s => s.MapFrom(i => i.HolidayPrice))
                .ForMember(i => i.PieceRate, s => s.MapFrom(i => i.PieceRate))
                .ForMember(i => i.LianYun, s => s.MapFrom(i => i.LianYun))
                .ForMember(i => i.EffectiveDate, s => s.MapFrom(i => i.EffectiveDate));

            CreateMap<CustomerShippingFee, CustomerShippingFeeHistory>()
                .ForMember(i => i.CustomerCode, s => s.MapFrom(i => i.CustomerCode))
                .ForMember(i => i.CustomerName, s => s.MapFrom(i => i.CustomerName))
                .ForMember(i => i.S60Cm, s => s.MapFrom(i => i.S60Cm))
                .ForMember(i => i.S90Cm, s => s.MapFrom(i => i.S90Cm))
                .ForMember(i => i.S120Cm, s => s.MapFrom(i => i.S120Cm))
                .ForMember(i => i.S110Cm, s => s.MapFrom(i => i.S110Cm))
                .ForMember(i => i.S150Cm, s => s.MapFrom(i => i.S150Cm))
                .ForMember(i => i.S180, s => s.MapFrom(i => i.S180))
                .ForMember(i => i.StationName, s => s.MapFrom(i => i.StationName))
                .ForMember(i => i.No1Bag, s => s.MapFrom(i => i.No1Bag))
                .ForMember(i => i.No2Bag, s => s.MapFrom(i => i.No2Bag))
                .ForMember(i => i.No3Bag, s => s.MapFrom(i => i.No3Bag))
                .ForMember(i => i.No4Bag, s => s.MapFrom(i => i.No4Bag))
                .ForMember(i => i.Holiday, s => s.MapFrom(i => i.Holiday))
                .ForMember(i => i.PieceRate, s => s.MapFrom(i => i.PieceRate))
                .ForMember(i => i.LianYun, s => s.MapFrom(i => i.LianYun))
                .ForMember(i => i.EffectiveDate, s => s.MapFrom(i => i.EffectiveDate))
                .ForMember(i => i.Id, opt => opt.Ignore());
        }

        private string GetCategoryShowName(string subpoenaCategory)
        {
            switch (subpoenaCategory)
            {
                case "41":
                    return "代收";
                case "51":
                    return "應收帳款";
                case "31":
                    return "預購袋";
                case "11":
                    return "元付";
            }
            return null;
        }
    }
}
