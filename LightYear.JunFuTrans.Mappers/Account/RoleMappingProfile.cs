﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.Mappers.Account
{
    public class RoleMappingProfile : AutoMapper.Profile
    {
        public RoleMappingProfile()
        {
            this.CreateMap<RoleInfoEntity, Role>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.RoleId))
                .ForMember(i => i.RoleName, s => s.MapFrom(i => i.Name))
                .ForMember(i => i.RoleEnName, s => s.MapFrom(i => i.EnName));

            this.CreateMap<Role, RoleInfoEntity>()
                .ForMember(i => i.RoleId, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.Name, s => s.MapFrom(i => i.RoleName))
                .ForMember(i => i.EnName, s => s.MapFrom(i => i.RoleEnName));

            this.CreateMap<SystemFunctionRoleMapping, RoleFunctionMappingEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.RoleId, s => s.MapFrom(i => i.RoleId))
                .ForMember(i => i.FunctionId, s => s.MapFrom(i => i.FunctionId))
                .ForMember(i => i.FunctionValue, s => s.MapFrom(i => i.FunctionValue));

            this.CreateMap<RoleFunctionMappingEntity, SystemFunctionRoleMapping>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.RoleId, s => s.MapFrom(i => i.RoleId))
                .ForMember(i => i.FunctionId, s => s.MapFrom(i => i.FunctionId))
                .ForMember(i => i.FunctionValue, s => s.MapFrom(i => i.FunctionValue));

            this.CreateMap<UserFunctionMappingEntity, SystemFunctionUserMapping>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.UserId, s => s.MapFrom(i => i.UserId))
                .ForMember(i => i.UserType, s => s.MapFrom(i => i.UserType))
                .ForMember(i => i.FunctionId, s => s.MapFrom(i => i.FunctionId))
                .ForMember(i => i.FunctionValue, s => s.MapFrom(i => i.FunctionValue));

            this.CreateMap<SystemFunctionUserMapping, UserFunctionMappingEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.UserId, s => s.MapFrom(i => i.UserId))
                .ForMember(i => i.UserType, s => s.MapFrom(i => i.UserType))
                .ForMember(i => i.FunctionId, s => s.MapFrom(i => i.FunctionId))
                .ForMember(i => i.FunctionValue, s => s.MapFrom(i => i.FunctionValue));
        }
    }
}
