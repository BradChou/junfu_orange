﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.Mappers.Account
{
    public class DriverMappingProfile : AutoMapper.Profile
    {
        public DriverMappingProfile()
        {
            //from DriverEntity to TbDriver
            this.CreateMap<DriverEntity,TbDriver>()
                .ForMember(i => i.DriverId, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.DriverCode, s => s.MapFrom(i => i.DriverCode))
                .ForMember(i => i.DriverName, s => s.MapFrom(i => i.DriverName))
                .ForMember(i => i.DriverMobile, s => s.MapFrom(i => i.DriverMobile))
                .ForMember(i => i.SupplierCode, s => s.MapFrom(i => i.SupplierCode))
                .ForMember(i => i.EmpCode, s => s.MapFrom(i => i.EmpCode))
                .ForMember(i => i.SiteCode, s => s.MapFrom(i => i.SiteCode))
                .ForMember(i => i.Area, s => s.MapFrom(i => i.Area))
                .ForMember(i => i.LoginPassword, s => s.MapFrom(i => i.LoginPassword))
                .ForMember(i => i.LoginDate, s => s.MapFrom(i => i.LoginDate))
                .ForMember(i => i.ActiveFlag, s => s.MapFrom(i => i.ActiveFlag))
                .ForMember(i => i.ExternalDriver, s => s.MapFrom(i => i.ExternalDriver))
                .ForMember(i => i.PushId, s => s.MapFrom(i => i.PushId))
                .ForMember(i => i.WgsX, s => s.MapFrom(i => i.WgsX))
                .ForMember(i => i.WgsY, s => s.MapFrom(i => i.WgsY))
                .ForMember(i => i.Station, s => s.MapFrom(i => i.Station))
                .ForMember(i => i.Cdate, s => s.MapFrom(i => i.CDate))
                .ForMember(i => i.Cuser, s => s.MapFrom(i => i.CUser))
                .ForMember(i => i.Udate, s => s.MapFrom(i => i.UDate))
                .ForMember(i => i.Uuser, s => s.MapFrom(i => i.UUser))
                .ForMember(i => i.Office, s => s.MapFrom(i => i.Office));

            //from TbDriver to DriverEntity
            this.CreateMap<TbDriver, DriverEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.DriverId))
                .ForMember(i => i.DriverCode, s => s.MapFrom(i => i.DriverCode))
                .ForMember(i => i.DriverName, s => s.MapFrom(i => i.DriverName))
                .ForMember(i => i.DriverMobile, s => s.MapFrom(i => i.DriverMobile))
                .ForMember(i => i.SupplierCode, s => s.MapFrom(i => i.SupplierCode))
                .ForMember(i => i.EmpCode, s => s.MapFrom(i => i.EmpCode))
                .ForMember(i => i.SiteCode, s => s.MapFrom(i => i.SiteCode))
                .ForMember(i => i.Area, s => s.MapFrom(i => i.Area))
                .ForMember(i => i.LoginPassword, s => s.MapFrom(i => i.LoginPassword))
                .ForMember(i => i.LoginDate, s => s.MapFrom(i => i.LoginDate))
                .ForMember(i => i.ActiveFlag, s => s.MapFrom(i => i.ActiveFlag))
                .ForMember(i => i.ExternalDriver, s => s.MapFrom(i => i.ExternalDriver))
                .ForMember(i => i.PushId, s => s.MapFrom(i => i.PushId))
                .ForMember(i => i.WgsX, s => s.MapFrom(i => i.WgsX))
                .ForMember(i => i.WgsY, s => s.MapFrom(i => i.WgsY))
                .ForMember(i => i.Station, s => s.MapFrom(i => i.Station))
                .ForMember(i => i.CDate, s => s.MapFrom(i => i.Cdate))
                .ForMember(i => i.CUser, s => s.MapFrom(i => i.Cuser))
                .ForMember(i => i.UDate, s => s.MapFrom(i => i.Udate))
                .ForMember(i => i.UUser, s => s.MapFrom(i => i.Uuser))
                .ForMember(i => i.Office, s => s.MapFrom(i => i.Office));

            this.CreateMap<TbDriver, UserInfoEntity>()
                .ForMember(i => i.UsersId, s => s.MapFrom(i => i.DriverId))
                .ForMember(i => i.UserAccount, s => s.MapFrom(i => i.DriverCode))
                .ForMember(i => i.UserName, s => s.MapFrom(i => i.DriverName))
                .ForMember(i => i.ShowName, s => s.MapFrom(i => i.DriverCode + "-( " + i.DriverName + " )"))
                .ForMember(i => i.ActionStation, s => s.MapFrom(i => i.Station))
                .ForMember(i => i.AccountType, s => s.MapFrom(i => AccountType.Driver))
                .ForMember(i => i.CookieExpiration, s => s.MapFrom(i => DateTime.Now.AddDays(1)))
                .ForMember(i => i.Station, s => s.MapFrom(i => i.Station));

            this.CreateMap<TbAccount, UserInfoEntity>()
                .ForMember(i => i.UsersId, s => s.MapFrom(i => i.AccountId))
                .ForMember(i => i.UserAccount, s => s.MapFrom(i => i.AccountCode))
                .ForMember(i => i.UserName, s => s.MapFrom(i => i.UserName))
                .ForMember(i => i.ShowName, s => s.MapFrom(i => i.AccountCode + "-( " + i.UserName + " )" ))
                .ForMember(i => i.AccountType, s => s.MapFrom(i => 1))
                .ForMember(i => i.ActionStation, s => s.MapFrom(i => string.Empty))
                .ForMember(i => i.AccountType, s => s.MapFrom(i => AccountType.Customer))
                .ForMember(i => i.CookieExpiration, s => s.MapFrom(i => DateTime.Now.AddDays(1)))
                .ForMember(i => i.Station, s => s.MapFrom(i => string.Empty));
        }
    }
}
