﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.BL.BE.SignPaperPhoto;

namespace LightYear.JunFuTrans.Mappers.SignPaperPhoto
{
    public class SignPaperPhotoMappingProfile :AutoMapper.Profile
    {
        public SignPaperPhotoMappingProfile()
        {
            CreateMap<TcDeliveryRequest, SignPaperPhotoOutputEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(d => d.RequestId))
                .ForMember(i => i.CheckNumber, s => s.MapFrom(d => d.CheckNumber))
                .ForMember(i => i.OrderNumber, s => s.MapFrom(d => d.OrderNumber))
                .ForMember(i => i.ReceiveContact, s => s.MapFrom(d => d.ReceiveContact))
                .ForMember(i => i.ReceiveAddress, s => s.MapFrom(d => d.ReceiveCity + d.ReceiveArea + d.ReceiveAddress))
                .ForMember(i => i.PrintDate, s => s.MapFrom(d => d.PrintDate))
                .ForMember(i => i.ShipDate, s => s.MapFrom(d => d.ShipDate));
        }
    }
}
