﻿using LightYear.JunFuTrans.BL.BE.DeliveryRequest;
using LightYear.JunFuTrans.BL.BE.PickUpRequest;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.Mappers.PickUpRequest
{
    public class PickUpRequestMappingProfile : AutoMapper.Profile
    {
        public PickUpRequestMappingProfile()
        {
            CreateMap<PickUpRequestLog, PickUpInfoShowEntity>()
                .ForMember(i => i.NotifyDate, s => s.MapFrom(i => i.Cdate))
                .ForMember(i => i.PickupRequestId, s => s.MapFrom(i => i.PickUpId))
                .ForMember(i => i.CustomerCode, s => s.MapFrom(i => i.RequestCustomerCode))  // 原本的 customer_code 存的是帳號
                .ForMember(i => i.ShipType, s => s.MapFrom(i => i.PackageType))
                .ForMember(i => i.VehicleType, s => s.MapFrom(i => i.VechileType))
                .ForMember(i => i.PickUpAvailTime, s => s.MapFrom(i => i.PickUpDate))
                .ForMember(i => i.ShouldPickUpPieces, s => s.MapFrom(i => i.PickUpPieces))
                .ForMember(i => i.Remark, s => s.MapFrom(i => i.Remark))
                .ForMember(i => i.ReassignMd, s => s.MapFrom(i => i.ReassignMd))
                .ForMember(i => i.MdUpdateUser, s => s.MapFrom(i => i.MdUuser));

            CreateMap<PickUpInfoShowEntity, PickUpRequestLog>()
                .ForMember(i => i.Cdate, s => s.MapFrom(i => i.NotifyDate))
                .ForMember(i => i.PickUpId, s => s.MapFrom(i => i.PickupRequestId))
                .ForMember(i => i.RequestCustomerCode, s => s.MapFrom(i => i.CustomerCode))  // 原本的 customer_code 存的是帳號
                .ForMember(i => i.PackageType, s => s.MapFrom(i => i.ShipType))
                .ForMember(i => i.VechileType, s => s.MapFrom(i => i.VehicleType))
                .ForMember(i => i.PickUpDate, s => s.MapFrom(i => i.PickUpAvailTime))
                .ForMember(i => i.PickUpPieces, s => s.MapFrom(i => i.ShouldPickUpPieces))
                .ForMember(i => i.Remark, s => s.MapFrom(i => i.Remark))
                .ForMember(i => i.ReassignMd, s => s.MapFrom(i => i.ReassignMd))
                .ForMember(i => i.MdUuser, s => s.MapFrom(i => i.MdUpdateUser));

            CreateMap<TbItemCode, PickUpOptionsEntity>()
                .ForMember(i => i.Name, s => s.MapFrom(i => i.CodeName))
                .ForMember(i => i.OptionId, s => s.MapFrom(i => i.CodeId));  // 原本的 customer_code 存的是帳號

            CreateMap<PickUpInfoShowEntity, TtDeliveryScanLog>()
                .ForMember(s => s.CheckNumber, sl => sl.MapFrom(s => s.CheckNumber))
                .ForMember(s => s.ReceiveOption, sl => sl.MapFrom(s => s.OptionSelector.OptionId));
        }
    }
}
