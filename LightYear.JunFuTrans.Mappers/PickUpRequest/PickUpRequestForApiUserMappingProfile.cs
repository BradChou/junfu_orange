﻿using LightYear.JunFuTrans.BL.BE.PickUpRequest;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.Mappers.PickUpRequest
{
    public class PickUpRequestForApiUserMappingProfile : AutoMapper.Profile
    {
        public PickUpRequestForApiUserMappingProfile()
        {
            CreateMap<PickupRequestForApiuser, PickUpInfoShowEntity>()
                .ForMember(i => i.NotifyDate, s => s.MapFrom(i => i.RequestDate))
                .ForMember(i => i.CustomerCode, s => s.MapFrom(i => i.CustomerCode))
                .ForMember(i => i.ShouldPickUpPieces, s => s.MapFrom(i => i.Pieces))
                .ForMember(i => i.AssignedMd, s => s.MapFrom(i => i.Md))
                .ForMember(i => i.AssignedSd, s => s.MapFrom(i => i.Sd))
                .ForMember(i => i.CheckNumber, s => s.MapFrom(i => i.CheckNumber))
                .ForMember(i => i.PutOrder, s => s.MapFrom(i => i.Putorder))
                .ForMember(i => i.ReassignMd, s => s.MapFrom(i => i.ReassignMd))
                .ForMember(i => i.MdUpdateUser, s => s.MapFrom(i => i.MdUuser))
                .ForMember(i => i.PickUpStation, s => s.MapFrom(i => i.SupplierCode))
                .ForMember(i => i.PickupRequestId, s => s.MapFrom(i => i.Id));

            CreateMap<PickUpInfoShowEntity, PickupRequestForApiuser>()
                .ForMember(i => i.RequestDate, s => s.MapFrom(i => i.NotifyDate))
                .ForMember(i => i.CustomerCode, s => s.MapFrom(i => i.CustomerCode))
                .ForMember(i => i.Pieces, s => s.MapFrom(i => i.ShouldPickUpPieces))
                .ForMember(i => i.Md, s => s.MapFrom(i => i.AssignedMd))
                .ForMember(i => i.Sd, s => s.MapFrom(i => i.AssignedSd))
                .ForMember(i => i.CheckNumber, s => s.MapFrom(i => i.CheckNumber))
                .ForMember(i => i.Putorder, s => s.MapFrom(i => i.PutOrder))
                .ForMember(i => i.ReassignMd, s => s.MapFrom(i => i.ReassignMd))
                .ForMember(i => i.MdUuser, s => s.MapFrom(i => i.MdUpdateUser))
                .ForMember(i => i.SupplierCode, s => s.MapFrom(i => i.PickUpStation))
                .ForMember(i => i.Id, s => s.MapFrom(i => i.PickupRequestId));
        }
    }
}
