﻿using LightYear.JunFuTrans.BL.BE.DeliveryRate;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.Mappers.DeliveryRate
{
    public class DeliveryRateMappingProfile : AutoMapper.Profile
    {
        public DeliveryRateMappingProfile()
        {
            // from TbStation to StationEntity
            CreateMap<TbStation, StationEntity>()
                .ForMember(i => i.StationName, s => s.MapFrom(i => i.StationName))
                .ForMember(i => i.StationScode, s => s.MapFrom(i => i.StationScode));

            // from TbCustomer to CustomerEntity
            CreateMap<TbCustomer, CustomerEntity>()
                .ForMember(i => i.CustomerCode, s => s.MapFrom(i => i.CustomerCode))
                .ForMember(i => i.CustomerName, s => s.MapFrom(i => i.CustomerName));

            // from StationArea to StationEntity
            CreateMap<StationArea, StationEntity>()
                .ForMember(i => i.StationName, s => s.MapFrom(i => i.StationName))
                .ForMember(i => i.StationCode, s => s.MapFrom(i => i.StationCode));
        }
    }
}
