﻿using LightYear.JunFuTrans.BL.BE.PalletCustomerWriteOff;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.Mappers.PalletCustomerWriteOff
{
    public class PalletCustomerWriteOffMappingProfile : AutoMapper.Profile
    {
        public PalletCustomerWriteOffMappingProfile()
        {
            CreateMap<TbSupplier, PalletCustomerDropDownList>()
                .ForMember(i => i.SupplierCode, s => s.MapFrom(i => i.SupplierCode))
                .ForMember(i => i.SupplierCodeAndName, s => s.MapFrom(i => (i.SupplierCode + "-" + i.SupplierName)));
        }
    }
}
