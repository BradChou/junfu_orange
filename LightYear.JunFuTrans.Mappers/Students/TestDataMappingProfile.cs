﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.StudentTest;
using LightYear.JunFuTrans.DA.LightYearTest;

namespace LightYear.JunFuTrans.Mappers.Students
{
    public class TestDataMappingProfile : AutoMapper.Profile
    {
        public TestDataMappingProfile()
        {
            //from Student to PersonEntity
            this.CreateMap<TestData, FrontEndDataEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.Field1, s => s.MapFrom(i => i.TestData_))
                .ForMember(i => i.Field2, s => s.MapFrom(i => i.TestData2))
                .ForMember(i => i.Field3, s => s.MapFrom(i => i.TestData3));

            //from PersonEntity to Student
            this.CreateMap<FrontEndDataEntity, TestData>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.TestData_, s => s.MapFrom(i => i.Field1))
                .ForMember(i => i.TestData2, s => s.MapFrom(i => i.Field2))
                .ForMember(i => i.TestData3, s => s.MapFrom(i => i.Field3));
        }
    }
}
