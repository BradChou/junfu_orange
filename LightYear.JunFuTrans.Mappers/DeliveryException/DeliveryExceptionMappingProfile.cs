﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.BL.BE.DeliveryException;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.Mappers.DeliveryException
{
    public class DeliveryExceptionMappingProfile : AutoMapper.Profile
    {
        public DeliveryExceptionMappingProfile()
        {
            this.CreateMap<DeliveryExceptionEntity, ExceptionReport>()
                .ForMember(e => e.Id, d => d.MapFrom(e => e.Id))
                .ForMember(e => e.Cdate, d => d.MapFrom(e => e.CDate))
                .ForMember(e => e.Udate, d => d.MapFrom(e => e.UDate))
                .ForMember(e => e.ReportStation, d => d.MapFrom(e => e.ReportStation))
                .ForMember(e => e.CheckNumber, d => d.MapFrom(e => e.CheckNumber))
                .ForMember(e => e.ReportMatter, d => d.MapFrom(e => e.ReportMatter))
                .ForMember(e => e.SketchMatter, d => d.MapFrom(e => e.SketchMatter))
                .ForMember(e => e.HandleResult, d => d.MapFrom(e => e.HandleResult))
                .ForMember(e => e.IsClosed, d => d.MapFrom(e => e.IsClosed))
                .ForMember(e => e.RelativeCheckNumber, d => d.MapFrom(e => e.RelativeCheckNumber))
                .ForMember(e => e.ReportName, d => d.MapFrom(e => e.ReportName))
                .ForMember(e => e.PicLeft, d => d.MapFrom(e => e.picLeft))
                .ForMember(e => e.PicRight, d => d.MapFrom(e => e.picRight))
                .ForMember(e => e.PicFront, d => d.MapFrom(e => e.picFront))
                .ForMember(e => e.PicBack, d => d.MapFrom(e => e.picBack))
                .ForMember(e => e.PicTop, d => d.MapFrom(e => e.picTop))
                .ForMember(e => e.PicBottom, d => d.MapFrom(e => e.picBottom))
                .ForMember(e => e.Sendstation, d => d.MapFrom(e => e.SendStaion))
            .ForMember(e => e.Desstation, d => d.MapFrom(e => e.DesStaion))
            .ForMember(e => e.OtherMatters, d => d.MapFrom(e => e.OtherMatters));

            this.CreateMap<ExceptionReport, DeliveryExceptionEntity>()
                .ForMember(d => d.Id, e => e.MapFrom(d => d.Id))
                .ForMember(d => d.CDate, e => e.MapFrom(d => d.Cdate))
                .ForMember(d => d.UDate, e => e.MapFrom(d => d.Udate))
                .ForMember(d => d.ReportStation, e => e.MapFrom(d => d.ReportStation))
                .ForMember(d => d.CheckNumber, e => e.MapFrom(d => d.CheckNumber))
                .ForMember(d => d.ReportMatter, e => e.MapFrom(d => d.ReportMatter))
                .ForMember(d => d.SketchMatter, e => e.MapFrom(d => d.SketchMatter))
                .ForMember(d => d.HandleResult, e => e.MapFrom(d => d.HandleResult))
                .ForMember(d => d.IsClosed, e => e.MapFrom(d => d.IsClosed))
                .ForMember(d => d.RelativeCheckNumber, e => e.MapFrom(d => d.RelativeCheckNumber))
                .ForMember(d => d.ReportName, e => e.MapFrom(d => d.ReportName))
                .ForMember(d => d.picLeft, e => e.MapFrom(d => d.PicLeft))
                .ForMember(d => d.picRight, e => e.MapFrom(d => d.PicRight))
                .ForMember(d => d.picFront, e => e.MapFrom(d => d.PicFront))
                .ForMember(d => d.picBack, e => e.MapFrom(d => d.PicBack))
                .ForMember(d => d.picTop, e => e.MapFrom(d => d.PicTop))
                .ForMember(d => d.picBottom, e => e.MapFrom(d => d.PicBottom))
                .ForMember(e => e.SendStaion, d => d.MapFrom(e => e.Sendstation))
                .ForMember(e => e.DesStaion, d => d.MapFrom(e => e.Desstation))
                .ForMember(d => d.OtherMatters, e => e.MapFrom(d => d.OtherMatters));

            this.CreateMap<ExceptionReportLog, DeliveryExceptionEntity>()
                .ForMember(l => l.CheckNumber, e => e.MapFrom(l => l.CheckNumber))
                .ForMember(l => l.HandleResult, e => e.MapFrom(l => l.HandleResult))
                .ForMember(l => l.HandleStation, e => e.MapFrom(l => l.HandleStation))
                .ForMember(l => l.CDate, e => e.MapFrom(l => l.Cdate))
                .ForMember(l => l.UDate, e => e.MapFrom(l => l.Udate));

            this.CreateMap<DeliveryExceptionEntity, ExceptionReportLog>()
                .ForMember(e => e.CheckNumber, l => l.MapFrom(e => e.CheckNumber))
                .ForMember(e => e.HandleResult, l => l.MapFrom(e => e.HandleResult))
                .ForMember(e => e.HandleStation, l => l.MapFrom(e => e.HandleStation))
                .ForMember(e => e.Cdate, l => l.MapFrom(e => e.CDate))
                .ForMember(e => e.Udate, l => l.MapFrom(e => e.UDate));

            this.CreateMap<CreateDeliveryExceptionEntity, TcDeliveryRequest>()
                .ForMember(t => t.RequestId, d => d.MapFrom(t => t.Id))
                .ForMember(t => t.CheckNumber, d => d.MapFrom(t => t.CheckNumber))
                .ForMember(t => t.SendContact, d => d.MapFrom(t => t.SendContact))
                .ForMember(t => t.SupplierCode, d => d.MapFrom(t => t.SupplierCode))
                .ForMember(t => t.Pieces, d => d.MapFrom(t => t.Pieces))
                .ForMember(t => t.ReceiveContact, d => d.MapFrom(t => t.ReceiveContact))
                .ForMember(t => t.AreaArriveCode, d => d.MapFrom(t => t.AreaArriveCode));

            this.CreateMap<TcDeliveryRequest, CreateDeliveryExceptionEntity>()
                .ForMember(d => d.Id, t => t.MapFrom(d => d.RequestId))
                .ForMember(d => d.CheckNumber, t => t.MapFrom(d => d.CheckNumber))
                .ForMember(d => d.SendContact, t => t.MapFrom(d => d.SendContact))
                .ForMember(d => d.SupplierCode, t => t.MapFrom(d => d.SupplierCode))
                .ForMember(d => d.Pieces, t => t.MapFrom(d => d.Pieces))
                .ForMember(d => d.ReceiveContact, t => t.MapFrom(d => d.ReceiveContact))
                .ForMember(d => d.AreaArriveCode, t => t.MapFrom(d => d.AreaArriveCode));

        }

    }
}
