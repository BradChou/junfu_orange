﻿using LightYear.JunFuTrans.BL.BE.PalletACSectionShouldPay;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.Mappers.PalletACSectionShouldPay
{
    public class PalletACSectionShouldPayMappingProfile : AutoMapper.Profile
    {
        public PalletACSectionShouldPayMappingProfile()
        {
            CreateMap<PalletACSectionShouldPayEntity, PalletAcSectionShouldPay>()
                .ForMember(i => i.ASectionPayment, s => s.MapFrom(i => i.ASectionPayment))
                .ForMember(i => i.CSectionShippingFee, s => s.MapFrom(i => i.CShippingFee))
                .ForMember(i => i.RemoteSectionFee, s => s.MapFrom(i => i.RemoteFee))
                .ForMember(i => i.SpecialFee, s => s.MapFrom(i => i.CSectionSpecialFee))
                .ForMember(i => i.UpdateUser, s => s.MapFrom(i => i.UpdateUser))
                .ForMember(i => i.RequestId, s => s.MapFrom(i => i.RequestId))
                .ForMember(i => i.Id, s => s.MapFrom(i => i.PalletACSectionShouldPayId));
        }
    }
}
