﻿using LightYear.JunFuTrans.BL.BE.TMS;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.Mappers.TMS
{
    public class TMSMappingProfile : AutoMapper.Profile
    {
        public TMSMappingProfile()
        {
            CreateMap<BSectionStop, BL.BE.TMS.StationEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.Name, s => s.MapFrom(i => i.Name))
                .ForMember(i => i.CityId, s => s.MapFrom(i => i.CityId))
                .ForMember(i => i.DistrictId, s => s.MapFrom(i => i.DistrictId))
                .ForMember(i => i.Address, s => s.MapFrom(i => i.Address))
                .ForMember(i => i.Position, s => s.MapFrom(i => i.Position))
                .ForMember(i => i.IsActive, s => s.MapFrom(i => i.IsActive));

            CreateMap<TmsBoxCountEntity, LtWarehouseBoxCount>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.WarehouseId, s => s.MapFrom(i => i.BSectionStopId))
                .ForMember(i => i.ShipDate, s => s.MapFrom(i => i.ShipDate))
                .ForMember(i => i.ActualFlowCount, s => s.MapFrom(i => i.ActualFlowBoxCount))
                .ForMember(i => i.ActualMixCount, s => s.MapFrom(i => i.ActualMixBoxCount));
        }
    }
}
