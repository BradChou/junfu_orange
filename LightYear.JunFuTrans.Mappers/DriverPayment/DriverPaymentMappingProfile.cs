﻿using LightYear.JunFuTrans.BL.BE.DriverToStationPayment;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.Mappers.DriverToStationPayment
{
    public class DriverPaymentMappingProfile : AutoMapper.Profile
    {
        public DriverPaymentMappingProfile()
        {
            //from TbDriver to DriverPaymentEntity
            this.CreateMap<TbDriver, DriverPaymentEntity>()
                .ForMember(i => i.DriverCode, s => s.MapFrom(i => i.DriverCode))
                .ForMember(i => i.DriverName, s => s.MapFrom(i => i.DriverName))
                .ForMember(i => i.StationScode, s => s.MapFrom(i => i.Station));

            //from DriverPaymentEntity to DriverPaymentSheet
            this.CreateMap<DriverPaymentEntity, DriverPaymentSheet>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.CreateDate, s => s.MapFrom(i => i.CreateDate))
                .ForMember(i => i.DriverCode, s => s.MapFrom(i => i.DriverCode))
                .ForMember(i => i.DriverName, s => s.MapFrom(i => i.DriverName))
                .ForMember(i => i.StationName, s => s.MapFrom(i => i.StationName))
                .ForMember(i => i.StationScode, s => s.MapFrom(i => i.StationScode))
                //.ForMember(i => i.YuanfuCount, s => s.MapFrom(i => i.YuanFuCount))
                //.ForMember(i => i.YuanfuMoney, s => s.MapFrom(i => i.YuanFuMoney))
                //.ForMember(i => i.CashOnDeliveryCount, s => s.MapFrom(i => i.CashOnDeliveryCount))
                //.ForMember(i => i.CashOnDeliveryMoney, s => s.MapFrom(i => i.CashOnDeliveryMoney))
                //.ForMember(i => i.RemittanceOnDelivery, s => s.MapFrom(i => i.RemittanceOnDelivery))
                //.ForMember(i => i.CheckOnDelivery, s => s.MapFrom(i => i.CheckOnDelivery))
                //.ForMember(i => i.DestinationCollectCount, s => s.MapFrom(i => i.DestinationCount))
                //.ForMember(i => i.DestinationCollectMoney, s => s.MapFrom(i => i.DestinationMoney))
                .ForMember(i => i.TransactionLastFive, s => s.MapFrom(i => i.TranscationLastFive))
                .ForMember(i => i.RemittanceAmount, s => s.MapFrom(i => i.RemittanceAmount))
                .ForMember(i => i.CheckAmount, s => s.MapFrom(i => i.CheckAmount))
                .ForMember(i => i.CoinsMoney, s => s.MapFrom(i => i.CoinsAmount));

            //from DriverPaymentSheet to DriverPaymentEntity
            this.CreateMap<DriverPaymentSheet, DriverPaymentEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.CreateDate, s => s.MapFrom(i => i.CreateDate))
                .ForMember(i => i.DriverCode, s => s.MapFrom(i => i.DriverCode))
                .ForMember(i => i.DriverName, s => s.MapFrom(i => i.DriverName))
                .ForMember(i => i.StationName, s => s.MapFrom(i => i.StationName))
                .ForMember(i => i.StationScode, s => s.MapFrom(i => i.StationScode))
                //.ForMember(i => i.YuanFuCount, s => s.MapFrom(i => i.YuanfuCount))
                //.ForMember(i => i.YuanFuMoney, s => s.MapFrom(i => i.YuanfuMoney))
                //.ForMember(i => i.CashOnDeliveryCount, s => s.MapFrom(i => i.CashOnDeliveryCount))
                //.ForMember(i => i.CashOnDeliveryMoney, s => s.MapFrom(i => i.CashOnDeliveryMoney))
                //.ForMember(i => i.RemittanceOnDelivery, s => s.MapFrom(i => i.RemittanceOnDelivery))
                //.ForMember(i => i.CheckOnDelivery, s => s.MapFrom(i => i.CheckOnDelivery))
                //.ForMember(i => i.DestinationCount, s => s.MapFrom(i => i.DestinationCollectCount))
                //.ForMember(i => i.DestinationMoney, s => s.MapFrom(i => i.DestinationCollectMoney))
                .ForMember(i => i.TranscationLastFive, s => s.MapFrom(i => i.TransactionLastFive))
                .ForMember(i => i.RemittanceAmount, s => s.MapFrom(i => i.RemittanceAmount))
                .ForMember(i => i.CheckAmount, s => s.MapFrom(i => i.CheckAmount))
                .ForMember(i => i.CoinsAmount, s => s.MapFrom(i => i.CoinsMoney));

            // from DriverPaymentDetailEntity to DriverPaymentDetail
            CreateMap<DriverPaymentDetailEntity, DriverPaymentDetail>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.CheckNumber, s => s.MapFrom(i => i.CheckNumber))
                .ForMember(i => i.SubpoenaCategory, s => s.MapFrom(i => i.SubpoenaCategory))
                .ForMember(i => i.CollectionMoney, s => s.MapFrom(i => i.CollectionMoney))
                .ForMember(i => i.PaymentSheetId, s => s.MapFrom(i => i.DriverPaymentId));

            // from DriverPaymentDetail to DriverPaymentDetailEntity
            CreateMap<DriverPaymentDetail, DriverPaymentDetailEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.CheckNumber, s => s.MapFrom(i => i.CheckNumber))
                .ForMember(i => i.SubpoenaCategory, s => s.MapFrom(i => i.SubpoenaCategory))
                .ForMember(i => i.CollectionMoney, s => s.MapFrom(i => i.CollectionMoney))
                .ForMember(i => i.DriverPaymentId, s => s.MapFrom(i => i.PaymentSheetId));

            // from DriverPaymentSheet to StationCollectMoneyEntity
            CreateMap<DriverPaymentSheet, StationCollectMoneyEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.DriverCode, s => s.MapFrom(i => i.DriverCode))
                .ForMember(i => i.DriverName, s => s.MapFrom(i => i.DriverName))
                .ForMember(i => i.NowPayAmount, s => s.MapFrom(i => i.AcutalPayAmount))
                .ForMember(i => i.CurrentPaymentBalace, s => s.MapFrom(i => i.PaymentBalance))
                .ForMember(i => i.TransactionLastFive, s => s.MapFrom(i => i.TransactionLastFive));

            //// from StationCollectMoneyEntity to DriverPaymentSheet
            //CreateMap<StationCollectMoneyEntity, DriverPaymentSheet>()
            //    .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
            //    .ForMember(i => i.DriverCode, s => s.MapFrom(i => i.DriverCode))
            //    .ForMember(i => i.DriverName, s => s.MapFrom(i => i.DriverName))
            //    .ForMember(i => i.CashOnDeliveryMoney, s => s.MapFrom(i => i.CashOnDeliveryMoney))
            //    .ForMember(i => i.DestinationCollectMoney, s => s.MapFrom(i => i.DestinationCollectMoney))
            //    .ForMember(i => i.CheckOnDelivery, s => s.MapFrom(i => i.CheckOnDeliveryMoney))
            //    .ForMember(i => i.PaymentBalance, s => s.MapFrom(i => i.PreviousPaymentBalace))
            //    .ForMember(i => i.AcutalPayAmount, s => s.MapFrom(i => i.ActualPayedMoney));

            // from DriverPaymentSheet to PaidHistoryEntity
            CreateMap<DriverPaymentSheet, PaidHistoryEntity>()
                .ForMember(i => i.TranscationLastFive, s => s.MapFrom(i => i.TransactionLastFive))
                .ForMember(i => i.RemittanceAmount, s => s.MapFrom(i => i.RemittanceAmount))
                .ForMember(i => i.CheckAmount, s => s.MapFrom(i => i.CheckAmount))
                .ForMember(i => i.CoinsAmount, s => s.MapFrom(i => i.CoinsMoney));

            CreateMap<TcDeliveryRequest, DriverPaymentDetailEntity>()
                .ForMember(i => i.CheckNumber, s => s.MapFrom(i => i.CheckNumber))
                .ForMember(i => i.SubpoenaCategory, s => s.MapFrom(i => i.SubpoenaCategory))
                .ForMember(i => i.SubpoenaCategoryShowName, s => s.MapFrom(i => GetSubpoenaCategoryShowName(i.SubpoenaCategory)))
                .ForMember(i => i.CategoryOrder, s => s.MapFrom(i => GetCategoryOrder(i.SubpoenaCategory)))
                .ForMember(i => i.CollectionMoney, s => s.MapFrom(i => i.CollectionMoney))
                .ForMember(i => i.PaidMethod, s => s.MapFrom(i => i.PaymentMethod ?? "現金"))
                .ForMember(i => i.AreaArriveCode, s => s.MapFrom(i => i.AreaArriveCode));
        }

        private string GetSubpoenaCategoryShowName(string subpoeanaCategory)
        {
            switch (subpoeanaCategory)
            {
                case "41":
                    return "41 代收";
                case "51":
                    return "51 應收帳款";
                case "31":
                    return "31 預購袋";
            }
            return null;
        }
        private int GetCategoryOrder(string subpoeanaCategory)
        {
            switch (subpoeanaCategory)
            {
                case "41":
                    return 3;
                case "51":
                    return 2;
                case "31":
                    return 1;
            }
            return 4;
        }

    }
}
