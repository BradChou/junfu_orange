﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LightYear.JunFuTrans.DA.JunFuTransDb
{
    public class TruckDepartmentConfiguration : IEntityTypeConfiguration<TruckDepartment>
    {
        public void Configure(EntityTypeBuilder<TruckDepartment> builder)
        {
            builder.ToTable("TruckDepartment", "dbo");
            builder.HasKey(x => x.id).HasName("PK_TruckDepartment").IsClustered();

            builder.Property(x => x.id).HasColumnName(@"id").HasColumnType("int").IsRequired().ValueGeneratedOnAdd().UseIdentityColumn();
            builder.Property(x => x.DepartmentCode).HasColumnName(@"DepartmentCode").HasColumnType("nvarchar(50)").IsRequired(true).HasMaxLength(50);
            builder.Property(x => x.DepartmentName).HasColumnName(@"DepartmentName").HasColumnType("nvarchar(50)").IsRequired(true).HasMaxLength(50);
            builder.Property(x => x.CreateUser).HasColumnName(@"CreateUser").HasColumnType("nvarchar(20)").IsRequired(true).HasMaxLength(20);
            builder.Property(x => x.CreateDate).HasColumnName(@"CreateDate").HasColumnType("datetime").IsRequired(true);
            builder.Property(x => x.UpdateUser).HasColumnName(@"UpdateUser").HasColumnType("nvarchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.UpdateDate).HasColumnName(@"UpdateDate").HasColumnType("datetime").IsRequired(false);
            builder.Property(x => x.IsActive).HasColumnName(@"IsActive").HasColumnType("bit").IsRequired(true);
        }
    }
}
