﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace LightYear.JunFuTrans.DA.JunFuTransDb
{
    class VW_TruckVendorPaymentLogDSERPＭergeDataConfiguration : IEntityTypeConfiguration<VW_TruckVendorPaymentLogDSERPＭergeData>
    {
        public void Configure(EntityTypeBuilder<VW_TruckVendorPaymentLogDSERPＭergeData> builder)
        {
            builder.ToView("VW_TruckVendorPaymentLogDSERPＭergeData", "dbo");
            builder.HasNoKey();

            builder.Property(x => x.FeeType).HasColumnName(@"FeeType").HasColumnType("nvarchar(20)").IsRequired(true).HasMaxLength(20);
            builder.Property(x => x.RegisteredDate).HasColumnName(@"RegisteredDate").HasColumnType("datetime").IsRequired(true);
            builder.Property(x => x.Company).HasColumnName(@"Company").HasColumnType("varchar(10)").IsRequired(true).HasMaxLength(10);
            builder.Property(x => x.VoucherCode).HasColumnName(@"VoucherCode").HasColumnType("varchar(10)").IsRequired(true).HasMaxLength(10);
            builder.Property(x => x.DinShinItemCode).HasColumnName(@"DinShinItemCode").HasColumnType("varchar(10)").IsRequired(true).HasMaxLength(10);
            builder.Property(x => x.SupplierUID).HasColumnName(@"SupplierUID").HasColumnType("varchar(10)").IsRequired(false).HasMaxLength(10);
            builder.Property(x => x.Accounts).HasColumnName(@"Accounts").HasColumnType("int").IsRequired(true);
            builder.Property(x => x.CollectionPayCode).HasColumnName(@"CollectionPayCode").HasColumnType("varchar(1)").IsRequired(true).HasMaxLength(1);
            builder.Property(x => x.CustomerUID).HasColumnName(@"CustomerUID").HasColumnType("varchar(10)").IsRequired(false).HasMaxLength(10);
            builder.Property(x => x.CollectionAccounts).HasColumnName(@"CollectionAccounts").HasColumnType("int").IsRequired(true);
            builder.Property(x => x.Spread).HasColumnName(@"Spread").HasColumnType("int").IsRequired(true);
            builder.Property(x => x.CarLicense).HasColumnName(@"CarLicense").HasColumnType("nvarchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.Note02).HasColumnName(@"Note02").HasColumnType("nvarchar(MAX)").IsRequired(false);
            builder.Property(x => x.Department).HasColumnName(@"Department").HasColumnType("nvarchar(50)").IsRequired(true).HasMaxLength(20);
            builder.Property(x => x.CompanyUID).HasColumnName(@"CompanyUID").HasColumnType("varchar(10)").IsRequired(true).HasMaxLength(10);
            builder.Property(x => x.InvoiceNumber).HasColumnName(@"InvoiceNumber").HasColumnType("varchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.InvoiceDate).HasColumnName(@"InvoiceDate").HasColumnType("datetime").IsRequired(false);
            builder.Property(x => x.CreateUser).HasColumnName(@"CreateUser").HasColumnType("nvarchar(10)").IsRequired(true).HasMaxLength(10);
            builder.Property(x => x.HasDownload).HasColumnName(@"HasDownload").HasColumnType("bit").IsRequired(true);
            builder.Property(x => x.MergeRandomCode).HasColumnName(@"MergeRandomCode").HasColumnType("nvarchar(10)").IsRequired(true).HasMaxLength(10);
            builder.Property(x => x.DownloadRandomCode).HasColumnName(@"DownloadRandomCode").HasColumnType("nvarchar(15)").IsRequired(true).HasMaxLength(15);

        }

    }
}
