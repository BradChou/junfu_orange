﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.DA.JunFuTransDb
{
	public class FeeTypeRelate
	{
		public int Id { get; set; }
		public int CodeId { get; set; }
		public string CodeName { get; set; }
		public bool HasInvoice { get; set; }
		public bool IsActive { get; set; }
		public string PayableCode { get; set; }
		public string ReceivableCode { get; set; }
		public string DinShinItemCode { get; set; }
		public string DinShinItemName { get; set; }
		public string StockPayCode { get; set; }
		public string StockPayName { get; set; }
		public string RevenueCode { get; set; }
		public string RevenueName { get; set; }
		public string CollectionReceiveCode { get; set; }
		public string CollectionReceiveName { get; set; }
		public string InvoiceSubjectName { get; set; }
	}
}
