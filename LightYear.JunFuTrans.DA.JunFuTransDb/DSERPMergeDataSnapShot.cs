﻿
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.DA.JunFuTransDb
{
    public class DSERPMergeDataSnapShot
    {
        public int Id { get; set; }
        public int TruckVendorPaymentLogDSERPId { get; set; }
        public int TruckVendorPaymentLogId { get; set; }
        public string FeeType { get; set; }
        public DateTime RegisteredDate { get; set; }
        public DateTime UploadDate { get; set; }
        public string Company { get; set; }
        public string VoucherCode { get; set; }
        public string DinShinItemCode { get; set; }
        public string SupplierUID { get; set; }
        public int Accounts { get; set; }
        public string CollectionPayCode { get; set; }
        public string CustomerUID { get; set; }
        public int CollectionAccounts { get; set; }
        public int Spread { get; set; }
        public string CarLicense { get; set; }
        public string Note02 { get; set; }
        public string Department { get; set; }
        public string CompanyUID { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateUser { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateUser { get; set; }
        public bool IsActive { get; set; }
        public bool HasDownload { get; set; }
        public DateTime? DownloadDate { get; set; }
        public string DownloadUser { get; set; }
        public string MergeRandomCode { get; set; }
        public string DownloadRandomCode { get; set; }

        public DSERPMergeDataSnapShot()
        {
            DownloadDate = DateTime.Now;
        }
        public static implicit operator DSERPMergeDataSnapShot(TruckVendorPaymentLogDSERP data)
        {

            return new DSERPMergeDataSnapShot
            {
                TruckVendorPaymentLogDSERPId = data.Id,
                TruckVendorPaymentLogId = data.TruckVendorPaymentLogId,
                FeeType = data.FeeType,
                RegisteredDate = data.RegisteredDate,
                UpdateDate = data.UpdateDate,
                Company = data.Company,
                VoucherCode = data.VoucherCode,
                DinShinItemCode = data.DinShinItemCode,
                SupplierUID = data.SupplierUID,
                Accounts = data.Accounts,
                CollectionPayCode = data.CollectionPayCode,
                CustomerUID = data.CustomerUID,
                CollectionAccounts = data.CollectionAccounts,
                Spread = data.Spread,
                CarLicense = data.CarLicense,
                Note02 = data.Note02,
                Department = data.Department,
                CompanyUID = data.CompanyUID,
                InvoiceNumber = data.InvoiceNumber,
                InvoiceDate = data.InvoiceDate,
                CreateUser = data.CreateUser,
                UploadDate = data.UploadDate,
                UpdateUser = data.UpdateUser,
                IsActive = data.IsActive,
                HasDownload = data.HasDownload,
                MergeRandomCode = data.MergeRandomCode,
                CreateDate = data.CreateDate
            };
        }

    }
}
