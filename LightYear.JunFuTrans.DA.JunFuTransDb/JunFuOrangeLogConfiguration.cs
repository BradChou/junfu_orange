﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LightYear.JunFuTrans.DA.JunFuTransDb
{
    public class JunFuOrangeLogConfiguration : IEntityTypeConfiguration<JunFuOrangeLog>
    {
        public void Configure(EntityTypeBuilder<JunFuOrangeLog> builder)
        {
            builder.ToTable("JunFuOrangeLog", "dbo");
            builder.HasKey(x => x.Id).HasName("PK_JunFuOrangeLog").IsClustered();

            builder.Property(x => x.Id).HasColumnName(@"Id").HasColumnType("int").IsRequired().ValueGeneratedOnAdd().UseIdentityColumn();
            builder.Property(x => x.Guid).HasColumnName(@"Guid").HasColumnType("varchar(50)").IsRequired(true).HasMaxLength(50);
            builder.Property(x => x.Account).HasColumnName(@"Account").HasColumnType("nvarchar(50)").IsRequired(false).HasMaxLength(50);
            builder.Property(x => x.RequestBody).HasColumnName(@"RequestBody").HasColumnType("nvarchar(MAX)").IsRequired(true).HasMaxLength(int.MaxValue);
            builder.Property(x => x.ResponseBody).HasColumnName(@"ResponseBody").HasColumnType("nvarchar(MAX)").IsRequired(true).HasMaxLength(int.MaxValue);
            builder.Property(x => x.RequestHeader).HasColumnName(@"RequestHeader").HasColumnType("nvarchar(MAX)").IsRequired(true).HasMaxLength(int.MaxValue);
            builder.Property(x => x.Path).HasColumnName(@"Path").HasColumnType("nvarchar(500)").IsRequired(true).HasMaxLength(500);
            builder.Property(x => x.QueryString).HasColumnName(@"QueryString").HasColumnType("nvarchar(MAX)").IsRequired(true).HasMaxLength(int.MaxValue);
            builder.Property(x => x.StartDate).HasColumnName(@"StartDate").HasColumnType("datetime").IsRequired(true);
            builder.Property(x => x.EndDate).HasColumnName(@"EndDate").HasColumnType("datetime").IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName(@"CreateDate").HasColumnType("datetime2(7)").IsRequired(true);
        }
    }
}
