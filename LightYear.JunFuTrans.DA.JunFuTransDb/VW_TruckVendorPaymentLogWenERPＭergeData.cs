﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.DA.JunFuTransDb
{
    public class VW_TruckVendorPaymentLogWenERPＭergeData
    {
        public string Year { get; set; }
        public string InvoicingNumber { get; set; }
        public DateTime RegisteredDate { get; set; }
        public string OrderType { get; set; }
        public string SupplierUID { get; set; }
        public string CustomerUID { get; set; }
        public string Number { get; set; }
        public string ProductCode { get; set; }
        public string TaxType { get; set; }
        public bool TaxInclude { get; set; }
        public int Quantity { get; set; }
        public int Payment { get; set; }
        public int UnitPrice { get; set; }
        public string ElecInovoice { get; set; }
        public string PaperInovoice { get; set; }
        public DateTime? ReconciliationDate { get; set; }
        public DateTime? CheckoutDate { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string ManufactureCode { get; set; }
        public int? ProductionQuantity { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string VoucherType { get; set; }
        public string Note01 { get; set; }
        public string Note02 { get; set; }
        public string NoteMain { get; set; }
        public string SupplierContact { get; set; }
        public string InvoicingSpecial { get; set; }
        public string SupplierUniformID { get; set; }
        public string SupplierAddress { get; set; }
        public string PackageType { get; set; }
        public string ForeignCurrencyCode { get; set; }
        public double? ExchangeRate { get; set; }
        public double? ForeignCurrencyUnitPrice { get; set; }
        public double? ForeignCurrencyPayment { get; set; }
        public string OrderNumber { get; set; }
        public string PurchaseNumber { get; set; }
        public string SalesCode { get; set; }
        public string Department { get; set; }
        public string ProjectCode { get; set; }
        public string CarLicense { get; set; }
        public string Company { get; set; }
        public string VendorName { get; set; }
    }
}
