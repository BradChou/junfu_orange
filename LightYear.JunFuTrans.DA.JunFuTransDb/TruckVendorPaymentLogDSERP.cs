﻿
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.DA.JunFuTransDb
{
    public class TruckVendorPaymentLogDSERP
    {
        public int Id { get; set; }
        public int TruckVendorPaymentLogId { get; set; }
        public string FeeType { get; set; }
        public DateTime RegisteredDate { get; set; }
        public DateTime UploadDate { get; set; }
        public string Company { get; set; }
        public string VoucherCode { get; set; }
        public string DinShinItemCode { get; set; }
        public string SupplierUID { get; set; }
        public int Accounts { get; set; }
        public string CollectionPayCode { get; set; }
        public string CustomerUID { get; set; }
        public int CollectionAccounts { get; set; }
        public int Spread { get; set; }
        public string CarLicense { get; set; }
        public string Note02 { get; set; }
        public string Department { get; set; }
        public string CompanyUID { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateUser { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateUser { get; set; }
        public bool IsActive { get; set; }
        public bool HasDownload { get; set; }
        public DateTime? DownloadDate { get; set; }
        public string DownloadUser { get; set; }
        public string MergeRandomCode { get; set; }

        public TruckVendorPaymentLogDSERP()
        {
            CreateDate = DateTime.Now;
            IsActive = true;
            HasDownload = false;
            MergeRandomCode = "";
        }
        public static implicit operator TruckVendorPaymentLogDSERP(TruckVendorPaymentLog data)
        {
            data.RegisteredDate ??= DateTime.Now;
            var year = ((DateTime)data.RegisteredDate).Year.ToString();
            data.Payment ??= 0;
            var payment = (int)data.Payment;
            return new TruckVendorPaymentLogDSERP
            {
                Note02 = data.Memo,
                CarLicense = data.CarLicense.ToUpper(),
                Department = data.Department,
                InvoiceNumber = data.InvoiceNumber,
                InvoiceDate = data.InvoiceDate,
                CreateUser = data.CreateUser
            };
        }
    }
}
