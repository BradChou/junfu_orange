﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.DA.JunFuTransDb
{
	public class TruckCompanyRelate
	{
		public int Id { get; set; }
		public int CodeId { get; set; }
		public string CodeName { get; set; }
		public bool IsActive { get; set; }
		public string CompanyUID { get; set; }
	}
}
