﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace LightYear.JunFuTrans.DA.JunFuTransDb
{
    class TruckVendorPaymentLogWenERPConfiguration : IEntityTypeConfiguration<TruckVendorPaymentLogWenERP>
    {
        public void Configure(EntityTypeBuilder<TruckVendorPaymentLogWenERP> builder)
        {
            builder.ToTable("TruckVendorPaymentLogWenERP", "dbo");
            builder.HasKey(x => x.Id).HasName("PK_TruckVendorPaymentLogWenERP").IsClustered();

            builder.Property(x => x.Id).HasColumnName(@"Id").HasColumnType("int").IsRequired().ValueGeneratedOnAdd().UseIdentityColumn();
            builder.Property(x => x.TruckVendorPaymentLogId).HasColumnName(@"TruckVendorPaymentLogId").HasColumnType("int").IsRequired(true);
            builder.Property(x => x.Year).HasColumnName(@"Year").HasColumnType("varchar(4)").IsRequired(true).HasMaxLength(4);
            builder.Property(x => x.InvoicingNumber).HasColumnName(@"InvoicingNumber").HasColumnType("varchar(11)").IsRequired(false).HasMaxLength(11);
            builder.Property(x => x.SerialNumber).HasColumnName(@"SerialNumber").HasColumnType("varchar(5)").IsRequired(true).HasMaxLength(5);
            builder.Property(x => x.RegisteredDate).HasColumnName(@"RegisteredDate").HasColumnType("datetime").IsRequired(true);
            builder.Property(x => x.FeeType).HasColumnName(@"FeeType").HasColumnType("nvarchar(20)").IsRequired(true).HasMaxLength(20);
            builder.Property(x => x.OrderType).HasColumnName(@"OrderType").HasColumnType("varchar(10)").IsRequired(true).HasMaxLength(10);
            builder.Property(x => x.SupplierID).HasColumnName(@"SupplierID").HasColumnType("int").IsRequired(true);
            builder.Property(x => x.SupplierUID).HasColumnName(@"SupplierUID").HasColumnType("varchar(10)").IsRequired(true).HasMaxLength(10);
            builder.Property(x => x.CustomerUID).HasColumnName(@"CustomerUID").HasColumnType("varchar(10)").IsRequired(true).HasMaxLength(10);
            builder.Property(x => x.Number).HasColumnName(@"Number").HasColumnType("varchar(10)").IsRequired(true).HasMaxLength(10);
            builder.Property(x => x.ProductCode).HasColumnName(@"ProductCode").HasColumnType("varchar(10)").IsRequired(true).HasMaxLength(10);
            builder.Property(x => x.TaxType).HasColumnName(@"TaxType").HasColumnType("varchar(10)").IsRequired(true).HasMaxLength(10);
            builder.Property(x => x.TaxInclude).HasColumnName(@"TaxInclude").HasColumnType("bit").IsRequired(true);
            builder.Property(x => x.Quantity).HasColumnName(@"Quantity").HasColumnType("int").IsRequired(true);
            builder.Property(x => x.Payment).HasColumnName(@"Payment").HasColumnType("int").IsRequired(true);
            builder.Property(x => x.UnitPrice).HasColumnName(@"UnitPrice").HasColumnType("int").IsRequired(true);
            builder.Property(x => x.ElecInovoice).HasColumnName(@"ElecInovoice").HasColumnType("varchar(1)").IsRequired(true).HasMaxLength(1);
            builder.Property(x => x.PaperInovoice).HasColumnName(@"PaperInovoice").HasColumnType("varchar(1)").IsRequired(true).HasMaxLength(1);


            builder.Property(x => x.ReconciliationDate).HasColumnName(@"ReconciliationDate").HasColumnType("datetime").IsRequired(false);
            builder.Property(x => x.CheckoutDate).HasColumnName(@"CheckoutDate").HasColumnType("datetime").IsRequired(false);
            builder.Property(x => x.PaymentDate).HasColumnName(@"PaymentDate").HasColumnType("datetime").IsRequired(false);
            builder.Property(x => x.ManufactureCode).HasColumnName(@"ManufactureCode").HasColumnType("varchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.ProductionQuantity).HasColumnName(@"ProductionQuantity").HasColumnType("int").IsRequired(false);
            builder.Property(x => x.InvoiceNumber).HasColumnName(@"InvoiceNumber").HasColumnType("varchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.InvoiceDate).HasColumnName(@"InvoiceDate").HasColumnType("datetime").IsRequired(false);
            builder.Property(x => x.VoucherType).HasColumnName(@"VoucherType").HasColumnType("varchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.Note01).HasColumnName(@"Note01").HasColumnType("nvarchar(MAX)").IsRequired(false);
            builder.Property(x => x.Note02).HasColumnName(@"Note02").HasColumnType("nvarchar(MAX)").IsRequired(false);
            builder.Property(x => x.NoteMain).HasColumnName(@"NoteMain").HasColumnType("nvarchar(MAX)").IsRequired(false);
            builder.Property(x => x.SupplierContact).HasColumnName(@"SupplierContact").HasColumnType("varchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.InvoicingSpecial).HasColumnName(@"InvoicingSpecial").HasColumnType("nvarchar(200)").IsRequired(false).HasMaxLength(200);
            builder.Property(x => x.SupplierUniformID).HasColumnName(@"SupplierUniformID").HasColumnType("varchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.SupplierAddress).HasColumnName(@"SupplierAddress").HasColumnType("nvarchar(200)").IsRequired(false).HasMaxLength(200);
            builder.Property(x => x.PackageType).HasColumnName(@"PackageType").HasColumnType("varchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.ForeignCurrencyCode).HasColumnName(@"ForeignCurrencyCode").HasColumnType("varchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.ExchangeRate).HasColumnName(@"ExchangeRate").HasColumnType("float)").IsRequired(false);
            builder.Property(x => x.ForeignCurrencyUnitPrice).HasColumnName(@"ForeignCurrencyUnitPrice").HasColumnType("float)").IsRequired(false);
            builder.Property(x => x.ForeignCurrencyPayment).HasColumnName(@"ForeignCurrencyPayment").HasColumnType("float)").IsRequired(false);
            builder.Property(x => x.OrderNumber).HasColumnName(@"OrderNumber").HasColumnType("varchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.PurchaseNumber).HasColumnName(@"PurchaseNumber").HasColumnType("varchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.SalesCode).HasColumnName(@"SalesCode").HasColumnType("varchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.Department).HasColumnName(@"Department").HasColumnType("nvarchar(50)").IsRequired(true).HasMaxLength(20);
            builder.Property(x => x.ProjectCode).HasColumnName(@"ProjectCode").HasColumnType("nvarchar(50)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.CarLicense).HasColumnName(@"CarLicense").HasColumnType("nvarchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.Company).HasColumnName(@"Company").HasColumnType("nvarchar(20)").IsRequired(true).HasMaxLength(20);
            builder.Property(x => x.VendorName).HasColumnName(@"VendorName").HasColumnType("nvarchar(20)").IsRequired(true).HasMaxLength(20);
            builder.Property(x => x.CreateDate).HasColumnName(@"CreateDate").HasColumnType("datetime").IsRequired(true);
            builder.Property(x => x.CreateUser).HasColumnName(@"CreateUser").HasColumnType("nvarchar(10)").IsRequired(true).HasMaxLength(20);
            builder.Property(x => x.UpdateDate).HasColumnName(@"UpdateDate").HasColumnType("datetime").IsRequired(false);
            builder.Property(x => x.UpdateUser).HasColumnName(@"UpdateUser").HasColumnType("nvarchar(10)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.IsActive).HasColumnName(@"IsActive").HasColumnType("bit").IsRequired(true);
            builder.Property(x => x.HasDownload).HasColumnName(@"HasDownload").HasColumnType("bit").IsRequired(true);
            builder.Property(x => x.DownloadDate).HasColumnName(@"DownloadDate").HasColumnType("datetime").IsRequired(false);
            builder.Property(x => x.DownloadUser).HasColumnName(@"DownloadUser").HasColumnType("nvarchar(10)").IsRequired(false).HasMaxLength(20);
        }
    }
}
