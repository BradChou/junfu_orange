﻿
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.DA.JunFuTransDb
{
    public class TruckVendorPaymentLogWenERP
    {
        public int Id { get; set; }
        public int TruckVendorPaymentLogId { get; set; }
        public string Year { get; set; }
        public string InvoicingNumber { get; set; }
        public string SerialNumber { get; set; }
        public DateTime RegisteredDate { get; set; }
        public string FeeType { get; set; }
        public string OrderType { get; set; }
        public int SupplierID { get; set; }
        public string SupplierUID { get; set; }
        public string CustomerUID { get; set; }
        public string Number { get; set; }
        public string ProductCode { get; set; }
        public string TaxType { get; set; }
        public bool TaxInclude { get; set; }
        public int Quantity { get; set; }
        public int Payment { get; set; }
        public int UnitPrice { get; set; }
        public string ElecInovoice { get; set; }
        public string PaperInovoice { get; set; }
        public DateTime? ReconciliationDate { get; set; }
        public DateTime? CheckoutDate { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string ManufactureCode { get; set; }
        public int? ProductionQuantity { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string VoucherType { get; set; }
        public string Note01 { get; set; }
        public string Note02 { get; set; }
        public string NoteMain { get; set; }
        public string SupplierContact { get; set; }
        public string InvoicingSpecial { get; set; }
        public string SupplierUniformID { get; set; }
        public string SupplierAddress { get; set; }
        public string PackageType { get; set; }
        public string ForeignCurrencyCode { get; set; }
        public double? ExchangeRate { get; set; }
        public double? ForeignCurrencyUnitPrice { get; set; }
        public double? ForeignCurrencyPayment { get; set; }
        public string OrderNumber { get; set; }
        public string PurchaseNumber { get; set; }
        public string SalesCode { get; set; }
        public string Department { get; set; }
        public string ProjectCode { get; set; }
        public string CarLicense { get; set; }
        public string Company { get; set; }
        public string VendorName { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateUser { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateUser { get; set; }
        public bool IsActive { get; set; }
        public bool HasDownload { get; set; }

        public DateTime? DownloadDate { get; set; }
        public string DownloadUser { get; set; }
        public TruckVendorPaymentLogWenERP()
        {
            InvoicingNumber = RegisteredDate.Year.ToString() + RegisteredDate.Month.ToString("00") + SerialNumber;
            CreateDate = DateTime.Now;
            IsActive = true;
            HasDownload = false;
        }
        public static implicit operator TruckVendorPaymentLogWenERP(TruckVendorPaymentLog data)
        {
            data.RegisteredDate ??= DateTime.Now;
            var year = ((DateTime)data.RegisteredDate).Year.ToString();
            data.Payment ??= 0;
            var payment = (int)data.Payment;
            return new TruckVendorPaymentLogWenERP
            {
                Year = year,
                RegisteredDate = (DateTime)data.RegisteredDate,
                FeeType = data.FeeType,
                Note02 = data.Memo,
                CarLicense = data.CarLicense.ToUpper(),
                VendorName = data.VendorName,
                Payment = payment,
                UnitPrice = payment,
                Department = data.Department,
                Company = data.Company,
                CreateUser = data.CreateUser
            };
        }
    }
}
