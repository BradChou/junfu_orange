﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.DA.JunFuTransDb
{
    public class VW_TruckVendorPaymentLogDSERPＭergeData
    {
        public string FeeType { get; set; }
        public DateTime RegisteredDate { get; set; }
        public string Company { get; set; }
        public string VoucherCode { get; set; }
        public string DinShinItemCode { get; set; }
        public string SupplierUID { get; set; }
        public int Accounts { get; set; }
        public string CollectionPayCode { get; set; }
        public string CustomerUID { get; set; }
        public int CollectionAccounts { get; set; }
        public int Spread { get; set; }
        public string CarLicense { get; set; }
        public string Note02 { get; set; }
        public string Department { get; set; }
        public string CompanyUID { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string CreateUser { get; set; }
        public string MergeRandomCode { get; set; }
        public string DownloadRandomCode { get; set; }
        public bool HasDownload { get; set; }

    }
}
