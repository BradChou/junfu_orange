﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LightYear.JunFuTrans.DA.JunFuTransDb
{
    class TruckCompanyRelateConfiguration : IEntityTypeConfiguration<TruckCompanyRelate>
    {
        public void Configure(EntityTypeBuilder<TruckCompanyRelate> builder)
        {
            builder.ToTable("TruckCompanyRelate", "dbo");
            builder.HasKey(x => x.Id).HasName("PK_TruckCompanyRelate").IsClustered();

            builder.Property(x => x.Id).HasColumnName(@"id").HasColumnType("int").IsRequired().ValueGeneratedOnAdd().UseIdentityColumn();
            builder.Property(x => x.CodeId).HasColumnName(@"CodeId").HasColumnType("int").IsRequired(true);
            builder.Property(x => x.CodeName).HasColumnName(@"CodeName").HasColumnType("nvarchar(100)").IsRequired(true).HasMaxLength(100);
            builder.Property(x => x.IsActive).HasColumnName(@"IsActive").HasColumnType("bit").IsRequired(true);
            builder.Property(x => x.CompanyUID).HasColumnName(@"CompanyUID").HasColumnType("varchar(10)").IsRequired(true).HasMaxLength(10);
        }
    }
}
