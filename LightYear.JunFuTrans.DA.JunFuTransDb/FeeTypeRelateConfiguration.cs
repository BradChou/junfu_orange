﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LightYear.JunFuTrans.DA.JunFuTransDb
{
    class FeeTypeRelateConfiguration : IEntityTypeConfiguration<FeeTypeRelate>
    {
        public void Configure(EntityTypeBuilder<FeeTypeRelate> builder)
        {
            builder.ToTable("FeeTypeRelate", "dbo");
            builder.HasKey(x => x.Id).HasName("PK_FeeTypeRelate").IsClustered();

            builder.Property(x => x.Id).HasColumnName(@"id").HasColumnType("int").IsRequired().ValueGeneratedOnAdd().UseIdentityColumn();
            builder.Property(x => x.CodeId).HasColumnName(@"CodeId").HasColumnType("int").IsRequired(true);
            builder.Property(x => x.CodeName).HasColumnName(@"CodeName").HasColumnType("nvarchar(100)").IsRequired(true).HasMaxLength(100);
            builder.Property(x => x.HasInvoice).HasColumnName(@"HasInvoice").HasColumnType("bit").IsRequired(true);
            builder.Property(x => x.IsActive).HasColumnName(@"IsActive").HasColumnType("bit").IsRequired(true);
            builder.Property(x => x.PayableCode).HasColumnName(@"PayableCode").HasColumnType("varchar(10)").IsRequired(true).HasMaxLength(10);
            builder.Property(x => x.ReceivableCode).HasColumnName(@"ReceivableCode").HasColumnType("varchar(10)").IsRequired(true).HasMaxLength(10);
            builder.Property(x => x.DinShinItemCode).HasColumnName(@"DinShinItemCode").HasColumnType("varchar(10)").IsRequired(true).HasMaxLength(10);
            builder.Property(x => x.DinShinItemName).HasColumnName(@"DinShinItemName").HasColumnType("nvarchar(50)").IsRequired(true).HasMaxLength(50);
            builder.Property(x => x.StockPayCode).HasColumnName(@"StockPayCode").HasColumnType("varchar(10)").IsRequired(true).HasMaxLength(10);
            builder.Property(x => x.StockPayName).HasColumnName(@"StockPayName").HasColumnType("nvarchar(50)").IsRequired(true).HasMaxLength(50);
            builder.Property(x => x.RevenueCode).HasColumnName(@"RevenueCode").HasColumnType("varchar(10)").IsRequired(true).HasMaxLength(10);
            builder.Property(x => x.RevenueName).HasColumnName(@"RevenueName").HasColumnType("nvarchar(50)").IsRequired(true).HasMaxLength(50);
            builder.Property(x => x.CollectionReceiveCode).HasColumnName(@"CollectionReceiveCode").HasColumnType("varchar(10)").IsRequired(true).HasMaxLength(10);
            builder.Property(x => x.CollectionReceiveName).HasColumnName(@"CollectionReceiveName").HasColumnType("nvarchar(50)").IsRequired(true).HasMaxLength(50);
            builder.Property(x => x.InvoiceSubjectName).HasColumnName(@"InvoiceSubjectName").HasColumnType("nvarchar(50)").IsRequired(true).HasMaxLength(50);
        }
    }
}
