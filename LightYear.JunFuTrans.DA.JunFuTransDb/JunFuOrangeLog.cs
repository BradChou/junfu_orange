﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.JunFuTransDb
{
    public class JunFuOrangeLog
    {
		public int Id { get; set; }
		public string Guid { get; set; }
		public string Account { get; set; }
		public string RequestBody { get; set; }
		public string ResponseBody { get; set; }
		public string RequestHeader { get; set; }
		public string Path { get; set; }
		public string QueryString { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public DateTime CreateDate { get; set; }
		public JunFuOrangeLog()
        {
			CreateDate = DateTime.Now;
        }
	}
}
