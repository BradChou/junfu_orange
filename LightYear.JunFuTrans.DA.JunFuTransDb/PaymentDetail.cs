// ------------------------------------------------------------------------------------------------
// This code was generated by EntityFramework Reverse POCO Generator (http://www.reversepoco.co.uk/).
// Created by Simon Hughes (https://about.me/simon.hughes).
//
// Registered to: Light-Year Technology
// Company      : Light-Year Technology
// Licence Type : Commercial
// Licences     : 1
// Valid until  : 21 五月 2025
//
// Do not make changes directly to this file - edit the template instead.
//
// The following connection settings were used to generate this file:
//     Connection String Name: "JunFuTransDbContext"
//     Connection String:      "Data Source=junfurdsdb.cioqpfywhsfk.us-east-2.rds.amazonaws.com,1433;Initial Catalog=JunFuTrans;Persist Security Info=True;User ID=admin;password=**zapped**;;MultiSubnetFailover=Yes;"
// ------------------------------------------------------------------------------------------------
// Database Edition       : Standard Edition (64-bit)
// Database Engine Edition: Standard
// Database Version       : 13.0.5820.21

// <auto-generated>
// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable NotAccessedVariable
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantCast
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// ReSharper disable UsePatternMatching
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.DA.JunFuTransDb
{
    // payment_detail
    /// <summary>
    /// 代收對帳作業-子單
    /// </summary>
    public class PaymentDetail
    {
        public int PaymentDetailId { get; set; } // payment_detail_id (Primary key)

        /// <summary>
        /// 母單編號
        /// </summary>
        public int PaymentId { get; set; } // payment_id

        /// <summary>
        /// 託運單號
        /// </summary>
        public string Barcode { get; set; } // barcode (length: 50)

        /// <summary>
        /// 應收金額
        /// </summary>
        public decimal ReceiveAmount { get; set; } // receive_amount

        /// <summary>
        /// 實收金額
        /// </summary>
        public decimal ActualAmount { get; set; } // actual_amount

        /// <summary>
        /// 建檔日期
        /// </summary>
        public DateTime? CreateDate { get; set; } // create_date

        /// <summary>
        /// 建檔人
        /// </summary>
        public int? CreateId { get; set; } // create_id

        /// <summary>
        /// 修改日期
        /// </summary>
        public DateTime? UpdateDate { get; set; } // update_date

        /// <summary>
        /// 修改人
        /// </summary>
        public int? UpdateId { get; set; } // update_id

        /// <summary>
        /// 是否刪除
        /// </summary>
        public bool IsDelete { get; set; } // is_delete

        public PaymentDetail()
        {
            IsDelete = false;
        }
    }

}
// </auto-generated>

