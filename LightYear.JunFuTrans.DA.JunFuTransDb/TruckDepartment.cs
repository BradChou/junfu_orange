﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.DA.JunFuTransDb
{
    public class TruckDepartment
    {
		public int id { get; set; }
		public string DepartmentCode { get; set; }
		public string DepartmentName { get; set; }
		public string CreateUser { get; set; }
		public DateTime CreateDate { get; set; }
		public string UpdateUser { get; set; }
		public DateTime? UpdateDate { get; set; }
		public bool IsActive { get; set; }

	}
}
