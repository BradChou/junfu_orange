﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace LightYear.JunFuTrans.DA.JunFuTransDb
{
    class TruckVendorPaymentLogDSERPConfiguration : IEntityTypeConfiguration<TruckVendorPaymentLogDSERP>
    {
        public void Configure(EntityTypeBuilder<TruckVendorPaymentLogDSERP> builder)
        {
            builder.ToTable("TruckVendorPaymentLogDSERP", "dbo");
            builder.HasKey(x => x.Id).HasName("PK_TruckVendorPaymentLogDSERP").IsClustered();

            builder.Property(x => x.Id).HasColumnName(@"Id").HasColumnType("int").IsRequired().ValueGeneratedOnAdd().UseIdentityColumn();
            builder.Property(x => x.TruckVendorPaymentLogId).HasColumnName(@"TruckVendorPaymentLogId").HasColumnType("int").IsRequired(true);
            builder.Property(x => x.FeeType).HasColumnName(@"FeeType").HasColumnType("nvarchar(20)").IsRequired(true).HasMaxLength(20);
            builder.Property(x => x.RegisteredDate).HasColumnName(@"RegisteredDate").HasColumnType("datetime").IsRequired(true);
            builder.Property(x => x.UploadDate).HasColumnName(@"UploadDate").HasColumnType("datetime").IsRequired(true);
            builder.Property(x => x.Company).HasColumnName(@"Company").HasColumnType("varchar(10)").IsRequired(true).HasMaxLength(10);
            builder.Property(x => x.VoucherCode).HasColumnName(@"VoucherCode").HasColumnType("varchar(10)").IsRequired(true).HasMaxLength(10);
            builder.Property(x => x.DinShinItemCode).HasColumnName(@"DinShinItemCode").HasColumnType("varchar(10)").IsRequired(true).HasMaxLength(10);
            builder.Property(x => x.SupplierUID).HasColumnName(@"SupplierUID").HasColumnType("varchar(10)").IsRequired(false).HasMaxLength(10);
            builder.Property(x => x.Accounts).HasColumnName(@"Accounts").HasColumnType("int").IsRequired(true);
            builder.Property(x => x.CollectionPayCode).HasColumnName(@"CollectionPayCode").HasColumnType("varchar(1)").IsRequired(true).HasMaxLength(1);
            builder.Property(x => x.CustomerUID).HasColumnName(@"CustomerUID").HasColumnType("varchar(10)").IsRequired(false).HasMaxLength(10);
            builder.Property(x => x.CollectionAccounts).HasColumnName(@"CollectionAccounts").HasColumnType("int").IsRequired(true);
            builder.Property(x => x.Spread).HasColumnName(@"Spread").HasColumnType("int").IsRequired(true);
            builder.Property(x => x.CarLicense).HasColumnName(@"CarLicense").HasColumnType("nvarchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.Note02).HasColumnName(@"Note02").HasColumnType("nvarchar(MAX)").IsRequired(false);
            builder.Property(x => x.Department).HasColumnName(@"Department").HasColumnType("nvarchar(50)").IsRequired(true).HasMaxLength(20);
            builder.Property(x => x.CompanyUID).HasColumnName(@"CompanyUID").HasColumnType("varchar(10)").IsRequired(true).HasMaxLength(10);
            builder.Property(x => x.InvoiceNumber).HasColumnName(@"InvoiceNumber").HasColumnType("varchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.InvoiceDate).HasColumnName(@"InvoiceDate").HasColumnType("datetime").IsRequired(false);
            builder.Property(x => x.CreateDate).HasColumnName(@"CreateDate").HasColumnType("datetime").IsRequired(true);
            builder.Property(x => x.CreateUser).HasColumnName(@"CreateUser").HasColumnType("nvarchar(10)").IsRequired(true).HasMaxLength(20);
            builder.Property(x => x.UpdateDate).HasColumnName(@"UpdateDate").HasColumnType("datetime").IsRequired(false);
            builder.Property(x => x.UpdateUser).HasColumnName(@"UpdateUser").HasColumnType("nvarchar(10)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.IsActive).HasColumnName(@"IsActive").HasColumnType("bit").IsRequired(true);
            builder.Property(x => x.HasDownload).HasColumnName(@"HasDownload").HasColumnType("bit").IsRequired(true);
            builder.Property(x => x.DownloadDate).HasColumnName(@"DownloadDate").HasColumnType("datetime").IsRequired(false);
            builder.Property(x => x.DownloadUser).HasColumnName(@"DownloadUser").HasColumnType("nvarchar(10)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.MergeRandomCode).HasColumnName(@"MergeRandomCode").HasColumnType("nvarchar(10)").IsRequired(true).HasMaxLength(20);
        }
    }
}
