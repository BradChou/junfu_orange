﻿using LightYear.JunFuTrans.BL.BE.PalletACSectionShouldPay;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.PalletACSectionShouldPay
{
    public interface IPalletACSectionShouldPayService
    {
        List<SuppliersEntity> GetSuppliers();
        List<CSectionOriginalShippingFee> GetCSectionFee();
        List<PalletACSectionShouldPayEntity> GetACSectionShouldPayDetails(DateTime start, DateTime end, string supplierCode, string checkNumber, string taskId);
        List<ArriveOptionIsPaymentRequired> GetArriveOptionIsPaymentRequired();
        void Update(PalletAcSectionShouldPay palletAcSectionShouldPay);
        int Insert(PalletAcSectionShouldPay palletAcSectionShouldPay);
        List<ShowAllSupplierEntity> GetAllSupplierACSectionShouldPay(DateTime start, DateTime end, string supplierCode);
        List<OneSupplierEntity> GetOneSupplierACSectionShouldPay(DateTime start, DateTime end, string supplierCode);
        byte[] Export(DateTime end, string supplierCode);
        Stream GenerateZipFile(List<InputJson> inputJsons);
    }
}
