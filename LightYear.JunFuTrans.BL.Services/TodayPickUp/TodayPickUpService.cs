﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using LightYear.JunFuTrans.BL.BE.TodayPickUp;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.Repositories.TodayPickUp;

namespace LightYear.JunFuTrans.BL.Services.TodayPickUp
{
    public class TodayPickUpService : ITodayPickUpService
    {
        public TodayPickUpService(ITodayPickUpRepository todayPickUpRepository, IMapper mapper)
        {
            this.TodayPickUpRepository = todayPickUpRepository;

            this.Mapper = mapper;
        }

        public ITodayPickUpRepository TodayPickUpRepository { get; set; }
        public IMapper Mapper { get; private set; }
        public List<TodayPickUpEntity> TodayPickUpEntities(TodayPickUpInputEntity todayPickUpInputEntity)
        {
            int counter = 0;            //計數器

            List<TtDeliveryScanLog> todayPickUpRepositoryEntities = TodayPickUpRepository.TodayPickUpRepositoryEntities(todayPickUpInputEntity);

            //var todayPickUpEntities = Mapper.Map<List<TodayPickUpEntity>>(todayPickUpRepositoryEntities);
            List<TodayPickUpEntity> todayPickUpEntities = new List<TodayPickUpEntity>();
            foreach (var i in todayPickUpRepositoryEntities)
            {
                TodayPickUpEntity test = new TodayPickUpEntity()
                {
                    CheckNumber = i.CheckNumber,
                    Pieces = i.Pieces,
                    ScanItem = i.ScanItem,
                    ScanDate = i.ScanDate
                };
                todayPickUpEntities.Add(test);
            }                                                                   //****以上這段須修正成綠色那段

            foreach (var i in todayPickUpEntities)
            {
                counter += 1;
                i.Index = counter;
                string[] date = i.ScanDate.ToString().Split(' ');
                i.FrontendShipDate = date[0] + " " + date[2];
                if (i.ScanItem == "5")                                         //*****為啥有空白鍵啊?
                {
                    i.PickUpStatus = "已集貨";
                }
            }

            return todayPickUpEntities;
        }
        public string GetDriverNameByDriverCode(string driverCode)
        {
            string result = TodayPickUpRepository.GetDriverNameByDriverCode(driverCode);
            return result;
        }
        public string GetDriverStationByScode(string scode)
        {
            string result = TodayPickUpRepository.GetDriverStationByScode(scode);
            return result;
        }
    }
}
