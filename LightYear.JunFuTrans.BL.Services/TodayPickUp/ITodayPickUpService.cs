﻿using LightYear.JunFuTrans.BL.BE.TodayPickUp;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.TodayPickUp
{
    public interface ITodayPickUpService
    {
        List<TodayPickUpEntity> TodayPickUpEntities(TodayPickUpInputEntity todayPickUpInputEntity);
        string GetDriverNameByDriverCode(string driverCode);
        string GetDriverStationByScode(string scode);
    }
}
