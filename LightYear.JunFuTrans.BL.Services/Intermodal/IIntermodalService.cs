﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.Intermodal
{
    public interface IIntermodalService
    {

        bool GetDeliverRequestByCheckNumbers(List<string> requestIds);

        public bool GuoYangSchedule(int step);
    }
}
