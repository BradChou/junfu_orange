﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.Repositories.Intermodal;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRequest;
using LightYear.JunFuTrans.BL.BE.Intermodal;
using Microsoft.Extensions.Configuration;
using System.IO;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Data;
using DocumentFormat.OpenXml;
using System.Net;
using System.Net.Http;

namespace LightYear.JunFuTrans.BL.Services.Intermodal
{
    public class IntermodalService : IIntermodalService
    {
        public IIntermodalRepository IntermodalRepository { get; set; }
        public IConfiguration Configuration { get; set; }

        public IDeliveryRequestRepository DeliveryRequestRepository { get; set; }

        public IntermodalService(IIntermodalRepository intermodalRepository, IDeliveryRequestRepository deliveryRequestRepository, IConfiguration configuration)
        {
            IntermodalRepository = intermodalRepository;
            this.DeliveryRequestRepository = deliveryRequestRepository;
            Configuration = configuration;
        }
        public bool GetDeliverRequestByCheckNumbers(List<string> requestIds)
        {
            //這段效能很差，改寫
            //IEnumerable<TcDeliveryRequest> deliveryRequests = IntermodalRepository.GetTcDeliveryRequestsByRequestIds(requestIds);      //篩選符合標準的request
            List<decimal> ids = new List<decimal>();

            foreach(string eachRequestId in requestIds)
            {
                try
                {
                    decimal eachId = Convert.ToDecimal(eachRequestId);
                    ids.Add(eachId);
                }
                catch
                { 
                    //dummy
                }
            }

            var deliveryRequests = DeliveryRequestRepository.GetAllByRequestIds(ids);

            List<string> returnCheckNumber = deliveryRequests.Select(d => d.CheckNumber).ToList();
            IEnumerable<GuoYangRequest> guoYangRequests = IntermodalRepository.GetGuoYangRequestsByCheckNumbers(returnCheckNumber).ToArray();

            //以下這行是真正需要寫入國陽的request
            TcDeliveryRequest[] deliveryRequestNew = deliveryRequests.Except(deliveryRequests.Where(x => guoYangRequests.Select(x => x.FseCheckNumber).Contains(x.CheckNumber))).ToArray();    //排除掉已經寫入國陽資料表的request

            List<GuoYangRequest> fromDeliveryReqeuestToGuoYangRequests = new List<GuoYangRequest>();
            DateTime now = DateTime.Now;
            GuoYangCheckNumberStartAndEndEntity guoYangStartAndEnd = IntermodalRepository.GetNextGuoYangCheckNumberBulky(deliveryRequestNew.Sum(x => x.Pieces ?? 1));


            long position = 100000000; //郵局前六碼為流水號，後面固定
            string email = Configuration.GetValue<string>("GuoYangSendFileEmail");
            int deliveryReqeustsLength = deliveryRequestNew.Count();

            for (var i = 0; i < deliveryReqeustsLength; i++)
            {
                int pieces = deliveryRequestNew[i].Pieces ?? 1;
                for (var p = 0; p < pieces; p++)
                {
                    GuoYangRequest guoYangEntity = new GuoYangRequest
                    {
                        FseCheckNumber = deliveryRequestNew[i].CheckNumber,
                        Cdate = now,
                        GuoyangCheckNumber = null,
                        ActiveFlag = null,
                        ScheduleId = null,
                        ReceiveContact = LengthTrimer(deliveryRequestNew[i].ReceiveContact, 20),
                        ReceiveAddress = LengthTrimer(deliveryRequestNew[i].ReceiveCity + deliveryRequestNew[i].ReceiveArea + deliveryRequestNew[i].ReceiveAddress, 40),
                        ReceiveTel = LengthTrimer(deliveryRequestNew[i].ReceiveTel1, 20),
                        EMail = email,
                        DetailCheckNumber = deliveryRequestNew[i].Pieces == 1 ? deliveryRequestNew[i].CheckNumber : deliveryRequestNew[i].CheckNumber + "-" + (p + 1).ToString(),
                        CollectionMoney = p > 0 ? 0 : deliveryRequestNew[i].CollectionMoney,
                        Remark = LengthTrimer(deliveryRequestNew[i].InvoiceDesc, 40),
                        IsClosed = false,
                        IsUploaded = false,
                        RequestId = deliveryRequestNew[i].RequestId
                    };
                    fromDeliveryReqeuestToGuoYangRequests.Add(guoYangEntity);
                }
            }

            bool result = IntermodalRepository.BulkyInsertIntoGuoYangTable(fromDeliveryReqeuestToGuoYangRequests);

            for (var i = 0; i < fromDeliveryReqeuestToGuoYangRequests.Count(); i++)
            {
                fromDeliveryReqeuestToGuoYangRequests[i].GuoyangCheckNumber = ((fromDeliveryReqeuestToGuoYangRequests[i].Id % 100000) * position + guoYangStartAndEnd.Start).ToString();        //100000十萬組貨號
            }

            bool resultUpdate = IntermodalRepository.BulkyUpdateGuoYang(fromDeliveryReqeuestToGuoYangRequests);

            return result;
        }

        public bool GuoYangSchedule(int step)
        {
            bool isSuccess = true; 

            if (step == 0)
            {
                //步驟0: 輸出出貨檔，時間: 0700
                if (!IntermodalRepository.IsTodayHoliday())    //國陽國定假日不允許傳出貨檔
                {
                    isSuccess = isSuccess && GuoYangSendContent();
                }
            }
            else if (step == 1)
            {
                //步驟1: 讀取回覆檔，時間:1000，抓取前一天的回覆檔
                int fileNumber = 1;
                string guoYangReturnDataName = guoYangReturnFileName(fileNumber);
                while (FtpDownload(guoYangReturnDataName))
                {
                    isSuccess = isSuccess && GuoYangReturnContent(guoYangReturnDataName);
                    fileNumber++;
                    guoYangReturnDataName = guoYangReturnFileName(fileNumber);
                }
            }
            else if (step == 2)
            {
                //步驟2: 用API獲取貨態，時間:1330、1900、0300
                isSuccess = isSuccess && ApiRequest();
            }

            return isSuccess;
        }

        private bool GuoYangSendContent()
        {
            int idStepZero = GuoYangScheduleLog(0, false, false, null, null);
            bool isSuccess = true;
            string remark = "";

            var memoryStream = new MemoryStream();
            string filePath = Configuration.GetValue<string>("GuoYangFtpData");
            string filePathUploaded = Configuration.GetValue<string>("GuoYangFtpDataUploaded");
            string guoYangItemName = Configuration.GetValue<string>("GuoYangItemName");
            string guoYangScanItemSevenDriverCode = Configuration.GetValue<string>("GuoYangScanItemSevenDriverCode");

            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            if (!Directory.Exists(filePathUploaded))
            {
                Directory.CreateDirectory(filePathUploaded);
            }

            IEnumerable<GuoYangRequest> requests = IntermodalRepository.SelectGuoYangRequestNeedToSendToday(guoYangScanItemSevenDriverCode);

            string fileName = "";
            if (requests.Count() > 0)
            {
                fileName = "jf_upload-" + DateTime.Now.ToString("yyyyMMdd") + "-01.xlsx";
                string fileFullName = filePath + fileName;
                try
                {
                    using (var document = SpreadsheetDocument.Create(memoryStream, SpreadsheetDocumentType.Workbook))
                    {
                        var workbookPart = document.AddWorkbookPart();
                        workbookPart.Workbook = new Workbook();

                        var worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                        worksheetPart.Worksheet = new Worksheet(new SheetData());

                        var sheets = workbookPart.Workbook.AppendChild(new Sheets());
                        sheets.Append(new Sheet()
                        {
                            Id = workbookPart.GetIdOfPart(worksheetPart),
                            SheetId = 1,
                            Name = "Sheet 1"
                        });

                        var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

                        var titleRow = new Row();
                        titleRow.Append(
                            new Cell() { CellValue = new CellValue("收件人") ,DataType = CellValues.String},
                            new Cell() { CellValue = new CellValue("收件人地址"), DataType = CellValues.String },
                            new Cell() { CellValue = new CellValue("收件人電話"), DataType = CellValues.String },
                            new Cell() { CellValue = new CellValue("電子信箱"), DataType = CellValues.String },
                            new Cell() { CellValue = new CellValue("明細貨號"), DataType = CellValues.String },
                            new Cell() { CellValue = new CellValue("郵局包裹號"), DataType = CellValues.String },
                            new Cell() { CellValue = new CellValue("商品名稱"), DataType = CellValues.String },
                            new Cell() { CellValue = new CellValue("件數"), DataType = CellValues.String },
                            new Cell() { CellValue = new CellValue("代收金額"), DataType = CellValues.String },
                            new Cell() { CellValue = new CellValue("備註"), DataType = CellValues.String },
                            new Cell() { CellValue = new CellValue("重量"), DataType = CellValues.String }
                            );
                        sheetData.AppendChild(titleRow);

                        foreach (var r in requests)
                        {
                            // 建立資料列物件
                            var row = new Row();
                            // 在資料列中插入欄位
                            row.Append(
                                new Cell() { CellValue = new CellValue(r.ReceiveContact), DataType = CellValues.String },
                                new Cell() { CellValue = new CellValue(r.ReceiveAddress), DataType = CellValues.String },
                                new Cell() { CellValue = new CellValue(r.ReceiveTel), DataType = CellValues.String },
                                new Cell() { CellValue = new CellValue(r.EMail), DataType = CellValues.String },
                                new Cell() { CellValue = new CellValue(r.DetailCheckNumber), DataType = CellValues.String },
                                new Cell() { CellValue = new CellValue(r.GuoyangCheckNumber), DataType = CellValues.String },
                                new Cell() { CellValue = new CellValue(guoYangItemName), DataType = CellValues.String },
                                new Cell() { CellValue = new CellValue(1.ToString()), DataType = CellValues.Number },
                                new Cell() { CellValue = new CellValue(r.CollectionMoney != null ? r.CollectionMoney.ToString() : 0.ToString()), DataType = CellValues.Number },
                                new Cell() { CellValue = new CellValue(r.Remark), DataType = CellValues.String },
                                new Cell() { CellValue = new CellValue(1000.ToString()), DataType = CellValues.Number }
                            );
                            // 插入資料列 
                            sheetData.AppendChild(row);
                        }

                        workbookPart.Workbook.Save();
                        document.Close();

                        using (var fileStream = new FileStream(fileFullName, FileMode.Create))
                        {
                            memoryStream.WriteTo(fileStream);
                        }
                    }

                    try
                    {
                        FtpUpload(fileName);
                    }
                    catch (Exception ex)
                    {
                        isSuccess = false;
                        remark += ex.ToString();
                    }

                    try
                    {
                        File.Move(filePath + fileName, filePathUploaded + fileName, true);
                    }
                    catch(Exception ex)
                    {
                        isSuccess = false;
                        remark += ex.ToString();
                    }

                    //上傳成功要將request表的is_uploaded和upload_file_name填上
                    foreach (var r in requests)
                    {
                        r.IsUploaded = true;
                        r.UploadFileName = fileName;
                    }
                    IntermodalRepository.BulkyUpdateGuoYang(requests);
                }
                catch (Exception ex)
                {
                    remark += ex.ToString();
                    isSuccess = false;
                }
            }

            GuoYangScheduleLog(0, isSuccess, true, remark, fileName);
            return true;
        }

        private bool GuoYangReturnContent(string fileName)
        {
            //注意:此方法一次只讀取一個檔案
            int scheduleId = GuoYangScheduleLog(1, false, false, null, null);
            bool isSuccess = true;
            string remark = "";

            DataTable dt = new DataTable();
            List<GuoYangReturnContent> data = new List<GuoYangReturnContent>();
            List<string> guoYangCheckNumberUpdate = new List<string>();
            List<string> guoYangCheckNumberInsert = new List<string>();
            string filePath = Configuration.GetValue<string>("GuoYangReturnContent");
            string filePathDownload = Configuration.GetValue<string>("GuoYangReturnContentDownloaded");

            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            if (!Directory.Exists(filePathDownload))
            {
                Directory.CreateDirectory(filePathDownload);
            }

            try
            {
                using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(filePath + fileName, false))
                {
                    WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;
                    IEnumerable<Sheet> sheets = spreadsheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                    string relationshipId = sheets.First().Id.Value;
                    WorksheetPart worksheetPart = (WorksheetPart)spreadsheetDocument.WorkbookPart.GetPartById(relationshipId);
                    Worksheet workSheet = worksheetPart.Worksheet;
                    SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                    Row[] rows = sheetData.Descendants<Row>().ToArray();

                    for (int rowIndex = 1; rowIndex < rows.Count(); rowIndex++)
                    {
                        GuoYangReturnContent content = new GuoYangReturnContent
                        {
                            Number = int.Parse(GetCellValue(spreadsheetDocument, rows[rowIndex].Descendants<Cell>().ElementAt(0))),
                            GuoyangCheckNumber = GetCellValue(spreadsheetDocument, rows[rowIndex].Descendants<Cell>().ElementAt(1)),
                            ReceiveContact = GetCellValue(spreadsheetDocument, rows[rowIndex].Descendants<Cell>().ElementAt(2)),
                            ReceiveAddress = GetCellValue(spreadsheetDocument, rows[rowIndex].Descendants<Cell>().ElementAt(3)),
                            ReceiveTel = GetCellValue(spreadsheetDocument, rows[rowIndex].Descendants<Cell>().ElementAt(4)),
                            CollectionMoney = int.Parse(GetCellValue(spreadsheetDocument, rows[rowIndex].Descendants<Cell>().ElementAt(5))),
                            DetailCheckNumber = GetCellValue(spreadsheetDocument, rows[rowIndex].Descendants<Cell>().ElementAt(6)),
                            Piece = int.Parse(GetCellValue(spreadsheetDocument, rows[rowIndex].Descendants<Cell>().ElementAt(7))),
                            FileName = fileName,
                            Cdate = DateTime.Now,
                            ScheduleId = scheduleId
                        };

                        data.Add(content);

                        if (content.GuoyangCheckNumber.EndsWith("18"))
                        {
                            guoYangCheckNumberInsert.Add(content.GuoyangCheckNumber);
                        }
                        else
                        {
                            guoYangCheckNumberUpdate.Add(content.GuoyangCheckNumber);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                remark += ex.ToString();
            }

            bool isSuccessReturnContent = IntermodalRepository.InsertGuoYangReturnContent(data);

            //先處理單號未改變的
            IEnumerable<GuoYangRequest> requestsUpdate = IntermodalRepository.SelectGuoYangRequest(guoYangCheckNumberUpdate);
            foreach (var r in requestsUpdate)
            {
                r.IsEcho = true;

            }
            bool isSuccessRequestUpdate = IntermodalRepository.BulkyUpdateGuoYang(requestsUpdate);

            //再處理末碼被改為"18"的
            IEnumerable<GuoYangRequest> requestsNeedToUpdate = IntermodalRepository.SelectGuoYangRequest(guoYangCheckNumberInsert.Select(x => x.Substring(0, 12) + "78"));
            foreach (var r in requestsNeedToUpdate)
            {
                r.IsClosed = true;
            }
            bool isSuccessRequestNeedToUpdate = IntermodalRepository.BulkyUpdateGuoYang(requestsNeedToUpdate);

            foreach (var r in requestsNeedToUpdate)
            {
                r.Id = 0;
                r.GuoyangCheckNumber = r.GuoyangCheckNumber.Substring(0, 12) + "18";
                r.IsEcho = true;
                r.IsClosed = false;
            }
            bool isSuccessRequestInsert = IntermodalRepository.BulkyInsertIntoGuoYangTable(requestsNeedToUpdate);

            isSuccess = isSuccessReturnContent && isSuccessRequestUpdate && isSuccessRequestNeedToUpdate && isSuccessRequestInsert;

            try
            {
                File.Move(filePath + fileName, filePathDownload + fileName, true);
            }
            catch(Exception ex)
            {
                isSuccess = false;
                remark += ex.ToString();
            }

            GuoYangScheduleLog(1, isSuccess, true, remark, fileName);

            return true;
        }

        private static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            string value = cell.CellValue.InnerXml;
            if (cell.DataType != null && (cell.DataType.Value == CellValues.SharedString || cell.DataType.Value == CellValues.Number))
            {
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else //浮點數和日期對應的cell.DataType都為NULL
            {
                // DateTime.FromOADate((double.Parse(value)); 如果確定是日期就可以直接用過該方法轉換為日期對象，可是無法確定DataType==NULL的時候這個CELL 數據到底是浮點型還是日期.(日期被自動轉換為浮點
                return value;
            }
        }

        private string LengthTrimer(string input, int length)
        {
            string result = input;
            if (input.Length > length)
            {
                input.Substring(0, length);
            }

            return result;
        }

        private int GuoYangScheduleLog(int step, bool isSuccess, bool isEnd, string remark = "", string fileName = null)
        {
            try
            {
                if (!isEnd)
                {
                    GuoYangScheduleLog log = new GuoYangScheduleLog
                    {
                        Cdate = DateTime.Now,
                        IsSuccess = isSuccess,
                        StartTime = DateTime.Now,
                        Step = step,
                        Remark = remark == null ? null : remark.Substring(0, 400)
                    };
                    return IntermodalRepository.InsertGuoYangSchedule(log);
                }
                else
                {
                    GuoYangScheduleLog log = IntermodalRepository.GetLastScheduleLogForAssignStep(step);
                    if (!(log is null))
                    {
                        log.IsSuccess = isSuccess;
                        log.EndTime = DateTime.Now;
                        log.Step = step;
                        log.Remark = remark.Length > 400 ? remark.Substring(0, 400) : remark;
                        log.FileName = fileName;
                    }
                    return IntermodalRepository.UpdateScheduleLogForAssignStep(log);
                }
            }
            catch
            {
                return -1;
            }
        }

        bool FtpUpload(string fileName)
        {
            bool isSuccess = true;
            string guoYangLocalUri = Configuration.GetValue<string>("GuoYangFtpData");
            string guoYangFtpUrl = Configuration.GetValue<string>("GuoYangFtpUrl");
            string guoYangAccount = Configuration.GetValue<string>("GuoYangFtpAccount");
            string guoYangPassword = Configuration.GetValue<string>("GuoYangFtpPassword");
            string guoYangLocalFullName = guoYangLocalUri + fileName;
            try
            {
                FtpWebRequest ftpReq = (FtpWebRequest)WebRequest.Create(guoYangFtpUrl + fileName);
                ftpReq.UseBinary = true;
                ftpReq.Method = WebRequestMethods.Ftp.UploadFile;
                ftpReq.Credentials = new NetworkCredential(guoYangAccount, guoYangPassword);

                byte[] b = File.ReadAllBytes(guoYangLocalFullName);

                ftpReq.ContentLength = b.Length;
                using (Stream s = ftpReq.GetRequestStream())
                {
                    s.Write(b, 0, b.Length);
                }

                FtpWebResponse ftpResp = (FtpWebResponse)ftpReq.GetResponse();

                if (ftpReq != null)
                {

                }

                return isSuccess;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        bool FtpDownload(string fileName)
        {
            bool isSuccess = true;
            string guoYangLocalUriDownload = Configuration.GetValue<string>("GuoYangReturnContent");
            string guoYangFtpUrlDownload = Configuration.GetValue<string>("GuoYangReturnContentDownloaded");
            string guoYangFtrpReturnUri = Configuration.GetValue<string>("GuoYangFtpReturnUrl");
            string guoYangLocalFullName = guoYangLocalUriDownload + fileName;
            string guoYangAccount = Configuration.GetValue<string>("GuoYangFtpAccount");
            string guoYangPassword = Configuration.GetValue<string>("GuoYangFtpPassword");

            if (!Directory.Exists(guoYangLocalUriDownload))
            {
                Directory.CreateDirectory(guoYangLocalUriDownload);
            }

            if (!Directory.Exists(guoYangFtpUrlDownload))
            {
                Directory.CreateDirectory(guoYangFtpUrlDownload);
            }


            try
            {
                FtpWebRequest ftpReq = (FtpWebRequest)WebRequest.Create(guoYangFtrpReturnUri + fileName);
                ftpReq.UseBinary = true;
                ftpReq.Method = WebRequestMethods.Ftp.DownloadFile;
                ftpReq.Credentials = new NetworkCredential(guoYangAccount, guoYangPassword);

                FtpWebResponse ftpResp = (FtpWebResponse)ftpReq.GetResponse();

                Stream responseStream = ftpResp.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);

                using (FileStream writer = new FileStream(guoYangLocalFullName, FileMode.Create))
                {
                    long length = ftpResp.ContentLength;
                    int bufferSize = 1048;
                    int readCount;
                    byte[] buffer = new byte[2048];

                    readCount = responseStream.Read(buffer, 0, bufferSize);
                    while (readCount > 0)
                    {
                        writer.Write(buffer, 0, readCount);
                        readCount = responseStream.Read(buffer, 0, bufferSize);
                    }
                }

                reader.Close();
                ftpResp.Close();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        string guoYangReturnFileName(int fileNumber)
        {
            string dateStamp = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
            return string.Format(Configuration.GetValue<string>("GuoYangReturnDataFileFormat"), dateStamp + "-" + string.Format("{0:00}", fileNumber));
        }

        bool ApiRequest()
        {
            int idStepTwo = GuoYangScheduleLog(2, false, false, null, null);
            string[] activeGuoYangCheckNumberArray = IntermodalRepository.GetActiveRquestFromGuoYangRequest();


            string statusUrl = Configuration.GetValue<string>("GuoYangStatusUrl");
            string customerCode = Configuration.GetValue<string>("GuoYangStatusCustomerCode");
            string interfaceCode = Configuration.GetValue<string>("GuoYangStatusInterfaceCode");
            string driverCode = Configuration.GetValue<string>("GuoYangScanItemSevenDriverCode");

            string code = Md5Crypto(interfaceCode + customerCode);
            string returnContent = "";
            bool isSuccess = true;
            List<GuoYangApiReturnStatus> entities = new List<GuoYangApiReturnStatus>();
            List<string> deliverySuccessChekcNumbers = new List<string>();
            GuoYangStatus[] guoyangStatus = IntermodalRepository.GetGuoYangStatuses();
            List<TtDeliveryScanLog> scanLogs = new List<TtDeliveryScanLog>();
            IEnumerable<GuoYangRequest> requests = IntermodalRepository.SelectGuoYangRequest(activeGuoYangCheckNumberArray);
            string remark = "";

            List<GuoYangApiReturnStatus> latestStatus = IntermodalRepository.GetTheLatestApiReturnStatus(activeGuoYangCheckNumberArray);

            for (int i = 0; i < activeGuoYangCheckNumberArray.Length; i++)
            {
                try
                {
                    string completeUrl = string.Format(statusUrl, customerCode, code, activeGuoYangCheckNumberArray[i]);
                    using (WebClient webClient = new WebClient())
                    {
                        using (Stream stream = webClient.OpenRead(completeUrl))
                        {
                            using (StreamReader reader = new StreamReader(stream))
                            {
                                returnContent = reader.ReadToEnd();

                            }
                        }
                    }

                    string[] returnContentSplit = returnContent.Split('=', '&');
                    if (returnContentSplit.Length == 12)
                    {
                        GuoYangStatus statusEntity = guoyangStatus.Where(s => s.GuoyangStatusName == returnContentSplit[11]).FirstOrDefault();

                        GuoYangApiReturnStatus entity = new GuoYangApiReturnStatus
                        {
                            ApiPostNo = returnContentSplit[1],
                            ApiType = returnContentSplit[3],
                            ApiInDate = DateTime.Parse(returnContentSplit[5]),
                            ApiNowType = returnContentSplit[7],
                            ApiNowDate = DateTime.Parse(returnContentSplit[9]),
                            ApiRemark = returnContentSplit[11],
                            Cdate = DateTime.Now,
                            ScheduleId = idStepTwo,
                            FseScanItem = statusEntity.FseScanItem,
                            FseArriveOption = statusEntity.FseArriveOption,
                            FseDeliveryOption = statusEntity.FseDeliveryOption,
                            FseReceiveOption = statusEntity.FseReceiveOption,
                            FseSendOption = statusEntity.FseSendOption
                        };

                        if (latestStatus.Where(ls => ls.ApiPostNo == entity.ApiPostNo && ls.ApiType == entity.ApiType && ls.ApiInDate == entity.ApiInDate && ls.ApiNowDate == entity.ApiNowDate && ls.ApiNowType == entity.ApiNowType && ls.ApiRemark == entity.ApiRemark).Count() == 0)       //若與上一筆一樣，則不寫入
                        {
                            TtDeliveryScanLog scanLogEntity = new TtDeliveryScanLog
                            {
                                DriverCode = driverCode,
                                CheckNumber = requests.Where(x => x.GuoyangCheckNumber == entity.ApiPostNo).FirstOrDefault()?.FseCheckNumber, 
                                ScanDate = entity.ApiNowDate,
                                Cdate = DateTime.Now,
                                ArriveOption = entity.FseArriveOption,
                                DeliveryOption = entity.FseDeliveryOption,
                                ReceiveOption = entity.FseReceiveOption,
                                SendOption = entity.FseSendOption,
                                ScanItem = entity.FseScanItem
                            };

                            entities.Add(entity);

                            scanLogs.Add(scanLogEntity);

                            if (entity.ApiRemark == "投遞成功")
                            {
                                deliverySuccessChekcNumbers.Add(activeGuoYangCheckNumberArray[i]);
                            }
                        }
                    }
                    else
                    {
                        if (latestStatus.Where(ls => ls.ApiPostNo == activeGuoYangCheckNumberArray[i] && ls.Remark == returnContent).Count() == 0)       //若與上一筆一樣，則不寫入
                        {
                            GuoYangApiReturnStatus entity = new GuoYangApiReturnStatus
                            {
                                ApiPostNo = activeGuoYangCheckNumberArray[i],
                                Remark = returnContent,
                                Cdate = DateTime.Now,
                                ScheduleId = idStepTwo
                            };
                            entities.Add(entity);
                        }
                    }
                }
                catch (Exception ex)
                {
                    isSuccess = false;
                    remark += ex.ToString();
                }
            }

            IntermodalRepository.InsertIntoApiReturn(entities);


            IEnumerable<GuoYangRequest> deliverySuccessRequests = IntermodalRepository.SelectGuoYangRequest(deliverySuccessChekcNumbers);
            foreach (var d in deliverySuccessRequests)
            {
                d.IsClosed = true;
            }

            IntermodalRepository.BulkyUpdateGuoYang(deliverySuccessRequests);

            IntermodalRepository.InsertIntoTtDeliveryScanLog(scanLogs);

            GuoYangScheduleLog(2, isSuccess, true);

            return isSuccess;
        }

        string Md5Crypto(string plainText)
        {
            string result = "";
            using (var cryptoMD5 = System.Security.Cryptography.MD5.Create())
            {
                var bytes = Encoding.UTF8.GetBytes(plainText);
                var hash = cryptoMD5.ComputeHash(bytes);
                result = BitConverter.ToString(hash).Replace("-", String.Empty).ToLower();
            }
            return result;
        }
    }
}
