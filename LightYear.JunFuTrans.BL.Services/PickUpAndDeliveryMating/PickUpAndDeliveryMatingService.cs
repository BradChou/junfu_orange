﻿using AutoMapper;
using LightYear.JunFuTrans.BL.BE.PickUpAndDeliveryMating;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.Repositories.Account;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRequest;
using LightYear.JunFuTrans.DA.Repositories.PickUpAndDeliveryMating;
using LightYear.JunFuTrans.DA.Repositories.Station;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.PickUpAndDeliveryMating
{
    public class PickUpAndDeliveryMatingService : IPickUpAndDeliveryMatingService
    {
        public IPickUpAndDeliveryMatingRepository PickUpAndDeliveryMatingRepository { get; set; }
        public IWriteOffRepository WriteOffRepository { get; set; }
        public IDriverRepository DriverRepository { get; set; }
        public IMapper Mapper { get; set; }
        public IStationRepository StationRepository { get; set; }

        public PickUpAndDeliveryMatingService(
            IPickUpAndDeliveryMatingRepository pickUpAndDeliveryMatingRepository,
            IMapper mapper,
            IWriteOffRepository writeOffRepository,
            IDriverRepository driverRepository,
            IStationRepository stationRepository
            )
        {
            this.PickUpAndDeliveryMatingRepository = pickUpAndDeliveryMatingRepository;
            this.Mapper = mapper;
            this.WriteOffRepository = writeOffRepository;
            this.DriverRepository = driverRepository;
            this.StationRepository = stationRepository;
        }

        public List<PickUpAndDeliveryMatingEntity> GetAllByAreaStationScodeTimeAndStatus(string area, string scode, DateTime month, string status)
        {

            if (area.Length > 0 && scode == "0")
            {
                // get by area
                var stations = StationRepository.GetStationByArea(area).Select(s => s.StationScode);

                return GetAllByStationsTimeAndStatus(stations, month, status);
            }
            else
            {
                // get by station
                return GetAllByStationScodeTimeAndStatus(scode, month, status);
            }

            #region MyRegion
            /*var checknumberlist = Entities.Select(p => p.CheckNumber).ToList();

    var writeoffnormal = WriteOffRepository.GetWriteOffsNormal(checknumberlist);

    if (writeoffnormal != null)
    {
        var writeofflist = WriteOffRepository.GetWriteOffList(checknumberlist);

        var writeoffchecknumber = writeofflist.Select(p => p.CheckNumber).ToList();

        int counterid = 0;

        foreach (var Entity in Entities)
        {
            for (int i = 0; i < writeoffchecknumber.Count; i++)
            {
                WriteOff writeoff = writeoffnormal[writeoffchecknumber[i]];
                if (Entity.CheckNumber == writeoff.CheckNumber)
                {
                    Entity.CheckNumber = writeoff.CheckNumber;
                    var name = DriverRepository.GetByEmpCode(writeoff.EmpCode).DriverName;
                    var code = DriverRepository.GetByEmpCode(writeoff.EmpCode).DriverCode;
                    Entity.DriverFullName = code + "-" + name;
                }
            }
            counterid++;
            Entity.Id = counterid;
            Entity.ScanItem = "已配達";
        }
    }*/
            #endregion
        }

        public List<PickUpAndDeliveryMatingEntity> GetDetailByStationScodeAndTime(string scode, DateTime month)
        {
            return GetAllByStationScodeTimeAndStatus(scode, month, "*");
        }

        // 明細 by 司機
        public List<PickUpAndDeliveryMatingEntity> GetDetailByDriver(string driverCode, DateTime month)
        {
            List<PickUpAndDeliveryMatingEntity> result = new List<PickUpAndDeliveryMatingEntity>();

            List<PickUpAndDeliveryMatingEntity> pickUpAndDeliveryMatingEntities = PickUpAndDeliveryMatingRepository.GetPickUpByMonthAndDriver(month, driverCode);
            result.AddRange(pickUpAndDeliveryMatingEntities);

            List<PickUpAndDeliveryMatingEntity> deliveryMatingEntities = PickUpAndDeliveryMatingRepository.GetDeliveryMatingByMonthAndDriver(month, driverCode);
            result.AddRange(deliveryMatingEntities);

            return result;
        }

        // 明細 by 站所
        private List<PickUpAndDeliveryMatingEntity> GetAllByStationScodeTimeAndStatus(string scode, DateTime month, string status)
        {
            List<PickUpAndDeliveryMatingEntity> entities = new List<PickUpAndDeliveryMatingEntity>();

            if (status == "5" || status == "*")
            {
                List<PickUpAndDeliveryMatingEntity> pickUpAndDeliveryMatingEntities = PickUpAndDeliveryMatingRepository.GetPickUpByMonthStationScode(month, scode);
                entities.AddRange(pickUpAndDeliveryMatingEntities);
            }

            if (status == "3" || status == "*")
            {
                List<PickUpAndDeliveryMatingEntity> matingEntities = PickUpAndDeliveryMatingRepository.GetDeliveryMatingByMonthStationScode(month, scode);
                entities.AddRange(matingEntities);
            }

            return entities.OrderByDescending(d => d.DriverCode).ToList();
        }

        // (目前沒有用)明細 by 區域
        private List<PickUpAndDeliveryMatingEntity> GetAllByStationsTimeAndStatus(IEnumerable<string> stations, DateTime month, string status)
        {
            List<PickUpAndDeliveryMatingEntity> Entities = new List<PickUpAndDeliveryMatingEntity>();

            if (status == "5" || status == "*")
            {
                List<PickUpAndDeliveryMatingEntity> pickUpAndDeliveryMatingEntities = PickUpAndDeliveryMatingRepository.GetPickUpByMonthAndArea(month, stations);
                foreach (var Entity in pickUpAndDeliveryMatingEntities)
                {
                    Entity.ScanItem = "已集貨";
                }
                Entities.AddRange(pickUpAndDeliveryMatingEntities);
            }

            if (status == "3" || status == "*")
            {
                List<PickUpAndDeliveryMatingEntity> matingEntities = PickUpAndDeliveryMatingRepository.GetDeliveryMatingByMonthAndStations(month, stations);
                foreach (var Entity in matingEntities)
                {
                    Entity.ScanItem = "已配達";
                }
                Entities.AddRange(matingEntities);
            }

            return Entities.OrderByDescending(d => d.DriverCode).ToList();
        }

        public List<PickUpAndDeliveryMatingFirstEntity> GetDriverAllCountByMonthStationScode(string area, string scode, DateTime month)
        {
            List<PickUpAndDeliveryMatingFirstEntity> Entities = new List<PickUpAndDeliveryMatingFirstEntity>();

            if (scode == "-1")
            {
                Entities = PickUpAndDeliveryMatingRepository.GetDriverAllCountByMonth(month);
            }
            else if (scode == "0")
            {
                var stations = StationRepository.GetStationByArea(area).Select(s => s.StationScode);
                Entities = PickUpAndDeliveryMatingRepository.GetDriverAllCountByMonthStations(month, stations);
            }
            else
            {
                Entities = PickUpAndDeliveryMatingRepository.GetDriverAllCountByMonthStationScode(month, scode);
            }

            var s = StationRepository.GetAll();
            for (var i = 0; i < Entities.Count; i++)
            {
                for (var j = 0; j < s.Count; j++)
                {
                    if (Entities[i].StationScode == s[j].StationScode)
                    {
                        Entities[i].StationName = s[j].StationName;
                        break;
                    }
                }
            }
            return Entities.OrderByDescending(d => d.DriverCode).ToList();
        }
    }
}
