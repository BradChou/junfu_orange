﻿using LightYear.JunFuTrans.BL.BE.PickUpAndDeliveryMating;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.PickUpAndDeliveryMating
{
    public interface IPickUpAndDeliveryMatingService
    {
        List<PickUpAndDeliveryMatingEntity> GetAllByAreaStationScodeTimeAndStatus(string area, string scode, DateTime month, string status);

        List<PickUpAndDeliveryMatingEntity> GetDetailByStationScodeAndTime(string scode, DateTime month);

        List<PickUpAndDeliveryMatingEntity> GetDetailByDriver(string driverCode, DateTime month);

        List<PickUpAndDeliveryMatingFirstEntity> GetDriverAllCountByMonthStationScode(string area, string scode, DateTime month);
    }
}
