﻿using AutoMapper;
using LightYear.JunFuTrans.BL.BE.TMS;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRequest;
using LightYear.JunFuTrans.DA.Repositories.Station;
using LightYear.JunFuTrans.DA.Repositories.TMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.TMS
{
    public class TMSPalletMatrixService : ITMSPalletMatrixService
    {
        public ITMSScheduleService TMSScheduleService;

        public IDeliveryRequestRepository DeliveryRequestRepository { get; }

        public IBSectionFixedRunRepository BSectionFixedRunRepository { get; }

        public ILtWarehouseRepository LtWarehouseRepository { get; }

        public IStationRepository StationRepository { get; set; }

        public IMapper Mapper { get; set; }

        public TMSPalletMatrixService(ITMSScheduleService tMSScheduleService, IDeliveryRequestRepository deliveryRequestRepository,
             IBSectionFixedRunRepository bSectionFixedRunRepository, ILtWarehouseRepository ltWarehouseRepository, IStationRepository stationRepository,
            IMapper mapper)
        {
            DeliveryRequestRepository = deliveryRequestRepository;
            TMSScheduleService = tMSScheduleService;
            BSectionFixedRunRepository = bSectionFixedRunRepository;
            LtWarehouseRepository = ltWarehouseRepository;
            StationRepository = stationRepository;
            Mapper = mapper;
        }

        #region 板數表
        public List<TmsPositionStationEntity> GetStationPositionTitle()
        {
            List<BSectionStop> bSectionStops = TMSScheduleService.GetBSectionStops();
            List<string> positions = new List<string>() { "北", "中", "南", "東" };

            List<TmsPositionStationEntity> stationPositionEntities = new List<TmsPositionStationEntity>();

            for (int i = 0; i < positions.Count; i++)
            {
                List<BSectionStop> bSections = bSectionStops.Where(s => s.Position == positions[i] && s.IsActive == true)
                    .OrderBy(s => s.CityId).ToList();

                if (bSections == null || bSections.Count == 0)
                {
                    continue;
                }

                stationPositionEntities.Add(new TmsPositionStationEntity()
                {
                    Position = positions[i],
                    Stations = bSections.Select(s => s.Name).ToList()
                });
            }

            stationPositionEntities.Add(new TmsPositionStationEntity() { Stations = new List<string> { "發送總計" } });

            return stationPositionEntities;
        }

        public List<TMSPlateCountEntity> GetPalletCountByPrintDate(DateTime printDate)
        {
            List<BSectionStop> stops = TMSScheduleService.GetBSectionStops()
                .Where(s => s.IsActive == true).OrderBy(s => s.CityId).ToList();

            var deliveryRequest = DeliveryRequestRepository.GetPalletByPrintDate(printDate);

            Dictionary<int, List<string>> stopIdsupplierCode = GetSupplierCodeByStopIds(stops.Select(s => s.Id).ToList());

            List<TMSPlateCountEntity> tmsPalletCount = new List<TMSPlateCountEntity>();
            foreach (var arrive in stops)
            {
                List<PalletCountEntity> palletCount = new List<PalletCountEntity>();
                List<string> arriveSup = stopIdsupplierCode[arrive.Id];

                foreach (var send in stops)
                {
                    int count = 0;

                    List<string> sendSup = stopIdsupplierCode[send.Id];

                    count += GetCountBySupplierCode(deliveryRequest, arriveSup, sendSup);

                    palletCount.Add(new PalletCountEntity { SendStation = send.Name, Count = count });
                }

                tmsPalletCount.Add(new TMSPlateCountEntity { AreaArrive = arrive.Name, RowData = palletCount });
            }

            // add row sum
            foreach (var row in tmsPalletCount)
            {
                int rowSum = 0;
                rowSum += row.RowData.Sum(p => p.Count);
                row.RowData.Add(new PalletCountEntity { SendStation = "發送總計", Count = rowSum });
            }

            // add column sum
            tmsPalletCount.Add(new TMSPlateCountEntity { AreaArrive = "到著總計", RowData = new List<PalletCountEntity>() });
            for (int i = 0; i <= stops.Count; i++)
            {
                tmsPalletCount.Last().RowData.Add(new PalletCountEntity());
                int colSum = 0;
                colSum += tmsPalletCount.Sum(s => s.RowData[i].Count);
                tmsPalletCount.Last().RowData[i].Count = colSum;
            }

            return tmsPalletCount;
        }

        private int GetCountBySupplierCode(List<TcDeliveryRequest> deliveryRequests, List<string> arriveStation, List<string> sendStation)
        {
            var plateCountEntity = from d in deliveryRequests
                                   where arriveStation.Contains(d.AreaArriveCode) && sendStation.Contains(d.SendStationScode)
                                   && d.AreaArriveCode != d.SendStationScode && d.PricingType != "05" && d.CancelDate == null
                                   select d.Plates ?? 0;

            return plateCountEntity.Sum();
        }
        #endregion

        public Dictionary<int, List<string>> GetSupplierCodeByStopIds(List<int> stopIds)
        {
            var dic = BSectionFixedRunRepository.GetMappingByStopIDs(stopIds);

            var suppliers = BSectionFixedRunRepository.GetSuppliers();

            Dictionary<int, List<string>> result = new Dictionary<int, List<string>>();

            List<decimal> supplierIds;

            foreach (var entry in dic)
            {
                supplierIds = entry.Value.ToList();
                var supplierCodes = suppliers.Where(s => supplierIds.Contains(s.SupplierId)).Select(s => s.SupplierCode).ToList();
                result.Add(entry.Key, supplierCodes);
            }

            return result;
        }

        public List<TmsBoxCountEntity> GetBoxCountEntities(DateTime shipDate)
        {
            var warehouse = LtWarehouseRepository.GetLtWarehouses();
            var warehouseStationIdMapping = LtWarehouseRepository.GetWarehouseStationMapping();
            var stations = StationRepository.GetAll();

            Dictionary<int, List<string>> warehouseStationMapping = new Dictionary<int, List<string>>();
            foreach (var entry in warehouseStationIdMapping)
            {
                warehouseStationMapping.Add(entry.Key, stations.Where(s => entry.Value.Contains(s.Id)).Select(s => s.StationScode).ToList());
            }

            var requests = DeliveryRequestRepository.GetByShipDatePeriod(shipDate.Date, shipDate.Date.AddDays(1));
            var requestsWithoutSameStation = requests.Where(r => r.AreaArriveCode != r.SendStationScode).ToList();

            var requestWithoutMomo = requestsWithoutSameStation.Where(r => r.CustomerCode != "F3500010002").ToList();            

            List<TmsBoxCountEntity> resultFromRequests = new List<TmsBoxCountEntity>();

            foreach (var entry in warehouseStationMapping)
            {
                string warehouseName = warehouse.FirstOrDefault(w => w.Id == entry.Key).Name;

                if (entry.Key != 8)
                {
                    var mixBoxCount = requestWithoutMomo.Where(r => entry.Value.Contains(r.SendStationScode)).Sum(s => s.Pieces ?? 0);
                    var flowBoxCount = requestWithoutMomo.Where(r => entry.Value.Contains(r.AreaArriveCode)).Sum(s => s.Pieces ?? 0);

                    resultFromRequests.Add(new TmsBoxCountEntity
                    {
                        BSectionStopId = entry.Key,
                        BSectionStop = warehouseName,
                        ShipDate = shipDate.Date,
                        EstimateMixBoxCount = mixBoxCount / 75,
                        EstimateFlowBoxCount = flowBoxCount / 75
                    });
                }
                else
                {
                    var mixBoxCount = requestsWithoutSameStation.Where(r => r.CustomerCode == "F3500010002")
                        .Where(r => entry.Value.Contains(r.AreaArriveCode)).Sum(s => s.Pieces ?? 0);

                    resultFromRequests.Add(new TmsBoxCountEntity
                    {
                        BSectionStopId = entry.Key,
                        BSectionStop = warehouseName,
                        ShipDate = shipDate.Date,
                        EstimateMixBoxCount = mixBoxCount / 75,
                        EstimateFlowBoxCount = 0
                    });
                }
            }

            var warehouseBoxCount = LtWarehouseRepository.GetBoxCountsByPrintDate(shipDate);

            foreach (var e in resultFromRequests)
            {
                var data = warehouseBoxCount.FirstOrDefault(w => w.WarehouseId == e.BSectionStopId);
                if (data == null)
                    continue;
                e.Id = data.Id;
                e.ActualFlowBoxCount = data.ActualFlowCount;
                e.ActualMixBoxCount = data.ActualMixCount;
            }

            return resultFromRequests;
        }

        public bool UpdateBoxCountEntity(TmsBoxCountEntity entity, string accountCode)
        {
            LtWarehouseBoxCount boxCount = Mapper.Map<LtWarehouseBoxCount>(entity);

            if (entity.Id == 0)
            {
                boxCount.Cdate = DateTime.Now;
                LtWarehouseRepository.InsertBoxCountEntity(boxCount);
            }
            else
            {
                boxCount.Uuser = accountCode;
                boxCount.Udate = DateTime.Now;

                LtWarehouseRepository.UpdateBoxCountEntity(boxCount);
            }

            return true;
        }

        public bool UpdateBoxCountEntityBatch(List<TmsBoxCountEntity> entities, string accountCode)
        {
            List<LtWarehouseBoxCount> boxCount = Mapper.Map<List<LtWarehouseBoxCount>>(entities);

            foreach (var e in boxCount)
            {
                if (e.Id == 0)
                {
                    e.Cdate = DateTime.Now;
                    LtWarehouseRepository.InsertBoxCountEntity(e);
                }
                else
                {
                    e.Uuser = accountCode;
                    e.Udate = DateTime.Now;

                    LtWarehouseRepository.UpdateBoxCountEntity(e);
                }
            }

            return true;
        }
    }
}
