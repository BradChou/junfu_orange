﻿using LightYear.JunFuTrans.BL.BE.TMS;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.TMS
{
    public interface ITMSPalletMatrixService
    {
        List<TmsPositionStationEntity> GetStationPositionTitle();

        List<TMSPlateCountEntity> GetPalletCountByPrintDate(DateTime printDate);

        Dictionary<int, List<string>> GetSupplierCodeByStopIds(List<int> stopIds);

        List<TmsBoxCountEntity> GetBoxCountEntities(DateTime printDate);

        bool UpdateBoxCountEntity(TmsBoxCountEntity entity, string accountCode);

        bool UpdateBoxCountEntityBatch(List<TmsBoxCountEntity> entities, string accountCode);
    }
}
