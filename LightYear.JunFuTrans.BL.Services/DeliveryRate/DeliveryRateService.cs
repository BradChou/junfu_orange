﻿using AutoMapper;
using AutoMapper.Mappers.Internal;
using LightYear.JunFuTrans.BL.BE.DeliveryRate;
using LightYear.JunFuTrans.BL.Services.Barcode;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.Repositories.Account;
using LightYear.JunFuTrans.DA.Repositories.Customer;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRate;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRequest;
using LightYear.JunFuTrans.DA.Repositories.Station;
using Microsoft.EntityFrameworkCore.Migrations.Operations;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.DeliveryRate
{
    public class DeliveryRateService : IDeliveryRateService
    {
        public IDeliveryRequestRepository DeliveryRequestRepository { get; set; }

        public IDeliveryScanLogRepository DeliveryScanLogRepository { get; set; }

        public IDeliveryRateRepository DeliveryRateRepository { get; set; }

        public IStationRepository StationRepository { get; set; }

        public IStationAreaRepository StationAreaRepository { get; set; }

        public ICustomerRepository CustomerRepository { get; set; }

        public IDriverRepository DriverRepository { get; set; }

        public IDeliveryRequestJoinDeliveryScanLogRepository DeliveryRequestJoinDeliveryScanLogRepository { get; set; }

        public IMapper Mapper { get; set; }

        public DeliveryRateService(IDeliveryRateRepository deliveryRateRepository, IStationRepository stationRepository,
            ICustomerRepository customerRepository, IDriverRepository driverRepository, IMapper mapper, IDeliveryRequestRepository deliveryRequestRepository,
            IDeliveryScanLogRepository deliveryScanLogRepository, IDeliveryRequestJoinDeliveryScanLogRepository deliveryRequestJoinDeliveryScanLogRepository,
            IStationAreaRepository stationAreaRepository)
        {
            DeliveryRateRepository = deliveryRateRepository;
            StationRepository = stationRepository;
            CustomerRepository = customerRepository;
            DriverRepository = driverRepository;
            Mapper = mapper;
            DeliveryRequestRepository = deliveryRequestRepository;
            DeliveryScanLogRepository = deliveryScanLogRepository;
            DeliveryRequestJoinDeliveryScanLogRepository = deliveryRequestJoinDeliveryScanLogRepository;
            StationAreaRepository = stationAreaRepository;
        }

        public List<StationEntity> GetStations()
        {
            List<TbStation> stationList = StationRepository.GetAll();
            List<StationEntity> stationEntities = Mapper.Map<List<TbStation>, List<StationEntity>>(stationList);
            stationEntities.Insert(0, new StationEntity { StationName = "全公司", StationScode = "All", StationCode = "All" });

            return stationEntities;
        }

        public List<CustomerEntity> GetLTLCustomers()
        {
            List<TbStation> stations = StationRepository.GetAll();
            List<TbCustomer> customers = new List<TbCustomer>();
            foreach (var s in stations)
            {
                var customersByStation = CustomerRepository.GetCustomersByStation(s.StationCode);
                customers.AddRange(customersByStation);
            }

            var customerEntities = Mapper.Map<List<TbCustomer>, List<CustomerEntity>>(customers);
            customerEntities.Insert(0, new CustomerEntity { CustomerName = "全選", CustomerCode = "All" });

            return customerEntities;
        }

        public DeliveryRateEntity GetTodaysDeliveryRateByStation(string stationScode)
        {
            IEnumerable<DeliveryRequestJoinScanLogForMatingRate> requests = GetEntitiesByStationListAndDeliveryDate(new List<string> { stationScode }, DateTime.Today, DateTime.Today);
            int orderNumber = requests.Select(r => r.CheckNumber).Distinct().Count();
            int matingNumber = requests.Where(r => r.ArriveOption == "3").Select(r => r.CheckNumber).Distinct().Count();

            return new DeliveryRateEntity
            {
                OrdersNumber = orderNumber,
                MatingNumber = matingNumber
            };
        }

        public DeliveryRateEntity GetTodaysDeliveryRate()
        {
            IEnumerable<DeliveryRequestJoinScanLogForMatingRate> requests = GetEntitiesByDeliveryDate(DateTime.Today, DateTime.Today);
            int orderNumber = requests.Select(r => r.CheckNumber).Distinct().Count();
            int matingNumber = requests.Where(r => r.ArriveOption == "3").Select(r => r.CheckNumber).Distinct().Count();

            return new DeliveryRateEntity
            {
                OrdersNumber = orderNumber,
                MatingNumber = matingNumber
            };
        }

        #region 第一層
        public List<DeliveryRateEntity> GetDeliveryRateEntitiesByTimePeriod(DateTime start, DateTime end)
        {
            IEnumerable<DeliveryRequestJoinScanLogForMatingRate> requests = GetEntitiesByDeliveryDate(start, end).ToList();

            var unsortedResult = GetDeliveryRateEntitiesByJoinedEntity(requests);

            var sortedResult = SortDeliveryRateEntities(unsortedResult);

            return sortedResult;
        }

        public List<DeliveryRateEntity> GetDeliveryRateEntitiesByAreaAndTimePeriod(string areaName, DateTime start, DateTime end)
        {
            var stationScode = StationAreaRepository.GetStationsByAreaName(areaName).Select(s => s.StationScode);
            var requests = GetEntitiesByStationListAndDeliveryDate(stationScode, start, end).ToList();

            var unsortedResult = GetDeliveryRateEntitiesByJoinedEntity(requests);

            var sortedResult = SortDeliveryRateEntities(unsortedResult);

            return sortedResult;
        }

        public List<DeliveryRateEntity> GetDeliveryRateEntityByStationAndTimePeriod(DateTime start, DateTime end, string stationScode)
        {
            var requests = GetEntitiesByStationListAndDeliveryDate(new List<string>() { stationScode }, start, end).ToList();

            var unsortedResult = GetDeliveryRateEntitiesByJoinedEntity(requests);

            var sortedResult = SortDeliveryRateEntities(unsortedResult);

            return sortedResult;
        }

        public List<DeliveryRateEntity> GetDeliveryRateEntityByCustomerAndTimePeriod(DateTime start, DateTime end, string customerCode)
        {
            IEnumerable<DeliveryRequestJoinScanLogForMatingRate> requests = GetEntitiesByCustomerAndDeliveryDate(customerCode, start, end);

            var unsortedResult = GetDeliveryRateEntitiesByJoinedEntity(requests);

            var sortedResult = SortDeliveryRateEntities(unsortedResult);

            return sortedResult;
        }


        private IEnumerable<DeliveryRequestJoinScanLogForMatingRate> GetEntitiesByDeliveryDate(DateTime deliveryDateStart, DateTime deliveryDateEnd)
        {
            // 從ship date 抓
            DateTime shipDateStart = deliveryDateStart.AddDays(-1).AddHours(5);
            DateTime shipDateEnd = deliveryDateEnd.AddDays(-1).AddHours(5);
            IEnumerable<DeliveryRequestJoinScanLogForMatingRate> requestsGetByShipDate = DeliveryRequestJoinDeliveryScanLogRepository.GetJoinedEntitiesByShipDatePeriod(shipDateStart, shipDateEnd).ToList();

            // 時間內有掃配送
            IEnumerable<DeliveryRequestJoinScanLogForMatingRate> requestsGetByDeliveryDate = DeliveryRequestJoinDeliveryScanLogRepository.GetJoinedEntityByDeliveryDate(deliveryDateStart.AddHours(5), deliveryDateEnd.AddHours(5)).ToList();

            // 沒配送也沒銷單
            var notMating = DeliveryRequestRepository.GetNotMatingByShipDatePeriod(shipDateStart.AddDays(-7), shipDateStart);
            var notMatingEntities = Mapper.Map<IEnumerable<DeliveryRequestJoinScanLogForMatingRate>>(notMating);

            return (requestsGetByShipDate ?? Enumerable.Empty<DeliveryRequestJoinScanLogForMatingRate>())
                .Concat(requestsGetByDeliveryDate ?? Enumerable.Empty<DeliveryRequestJoinScanLogForMatingRate>())
                .Concat(notMatingEntities ?? Enumerable.Empty<DeliveryRequestJoinScanLogForMatingRate>());
        }

        private IEnumerable<DeliveryRequestJoinScanLogForMatingRate> GetEntitiesByStationListAndDeliveryDate(IEnumerable<string> stationScode, DateTime deliveryDateStart, DateTime deliveryDateEnd)
        {
            // 從ship date 抓
            DateTime shipDateStart = deliveryDateStart.AddDays(-1).AddHours(5);
            DateTime shipDateEnd = deliveryDateEnd.AddDays(-1).AddHours(5);
            IEnumerable<DeliveryRequestJoinScanLogForMatingRate> requestsGetByShipDate =
                DeliveryRequestJoinDeliveryScanLogRepository.GetJoinedEntitiesByStationAndShipDatePeriod(stationScode, shipDateStart, shipDateEnd).ToList();

            // 時間內有掃配送
            IEnumerable<DeliveryRequestJoinScanLogForMatingRate> requestsGetByDeliveryDate =
                DeliveryRequestJoinDeliveryScanLogRepository.GetJoinedEntityByStationListAndDeliveryDate(stationScode, deliveryDateStart.AddHours(5), deliveryDateEnd.AddHours(5)).ToList();

            // 沒配送也沒銷單
            var notMating = DeliveryRequestRepository.GetNotMatingByStationListAndShipDatePeriod(stationScode, shipDateStart.AddDays(-7), shipDateStart);
            var notMatingEntities = Mapper.Map<IEnumerable<DeliveryRequestJoinScanLogForMatingRate>>(notMating);

            return (requestsGetByShipDate ?? Enumerable.Empty<DeliveryRequestJoinScanLogForMatingRate>())
                .Concat(requestsGetByDeliveryDate ?? Enumerable.Empty<DeliveryRequestJoinScanLogForMatingRate>())
                .Concat(notMatingEntities ?? Enumerable.Empty<DeliveryRequestJoinScanLogForMatingRate>());
        }

        private IEnumerable<DeliveryRequestJoinScanLogForMatingRate> GetEntitiesByCustomerAndDeliveryDate(string customerCode, DateTime deliveryDateStart, DateTime deliveryDateEnd)
        {
            // 從ship date 抓
            DateTime shipDateStart = deliveryDateStart.AddDays(-1).AddHours(5);
            DateTime shipDateEnd = deliveryDateEnd.AddDays(-1).AddHours(5);
            IEnumerable<DeliveryRequestJoinScanLogForMatingRate> requestsGetByShipDate =
                DeliveryRequestJoinDeliveryScanLogRepository.GetJoinedEntitiesByCustomerAndShipDatePeriod(customerCode, shipDateStart, shipDateEnd).ToList();

            // 時間內有掃配送
            IEnumerable<DeliveryRequestJoinScanLogForMatingRate> requestsGetByDeliveryDate =
                DeliveryRequestJoinDeliveryScanLogRepository.GetJoinedEntityByCustomerAndDeliveryDate(customerCode, deliveryDateStart.AddHours(5), deliveryDateEnd.AddHours(5)).ToList();

            // 沒配送也沒銷單
            var notMating = DeliveryRequestRepository.GetNotMatingByCustomerAndShipDatePeriod(customerCode, shipDateStart.AddDays(-7), shipDateStart);
            var notMatingEntities = Mapper.Map<IEnumerable<DeliveryRequestJoinScanLogForMatingRate>>(notMating);

            return (requestsGetByShipDate ?? Enumerable.Empty<DeliveryRequestJoinScanLogForMatingRate>())
                .Concat(requestsGetByDeliveryDate ?? Enumerable.Empty<DeliveryRequestJoinScanLogForMatingRate>())
                .Concat(notMatingEntities ?? Enumerable.Empty<DeliveryRequestJoinScanLogForMatingRate>());
        }

        private List<DeliveryRateEntity> GetDeliveryRateEntitiesByJoinedEntity(IEnumerable<DeliveryRequestJoinScanLogForMatingRate> requests)
        {
            var orderNumber = CountOrderByJoinedEntity(requests);
            var deliveryNumber = CountDeliveryByJoinedEntity(requests);
            var matingBeforeNoon = CountDeliveryMatingByJoinedEntity(requests, 12);
            var matingBeforeFifteen = CountDeliveryMatingByJoinedEntity(requests, 15);
            var matingBeforeEighteen = CountDeliveryMatingByJoinedEntity(requests, 18);
            var matingBeforeTwenty = CountDeliveryMatingByJoinedEntity(requests, 20);
            var matingNumber = CountDeliveryMatingByJoinedEntity(requests, 24);
            var notWriteOff = CountNotWriteOffByJoinedEntity(requests);
            var notDelivery = CountNotDelivey(requests);

            var stations = StationRepository.GetAll();

            var deliveryRateEntity = (from o in orderNumber
                                      join mn in matingBeforeNoon on o.StationScode equals mn.StationScode into s
                                      from mn in s.DefaultIfEmpty()
                                      join mf in matingBeforeFifteen on o.StationScode equals mf.StationScode into t
                                      from mf in t.DefaultIfEmpty()
                                      join me in matingBeforeEighteen on o.StationScode equals me.StationScode into u
                                      from me in u.DefaultIfEmpty()
                                      join mt in matingBeforeTwenty on o.StationScode equals mt.StationScode into v
                                      from mt in v.DefaultIfEmpty()
                                      join m in matingNumber on o.StationScode equals m.StationScode into w
                                      from m in w.DefaultIfEmpty()
                                      join nwf in notWriteOff on o.StationScode equals nwf.StationScode into x
                                      from nwf in x.DefaultIfEmpty()
                                      join nd in notDelivery on o.StationScode equals nd.StationScode into y
                                      from nd in y.DefaultIfEmpty()
                                      join stn in stations on o.StationScode equals stn.StationScode into z
                                      from stn in z.DefaultIfEmpty()
                                      select new DeliveryRateEntity
                                      {
                                          StationScode = o.StationScode,
                                          District = stn?.BusinessDistrict,
                                          OrdersNumber = o.CountNumber,
                                          DeliveryNumber = o.CountNumber,
                                          MatingNumBeforeNoon = mn == null ? 0 : mn.CountNumber,
                                          MatingNumBeforeFifteen = mf == null ? 0 : mf.CountNumber,
                                          MatingNumBeforeEighteen = me == null ? 0 : me.CountNumber,
                                          MatingNumBeforeTwenty = mt == null ? 0 : mt.CountNumber,
                                          MatingNumber = m == null ? 0 : m.CountNumber,
                                          NotWriteOffNumber = nwf == null ? 0 : nwf.CountNumber,
                                          NotDeliverNum = nd == null ? 0 : nd.CountNumber
                                      }).ToList();

            foreach (var e in deliveryRateEntity)
            {
                e.StationName = StationRepository.GetStationNameByScode(e.StationScode);
            }

            return deliveryRateEntity;
        }

        private List<DeliveryRateEntity> SortDeliveryRateEntities(List<DeliveryRateEntity> originalList)
        {
            var temp = originalList.ToList();

            temp = temp.Where(t => t.District != null).OrderBy(t => t.District).ToList();

            return temp;
        }

        public IEnumerable<StationCountEntity> CountDeliveryByJoinedEntity(IEnumerable<DeliveryRequestJoinScanLogForMatingRate> joinedEntities)
        {
            var deliveryNumber = (from e in joinedEntities
                                  where e.ScanItem == "2"
                                  select new
                                  {
                                      CheckNumber = e.CheckNumber,
                                      StationScode = e.AreaArriveCode,
                                      Count = 1
                                  }).Distinct();

            return from d in deliveryNumber
                   group d by d.StationScode into g
                   select new StationCountEntity
                   {
                       StationScode = g.Key,
                       CountNumber = g.Sum(a => a.Count)
                   };
        }

        public IEnumerable<StationCountEntity> CountOrderByJoinedEntity(IEnumerable<DeliveryRequestJoinScanLogForMatingRate> joinedEntities)
        {
            var orderNumber = (from e in joinedEntities
                               select new
                               {
                                   CheckNumber = e.CheckNumber,
                                   StationScode = e.AreaArriveCode,
                                   Count = 1
                               }).Distinct();

            return from o in orderNumber
                   group o by o.StationScode into g
                   select new StationCountEntity
                   {
                       StationScode = g.Key,
                       CountNumber = g.Sum(a => a.Count)
                   };
        }

        public IEnumerable<StationCountEntity> CountDeliveryMatingByJoinedEntity(IEnumerable<DeliveryRequestJoinScanLogForMatingRate> joinedEntities, byte timeInDay)
        {
            var orderNumber = (from e in joinedEntities
                               where e.ScanItem == "3" && e.ArriveOption == "3" && e.ScanDate < e.ShipDate.Date.AddDays(1).AddHours(timeInDay)
                               select new
                               {
                                   CheckNumber = e.CheckNumber,
                                   StationScode = e.AreaArriveCode,
                                   Count = 1
                               }).Distinct();

            return from o in orderNumber
                   group o by o.StationScode into g
                   select new StationCountEntity
                   {
                       StationScode = g.Key,
                       CountNumber = g.Sum(a => a.Count)
                   };
        }

        public IEnumerable<StationCountEntity> CountNotWriteOffByJoinedEntity(IEnumerable<DeliveryRequestJoinScanLogForMatingRate> joinedEntities)
        {
            var matingSuccess = (from e in joinedEntities
                                 where e.ScanItem == "3" && e.ArriveOption == "3"
                                 select new
                                 {
                                     CheckNumber = e.CheckNumber,
                                     StationScode = e.AreaArriveCode,
                                     Count = 1
                                 }).Distinct();

            var total = (from e in joinedEntities
                         select new
                         {
                             CheckNumber = e.CheckNumber,
                             StationScode = e.AreaArriveCode,
                             Count = 1
                         }).Distinct();

            return from notWriteOff in total.Except(matingSuccess)
                   group notWriteOff by notWriteOff.StationScode into g
                   select new StationCountEntity
                   {
                       StationScode = g.Key,
                       CountNumber = g.Sum(a => a.Count)
                   };
        }

        public IEnumerable<StationCountEntity> CountNotDelivey(IEnumerable<DeliveryRequestJoinScanLogForMatingRate> joinedEntities)
        {
            var delivery = (from e in joinedEntities
                            where (e.ScanItem == "3" || e.ScanItem == "2")
                            select new
                            {
                                CheckNumber = e.CheckNumber,
                                StationScode = e.AreaArriveCode,
                                Count = 1
                            }).Distinct();

            var total = (from e in joinedEntities
                         select new
                         {
                             CheckNumber = e.CheckNumber,
                             StationScode = e.AreaArriveCode,
                             Count = 1
                         }).Distinct();

            return from notDelivey in total.Except(delivery)
                   group notDelivey by notDelivey.StationScode into g
                   select new StationCountEntity
                   {
                       StationScode = g.Key,
                       CountNumber = g.Sum(a => a.Count)
                   };
        }

        public DeliveryRateEntity ConvertToPercent(DeliveryRateEntity deliveryRateEntity)
        {
            return new DeliveryRateEntity
            {
                StationScode = deliveryRateEntity.StationScode,
                StationName = deliveryRateEntity.StationName,
                OrdersNumber = deliveryRateEntity.OrdersNumber,
                MatingNumBeforeNoon = deliveryRateEntity.MatingNumBeforeNoon / deliveryRateEntity.OrdersNumber,
                MatingNumBeforeFifteen = deliveryRateEntity.MatingNumBeforeFifteen / deliveryRateEntity.OrdersNumber,
                MatingNumBeforeEighteen = deliveryRateEntity.MatingNumBeforeEighteen / deliveryRateEntity.OrdersNumber,
                MatingNumBeforeTwenty = deliveryRateEntity.MatingNumBeforeTwenty / deliveryRateEntity.OrdersNumber,
                MatingNumber = deliveryRateEntity.MatingNumber / deliveryRateEntity.OrdersNumber,
                NotWriteOffNumber = deliveryRateEntity.NotWriteOffNumber / deliveryRateEntity.OrdersNumber,
                DeliveryNumber = deliveryRateEntity.DeliveryNumber / deliveryRateEntity.OrdersNumber,
                NotDeliverNum = deliveryRateEntity.NotDeliverNum / deliveryRateEntity.OrdersNumber
            };
        }
        #endregion

        #region 第二層
        public IEnumerable<DriverCountEntity> GetDriverMatingBeforeTime(IEnumerable<DeliveryRequestJoinScanLogForMatingRate> joinedEntites, byte timeInDay)
        {
            var deliveryMating = (from e in joinedEntites
                                  where e.ScanItem == "3" && e.ScanDate < e.ShipDate.Date.AddDays(1).AddHours(timeInDay)
                                  select new
                                  {
                                      CheckNumber = e.CheckNumber,
                                      DriverCode = e.DriverCode,
                                      Count = 1
                                  }).Distinct();

            return from m in deliveryMating
                   group m by m.DriverCode into g
                   select new DriverCountEntity
                   {
                       DriverCode = g.Key,
                       CountNumber = g.Sum(m => m.Count)
                   };
        }

        public IEnumerable<DriverCountEntity> GetOrderNumber(IEnumerable<DeliveryRequestJoinScanLogForMatingRate> joinedEntites)
        {
            var deliveryNumber = (from e in joinedEntites
                                  where e.DriverCode != null
                                  select new
                                  {
                                      CheckNumber = e.CheckNumber,
                                      DriverCode = e.DriverCode,
                                      Count = 1
                                  }).Distinct();

            return from m in deliveryNumber
                   group m by m.DriverCode into g
                   select new DriverCountEntity
                   {
                       DriverCode = g.Key,
                       CountNumber = g.Sum(m => m.Count)
                   };
        }

        public IEnumerable<DriverCountEntity> GetDeliveryNumber(IEnumerable<DeliveryRequestJoinScanLogForMatingRate> joinedEntites)
        {
            var deliveryNumber = (from e in joinedEntites
                                  where e.ScanItem == "2"
                                  select new
                                  {
                                      CheckNumber = e.CheckNumber,
                                      DriverCode = e.DriverCode,
                                      Count = 1
                                  }).Distinct();

            return from m in deliveryNumber
                   group m by m.DriverCode into g
                   select new DriverCountEntity
                   {
                       DriverCode = g.Key,
                       CountNumber = g.Sum(m => m.Count)
                   };
        }

        public IEnumerable<DriverCountEntity> GetNotWriteOffNumber(IEnumerable<DeliveryRequestJoinScanLogForMatingRate> joinedEntites)
        {
            var matingSuccess = (from e in joinedEntites
                                 where e.ScanItem == "3" && e.ArriveOption == "3"
                                 select new
                                 {
                                     CheckNumber = e.CheckNumber,
                                     DriverCode = e.DriverCode,
                                     Count = 1
                                 }).Distinct();

            var total = (from e in joinedEntites
                         where e.ScanItem == "2"
                         select new
                         {
                             CheckNumber = e.CheckNumber,
                             DriverCode = e.DriverCode,
                             Count = 1
                         }).Distinct();

            return from e in total.Except(matingSuccess)
                   group e by e.DriverCode into g
                   select new DriverCountEntity
                   {
                       DriverCode = g.Key,
                       CountNumber = g.Sum(m => m.Count)
                   };
        }

        public List<DeliveryRateByDriverEntity> GetEntitiesByStationSCode(string stationSCode, DateTime deliveryDateStart, DateTime deliveryDateEnd)
        {           
            IEnumerable<DeliveryRequestJoinScanLogForMatingRate> entities = GetEntitiesByStationListAndDeliveryDate(new List<string> { stationSCode }, deliveryDateStart, deliveryDateEnd);

            var orderNumber = GetOrderNumber(entities);
            var deliveryNumber = GetDeliveryNumber(entities);
            var matingBeforeNoon = GetDriverMatingBeforeTime(entities, 12);
            var matingBeforeFifteen = GetDriverMatingBeforeTime(entities, 15);
            var matingBeforeEighteen = GetDriverMatingBeforeTime(entities, 18);
            var matingBeforeTwenty = GetDriverMatingBeforeTime(entities, 20);
            var matingNumber = GetDriverMatingBeforeTime(entities, 24);
            var notWriteOff = GetNotWriteOffNumber(entities);

            var deliveryRateByDriverEntities = (from o in orderNumber
                                                join d in deliveryNumber on o.DriverCode equals d.DriverCode into z
                                                from d in z.DefaultIfEmpty()
                                                join mn in matingBeforeNoon on o.DriverCode equals mn.DriverCode into s
                                                from mn in s.DefaultIfEmpty()
                                                join mf in matingBeforeFifteen on o.DriverCode equals mf.DriverCode into t
                                                from mf in t.DefaultIfEmpty()
                                                join me in matingBeforeEighteen on o.DriverCode equals me.DriverCode into u
                                                from me in u.DefaultIfEmpty()
                                                join mt in matingBeforeTwenty on o.DriverCode equals mt.DriverCode into v
                                                from mt in v.DefaultIfEmpty()
                                                join m in matingNumber on o.DriverCode equals m.DriverCode into w
                                                from m in w.DefaultIfEmpty()
                                                join nwf in notWriteOff on o.DriverCode equals nwf.DriverCode into x
                                                from nwf in x.DefaultIfEmpty()
                                                select new DeliveryRateByDriverEntity
                                                {
                                                    DriverCode = o.DriverCode,
                                                    OrderNumber = o.CountNumber,
                                                    DeliveryNumber = d == null ? 0 : d.CountNumber,
                                                    MatingNumBeforeNoon = mn == null ? 0 : mn.CountNumber,
                                                    MatingNumBeforeFifteen = mf == null ? 0 : mf.CountNumber,
                                                    MatingNumBeforeEighteen = me == null ? 0 : me.CountNumber,
                                                    MatingNumBeforeTwenty = mt == null ? 0 : mt.CountNumber,
                                                    MatingNumber = m == null ? 0 : m.CountNumber,
                                                    NotWriteOffNumber = nwf == null ? 0 : nwf.CountNumber
                                                }).ToList();

            deliveryRateByDriverEntities = (from d in deliveryRateByDriverEntities
                                           group d by d.DriverCode.ToUpper() into g
                                           select new DeliveryRateByDriverEntity
                                           {
                                               DriverCode = g.Key,
                                               OrderNumber = g.Sum(d => d.OrderNumber),
                                               DeliveryNumber = g.Sum(d => d.OrderNumber),
                                               MatingNumBeforeNoon = g.Sum(d => d.OrderNumber),
                                               MatingNumBeforeFifteen = g.Sum(d => d.OrderNumber),
                                               MatingNumBeforeEighteen = g.Sum(d => d.OrderNumber),
                                               MatingNumBeforeTwenty = g.Sum(d => d.OrderNumber),
                                               MatingNumber = g.Sum(d => d.OrderNumber),
                                               NotWriteOffNumber = g.Sum(d => d.OrderNumber)
                                           }).ToList();

            foreach (var e in deliveryRateByDriverEntities)
            {
                var driver = DriverRepository.GetByDriverCode(e.DriverCode);
                e.DriverName = e.DriverCode + " " + driver.DriverName;
                e.StationName = StationRepository.GetStationNameByScode(stationSCode);
            }

            //for (var i = 0; i < deliveryRateByDriverEntities.Count; i++)
            //{
            //    for (var j = 0; j < i; j++)
            //    {
            //        if (deliveryRateByDriverEntities[i].DriverCode == deliveryRateByDriverEntities[j].DriverCode)
            //        {
            //            deliveryRateByDriverEntities[j].MatingNumBeforeEighteen = deliveryRateByDriverEntities[i].MatingNumBeforeEighteen + deliveryRateByDriverEntities[j].MatingNumBeforeEighteen;
            //            deliveryRateByDriverEntities[j].MatingNumBeforeFifteen = deliveryRateByDriverEntities[i].MatingNumBeforeFifteen + deliveryRateByDriverEntities[j].MatingNumBeforeFifteen;
            //            deliveryRateByDriverEntities[j].MatingNumBeforeNoon = deliveryRateByDriverEntities[i].MatingNumBeforeNoon + deliveryRateByDriverEntities[j].MatingNumBeforeNoon;
            //            deliveryRateByDriverEntities[j].MatingNumBeforeTwenty = deliveryRateByDriverEntities[i].MatingNumBeforeTwenty + deliveryRateByDriverEntities[j].MatingNumBeforeTwenty;
            //            deliveryRateByDriverEntities[j].MatingNumber = deliveryRateByDriverEntities[i].MatingNumber + deliveryRateByDriverEntities[j].MatingNumber;
            //            deliveryRateByDriverEntities[j].OrderNumber = deliveryRateByDriverEntities[i].OrderNumber + deliveryRateByDriverEntities[j].OrderNumber;
            //            deliveryRateByDriverEntities[j].NotWriteOffNumber = deliveryRateByDriverEntities[i].NotWriteOffNumber + deliveryRateByDriverEntities[j].NotWriteOffNumber;
            //            deliveryRateByDriverEntities[j].DeliveryNumber = deliveryRateByDriverEntities[i].DeliveryNumber + deliveryRateByDriverEntities[j].DeliveryNumber;
            //            deliveryRateByDriverEntities.RemoveAt(i);
            //            i--;
            //            break;
            //        }
            //    }
            //}

            return deliveryRateByDriverEntities;
        }

        public DeliveryRateByDriverEntity ConvertToPercent(DeliveryRateByDriverEntity driverDeliveryRateEntity)
        {
            return new DeliveryRateByDriverEntity
            {
                StationName = driverDeliveryRateEntity.StationName,
                OrderNumber = driverDeliveryRateEntity.OrderNumber,
                MatingNumBeforeEighteen = driverDeliveryRateEntity.MatingNumBeforeEighteen / driverDeliveryRateEntity.OrderNumber,
                MatingNumBeforeFifteen = driverDeliveryRateEntity.MatingNumBeforeFifteen / driverDeliveryRateEntity.OrderNumber,
                MatingNumBeforeNoon = driverDeliveryRateEntity.MatingNumBeforeNoon / driverDeliveryRateEntity.OrderNumber,
                MatingNumBeforeTwenty = driverDeliveryRateEntity.MatingNumBeforeTwenty / driverDeliveryRateEntity.OrderNumber,
                MatingNumber = driverDeliveryRateEntity.MatingNumber / driverDeliveryRateEntity.OrderNumber,
                DeliveryNumber = driverDeliveryRateEntity.DeliveryNumber / driverDeliveryRateEntity.OrderNumber,
                NotWriteOffNumber = driverDeliveryRateEntity.NotWriteOffNumber / driverDeliveryRateEntity.OrderNumber,
                DriverCode = driverDeliveryRateEntity.DriverCode,
                DriverName = driverDeliveryRateEntity.DriverName
            };
        }
        #endregion

        public List<DeliveryDetailEntity> GetDeliveryDetailsByDriverCodeAndDeliveryDate(string driverCode, DateTime deliveryDateStart, DateTime deliveryDateEnd)
        {
            deliveryDateStart = deliveryDateStart.AddHours(5);
            deliveryDateEnd = deliveryDateEnd.AddHours(5);
            List<DeliveryDetailEntity> deliveryDetailEntities = DeliveryRequestJoinDeliveryScanLogRepository.GetDeliveryDetailByDriverCodeAndDeliveryDatePeriod(driverCode, deliveryDateStart, deliveryDateEnd).ToList();
            deliveryDetailEntities = ChangeScanItemToChinese(deliveryDetailEntities);

            var driver = DriverRepository.GetByDriverCode(driverCode);
            string driverName = driver.DriverName;

            // 改成寫司機所屬站所
            string stationName = StationRepository.GetStationNameByScode(driver.Station);

            foreach (var entity in deliveryDetailEntities)
            {
                entity.DriverName = driverName;
                entity.StationName = stationName;
            }

            return deliveryDetailEntities;
        }

        private List<DeliveryDetailEntity> ChangeScanItemToChinese(List<DeliveryDetailEntity> input)
        {
            for (var i = 0; i < input.Count; i++)
            {
                switch (input[i].ArriveOption)
                {
                    case "1":
                        input[i].ArriveOption = "客戶不在";
                        break;
                    case "2":
                        input[i].ArriveOption = "約定再配";
                        break;
                    case "3":
                        input[i].ArriveOption = "正常配交";
                        break;
                    case "4":
                        input[i].ArriveOption = "拒收";
                        break;
                    case "5":
                        input[i].ArriveOption = "地址錯誤";
                        break;
                    case "6":
                        input[i].ArriveOption = "查無此人";
                        break;

                }

            }

            return input;
        }
    }

}
