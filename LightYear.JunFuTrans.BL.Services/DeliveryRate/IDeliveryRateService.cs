﻿using LightYear.JunFuTrans.BL.BE.DeliveryRate;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.DeliveryRate
{
    public interface IDeliveryRateService
    {
        #region 下拉選單
        List<StationEntity> GetStations();

        /// <summary>
        /// 取得零擔客戶
        /// </summary>
        /// <returns></returns>
        List<CustomerEntity> GetLTLCustomers();
        #endregion

        DeliveryRateEntity GetTodaysDeliveryRateByStation(string stationScode);

        DeliveryRateEntity GetTodaysDeliveryRate();

        #region 第一層
        /// <summary>
        /// 站所全選, 客戶全選
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        List<DeliveryRateEntity> GetDeliveryRateEntitiesByTimePeriod(DateTime start, DateTime end);

        /// <summary>
        /// 選區域，區域內站所全選，客戶全選
        /// </summary>
        /// <param name="areaName"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        List<DeliveryRateEntity> GetDeliveryRateEntitiesByAreaAndTimePeriod(string areaName, DateTime start, DateTime end);

        /// <summary>
        /// 選站所, 客戶全選
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="stationScode"></param>
        /// <returns></returns>
        List<DeliveryRateEntity> GetDeliveryRateEntityByStationAndTimePeriod(DateTime start, DateTime end, string stationScode);

        /// <summary>
        /// 選客代, 站所全選
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="customerCode"></param>
        /// <returns></returns>
        List<DeliveryRateEntity> GetDeliveryRateEntityByCustomerAndTimePeriod(DateTime start, DateTime end, string customerCode);

        DeliveryRateEntity ConvertToPercent(DeliveryRateEntity deliveryRateEntity);
        #endregion

        List<DeliveryRateByDriverEntity> GetEntitiesByStationSCode(string stationSCode, DateTime start, DateTime end);

        DeliveryRateByDriverEntity ConvertToPercent(DeliveryRateByDriverEntity driverDeliveryRateEntity);

        List<DeliveryDetailEntity> GetDeliveryDetailsByDriverCodeAndDeliveryDate(string driverCode, DateTime deliveryDateStart, DateTime deliveryDateEnd);
    }
}
