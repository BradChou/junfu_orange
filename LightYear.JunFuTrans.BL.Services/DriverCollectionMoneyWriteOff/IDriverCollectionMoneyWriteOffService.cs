﻿using LightYear.JunFuTrans.BL.BE.DriverCollectionMoneyWriteOff;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.DriverCollectionMoneyWriteOff
{
    public interface IDriverCollectionMoneyWriteOffService
    {
        List<DriverCollectionMoneyWriteOffEntity> GetAllByStationAndDatetime(string scode, DateTime start, DateTime end);
    }
}
