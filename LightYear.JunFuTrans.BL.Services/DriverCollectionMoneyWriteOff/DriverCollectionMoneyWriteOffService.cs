﻿using AutoMapper;
using LightYear.JunFuTrans.BL.BE.DriverCollectionMoneyWriteOff;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.Repositories.Account;
using LightYear.JunFuTrans.DA.Repositories.DriverCollectionMoneyWriteOff;
using LightYear.JunFuTrans.DA.Repositories.Station;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.DriverCollectionMoneyWriteOff
{
    public class DriverCollectionMoneyWriteOffService : IDriverCollectionMoneyWriteOffService
    {
        public IDriverCollectionMoneyWriteOffRepository DriverCollectionMoneyWriteOffRepository { get; set; }

        public IStationRepository StationRepository { get; set; }

        public IDriverRepository DriverRepository { get; set; }

        public IMapper Mapper { get; set; }

        public DriverCollectionMoneyWriteOffService(
            IDriverCollectionMoneyWriteOffRepository driverCollectionMoneyWriteOffRepository,
            IStationRepository stationRepository,
            IDriverRepository driverRepository,
            IMapper mapper
            )
        {
            this.DriverCollectionMoneyWriteOffRepository = driverCollectionMoneyWriteOffRepository;
            this.StationRepository = stationRepository;
            this.DriverRepository = driverRepository;
            this.Mapper = mapper;
        }

        public List<DriverCollectionMoneyWriteOffEntity> GetAllByStationAndDatetime(string scode, DateTime start, DateTime end)
        {
            DateTime actualEndDate = end.AddDays(1);

            List<DriverCollectionMoneyWriteOffEntity> driverCollectionMoneyWriteOffEntities = new List<DriverCollectionMoneyWriteOffEntity>();

            if (scode.Equals("-1"))
            {
                driverCollectionMoneyWriteOffEntities = DriverCollectionMoneyWriteOffRepository.GetAllByDate(start, actualEndDate);
            }
            else
            {
                driverCollectionMoneyWriteOffEntities = DriverCollectionMoneyWriteOffRepository.GetAllByDateAndStation(scode, start, actualEndDate);
            }

             var stationScode = driverCollectionMoneyWriteOffEntities.Select(a => a.ArriveStationName).ToList();

             var name = StationRepository.GetTbStations(stationScode);

            foreach (DriverCollectionMoneyWriteOffEntity driverCollectionMoneyWriteOffEntity in driverCollectionMoneyWriteOffEntities)
            {
                for (int i = 0; i < stationScode.Count; i++)
                {       
                    if (stationScode[i]!=null)
                    {                  
                        if (stationScode[i].Length > 0)
                        {
                            TbStation s = name[stationScode[i]];

                            if (driverCollectionMoneyWriteOffEntity.ArriveStationName == s.StationScode)
                            {
                                driverCollectionMoneyWriteOffEntity.ArriveStationName = s.StationName;
                                break;
                            }

                        }
                    }
                }

                if ((string.Compare(driverCollectionMoneyWriteOffEntity.CheckNumber, "103008900500") > 0 
                    && string.Compare(driverCollectionMoneyWriteOffEntity.CheckNumber, "103008904999") < 0)
                    || ( string.Compare(driverCollectionMoneyWriteOffEntity.CheckNumber, "600000000000") > 0
                    && string.Compare(driverCollectionMoneyWriteOffEntity.CheckNumber, "600000099999") < 0
                    ))

                {
                   driverCollectionMoneyWriteOffEntity.ReceiverType = "預購袋";
                }
                else
                {
                   driverCollectionMoneyWriteOffEntity.ReceiverType = "一般件";
                }

                var driverName = DriverRepository.GetDriverNameByDriverCode(driverCollectionMoneyWriteOffEntity.DriverCode);

                driverCollectionMoneyWriteOffEntity.DriverCodeAndName = driverCollectionMoneyWriteOffEntity.DriverCode + driverName;

                bool answer = DriverCollectionMoneyWriteOffRepository.IsDriverPaymentSheetOrNot(driverCollectionMoneyWriteOffEntity.CheckNumber);

                if (answer == true)
                {
                    driverCollectionMoneyWriteOffEntity.IsDriverPaymentSheetYN = "Y";
                }
                else
                {
                    driverCollectionMoneyWriteOffEntity.IsDriverPaymentSheetYN = "N";
                }

                //switch (driverCollectionMoneyWriteOffEntity.ScanItem)
                //{
                //    case "1":
                //    driverCollectionMoneyWriteOffEntity.ScanItem = "到著";
                //    break;

                //    case "2":
                //    driverCollectionMoneyWriteOffEntity.ScanItem = "配送";
                //    break;

                //    case "3":
                //    driverCollectionMoneyWriteOffEntity.ScanItem = "配達";
                //    break;
                //}
                driverCollectionMoneyWriteOffEntity.ScanItem = "配達";

            }

            return driverCollectionMoneyWriteOffEntities;
        }
    }
}
