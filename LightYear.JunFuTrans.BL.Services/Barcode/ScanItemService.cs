﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.Barcode
{
    public class ScanItemService : IScanItemService
    {
        public string GetArriveOptionChinese(ArriveOption arriveOption)
        {
            switch (arriveOption)
            {
                case ArriveOption.NotAtHome:
                    return "客戶不在";
                case ArriveOption.MaybeTomorrow:
                    return "約定再配";
                case ArriveOption.DeliverySuccess:
                    return "正常配交";
                case ArriveOption.DeliveryDeny:
                    return "拒收";
                case ArriveOption.WrongAddress:
                    return "地址錯誤";
                case ArriveOption.WrongPerson:
                    return "查無此人";
                case ArriveOption.MissingOfPart:
                    return "缺件配達";
                case ArriveOption.Broken:
                    return "破損配達";
                case ArriveOption.LackOfPart:
                    return "短少配達";
                case ArriveOption.InvoiceKeep:
                    return "簽單留置";
                case ArriveOption.Null:
                    return "尚未配達";
                default:
                    return null;
            }
        }

        public string GetScanItemChinese(ScanItem scanItem)
        {
            switch (scanItem)
            {
                case ScanItem.PickUp:
                    return "集貨";
                case ScanItem.DropOff:
                    return "卸集";
                case ScanItem.Origin:
                    return "發送";
                case ScanItem.Distination:
                    return "到著";
                case ScanItem.Delivery:
                    return "配送";
                case ScanItem.DeliveryMating:
                    return "配達";
                default:
                    return null;
            }
        }

        public string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            if (fi == null)
                return null;

            DescriptionAttribute[] attributes = fi.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];

            if (attributes != null && attributes.Any())
            {
                return attributes.First().Description == "4號袋" ? "袋" : attributes.First().Description;
            }

            return value.ToString();
        }
    }

    /// <summary>
    /// 掃讀歷程
    /// </summary>
    public enum ScanItem
    {
        /// <summary>
        /// 集貨
        /// </summary>
        PickUp = 5,

        /// <summary>
        /// 卸集
        /// </summary>
        DropOff = 6,

        /// <summary>
        /// 發送
        /// </summary>
        Origin = 7,

        /// <summary>
        /// 到著
        /// </summary>
        Distination = 1,

        /// <summary>
        /// 配送
        /// </summary>
        Delivery = 2,

        /// <summary>
        /// 配達
        /// </summary>
        DeliveryMating = 3
    }


    /// <summary>
    /// 配達區分
    /// </summary>
    public enum ArriveOption
    {
        /// <summary>
        /// 客戶不在
        /// </summary>
        NotAtHome = 1,

        /// <summary>
        /// 約定再配
        /// </summary>
        MaybeTomorrow = 2,

        /// <summary>
        /// 正常配交
        /// </summary>
        DeliverySuccess = 3,

        /// <summary>
        /// 拒收
        /// </summary>
        DeliveryDeny = 4,

        /// <summary>
        /// 地址錯誤
        /// </summary>
        WrongAddress = 5,

        /// <summary>
        /// 查無此人
        /// </summary>
        WrongPerson = 6,

        /// <summary>
        /// 缺件配達
        /// </summary>
        MissingOfPart = 7,

        /// <summary>
        /// 破損配達
        /// </summary>
        Broken = 8,

        /// <summary>
        /// 短少配達
        /// </summary>
        LackOfPart = 9,

        /// <summary>
        /// 簽單留置
        /// </summary>
        InvoiceKeep = 0,

        /// <summary>
        /// 尚未配達
        /// </summary>
        Null = 99
    }
}
