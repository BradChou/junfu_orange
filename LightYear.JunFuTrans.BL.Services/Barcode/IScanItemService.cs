﻿using LightYear.JunFuTrans.BL.BE.Barcode;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.Barcode
{
    public interface IScanItemService
    {
        string GetScanItemChinese(ScanItem scanItem);

        string GetArriveOptionChinese(ArriveOption arriveOption);

        string GetEnumDescription(Enum value);
    }
}
