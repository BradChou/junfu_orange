﻿using AutoMapper;
using LightYear.JunFuTrans.DA.Repositories.ConstantData;
using LightYear.JunFuTrans.DA.JunFuDb;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.ConstantData
{
    public class RegionService : IRegionService
    {
        public IRegionRepository RegionRepository { get; set; }

        public RegionService(IRegionRepository regionRepository)
        {
            this.RegionRepository = regionRepository;
        }

        public List<TbPostCity> GetCityList()
        {
            List<TbPostCity> citys = this.RegionRepository.GetCityList();

            return citys;
        }

        public List<TbPostCityArea> GetDistrictList()
        {
            List<TbPostCityArea> districts = this.RegionRepository.GetDistrictList();

            return districts;
        }
    }
}
