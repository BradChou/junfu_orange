﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.SignForm;

namespace LightYear.JunFuTrans.BL.Services.SignFormImage
{
    public interface ISignFormImageService
    {
        IEnumerable<SignFormEntity> GetData(DateTime deliveryDateStart, DateTime deliveryDateEnd, string customerCode, string stationScode, int signFormExistType);
    }
}
