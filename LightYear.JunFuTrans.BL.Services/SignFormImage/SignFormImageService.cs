﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.Repositories.SignFormImage;
using System.Linq;
using LightYear.JunFuTrans.BL.BE.SignForm;

namespace LightYear.JunFuTrans.BL.Services.SignFormImage
{
    public class SignFormImageService : ISignFormImageService
    {
        public ISignFormImageRepository SignFormImageRepository;
        public SignFormImageService(ISignFormImageRepository signFormImageRepository)
        {
            SignFormImageRepository = signFormImageRepository;
        }
        public IEnumerable<SignFormEntity> GetData(DateTime deliveryDateStart, DateTime deliveryDateEnd, string customerCode, string stationScode, int signFormExistType)
        {
            if (customerCode == "")
            {
                return SignFormFilter(SignFormImageRepository.GetDataByDatePeriod(deliveryDateStart, deliveryDateEnd, stationScode), signFormExistType);
            }
            else if (stationScode == "")
            {
                return SignFormFilter(SignFormImageRepository.GetDataByCustomerCode(deliveryDateStart, deliveryDateEnd, customerCode), signFormExistType);
            }
            else
            {
                return SignFormFilter(SignFormImageRepository.GetDataByDatePeriodAndCustomerCode(deliveryDateStart, deliveryDateEnd, customerCode, stationScode), signFormExistType);
            }
        }

        private IEnumerable<SignFormEntity> SignFormFilter(IEnumerable<SignFormEntity> entities, int signFormExistType)
        {
            switch (signFormExistType)
            {
                case 0:
                    return entities.Where(x => !x.IsSignPaperExist);
                case 1:
                    return entities.Where(x => x.IsSignPaperExist);
                case 2:
                    return entities;
                default:
                    return entities.Where(x => x.IsSignPaperExist);
            }
        }
    }
}
