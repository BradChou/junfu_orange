﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections.Specialized;
using LightYear.JunFuTrans.BL.BE.Enumeration;

namespace LightYear.JunFuTrans.BL.Services.Reports
{
    public interface IReportPrintService
    {
        public Stream GetReport<TRequest>(TRequest request, string reportName);

        public Stream GetNomalReport(NameValueCollection reportParams, string reportName, ReportExportType exportType);

        public Stream GetNomalReport(Dictionary<string, string[]> reportParams, string reportName, ReportExportType exportType);

        public Stream GetNomalReport(NameValueCollection reportParams, string reportName, ReportExportType exportType, string imageType);

        public Stream GetNomalReport(Dictionary<string, string[]> reportParams, string reportName, ReportExportType exportType, string imageType);

        string GetReportTypeMimeString(ReportExportType exportType);

        string GetReportTypeMimeString(ReportExportType exportType, string imageType);
    }
}
