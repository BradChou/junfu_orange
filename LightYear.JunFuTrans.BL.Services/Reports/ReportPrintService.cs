﻿using LightYear.JunFuTrans.Utilities.Reports;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using LightYear.JunFuTrans.BL.BE.Enumeration;

namespace LightYear.JunFuTrans.BL.Services.Reports
{
    public class ReportPrintService : IReportPrintService
    {
        public IReportHelper ReportHelper { get; private set; }

        public ReportPrintService(IReportHelper reportHelper)
        {
            this.ReportHelper = reportHelper;
        }

        public Stream GetReport<TRequest>(TRequest request, string reportName)
        {
            var pdfStream = this.ReportHelper.GetReport(request, reportName);
            return pdfStream;
        }

        public Stream GetNomalReport(NameValueCollection reportParams, string reportName, ReportExportType exportType)
        {
            var pdfStream = this.ReportHelper.GetNomalReport(reportParams, reportName, exportType.ToString("G"));
            return pdfStream;
        }

        public Stream GetNomalReport(Dictionary<string, string[]> reportParams, string reportName, ReportExportType exportType)
        {
            var pdfStream = this.ReportHelper.GetNomalReport(reportParams, reportName, exportType.ToString("G"));
            return pdfStream;
        }

        public Stream GetNomalReport(NameValueCollection reportParams, string reportName, ReportExportType exportType , string imageType = "PNG")
        {
            var pdfStream = this.ReportHelper.GetNomalReport(reportParams, reportName, exportType.ToString("G"), imageType);
            return pdfStream;
        }

        public Stream GetNomalReport(Dictionary<string, string[]> reportParams, string reportName, ReportExportType exportType, string imageType = "PNG")
        {
            var pdfStream = this.ReportHelper.GetNomalReport(reportParams, reportName, exportType.ToString("G"), imageType);
            return pdfStream;
        }

        public string GetReportTypeMimeString(ReportExportType exportType, string imageType)
        {
            string output;

            switch(exportType)
            {
                case ReportExportType.CSV:
                    output = "text/csv";
                    break;
                case ReportExportType.EXCEL:
                    output = "application/vnd.ms-excel";
                    break;
                case ReportExportType.EXCELOPENXML:
                    output = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    break;
                case ReportExportType.HTML5:
                    output = "text/html";
                    break;
                case ReportExportType.IMAGE:
                    //output = "image/TIF";
                    output = string.Format("image/{0}", imageType);
                    break;
                case ReportExportType.PDF:
                    output = "application/pdf";
                    break;
                case ReportExportType.WORD:
                    output = "application/msword";
                    break;
                case ReportExportType.WORDOPENXML:
                    output = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                    break;
                case ReportExportType.XML:
                    output = "text/xml";
                    break;
                default:
                    output = "text/html";
                    break;
            }

            return output;
        }

        public string GetReportTypeMimeString(ReportExportType exportType)
        {
            return GetReportTypeMimeString(exportType, "PNG");
        }
    }
}
