﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using AutoMapper;
using LightYear.JunFuTrans.DA.Repositories.Account;
using LightYear.JunFuTrans.DA.Repositories.Station;
using System.Threading.Tasks;
using JunFuTrans.DA.JunFuTrans.DA;
using JunFuTrans.DA.JunFuTrans.condition;
using System.Text.RegularExpressions;
using LightYear.JunFuTrans.BL.BE.Enumeration;
using System.Transactions;
using LightYear.JunFuTrans.DA.Repositories.JunFuOrangeLogs;

namespace LightYear.JunFuTrans.BL.Services.TruckVendorPaymentAndMaintenance
{
    public class TruckVendorPaymentService : ITruckVendorPaymentService
    {
        public ITruckVendorPaymentRepository TruckVendorPaymentRepository { get; set; }
        public ITruckVendorMaintenanceRepository TruckVendorMaintenanceRepository { get; set; }
        public IAccountsRepository AccountsRepository { get; set; }
        public JunFuDbContext JunFuDbContext { get; set; }
        public IMapper Mapper { get; private set; }
        public IStationAreaRepository StationAreaRepository { get; set; }

        public ITbItemCodesRepository TbItemCodesRepository { get; set; }
        public ITbSuppliersRepository TbSuppliersRepository { get; set; }
        public ITruckDepartmentRepository TruckDepartmentRepository { get; set; }
        public ITruckVendorPaymentLogWenERPRepository TruckVendorPaymentLogWenERPRepository { get; set; }
        public ITruckVendorPaymentLogDSERPRepository TruckVendorPaymentLogDSERPRepository { get; set; }
        public IVW_TruckVendorPaymentLogWenERPＭergeDataRepository VWTruckLogRepository { get; set; }
        public IFeeTypeRelateRepository FeeTypeRelateRepository { get; set; }
        public ITruckCompanyRelateRepository TruckCompanyRelateRepository { get; set; }

        public IUserRoleMappingRepository UserRoleMappingRepository { get; set; }
        public IJunFuOrangeLogRepository JunFuOrangeLogRepository { get; set; }
        public IDSERPMergeDataSnapShotRepository DSERPMergeDataSnapShotRepository { get; set; }
        public IVW_TruckVendorPaymentLogDSERPＭergeDataRepository VWDSTruckLogRepository { get; set; }

        public TruckVendorPaymentService(ITruckVendorPaymentRepository truckVendorPaymentRepository, ITruckVendorMaintenanceRepository truckVendorMaintenanceRepository
            , JunFuDbContext junFuDbContext, IMapper mapper, IAccountsRepository accountsRepository, IStationAreaRepository station, ITbItemCodesRepository tbItemCodesRepository
            , ITbSuppliersRepository tbSuppliersRepository, ITruckDepartmentRepository truckDepartmentRepository, ITruckVendorPaymentLogWenERPRepository truckVendorPaymentLogWenERPRepository
            , ITruckVendorPaymentLogDSERPRepository truckVendorPaymentLogDSERPRepository, ITruckCompanyRelateRepository truckCompanyRelateRepository
            , IVW_TruckVendorPaymentLogWenERPＭergeDataRepository vwTruckLogRepository, IFeeTypeRelateRepository feeTypeRelateRepository, IUserRoleMappingRepository userRoleMappingRepository
            , IJunFuOrangeLogRepository junFuOrangeLogRepository, IDSERPMergeDataSnapShotRepository dSERPMergeDataSnapShotRepository
            , IVW_TruckVendorPaymentLogDSERPＭergeDataRepository vwDSTruckLogRepository)
        {
            TruckVendorPaymentRepository = truckVendorPaymentRepository;
            TruckVendorMaintenanceRepository = truckVendorMaintenanceRepository;
            JunFuDbContext = junFuDbContext;
            this.Mapper = mapper;
            AccountsRepository = accountsRepository;
            StationAreaRepository = station;
            TbItemCodesRepository = tbItemCodesRepository;
            TbSuppliersRepository = tbSuppliersRepository;
            TruckDepartmentRepository = truckDepartmentRepository;
            TruckVendorPaymentLogWenERPRepository = truckVendorPaymentLogWenERPRepository;
            TruckVendorPaymentLogDSERPRepository = truckVendorPaymentLogDSERPRepository;
            UserRoleMappingRepository = userRoleMappingRepository;
            VWTruckLogRepository = vwTruckLogRepository;
            FeeTypeRelateRepository = feeTypeRelateRepository;
            TruckCompanyRelateRepository = truckCompanyRelateRepository;
            JunFuOrangeLogRepository = junFuOrangeLogRepository;
            DSERPMergeDataSnapShotRepository = dSERPMergeDataSnapShotRepository;
            VWDSTruckLogRepository = vwDSTruckLogRepository;
        }

        public List<TruckVendorPaymentLogFrontEndShowEntity> GetAllByMonthAndTruckVendor(DateTime start, DateTime end, string vendor)
        {
            var feetypes = TruckVendorPaymentRepository.GetFeeTypes();
            var companies = TruckVendorPaymentRepository.GetCompanies();
            //var vendornames = TruckVendorPaymentRepository.GetTruckVendors();
            var vendors = GetSupplier();
            var vendornames = vendors.Select(p => p.vendorName);

            List<TruckVendorPaymentLog> data = TruckVendorPaymentRepository.GetAllByMonthAndTruckVendor(start, end, vendor);
            data = data.Where(p => (bool)p.IsActive == true).ToList();
            //var result = new List<TruckVendorPaymentLogFrontEndShowEntity>();
            var result = this.Mapper.Map<List<TruckVendorPaymentLogFrontEndShowEntity>>(data);
            foreach (var item in result)
            {
                item.Cost = 0;
            }
            data = TruckVendorPaymentRepository.GetAllByMonthAndTruckVendorCostData(start, end, vendor);
            data = data.Where(p => (bool)p.IsActive == true).ToList();
            //var result = new List<TruckVendorPaymentLogFrontEndShowEntity>();
            var costResult = this.Mapper.Map<List<TruckVendorPaymentLogFrontEndShowEntity>>(data);
            foreach (var item in costResult)
            {
                item.Payment = 0;
                item.VendorName = item.Payable;
            }
            result.AddRange(costResult);
            result = ConvertCodeIdToName(result);
            //foreach (var d in data)
            //{
            //    foreach (var f in feetypes)
            //    {
            //        if (d.FeeType == f.Id)
            //        {
            //            d.FeeType = f.Name;
            //            break;
            //        }
            //    }

            //    foreach (var c in companies)
            //    {
            //        if (d.Company == c.Id)
            //        {
            //            d.Company = c.Name;
            //            break;
            //        }
            //    }

            //    foreach (var v in vendors)
            //    {
            //        if (d.VendorName == v.supplier_id.ToString())
            //        {
            //            d.VendorName = v.vendorName;
            //            break;
            //        }
            //    }
            //}
            return result;
        }

        public List<TruckVendorPaymentLogFrontEndShowEntity> GetAllByMonthAndTruckVendor_Old(DateTime start, DateTime end, string vendor)
        {
            var feetypes = TruckVendorPaymentRepository.GetFeeTypes();
            var companies = TruckVendorPaymentRepository.GetCompanies();
            var vendornames = TruckVendorPaymentRepository.GetTruckVendors();
            //var vendors = GetSupplier();
            //var vendornames = vendors.Select(p => p.vendorName);

            List<TruckVendorPaymentLog> data = TruckVendorPaymentRepository.GetAllByMonthAndTruckVendor_Old(start, end, vendor);
            data = data.Where(p => (bool)p.IsActive == true).ToList();
            //var result = new List<TruckVendorPaymentLogFrontEndShowEntity>();
            var result = this.Mapper.Map<List<TruckVendorPaymentLogFrontEndShowEntity>>(data);

            result = ConvertCodeIdToName_Old(result);
            //foreach (var d in data)
            //{
            //    foreach (var f in feetypes)
            //    {
            //        if (d.FeeType == f.Id)
            //        {
            //            d.FeeType = f.Name;
            //            break;
            //        }
            //    }

            //    foreach (var c in companies)
            //    {
            //        if (d.Company == c.Id)
            //        {
            //            d.Company = c.Name;
            //            break;
            //        }
            //    }

            //    foreach (var v in vendors)
            //    {
            //        if (d.VendorName == v.supplier_id.ToString())
            //        {
            //            d.VendorName = v.vendorName;
            //            break;
            //        }
            //    }
            //}
            return result;
        }

        public List<DisbursementEntity> GetDisbursementByMonthAndTruckVendor(DateTime start, DateTime end, string vendor)
        {
            List<DropDownListEntity> feetypes = TruckVendorPaymentRepository.GetFeeTypes();
            List<DisbursementEntity> disbursementEntities = TruckVendorPaymentRepository.GetDisbursementByMonthAndTruckVendor(start, end, vendor);

            foreach (var d in disbursementEntities)
            {
                d.FeeType = feetypes.Where(a => a.Id == d.FeeType).FirstOrDefault()?.Name;
            }
            return disbursementEntities;
        }
        public List<DisbursementEntity> GetDisbursementByMonthAndTruckVendor_Old(DateTime start, DateTime end, string vendor)
        {
            List<DropDownListEntity> feetypes = TruckVendorPaymentRepository.GetFeeTypes();
            List<DisbursementEntity> disbursementEntities = TruckVendorPaymentRepository.GetDisbursementByMonthAndTruckVendor_Old(start, end, vendor);

            foreach (var d in disbursementEntities)
            {
                d.FeeType = feetypes.Where(a => a.Id == d.FeeType).FirstOrDefault()?.Name;
            }
            return disbursementEntities;
        }

        public PaymentEntity GetIncomeAndExpenditure(DateTime start, DateTime end, string vendor)
        {
            return TruckVendorPaymentRepository.GetIncomeAndExpenditure(start, end, vendor);
        }
        public PaymentEntity GetIncomeAndExpenditure_Old(DateTime start, DateTime end, string vendor)
        {
            return TruckVendorPaymentRepository.GetIncomeAndExpenditure_Old(start, end, vendor);
        }

        public List<int> Save(List<TruckVendorPaymentLog> truckVendorPaymentLogs)
        {
            var feetypes = GetFeeTypes();
            var companies = GetCompanies();
            var vendornames = GetTruckVendors();
            List<int> ids = new List<int>();

            if (truckVendorPaymentLogs[0].Id == 0 && truckVendorPaymentLogs[0].ImportRandomCode != null) //整批匯入
            {
                foreach (var truckVendorPaymentLog in truckVendorPaymentLogs)
                {
                    truckVendorPaymentLog.FeeType = feetypes.Where(a => a.Name.Equals(truckVendorPaymentLog.FeeType)).FirstOrDefault()?.Id;
                    truckVendorPaymentLog.Company = companies.Where(a => a.Name.Equals(truckVendorPaymentLog.Company)).FirstOrDefault()?.Id;
                    //truckVendorPaymentLog.VendorName = vendornames.Where(a => a.VendorName.Equals(truckVendorPaymentLog.VendorName)).FirstOrDefault()?.Id.ToString();
                    truckVendorPaymentLog.CreateDate = DateTime.Now;
                }
                ids = TruckVendorPaymentRepository.InsertBatch(truckVendorPaymentLogs);

                return ids;
            }

            var forInsert = truckVendorPaymentLogs.Where(a => a.Id == 0 && a.ImportRandomCode == null).ToList(); // 多筆新增

            if (forInsert.Count > 0)
            {
                foreach (var truckVendorPaymentLog in forInsert)
                {
                    truckVendorPaymentLog.FeeType = feetypes.Where(a => a.Name.Equals(truckVendorPaymentLog.FeeType)).FirstOrDefault()?.Id;
                    truckVendorPaymentLog.Company = companies.Where(a => a.Name.Equals(truckVendorPaymentLog.Company)).FirstOrDefault()?.Id;
                    truckVendorPaymentLog.VendorName = vendornames.Where(a => a.VendorName.Equals(truckVendorPaymentLog.VendorName)).FirstOrDefault()?.Id.ToString();
                    truckVendorPaymentLog.ImportRandomCode = "";
                    truckVendorPaymentLog.RegisteredDate = truckVendorPaymentLog.RegisteredDate.Value.AddHours(8); //kendo grid新增或編輯時間要加8小時，因為台灣在UTC+8
                    truckVendorPaymentLog.CreateDate = DateTime.Now;
                }
                ids = TruckVendorPaymentRepository.InsertBatch(forInsert);
            }

            return ids;
        }
        #region 轉換成文中ERP表單
        void TransferLogToWenERP(List<TruckVendorPaymentLog> truckVendorPaymentLogs)
        {
            var insertWenERPLogs = new List<TruckVendorPaymentLogWenERP>();
            var itemList = TruckVendorPaymentRepository.GetFeeTypeTbItemCodes();
            try
            {
                foreach (var truckVendorPaymentLog in truckVendorPaymentLogs)
                {
                    if (truckVendorPaymentLog.Payment == 0)
                    {
                        continue;
                    }
                    var insertWenERPLog = new TruckVendorPaymentLogWenERP();
                    var itemType = itemList.FirstOrDefault(p => p.CodeId == truckVendorPaymentLog.FeeType).Memo;
                    int supplierID = 0;
                    int.TryParse(truckVendorPaymentLog.VendorName, out supplierID);
                    var supplier = TbSuppliersRepository.GetSupplierById(supplierID);
                    insertWenERPLog.SupplierID = supplierID;
                    //隱含轉換
                    insertWenERPLog = truckVendorPaymentLog;
                    //暫時先不用
                    insertWenERPLog.SerialNumber = "";
                    //id 1對1
                    insertWenERPLog.TruckVendorPaymentLogId = truckVendorPaymentLog.Id;
                    //單別 支出項目寫 20 (銷貨)，收入項目寫 10 (進貨)
                    //產品代號 支出項目寫 ZAA0030002，收入項目抓 ZAA0010001
                    //傳票類別 支出項目寫 20-3 (銷貨)，收入項目寫 10 (進貨)
                    if (itemType.Contains("支出"))
                    {
                        insertWenERPLog.OrderType = "20";
                        insertWenERPLog.ProductCode = "ZAA0030002";
                        insertWenERPLog.VoucherType = "20-3";
                    }
                    else if (itemType.Contains("收入"))
                    {
                        insertWenERPLog.OrderType = "10";
                        insertWenERPLog.ProductCode = "ZAA0010001";
                        insertWenERPLog.VoucherType = "10";
                    }
                    else
                    {
                        insertWenERPLog.OrderType = "0";
                        insertWenERPLog.ProductCode = "0";
                        insertWenERPLog.VoucherType = "0";
                    }
                    //抓統編或身分證字號
                    if (supplier != null)
                    {
                        if (!string.IsNullOrEmpty(supplier.UniformNumbers))
                        {
                            insertWenERPLog.SupplierUID = supplier.UniformNumbers;
                            insertWenERPLog.CustomerUID = supplier.UniformNumbers;
                        }
                        else if (!string.IsNullOrEmpty(supplier.IdNo))
                        {
                            insertWenERPLog.SupplierUID = supplier.IdNo;
                            insertWenERPLog.CustomerUID = supplier.IdNo;
                        }
                    }
                    //序號 一律寫 001
                    insertWenERPLog.Number = "001";
                    //稅別 法人寫 3 (三聯式發票)，自然人預設寫 6 (不開發票)
                    //未稅單價	電子發票註記 當稅別欄位值 = 2, 3 就寫 Y (開電子發票)，6 就寫 N (不開發票)
                    if (Regex.IsMatch(insertWenERPLog.SupplierUID, @"^[0-9]{8}$"))
                    {
                        insertWenERPLog.TaxType = "3";
                        insertWenERPLog.ElecInovoice = "Y";
                        insertWenERPLog.PaperInovoice = "Y";
                    }
                    else if (Regex.IsMatch(insertWenERPLog.SupplierUID, @"^[A-Za-z]{1}[1-2]{1}[0-9]{8}$"))
                    {
                        insertWenERPLog.TaxType = "6";
                        insertWenERPLog.ElecInovoice = "N";
                        insertWenERPLog.PaperInovoice = "N";
                    }
                    else
                    {
                        insertWenERPLog.TaxType = "0";
                        insertWenERPLog.ElecInovoice = "N";
                        insertWenERPLog.PaperInovoice = "N";
                    }
                    //單價含稅否 一律寫 0 (不含稅)
                    insertWenERPLog.TaxInclude = false;
                    //數量 一律寫 1
                    insertWenERPLog.Quantity = 1;
                    //SerialNumber 先給0
                    //var serialNum = TruckVendorPaymentLogWenERPRepository.GetSerialNumber(insertWenERPLog.RegisteredDate);
                    //var serialNumFormat = "00000";
                    //insertWenERPLog.SerialNumber = GetSerialNumber(insertWenERPLog.RegisteredDate);
                    //insertWenERPLog.InvoicingNumber = insertWenERPLog.RegisteredDate.Year.ToString() + insertWenERPLog.RegisteredDate.Month.ToString("00") + insertWenERPLog.SerialNumber;

                    //SerialNumber目前用Trigger在抓取 先給一個0值
                    insertWenERPLog.SerialNumber = "00000";
                    insertWenERPLog.InvoicingNumber = "00000";
                    insertWenERPLogs.Add(insertWenERPLog);
                    //TruckVendorPaymentLogWenERPRepository.Insert(insertWenERPLog);
                }

            }
            catch (Exception)
            {
                throw;
            }
            TruckVendorPaymentLogWenERPRepository.InsertBatch(insertWenERPLogs);
            //WenERPSerialNumberProcess(insertWenERPLogs);
        }

        #region SerialNumber
        string GetSerialNumber(DateTime registeredDate)
        {
            var serialNumFormat = "00000";
            var serialNum = TruckVendorPaymentLogWenERPRepository.GetSerialNumber(registeredDate);
            return (serialNum + 1).ToString(serialNumFormat);
        }
        /// <summary>
        /// SerialNumber回填
        /// </summary>
        /// <param name="logs"></param>
        void WenERPSerialNumberProcess(List<TruckVendorPaymentLogWenERP> logs)
        {
            var serialNumFormat = "00000";
            foreach (var log in logs)
            {
                var serialNum = TruckVendorPaymentLogWenERPRepository.GetSerialNumber(log.RegisteredDate);
                log.SerialNumber = (serialNum + 1).ToString(serialNumFormat);
                log.InvoicingNumber = log.RegisteredDate.Year.ToString() + log.RegisteredDate.Month.ToString("00") + log.SerialNumber;
                TruckVendorPaymentLogWenERPRepository.Update(log);
            }
        }
        #endregion

        #endregion

        #region 轉換成鼎新ERP表單
        void TransferLogToDSERP(List<TruckVendorPaymentLog> truckVendorPaymentLogs)
        {
            var insertDSERPLogs = new List<TruckVendorPaymentLogDSERP>();
            var itemList = TruckVendorPaymentRepository.GetFeeTypeTbItemCodes();
            var feetypeRelateList = FeeTypeRelateRepository.GetPublicAll();
            var truckRelateList = TruckCompanyRelateRepository.GetPublicAll();
            var supplierList = GetSupplier();
            var companies = GetCompanies();
            try
            {
                foreach (var truckVendorPaymentLog in truckVendorPaymentLogs)
                {
                    var insertDSERPLog = new TruckVendorPaymentLogDSERP();
                    //隱含轉換
                    insertDSERPLog = truckVendorPaymentLog;
                    //id 1對1
                    insertDSERPLog.TruckVendorPaymentLogId = truckVendorPaymentLog.Id;
                    insertDSERPLog.UploadDate = (DateTime)truckVendorPaymentLog.CreateDate;
                    insertDSERPLog.RegisteredDate = (DateTime)truckVendorPaymentLog.RegisteredDate;
                    insertDSERPLog.FeeType = truckVendorPaymentLog.FeeType;
                    var company = companies.FirstOrDefault(p => p.Id == truckVendorPaymentLog.Company);
                    var pay = (int)truckVendorPaymentLog.Payment;
                    var cost = truckVendorPaymentLog.Cost;
                    var accounts = 0;
                    var collectionAccounts = 0;
                    var spread = truckVendorPaymentLog.Spread;
                    var itemCode = 0;
                    int.TryParse(truckVendorPaymentLog.FeeType, out itemCode);
                    var feetypeRelate = feetypeRelateList.FirstOrDefault(p => p.CodeId == itemCode);
                    var companyItemCode = 0;
                    int.TryParse(truckVendorPaymentLog.Company, out companyItemCode);
                    var truckRelate = truckRelateList.FirstOrDefault(p => p.CodeId == companyItemCode);
                    //如果公司別(車行)是峻富則固定寫 JUNFU
                    //峻富以外的車行則固定寫 CH
                    if (company.Name == "峻富")
                    {
                        insertDSERPLog.Company = "JF";
                    }
                    else if (company.Name == "金來")
                    {
                        insertDSERPLog.Company = "GL";
                    }
                    else
                    {
                        insertDSERPLog.Company = "CH";
                    }
                    insertDSERPLog.CompanyUID = truckRelate.CompanyUID;
                    //當明細的收入與支出皆 > 0 /*則應付憑單單別*/ = 7120             應付對象=>供應商 應收對象=>車主
                    //當明細的收入 > 0 且支出 = 0，則抓取交易項目關聯的應收憑單單別    應付對象=>車主   應收對象=>供應商
                    //當明細的收入 = 0 且支出 > 0，則抓取交易項目關聯的應付憑單單別    應付對象=>供應商  應收對象=>車主
                    //當明細的收入> 0 且支出 > 0且價差 >= 0，則代付碼 = Y
                    //當明細的收入 = 0或支出 = 0，則代付碼 = N
                    int vendorID = 0;
                    int.TryParse(truckVendorPaymentLog.VendorName, out vendorID);
                    int payableID = 0;
                    int.TryParse(truckVendorPaymentLog.Payable, out payableID);
                    var supplier = new VW_tbSupplier();
                    var customer = new VW_tbSupplier();
                    var isPayLog = false;
                    if (pay > 0 && cost > 0 && spread >= 0)
                    {
                        isPayLog = true;
                        insertDSERPLog.VoucherCode = "7120";
                        insertDSERPLog.CollectionPayCode = "Y";
                        accounts = cost;
                        collectionAccounts = pay;
                        supplier = supplierList.FirstOrDefault(p => p.supplier_id == payableID);
                        customer = supplierList.FirstOrDefault(p => p.supplier_id == vendorID);
                    }
                    else if (pay > 0 && cost == 0)
                    {
                        insertDSERPLog.VoucherCode = feetypeRelate.ReceivableCode;
                        insertDSERPLog.CollectionPayCode = "N";
                        accounts = pay;
                        supplier = supplierList.FirstOrDefault(p => p.supplier_id == vendorID);
                        customer = supplierList.FirstOrDefault(p => p.supplier_id == payableID);
                    }
                    else if (pay == 0 && cost > 0)
                    {
                        isPayLog = true;
                        insertDSERPLog.VoucherCode = feetypeRelate.PayableCode;
                        insertDSERPLog.CollectionPayCode = "N";
                        accounts = cost;
                        supplier = supplierList.FirstOrDefault(p => p.supplier_id == payableID);
                        customer = supplierList.FirstOrDefault(p => p.supplier_id == vendorID);
                    }
                    if (supplier != null)
                    {
                        insertDSERPLog.SupplierUID = supplier.IDNO;
                        var hasHead = supplier.headquartersType == null ? false : (bool)supplier.headquartersType;
                        if (hasHead && (supplier.headquarters != null))
                        {
                            if (isPayLog)
                            {
                                if (supplier.headquarters.Length >= 8)
                                {
                                    if (Regex.IsMatch(supplier.headquarters.Substring(0, 8), @"^[0-9]{8}$"))
                                    {
                                        insertDSERPLog.SupplierUID = supplier.headquarters.Substring(0, 8);
                                    }
                                }
                            }
                        }
                    }
                    if (customer != null)
                    {
                        insertDSERPLog.CustomerUID = customer.IDNO;
                    }
                    insertDSERPLog.DinShinItemCode = feetypeRelate.DinShinItemCode;
                    insertDSERPLog.Accounts = accounts;
                    insertDSERPLog.CollectionAccounts = collectionAccounts;
                    insertDSERPLog.Spread = spread;
                    insertDSERPLogs.Add(insertDSERPLog);
                }

            }
            catch (Exception)
            {
                throw;
            }
            TruckVendorPaymentLogDSERPRepository.InsertBatch(insertDSERPLogs);
        }
        #endregion



        public void Update(TruckVendorPaymentLog truckVendorPaymentLog)
        {
            var updateLog = TruckVendorPaymentRepository.GetById(truckVendorPaymentLog.Id);
            var feetypes = GetFeeTypes();
            var companies = GetCompanies();
            //var vendornames = GetTruckVendors();
            var vendornames = GetSupplier();
            truckVendorPaymentLog.FeeType = feetypes.Where(a => a.Name.Equals(truckVendorPaymentLog.FeeType)).FirstOrDefault()?.Id;
            truckVendorPaymentLog.Company = companies.Where(a => a.Name.Equals(truckVendorPaymentLog.Company)).FirstOrDefault()?.Id;
            updateLog.VendorName = vendornames.Where(a => a.vendorName.Equals(truckVendorPaymentLog.VendorName)).FirstOrDefault()?.supplier_id.ToString();
            updateLog.UpdateDate = DateTime.Now;
            updateLog.UpdateUser = truckVendorPaymentLog.UpdateUser;
            TruckVendorPaymentRepository.Update(updateLog);
        }

        public void UpdateVendorLog(TruckVendorPaymentLog truckVendorPaymentLog)
        {
            var updateLog = TruckVendorPaymentRepository.GetById(truckVendorPaymentLog.Id);
            var feetypes = GetFeeTypes();
            var companies = GetCompanies();
            //var vendornames = GetTruckVendors();
            var vendornames = GetSupplier();
            updateLog.FeeType = feetypes.Where(a => a.Name.Equals(truckVendorPaymentLog.FeeType)).FirstOrDefault()?.Id;
            updateLog.Company = companies.Where(a => a.Name.Equals(truckVendorPaymentLog.Company)).FirstOrDefault()?.Id;
            //updateLog.VendorName = vendornames.Where(a => a.VendorName.Equals(truckVendorPaymentLog.VendorName)).FirstOrDefault()?.Id.ToString();
            updateLog.VendorName = vendornames.Where(a => a.vendorName.Equals(truckVendorPaymentLog.VendorName)).FirstOrDefault()?.supplier_id.ToString();
            updateLog.Payable = vendornames.Where(a => a.vendorName.Equals(truckVendorPaymentLog.Payable)).FirstOrDefault()?.supplier_id.ToString(); updateLog.Department = truckVendorPaymentLog.Department;
            updateLog.UpdateDate = DateTime.Now;
            updateLog.UpdateUser = truckVendorPaymentLog.UpdateUser;
            updateLog.RegisteredDate = truckVendorPaymentLog.RegisteredDate;
            updateLog.CarLicense = truckVendorPaymentLog.CarLicense;
            updateLog.Payment = truckVendorPaymentLog.Payment == null ? 0 : truckVendorPaymentLog.Payment;
            updateLog.Cost = truckVendorPaymentLog.Cost;
            updateLog.InvoiceDate = truckVendorPaymentLog.InvoiceDate;
            updateLog.InvoiceNumber = truckVendorPaymentLog.InvoiceNumber;
            updateLog.Memo = truckVendorPaymentLog.Memo;

            if (string.IsNullOrEmpty(updateLog.Payable))
            {
                updateLog.Payable = "";
            }
            if (string.IsNullOrEmpty(updateLog.VendorName))
            {
                updateLog.VendorName = "";
            }
            if (string.IsNullOrEmpty(updateLog.InvoiceNumber))
            {
                updateLog.InvoiceNumber = "";
            }
            if (updateLog.Payment > 0 && updateLog.Cost > 0)
            {
                updateLog.Spread = (int)updateLog.Payment - updateLog.Cost;
            }

            TruckVendorPaymentRepository.UpdateVendorPayment(updateLog);
        }
        //public void Delete(TruckVendorPaymentLogFrontEndShowEntity entity)
        public void Delete(TruckVendorPaymentLog truckVendorPaymentLog, string user)
        {
            //TruckVendorPaymentLog truckVendorPaymentLog = Mapper.Map<TruckVendorPaymentLog>(entity);
            //TruckVendorPaymentRepository.Delete(truckVendorPaymentLog);

            var updateLog = TruckVendorPaymentRepository.GetById(truckVendorPaymentLog.Id);
            //var updateWenERPLog = TruckVendorPaymentLogWenERPRepository.GetByTruckVendorPaymentLogId(truckVendorPaymentLog.Id);
            updateLog.UpdateDate = DateTime.Now;
            updateLog.UpdateUser = truckVendorPaymentLog.UpdateUser;
            updateLog.IsActive = false;
            TruckVendorPaymentRepository.UpdateVendorPayment(updateLog);
            //if (updateWenERPLog != null)
            //{
            //    updateWenERPLog.UpdateDate = DateTime.Now;
            //    updateWenERPLog.UpdateUser = truckVendorPaymentLog.UpdateUser;
            //    updateWenERPLog.IsActive = false;
            //    TruckVendorPaymentLogWenERPRepository.Update(updateWenERPLog);
            //}
            var deleteDSLog = TruckVendorPaymentLogDSERPRepository.GetByTruckVendorLogId(updateLog.Id);
            if (deleteDSLog != null)
            {
                deleteDSLog.IsActive = false;
                deleteDSLog.UpdateUser = user;
                deleteDSLog.UpdateDate = DateTime.Now;
                TruckVendorPaymentLogDSERPRepository.Delete(deleteDSLog);
            }
        }

        public void DeleteBatch(int[] ids, string user)
        {
            //TruckVendorPaymentLog truckVendorPaymentLog = Mapper.Map<TruckVendorPaymentLog>(entity);
            //TruckVendorPaymentRepository.Delete(truckVendorPaymentLog);
            var deleteLogs = TruckVendorPaymentRepository.GetByIds(ids);
            foreach (var log in deleteLogs)
            {
                log.IsActive = false;
                log.UpdateUser = user;
                log.UpdateDate = DateTime.Now;
            }
            TruckVendorPaymentRepository.DeleteBatch(deleteLogs);

            var deleteWenLogs = TruckVendorPaymentLogWenERPRepository.GetByTruckVendorLogIds(ids);
            if (deleteWenLogs.Count > 0)
            {
                foreach (var log in deleteWenLogs)
                {
                    log.IsActive = false;
                    log.UpdateUser = user;
                    log.UpdateDate = DateTime.Now;
                }
                TruckVendorPaymentLogWenERPRepository.DeleteBatch(deleteWenLogs);
            }
            var deleteDSLogs = TruckVendorPaymentLogDSERPRepository.GetByTruckVendorLogIds(ids);
            if (deleteDSLogs.Count > 0)
            {
                foreach (var log in deleteDSLogs)
                {
                    log.IsActive = false;
                    log.UpdateUser = user;
                    log.UpdateDate = DateTime.Now;
                }
                TruckVendorPaymentLogDSERPRepository.DeleteBatch(deleteDSLogs);
            }
        }
        public byte[] Export()
        {
            var memoryStream = new MemoryStream();
            using (var document = SpreadsheetDocument.Create(memoryStream, SpreadsheetDocumentType.Workbook))
            {
                var workbookPart = document.AddWorkbookPart();
                workbookPart.Workbook = new Workbook();

                var worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                worksheetPart.Worksheet = new Worksheet(new SheetData());

                var sheets = workbookPart.Workbook.AppendChild(new Sheets());
                sheets.Append(new Sheet()
                {
                    Id = workbookPart.GetIdOfPart(worksheetPart),
                    SheetId = 1,
                    Name = "工作表1"
                });

                // 從 Worksheet 取得要編輯的 SheetData
                var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

                // 建立資料列物件
                var row = new Row();
                // 在資料列中插入欄位
                row.Append(
                    new Cell() { CellValue = new CellValue("發生日期"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("交易項目"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("供應商名稱"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("車號"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("金額"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("摘要"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("公司別"), DataType = CellValues.String }
                );
                // 插入資料列 
                sheetData.AppendChild(row);

                List<string> FeeTypesName = GetFeeTypes().Select(a => a.Name).ToList();
                string DropDownFeeTypeName = string.Join(",", FeeTypesName);

                //List<string> TruckVendorsName = GetTruckVendors().Select(a => a.VendorName).ToList();
                //string DropDownTruckVendorsName = string.Join(",", TruckVendorsName);

                List<string> CompaniesName = GetCompanies().Select(a => a.Name).ToList();
                string DropDownCompaniesName = string.Join(",", CompaniesName);

                DataValidations dataValidations = new DataValidations();

                //交易項目下拉選單
                DataValidation dataValidation = new DataValidation()
                {
                    Type = DataValidationValues.List,
                    AllowBlank = true,
                    SequenceOfReferences = new ListValue<StringValue>() { InnerText = "B:B" }
                };
                Formula1 formula = new Formula1();
                formula.Text = "\"" + DropDownFeeTypeName + "\"";

                //公司別下拉選單
                DataValidation dataValidation1 = new DataValidation()
                {
                    Type = DataValidationValues.List,
                    AllowBlank = true,
                    SequenceOfReferences = new ListValue<StringValue>() { InnerText = "G:G" }
                };
                Formula1 formula1 = new Formula1();
                formula1.Text = "\"" + DropDownCompaniesName + "\"";

                dataValidation.Append(formula);
                dataValidation1.Append(formula1);
                dataValidations.Append(dataValidation);
                dataValidations.Append(dataValidation1);

                worksheetPart.Worksheet.AppendChild(dataValidations);

                workbookPart.Workbook.Save();
                document.Close();
            }
            return memoryStream.ToArray();
        }

        public List<TruckVendorPaymentLog> GetLatestUploadData(string ImportRandomCode = "")
        {
            var feetypes = TruckVendorPaymentRepository.GetFeeTypes();
            var companies = TruckVendorPaymentRepository.GetCompanies();
            //var vendornames = TruckVendorPaymentRepository.GetTruckVendors();
            var vendornames = TbSuppliersRepository.GetPublicAll();
            var departments = TruckDepartmentRepository.GetPublicAll();

            List<TruckVendorPaymentLog> data = TruckVendorPaymentRepository.GetLatestUploadData(ImportRandomCode);

            int i = 1;
            foreach (var d in data)
            {
                foreach (var f in feetypes)
                {
                    if (d.FeeType == f.Id)
                    {
                        d.FeeType = f.Name;
                        break;
                    }
                }

                foreach (var c in companies)
                {
                    if (d.Company == c.Id)
                    {
                        d.Company = c.Name;
                        break;
                    }
                }

                foreach (var v in vendornames)
                {
                    if (d.VendorName == v.SupplierId.ToString())
                    {
                        d.VendorName = v.SupplierName;
                        break;
                    }
                }

                foreach (var department in departments)
                {
                    if (d.Department == department.DepartmentCode.ToString())
                    {
                        d.Department = department.DepartmentName;
                        break;
                    }
                }

                //foreach (var v in vendornames)
                //{
                //    if (d.VendorName == v.Id.ToString())
                //    {
                //        d.VendorName = v.VendorName;
                //        break;
                //    }
                //}

                d.Id = i++;
            }
            return data;
        }

        public List<TbAccount> GetSuppliersAccount()
        {
            return AccountsRepository.GetSuppliersAccount();
        }

        public TbAccount GetByAccount(string account)
        {
            return AccountsRepository.GetByAccountCode(account);
        }

        public TruckVendor GetTruckVendorByTruckVendorAccountCode(string accountCode)
        {
            return TruckVendorPaymentRepository.GetTruckVendorByTruckVendorAccountCode(accountCode);
        }

        #region 供應商
        public List<TruckVendor> GetTruckVendorsForMaintenance()
        {
            return TruckVendorMaintenanceRepository.GetTruckVendorsForMaintenance();
        }

        public void SaveTruckVendor(TruckVendor truckVendor)
        {
            truckVendor.VendorName = truckVendor.VendorName.Trim();

            if (truckVendor.Id == 0)
            {
                truckVendor.CreateDate = DateTime.Now;
                TruckVendorMaintenanceRepository.Insert(truckVendor);
            }
            else
            {
                truckVendor.UpdateDate = DateTime.Now;
                TruckVendorMaintenanceRepository.Update(truckVendor);
            }
        }
        #endregion

        #region 公司別

        public List<TbItemCode> GetCompanyTbItemCodes()
        {
            return TruckVendorMaintenanceRepository.GetCompanyTbItemCodes();
        }

        #endregion

        #region 交易項目

        public List<FrontEndShowEntity> GetIncomeFeeType()
        {
            List<TbItemCode> incomeFeeItemCodeList = TruckVendorMaintenanceRepository.GetIncomeFeeTypeTbItemCodes();
            List<TbItemMappingCode> incomeFeeMappingJFDeptList = TruckVendorMaintenanceRepository.GetJFDeptFeeTypeMappingTbItemCodes();
            List<TbItemCode> JFDeptList = TruckVendorMaintenanceRepository.GetJFDepartment();

            var data = from incomeFee in incomeFeeItemCodeList
                       join incomeFeeMappingJFDept in incomeFeeMappingJFDeptList on incomeFee.CodeId equals incomeFeeMappingJFDept.MappingBId into temp
                       from incomeFeeMappingJFDept in temp.DefaultIfEmpty()
                       join JFDept in JFDeptList on incomeFeeMappingJFDept?.MappingAId equals JFDept?.CodeId into temp2
                       from JFDept in temp2.DefaultIfEmpty()
                       select new FrontEndShowEntity
                       {
                           Seq = incomeFee.Seq,
                           CodeId = incomeFee.CodeId,
                           CodeName = incomeFee.CodeName,
                           ActiveFlag = incomeFee.ActiveFlag,
                           DutyDept = JFDept?.CodeName
                       };

            return data.ToList();
        }

        public List<FrontEndShowEntity> GetExpenditureFeeType()
        {
            List<TbItemCode> expendFeeItemCode = TruckVendorMaintenanceRepository.GetExpenditureFeeTypeTbItemCodes();
            List<TbItemMappingCode> expendFeeMappingJFDeptList = TruckVendorMaintenanceRepository.GetJFDeptFeeTypeMappingTbItemCodes();
            List<TbItemCode> JFDeptList = TruckVendorMaintenanceRepository.GetJFDepartment();

            var data = from expendFee in expendFeeItemCode
                       join incomeFeeMappingJFDept in expendFeeMappingJFDeptList on expendFee.CodeId equals incomeFeeMappingJFDept.MappingBId into temp
                       from incomeFeeMappingJFDept in temp.DefaultIfEmpty()
                       join JFDept in JFDeptList on incomeFeeMappingJFDept?.MappingAId equals JFDept?.CodeId into temp2
                       from JFDept in temp2.DefaultIfEmpty()
                       select new FrontEndShowEntity
                       {
                           Seq = expendFee.Seq,
                           CodeId = expendFee.CodeId,
                           CodeName = expendFee.CodeName,
                           ActiveFlag = expendFee.ActiveFlag,
                           DutyDept = JFDept?.CodeName
                       };

            return data.ToList();
        }

        public async Task<bool> SaveExpenditureFeeType(FrontEndShowEntity entity)
        {
            try
            {
                TbItemCode tbItemCode = Mapper.Map<TbItemCode>(entity);
                //JunFuWebServiceInService.WebServiceSoapClient webServiceSoapClient = new JunFuWebServiceInService.WebServiceSoapClient(JunFuWebServiceInService.WebServiceSoapClient.EndpointConfiguration.WebServiceSoap);
                //await webServiceSoapClient.SaveExpenditureFeeTypeAsync(tbItemCode.CodeName.Trim(), tbItemCode.ActiveFlag ?? false, tbItemCode.Seq);

                if (tbItemCode != null)
                {
                    tbItemCodes_DAL _tbItemCodes_DAL = new tbItemCodes_DAL();

                    string CodeName = tbItemCode.CodeName.Trim();
                    bool ActiveFlag = tbItemCode.ActiveFlag ?? false;
                    decimal Seq = tbItemCode.Seq;

                    if (Seq == 0)
                    {
                        await _tbItemCodes_DAL.InsertExpenditureFeeType(CodeName);
                    }
                    else
                    {
                        tbItemCodes_Condition _tbItemCodes_Condition = new tbItemCodes_Condition();
                        _tbItemCodes_Condition.code_name = CodeName;
                        _tbItemCodes_Condition.active_flag = ActiveFlag;
                        _tbItemCodes_Condition.seq = Seq;
                        await _tbItemCodes_DAL.UpdatetbItemCodes(_tbItemCodes_Condition);

                    }

                }

                // TbMappingItemCode A:Dept -> B:expendFee
                int deptCodeId;
                int.TryParse(entity.DutyDept, out deptCodeId);

                int codeId;
                codeId = entity.CodeId == "" || entity.CodeId == "0" ? int.Parse(TruckVendorMaintenanceRepository.GetLastFeeCodeId()) : int.Parse(entity.CodeId);

                int mappingId = TruckVendorMaintenanceRepository.GetMappingIdByFeeType(codeId);



                tbItemMappingCodes_DAL tbItemMappingCodes_DAL = new tbItemMappingCodes_DAL();

                if (mappingId == 0)
                {
                    tbItemMappingCodes_Condition _condition = new tbItemMappingCodes_Condition();

                    _condition.mapping_a_id = deptCodeId.ToString();
                    _condition.mapping_b_id = codeId.ToString();
                    _condition.active_flag = true;

                    await tbItemMappingCodes_DAL.InsertIncomeFeeType(_condition);
                    return true;
                }
                else
                {
                    tbItemMappingCodes_Condition _condition = new tbItemMappingCodes_Condition();
                    _condition.mapping_id = mappingId;
                    _condition.mapping_a_id = deptCodeId.ToString();
                    _condition.mapping_b_id = codeId.ToString();
                    _condition.active_flag = true;

                    await tbItemMappingCodes_DAL.UpdatetbItemCodes(_condition);
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
            // return await webServiceSoapClient.UpdateJfDeptFeeTypeMappingCodeAsync(mappingId, deptCodeId, codeId, true);
        }

        public async Task<bool> SaveIncomeFeeType(FrontEndShowEntity entity)
        {
            try
            {
                TbItemCode tbItemCode = Mapper.Map<TbItemCode>(entity);
                //JunFuWebServiceInService.WebServiceSoapClient webServiceSoapClient = new JunFuWebServiceInService.WebServiceSoapClient(JunFuWebServiceInService.WebServiceSoapClient.EndpointConfiguration.WebServiceSoap);
                // await webServiceSoapClient.SaveIncomeFeeTypeAsync(tbItemCode.CodeName.Trim(), tbItemCode.ActiveFlag ?? false, tbItemCode.Seq);
                if (tbItemCode != null)
                {
                    tbItemCodes_DAL _tbItemCodes_DAL = new tbItemCodes_DAL();

                    string CodeName = tbItemCode.CodeName.Trim();
                    bool ActiveFlag = tbItemCode.ActiveFlag ?? false;
                    decimal Seq = tbItemCode.Seq;

                    if (Seq == 0)
                    {
                        await _tbItemCodes_DAL.InsertIncomeFeeType(CodeName);
                    }
                    else
                    {
                        tbItemCodes_Condition _tbItemCodes_Condition = new tbItemCodes_Condition();
                        _tbItemCodes_Condition.code_name = CodeName;
                        _tbItemCodes_Condition.active_flag = ActiveFlag;
                        _tbItemCodes_Condition.seq = Seq;
                        await _tbItemCodes_DAL.UpdatetbItemCodes(_tbItemCodes_Condition);

                    }

                }


                // TbMappingItemCode A:Dept -> B:expendFee
                int deptCodeId;
                int.TryParse(entity.DutyDept, out deptCodeId);

                int codeId;
                codeId = entity.CodeId == "" || entity.CodeId == "0" ? int.Parse(TruckVendorMaintenanceRepository.GetLastFeeCodeId()) : int.Parse(entity.CodeId);

                int mappingId = TruckVendorMaintenanceRepository.GetMappingIdByFeeType(codeId);


                tbItemMappingCodes_DAL tbItemMappingCodes_DAL = new tbItemMappingCodes_DAL();

                if (mappingId == 0)
                {
                    tbItemMappingCodes_Condition _condition = new tbItemMappingCodes_Condition();

                    _condition.mapping_a_id = deptCodeId.ToString();
                    _condition.mapping_b_id = codeId.ToString();
                    _condition.active_flag = true;

                    await tbItemMappingCodes_DAL.InsertIncomeFeeType(_condition);
                    return true;
                }
                else
                {
                    tbItemMappingCodes_Condition _condition = new tbItemMappingCodes_Condition();
                    _condition.mapping_id = mappingId;
                    _condition.mapping_a_id = deptCodeId.ToString();
                    _condition.mapping_b_id = codeId.ToString();
                    _condition.active_flag = true;

                    await tbItemMappingCodes_DAL.UpdatetbItemCodes(_condition);
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }

        }
        #endregion

        #region 下拉選單

        public List<DropDownListEntity> GetFeeTypes()
        {
            return TruckVendorPaymentRepository.GetFeeTypes();
        }

        public List<DropDownListEntity> GetCompanies()
        {
            return TruckVendorPaymentRepository.GetCompanies();
        }

        public List<TruckVendor> GetTruckVendors()
        {
            return TruckVendorPaymentRepository.GetTruckVendors();
        }
        public List<VW_tbSupplier> GetSupplier()
        {
            //return TruckVendorPaymentRepository.GetTruckVendors();
            return TbSuppliersRepository.GetVWAll();
        }
        public VW_tbSupplier GetSupplierById(int id)
        {
            //return TruckVendorPaymentRepository.GetTruckVendors();
            return TbSuppliersRepository.GetVWSupplierById(id);
        }

        public List<DropDownListEntity> GetDepartments()
        {
            return TruckVendorPaymentRepository.GetDepartments();
        }
        public List<FeeTypeRelate> GetFeetypeHasInvoice()
        {
            //return TruckVendorPaymentRepository.GetTruckVendors();
            return FeeTypeRelateRepository.GetHasInvoice();
        }
        public bool FeetypeHasInvoice(int id)
        {
            var itemHasInvoice = GetFeetypeHasInvoice();
            if (itemHasInvoice.Any(p => p.CodeId == id))
            {
                return true;
            }
            return false;
        }

        public bool HasCompanies(string company)
        {
            var companyList = GetCompanies();
            if (companyList.Any(p => p.Name == company))
            {
                return true;
            }
            return false;
        }
        public bool HasDepartments(string department)
        {
            var companyList = GetDepartments();
            if (companyList.Any(p => p.Id == department))
            {
                return true;
            }
            return false;
        }
        public bool HasSupplier(string supplier)
        {
            var tmpId = 0;
            int.TryParse(supplier, out tmpId);
            var supplierList = GetSupplier();
            if (supplierList.Any(p => p.supplier_id == tmpId))
            {
                return true;
            }
            return false;
        }
        #endregion

        #region 編輯收支明細
        public List<TruckVendorPaymentLogFrontEndShowEntity> GetFrontEndEntityByTimePeriodAndDuty(DateTime start, DateTime end, string dutyDept)
        {
            List<string> feeTypes = TbItemCodesRepository.GetFeeTypesCodeByDeptCodeId(dutyDept);

            var paymentLogRepository = TruckVendorPaymentRepository.GetPaymentByTimePeriodAndFeeType(start, end, feeTypes.ToArray());

            var paymentEntity = this.Mapper.Map<List<TruckVendorPaymentLogFrontEndShowEntity>>(paymentLogRepository);

            paymentEntity = ConvertCodeIdToName(paymentEntity);

            return paymentEntity;
        }

        public List<TruckVendorPaymentLogFrontEndShowEntity> GetFrontEndEntityByDuty(string dutyDept)
        {
            List<string> feeTypes = TbItemCodesRepository.GetFeeTypesCodeByDeptCodeId(dutyDept);

            var paymentLogRepository = TruckVendorPaymentRepository.GetPaymentByFeeType(feeTypes.ToArray());

            var paymentEntity = this.Mapper.Map<List<TruckVendorPaymentLogFrontEndShowEntity>>(paymentLogRepository);

            paymentEntity = ConvertCodeIdToName(paymentEntity);

            return paymentEntity;
        }

        public List<TruckVendorPaymentLogFrontEndShowEntity> GetFrontEndEntityByTimePeriod(DateTime start, DateTime end)
        {
            var paymentLogRepository = TruckVendorPaymentRepository.GetPaymentByTimePeriod(start, end);

            var paymentEntity = this.Mapper.Map<List<TruckVendorPaymentLogFrontEndShowEntity>>(paymentLogRepository);

            return ConvertCodeIdToName(paymentEntity);
        }

        public List<TruckVendorPaymentLogFrontEndShowEntity> GetFrontEndEntityBySearchModel(TruckVendorPaymentLogSearchEntity search)
        {
            //var paymentLogRepository = TruckVendorPaymentRepository.GetPaymentByTimePeriod(start, end);
            var result = new List<TruckVendorPaymentLogFrontEndShowEntity>();
            //List<string> feeTypes = new List<string>();
            List<JFCreateUserDropDownListEntity> users = new List<JFCreateUserDropDownListEntity>();
            //ERPSearchType dateCondition = new ERPSearchType();
            ERPSearchType userCondition = new ERPSearchType();
            ERPSearchType checkAccountCondition = new ERPSearchType();
            ERPSearchType feeTypeCondition = new ERPSearchType();
            ERPSearchType vendorCondition = new ERPSearchType();
            ERPSearchType payableCondition = new ERPSearchType();

            if (search.hasCheckAccount == true)
            {
                checkAccountCondition = ERPSearchType.HasCheckAccounted;
            }
            else if (search.hasCheckAccount == false)
            {
                checkAccountCondition = ERPSearchType.HasNotCheckAccounted;
            }
            else
            {
                checkAccountCondition = ERPSearchType.IgnoreCheckAccount;
            }
            if (search.duty == "*" && search.createUser == "*")
            {
                userCondition = ERPSearchType.AllDept;
            }
            else if (search.createUser == "*")
            {
                switch (search.duty)
                {
                    case "1":
                        search.duty = "11";
                        break;
                    case "2":
                        search.duty = "15";
                        break;
                    case "3":
                        search.duty = "13";
                        break;
                    case "4":
                        search.duty = "12";
                        break;
                    case "5":
                        search.duty = "16";
                        break;
                    default:
                        search.duty = "0";
                        break;
                }
                userCondition = ERPSearchType.DeptAllUser;
                //feeTypes = TbItemCodesRepository.GetFeeTypesCodeByDeptCodeId(search.duty);
                List<TbAccount> accountAll = AccountsRepository.GetPublicAll();
                //List<JFDepartmentDropDownListEntity> areas = StationAreaRepository.GetJFDepartment();
                List<UserRoleMapping> userRoles = UserRoleMappingRepository.GetAllUserRoleMappingsByRoleId(int.Parse(search.duty));

                foreach (var userRole in userRoles)
                {
                    var account = AccountsRepository.GetById(userRole.UserId);
                    if (account != null)
                    {
                        users.Add(new JFCreateUserDropDownListEntity() { Id = account.AccountCode, Name = account.AccountCode + " " + account.UserName });
                    }
                }
            }
            else
            {
                userCondition = ERPSearchType.UserDepend;
                //feeTypes = TbItemCodesRepository.GetFeeTypesCodeByDeptCodeId(search.duty);
            }
            if (search.feeType == "*")
            {
                feeTypeCondition = ERPSearchType.AllFeeType;
            }
            else
            {
                feeTypeCondition = ERPSearchType.FeeTypeDepend;
            }
            if (search.vendor == "*")
            {
                vendorCondition = ERPSearchType.AllVendor;
            }
            else
            {
                vendorCondition = ERPSearchType.VendorDepend;
            }
            if (search.payble == "*")
            {
                payableCondition = ERPSearchType.AllPayable;
            }
            else
            {
                payableCondition = ERPSearchType.PayableDepend;
            }
            try
            {
                var data = TruckVendorPaymentRepository.GetPaymentBySearchModel(users, search, userCondition, checkAccountCondition, feeTypeCondition, vendorCondition, payableCondition);
                //var data = TruckVendorPaymentLogDSERPRepository.GetDSERPEntityBySearchModel(search, users, dateCondition, userCondition, downlodCondition);
                //var data = TruckVendorPaymentLogDSERPRepository.GetDSERPEntityBySearchModel(search, dateCondition, downlodCondition);
                //var paymentEntity = this.Mapper.Map<List<TruckVendorPaymentLogFrontEndShowEntity>>(paymentLogRepository);
                foreach (var log in data)
                {
                    result.Add(log);
                }
                result = ConvertCodeIdToName(result);
            }
            catch (Exception e)
            {
                var a = e.Message;
                throw;
            }


            return result;
        }

        private List<TruckVendorPaymentLogFrontEndShowEntity> ConvertCodeIdToName(List<TruckVendorPaymentLogFrontEndShowEntity> paymentEntity)
        {
            var supplierList = GetSupplier();
            var supplier = new VW_tbSupplier();

            var dutyMapping = TbItemCodesRepository.GetDutyDeptMapping();
            var department = TruckVendorMaintenanceRepository.GetJFDepartment();
            var company = TruckVendorMaintenanceRepository.GetCompanyTbItemCodes();
            var feeType = TruckVendorMaintenanceRepository.GetFeeTypeTbItemCodes();
            //var vendors = TruckVendorMaintenanceRepository.GetTruckVendorsForMaintenance();
            var vendors = TbSuppliersRepository.GetPublicAll();

            foreach (var e in paymentEntity)
            {
                if (e.FeeType != null && dutyMapping.ContainsKey(e.FeeType))
                {
                    var deptId = dutyMapping[e.FeeType];
                    e.DutyDept = department.FirstOrDefault(c => c.CodeId == deptId)?.CodeName;
                }
                if (!string.IsNullOrEmpty(e.VendorName))
                {
                    supplier = supplierList.FirstOrDefault(c => c.supplier_id == int.Parse(e.VendorName));
                    if (supplier != null)
                    {
                        e.VendorUID = supplier.IDNO;
                    }
                }
                if (!string.IsNullOrEmpty(e.Payable))
                {
                    supplier = supplierList.FirstOrDefault(c => c.supplier_id == int.Parse(e.Payable));
                    if (supplier != null)
                    {
                        e.PayableUID = supplier.IDNO;
                    }
                }
                e.CompanyType = company.FirstOrDefault(c => c.CodeId == e.CompanyType)?.CodeName;
                e.FeeType = feeType.FirstOrDefault(c => c.CodeId == e.FeeType)?.CodeName;
                e.VendorName = vendors.FirstOrDefault(c => c.SupplierId.ToString() == e.VendorName)?.SupplierName;
                e.Payable = vendors.FirstOrDefault(c => c.SupplierId.ToString() == e.Payable)?.SupplierName;
            }

            return paymentEntity;
        }
        private List<TruckVendorPaymentLogFrontEndShowEntity> ConvertCodeIdToName_Old(List<TruckVendorPaymentLogFrontEndShowEntity> paymentEntity)
        {

            var dutyMapping = TbItemCodesRepository.GetDutyDeptMapping();
            var department = TruckVendorMaintenanceRepository.GetJFDepartment();
            var company = TruckVendorMaintenanceRepository.GetCompanyTbItemCodes();
            var feeType = TruckVendorMaintenanceRepository.GetFeeTypeTbItemCodes();
            var vendors = TruckVendorMaintenanceRepository.GetTruckVendorsForMaintenance();
            //var vendors = TbSuppliersRepository.GetPublicAll();

            foreach (var e in paymentEntity)
            {
                if (e.FeeType != null && dutyMapping.ContainsKey(e.FeeType))
                {
                    var deptId = dutyMapping[e.FeeType];
                    e.DutyDept = department.FirstOrDefault(c => c.CodeId == deptId)?.CodeName;
                }
                if (e.VendorName != null && vendors.FirstOrDefault(c => c.Id.ToString() == e.VendorName) != null)
                {
                    e.VendorName = vendors.FirstOrDefault(c => c.Id.ToString() == e.VendorName).VendorName;
                }
                e.CompanyType = company.FirstOrDefault(c => c.CodeId == e.CompanyType)?.CodeName;
                e.FeeType = feeType.FirstOrDefault(c => c.CodeId == e.FeeType)?.CodeName;
                }

            return paymentEntity;
        }

        /// <summary>
        /// Kendo權責單位下拉
        /// </summary>
        /// <param name="addTotal"></param>
        /// <returns></returns>
        public KendoSelectDeptEntity GetJFDept(bool addTotal = false)
        {
            List<JFDepartmentDropDownListEntity> areas = StationAreaRepository.GetJFDepartment();

            if (addTotal)
            {
                areas.Insert(0, new JFDepartmentDropDownListEntity("*", "全部部門"));
            }

            return new KendoSelectDeptEntity { results = areas };
        }
        /// <summary>
        /// Kendo交易項目下拉
        /// </summary>
        /// <param name="addTotal"></param>
        /// <returns></returns>
        public KendoSelectDeptEntity GetFeetype(bool addTotal = false)
        {
            List<JFDepartmentDropDownListEntity> feetypes = StationAreaRepository.GetFeetype();

            if (addTotal)
            {
                feetypes.Insert(0, new JFDepartmentDropDownListEntity("*", "全部項目"));
            }

            return new KendoSelectDeptEntity { results = feetypes };
        }
        /// <summary>
        /// Kendo供應商下拉
        /// </summary>
        /// <param name="addTotal"></param>
        /// <returns></returns>
        public KendoSelectDeptEntity GetSuppliers(bool addTotal = false)
        {
            List<JFDepartmentDropDownListEntity> suppliers = TbSuppliersRepository.GetSuppliers();

            if (addTotal)
            {
                suppliers.Insert(0, new JFDepartmentDropDownListEntity("*", "全部供應商"));
            }

            return new KendoSelectDeptEntity { results = suppliers };
        }

        public List<TruckVendorPaymentLogFrontEndShowEntity> AddCreateUserInEntity(List<TruckVendorPaymentLogFrontEndShowEntity> data, string createUser)
        {
            var result = data.Where(p => p.CreateUser == createUser).ToList();
            var account = AccountsRepository.GetByAccountCode(createUser);
            foreach (var log in result)
            {
                log.CreateUser = account.AccountCode + " " + account.UserName;
            }
            return result;
        }
        public List<TruckVendorPaymentLogFrontEndShowEntity> AddNoteSearchInEntity(List<TruckVendorPaymentLogFrontEndShowEntity> data, string noteSearch)
        {
            var result = data.Where(p => p.Memo.ToLower().Contains(noteSearch.ToLower())).ToList();
            return result;
        }
        #endregion

        #region 文中
        public List<TruckVendorPaymentLogWenERPShowEntity> GetWenERPEntityByTimePeriodAndDuty(DateTime sDate, DateTime eDate, string dutyDept)
        {
            List<string> feeTypes = TbItemCodesRepository.GetFeeTypesCodeByDeptCodeId(dutyDept);

            var result = new List<TruckVendorPaymentLogWenERPShowEntity>();

            var data = TruckVendorPaymentLogWenERPRepository.GetByTimePeriodAndFeeType(sDate, eDate, feeTypes.ToArray());

            //var paymentEntity = this.Mapper.Map<List<TruckVendorPaymentLogFrontEndShowEntity>>(paymentLogRepository);

            //paymentEntity = ConvertCodeIdToName(paymentEntity);

            return result;
        }
        public List<TruckVendorPaymentLogWenERPShowEntity> GetWenERPEntityByDuty(string dutyDept)
        {
            List<string> feeTypes = TbItemCodesRepository.GetFeeTypesCodeByDeptCodeId(dutyDept);
            var result = new List<TruckVendorPaymentLogWenERPShowEntity>();

            //var paymentLogRepository = TruckVendorPaymentRepository.GetPaymentByFeeType(feeTypes.ToArray());

            //var paymentEntity = this.Mapper.Map<List<TruckVendorPaymentLogFrontEndShowEntity>>(paymentLogRepository);
            var data = TruckVendorPaymentLogWenERPRepository.GetByFeeType(feeTypes.ToArray());
            //var paymentEntity = this.Mapper.Map<List<TruckVendorPaymentLogFrontEndShowEntity>>(paymentLogRepository);
            foreach (var log in data)
            {
                result.Add(log);
            }
            result = ConvertCodeIdToNameWen(result);

            return result;
        }

        public List<TruckVendorPaymentLogWenERPShowEntity> GetWenERPEntityByTimePeriod(DateTime sDate, DateTime eDate)
        {
            //var paymentLogRepository = TruckVendorPaymentRepository.GetPaymentByTimePeriod(start, end);
            var result = new List<TruckVendorPaymentLogWenERPShowEntity>();


            var data = TruckVendorPaymentLogWenERPRepository.GetByTimePeriod(sDate, eDate);
            //var paymentEntity = this.Mapper.Map<List<TruckVendorPaymentLogFrontEndShowEntity>>(paymentLogRepository);
            foreach (var log in data)
            {
                result.Add(log);
            }
            result = ConvertCodeIdToNameWen(result);
            return result;

        }
        public List<TruckVendorPaymentLogWenERPShowEntity> GetWenERPEntityBySearchModel(TruckVendorPaymentLogWenERPSearchEntity search)
        {
            //var paymentLogRepository = TruckVendorPaymentRepository.GetPaymentByTimePeriod(start, end);
            var result = new List<TruckVendorPaymentLogWenERPShowEntity>();
            List<string> feeTypes = new List<string>();

            ERPSearchType dateCondition = new ERPSearchType();
            ERPSearchType userCondition = new ERPSearchType();
            ERPSearchType downlodCondition = new ERPSearchType();

            if (search.startUpload == null || search.endUpload == null)
            {
                dateCondition = ERPSearchType.OnlyRegisterDate;
            }
            else if (search.startRegister == null || search.endRegister == null)
            {
                dateCondition = ERPSearchType.OnlyUploadDate;
            }
            else
            {
                dateCondition = ERPSearchType.BothDate;
            }
            if (search.duty == "*" && search.createUser == "*")
            {
                userCondition = ERPSearchType.AllDept;
            }
            else if (search.createUser == "*")
            {
                userCondition = ERPSearchType.DeptAllUser;
                feeTypes = TbItemCodesRepository.GetFeeTypesCodeByDeptCodeId(search.duty);
            }
            else
            {
                userCondition = ERPSearchType.UserDepend;
                feeTypes = TbItemCodesRepository.GetFeeTypesCodeByDeptCodeId(search.duty);
            }
            if (search.hasDownload == true)
            {
                downlodCondition = ERPSearchType.HasDownload;
            }
            else if (search.hasDownload == false)
            {
                downlodCondition = ERPSearchType.HasNotDownload;
            }
            else
            {
                downlodCondition = ERPSearchType.IgnoreDownload;
            }
            var data = TruckVendorPaymentLogWenERPRepository.GetWenERPEntityBySearchModel(search, feeTypes.ToArray(), dateCondition, userCondition, downlodCondition);
            //var paymentEntity = this.Mapper.Map<List<TruckVendorPaymentLogFrontEndShowEntity>>(paymentLogRepository);
            foreach (var log in data)
            {
                result.Add(log);
            }
            result = ConvertCodeIdToNameWen(result);
            return result;

        }

        private List<TruckVendorPaymentLogWenERPShowEntity> ConvertCodeIdToNameWen(List<TruckVendorPaymentLogWenERPShowEntity> paymentEntity)
        {
            var dutyMapping = TbItemCodesRepository.GetDutyDeptMapping();
            var department = TruckVendorMaintenanceRepository.GetJFDepartment();
            var company = TruckVendorMaintenanceRepository.GetCompanyTbItemCodes();
            var feeType = TruckVendorMaintenanceRepository.GetFeeTypeTbItemCodes();
            //var vendors = TruckVendorMaintenanceRepository.GetTruckVendorsForMaintenance();
            var vendors = TbSuppliersRepository.GetPublicAll();

            foreach (var e in paymentEntity)
            {
                if (e.FeeType != null && dutyMapping.ContainsKey(e.FeeType))
                {
                    var deptId = dutyMapping[e.FeeType];
                    e.DutyDept = department.FirstOrDefault(c => c.CodeId == deptId)?.CodeName;
                }

                e.Company = company.FirstOrDefault(c => c.CodeId == e.Company)?.CodeName;
                e.FeeType = feeType.FirstOrDefault(c => c.CodeId == e.FeeType)?.CodeName;
                e.VendorName = vendors.FirstOrDefault(c => c.SupplierId.ToString() == e.VendorName)?.SupplierName;
            }

            return paymentEntity;
        }

        public KendoSelectCreateUserEntity GetCreateUserByDuty(string duty)
        {

            List<TbAccount> accountAll = AccountsRepository.GetPublicAll();
            //List<JFDepartmentDropDownListEntity> areas = StationAreaRepository.GetJFDepartment();
            List<UserRoleMapping> userRoles = UserRoleMappingRepository.GetAllUserRoleMappingsByRoleId(int.Parse(duty));
            List<JFCreateUserDropDownListEntity> users = new List<JFCreateUserDropDownListEntity>();
            foreach (var userRole in userRoles)
            {
                var account = AccountsRepository.GetById(userRole.UserId);
                if (account != null)
                {
                    users.Add(new JFCreateUserDropDownListEntity() { Id = account.AccountCode, Name = account.AccountCode + " " + account.UserName });
                }
            }
            users.Insert(0, new JFCreateUserDropDownListEntity("*", "全部"));
            return new KendoSelectCreateUserEntity { results = users };
        }
        public void UpdateWenERPDownloadRecord(int[] ids, string userAccount)
        {
            var updateLogs = TruckVendorPaymentLogWenERPRepository.GetByIds(ids);
            foreach (var log in updateLogs)
            {
                log.HasDownload = true;
                log.DownloadDate = DateTime.Now;
                log.DownloadUser = userAccount;
                TruckVendorPaymentLogWenERPRepository.Update(log);
            }
        }
        public void UpdateDSERPDownloadRecord(int[] ids, string userAccount)
        {
            var updateLogs = TruckVendorPaymentLogDSERPRepository.GetByIds(ids);
            foreach (var log in updateLogs)
            {
                log.HasDownload = true;
                log.DownloadDate = DateTime.Now;
                log.DownloadUser = userAccount;
                //TruckVendorPaymentLogDSERPRepository.Update(log);
            }
            TruckVendorPaymentLogDSERPRepository.UpdateBatch(updateLogs);
        }
        public string UpdateDSERPMergeRecord(int[] ids, string userAccount)
        {
            var mergeId = Guid.NewGuid().ToString().Substring(0, 10);
            var downloadId = Guid.NewGuid().ToString().Substring(0, 15);
            var snapShot = new List<DSERPMergeDataSnapShot>();
            //var updateLogs = TruckVendorPaymentLogDSERPRepository.GetByIds(ids).Where(p => string.IsNullOrEmpty(p.MergeRandomCode)).ToList();
            var updateLogs = TruckVendorPaymentLogDSERPRepository.GetByIds(ids);
            foreach (var log in updateLogs)
            {
                log.HasDownload = true;
                log.DownloadDate = DateTime.Now;
                log.DownloadUser = userAccount;
                if (string.IsNullOrEmpty(log.MergeRandomCode))
                {
                    log.MergeRandomCode = mergeId;
                }
                var tmp = new DSERPMergeDataSnapShot();
                tmp = log;
                tmp.DownloadRandomCode = downloadId;
                snapShot.Add(tmp);
                //TruckVendorPaymentLogDSERPRepository.Update(log);
            }
            try
            {
                TruckVendorPaymentLogDSERPRepository.UpdateBatch(updateLogs);
                DSERPMergeDataSnapShotRepository.InsertBatch(snapShot);
            }
            catch (Exception e)
            {
                var msg = e.Message;
                throw;
            }
            return downloadId;
        }
        public void WenERPCheckAccount(int[] ids, string userAccount)
        {
            try
            {
                var truckVendorPatmentLogsTrnsfer = TruckVendorPaymentRepository.GetByIds(ids);
                foreach (var log in truckVendorPatmentLogsTrnsfer)
                {
                    log.IsChecked = true;
                }

                //TransferLogToWenERP(truckVendorPatmentLogsTrnsfer);
                TransferLogToDSERP(truckVendorPatmentLogsTrnsfer);

                TruckVendorPaymentRepository.UpdateBatch(truckVendorPatmentLogsTrnsfer);
            }
            catch (Exception e)
            {
                var errLog = new JunFuOrangeLog();
                errLog.Guid = Guid.NewGuid().ToString("N");
                errLog.Account = userAccount;
                errLog.Path = "/TruckVendorPaymentAndMaintenance/WenERPCheckAccount";
                errLog.RequestBody = "";
                errLog.RequestHeader = "WenERPCheckAccount ERROR";
                errLog.RequestBody = e.Message;
                errLog.QueryString = "";
                errLog.StartDate = DateTime.Now;
                errLog.EndDate = DateTime.Now; 
                JunFuOrangeLogRepository.Insert(errLog);
            }
        }

        public byte[] ExportBasicInformation()
        {
            var memoryStream = new MemoryStream();
            using (var document = SpreadsheetDocument.Create(memoryStream, SpreadsheetDocumentType.Workbook))
            {
                var workbookPart = document.AddWorkbookPart();
                workbookPart.Workbook = new Workbook();
                var sheets = workbookPart.Workbook.AppendChild(new Sheets());

                var worksheetPart1 = workbookPart.AddNewPart<WorksheetPart>();
                worksheetPart1.Worksheet = new Worksheet(new SheetData());
                var worksheetPart2 = workbookPart.AddNewPart<WorksheetPart>();
                worksheetPart2.Worksheet = new Worksheet(new SheetData());
                var worksheetPart3 = workbookPart.AddNewPart<WorksheetPart>();
                worksheetPart3.Worksheet = new Worksheet(new SheetData());
                var worksheetPart4 = workbookPart.AddNewPart<WorksheetPart>();
                worksheetPart4.Worksheet = new Worksheet(new SheetData());

                //tbItemCodes
                sheets.Append(new Sheet()
                {
                    Id = workbookPart.GetIdOfPart(worksheetPart1),
                    SheetId = 1,
                    Name = "tbItemCodes"
                });
                //tbSuppliers
                sheets.Append(new Sheet()
                {
                    Id = workbookPart.GetIdOfPart(worksheetPart2),
                    SheetId = 2,
                    Name = "tbSuppliers"
                });
                //Department
                sheets.Append(new Sheet()
                {
                    Id = workbookPart.GetIdOfPart(worksheetPart3),
                    SheetId = 3,
                    Name = "Department"
                });
                //tbCompanyCodes
                sheets.Append(new Sheet()
                {
                    Id = workbookPart.GetIdOfPart(worksheetPart4),
                    SheetId = 4,
                    Name = "tbCompanyCodes"
                });

                // 從 Worksheet 取得要編輯的 SheetData
                var sheetData1 = worksheetPart1.Worksheet.GetFirstChild<SheetData>();

                // 建立資料列物件
                var row1 = new Row();
                // 在資料列中插入欄位
                row1.Append(
                    new Cell() { CellValue = new CellValue("code_name"), DataType = CellValues.String }
                );
                // 插入資料列 
                sheetData1.AppendChild(row1);

                List<string> FeeTypesName = GetFeeTypes().Select(a => a.Name).ToList();
                foreach (var item in FeeTypesName)
                {
                    row1 = new Row();
                    row1.Append(
                        new Cell() { CellValue = new CellValue(item), DataType = CellValues.String }
                    );
                    sheetData1.AppendChild(row1);
                }


                var sheetData2 = worksheetPart2.Worksheet.GetFirstChild<SheetData>();

                // 建立資料列物件
                var row2 = new Row();
                // 在資料列中插入欄位
                row2.Append(
                    new Cell() { CellValue = new CellValue("supplier_name"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("IDNO"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("supplier_id"), DataType = CellValues.String }
                );
                // 插入資料列 
                sheetData2.AppendChild(row2);
                var supplierList = GetSupplier();
                foreach (var supplier in supplierList)
                {
                    row2 = new Row();
                    row2.Append(
                        new Cell() { CellValue = new CellValue(supplier.vendorName), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(supplier.IDNO), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(supplier.supplier_id.ToString()), DataType = CellValues.String }
                    );
                    sheetData2.AppendChild(row2);
                }

                var sheetData3 = worksheetPart3.Worksheet.GetFirstChild<SheetData>();

                // 建立資料列物件
                var row3 = new Row();
                // 在資料列中插入欄位
                row3.Append(
                    new Cell() { CellValue = new CellValue("項次"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("部門\\工地編號"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("部門\\工地名稱"), DataType = CellValues.String }
                );
                // 插入資料列 
                sheetData3.AppendChild(row3);
                var departmentList = GetDepartments();
                var i = 1;
                foreach (var department in departmentList)
                {
                    row3 = new Row();
                    row3.Append(
                        new Cell() { CellValue = new CellValue(i.ToString()), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(department.Id), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(department.Name), DataType = CellValues.String }
                    ); ;
                    i++;
                    sheetData3.AppendChild(row3);
                }

                // 從 Worksheet 取得要編輯的 SheetData
                var sheetData4 = worksheetPart4.Worksheet.GetFirstChild<SheetData>();

                // 建立資料列物件
                var row4 = new Row();
                // 在資料列中插入欄位
                row4.Append(
                    new Cell() { CellValue = new CellValue("code_name"), DataType = CellValues.String }
                );
                // 插入資料列 
                sheetData4.AppendChild(row4);

                List<string> CompanyName = GetCompanies().Select(a => a.Name).ToList();
                foreach (var item in CompanyName)
                {
                    row4 = new Row();
                    row4.Append(
                        new Cell() { CellValue = new CellValue(item), DataType = CellValues.String }
                    );
                    sheetData4.AppendChild(row4);
                }

                workbookPart.Workbook.Save();
                document.Close();
            }
            //memoryStream.Seek(0, SeekOrigin.Begin);
            return memoryStream.ToArray();
        }

        public byte[] ExportMergedInformation(string[] aryIds)
        {
            var mergeLogs = VWTruckLogRepository.GetMergeDataByIId(aryIds);
            var company = TruckVendorMaintenanceRepository.GetCompanyTbItemCodes();
            var vendors = TbSuppliersRepository.GetPublicAll();

            var memoryStream = new MemoryStream();
            using (var document = SpreadsheetDocument.Create(memoryStream, SpreadsheetDocumentType.Workbook))
            {
                var workbookPart = document.AddWorkbookPart();
                workbookPart.Workbook = new Workbook();
                var sheets = workbookPart.Workbook.AppendChild(new Sheets());

                var worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                worksheetPart.Worksheet = new Worksheet(new SheetData());
                //tbItemCodes
                sheets.Append(new Sheet()
                {
                    Id = workbookPart.GetIdOfPart(worksheetPart),
                    SheetId = 1,
                    Name = "MergeData"
                });
                // 從 Worksheet 取得要編輯的 SheetData
                var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

                // 建立資料列物件
                var row = new Row();
                // 在資料列中插入欄位
                row.Append(
                    new Cell() { CellValue = new CellValue("年度"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("進銷單號碼"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("日期"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("單別"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("客戶供應商"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("請款客戶"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("序號"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("產品代號"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("稅別"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("單價含稅否"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("數量	"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("未稅單價"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("未稅金額"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("電子發票註記"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("列印紙本電子發票"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("對帳日"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("結帳日"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("收款日"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("製成品代號"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("生產數量"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("發票號碼"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("發票日期"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("傳票類別"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("明細備註"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("明細備註二"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("主檔備註"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("序號(客供商聯絡人)"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("進銷特殊欄位"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("序號(客戶供應商統編)"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("序號(客戶供應商地址)"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("包裝別"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("外幣代號"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("匯率"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("外幣未稅單價"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("外幣未稅金額"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("訂單單號"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("採購單號"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("業務員代號"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("部門工地編號"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("專案項目編號"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("車號"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("車行"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("車主"), DataType = CellValues.String }
                );
                // 插入資料列 
                sheetData.AppendChild(row);
                var strTaxInclude = string.Empty;
                foreach (var log in mergeLogs)
                {
                    strTaxInclude = log.TaxInclude ? "1" : "0";

                    log.Company = company.FirstOrDefault(c => c.CodeId == log.Company)?.CodeName;
                    log.VendorName = vendors.FirstOrDefault(c => c.SupplierId.ToString() == log.VendorName)?.SupplierName;
                    row = new Row();
                    row.Append(
                        new Cell() { CellValue = new CellValue(log.Year), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.InvoicingNumber), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.RegisteredDate.ToString("yyyyMMdd")), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.OrderType), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.SupplierUID), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.CustomerUID), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.Number), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.ProductCode), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.TaxType), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(strTaxInclude), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.Quantity.ToString()), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.UnitPrice.ToString()), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.Payment.ToString()), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.ElecInovoice), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.PaperInovoice), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.ReconciliationDate.ToString()), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.CheckoutDate.ToString()), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.PaymentDate.ToString()), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.ManufactureCode), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.ProductionQuantity.ToString()), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.InvoiceNumber), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.InvoiceDate.ToString()), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.VoucherType), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.Note01), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.Note02), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.NoteMain), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.SupplierContact), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.InvoicingSpecial), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.SupplierUniformID), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.SupplierAddress), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.PackageType), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.ForeignCurrencyCode), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.ExchangeRate.ToString()), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.ForeignCurrencyUnitPrice.ToString()), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.ForeignCurrencyPayment.ToString()), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.OrderNumber), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.PurchaseNumber), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.SalesCode), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.Department), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.ProjectCode), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.CarLicense), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.Company), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.VendorName), DataType = CellValues.String }
                    );
                    sheetData.AppendChild(row);
                }


                workbookPart.Workbook.Save();
                document.Close();
            }
            //memoryStream.Seek(0, SeekOrigin.Begin);
            return memoryStream.ToArray();
        }
        #endregion
        #region 鼎新
        public List<TruckVendorPaymentLogDSERPShowEntity> GetDSERPEntityBySearchModel(TruckVendorPaymentLogDSERPSearchEntity search)
        {
            //var paymentLogRepository = TruckVendorPaymentRepository.GetPaymentByTimePeriod(start, end);
            var result = new List<TruckVendorPaymentLogDSERPShowEntity>();
            //List<string> feeTypes = new List<string>();
            List<JFCreateUserDropDownListEntity> users = new List<JFCreateUserDropDownListEntity>();
            ERPSearchType dateCondition = new ERPSearchType();
            ERPSearchType downlodCondition = new ERPSearchType();
            ERPSearchType userCondition = new ERPSearchType();
            ERPSearchType feeTypeCondition = new ERPSearchType();


            if (search.startUpload == null || search.endUpload == null)
            {
                dateCondition = ERPSearchType.OnlyRegisterDate;
            }
            else if (search.startRegister == null || search.endRegister == null)
            {
                dateCondition = ERPSearchType.OnlyUploadDate;
            }
            else
            {
                dateCondition = ERPSearchType.BothDate;
            }
            if (search.hasDownload == true)
            {
                downlodCondition = ERPSearchType.HasDownload;
            }
            else if (search.hasDownload == false)
            {
                downlodCondition = ERPSearchType.HasNotDownload;
            }
            else
            {
                downlodCondition = ERPSearchType.IgnoreDownload;
            }
            if (search.duty == "*" && search.createUser == "*")
            {
                userCondition = ERPSearchType.AllDept;
            }
            else if (search.createUser == "*")
            {
                switch (search.duty)
                {
                    case "1":
                        search.duty = "11";
                        break;
                    case "2":
                        search.duty = "15";
                        break;
                    case "3":
                        search.duty = "13";
                        break;
                    case "4":
                        search.duty = "12";
                        break;
                    case "5":
                        search.duty = "16";
                        break;
                    default:
                        search.duty = "0";
                        break;
                }
                userCondition = ERPSearchType.DeptAllUser;
                //feeTypes = TbItemCodesRepository.GetFeeTypesCodeByDeptCodeId(search.duty);
                List<TbAccount> accountAll = AccountsRepository.GetPublicAll();
                //List<JFDepartmentDropDownListEntity> areas = StationAreaRepository.GetJFDepartment();
                List<UserRoleMapping> userRoles = UserRoleMappingRepository.GetAllUserRoleMappingsByRoleId(int.Parse(search.duty));

                foreach (var userRole in userRoles)
                {
                    var account = AccountsRepository.GetById(userRole.UserId);
                    if (account != null)
                    {
                        users.Add(new JFCreateUserDropDownListEntity() { Id = account.AccountCode, Name = account.AccountCode + " " + account.UserName });
                    }
                }
            }
            else
            {
                userCondition = ERPSearchType.UserDepend;
                //feeTypes = TbItemCodesRepository.GetFeeTypesCodeByDeptCodeId(search.duty);
            }
            if (search.feeType == "*")
            {
                feeTypeCondition = ERPSearchType.AllFeeType;
            }
            else
            {
                feeTypeCondition = ERPSearchType.FeeTypeDepend;
            }

            var data = TruckVendorPaymentLogDSERPRepository.GetDSERPEntityBySearchModel(search, users, dateCondition, userCondition, downlodCondition, feeTypeCondition);
            //var data = TruckVendorPaymentLogDSERPRepository.GetDSERPEntityBySearchModel(search, users, dateCondition, userCondition, downlodCondition);
            //var data = TruckVendorPaymentLogDSERPRepository.GetDSERPEntityBySearchModel(search, dateCondition, downlodCondition);
            //var paymentEntity = this.Mapper.Map<List<TruckVendorPaymentLogFrontEndShowEntity>>(paymentLogRepository);
            foreach (var log in data)
            {
                result.Add(log);
            }
            result = ConvertCodeIdToNameDS(result);
            return result;
        }
        private List<TruckVendorPaymentLogDSERPShowEntity> ConvertCodeIdToNameDS(List<TruckVendorPaymentLogDSERPShowEntity> paymentEntity)
        {
            var dutyMapping = TbItemCodesRepository.GetDutyDeptMapping();
            //var department = TruckVendorMaintenanceRepository.GetJFDepartment();
            //var company = TruckVendorMaintenanceRepository.GetCompanyTbItemCodes();
            var feeType = TruckVendorMaintenanceRepository.GetFeeTypeTbItemCodes();
            //var vendors = TruckVendorMaintenanceRepository.GetTruckVendorsForMaintenance();
            //var vendors = TbSuppliersRepository.GetPublicAll();

            foreach (var e in paymentEntity)
            {
                if (e.FeeType != null && dutyMapping.ContainsKey(e.FeeType))
                {
                    var deptId = dutyMapping[e.FeeType];
                    //e.DutyDept = department.FirstOrDefault(c => c.CodeId == deptId)?.CodeName;
                }

                //e.Company = company.FirstOrDefault(c => c.CodeId == e.Company)?.CodeName;
                e.FeeType = feeType.FirstOrDefault(c => c.CodeId == e.FeeType)?.CodeName;
                //e.VendorName = vendors.FirstOrDefault(c => c.SupplierId.ToString() == e.VendorName)?.SupplierName;
            }

            return paymentEntity;
        }
        public byte[] ExportDSMergedInformation(string downloadId)
        {
            var mergeLogs = VWDSTruckLogRepository.GetByDownloadId(downloadId);

            var memoryStream = new MemoryStream();
            using (var document = SpreadsheetDocument.Create(memoryStream, SpreadsheetDocumentType.Workbook))
            {
                var workbookPart = document.AddWorkbookPart();
                workbookPart.Workbook = new Workbook();
                var sheets = workbookPart.Workbook.AppendChild(new Sheets());

                var worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                worksheetPart.Worksheet = new Worksheet(new SheetData());
                //tbItemCodes
                sheets.Append(new Sheet()
                {
                    Id = workbookPart.GetIdOfPart(worksheetPart),
                    SheetId = 1,
                    Name = "MergeData"
                });
                // 從 Worksheet 取得要編輯的 SheetData
                var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

                // 建立資料列物件
                var row = new Row();
                // 在資料列中插入欄位
                row.Append(
                    new Cell() { CellValue = new CellValue("入帳公司別"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("憑單單別"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("產品代號(品號類別一)"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("客戶/供應商(原始交易對象)"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("未稅金額(收/付款金額)"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("代付碼"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("車主(客戶代號)"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("代付轉收金額(未稅金額)"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("ERP計算價差(收入)"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("車號(文註註記)"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("明細備註二"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("部門編號"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("專案項目編號(車行)"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("發票號碼"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("發票日期"), DataType = CellValues.String }
                );
                // 插入資料列 
                sheetData.AppendChild(row);
                foreach (var log in mergeLogs)
                {
                    var strInvoiceDate = string.Empty;
                    if (log.InvoiceDate != null)
                    {
                        strInvoiceDate = ((DateTime)log.InvoiceDate).ToString("yyyyMMdd");
                    }
                    row = new Row();
                    row.Append(
                        new Cell() { CellValue = new CellValue(log.Company), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.VoucherCode), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.DinShinItemCode), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.SupplierUID), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.Accounts.ToString()), DataType = CellValues.Number },
                        new Cell() { CellValue = new CellValue(log.CollectionPayCode), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.CustomerUID), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.CollectionAccounts.ToString()), DataType = CellValues.Number },
                        new Cell() { CellValue = new CellValue(log.Spread.ToString()), DataType = CellValues.Number },
                        new Cell() { CellValue = new CellValue(log.CarLicense), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.Note02), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.Department), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.CompanyUID), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(log.InvoiceNumber), DataType = CellValues.String },
                        new Cell() { CellValue = new CellValue(strInvoiceDate), DataType = CellValues.String }
                    );
                    sheetData.AppendChild(row);
                }
                workbookPart.Workbook.Save();
                document.Close();
            }
            //memoryStream.Seek(0, SeekOrigin.Begin);
            return memoryStream.ToArray();
        }

        #endregion
    }
}
