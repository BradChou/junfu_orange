﻿using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.BL.Services.TruckVendorPaymentAndMaintenance
{
    public interface ITruckVendorPaymentService
    {
        List<TruckVendorPaymentLogFrontEndShowEntity> GetAllByMonthAndTruckVendor(DateTime start, DateTime end, string vendor);

        List<TruckVendorPaymentLogFrontEndShowEntity> GetAllByMonthAndTruckVendor_Old(DateTime start, DateTime end, string vendor);
        List<DisbursementEntity> GetDisbursementByMonthAndTruckVendor(DateTime start, DateTime end, string vendor);
        List<DisbursementEntity> GetDisbursementByMonthAndTruckVendor_Old(DateTime start, DateTime end, string vendor);
        PaymentEntity GetIncomeAndExpenditure(DateTime start, DateTime end, string vendor);
        PaymentEntity GetIncomeAndExpenditure_Old(DateTime start, DateTime end, string vendor);
        List<TruckVendorPaymentLog> GetLatestUploadData(string ImportRandomCode = "");
        List<TbAccount> GetSuppliersAccount();
        TbAccount GetByAccount(string account);
        TruckVendor GetTruckVendorByTruckVendorAccountCode(string accountCode);

        List<TruckVendor> GetTruckVendors();

        List<DropDownListEntity> GetFeeTypes();

        List<DropDownListEntity> GetCompanies();

        List<FrontEndShowEntity> GetIncomeFeeType();

        List<FrontEndShowEntity> GetExpenditureFeeType();
        List<DropDownListEntity> GetDepartments();
        List<VW_tbSupplier> GetSupplier();
        VW_tbSupplier GetSupplierById(int id);
        Task<bool> SaveExpenditureFeeType(FrontEndShowEntity entity);

        Task<bool> SaveIncomeFeeType(FrontEndShowEntity entity);

        List<TbItemCode> GetCompanyTbItemCodes();
        List<TruckVendor> GetTruckVendorsForMaintenance();


        void SaveTruckVendor(TruckVendor truckVendor);
        List<int> Save(List<TruckVendorPaymentLog> truckVendorPaymentLog);
        void Update(TruckVendorPaymentLog truckVendorPaymentLog);

        void UpdateVendorLog(TruckVendorPaymentLog truckVendorPaymentLog);

        //void Delete(TruckVendorPaymentLogFrontEndShowEntity entity);
        void Delete(TruckVendorPaymentLog entity, string userAccount);
        void DeleteBatch(int[] intAryIds, string userAccount);

        byte[] Export();

        List<TruckVendorPaymentLogFrontEndShowEntity> GetFrontEndEntityByTimePeriodAndDuty(DateTime start, DateTime end, string dutyDept);

        List<TruckVendorPaymentLogFrontEndShowEntity> GetFrontEndEntityByDuty(string dutyDept);

        List<TruckVendorPaymentLogFrontEndShowEntity> GetFrontEndEntityByTimePeriod(DateTime start, DateTime end);

        public List<TruckVendorPaymentLogFrontEndShowEntity> GetFrontEndEntityBySearchModel(TruckVendorPaymentLogSearchEntity search);
        KendoSelectDeptEntity GetJFDept(bool addTotal = false);
        KendoSelectDeptEntity GetFeetype(bool addTotal = false);
        KendoSelectDeptEntity GetSuppliers(bool addTotal = false);
        List<TruckVendorPaymentLogFrontEndShowEntity> AddCreateUserInEntity(List<TruckVendorPaymentLogFrontEndShowEntity> data, string createUser);
        List<TruckVendorPaymentLogWenERPShowEntity> GetWenERPEntityBySearchModel(TruckVendorPaymentLogWenERPSearchEntity search);
        List<TruckVendorPaymentLogDSERPShowEntity> GetDSERPEntityBySearchModel(TruckVendorPaymentLogDSERPSearchEntity search);
        List<TruckVendorPaymentLogFrontEndShowEntity> AddNoteSearchInEntity(List<TruckVendorPaymentLogFrontEndShowEntity> data, string noteSearch);
        KendoSelectCreateUserEntity GetCreateUserByDuty(string duty);
        void UpdateWenERPDownloadRecord(int[] ids, string userAccount);
        void UpdateDSERPDownloadRecord(int[] ids, string userAccount);
        string UpdateDSERPMergeRecord(int[] ids, string userAccount);
        void WenERPCheckAccount(int[] ids, string userAccount);
        byte[] ExportBasicInformation();
        byte[] ExportMergedInformation(string[] aryIds);
        byte[] ExportDSMergedInformation(string downloadId);
        bool FeetypeHasInvoice(int id);
        bool HasCompanies(string company);
        bool HasDepartments(string company);
        bool HasSupplier(string vendorName);
    }
}
