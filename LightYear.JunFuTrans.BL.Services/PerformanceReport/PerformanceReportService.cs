﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.PerformanceReport;
using LightYear.JunFuTrans.BL.BE.StudentTest;
using LightYear.JunFuTrans.DA.Repositories.PerformanceReport;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using LightYear.JunFuTrans.BL.Services.DeliveryException;
using LightYear.JunFuTrans.BL.BE.DeliveryException;
using System.Linq;

namespace LightYear.JunFuTrans.BL.Services.PerformanceReport
{
    public class PerformanceReportService : IPerformanceReportService
    {
        public PerformanceReportService(IPerformanceReportRepository performanceReportRepository, IDeliveryExceptionService deliveryExceptionService)
        {
            this.PerformanceReportRepository = performanceReportRepository;
            this.DeliveryExceptionService = deliveryExceptionService;
        }

        public IPerformanceReportRepository PerformanceReportRepository { get; set; }
        public IDeliveryExceptionService DeliveryExceptionService { get; set; }

        public AccountEntity GetAccountData(AccountEntity accountCode)
        {
            var account = PerformanceReportRepository.GetByAccountCode(accountCode.AccountCode);

            List<StationEntity> stationList = DeliveryExceptionService.GetStation();

            string station = "";
            bool isMaster = false;
            string loginStation = accountCode.AccountLoginStation;
            if (loginStation == "FGM01" || loginStation == "FMD01" || loginStation == "FHD01" || loginStation == "FBD01" || loginStation == "FBM01" || loginStation == "FBM02")
            {
                station = "F99";         //F99在Station中代表峻富總公司
                isMaster = true;
            } 
            else
            {
                station = "F" + loginStation;
            }

            AccountEntity performanceReportEntity = new AccountEntity()
            {
                AccountCode = accountCode.AccountCode,
                AccountName = accountCode.AccountName,
                AccountStation = station,
                IsMaster = isMaster
            };

            return performanceReportEntity;
        }
        public List<PerformanceReportEntity> PerformanceReportList(FrontendInputEntity frontendInput)
        {
            int whereIsTheF = frontendInput.AccountCode.IndexOf("F");
            if (whereIsTheF != -1)
            {
                frontendInput.AccountCode = GetAccountCodeAndStationInString(frontendInput.AccountCode);
            }

            whereIsTheF = frontendInput.SendStation.IndexOf("F");
            if(whereIsTheF != -1)
            {
                frontendInput.SendStation = GetAccountCodeAndStationInString(frontendInput.SendStation);
            }

            if (frontendInput.AccountCode == "請選擇一客戶")
            {
                frontendInput.AccountCode = "*";                   //意即全客代
            }
            if (frontendInput.SendStation == "全部站所")
            {
                frontendInput.SendStation = "*";
            }

            List<PerformanceReportEntity> performanceReportList = new List<PerformanceReportEntity>();      //最終結果

            List<AccountEntity> accountsList = new List<AccountEntity>();                               //統計有哪些客戶用

            Double ticks = new TimeSpan(frontendInput.End.Ticks - frontendInput.Start.Ticks).TotalDays;             //計算執行區間有幾天

            int days = (int)ticks + 1;

            List<DateTime> daysList = new List<DateTime>();                         //放日期

            List<StationEntity> stationList = DeliveryExceptionService.GetStation();

            int number = -1;                                                           //計數器

            foreach (var i in stationList)                                      //將輸入的站所從中文改成代碼
            {
                if (i.Name == frontendInput.SendStation)
                {
                    frontendInput.SendStation = i.Code;
                }
            }

            for (var i = 0; i < days; i++)
            {
                DateTime day = frontendInput.Start.AddDays(i);
                daysList.Add(day);
            }

            List<PerformanceReportRepositoryEntity> performanceReportRepositoryList = new List<PerformanceReportRepositoryEntity>();

            if (frontendInput.AccountCode != null & frontendInput.AccountCode != " " & frontendInput.AccountCode != "*")         //若同時選站所與客代，則優先判定客代
            {
                performanceReportRepositoryList = PerformanceReportRepository.PerformanceReportRepositoryByAccountCodeList(frontendInput);
            }
            else if ((frontendInput.AccountCode == " " || frontendInput.AccountCode is null || frontendInput.AccountCode == "*") & frontendInput.SendStation != null & frontendInput.SendStation != " " & frontendInput.SendStation != "未選擇")
            {
                performanceReportRepositoryList = PerformanceReportRepository.PerformanceReportRepositoryBySendStationList(frontendInput);
            }

            foreach (var i in performanceReportRepositoryList)              //將日期只保留年月日，寫入PrintDate
            {
                i.PrintDate = DateTime.Parse(i.ShipDate.ToString().Split(" ")[0]);
                i.DetailEntity.DriverName = i.DetailEntity.DriverCode + i.DetailEntity.DriverName;
                if (i.DetailEntity.ShipDate != null)
                {
                    string[] str = i.DetailEntity.ShipDate.ToString().Split(' ');
                    i.DetailEntity.GiveDate = str[0] + " " + str[2];
                }
                else
                {
                    i.DetailEntity.GiveDate = "查無紀錄";
                }
            }

            foreach (var i in performanceReportRepositoryList)
            {
                var isExist = false;                            //判別客戶是否已存在於accountList
                foreach (var j in accountsList)
                {
                    if (i.AccountCode == j.AccountCode)
                    {
                        isExist = true;
                    }
                }
                if (isExist == false)
                {
                    AccountEntity account = new AccountEntity()
                    {
                        AccountCode = i.AccountCode,
                        AccountName = i.UserName
                    };
                    accountsList.Add(account);
                }
            }

            foreach (var i in accountsList)                                     //將最終顯示的左側客戶代碼與客戶名稱先輸入，其餘皆0
            {
                List<int> emptyIntList = new List<int>();
                List<List<PerformanceReportDetailEntity>> emptyDetailDoubleList = new List<List<PerformanceReportDetailEntity>>();
                number += 1;
                for (var j = 0; j < days; j++)
                {
                    emptyIntList.Add(0);
                    List<PerformanceReportDetailEntity> emptyDetailList = new List<PerformanceReportDetailEntity>();
                    emptyDetailDoubleList.Add(emptyDetailList);
                }

                PerformanceReportEntity piece = new PerformanceReportEntity()
                {
                    Index = number,
                    AccountCode = i.AccountCode,
                    UserName = i.AccountName,
                    PiecesPerDayList = emptyIntList,
                    AveragePerDay = 0,
                    Sum = 0,
                    DetailPerDayList = emptyDetailDoubleList,
                };
                performanceReportList.Add(piece);
            }

            foreach (var i in performanceReportRepositoryList)
            {
                foreach (var j in performanceReportList)
                {
                    if (i.AccountCode == j.AccountCode)
                    {
                        for (var k = 0; k < days; k++)
                        {
                            if (i.PrintDate == daysList[k])
                            {
                                j.PiecesPerDayList[k] += 1;
                                j.Sum += 1;
                                j.DetailPerDayList[k].Add(i.DetailEntity);
                                break;
                            }
                        }
                        break;
                    }
                }
            }

            foreach (var i in performanceReportList)
            {
                i.AveragePerDay = i.Sum / days;
            }

            if (performanceReportList.Count == 0)                       //若沒有任何交易紀錄，則回傳查無紀錄
            {
                List<int> emptyIntList = new List<int>();
                PerformanceReportEntity emptyEntity = new PerformanceReportEntity()
                {
                    AccountCode = "查無紀錄",
                    UserName = "查無紀錄",
                    PiecesPerDayList = emptyIntList,
                    AveragePerDay = 0,
                    Sum = 0
                };
                performanceReportList.Add(emptyEntity);
            }

            return performanceReportList;
        }

        public List<AccountEntity> GetAllAccount(AccountEntity accountEntity)
        {
            List<AccountEntity> accounts = new List<AccountEntity>();
            if (accountEntity.IsMaster)
            {
                accounts = PerformanceReportRepository.MasterGetAccountEntities(accountEntity.AccountCode.Substring(0, 3));
            }
            else
            {
                accounts = PerformanceReportRepository.GetAccountEntities(accountEntity.AccountCode.Substring(0, 3));
            }

            return accounts;
        }

        public List<AccountEntity> GetAccountByStation(string station)
        {
            List<AccountEntity> accounts = PerformanceReportRepository.GetAccountEntities(station);

            return accounts;
        }

        private string GetAccountCodeAndStationInString(string str)
        {
            int WhereIsTheF = str.IndexOf("F");
            string result = "";
            if (WhereIsTheF != -1)
            {
                str = str.Substring(WhereIsTheF);
                List<string> tempStringList = new List<string>();
                for (var i = 0; i < str.Length; i++)
                {
                    tempStringList.Add(str.Substring(i, 1));
                }
                byte[] bytesArray = Encoding.Default.GetBytes(str);

                for (var i = 0; i < bytesArray.Length; i++)
                {
                    if ((bytesArray[i] > 47 & bytesArray[i] < 58) || bytesArray[i] == 70)                 //70為F，判定式前面為數字
                    {
                        result += tempStringList[i];
                    }
                    else
                    {
                        break;
                    }
                }
            }
            return result;
        }
    }
}
