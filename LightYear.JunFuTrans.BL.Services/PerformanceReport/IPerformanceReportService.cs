﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.PerformanceReport;
using Microsoft.AspNetCore.SignalR;

namespace LightYear.JunFuTrans.BL.Services.PerformanceReport
{
    public interface IPerformanceReportService
    {
        List<PerformanceReportEntity> PerformanceReportList(FrontendInputEntity frontendInput);

        List<AccountEntity> GetAllAccount(AccountEntity accountEntity);

        AccountEntity GetAccountData(AccountEntity accountCode);

        List<AccountEntity> GetAccountByStation(string station);
    }
}
