﻿using LightYear.JunFuTrans.BL.BE.DeliveryAndDeliveryMating;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.DeliveryAndDeliveryMating
{
    public interface IDeliveryAndDeliveryMatingService
    {
        List<DeliveryAndDeliveryMatingEntity> GetAll(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption);
        List<ArriveOptionEntity> GetArriveOptions();
        List<DriversEntity> GetAllDrivers();
        int GetCountPieces(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption);
        int GetCountArriveOptions(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption);
        List<DriversEntity> GetDriversByStation(string stationscode);
    }
}
