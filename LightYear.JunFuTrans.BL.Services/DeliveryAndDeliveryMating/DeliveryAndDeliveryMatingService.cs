﻿using LightYear.JunFuTrans.BL.BE.DeliveryAndDeliveryMating;
using LightYear.JunFuTrans.DA.Repositories.DeliveryAndDeliveryMating;
using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.Services.Barcode;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Reflection;
using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.DA.Repositories.Account;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.BL.Services.DeliveryAndDeliveryMating
{
    public class DeliveryAndDeliveryMatingService : IDeliveryAndDeliveryMatingService
    {
        public IDeliveryAndDeliveryMatingRepository DeliveryAndDeliveryMatingRepository { get; set; }
        public IMapper Mapper { get; private set; }
        public IScanItemService ScanItemService { get; set; }
        public IDriverRepository DriverRepository { get; set; }

        public DeliveryAndDeliveryMatingService(IDeliveryAndDeliveryMatingRepository deliveryAndDeliveryMatingRepository, IMapper mapper, IScanItemService scanItemService, IDriverRepository driverRepository)
        {
            this.DeliveryAndDeliveryMatingRepository = deliveryAndDeliveryMatingRepository;
            this.Mapper = mapper;
            ScanItemService = scanItemService;
            DriverRepository = driverRepository;
        }

        public List<DeliveryAndDeliveryMatingEntity> GetAll(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption)
        {
            List<DeliveryAndDeliveryMatingEntity> deliveryAndDeliveryMatingEntities = new List<DeliveryAndDeliveryMatingEntity>();
            IEnumerable<TbItemCode> itemCodes = DeliveryAndDeliveryMatingRepository.GetAllTbItemCodesWhichSclassAO();

            if (driverCode.Equals("all"))
            {
                deliveryAndDeliveryMatingEntities = DeliveryAndDeliveryMatingRepository.GetAllByStationScode(start, end, stationscode, arriveOption);
            }
            else
            {
                deliveryAndDeliveryMatingEntities = DeliveryAndDeliveryMatingRepository.GetAllByDriverCode(start, end, driverCode, arriveOption);
            }

            int count = 0;
            foreach (var entity in deliveryAndDeliveryMatingEntities)
            {
                SubpoenaCategoryConfig subpoenaCategoryConfig = (SubpoenaCategoryConfig)Convert.ToInt32(entity.SubpoenaCategory);
                entity.SubpoenaCategory = ScanItemService.GetEnumDescription(subpoenaCategoryConfig);

                entity.ArriveOption = itemCodes.FirstOrDefault(item => item.CodeId == entity.ArriveOption) != null ? itemCodes.Where(item => item.CodeId == entity.ArriveOption).FirstOrDefault().CodeName : "尚未配達";

                var driverName = DriverRepository.GetDriverNameByDriverCode(entity.DriverNameCode);
                entity.DriverNameCode += driverName;

                count++;
                entity.Id = count;

            }

            if (deliveryAndDeliveryMatingEntities.Count() > 1)
            {
                deliveryAndDeliveryMatingEntities[deliveryAndDeliveryMatingEntities.Count() - 1].ExcelSum = "合計代收貨款";
                deliveryAndDeliveryMatingEntities[deliveryAndDeliveryMatingEntities.Count() - 1].ListSum = GetSumOfCollectionMoney(deliveryAndDeliveryMatingEntities);
            }
            return deliveryAndDeliveryMatingEntities;
        }

        public int GetCountPieces(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption)
        {
            return DeliveryAndDeliveryMatingRepository.GetCountPieces(start, end, driverCode, stationscode, arriveOption);
        }
        public int GetCountArriveOptions(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption)
        {
            return DeliveryAndDeliveryMatingRepository.GetCountArriveOptions(start, end, driverCode, stationscode, arriveOption);
        }
        public List<ArriveOptionEntity> GetArriveOptions()
        {
            List<ArriveOptionEntity> arriveOptionEntities = DeliveryAndDeliveryMatingRepository.GetArriveOptions();
            ArriveOptionEntity a = new ArriveOptionEntity();
            a.CodeId = "all";
            a.CodeName = "全選";
            List<ArriveOptionEntity> arr = new List<ArriveOptionEntity>();
            arr.Add(a);  //加了一個選項
            foreach (var entity in arriveOptionEntities)
            {
                arr.Add(entity); //arr List原本只有一個全選選項，加入其他已有的選項
            }
            return arr;
        }
        public List<DriversEntity> GetAllDrivers()
        {
            var data = DeliveryAndDeliveryMatingRepository.GetAllDrivers();
            var dataInfo = this.Mapper.Map<List<DriversEntity>>(data);
            dataInfo.Insert(0, new DriversEntity() { DriverCode = "all", DriverName = "全選" });
            return dataInfo;
        }

        public List<DriversEntity> GetDriversByStation(string stationscode)
        {
            var data = DeliveryAndDeliveryMatingRepository.GetDriversByStation(stationscode);
            var dataInfo = this.Mapper.Map<List<DriversEntity>>(data);
            dataInfo.Insert(0, new DriversEntity() { DriverCode = "all", DriverName = "全選" });
            return dataInfo;
        }

        private int GetSumOfCollectionMoney(List<DeliveryAndDeliveryMatingEntity> entities)
        {
            int result = 0;
            for (int i = 0; i < entities.Count(); i++)
            {
                result += entities[i].CollectionMoney;
            }
            return result;
        }
    }
}
