﻿using LightYear.JunFuTrans.BL.BE.DriverShiftSchedules;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.DriverShiftSchedules
{
    public interface IDriverShiftSchedulesService
    {
        List<InputEntity> GetAllByStationAndDate(string scode, DateTime date);

        List<DriverShiftArrangement> GetAllByStationScode(string scode);

        void Delete(int id);

        void Save(InputEntity inputEntity);

        int GetDriversCount(string scode);

        int GetTakeOffDriversCount(string scode, DateTime date);
    }
}
