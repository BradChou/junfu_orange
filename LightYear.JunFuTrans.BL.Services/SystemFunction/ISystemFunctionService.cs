﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.Menu;
using LightYear.JunFuTrans.BL.BE.Account;

namespace LightYear.JunFuTrans.BL.Services.SystemFunction
{
    public interface ISystemFunctionService
    {
        List<SystemCategoryEntity> GetCategoryEntities(string company);

        FrontendMenuEntity GetCategoryEntitiesByUserInfo(UserInfoEntity userInfoEntity);

        List<FunctionFrontendSettingEntity> GetFrontendFunctionInfos(int roleId, string company);

        List<FunctionFrontendSettingEntity> GetFrontendUserFunctionInfos(int userId, int accountType, string company = "");

        void SaveRoleFunction(int roleId, int[] funcitonId);

        void SaveUserFunction(int userId, int accountType, int[] funcitonId);

    }
}
