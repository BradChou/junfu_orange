﻿using AutoMapper;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.BE.DeliveryRate;
using LightYear.JunFuTrans.BL.BE.StationArea;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.Repositories.Station;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.StationWithArea
{
    public class StationAreaService : IStationAreaService
    {
        public IStationAreaRepository StationAreaRepository { get; set; }

        public IStationRepository StationRepository { get; set; }

        public IOrgAreaRepository OrgAreaRepository { get; set; }

        public IMapper Mapper { get; set; }

        public StationAreaService(IStationAreaRepository stationAreaRepository, IStationRepository stationRepository, IOrgAreaRepository orgAreaRepository, IMapper mapper)
        {
            StationAreaRepository = stationAreaRepository;
            StationRepository = stationRepository;
            OrgAreaRepository = orgAreaRepository;
            Mapper = mapper;
        }

        public List<StationAreaEntity> GetAll()
        {
            List<TbStation> stationList = StationRepository.GetAll();
            List<StationAreaEntity> stationAreasList = Mapper.Map<List<StationAreaEntity>>(stationList);
            foreach(var s in stationAreasList)
            {
                s.Id = 0;
                var savedData = StationAreaRepository.GetByStationScode(s.StationSCode);
                if(savedData != null)
                {
                    s.Id = savedData.Id;
                    s.Area = savedData.Area;
                }                
            }
            return stationAreasList;
        }

        public StationAreaEntity GetStationAreaById(int id)
        {
            var stationArea = StationAreaRepository.GetStationAreaById(id);
            return Mapper.Map<StationAreaEntity>(stationArea);
        }
        public StationAreaEntity GetByStationScode(string stationScode)
        {
            var stationArea = StationAreaRepository.GetByStationScode(stationScode);
            if (stationArea == null)
            {
                return new StationAreaEntity();
            }
            else
            {
                return Mapper.Map<StationAreaEntity>(stationArea);
            }
        }

        public StationAreaEntity Insert(StationAreaEntity stationAreaEntity)
        {
            stationAreaEntity.CreateDate = DateTime.Now;
            stationAreaEntity.UpdateDate = DateTime.Now;

            StationArea stationArea = Mapper.Map<StationArea>(stationAreaEntity);
            int id = StationAreaRepository.Insert(stationArea);
            stationAreaEntity.Id = id;

            return stationAreaEntity;
        }

        public void Update(StationAreaEntity stationAreaEntity)
        {
            if (stationAreaEntity.Id == 0)
            {
                Insert(stationAreaEntity);
                return;
            }
            stationAreaEntity.UpdateDate = DateTime.Now;
            StationArea stationArea = Mapper.Map<StationArea>(stationAreaEntity);
            StationAreaRepository.Update(stationArea);
        }

        public void Delete(int id)
        {
            StationAreaRepository.Delete(id);
        }

        public KendoSelectAreaEntity GetAreaList(bool addTotal = false)
        {
            List<string> areas = StationAreaRepository.GetAreaList();

            if (addTotal)
            {
                areas.Insert(0, "全選");
            }

            return new KendoSelectAreaEntity { results = areas };
        }

        public KendoSelectAreaEntity GetAreaListFromTbStation(bool addTotal = false)
        {
            List<string> areas = StationRepository.GetAreaList();

            if (addTotal)
            {
                areas.Insert(0, "全選");
            }

            return new KendoSelectAreaEntity { results = areas };
        }

        public KendoSelectStationEntity GetStarWith(string startWith, bool addTotal = false, bool isNeedAllStation = true)
        {
            List<TbStation> tbStations = new List<TbStation>();

            //沒資料時抓全部，否則抓開頭資料
            if (startWith == string.Empty)
            {
                tbStations = this.StationRepository.GetAll();
            }
            else
            {
                tbStations = this.StationRepository.GetStartWith(startWith);
            }

            var stationEntities = this.Mapper.Map<List<StationAreaEntity>>(tbStations);

            if( addTotal )
            {
                StationAreaEntity emptyStationAreaEntity = new StationAreaEntity
                {
                    StationCode = string.Empty,
                    StationName = string.Empty,
                    ShowName = "未選擇"
                };

                StationAreaEntity stationAreaEntity = new StationAreaEntity
                {
                    StationCode = "-1",
                    StationSCode = "-1",
                    StationName = string.Empty,
                    ShowName = "全部站所"
                };

                stationEntities.Insert(0, emptyStationAreaEntity);

                if (isNeedAllStation)
                {
                    stationEntities.Insert(1, stationAreaEntity);
                }
            }

            KendoSelectStationEntity kendoSelectStationEntity  = new KendoSelectStationEntity
            {
                results = stationEntities,
                __count = stationEntities.Count
            };

            return kendoSelectStationEntity;
        }

        public KendoSelectStationEntity GetStationsByAreaName(string areaName, bool addAll = true)
        {
            List<StationArea> stationAreas = StationAreaRepository.GetStationsByAreaName(areaName);
            KendoSelectStationEntity kendoSelectStationEntity = new KendoSelectStationEntity();
            kendoSelectStationEntity.results = Mapper.Map<List<StationAreaEntity>>(stationAreas);

            if(addAll)
                kendoSelectStationEntity.results.Insert(0, new StationAreaEntity
                {
                    ShowName = "區域內全選",
                    StationCode = "0",
                    StationSCode = "0"
                });

            return kendoSelectStationEntity;
        }

        public KendoSelectStationEntity GetStationsByAreaNameFromTbStation(string areaName, bool addAll = true)
        {
            List<TbStation> stationAreas = StationRepository.GetStationByArea(areaName);
            KendoSelectStationEntity kendoSelectStationEntity = new KendoSelectStationEntity();
            kendoSelectStationEntity.results = Mapper.Map<List<StationAreaEntity>>(stationAreas);

            if (addAll)
                kendoSelectStationEntity.results.Insert(0, new StationAreaEntity
                {
                    ShowName = "區域內全選",
                    StationCode = "0",
                    StationSCode = "0"
                });

            return kendoSelectStationEntity;
        }

        public List<Post5CodeMappingEntity> GetPost5CodeMappingEntityByStationSCode(string stationSCode)
        {
            var data = this.OrgAreaRepository.GetByStationSCode(stationSCode);
            List<Post5CodeMappingEntity> post5CodeMappingEntities = Mapper.Map<List<Post5CodeMappingEntity>>(data);

            return post5CodeMappingEntities;
        }

        public void SavePost5CodeMappingEntity(Post5CodeMappingEntity post5CodeMappingEntity)
        {
            OrgArea orgArea = Mapper.Map<OrgArea>(post5CodeMappingEntity);

            this.OrgAreaRepository.Update(orgArea);
        }

        public KendoSelectStationEntity GetStationsByStationSCode(string stationSCode)
        {
            List<StationAreaEntity> stationAreas = new List<StationAreaEntity>();
            TbStation tbStation = this.StationRepository.GetByScode(stationSCode);

            StationAreaEntity stationAreaEntityFirst = new StationAreaEntity
            {
                StationCode = string.Empty,
                StationName = string.Empty,
                ShowName = "未選擇"
            };

            stationAreas.Add(stationAreaEntityFirst);

            if ( tbStation != null )
            {
                StationAreaEntity stationAreaEntity = new StationAreaEntity
                {
                    StationCode = tbStation.Id.ToString(),
                    StationSCode = tbStation.StationScode,
                    StationName = tbStation.StationName,
                    ShowName = tbStation.StationName
                };

                stationAreas.Add(stationAreaEntity);
            }

            KendoSelectStationEntity kendoSelectStationEntity = new KendoSelectStationEntity();
            kendoSelectStationEntity.results = stationAreas;


            return kendoSelectStationEntity;
        }

        public void BatchChangOrgData(List<int> ids, string dataValue, string type)
        {
            switch(type)
            {
                case "1":
                    this.OrgAreaRepository.BatchUpdateSD(ids, dataValue);
                    break;
                case "2":
                    this.OrgAreaRepository.BatchUpdateMD(ids, dataValue);
                    break;
                case "3":
                    this.OrgAreaRepository.BatchUpdatePutOrder(ids, dataValue);
                    break;
            }
        }
    }
}
