﻿using AutoMapper;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.BE.BusinessReport;
using LightYear.JunFuTrans.BL.BE.DeliveryRequest;
using LightYear.JunFuTrans.BL.BE.DriverToStationPayment;
using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.BL.BE.Statics;
using LightYear.JunFuTrans.BL.BE.StationArea;
using LightYear.JunFuTrans.BL.Services.Barcode;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.Repositories.Account;
using LightYear.JunFuTrans.DA.Repositories.Customer;
using LightYear.JunFuTrans.DA.Repositories.DeliveryException;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRequest;
using LightYear.JunFuTrans.DA.Repositories.Station;
using LightYear.JunFuTrans.Utilities.Reports;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.DeliveryRequest
{
    public class DeliveryRequestService : IDeliveryRequestService
    {

        public IDeliveryRequestRepository DeliveryRequestRepository { get; set; }

        public IDeliveryScanLogRepository DeliveryScanLogRepository { get; set; }

        public IDriverRepository DriverRepository { get; set; }

        public IEmpRepository EmpRepository { get; set; }

        public IStationRepository StationRepository { get; set; }

        public ICustomerRepository CustomerRepository { get; set; }

        public IDeliveryExceptionRepository DeliveryExceptionRepository { get; set; }

        public IWriteOffRepository WriteOffRepository { get; set; }

        public IApiTokenMappingRepository ApiTokenMappingRepository { get; set; }

        public IReportHelper ReportHelper { get; private set; }

        public ICheckNumberSDMappingRepository CheckNumberSDMappingRepository { get; set; }

        public IOrgAreaRepository OrgAreaRepository { get; set; }

        public IScanItemService ScanItemService { get; set; }

        public IMapper Mapper { get; private set; }

        public ICustomizeAreaArriveCodeRepository CustomizeAreaArriveCodeRepository { get; set; }

        public IPrintTempInfoRepository PrintTempInfoRepository { get; set; }

        public DeliveryRequestService(
            IDeliveryRequestRepository deliveryRequestRepository,
            IDeliveryScanLogRepository deliveryScanLogRepository,
            IDriverRepository driverRepository,
            IEmpRepository empRepository,
            IStationRepository stationRepository,
            ICustomerRepository customerRepository,
            IDeliveryExceptionRepository deliveryExceptionRepository,
            IWriteOffRepository writeOffRepository,
            IApiTokenMappingRepository apiTokenMappingRepository,
            IReportHelper reportHelper,
            ICheckNumberSDMappingRepository checkNumberSDMappingRepository,
            IOrgAreaRepository orgAreaRepository,
            IMapper mapper,
            IScanItemService scanItemService,
            ICustomizeAreaArriveCodeRepository customizeAreaArriveCodeRepository,
            IPrintTempInfoRepository printTempInfoRepository
            )
        {
            this.DeliveryRequestRepository = deliveryRequestRepository;
            this.DeliveryScanLogRepository = deliveryScanLogRepository;
            this.DriverRepository = driverRepository;
            this.EmpRepository = empRepository;
            this.StationRepository = stationRepository;
            this.CustomerRepository = customerRepository;
            this.DeliveryExceptionRepository = deliveryExceptionRepository;
            this.WriteOffRepository = writeOffRepository;
            this.ApiTokenMappingRepository = apiTokenMappingRepository;
            this.ReportHelper = reportHelper;
            this.CheckNumberSDMappingRepository = checkNumberSDMappingRepository;
            this.OrgAreaRepository = orgAreaRepository;
            this.Mapper = mapper;
            ScanItemService = scanItemService;
            CustomizeAreaArriveCodeRepository = customizeAreaArriveCodeRepository;
            this.PrintTempInfoRepository = printTempInfoRepository;
        }

        /// <summary>
        /// 由託運單號取得託運資料
        /// </summary>
        /// <param name="checkNumber"></param>
        /// <returns></returns>
        public DeliveryRequestEntity GetDeliveryRequest(string checkNumber)
        {
            var data = this.DeliveryRequestRepository.GetByCheckNumber(checkNumber);

            if (data == null)
            {
                return null;
            }
            else
            {
                var deliveryRequestEntity = this.Mapper.Map<DeliveryRequestEntity>(data);

                //取得最新的狀態
                List<DeliveryScanLogEntity> deliveryScanLogEntities = this.GetScanLog(checkNumber);

                if (deliveryScanLogEntities.Count > 0)
                {
                    deliveryRequestEntity.ScanLogs = deliveryScanLogEntities;

                    deliveryRequestEntity.CurrentStatus = deliveryScanLogEntities[0].ScanStatus;

                    deliveryRequestEntity.LastScanDateTime = deliveryScanLogEntities[0].ScanDate;

                    switch (deliveryRequestEntity.CurrentStatus)
                    {
                        case 1:
                            deliveryRequestEntity.CurrentStatusName = "到著";
                            break;
                        case 2:
                            deliveryRequestEntity.CurrentStatusName = "配送";
                            break;
                        case 3:
                            deliveryRequestEntity.CurrentStatusName = "配達";
                            break;
                        case 4:
                            deliveryRequestEntity.CurrentStatusName = "配達";
                            break;
                        case 5:
                            deliveryRequestEntity.CurrentStatusName = "集貨";
                            break;
                        case 6:
                            deliveryRequestEntity.CurrentStatusName = "卸集";
                            break;
                        case 7:
                            deliveryRequestEntity.CurrentStatusName = "發送";
                            break;
                        default:
                            deliveryRequestEntity.CurrentStatusName = "不明";
                            break;
                    }
                }
                else
                {
                    deliveryRequestEntity.CurrentStatusName = "未掃讀";
                }

                return deliveryRequestEntity;
            }
        }

        /// <summary>
        /// 由託運單號取得掃讀歷程
        /// </summary>
        /// <param name="checkNumber"></param>
        /// <returns></returns>
        public List<DeliveryScanLogEntity> GetScanLog(string checkNumber)
        {
            var data = this.DeliveryScanLogRepository.GetByCheckNumber(checkNumber);

            var deliveryScanLogEntities = this.Mapper.Map<List<DeliveryScanLogEntity>>(data);

            this.UpdateRealScanStation(ref deliveryScanLogEntities);

            return deliveryScanLogEntities;
        }

        private TbStation GetDriverRealStation(string driverCode)
        {
            TbStation tbStation = null;

            var driver = this.DriverRepository.GetByDriverCode(driverCode);

            if (driver != null && driver.EmpCode != null)
            {
                var emp = this.EmpRepository.GetByEmpCode(driver.EmpCode);

                if (emp != null && emp.Station != null && emp.Station.Length > 0)
                {
                    try
                    {
                        int stationId = Convert.ToInt32(emp.Station.Trim());
                        tbStation = this.StationRepository.GetById(stationId);
                    }
                    catch
                    {

                    }
                }
                else if (driver.Station != null)
                {
                    try
                    {
                        tbStation = this.StationRepository.GetByScode(driver.Station);
                    }
                    catch
                    {
                    }
                }
            }

            return tbStation;
        }

        /// <summary>
        /// 更新真實的掃讀站所資訊
        /// </summary>
        /// <param name="deliveryScanLogEntities"></param>
        private void UpdateRealScanStation(ref List<DeliveryScanLogEntity> deliveryScanLogEntities)
        {
            foreach (DeliveryScanLogEntity deliveryScanLogEntity in deliveryScanLogEntities)
            {
                var station = GetDriverRealStation(deliveryScanLogEntity.DriverCode);

                if (station != null)
                {
                    deliveryScanLogEntity.RealScanStation = station.StationScode;
                }

            }
        }

        public List<DeliveryRequestStaticEntity> GetPassWeeksStaticsData(int weeks)
        {
            List<DeliveryRequestStaticEntity> deliveryRequestStaticEntities = new List<DeliveryRequestStaticEntity>();

            for (int i = 0; i < weeks; i++)
            {
                DateTime dtEnd = DateTime.Today.AddDays(-(i * 7));
                DateTime dtStart = DateTime.Today.AddDays(-((i + 1) * 7));

                int nTotalCount = this.DeliveryRequestRepository.GetAllCount(dtStart, dtEnd);
                int nMomoCount = this.DeliveryRequestRepository.GetAllCountWithCustomer(dtStart, dtEnd, "F3500010002");
                int nNonMomo = nTotalCount - nMomoCount;
                string periodStr = string.Format("{0}~{1}", dtStart.ToShortDateString(), dtEnd.ToShortDateString());

                DeliveryRequestStaticEntity deliveryRequestStaticEntity = new DeliveryRequestStaticEntity
                {
                    TotalCount = nTotalCount,
                    MomoCount = nMomoCount,
                    NonMomoCount = nNonMomo,
                    ZoneString = periodStr
                };

                deliveryRequestStaticEntities.Add(deliveryRequestStaticEntity);
            }

            return deliveryRequestStaticEntities;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stationScode"></param>
        /// <param name="deliveryDate"></param>
        /// <returns></returns>
        public int GetDPlusOneShouldDeliveryCount(string stationScode, DateTime deliveryDate)
        {
            if (stationScode.Length > 0)
            {
                //先取得站所資料
                var station = this.StationRepository.GetByScode(stationScode);

                int totalCount = this.GetDPlusOneShouldDeliveryList(stationScode, deliveryDate).Count();

                return totalCount;
            }
            else
            {
                return 0;
            }

        }

        /// <summary>
        /// 取得配送日後計算前一日出貨，該配送站所之 D+1 配達率應配筆數List
        /// </summary>
        /// <param name="stationScode"></param>
        /// <param name="deliveryDate"></param>
        /// <returns></returns>
        /// <summary>
        public List<TcDeliveryRequest> GetDPlusOneShouldDeliveryList(string stationScode, DateTime deliveryDate)
        {
            DateTime scanDeadLine = deliveryDate.AddDays(1);
            Dictionary<TcDeliveryRequest, string> data = DeliveryRequestRepository.GetDPlusOneDeliveryDictionary(stationScode, deliveryDate);
            List<TcDeliveryRequest> result = new List<TcDeliveryRequest>();

            foreach (KeyValuePair<TcDeliveryRequest, string> item in data)
            {
                if (item.Value == item.Key.AreaArriveCode && item.Key.LatestDestDate < scanDeadLine)
                {
                    result.Add(item.Key);
                }
                else
                {
                    if (item.Key.LatestDeliveryDate < scanDeadLine)
                    {
                        result.Add(item.Key);
                    }
                    else
                    {
                        if (item.Key.LatestScanItem == "3" && item.Key.LatestScanDate < scanDeadLine)
                        {
                            result.Add(item.Key);
                        }
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// 取得配送日後計算前一日出貨，該配送站所之 D+1 配達率配達筆數
        /// </summary>
        /// <param name="stationScode"></param>
        /// <param name="deliveryDate"></param>
        /// <returns></returns>
        public int GetDPlusOneSuccessDeliveryCount(string stationScode, DateTime deliveryDate)
        {
            if (stationScode.Length > 0)
            {
                //先取得站所資料
                var station = this.StationRepository.GetByScode(stationScode);

                int totalCount = this.GetDPlusOneSuccessDeliveryList(stationScode, deliveryDate).Count();

                return totalCount;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// 取得配送日後計算前一日出貨，該配送站所之 D+1 配達率配達筆數List
        /// </summary>
        /// <param name="stationSCode"></param>
        /// <param name="deliveryDate"></param>
        /// <returns></returns>
        public List<TcDeliveryRequest> GetDPlusOneSuccessDeliveryList(string stationScode, DateTime deliveryDate)
        {
            DateTime scanDeadLine = deliveryDate.AddDays(1);
            var shouldDeliveryList = this.GetDPlusOneShouldDeliveryList(stationScode, deliveryDate);
            List<TcDeliveryRequest> result = new List<TcDeliveryRequest>();

            foreach (TcDeliveryRequest request in shouldDeliveryList)
            {
                if (request.LatestScanDate < scanDeadLine && request.LatestScanItem == "3" && request.LatestScanArriveOption == "3")
                {
                    result.Add(request);
                }
            }

            return result;
        }

        /// <summary>
        /// 取得配送日後計算前一日出貨，該配送站所之 D+1 配達率未配達筆數List
        /// </summary>
        /// <param name="stationSCode">站所的scode</param>
        /// <param name="shipDate">寄送日</param>
        /// <returns></returns>
        public List<DashboardDeliveryRequestFrontendShowEntity> GetDPlusOneNotSuccessDeliveryList(string stationScode, DateTime deliveryDate)
        {
            if (stationScode.Length > 0)
            {
                var dataByScode = GetDPlusOneShouldDeliveryList(stationScode, deliveryDate);
                var dataByScodeSuccess = GetDPlusOneSuccessDeliveryList(stationScode, deliveryDate);
                IEnumerable<TcDeliveryRequest> differenceQuery = dataByScode.Except(dataByScodeSuccess);
                List<DashboardDeliveryRequestFrontendShowEntity> result = new List<DashboardDeliveryRequestFrontendShowEntity>();
                foreach (TcDeliveryRequest request in differenceQuery)
                {
                    DashboardDeliveryRequestFrontendShowEntity entity = new DashboardDeliveryRequestFrontendShowEntity();
                    entity.Id = request.RequestId;
                    entity.CheckNumber = request.CheckNumber;
                    entity.ReceiverName = request.ReceiveContact;
                    entity.ReceiverTel1 = request.ReceiveTel1;
                    entity.ReceiverAddress = request.ReceiveArea + request.ReceiveAddress;
                    entity.LatestScanStatus = request.LatestScanItem switch
                    {
                        "1" => "到著",
                        "2" => "配送",
                        "3" => "配達",
                        "4" => "配達",
                        "5" => "集貨",
                        "6" => "卸集",
                        "7" => "發送",
                        _ => "不明",
                    };
                    entity.LatestScanArriveOption = request.LatestScanArriveOption switch
                    {
                        "1" => "客戶不在",
                        "2" => "約定再配",
                        "3" => "正常配交",
                        "4" => "拒收",
                        "5" => "地址錯誤",
                        "6" => "查無此人",
                        _ => "",
                    };
                    entity.LatestScanDate = request.LatestScanDate.Value.ToString("yyyy/MM/dd HH:mm");

                    string DeliveryDriverCode = request.LatestDeliveryDriver != null ?
                                                request.LatestDeliveryDriver :
                                                request.LatestScanItem == "3" ? request.LatestScanDriverCode :
                                                "";

                    TbDriver DeliveryDriver = this.DriverRepository.GetByDriverCode(DeliveryDriverCode);
                    entity.DeliveryDriver = DeliveryDriver != null ?
                                            string.Format("{0}({1})", DeliveryDriver.DriverName, DeliveryDriver.DriverCode) :
                                            ".尚未配送";

                    result.Add(entity);
                }

                return result;

            }
            else
            {
                return new List<DashboardDeliveryRequestFrontendShowEntity>();
            }
        }

        /// <summary>
        /// 取得該站所留庫筆數List
        /// </summary>
        /// <param name="stationScode">站所的scode</param>
        /// <param name="DeadLineDate">結算日</param>
        /// <returns></returns>
        public Dictionary<DashboardDeliveryRequestFrontendShowEntity, int> GetDepotList(string stationScode, DateTime checkDate)
        {
            DateTime shipStartDate = checkDate.AddDays(-30);
            var data = this.DeliveryRequestRepository.GetDepotDeliveryList(stationScode, checkDate, shipStartDate);
            Dictionary<DashboardDeliveryRequestFrontendShowEntity, int> result = new Dictionary<DashboardDeliveryRequestFrontendShowEntity, int>();

            foreach (TcDeliveryRequest request in data)
            {
                DashboardDeliveryRequestFrontendShowEntity entity = new DashboardDeliveryRequestFrontendShowEntity();
                entity.Id = request.RequestId;
                entity.CheckNumber = request.CheckNumber;
                entity.ReceiverName = request.ReceiveContact;
                entity.ReceiverTel1 = request.ReceiveTel1;
                entity.ReceiverAddress = request.ReceiveArea + request.ReceiveAddress;
                entity.LatestScanStatus = request.LatestScanItem switch
                {
                    "1" => "到著",
                    "2" => "配送",
                    "3" => "配達",
                    "4" => "配達",
                    "5" => "集貨",
                    "6" => "卸集",
                    "7" => "發送",
                    _ => "不明",
                };
                entity.LatestScanArriveOption = request.LatestScanArriveOption switch
                {
                    "1" => "客戶不在",
                    "2" => "約定再配",
                    "3" => "正常配交",
                    "4" => "拒收",
                    "5" => "地址錯誤",
                    "6" => "查無此人",
                    _ => "",
                };
                entity.LatestScanDate = request.LatestScanDate.Value.ToString("yyyy/MM/dd HH:mm");

                string DeliveryDriverCode = request.LatestDeliveryDriver != null ?
                                            request.LatestDeliveryDriver :
                                            request.LatestScanItem == "3" ? request.LatestScanDriverCode :
                                            "";

                TbDriver DeliveryDriver = this.DriverRepository.GetByDriverCode(DeliveryDriverCode);
                entity.DeliveryDriver = DeliveryDriver != null ?
                                        string.Format("{0}({1})", DeliveryDriver.DriverName, DeliveryDriver.DriverCode) :
                                        ".尚未配送";

                //若最新配達區分非拒收，喪失，破損配達，即依據出貨時間與今天的時間差判斷屬於D+1,D+2,D+3留庫
                string[] closedArriveOptionArray = { "4", "8", "H" };
                DateTime shipDate = request.ShipDate ?? DateTime.Today;
                int dateDiff = (int)checkDate.Subtract(shipDate).TotalDays;

                if (!closedArriveOptionArray.Contains(request.LatestScanArriveOption) || request.LatestScanItem != "3")
                {
                    if (dateDiff == 1)
                    {
                        result[entity] = 1;
                    }
                    else if (dateDiff == 2)
                    {
                        result[entity] = 2;
                    }
                    else if (dateDiff > 2)
                    {
                        result[entity] = 3;
                    }
                    else
                    {
                        //dummy
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 取得該站所留庫筆數
        /// </summary>
        /// <param name="stationScode">站所的scode</param>
        /// <param name="depotCategory">留庫種類</param>
        /// <param name="DeadLineDate">結算日</param>
        /// <returns></returns>
        public List<DashboardDeliveryRequestFrontendShowEntity> GetDepotCategoryCount(string stationScode, DateTime checkDate, int depotCategory)
        {
            var data = GetDepotList(stationScode, checkDate);
            List<DashboardDeliveryRequestFrontendShowEntity> result = new List<DashboardDeliveryRequestFrontendShowEntity>();

            foreach (var entity in data.Keys)
            {
                if (data[entity] == depotCategory)
                {
                    result.Add(entity);
                }
            }

            return result;
        }
        /// <summary>
        /// 取得該站所留庫筆數
        /// </summary>
        /// <param name="stationScode">站所的scode</param>
        /// <param name="checkDate">結算日</param>
        /// <returns></returns>
        public Dictionary<int, int> GetDepotCount(string stationScode, DateTime checkDate)
        {
            Dictionary<int, int> result = new Dictionary<int, int>();
            if (stationScode.Length > 0)
            {
                var depotCategoryGroup = GetDepotList(stationScode, checkDate);
                int depot1 = 0;
                int depot2 = 0;
                int depot3 = 0;
                foreach (var entity in depotCategoryGroup.Keys)
                {
                    switch (depotCategoryGroup[entity])
                    {
                        case 1:
                            ++depot1;
                            break;
                        case 2:
                            ++depot2;
                            break;
                        case 3:
                            ++depot3;
                            break;
                        default:
                            break;
                    }
                }

                result[1] = depot1;
                result[2] = depot2;
                result[3] = depot3;

            }
            else
            {
                result[1] = 0;
                result[2] = 0;
                result[3] = 0;
            }

            return result;
        }

        /// </summary>
        /// <param name="stationSCode"></param>
        /// <param name="shipDate"></param>
        /// <returns></returns>
        public int GetShouldDeliverCount(string stationSCode, DateTime shipDate)
        {
            if (stationSCode.Length > 0)
            {
                //先取得站所資料
                var station = this.StationRepository.GetByScode(stationSCode);

                int countByScode = this.DeliveryRequestRepository.GetCountByArriveCodeAndDate(stationSCode, shipDate);
                int countByCode = this.DeliveryRequestRepository.GetCountByArriveCodeAndDate(station.StationCode, shipDate);

                int totalCount = countByCode + countByScode;
                return totalCount;
            }
            else
            {
                return 0;
            }
        }

        public int GetActualDeliverCount(string stationSCode, DateTime shipDate)
        {
            if (stationSCode.Length > 0)
            {

                //先取得站所資料
                var station = this.StationRepository.GetByScode(stationSCode);

                //取得一日配成功資料
                int countByScode = this.DeliveryRequestRepository.GetSuccessCountByArriveCodeAndDate(stationSCode, shipDate, 1);
                int countByCode = this.DeliveryRequestRepository.GetSuccessCountByArriveCodeAndDate(station.StationCode, shipDate, 1);

                int totalCount = countByCode + countByScode;

                return totalCount;
            }
            else
            {
                return 0;
            }
        }

        public List<DeliveryRequestEntity> GetShouldDeliver(string stationSCode, DateTime shipDate)
        {
            if (stationSCode.Length > 0)
            {
                //先取得站所資料
                var station = this.StationRepository.GetByScode(stationSCode);

                var dataByScode = this.DeliveryRequestRepository.GetListByArriveCodeAndDate(stationSCode, shipDate);
                var dataByCode = this.DeliveryRequestRepository.GetListByArriveCodeAndDate(station.StationCode, shipDate);

                dataByCode.AddRange(dataByScode);

                List<DeliveryRequestEntity> deliveryRequestEntities = this.Mapper.Map<List<DeliveryRequestEntity>>(dataByCode);

                return deliveryRequestEntities.OrderBy(o => o.ShipDate).ToList();
            }
            else
            {
                return new List<DeliveryRequestEntity>();
            }
        }

        public List<DeliveryRequestFrontendShowEntity> GetNotDeliverList(string stationSCode, DateTime shipDate, int days)
        {
            if (stationSCode.Length > 0)
            {
                //先取得站所資料
                var station = this.StationRepository.GetByScode(stationSCode);

                //應配資料
                var dataByScode = this.DeliveryRequestRepository.GetListByArriveCodeAndDate(stationSCode, shipDate);
                var dataByCode = this.DeliveryRequestRepository.GetListByArriveCodeAndDate(station.StationCode, shipDate);

                dataByCode.AddRange(dataByScode);

                //應配中成功配得資料
                var dataByScodeSuccess = this.DeliveryRequestRepository.GetSuccessListByArriveCodeAndDate(stationSCode, shipDate, days);
                var dataByCodeSuccess = this.DeliveryRequestRepository.GetSuccessListByArriveCodeAndDate(station.StationCode, shipDate, days);

                dataByCodeSuccess.AddRange(dataByScodeSuccess);

                IEnumerable<TcDeliveryRequest> differenceQuery = dataByCode.Except(dataByCodeSuccess);

                List<DeliveryRequestEntity> deliveryRequestEntities = this.Mapper.Map<List<DeliveryRequestEntity>>(differenceQuery);

                List<DeliveryRequestFrontendShowEntity> deliveryRequestFrontendShowEntities = AddDeliveryInfo(deliveryRequestEntities);

                return deliveryRequestFrontendShowEntities;
            }
            else
            {
                return new List<DeliveryRequestFrontendShowEntity>();
            }
        }

        /// <summary>
        /// 補入配送人資料
        /// </summary>
        /// <param name="deliveryRequestEntities"></param>
        /// <returns></returns>
        private List<DeliveryRequestFrontendShowEntity> AddDeliveryInfo(List<DeliveryRequestEntity> deliveryRequestEntities)
        {
            List<DeliveryRequestFrontendShowEntity> deliveryRequestFrontendShowEntities = new List<DeliveryRequestFrontendShowEntity>();

            var checkNumbers = deliveryRequestEntities.Select(a => a.CheckNumber).ToList();

            //取得配送資料
            var ScanLogs = this.DeliveryScanLogRepository.GetDeliveryScanLogsByScanItem(checkNumbers, "2");

            //取得司機資料
            Dictionary<string, TbDriver> driverDatas = new Dictionary<string, TbDriver>();

            foreach (DeliveryRequestEntity deliveryRequestEntity in deliveryRequestEntities)
            {
                DeliveryRequestFrontendShowEntity deliveryRequestFrontendShowEntity = new DeliveryRequestFrontendShowEntity
                {
                    Id = deliveryRequestEntity.Id,
                    CustomerCode = deliveryRequestEntity.CustomerCode,
                    CheckNumber = deliveryRequestEntity.CheckNumber,
                    ReceiverName = deliveryRequestEntity.ReceiverName,
                    ReceiveTel1 = deliveryRequestEntity.ReceiveTel1,
                    City = deliveryRequestEntity.City,
                    Area = deliveryRequestEntity.Area,
                    Address = deliveryRequestEntity.Address,
                    ShipDate = deliveryRequestEntity.ShipDate,
                    DeliveryDriverCode = string.Empty,
                    DeliveryShowName = ".尚未配送"
                };

                var data = (from scanLog in ScanLogs where scanLog.CheckNumber.Equals(deliveryRequestEntity.CheckNumber) select scanLog).ToList();

                if (data.Count() > 0)
                {
                    deliveryRequestFrontendShowEntity.DeliveryDriverCode = data[0].DriverCode;

                    if (!driverDatas.ContainsKey(data[0].DriverCode))
                    {
                        TbDriver tbDriver = this.DriverRepository.GetByDriverCode(data[0].DriverCode);
                        driverDatas.Add(data[0].DriverCode, tbDriver);
                    }

                    deliveryRequestFrontendShowEntity.DeliveryShowName = string.Format("{0}({1})", driverDatas[data[0].DriverCode].DriverName, data[0].DriverCode);
                }

                deliveryRequestFrontendShowEntities.Add(deliveryRequestFrontendShowEntity);
            }

            return deliveryRequestFrontendShowEntities;
        }

        public KendoSelectCustomerEntity GetByStationAndStartWith(string stationCode, string startWith, bool addTotal = false)
        {
            List<TbCustomer> getCustomers;
            //沒資料時抓全部，否則抓開頭資料
            if (stationCode == string.Empty || stationCode == "-1")
            {
                getCustomers = this.CustomerRepository.GetStartWith(startWith, 100);
            }
            else
            {
                getCustomers = this.CustomerRepository.GetByStationCodeAndStartWith(stationCode, startWith, 100);
            }

            var cusomterEntities = this.Mapper.Map<List<CustomerEntity>>(getCustomers);

            if (addTotal)
            {
                CustomerEntity customerEntity = new CustomerEntity
                {
                    CustomerCode = string.Empty,
                    ShowName = "請選擇一客戶"
                };

                cusomterEntities.Insert(0, customerEntity);
            }

            KendoSelectCustomerEntity kendoSelectCustomerEntity = new KendoSelectCustomerEntity
            {
                results = cusomterEntities,
                __count = cusomterEntities.Count
            };

            return kendoSelectCustomerEntity;
        }

        public List<DeliveryRequestEntity> GetErrorListByCustomerCode(string customerCode, DateTime start, DateTime end, DeliveryErrorTypeConfig deliveryErrorTypeConfig)
        {
            List<DeliveryRequestEntity> returnData = new List<DeliveryRequestEntity>();
            List<TcDeliveryRequest> data = new List<TcDeliveryRequest>();

            switch (deliveryErrorTypeConfig)
            {
                case DeliveryErrorTypeConfig.DeliveryMatingError:
                    var getData0 = this.DeliveryRequestRepository.GetErrorFinishByCustomerCodeAndTimePeriod(customerCode, start, end);
                    data.AddRange(getData0);
                    break;
                case DeliveryErrorTypeConfig.NotDeliveryMating:
                    var getData1 = this.DeliveryRequestRepository.GetNonFinishByCustomerCodeAndTimePeriod(customerCode, start, end);
                    data.AddRange(getData1);
                    break;
                case DeliveryErrorTypeConfig.Total:
                    var getData2 = this.DeliveryRequestRepository.GetErrorFinishByCustomerCodeAndTimePeriod(customerCode, start, end);
                    var getData3 = this.DeliveryRequestRepository.GetNonFinishByCustomerCodeAndTimePeriod(customerCode, start, end);
                    data.AddRange(getData2);
                    data.AddRange(getData3);
                    break;
            }

            returnData = this.Mapper.Map<List<DeliveryRequestEntity>>(data);

            return returnData;
        }

        public List<DeliveryRequestEntity> GetErrorListByArriveCode(string stationSCode, DateTime start, DateTime end, DeliveryErrorTypeConfig deliveryErrorTypeConfig)
        {
            if (stationSCode.Length > 0)
            {

                var station = this.StationRepository.GetByScode(stationSCode);

                List<DeliveryRequestEntity> data = new List<DeliveryRequestEntity>();

                if (stationSCode == "-1" || (station != null && station.StationCode != null && station.StationCode.Length > 0))
                {
                    List<TcDeliveryRequest> recdiveData = new List<TcDeliveryRequest>();

                    switch (deliveryErrorTypeConfig)
                    {
                        case DeliveryErrorTypeConfig.DeliveryMatingError:
                            var getData0 = this.DeliveryRequestRepository.GetErrorFinishByActArriveCodeAndTimePeriod(stationSCode, start, end);
                            recdiveData.AddRange(getData0);
                            break;
                        case DeliveryErrorTypeConfig.NotDeliveryMating:
                            var getData1 = this.DeliveryRequestRepository.GetNonFinishByActArriveCodeAndTimePeriod(stationSCode, start, end);
                            recdiveData.AddRange(getData1);
                            break;
                        case DeliveryErrorTypeConfig.Total:
                            var getData2 = this.DeliveryRequestRepository.GetErrorFinishByActArriveCodeAndTimePeriod(stationSCode, start, end);
                            var getData3 = this.DeliveryRequestRepository.GetNonFinishByActArriveCodeAndTimePeriod(stationSCode, start, end);
                            recdiveData.AddRange(getData2);
                            recdiveData.AddRange(getData3);
                            break;
                    }

                    data = this.Mapper.Map<List<DeliveryRequestEntity>>(recdiveData);
                }

                return data;
            }
            else
            {
                return new List<DeliveryRequestEntity>();
            }
        }

        private List<DeliveryRequestEntity> CheckError(Dictionary<string, DateTime?> shouldData, Dictionary<string, DateTime?> successData, DeliveryErrorTypeConfig deliveryErrorTypeConfig)
        {
            List<string> forOutput = new List<string>();

            foreach (string key in shouldData.Keys)
            {
                //先查未掃讀成功
                if (!successData.ContainsKey(key) && (deliveryErrorTypeConfig == DeliveryErrorTypeConfig.NotDeliveryMating || deliveryErrorTypeConfig == DeliveryErrorTypeConfig.Total))
                {
                    forOutput.Add(key);
                }

                //查詢掃讀錯誤
                if (successData.ContainsKey(key) && successData[key] != shouldData[key] && (deliveryErrorTypeConfig == DeliveryErrorTypeConfig.DeliveryMatingError || deliveryErrorTypeConfig == DeliveryErrorTypeConfig.Total))
                {
                    forOutput.Add(key);
                }
            }

            List<TcDeliveryRequest> deliveryRequests = this.DeliveryRequestRepository.GetDeliveryRequestsByCheckNumberList(forOutput);

            List<DeliveryRequestEntity> deliveryRequestEntities = this.Mapper.Map<List<DeliveryRequestEntity>>(deliveryRequests);

            return deliveryRequestEntities;
        }

        private List<GetDeliveryAnomalyShowEntity> MappingDeliveryRequestEntityToFrontend(List<DeliveryRequestEntity> data)
        {
            List<GetDeliveryAnomalyShowEntity> getDeliveryAnomalyShowEntities = new List<GetDeliveryAnomalyShowEntity>();

            var checkNumbers = data.Select(a => a.CheckNumber).ToList();
            var listStationScode = data.Select(r => r.AreaArriveCode).Distinct().ToList();
            var listStationInfo = StationRepository.GetStationsByScodes(listStationScode);

            var lastCheckInfo = this.DeliveryRequestRepository.GetLastScanLog(checkNumbers);

            // 應該不需要抓司機資料
            //var listDriverCode = lastCheckInfo.Values.ToList().Select(a => a.DriverCode).Distinct().ToList();

            //var listDriverInfo = this.DriverRepository.GetDriversByDriverCodes(listDriverCode);

            //var listEmpCode = listDriverInfo.Values.ToList().Select(a => a.EmpCode).Distinct().ToList();

            //var listEmpInfo = this.EmpRepository.GetEmpsByEmpCodes(listEmpCode);

            //var listStationCode = listEmpInfo.Values.ToList().Select(a => a.Station).Distinct().ToList();

            //var listStationInfo = this.StationRepository.GetTbStationsByIds(listStationCode);

            var listCustomerCode = data.Select(a => a.CustomerCode.ToUpper()).Distinct().ToList();

            var listCustomerInfo = this.CustomerRepository.GetCustomersByCustomerCodes(listCustomerCode);

            //取得站所、出貨人等資訊
            Dictionary<string, string> stationMapping = new Dictionary<string, string>();
            Dictionary<string, string> customerMapping = new Dictionary<string, string>();

            int i = 1;

            foreach (DeliveryRequestEntity deliveryRequestEntity in data)
            {
                /*string arriveStation = "";
                if (Array.Exists(listStationInfo.ToArray(), e => e.StationCode == deliveryRequestEntity.AreaArriveCode))
                {
                    arriveStation = listStationInfo.FirstOrDefault(s => s.StationScode == deliveryRequestEntity.AreaArriveCode).StationName;
                }
                else
                {
                    arriveStation = "全部站所";
                }*/

                DateTime shipDate = deliveryRequestEntity.ShipDate ?? DateTime.MinValue;
                int getPices = 0;
                string arriveOption = "";
                string scanItemName = string.Empty;
                string arriveOptionDesc = "";
                DateTime? scanDate = DateTime.MinValue;
                string senderName = string.Empty;

                //取得最後掃讀記錄
                if (lastCheckInfo.ContainsKey(deliveryRequestEntity.CheckNumber))
                {

                    var scanLog = lastCheckInfo[deliveryRequestEntity.CheckNumber];

                    scanDate = scanLog.ScanDate;

                    //if (scanLog != null && scanLog.DriverCode != null && scanLog.DriverCode.Length > 0 && listDriverInfo.ContainsKey(scanLog.DriverCode.ToUpper()) )
                    //{
                    //    var eachDriver = listDriverInfo[scanLog.DriverCode.ToUpper()];

                    //    if (eachDriver != null && eachDriver.EmpCode != null && eachDriver.EmpCode.Length > 0 && listEmpInfo.ContainsKey(eachDriver.EmpCode.ToUpper()))
                    //    {
                    //        var eachEmp = listEmpInfo[eachDriver.EmpCode.ToUpper()];

                    //        if (eachEmp != null && eachEmp.Station != null && eachEmp.Station.Length > 0 && listStationInfo.ContainsKey(eachEmp.Station.ToUpper()))
                    //        {
                    //            var eachStation = listStationInfo[eachEmp.Station.ToUpper()];

                    //            arriveStation = eachStation.StationName;
                    //        }
                    //    }

                    //}

                    getPices = scanLog.Pieces ?? 0;
                    arriveOption = scanLog.AreaArriveCode;

                    scanItemName = scanLog.ScanItem switch
                    {
                        "1" => "到著",
                        "2" => "配送",
                        "3" => "配達",
                        "4" => "配達",
                        "5" => "集貨",
                        "6" => "卸集",
                        "7" => "發送",
                        _ => "不明",
                    };

                    arriveOptionDesc = scanLog.ArriveOption switch
                    {
                        "1" => "客戶不在",
                        "2" => "約定再配",
                        "3" => "正常配交",
                        "4" => "拒收",
                        "5" => "地址錯誤",
                        "6" => "查無此人",
                        _ => "",                    //議題#405
                    };
                }

                if (deliveryRequestEntity.CustomerCode != null && deliveryRequestEntity.CustomerCode.Length > 0 && listCustomerInfo.ContainsKey(deliveryRequestEntity.CustomerCode.ToUpper()))
                {
                    var eachCustomer = listCustomerInfo[deliveryRequestEntity.CustomerCode.ToUpper()];

                    if (eachCustomer != null)
                    {
                        senderName = string.Format("{0}({1})", eachCustomer.CustomerName, eachCustomer.CustomerCode);
                    }
                }

                GetDeliveryAnomalyShowEntity getDeliveryAnomalyShowEntity = new GetDeliveryAnomalyShowEntity
                {
                    Id = deliveryRequestEntity.Id,
                    No = i,
                    CheckNumber = deliveryRequestEntity.CheckNumber,
                    SenderName = senderName,
                    StationName = deliveryRequestEntity.AreaArriveCode == "" ? "" : listStationInfo.FirstOrDefault(x => x.StationScode == deliveryRequestEntity.AreaArriveCode).StationName,
                    ShipDate = (shipDate == DateTime.MinValue) ? "" : shipDate.ToString("yyyyMMdd"),
                    SendPices = deliveryRequestEntity.Pieces,
                    GetPices = getPices,
                    ReceiverName = deliveryRequestEntity.ReceiverName,
                    ZhuArea = string.Empty,
                    PaiArea = string.Empty,
                    ArriveOption = arriveOption,
                    ScanItemName = scanItemName,
                    ArriveOptionDesc = arriveOptionDesc,
                    ScanDate = scanDate,
                    ReceiveAddress = deliveryRequestEntity.City + deliveryRequestEntity.Area + deliveryRequestEntity.Address,
                    ReceiveTel = deliveryRequestEntity.ReceiveTel1 is null ? "" : deliveryRequestEntity.ReceiveTel1
                };

                getDeliveryAnomalyShowEntities.Add(getDeliveryAnomalyShowEntity);

                i++;
            }

            return getDeliveryAnomalyShowEntities;
        }

        public List<GetDeliveryAnomalyShowEntity> GetFrontendErrorListByCustomerCode(string customerCode, DateTime start, DateTime end, DeliveryErrorTypeConfig deliveryErrorTypeConfig)
        {
            //取得未銷清單
            var data = GetErrorListByCustomerCode(customerCode, start, end, deliveryErrorTypeConfig);

            var output = MappingDeliveryRequestEntityToFrontend(data);

            return output;
        }

        public List<GetDeliveryAnomalyShowEntity> GetFrontendErrorListByArriveCode(string stationSCode, DateTime start, DateTime end, DeliveryErrorTypeConfig deliveryErrorTypeConfig)
        {
            //取得未銷清單
            var data = GetErrorListByArriveCode(stationSCode, start, end, deliveryErrorTypeConfig);

            var output = MappingDeliveryRequestEntityToFrontend(data);

            return output;
        }

        private List<TcDeliveryRequest> GetReturnDeliveryListByCustomerCode(string customerCode, DateTime start, DateTime end)
        {
            return DeliveryRequestRepository.GetReturnDeliveryCheckNumberListByCustomerCodeAndDateZone(customerCode, start, end);
        }

        private List<TcDeliveryRequest> GetReturnDeliveryListBySendStationCode(string stationScode, DateTime start, DateTime end)
        {
            return DeliveryRequestRepository.GetReturnDeliveryCheckNumberLisyBySendStationCodeAndDateZone(stationScode, start, end);
        }

        private List<TcDeliveryRequest> GetAllReturnDeliveryListByDateZone(DateTime start, DateTime end)
        {
            return DeliveryRequestRepository.GetAllReturnDeliveryCheckNumberLisyByDateZone(start, end);
        }
        private List<ReturnDeliveryShowEntity> MappingReturnDeliveryEntityListToFrontend(List<TcDeliveryRequest> data, PickUpOrNotTypeConfig type)
        {
            List<ReturnDeliveryShowEntity> output = new List<ReturnDeliveryShowEntity>();
            Dictionary<TcDeliveryRequest, bool> isCloseNormallyDictionary = GetDeliveryClosedOrNotDictionary(data);
            int count = 1;
            foreach (var request in data)
            {
                ReturnDeliveryShowEntity aEntity = new ReturnDeliveryShowEntity();
                TbStation sendStation = StationRepository.GetByScode(request.SendStationScode);
                TbStation arriveStation = StationRepository.GetByScode(request.AreaArriveCode);

                aEntity.No = count;
                aEntity.PrintDate = String.Format("{0:yyyy/MM/dd}", request.PrintDate);
                aEntity.ShipDate = String.Format("{0:yyyy/MM/dd}", request.ShipDate);
                aEntity.SendStationName = sendStation != null ? sendStation.StationName : "";
                aEntity.ArriveStationName = arriveStation != null ? arriveStation.StationName : "";
                aEntity.CheckNumber = request.CheckNumber;
                aEntity.ReturnerName = request.SendContact;
                aEntity.ReturnerPhone = request.SendTel;
                aEntity.ReturnerAddress = request.SendCity + request.SendArea + request.SendAddress;
                aEntity.Pieces = (int)request.Pieces;
                aEntity.ReturnItem = request.InvoiceDesc;
                aEntity.DeliveryState = (isCloseNormallyDictionary[request]) ? "已結案" : "未結案";
                aEntity.PickUpState = (request.ShipDate != null) ? "已取" : "未取";

                aEntity.LatestScanItem = (request.LatestScanItem == null) ? null : ScanItemService.GetScanItemChinese((ScanItem)int.Parse(request.LatestScanItem));
                aEntity.LatestScanDate = request.LatestScanDate;
                aEntity.LatestArriveOption = request.LatestArriveOption == null ? null
                    : ScanItemService.GetArriveOptionChinese((ArriveOption)int.Parse(request.LatestArriveOption));

                switch (type)
                {
                    case PickUpOrNotTypeConfig.Total:
                        output.Add(aEntity);
                        count += 1;
                        break;
                    case PickUpOrNotTypeConfig.NotPickUpYet:
                        if (request.ShipDate == null)
                        {
                            output.Add(aEntity);
                            count += 1;
                        }
                        break;
                    case PickUpOrNotTypeConfig.AlreadyPickUp:
                        if (request.ShipDate != null)
                        {
                            output.Add(aEntity);
                            count += 1;
                        }
                        break;
                }
            }
            return output;
        }
        public List<ReturnDeliveryShowEntity> GetFrontendReturnDeliveryListByCustomerCode(string customerCode, DateTime start, DateTime end, PickUpOrNotTypeConfig pickUpOrNotTypeConfig)
        {
            List<TcDeliveryRequest> data = GetReturnDeliveryListByCustomerCode(customerCode, start, end);
            List<ReturnDeliveryShowEntity> output = MappingReturnDeliveryEntityListToFrontend(data, pickUpOrNotTypeConfig);
            return output;
        }

        public List<ReturnDeliveryShowEntity> GetFrontendReturnDeliveryListBySendStationCode(string stationScode, DateTime start, DateTime end, PickUpOrNotTypeConfig pickUpOrNotTypeConfig)
        {
            List<TcDeliveryRequest> data = GetReturnDeliveryListBySendStationCode(stationScode, start, end);
            List<ReturnDeliveryShowEntity> output = MappingReturnDeliveryEntityListToFrontend(data, pickUpOrNotTypeConfig);
            return output;
        }

        public List<ReturnDeliveryShowEntity> GetFrontendReturnDeliveryListByDateZone(DateTime start, DateTime end, PickUpOrNotTypeConfig pickUpOrNotTypeConfig)
        {
            List<TcDeliveryRequest> data = GetAllReturnDeliveryListByDateZone(start, end);
            List<ReturnDeliveryShowEntity> output = MappingReturnDeliveryEntityListToFrontend(data, pickUpOrNotTypeConfig);
            return output;
        }
        public List<DeliveryRequestMenuVerifyEntity> GetNeedToVerifyDeliveryRequestByScanDate(DateTime startDate, DateTime endDate, string areaName, string scode)
        {
            if (areaName == "全選")
            {
                if (!scode.Equals("-1") & !string.IsNullOrEmpty(scode))
                {
                    //選個別站所
                    var data = this.DeliveryRequestRepository.GetErrorDoneDeliveryRequestsByScanDateStationScode(startDate, endDate, scode);

                    var dataEntities = ChangeDeliveryRequestsToDeliveryRequestMenuVerifyEntities(data);

                    return dataEntities;
                }
                else
                {
                    var data = this.DeliveryRequestRepository.GetAllAreaErrorDoneDeliveryRequestsByScanDate(startDate, endDate);

                    var dataEntities = ChangeDeliveryRequestsToDeliveryRequestMenuVerifyEntities(data);

                    return dataEntities;
                }
            }
            else
            {
                if (scode.Equals("0")) //區域內全選
                {
                    var data = this.DeliveryRequestRepository.GetErrorDoneDeliveryRequestsByScanDateArea(startDate, endDate, areaName);

                    var dataEntities = ChangeDeliveryRequestsToDeliveryRequestMenuVerifyEntities(data);

                    return dataEntities;
                }
                else
                {
                    //選個別站所
                    var data = this.DeliveryRequestRepository.GetErrorDoneDeliveryRequestsByScanDateStationScode(startDate, endDate, scode);

                    var dataEntities = ChangeDeliveryRequestsToDeliveryRequestMenuVerifyEntities(data);

                    return dataEntities;
                }
            }
        }

        public List<DeliveryRequestMenuVerifyEntity> GetNeedToVerifyDeliveryRequestByEmpCode(string empCode)
        {
            TbDriver tbDriver = this.DriverRepository.GetByEmpCode(empCode);

            string driverCode = (tbDriver == null) ? empCode : tbDriver.DriverCode;

            var data = this.DeliveryRequestRepository.GetErrorDoneDeliveryRequestsByDriverCode(driverCode);

            var dataEntities = ChangeDeliveryRequestsToDeliveryRequestMenuVerifyEntities(data);

            return dataEntities;
        }

        public List<DeliveryRequestMenuVerifyEntity> GetNeedToVerifyDeliveryRequestByCheckNumber(string checkNumber)
        {
            List<DeliveryRequestMenuVerifyEntity> dataList = new List<DeliveryRequestMenuVerifyEntity>();

            TcDeliveryRequest tcDeliveryRequest = this.DeliveryRequestRepository.GetNotLessThanTruckloadByCheckNumber(checkNumber);

            var checkNumberInWriteOff = this.WriteOffRepository.GetWriteOffNormal(checkNumber);

            if (checkNumberInWriteOff is null)
            {
                if (tcDeliveryRequest is null)
                {
                    return dataList;
                }
                if (tcDeliveryRequest.DeliveryCompleteDate != null && tcDeliveryRequest.LatestScanDate > tcDeliveryRequest.DeliveryCompleteDate)
                {
                    List<TcDeliveryRequest> data = new List<TcDeliveryRequest>();

                    data.Add(tcDeliveryRequest);

                    var dataEntities = ChangeDeliveryRequestsToDeliveryRequestMenuVerifyEntities(data);

                    dataList.AddRange(dataEntities);

                    return dataList;
                }
                return dataList;
            }

            if (tcDeliveryRequest.CheckNumber.Contains(checkNumberInWriteOff.CheckNumber))
            {
                return dataList;
            }
            else
            {
                if (tcDeliveryRequest.DeliveryCompleteDate != null && tcDeliveryRequest.LatestScanDate > tcDeliveryRequest.DeliveryCompleteDate)
                {
                    List<TcDeliveryRequest> data = new List<TcDeliveryRequest>();

                    data.Add(tcDeliveryRequest);

                    var dataEntities = ChangeDeliveryRequestsToDeliveryRequestMenuVerifyEntities(data);

                    dataList.AddRange(dataEntities);
                }
            }
            return dataList;
        }

        private List<DeliveryRequestMenuVerifyEntity> ChangeDeliveryRequestsToDeliveryRequestMenuVerifyEntities(List<TcDeliveryRequest> tcDeliveryRequests)
        {
            //回到的資料需要再補
            var dataEntities = this.Mapper.Map<List<DeliveryRequestMenuVerifyEntity>>(tcDeliveryRequests);

            var listData = dataEntities.Select(a => a.CheckNumber).ToList();

            var stationScode = dataEntities.Select(a => a.Station).ToList();

            // var stationName = this.StationRepository.GetListStationNameByListStationScode(stationScode);

            //station.AddRange(stationName);

            var sta = this.StationRepository.GetTbStations(stationScode);

            var scanData = this.DeliveryRequestRepository.GetLastScanLog(listData);

            var driverCodes = scanData.Values.ToList().Select(a => a.DriverCode).ToList();

            var driverMapping = this.DriverRepository.GetDriversByDriverCodes(driverCodes);

            var stationCodes = driverMapping.Values.ToList().Select(a => a.Station).Distinct().ToList();

            var stationMappings = this.StationRepository.GetTbStations(stationCodes);

            //取得手動銷號
            var writeOffs = this.WriteOffRepository.GetWriteOffs(listData);

            var writeOffCheckLists = writeOffs.Values.ToList().Select(a => a.WriteOffCheckListId).Distinct().ToList();

            var writeOffCheckListDictionaries = this.DeliveryExceptionRepository.GetWriteOffCheckListsByIds(writeOffCheckLists);

            var verifyStationScodes = writeOffs.Values.ToList().Select(a => a.StationCode).Distinct().ToList();

            var verifyStationMappings = this.StationRepository.GetTbStations(verifyStationScodes);

            int i = 1;

            //開始回補資料
            foreach (DeliveryRequestMenuVerifyEntity deliveryRequestMenuVerifyEntity in dataEntities)
            {
                for (int p = 0; p < stationScode.Count; p++)
                {
                    if (stationScode[p] != "")
                    {
                        TbStation s = sta[stationScode[p]];
                        if (deliveryRequestMenuVerifyEntity.Station == s.StationScode)
                        {
                            deliveryRequestMenuVerifyEntity.Station = s.StationName;
                            break;
                        }
                    }
                }

                if (scanData.ContainsKey(deliveryRequestMenuVerifyEntity.CheckNumber))
                {
                    TtDeliveryScanLog ttDeliveryScanLog = scanData[deliveryRequestMenuVerifyEntity.CheckNumber];

                    TbDriver tbDriver = driverMapping[ttDeliveryScanLog.DriverCode.ToUpper()];

                    //TbStation tbStation = (tbDriver == null || tbDriver.Station == string.Empty || tbDriver.Station == null) ? null : stationMappings[tbDriver.Station];
                    //deliveryRequestMenuVerifyEntity.

                    deliveryRequestMenuVerifyEntity.SerialNo = i;

                    //deliveryRequestMenuVerifyEntity.Station = (tbStation == null) ? string.Empty : tbStation.StationName;

                    deliveryRequestMenuVerifyEntity.DeliveryArea = string.Empty;

                    deliveryRequestMenuVerifyEntity.EmpCode = (tbDriver == null) ? string.Empty : tbDriver.EmpCode;

                    deliveryRequestMenuVerifyEntity.ArriveOption = ttDeliveryScanLog.ArriveOption switch
                    {
                        "1" => "客戶不在",
                        "2" => "約定再配",
                        "3" => "正常配交",
                        "4" => "拒收",
                        "5" => "地址錯誤",
                        "6" => "查無此人",
                        _ => "",                        //議題#405
                    };

                }

                else if (writeOffs.ContainsKey(deliveryRequestMenuVerifyEntity.CheckNumber))
                {
                    int writeOffCheckListId = writeOffs[deliveryRequestMenuVerifyEntity.CheckNumber].WriteOffCheckListId ?? 0;

                    string verifyStationCode = writeOffs[deliveryRequestMenuVerifyEntity.CheckNumber].StationCode;

                    TbStation verifyStation = verifyStationMappings[verifyStationCode];

                    StationAreaEntity verifyStationEntity = this.Mapper.Map<StationAreaEntity>(verifyStation);

                    deliveryRequestMenuVerifyEntity.VerifyEmpCode = writeOffs[deliveryRequestMenuVerifyEntity.CheckNumber].EmpCode;
                    deliveryRequestMenuVerifyEntity.VerifyOption = new WriteOffCheckListEntity
                    {
                        Id = writeOffCheckListId,
                        Information = (writeOffs[deliveryRequestMenuVerifyEntity.CheckNumber].WriteOffCheckListId == null) ? "" : writeOffCheckListDictionaries[writeOffCheckListId].Information,
                        Type = (writeOffs[deliveryRequestMenuVerifyEntity.CheckNumber].WriteOffCheckListId == null) ? "" : writeOffCheckListDictionaries[writeOffCheckListId].Type
                    };

                    deliveryRequestMenuVerifyEntity.VerifyStation = verifyStationEntity;

                }
                i++;
            }

            return dataEntities;
        }

        public List<WriteOffCheckListEntity> GetAllWriteOffCheckLists()
        {
            var datas = this.DeliveryExceptionRepository.GetAllWriteOffCheckList();

            var deliveryScanLogEntities = this.Mapper.Map<List<WriteOffCheckListEntity>>(datas);

            return deliveryScanLogEntities;
        }

        public List<WriteOffCheckListEntity> GetErrorWriteOffCheckLists()
        {
            var datas = this.DeliveryExceptionRepository.GetAllWriteOffCheckList();

            var deliveryScanLogEntities = this.Mapper.Map<List<WriteOffCheckListEntity>>(datas);

            return deliveryScanLogEntities;
        }

        public List<WriteOffCheckListEntity> GetNormalWriteOffCheckLists()
        {
            var datas = this.DeliveryExceptionRepository.GetAllWriteOffCheckList();

            var deliveryScanLogEntities = this.Mapper.Map<List<WriteOffCheckListEntity>>(datas);

            return deliveryScanLogEntities;
        }

        public void SaveMenuVerifyDeliveryRequest(DeliveryRequestMenuVerifyEntity deliveryRequestMenuVerifyEntity)
        {
            //先查是否有資料
            DA.JunFuTransDb.WriteOff writeOff = this.WriteOffRepository.GetWriteOff(deliveryRequestMenuVerifyEntity.CheckNumber);

            if (writeOff == null)
            {
                writeOff = new DA.JunFuTransDb.WriteOff();

                writeOff.CheckNumber = deliveryRequestMenuVerifyEntity.CheckNumber;
                writeOff.StationCode = deliveryRequestMenuVerifyEntity.VerifyStation.StationSCode;
                writeOff.EmpCode = deliveryRequestMenuVerifyEntity.VerifyEmpCode;
                writeOff.WriteOffCheckListId = deliveryRequestMenuVerifyEntity.VerifyOption.Id;

                this.WriteOffRepository.Insert(writeOff);
            }
            else
            {
                writeOff.StationCode = deliveryRequestMenuVerifyEntity.VerifyStation.StationSCode;
                writeOff.EmpCode = deliveryRequestMenuVerifyEntity.VerifyEmpCode;
                writeOff.WriteOffCheckListId = deliveryRequestMenuVerifyEntity.VerifyOption.Id;

                this.WriteOffRepository.Update(writeOff);
            }

        }

        public TagImageBase64Entity GetTagImageBase64Entity(string checkNumber, string token)
        {

            TagImageBase64Entity tagImageBase64Entity = new TagImageBase64Entity();

            bool verifyInDB = false;

            //取得訂單資料
            DeliveryRequestEntity deliveryRequestEntity = this.GetDeliveryRequest(checkNumber);

            List<ApiTokenMapping> apiTokenMappings = this.ApiTokenMappingRepository.GetApiTokenMappingsByToken(token);

            //驗證skyeyes的正確性
            try
            {
                var json = Encoding.UTF8.GetString(Convert.FromBase64String(token));

                SkyEyesTokenInfoEntity skyEyesTokenInfoEntity = JsonConvert.DeserializeObject<SkyEyesTokenInfoEntity>(json);

                Int64 nowSpan = Convert.ToInt32((DateTime.Now - new DateTime(1970, 1, 1)).TotalSeconds);

                if (nowSpan > skyEyesTokenInfoEntity.exp)
                {
                    tagImageBase64Entity.Success = false;
                    tagImageBase64Entity.Message = "token is expired !";

                    return tagImageBase64Entity;
                }

                if (deliveryRequestEntity != null)
                {
                    string accountCode = deliveryRequestEntity.CustomerCode.Trim();
                    string jsonAccountCode = skyEyesTokenInfoEntity.info.account_code.Trim();

                    if (accountCode != jsonAccountCode)
                    {
                        tagImageBase64Entity.Success = false;
                        tagImageBase64Entity.Message = "token is not match !";

                        return tagImageBase64Entity;
                    }
                }
            }
            catch
            {
                verifyInDB = true;
            }

            if (verifyInDB && (apiTokenMappings == null || apiTokenMappings.Count() == 0))
            {
                tagImageBase64Entity.Success = false;
                tagImageBase64Entity.Message = "token is incorrect !";

                return tagImageBase64Entity;
            }

            //if ( deliveryRequestEntity == null )
            //{
            //    tagImageBase64Entity.Success = false;
            //    tagImageBase64Entity.Message = "data is not exist !";

            //    return tagImageBase64Entity;
            //}

            if (verifyInDB)
            {
                var dataCount = (from apis in apiTokenMappings where apis.CustomerCode.Equals(deliveryRequestEntity.CustomerCode) select apis).Count();

                if (dataCount == 0)
                {
                    tagImageBase64Entity.Success = false;
                    tagImageBase64Entity.Message = "customer error!";

                    return tagImageBase64Entity;
                }
            }

            NameValueCollection reportParams = new NameValueCollection
            {
                ["check_number"] = checkNumber
            };

            ReportExportType reportExportType = ReportExportType.IMAGE;

            var pdfStream = this.ReportHelper.GetNomalReport(reportParams, "LTReelLabel", reportExportType.ToString("G"));

            byte[] bytes;
            using (var memoryStream = new MemoryStream())
            {
                pdfStream.CopyTo(memoryStream);
                bytes = memoryStream.ToArray();
            }

            string base64 = Convert.ToBase64String(bytes);

            tagImageBase64Entity.Success = true;
            tagImageBase64Entity.Image = base64;

            return tagImageBase64Entity;
        }

        public List<string> GetCheckNumbersByIds(int[] requestIds)
        {
            List<decimal> inputData = new List<decimal>();

            foreach (int eachRequestId in requestIds)
            {
                decimal eachDecimal = (decimal)eachRequestId;

                inputData.Add(eachDecimal);
            }

            return this.DeliveryRequestRepository.GetCheckNumbersByIds(inputData);

        }

        public Dictionary<TcDeliveryRequest, bool> GetDeliveryClosedOrNotDictionary(List<TcDeliveryRequest> deliveryRequests)
        {
            Dictionary<TcDeliveryRequest, bool> result = new Dictionary<TcDeliveryRequest, bool>();

            foreach (TcDeliveryRequest deliveryRequest in deliveryRequests)
            {
                if (deliveryRequest.DeliveryCompleteDate != null)
                {
                    if (deliveryRequest.LatestScanDate == deliveryRequest.DeliveryCompleteDate)
                    {
                        result[deliveryRequest] = true;
                    }
                    else
                    {
                        if (WriteOffRepository.isClosedNormally(deliveryRequest.CheckNumber))
                        {
                            result[deliveryRequest] = true;
                        }
                        else
                        {
                            result[deliveryRequest] = false;
                        }
                    }
                }
                else
                {
                    result[deliveryRequest] = false;
                }
 
            }
            return result;
        }

        public IEnumerable<DeliveryRequestEntity> GetDeliveryRequestsByCheckNumberList(IEnumerable<string> checkNumbers)
        {
            var requests = DeliveryRequestRepository.GetByCheckNumberList(checkNumbers);
            return Mapper.Map<List<DeliveryRequestEntity>>(requests);
        }

        public void SaveDeliveryRequestMDSDByCheckNumbers(IEnumerable<string> checkNumbers)
        {
            var requests = DeliveryRequestRepository.GetByCheckNumberList(checkNumbers);

            var ids = requests.Select(a => a.RequestId).ToArray();

            SaveDeliveryRequestMDSD(ids);
        }

        public void SaveDeliveryRequestMDSD(decimal[] requestIds)
        {
            //取得託運單資料
            var data = this.DeliveryRequestRepository.GetAllByRequestIds(requestIds.ToList());

            List<CheckNumberSdMapping> insertDatas = new List<CheckNumberSdMapping>();

            var checkExists = this.CheckNumberSDMappingRepository.CheckExistByRequestIds(requestIds.ToList());

            foreach (TcDeliveryRequest tcDeliveryRequest in data)
            {
                if (!checkExists.ContainsKey((long)tcDeliveryRequest.RequestId))
                {
                    FSEAddressEntity fSEAddressEntity = new FSEAddressEntity(tcDeliveryRequest.ReceiveAddress, tcDeliveryRequest.ReceiveCity, tcDeliveryRequest.ReceiveArea);

                    var eachDatas = this.OrgAreaRepository.GetByFSEAddressEntity(fSEAddressEntity);

                    if (eachDatas.Count > 0)
                    {
                        CheckNumberSdMapping checkNumberSdMapping = new CheckNumberSdMapping
                        {
                            CheckNumber = tcDeliveryRequest.CheckNumber,
                            RequestId = (long)tcDeliveryRequest.RequestId,
                            OrgAreaId = eachDatas[0].Id,
                            Md = eachDatas[0].MdNo,
                            Sd = eachDatas[0].SdNo,
                            PutOrder = eachDatas[0].PutOrder,
                            Cdate = DateTime.Now,
                            Udate = DateTime.Now
                        };

                        insertDatas.Add(checkNumberSdMapping);
                        //this.CheckNumberSDMappingRepository.Insert(checkNumberSdMapping);
                    }
                }
            }

            if (insertDatas.Count > 0)
            {
                this.CheckNumberSDMappingRepository.InsertMutiData(insertDatas);
            }
        }

        public IEnumerable<CityAreaEntity> GetCityAreaEntitiesByAddress(string address)
        {
            FSEAddressEntity fSEAddressEntity = new FSEAddressEntity(address, "", "");

            var data = this.OrgAreaRepository.GetCityAreas(fSEAddressEntity.Road);

            return data;
        }

        public IEnumerable<AreaArriveCodeForLabelEntity> GetAreaArriveCodeByRequestIds(string requestIds)
        {
            string[] strIds = requestIds.Split(',');
            decimal[] dIds = new decimal[strIds.Length];

            for(int i=0; i<strIds.Length; i++)
            {
                decimal.TryParse(strIds[i], out dIds[i]);
            }

            IEnumerable<TcDeliveryRequest> requests = DeliveryRequestRepository.GetAllByRequestIds(dIds.ToList());

            List<CheckNumberWithNormalizeAddress> normalizeAddresses = new List<CheckNumberWithNormalizeAddress>();

            foreach(var e in requests)
            {
                var fSEAddressEntity = new FSEAddressEntity(e.ReceiveAddress, e.ReceiveCity, e.ReceiveArea);
                normalizeAddresses.Add(new CheckNumberWithNormalizeAddress
                {
                    CheckNumber = e.CheckNumber,
                    CustomerCode = e.CustomerCode,
                    City = fSEAddressEntity.City,
                    Area = fSEAddressEntity.District,
                    Road = fSEAddressEntity.TransAddress.Trim()
                });
            }

            var customizeAreaArriveCode = CustomizeAreaArriveCodeRepository.GetAll();

            var result = from address in normalizeAddresses
                         join aac in customizeAreaArriveCode on new { City = address.City, Area = address.Area, Road = address.Road, address.CustomerCode } 
                         equals new {City = aac.ReceiveCity, Area = aac.ReceiveArea, Road = aac.ReceiveRoad, aac.CustomerCode }
                         select new AreaArriveCodeForLabelEntity
                         {
                             CheckNumber = address.CheckNumber,
                             AreaArriveCode = aac.ShowedAreaArriveCode
                         };

            return result;
        }

        /// <summary>
        /// 取得暫存的資訊
        /// </summary>
        /// <param name="infoKey"></param>
        /// <returns></returns>
        public IEnumerable<AreaArriveCodeForLabelEntity> GetAreaArriveCodeForLabelEntitiesByInfoKey(string infoKey)
        {
            var data = this.PrintTempInfoRepository.GetByInfoKey(infoKey);

            if( data != null && data.Info.Length > 0 )
            {

                var result = JsonConvert.DeserializeObject<IEnumerable<AreaArriveCodeForLabelEntity>>(data.Info);

                //後續加入刪除的部分

                return result;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 寫入暫存的大量列印資訊
        /// </summary>
        /// <param name="areaArriveCodeForLabelEntities"></param>
        /// <returns></returns>
        public string SetAreaArriveCodeForLabelEntities(IEnumerable<AreaArriveCodeForLabelEntity> areaArriveCodeForLabelEntities)
        {
            string guid = Guid.NewGuid().ToString();

            string json = JsonConvert.SerializeObject(areaArriveCodeForLabelEntities);

            PrintTempInfo printTempInfo = new PrintTempInfo();
            printTempInfo.InfoKey = guid;
            printTempInfo.Info = json;
            PrintTempInfoRepository.Insert(printTempInfo);

            return guid;
        }

        public string SetAreaArriveCodeForLabelEntitiesByIds(string requestIds)
        {
            var data = GetAreaArriveCodeByRequestIds(requestIds);

            return SetAreaArriveCodeForLabelEntities(data);
        }

        //public IEnumerable<DriverPaymentDetailEntity> GetDriverPaymentDetialsByChechNums(IEnumerable<string> checkNumbers)
        //{
        //    var requests = DeliveryRequestRepository.GetByCheckNumberList(checkNumbers);
        //    return Mapper.Map<IEnumerable<DriverPaymentDetailEntity>>(requests);
        //}

    }
}
