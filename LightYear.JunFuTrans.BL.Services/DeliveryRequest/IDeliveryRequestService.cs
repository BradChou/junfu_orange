﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.DeliveryRequest;
using LightYear.JunFuTrans.BL.BE.Statics;
using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.BL.BE.DriverToStationPayment;

namespace LightYear.JunFuTrans.BL.Services.DeliveryRequest
{
    public interface IDeliveryRequestService
    {
        List<DeliveryScanLogEntity> GetScanLog(string checkNumber);

        DeliveryRequestEntity GetDeliveryRequest(string checkNumber);

        IEnumerable<DeliveryRequestEntity> GetDeliveryRequestsByCheckNumberList(IEnumerable<string> checkNumbers);

        /// <summary>
        /// 取得過去各周的總件數統計表
        /// </summary>
        /// <param name="weeks">周數</param>
        /// <returns></returns>
        List<DeliveryRequestStaticEntity> GetPassWeeksStaticsData(int weeks);

        /// <summary>
        /// 取得配送日後計算前一日出貨，該配送站所之 D+1 配達率應配筆數
        /// </summary>
        /// <param name="stationScode"></param>
        /// <param name="deliveryDate"></param>
        /// <returns></returns>
        public int GetDPlusOneShouldDeliveryCount(string stationScode, DateTime deliveryDate);

        /// <summary>
        /// 取得配送日後計算前一日出貨，該配送站所之 D+1 配達率配達筆數
        /// </summary>
        /// <param name="stationScode"></param>
        /// <param name="deliveryDate"></param>
        /// <returns></returns>
        public int GetDPlusOneSuccessDeliveryCount(string stationScode, DateTime deliveryDate);

        /// <summary>
        /// 取得配送日後計算前一日出貨，該配送站所之 D+1 配達率應配筆數List
        /// </summary>
        /// <param name="stationScode"></param>
        /// <param name="deliveryDate"></param>
        /// <returns></returns>
        public List<TcDeliveryRequest> GetDPlusOneShouldDeliveryList(string stationScode, DateTime deliveryDate);

        /// <summary>
        /// 取得配送日後計算前一日出貨，該配送站所之 D+1 配達率配達筆數List
        /// </summary>
        /// <param name="stationScode"></param>
        /// <param name="deliveryDate"></param>
        /// <returns></returns>
        public List<TcDeliveryRequest> GetDPlusOneSuccessDeliveryList(string stationScode, DateTime deliveryDate);

        /// <summary>
        /// 取得配送日後計算前一日出貨，該配送站所之 D+1 配達率未配達筆數List
        /// </summary>
        /// <param name="stationScode">站所的scode</param>
        /// <param name="deliveryDate">配送日</param>
        /// <returns></returns>
        public List<DashboardDeliveryRequestFrontendShowEntity> GetDPlusOneNotSuccessDeliveryList(string stationScode, DateTime deliveryDate);

        /// <summary>
        /// 取得該站所留庫筆數List
        /// </summary>
        /// <param name="stationScode">站所的scode</param>
        /// <param name="DeadLineDate">結算日</param>
        /// <returns></returns>
        public Dictionary<DashboardDeliveryRequestFrontendShowEntity, int> GetDepotList(string stationScode, DateTime DeadLineDate);

        /// <summary>
        /// 取得該站所留庫筆數
        /// </summary>
        /// <param name="stationScode">站所的scode</param>
        /// <param name="DeadLineDate">結算日</param>
        /// <returns></returns>
        public Dictionary<int, int> GetDepotCount(string stationScode, DateTime DeadLineDate);

        /// <summary>
        /// 取得該站所留庫筆數
        /// </summary>
        /// <param name="stationScode">站所的scode</param>
        /// <param name="depotCategory">留庫種類</param>
        /// <param name="DeadLineDate">結算日</param>
        /// <returns></returns>
        public List<DashboardDeliveryRequestFrontendShowEntity> GetDepotCategoryCount(string stationScode,DateTime DeadLineDate, int depotCategory);
        /// <summary>
        /// 取得站所該日的應配數量
        /// </summary>
        /// <param name="stationSCode">站所的scode</param>
        /// <param name="shipDate">寄送日</param>
        /// <returns></returns>
        int GetShouldDeliverCount(string stationSCode, DateTime shipDate);

        /// <summary>
        /// 取得站所應配日中，實際完成配送的清單
        /// </summary>
        /// <param name="stationSCode">站所的scode</param>
        /// <param name="shipDate">寄送日</param>
        /// <returns></returns>
        int GetActualDeliverCount(string stationSCode, DateTime shipDate);

        /// <summary>
        /// 取得站所的應配清單
        /// </summary>
        /// <param name="stationSCode">站所的scode</param>
        /// <param name="shipDate">寄送日</param>
        /// <returns></returns>
        List<DeliveryRequestEntity> GetShouldDeliver(string stationSCode, DateTime shipDate);

        /// <summary>
        /// 取得站所未在應配日配達的清單
        /// </summary>
        /// <param name="stationSCode">站所的scode</param>
        /// <param name="shipDate">寄送日</param>
        /// <param name="days">幾日配</param>
        /// <returns></returns>
        List<DeliveryRequestFrontendShowEntity> GetNotDeliverList(string stationSCode, DateTime shipDate, int days);

        /// <summary>
        /// 以站所別查詢相關的客戶
        /// </summary>
        /// <param name="stationCode"></param>
        /// <param name="startWith"></param>
        /// <returns></returns>
        KendoSelectCustomerEntity GetByStationAndStartWith(string stationCode, string startWith, bool addTotal = false);

		/// <summary>
        /// 以客戶代碼及寄送區間取得未成功配送的資料
        /// </summary>
        /// <param name="customerCode">客戶代碼</param>
        /// <param name="start">寄出起始時間</param>
        /// <param name="end">寄出迄止時間</param>
        /// <param name="deliveryErrorTypeConfig">銷號狀況</param>
        /// <returns>取得未成功的結果</returns>
        List<DeliveryRequestEntity> GetErrorListByCustomerCode(string customerCode, DateTime start, DateTime end, DeliveryErrorTypeConfig deliveryErrorTypeConfig);

        /// <summary>
        /// 以到著站及寄送時間區間取得未成功配送的資料
        /// </summary>
        /// <param name="stationSCode">到著站代碼</param>
        /// <param name="start">寄出起始時間</param>
        /// <param name="end">寄出迄止時間</param>
        /// <param name="deliveryErrorTypeConfig">銷號狀況</param>
        /// <returns></returns>
        List<DeliveryRequestEntity> GetErrorListByArriveCode(string stationSCode, DateTime start, DateTime end, DeliveryErrorTypeConfig deliveryErrorTypeConfig);

        List<GetDeliveryAnomalyShowEntity> GetFrontendErrorListByCustomerCode(string customerCode, DateTime start, DateTime end, DeliveryErrorTypeConfig deliveryErrorTypeConfig);

        List<GetDeliveryAnomalyShowEntity> GetFrontendErrorListByArriveCode(string stationSCode, DateTime start, DateTime end, DeliveryErrorTypeConfig deliveryErrorTypeConfig);

		/// <summary>
        /// 以客戶代碼及派遣時間區間取得逆物流資料 
        /// </summary>
        /// <param name="customerSCode">客戶代碼</param>
        /// <param name="start">派遣起始時間</param>
        /// <param name="end">派遣迄止時間</param>
        List<ReturnDeliveryShowEntity> GetFrontendReturnDeliveryListByCustomerCode(string customerCode, DateTime start, DateTime endd, PickUpOrNotTypeConfig pickUpOrNotTypeConfig);

        /// <summary>
        /// 以發送站代碼及派遣時間區間取得逆物流資料 
        /// </summary>
        /// <param name="sendStationSCode">發送站代碼</param>
        /// <param name="start">派遣起始時間</param>
        /// <param name="end">派遣迄止時間</param>
        List<ReturnDeliveryShowEntity> GetFrontendReturnDeliveryListBySendStationCode(string stationScode, DateTime start, DateTime endd, PickUpOrNotTypeConfig pickUpOrNotTypeConfig);
        /// <summary>
        /// 以派遣時間區間取得逆物流資料 
        /// </summary>
        /// <param name="start">派遣起始時間</param>
        /// <param name="end">派遣迄止時間</param>
        List<ReturnDeliveryShowEntity> GetFrontendReturnDeliveryListByDateZone(DateTime start, DateTime end, PickUpOrNotTypeConfig pickUpOrNotTypeConfig);

        /// <summary>
        /// 以託運單號取得結案或未結案Dictionary 
        /// </summary>
        /// <param name="check_number">託運單號</param>

        Dictionary<TcDeliveryRequest, bool> GetDeliveryClosedOrNotDictionary(List<TcDeliveryRequest> deliveryRequests);
        List<DeliveryRequestMenuVerifyEntity> GetNeedToVerifyDeliveryRequestByScanDate(DateTime startDate, DateTime endDate, string areaName, string scode);

        List<DeliveryRequestMenuVerifyEntity> GetNeedToVerifyDeliveryRequestByEmpCode(string empCode);

        List<DeliveryRequestMenuVerifyEntity> GetNeedToVerifyDeliveryRequestByCheckNumber(string checkNumber);

        List<WriteOffCheckListEntity> GetAllWriteOffCheckLists();

        List<WriteOffCheckListEntity> GetErrorWriteOffCheckLists();

        List<WriteOffCheckListEntity> GetNormalWriteOffCheckLists();

        void SaveMenuVerifyDeliveryRequest(DeliveryRequestMenuVerifyEntity deliveryRequestMenuVerifyEntity);

        TagImageBase64Entity GetTagImageBase64Entity(string checkNumber, string token);

        List<string> GetCheckNumbersByIds(int[] requestIds);

        void SaveDeliveryRequestMDSD(decimal[] requestIds);

        void SaveDeliveryRequestMDSDByCheckNumbers(IEnumerable<string> checkNumbers);

        /// <summary>
        /// 以地址的路段取得縣市區
        /// </summary>
        /// <param name="address">地址</param>
        /// <returns>縣市區</returns>
        IEnumerable<CityAreaEntity> GetCityAreaEntitiesByAddress(string address);

        /// <summary>
        /// 以 request_ids (逗點區隔) 取得 momo 逆物流倉庫客製到著碼
        /// </summary>
        /// <param name="requestIds"></param>
        /// <returns></returns>
        IEnumerable<AreaArriveCodeForLabelEntity> GetAreaArriveCodeByRequestIds(string requestIds);

        //IEnumerable<DriverPaymentDetailEntity> GetDriverPaymentDetialsByChechNums(IEnumerable<string> checkNumbers);

        /// <summary>
        /// 取得暫存的大量列印temp資訊
        /// </summary>
        /// <param name="infoKey"></param>
        /// <returns></returns>
        IEnumerable<AreaArriveCodeForLabelEntity> GetAreaArriveCodeForLabelEntitiesByInfoKey(string infoKey);

        /// <summary>
        /// 寫入暫存的大量列印temp資訊
        /// </summary>
        /// <param name="areaArriveCodeForLabelEntities"></param>
        /// <returns></returns>
        string SetAreaArriveCodeForLabelEntities(IEnumerable<AreaArriveCodeForLabelEntity> areaArriveCodeForLabelEntities);

        /// <summary>
        /// 直接寫入大量列印的temp資料
        /// </summary>
        /// <param name="requestIds"></param>
        /// <returns></returns>
        string SetAreaArriveCodeForLabelEntitiesByIds(string requestIds);
    }
}
