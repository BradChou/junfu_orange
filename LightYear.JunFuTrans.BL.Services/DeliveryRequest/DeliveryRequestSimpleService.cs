﻿using AutoMapper;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.BE.BusinessReport;
using LightYear.JunFuTrans.BL.BE.DeliveryRequest;
using LightYear.JunFuTrans.BL.BE.PickUpRequest;
using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.BL.BE.Statics;
using LightYear.JunFuTrans.BL.BE.Api;
using LightYear.JunFuTrans.BL.Services.Barcode;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.Repositories.Account;
using LightYear.JunFuTrans.DA.Repositories.PickUpRequest;
using LightYear.JunFuTrans.DA.Repositories.DeliveryException;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRequest;
using LightYear.JunFuTrans.DA.Repositories.Station;
using LightYear.JunFuTrans.DA.Repositories.Customer;
using LightYear.JunFuTrans.Utilities.Reports;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using LightYear.JunFuTrans.DA.Repositories.CbmSize;

namespace LightYear.JunFuTrans.BL.Services.DeliveryRequest
{
    public class DeliveryRequestSimpleService : IDeliveryRequestSimpleService
    {

        public IDeliveryRequestModifyRepository DeliveryRequestModifyRepository { get; set; }

        public IPickUpRequestRepository PickUpRequestRepository { get; set; }

        public ICheckNumberSDMappingRepository CheckNumberSDMappingRepository { get; set; }

        public IOrgAreaRepository OrgAreaRepository { get; set; }

        public IStationRepository StationRepository { get; set; }

        public ICustomerRepository CustomerRepository { get; set; }

        public IDriverRepository DriverRepository { get; set; }

        public IDeliveryScanLogRepository DeliveryScanLogRepository { get; set; }

        public IPickupRequestForApiuserRepository PickupRequestForApiuserRepository { get; set; }

        public ICheckNumberPreheadRepository CheckNumberPreheadRepository { get; set; }

        public IMapper Mapper { get; private set; }

        public ICbmSizeRepository CbmSizeRepository { get; set; }

        public DeliveryRequestSimpleService(
            IDeliveryRequestModifyRepository deliveryRequestModifyRepository,
            IPickUpRequestRepository pickUpRequestRepository,
            ICheckNumberSDMappingRepository checkNumberSDMappingRepository,
            IOrgAreaRepository orgAreaRepository,
            IStationRepository stationRepository,
            ICustomerRepository customerRepository,
            IDriverRepository driverRepository,
            IDeliveryScanLogRepository deliveryScanLogRepository,
            IPickupRequestForApiuserRepository pickupRequestForApiuserRepository,
            IMapper mapper,
            ICheckNumberPreheadRepository checkNumberPreheadRepository,
            ICbmSizeRepository cbmSizeRepository
            )
        {
            this.DeliveryRequestModifyRepository = deliveryRequestModifyRepository;
            this.PickUpRequestRepository = pickUpRequestRepository;
            this.CheckNumberSDMappingRepository = checkNumberSDMappingRepository;
            this.OrgAreaRepository = orgAreaRepository;
            this.StationRepository = stationRepository;
            this.CustomerRepository = customerRepository;
            this.DriverRepository = driverRepository;
            this.DeliveryScanLogRepository = deliveryScanLogRepository;
            this.PickupRequestForApiuserRepository = pickupRequestForApiuserRepository;
            this.Mapper = mapper;
            CheckNumberPreheadRepository = checkNumberPreheadRepository;
            CbmSizeRepository = cbmSizeRepository;
        }

        public List<EDIResultEntity> EDIAPI(string token, string getJson, bool addPickUpRequest = true)
        {
            List<EDIResultEntity> eDIResultEntities = new List<EDIResultEntity>();

            //先驗證token
            try
            {
                var json = Encoding.UTF8.GetString(Convert.FromBase64String(token));

                var payload = JsonConvert.DeserializeObject<PayloadEntity>(json);

                string customerCode = payload.info.account_code;

                //28800是utc
                Int64 nowSpan = Convert.ToInt32((DateTime.Now - new DateTime(1970, 1, 1)).TotalSeconds) - 28800 - 600;
                if (payload.exp < nowSpan)
                {
                    throw new Exception("token expire!");
                }

                string stopShippingCode = CustomerRepository.GetByCustomerCode(customerCode).StopShippingCode;
                if(stopShippingCode != "0")
                {
                    throw new Exception("the account code is not active");
                }

                var insertEDI = EDIAPIJsonInsert(getJson, customerCode, addPickUpRequest);

                eDIResultEntities.AddRange(insertEDI);
            }
            catch (Exception ex)
            {
                EDIResultEntity eDIResultEntity = new EDIResultEntity
                {
                    Result = false,
                    Msg = ex.Message
                };

                eDIResultEntities.Add(eDIResultEntity);
            }

            return eDIResultEntities;
        }

        private List<EDIResultEntity> EDIAPIJsonInsert(string getJson, string customCode = "", bool addPickUpRequest = true)
        {
            bool isShopee = customCode == "F1300600002" || customCode == "F2900210002" ? true : false;      //蝦皮與光年甲配都視為蝦皮

            List<EDIResultEntity> eDIResultEntities = new List<EDIResultEntity>();

            TcCbmSize[] cbmSizes = CbmSizeRepository.GetAllCbmSize();

            try
            {
                var deliveryRequestApiEntities = JsonConvert.DeserializeObject<List<DeliveryRequestApiEntity>>(getJson);


                foreach (DeliveryRequestApiEntity deliveryRequestApiEntity in deliveryRequestApiEntities)
                {
                    EDIResultEntity eDIResultEntity = new EDIResultEntity();

                    eDIResultEntity.Result = true;

                    try
                    {

                        if (customCode.Length > 0 && deliveryRequestApiEntity.CustomerCode.Length > 0 && deliveryRequestApiEntity.CustomerCode != customCode)
                        {
                            eDIResultEntity.OrderNumber = deliveryRequestApiEntity.OrderNumber;
                            eDIResultEntity.Result = false;
                            eDIResultEntity.Msg = "客戶代碼不一致";

                            eDIResultEntities.Add(eDIResultEntity);
                            continue;
                        }

                        //先轉為tcDeliveryRequestApiEntity
                        var tcDeliveryRequest = this.Mapper.Map<TcDeliveryRequest>(deliveryRequestApiEntity);

                        var customer = this.CustomerRepository.GetByCustomerCode(tcDeliveryRequest.CustomerCode);

                        eDIResultEntity.OrderNumber = tcDeliveryRequest.OrderNumber;

                        //無代碼代碼時，使用外部傳入的
                        if (deliveryRequestApiEntity.CustomerCode == null || deliveryRequestApiEntity.CustomerCode.Length == 0)
                        {
                            tcDeliveryRequest.CustomerCode = customCode;
                        }

                        //取得收件地址
                        //FSEAddressEntity fSEAddressEntity = new FSEAddressEntity(deliveryRequestApiEntity.ReceiverAddress);
                        var receiverCity = this.DeliveryRequestModifyRepository.GetAddressCityInfo(deliveryRequestApiEntity.ReceiverAddress);

                        tcDeliveryRequest.ReceiveCity = receiverCity.city;
                        tcDeliveryRequest.ReceiveArea = receiverCity.area;

                        string realReceiveAddress = deliveryRequestApiEntity.ReceiverAddress;

                        if( receiverCity == null || receiverCity.city == null || receiverCity.area == null )
                        {
                            eDIResultEntity.OrderNumber = deliveryRequestApiEntity.OrderNumber;
                            eDIResultEntity.Result = false;
                            eDIResultEntity.Msg = "無法正確取得收件人的行政區，將無法正確寄送";

                            eDIResultEntities.Add(eDIResultEntity);
                            continue;

                        }

                        if (receiverCity.city.Length > 0)
                        {
                            realReceiveAddress = realReceiveAddress.Replace(receiverCity.city, "");
                        }

                        if (receiverCity.area.Length > 0)
                        {
                            realReceiveAddress = realReceiveAddress.Replace(receiverCity.area, "");
                        }

                        tcDeliveryRequest.ReceiveAddress = realReceiveAddress;

                        //取得寄件地址
                        var sendCity = this.DeliveryRequestModifyRepository.GetAddressCityInfo(deliveryRequestApiEntity.SenderAddress);

                        tcDeliveryRequest.SendCity = sendCity.city;
                        tcDeliveryRequest.SendArea = sendCity.area;

                        string realSendAddress = deliveryRequestApiEntity.SenderAddress;

                        if (sendCity.city != null && sendCity.city.Length > 0)
                        {
                            realSendAddress = realSendAddress.Replace(sendCity.city, "");
                        }

                        if (sendCity.area != null && sendCity.area.Length > 0)
                        {
                            realSendAddress = realSendAddress.Replace(sendCity.area, "");
                        }

                        tcDeliveryRequest.SendAddress = realSendAddress;

                        //判斷取件站所(另外要再寫入取件相關表)
                        FSEAddressEntity fSEAddressEntitySend = new FSEAddressEntity(deliveryRequestApiEntity.SenderAddress);

                        var sendDatas = this.OrgAreaRepository.GetByFSEAddressEntity(fSEAddressEntitySend);

                        bool bNotSend = true;

                        if(fSEAddressEntitySend.OrgAddress == "桃園市龜山區航空城JUNFU物流倉")
                        {
                            bNotSend = false;
                        }

                        OrgArea orgAreaSend = new OrgArea();

                        if (sendDatas.Count > 0)
                        {
                            foreach (OrgArea orgArea in sendDatas)
                            {
                                //無站所或聯運區的排除
                                if (orgArea.StationScode != null && orgArea.StationScode != "99" && orgArea.StationScode != "95")
                                {
                                    orgAreaSend = orgArea;
                                    bNotSend = false;
                                    break;
                                }
                            }
                        }

                        if (bNotSend && addPickUpRequest == true)
                        {
                            throw new Exception("寄件地址不在服務範圍內！");
                        }

                        //取得托運單
                        var distributor = this.DeliveryRequestModifyRepository.GetDistributor(receiverCity.city, receiverCity.area);

                        if(!isShopee)
                        {
                            tcDeliveryRequest.SupplierCode = customer.SupplierCode;
                        }
                        else if (distributor.supplier_code != "*9聯運")
                        {
                            tcDeliveryRequest.SupplierCode = distributor.supplier_code;
                        }
                        
                        tcDeliveryRequest.AreaArriveCode = distributor.area_arrive_code;

                        eDIResultEntity.Area = distributor.supplier_name; 
                        eDIResultEntity.AreaId = distributor.area_arrive_code;

                        if (isShopee)
                        {
                            var station = this.DeliveryRequestModifyRepository.GetDistributor(sendCity.city, sendCity.area);
                            tcDeliveryRequest.SendStationScode = station.area_arrive_code;
                            if (tcDeliveryRequest.CbmSize != null)
                            {
                                TcCbmSize cbmSize = cbmSizes.Where(s => s.CbmId == tcDeliveryRequest.CbmSize.ToString()).FirstOrDefault();
                                if (cbmSize != null)
                                {
                                    tcDeliveryRequest.CbmLength = cbmSize.DefaultCbmLength;
                                    tcDeliveryRequest.CbmWidth = cbmSize.DefaultCbmWidth;
                                    tcDeliveryRequest.CbmHeight = cbmSize.DefaultCbmHeight;
                                    tcDeliveryRequest.CbmWeight = cbmSize.DefaultCbmWeight;
                                    tcDeliveryRequest.CbmCont = cbmSize.DefaultCbmCont;
                                }
                            }
                        }
                        else
                        {
                            tcDeliveryRequest.SendStationScode = customer.SupplierCode.Replace("F", "");
                        }

                        TcDeliveryRequest returnDeliveryRequest;

                        //代收貨款是否超過2萬
                        if (deliveryRequestApiEntity.CollectionMoney > 20000)
                        { 
                            throw new Exception("代收貨款超過20,000上限，請重新輸入"); 
                        }


                        //如果是蝦皮，先查是否已有重復的訂單
                        if ( isShopee && deliveryRequestApiEntity.OrderNumber != null && deliveryRequestApiEntity.OrderNumber.Length > 0)
                        {
                            //先驗證是否有重復訂單
                            var checkDeliveryRequest = this.DeliveryRequestModifyRepository.GetTcDeliveryRequestByCustomerCodeAndOrderNumber(customCode, deliveryRequestApiEntity.OrderNumber);

                            if( checkDeliveryRequest != null )
                            {
                                //查證資料是否集貨
                                var myScanLog = this.DeliveryScanLogRepository.GetByCheckNumber(checkDeliveryRequest.CheckNumber);

                                if( myScanLog == null || myScanLog.Count == 0 )
                                {
                                    returnDeliveryRequest = checkDeliveryRequest;
                                    returnDeliveryRequest.ReceiveCity = tcDeliveryRequest.ReceiveCity;
                                    returnDeliveryRequest.ReceiveArea = tcDeliveryRequest.ReceiveArea;
                                    returnDeliveryRequest.ReceiveAddress = tcDeliveryRequest.ReceiveAddress;
                                    returnDeliveryRequest.SendCity = tcDeliveryRequest.SendCity;
                                    returnDeliveryRequest.SendArea = tcDeliveryRequest.SendArea;
                                    returnDeliveryRequest.SendAddress = tcDeliveryRequest.SendAddress;
                                    returnDeliveryRequest.SupplierCode = tcDeliveryRequest.SupplierCode;
                                    returnDeliveryRequest.AreaArriveCode = tcDeliveryRequest.AreaArriveCode;
                                    returnDeliveryRequest.SendStationScode = tcDeliveryRequest.SendStationScode;
                                    returnDeliveryRequest.CbmSize = tcDeliveryRequest.CbmSize;
                                    returnDeliveryRequest.CbmLength = tcDeliveryRequest.CbmLength;
                                    returnDeliveryRequest.CbmWidth = tcDeliveryRequest.CbmWidth;
                                    returnDeliveryRequest.CbmHeight = tcDeliveryRequest.CbmHeight;
                                    returnDeliveryRequest.CbmWeight = tcDeliveryRequest.CbmWeight;
                                    returnDeliveryRequest.CbmCont = tcDeliveryRequest.CbmCont;
                                }
                                else
                                {
                                    throw new Exception("此訂單編號已存在並取件");
                                }
                            }
                            else
                            {
                                returnDeliveryRequest = this.DeliveryRequestModifyRepository.Insert(tcDeliveryRequest);
                            }
                        }
                        else if( isShopee)
                        {
                            throw new Exception("無訂單編號");
                        }
                        else
                        {
                            returnDeliveryRequest = this.DeliveryRequestModifyRepository.Insert(tcDeliveryRequest);
                        }

                        //計算託運單費用
                        var shipFeeInfo = this.DeliveryRequestModifyRepository.GetShipFee(Convert.ToInt64(returnDeliveryRequest.RequestId));

                        returnDeliveryRequest.SupplierFee = shipFeeInfo.supplier_fee;
                        returnDeliveryRequest.TotalFee = shipFeeInfo.supplier_fee;

                        // 預購袋扣掉貨號數量
                        if(customer.ProductType == 2 || customer.ProductType == 3)
                        {
                            JunFuWebServiceInService.WebServiceSoapClient webServiceSoapClient = new JunFuWebServiceInService.WebServiceSoapClient(JunFuWebServiceInService.WebServiceSoapClient.EndpointConfiguration.WebServiceSoap);
                            webServiceSoapClient.UpdateCheckNumberSettingAsync(customCode, 1);
                        }

                        //如果沒使用區間，主動產生check_number
                        if (deliveryRequestApiEntity.CheckNumber.Length == 0 && (returnDeliveryRequest.CheckNumber == null || returnDeliveryRequest.CheckNumber.Length == 0 ))
                        {
                            long checkNumberTemp = 100000000 + Convert.ToInt64(returnDeliveryRequest.RequestId);
                            int tail = Convert.ToInt32(returnDeliveryRequest.RequestId % 7);

                            string checkNumberPrefix = "";
                            if (isShopee)
                            {
                                checkNumberPrefix = "700";
                            }
                            else
                            {
                                checkNumberPrefix = CheckNumberPreheadRepository.GetPrefixByProductType(customer.ProductType.ToString());
                            }

                            string currentCheckNumber = string.Format("{0}{1}{2}", checkNumberPrefix, checkNumberTemp.ToString().Substring(1), tail.ToString());

                            returnDeliveryRequest.CheckNumber = currentCheckNumber;

                            eDIResultEntity.CheckNumber = currentCheckNumber;
                        }
                        else if( returnDeliveryRequest.CheckNumber != null && returnDeliveryRequest.CheckNumber.Length > 0 )
                        {
                            eDIResultEntity.CheckNumber = returnDeliveryRequest.CheckNumber;
                        }

                        //計算sdmd資訊
                        FSEAddressEntity fSEAddressEntity = new FSEAddressEntity(returnDeliveryRequest.ReceiveAddress, returnDeliveryRequest.ReceiveCity, returnDeliveryRequest.ReceiveArea);

                        var eachDatas = this.OrgAreaRepository.GetByFSEAddressEntity(fSEAddressEntity);

                        if (eachDatas.Count > 0)
                        {
                            CheckNumberSdMapping checkNumberSdMapping = new CheckNumberSdMapping
                            {
                                CheckNumber = returnDeliveryRequest.CheckNumber,
                                RequestId = (long)returnDeliveryRequest.RequestId,
                                OrgAreaId = eachDatas[0].Id,
                                Md = eachDatas[0].MdNo,
                                Sd = eachDatas[0].SdNo,
                                PutOrder = eachDatas[0].PutOrder,
                                Cdate = DateTime.Now,
                                Udate = DateTime.Now
                            };

                            eDIResultEntity.Md = checkNumberSdMapping.Md;
                            eDIResultEntity.Sd = checkNumberSdMapping.Sd;
                            eDIResultEntity.Putorder = checkNumberSdMapping.PutOrder;
                            //eDIResultEntity.Area = eachDatas[0].StationName;
                            //eDIResultEntity.AreaId = eachDatas[0].StationScode;

                            this.CheckNumberSDMappingRepository.Insert(checkNumberSdMapping);

                            //站所資訊
                            //var stationCode = this.StationRepository.GetStationCodeByStationScode(eachDatas[0].StationScode);
                            var stationCode = customer.SupplierCode;
                            returnDeliveryRequest.SupplierCode = stationCode;
                            returnDeliveryRequest.ReceiveZip = eachDatas[0].Zipcode;
                        }

                        //存檔
                        this.DeliveryRequestModifyRepository.Update(returnDeliveryRequest);

                        if (addPickUpRequest)
                        {
                            //產生取貨資料
                            if (isShopee)
                            {
                                PickupRequestForApiuser pickupRequestForApiuser = new PickupRequestForApiuser
                                {
                                    CheckNumber = returnDeliveryRequest.CheckNumber,
                                    CustomerCode = returnDeliveryRequest.CustomerCode,
                                    Md = orgAreaSend.MdNo,
                                    ReassignMd = orgAreaSend.MdNo,
                                    Pieces = (returnDeliveryRequest.Pieces == null || returnDeliveryRequest.Pieces == 0) ? returnDeliveryRequest.Plates : returnDeliveryRequest.Pieces,
                                    Putorder = orgAreaSend.PutOrder,
                                    RequestDate = DateTime.Now,
                                    Sd = orgAreaSend.SdNo,
                                    SendArea = orgAreaSend.Area,
                                    SendCity = orgAreaSend.City,
                                    SendRoad = fSEAddressEntitySend.TransAddress,
                                    SendTel = returnDeliveryRequest.SendTel,
                                    SupplierCode = this.StationRepository.GetStationCodeByStationScode(orgAreaSend.StationScode)
                                };

                                this.PickupRequestForApiuserRepository.Insert(pickupRequestForApiuser);
                            }
                            else
                            {
                                PickUpRequestLog pickUpRequestLog = new PickUpRequestLog();
                                pickUpRequestLog.RequestCustomerCode = deliveryRequestApiEntity.CustomerCode;
                                pickUpRequestLog.PickUpPieces = deliveryRequestApiEntity.Pieces;                            

                                PickUpRequestRepository.Insert(pickUpRequestLog);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        eDIResultEntity.Result = false;
                        eDIResultEntity.Msg = ex.ToString();
                    }

                    eDIResultEntities.Add(eDIResultEntity);
                }
            }
            catch (Exception ex)
            {
                EDIResultEntity eDIResultEntity = new EDIResultEntity
                {
                    Result = false,
                    Msg = ex.ToString()
                };

                eDIResultEntities.Add(eDIResultEntity);
            }

            return eDIResultEntities;
        }

        public TcDeliveryRequest GetRequest(long requestId)
        {
            var returnData = this.DeliveryRequestModifyRepository.SelectById(requestId);

            return returnData;
        }

        public List<ApiScanLogEntity> GetScanLogByCustomerCodeAndTimeZone(string customerCode, DateTime start, DateTime end, bool isCheckNumberNull = true)
        {
            List<ApiScanLogEntity> data = this.DeliveryScanLogRepository.GetApiScanLogByCustomerCodeAndTimeZone(customerCode, start, end, isCheckNumberNull);

            List<string> stationCodes = data.Select(d => d.stationCode).Distinct().ToList();

            var stations = this.StationRepository.GetTbStations(stationCodes);

            var driverCodes = data.Select(d => d.driverCode).Distinct().ToList();

            var drivers = this.DriverRepository.GetDriversByDriverCodes(driverCodes);

            var arriveCodes = this.DeliveryScanLogRepository.GetItemCode("AO");

            //針對有回應訊息進行修正
            arriveCodes["H"] = "貨故結案";

            var errorCodes = this.DeliveryScanLogRepository.GetItemCode("EO");

            List<ApiScanLogEntity> returnData = new List<ApiScanLogEntity>();

            foreach (ApiScanLogEntity apiScanLogEntity in data)
            {
                try
                {
                    apiScanLogEntity.driverName = (drivers.ContainsKey(apiScanLogEntity.driverCode.Trim())) ? drivers[apiScanLogEntity.driverCode.Trim()].DriverName : string.Empty;

                    apiScanLogEntity.stationName = (stations.ContainsKey(apiScanLogEntity.stationCode)) ? stations[apiScanLogEntity.stationCode].StationName : string.Empty;

                    if (apiScanLogEntity.deliveryType == "R")
                    {
                        //逆物流時，調整對應的逆物流單號

                        string reCheckNumber = apiScanLogEntity.checkNumber;

                        apiScanLogEntity.checkNumber = apiScanLogEntity.returnCheckNumber;

                        apiScanLogEntity.returnCheckNumber = reCheckNumber;
                    }

                    if (apiScanLogEntity.scanName == "3")
                    {
                        if (apiScanLogEntity.itemCodes != null)
                            apiScanLogEntity.status = (arriveCodes.ContainsKey(apiScanLogEntity.itemCodes)) ? arriveCodes[apiScanLogEntity.itemCodes] : string.Empty;
                    }
                    else //除了配達歷程之外，其他歷程不回傳狀態
                    {
                        if (apiScanLogEntity.itemCodes != null)
                            apiScanLogEntity.status = string.Empty;
                            apiScanLogEntity.itemCodes = string.Empty;
                    }

                    switch (apiScanLogEntity.scanName)
                    {
                        case "1":
                            apiScanLogEntity.scanName = "到著";
                            break;
                        case "2":
                            apiScanLogEntity.scanName = "配送";
                            break;
                        case "3":
                        case "4":
                            apiScanLogEntity.scanName = "配達";
                            break;
                        case "5":
                            apiScanLogEntity.scanName = "集貨";
                            break;
                        case "6":
                            apiScanLogEntity.scanName = "卸集";
                            break;
                        case "7":
                            apiScanLogEntity.scanName = "發送";
                            break;
                        default:
                            break;

                    }

                    //去除毫秒
                    if( apiScanLogEntity.scanDate != null && apiScanLogEntity.scanDate != DateTime.MinValue && apiScanLogEntity.scanDate.Millisecond > 0 )
                    {
                        apiScanLogEntity.scanDate = apiScanLogEntity.scanDate.AddMilliseconds(-apiScanLogEntity.scanDate.Millisecond);
                    }

                    returnData.Add(apiScanLogEntity);
                }
                catch
                {
                    continue;
                }
            }

            return returnData;
        }

        public ApiPickupLogReturnEntity GetPickupLogReturnEntityByCustomer(string customerCode)
        {
            //先取得過去24小時集貨的資料
            DateTime start = DateTime.Now.AddHours(-24);

            var pickedupData = this.DeliveryScanLogRepository.GetLastPickUpScan(customerCode, start);

            var pickedupCheckNumbers = (from request in pickedupData select request.CheckNumber).Distinct().ToList();

            var pickedupRequestInfo = this.DeliveryScanLogRepository.GetByCheckNumberList(pickedupCheckNumbers);

            //整理最後取件的時間
            Dictionary<string, DateTime> keyValuePairs = new Dictionary<string, DateTime>();
            Dictionary<string, string> driverCodes = new Dictionary<string, string>();
            Dictionary<string, string> notPickDrivers = new Dictionary<string, string>();
            Dictionary<string, int> checkNumberGetNum = new Dictionary<string, int>();
            Dictionary<string, bool> checkNumberInsert = new Dictionary<string, bool>();

            //取得司機資訊
            var driverCodeList = (from scan in pickedupData select scan.DriverCode).Distinct().ToList();
            var driverInfo = this.DriverRepository.GetDriversByDriverCodes(driverCodeList);

            //取得集貨失敗的代碼
            var receiveOptions = this.DeliveryScanLogRepository.GetReceiveOptionCode();
            Dictionary<string, string> receiveOptionsPairs = receiveOptions.ToDictionary(p => p.CodeId, p => p.CodeName);


            foreach (TtDeliveryScanLog ttDeliveryScanLog in pickedupData)
            {
                TcDeliveryRequest tcDeliveryRequest = pickedupRequestInfo[ttDeliveryScanLog.CheckNumber];

                string addressData = string.Format("{0}{1}{2}{3}", tcDeliveryRequest.SendContact, tcDeliveryRequest.SendCity, tcDeliveryRequest.SendArea, tcDeliveryRequest.SendAddress);

                //地址的最晚取件時間
                if (!keyValuePairs.ContainsKey(addressData))
                {
                    keyValuePairs.Add(addressData, (DateTime)ttDeliveryScanLog.ScanDate);
                    driverCodes.Add(addressData, ttDeliveryScanLog.DriverCode);
                }
                else if (keyValuePairs[addressData] < ttDeliveryScanLog.ScanDate)
                {
                    keyValuePairs[addressData] = (DateTime)ttDeliveryScanLog.ScanDate;
                    driverCodes[addressData] = ttDeliveryScanLog.DriverCode;
                }

                //取件個數
                int nPiece = 1;
                if (ttDeliveryScanLog.Pieces != null && ttDeliveryScanLog.Pieces != 0)
                {
                    nPiece = (int)ttDeliveryScanLog.Pieces;
                }

                if (!checkNumberGetNum.ContainsKey(ttDeliveryScanLog.CheckNumber))
                {
                    checkNumberGetNum.Add(ttDeliveryScanLog.CheckNumber, nPiece);
                }
                else
                {
                    checkNumberGetNum[ttDeliveryScanLog.CheckNumber] += nPiece;
                }

                //集貨成功才用集貨成功資訊
                bool pickupSuccess = false;

                if (ttDeliveryScanLog.ReceiveOption == null || ttDeliveryScanLog.ReceiveOption == string.Empty || ttDeliveryScanLog.ReceiveOption == "20")
                {
                    pickupSuccess = true;
                }

                //是否已放入輸出資料的初始化
                if (pickupSuccess && !checkNumberInsert.ContainsKey(ttDeliveryScanLog.CheckNumber))
                {
                    checkNumberInsert.Add(ttDeliveryScanLog.CheckNumber, false);
                }
            }

            //取得過去3天還沒集貨的資料
            DateTime putStart = DateTime.Now.AddDays(-3);
            var notPickedInfo = this.DeliveryScanLogRepository.GetLastNotPickUpScan(customerCode, putStart);

            List<TcDeliveryRequest> goAndNotGetData = new List<TcDeliveryRequest>();

            foreach (TcDeliveryRequest tcDeliveryRequest1 in notPickedInfo)
            {
                string addressData1 = string.Format("{0}{1}{2}{3}", tcDeliveryRequest1.SendContact, tcDeliveryRequest1.SendCity, tcDeliveryRequest1.SendArea, tcDeliveryRequest1.SendAddress);

                if (keyValuePairs.ContainsKey(addressData1) && keyValuePairs[addressData1] > tcDeliveryRequest1.Cdate)
                {
                    goAndNotGetData.Add(tcDeliveryRequest1);
                    notPickDrivers.Add(tcDeliveryRequest1.CheckNumber, driverCodes[addressData1]);
                }
            }

            //開始產生資訊
            List<ApiPickupLogEntity> returnData = new List<ApiPickupLogEntity>();

            //已集貨資訊
            foreach (TtDeliveryScanLog ttDeliveryScanLog1 in pickedupData)
            {
                //集貨成功才用集貨成功資訊
                bool pickupSuccess = false;

                if (ttDeliveryScanLog1.ReceiveOption == null || ttDeliveryScanLog1.ReceiveOption == string.Empty || ttDeliveryScanLog1.ReceiveOption == "20")
                {
                    pickupSuccess = true;
                }

                //成功集貨的
                if (pickupSuccess && checkNumberInsert.ContainsKey(ttDeliveryScanLog1.CheckNumber) && !checkNumberInsert[ttDeliveryScanLog1.CheckNumber])
                {
                    string myCustomerCode = (pickedupRequestInfo.ContainsKey(ttDeliveryScanLog1.CheckNumber)) ? pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].CustomerCode : customerCode;
                    string deliveryType = (pickedupRequestInfo.ContainsKey(ttDeliveryScanLog1.CheckNumber)) ? pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].DeliveryType : "無資訊";

                    string driverCode = ttDeliveryScanLog1.DriverCode ?? string.Empty;
                    string driverType = (driverInfo.ContainsKey(driverCode)) ? driverInfo[driverCode].DriverType : "CS";
                    string driverTypeCode = (driverInfo.ContainsKey(driverCode)) ? driverInfo[driverCode].DriverTypeCode : "0";
                    ttDeliveryScanLog1.ScanDate = RemoveMilliSecond(ttDeliveryScanLog1.ScanDate);
                    pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].ShipDate = RemoveMilliSecond(pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].ShipDate);

                    ApiPickupLogEntity apiPickupLogEntity = new ApiPickupLogEntity
                    {
                        ActualPickup = true,
                        ActualPickupPieces = checkNumberGetNum[ttDeliveryScanLog1.CheckNumber],
                        CheckNumber = ttDeliveryScanLog1.CheckNumber,
                        CustomerCode = myCustomerCode,
                        DeliveryType = deliveryType,
                        DriverCode = driverCode,
                        DriverType = driverType,
                        DriverTypeCode = driverTypeCode,
                        FalseItem = string.Empty,
                        ScanDate = ttDeliveryScanLog1.ScanDate,
                        ScanName = "集貨",
                        ShipDate = pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].ShipDate == null ? (DateTime)ttDeliveryScanLog1.ScanDate : (DateTime)pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].ShipDate,
                        ShouldPickupPieces = (int)pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].Pieces,
                        SupplierCode = pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].SupplierCode
                    };

                    returnData.Add(apiPickupLogEntity);

                    checkNumberInsert[ttDeliveryScanLog1.CheckNumber] = true;
                }

                //集貨失敗的情況
                if (!pickupSuccess)
                {
                    string myCustomerCode = (pickedupRequestInfo.ContainsKey(ttDeliveryScanLog1.CheckNumber)) ? pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].CustomerCode : customerCode;
                    string deliveryType = (pickedupRequestInfo.ContainsKey(ttDeliveryScanLog1.CheckNumber)) ? pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].DeliveryType : "無資訊";

                    string driverCode = ttDeliveryScanLog1.DriverCode ?? string.Empty;
                    string driverType = (driverInfo.ContainsKey(driverCode)) ? driverInfo[driverCode].DriverType : "CS";
                    string driverTypeCode = (driverInfo.ContainsKey(driverCode)) ? driverInfo[driverCode].DriverTypeCode : "0";
                    ttDeliveryScanLog1.ScanDate = RemoveMilliSecond(ttDeliveryScanLog1.ScanDate);
                    pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].ShipDate = RemoveMilliSecond(pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].ShipDate);

                    ApiPickupLogEntity apiPickupLogEntity = new ApiPickupLogEntity
                    {
                        ActualPickup = false,
                        ActualPickupPieces = checkNumberGetNum[ttDeliveryScanLog1.CheckNumber],
                        CheckNumber = ttDeliveryScanLog1.CheckNumber,
                        CustomerCode = myCustomerCode,
                        DeliveryType = deliveryType,
                        DriverCode = driverCode,
                        DriverType = driverType,
                        DriverTypeCode = driverTypeCode,
                        FalseItem = receiveOptionsPairs[ttDeliveryScanLog1.ReceiveOption],
                        ScanDate = ttDeliveryScanLog1.ScanDate,
                        ScanName = "集貨",
                        ShipDate = pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].ShipDate == null ? (DateTime)ttDeliveryScanLog1.ScanDate : (DateTime)pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].ShipDate,
                        ShouldPickupPieces = (int)pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].Pieces,
                        SupplierCode = pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].SupplierCode
                    };

                    returnData.Add(apiPickupLogEntity);
                }
            }

            //未集貨的部分，不主動回拋無件可取貨態
            //foreach (TcDeliveryRequest tcDeliveryRequest2 in goAndNotGetData)
            //{
            //    string driverCode = (notPickDrivers.ContainsKey(tcDeliveryRequest2.CheckNumber)) ? notPickDrivers[tcDeliveryRequest2.CheckNumber] : string.Empty;
            //    string driverType = (driverInfo.ContainsKey(driverCode)) ? driverInfo[driverCode].DriverType : "CS";
            //    string driverTypeCode = (driverInfo.ContainsKey(driverCode)) ? driverInfo[driverCode].DriverTypeCode : "0";

            //    string addressData2 = string.Format("{0}{1}{2}{3}", tcDeliveryRequest2.SendContact, tcDeliveryRequest2.SendCity, tcDeliveryRequest2.SendArea, tcDeliveryRequest2.SendAddress);
            //    keyValuePairs[addressData2] = (DateTime)RemoveMilliSecond(keyValuePairs[addressData2]);

            //    ApiPickupLogEntity apiPickupLogEntity = new ApiPickupLogEntity
            //    {
            //        ActualPickup = false,
            //        ActualPickupPieces = 0,
            //        CheckNumber = tcDeliveryRequest2.CheckNumber,
            //        CustomerCode = tcDeliveryRequest2.CustomerCode,
            //        DeliveryType = tcDeliveryRequest2.DeliveryType,
            //        DriverCode = driverCode,
            //        DriverType = driverType,
            //        DriverTypeCode = driverTypeCode,
            //        FalseItem = "無件可取",
            //        ScanDate = null,
            //        ScanName = "集貨",
            //        ShipDate = keyValuePairs[addressData2],
            //        ShouldPickupPieces = (int)tcDeliveryRequest2.Pieces,
            //        SupplierCode = tcDeliveryRequest2.SupplierCode
            //    };

            //    returnData.Add(apiPickupLogEntity);
            //}

            ApiPickupLogReturnEntity apiPickupLogReturnEntity = new ApiPickupLogReturnEntity();

            apiPickupLogReturnEntity.PickupInfo = returnData;

            return apiPickupLogReturnEntity;
        }

        public ApiCbmInfoReturnEntity GetMeasuredCbmInfo(string customerCode)
        {
            //取得過去24小時量的資訊
            /*DateTime start = DateTime.Now.AddHours(-24);
            DateTime end = DateTime.Now;*/

            DateTime now = DateTime.Now;
            DateTime yesterday = now.AddDays(-1);
            DateTime start = new DateTime(yesterday.Year, yesterday.Month, yesterday.Day, 5, 0, 0);
            DateTime end = new DateTime(now.Year, now.Month, now.Day, 5, 0, 0);

            var requests = this.DeliveryScanLogRepository.GetMeasuredInfo(customerCode, start, end);

            List<ApiCbmInfoEntity> apiCbmInfoEntities = new List<ApiCbmInfoEntity>();

            foreach (TcDeliveryRequest tcDeliveryRequest in requests)
            {
                if (tcDeliveryRequest.CbmLength == null || tcDeliveryRequest.CbmHeight == null || tcDeliveryRequest.CbmWidth == null)
                {
                    tcDeliveryRequest.CbmLength = 200;
                    tcDeliveryRequest.CbmHeight = 200;
                    tcDeliveryRequest.CbmWidth = 190;
                    tcDeliveryRequest.CbmCont = 0.3;
                    tcDeliveryRequest.CbmWeight = 1.6;
                }

                double sum = (double)tcDeliveryRequest.CbmLength + (double)tcDeliveryRequest.CbmHeight + (double)tcDeliveryRequest.CbmWidth;

                string size = "";

                if (sum <= 600)
                {
                    size = "S60";
                }
                else if (sum <= 900)
                {
                    size = "S90";
                }
                else if (sum <= 1200)
                {
                    size = "S120";
                }
                else 
                {
                    size = "S150";
                }
             


                ApiCbmInfoEntity apiCbmInfoEntity = new ApiCbmInfoEntity
                {
                    CheckNumber = tcDeliveryRequest.CheckNumber,
                    Size = size,
                    Length = (Double)tcDeliveryRequest.CbmLength,
                    Width = (Double)tcDeliveryRequest.CbmWidth,
                    Height = (Double)tcDeliveryRequest.CbmHeight,
                    SidesSum = sum,
                    Cbm = (Double)tcDeliveryRequest.CbmCont,
                    Weight = tcDeliveryRequest.CbmWeight ?? 0,
                    MeasurementTime = DateTime.Today
                };

                apiCbmInfoEntities.Add(apiCbmInfoEntity);
            }

            ApiCbmInfoReturnEntity apiCbmInfoReturnEntity = new ApiCbmInfoReturnEntity
            {
                CbmInfo = apiCbmInfoEntities
            };

            return apiCbmInfoReturnEntity;
        }
        private DateTime? RemoveMilliSecond(DateTime? dt)
        {
            if (dt != null)
                return ((DateTime)dt).AddMilliseconds(-((DateTime)dt).Millisecond);
            else
                return null;
        }
    }
}
