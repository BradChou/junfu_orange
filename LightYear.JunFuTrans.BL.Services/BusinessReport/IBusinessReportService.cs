﻿using LightYear.JunFuTrans.BL.BE.BusinessReport;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.BusinessReport
{
    public interface IBusinessReportService
    {
        List<BusinessReportEntity> PieceData(DateTime start, DateTime end, string areaArriveCode);
        List<AreaReportEntity> FrontendData(DateTime start, DateTime end, bool detailOrNot, string areaArriveCode);
        List<int> DaysAndSumAndAverage(StartAndEndEntity startAndEnd, List<AreaReportEntity> areaReportEntityList);
        List<StationEntity> StationEntities();
    }
}
