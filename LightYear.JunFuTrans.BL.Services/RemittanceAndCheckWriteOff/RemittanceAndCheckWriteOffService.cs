﻿using AutoMapper;
using LightYear.JunFuTrans.BL.BE.RemittanceAndCheckWriteOff;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.Repositories.RemittanceAndCheckWriteOff;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.RemittanceAndCheckWriteOff
{
    public class RemittanceAndCheckWriteOffService : IRemittanceAndCheckWriteOffService
    {
        public IMapper Mapper { get; private set; }
        IRemittanceAndCheckWriteOffRepository RemittanceAndCheckWriteOffRepository;

        public RemittanceAndCheckWriteOffService(IMapper mapper, IRemittanceAndCheckWriteOffRepository remittanceAndCheckWriteOffRepository)
        {
            Mapper = mapper;
            RemittanceAndCheckWriteOffRepository = remittanceAndCheckWriteOffRepository;
        }


        public List<RemittanceAndCheckWriteOffEntity> GetAllByDateAndStation(DateTime start, DateTime end, string scode)
        {
            List<RemittanceAndCheckWriteOffEntity> remittanceAndCheckWriteOffEntities = new List<RemittanceAndCheckWriteOffEntity>();

            if (scode == "-1" )
            {
                remittanceAndCheckWriteOffEntities = RemittanceAndCheckWriteOffRepository.GetAllByDate(start,end);
            }
            else 
            {
                remittanceAndCheckWriteOffEntities = RemittanceAndCheckWriteOffRepository.GetAllByDateAndStation(start,end,scode);
            }

            foreach (var entity in remittanceAndCheckWriteOffEntities)
            {
                if ((string.Compare(entity.CheckNumber, "103008900500") > 0
                    && string.Compare(entity.CheckNumber, "103008904999") < 0)
                    || (string.Compare(entity.CheckNumber, "600000000000") > 0
                    && string.Compare(entity.CheckNumber, "600000099999") < 0
                    ))
                {
                    entity.ReceiverType = "預購袋";
                }
                else
                {
                    entity.ReceiverType = "一般件";
                }

                entity.WriteOffDate = RemittanceAndCheckWriteOffRepository.GetWriteOffDateByRequestId(entity.RequestId);

                entity.LastFiveNumber = RemittanceAndCheckWriteOffRepository.GetLastFiveNumberByCheckNumber(entity.CheckNumber);

                    bool? answer = RemittanceAndCheckWriteOffRepository.IsPaymentWriteOffOrNot(entity.RequestId);

                    if (answer == true)
                    {
                        entity.IsWriteOffChinese = "是";
                    }
                    else
                    {
                        entity.IsWriteOffChinese = "否";
                    }
            }

            return remittanceAndCheckWriteOffEntities;
        }

        public void ChangePaymentWriteOffByRequestId(List<int> requestId , int writeoff)
        {
            List<PaymentWriteOff> paymentWriteOffs = RemittanceAndCheckWriteOffRepository.GetPaymentWriteOffByRequestId(requestId);

            if (paymentWriteOffs.Count > 0 ) //有request id
            {
                this.RemittanceAndCheckWriteOffRepository.UpdatePaymentWriteOffByRequestId(requestId, writeoff);
            }
            else
            {
                this.RemittanceAndCheckWriteOffRepository.InsertPaymentWriteOffByRequestId(requestId);
            }
        }
    }
}
