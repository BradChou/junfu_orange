﻿using LightYear.JunFuTrans.BL.BE.RemittanceAndCheckWriteOff;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.RemittanceAndCheckWriteOff
{
    public interface IRemittanceAndCheckWriteOffService
    {
        List<RemittanceAndCheckWriteOffEntity> GetAllByDateAndStation(DateTime start, DateTime end, string scode);
        void ChangePaymentWriteOffByRequestId(List<int> requestId, int writeoff);
    }
}
