﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.LightYearTest;
using LightYear.JunFuTrans.BL.BE.StudentTest;

namespace LightYear.JunFuTrans.BL.Services.StudentTest
{
    public interface IStudentService
    {
        List<PersonEntity> GetAllStudents();

        PersonEntity GetStudent(int id);

        void SaveStudent(PersonEntity personEntity);

        void DeleteStudent(int id);

    }
}
