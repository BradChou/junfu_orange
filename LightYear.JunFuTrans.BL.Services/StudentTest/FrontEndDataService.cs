﻿using LightYear.JunFuTrans.BL.BE.StudentTest;
using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.LightYearTest;
using LightYear.JunFuTrans.DA.Repositories.StudentTest;
using AutoMapper;

namespace LightYear.JunFuTrans.BL.Services.StudentTest
{
    public class FrontEndDataService : IFrontEndDataService
    {
        public ITestDataRepository TestDataRepository { get; set; }

        public IMapper Mapper { get; private set; }

        public FrontEndDataService(ITestDataRepository testDataRepository, IMapper mapper)
        {
            this.TestDataRepository = testDataRepository;
            this.Mapper = mapper;
        }

        public void DeleteFrontData(int id)
        {
            this.TestDataRepository.Delete(id);
        }

        public List<FrontEndDataEntity> GetAllFrontEndDatas()
        {
            var data = this.TestDataRepository.GetTestDatas();

            var frontEndResult = this.Mapper.Map<List<FrontEndDataEntity>>(data);

            return frontEndResult;
        }

        public FrontEndDataEntity GetFrontEndData(int id)
        {
            var data = this.TestDataRepository.GetTestData(id);

            var frontendDataEntity = this.Mapper.Map<FrontEndDataEntity>(data);

            return frontendDataEntity;
        }

        public int SaveFrontEndData(FrontEndDataEntity frontEndDataEntity)
        {
            var testData = this.Mapper.Map<TestData>(frontEndDataEntity);

            if (frontEndDataEntity.Id == 0)
            {
                int id = this.TestDataRepository.Insert(testData);

                return id;
            }
            else
            {
                this.TestDataRepository.Update(testData);

                return frontEndDataEntity.Id;
            }
        }
    }
}
