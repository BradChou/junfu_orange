﻿using LightYear.JunFuTrans.DA.LightYearTest;
using LightYear.JunFuTrans.DA.Repositories.StudentTest;
using LightYear.JunFuTrans.BL.BE;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.StudentTest;

namespace LightYear.JunFuTrans.BL.Services.StudentTest
{
    public class StudentService : IStudentService
    {
        public StudentService(IStudentRepository studentRepository, IMapper mapper)
        {
            this.StudentRepository = studentRepository;
            this.Mapper = mapper;
        }

        public IStudentRepository StudentRepository { get; set; }

        public IMapper Mapper { get; private set; }

        public List<PersonEntity> GetAllStudents()
        {
            var data = this.StudentRepository.GetStudents();

            var personResult = this.Mapper.Map<List<PersonEntity>>(data);

            return personResult;
        }

        public PersonEntity GetStudent(int id)
        {
            var data = this.StudentRepository.GetStudent(id);

            var personEntity = this.Mapper.Map<PersonEntity>(data);

            return personEntity;
        }

        public void SaveStudent(PersonEntity personEntity)
        {
            var student = this.Mapper.Map<Student>(personEntity);

            if( personEntity.Id == 0 || personEntity.Id == null)
            {
                this.StudentRepository.Insert(student);
            }
            else
            {
                this.StudentRepository.Update(student);
            }
        }

        public void DeleteStudent(int id)
        {
            this.StudentRepository.Delete(id);
        }

    }
}
