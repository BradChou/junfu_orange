﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.StudentTest;

namespace LightYear.JunFuTrans.BL.Services.StudentTest
{
    public interface IFrontEndDataService
    {
        List<FrontEndDataEntity> GetAllFrontEndDatas();

        FrontEndDataEntity GetFrontEndData(int id);

        int SaveFrontEndData(FrontEndDataEntity frontEndDataEntity);

        void DeleteFrontData(int id);
    }
}
