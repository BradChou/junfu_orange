﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.PickUpRequest;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.BL.Services.PickUpInfo
{
    public interface IPickUpInfoService
    {
        List<PickUpInfoShowEntity> GetFrontendPickUpEntitiesByStationCode(string stationCode, DateTime start, DateTime end, string customerCode, int isPickUped);
        
        List<PickUpInfoShowEntity> GetFrontendPickUpEntitiesBySendArea(string stationCode, DateTime start, DateTime end, string customerCode, int isPickUped);

        IEnumerable<PickUpOptionsEntity> GetItemCodesOptionsAllowToEdit();

        bool UpdateRoInfo(IEnumerable<PickUpInfoShowEntity> updateData, string accountCode, string isNormal);
    }
}
