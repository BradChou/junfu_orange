﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using AutoMapper;
using LightYear.JunFuTrans.BL.BE.DeliveryRequest;
using LightYear.JunFuTrans.BL.BE.PickUpRequest;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.Repositories.Account;
using LightYear.JunFuTrans.DA.Repositories.Customer;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRequest;
using LightYear.JunFuTrans.DA.Repositories.PickUpRequest;

namespace LightYear.JunFuTrans.BL.Services.PickUpInfo
{
    public class PickUpInfoService : IPickUpInfoService
    {
        public IPickUpRequestRepository PickUpRequestRepository { get; set; }

        public IDriverRepository DriverRepository { get; set; }

        public ICustomerRepository CustomerRepository { get; set; }

        public IDeliveryRequestJoinDeliveryScanLogRepository JoinedRepository { get; set; }

        public IMapper Mapper { get; set; }

        public IPickupRequestForApiuserRepository PickupRequestForApiuserRepository { get; set; }

        public IDeliveryRequestRepository DeliveryRequestRepository { get; set; }

        public IAccountsRepository AccountsRepository { get; set; }

        public PickUpInfoService(IPickUpRequestRepository pickUpRequestRepository, IMapper mapper, IDriverRepository driverRepository,
            ICustomerRepository customerRepository, IDeliveryRequestJoinDeliveryScanLogRepository joinedRequestRepository, IPickupRequestForApiuserRepository pickupRequestApi,
            IDeliveryRequestRepository deliveryRequestRepository, IAccountsRepository accountsRepository)
        {
            PickUpRequestRepository = pickUpRequestRepository;
            Mapper = mapper;
            DriverRepository = driverRepository;
            CustomerRepository = customerRepository;
            JoinedRepository = joinedRequestRepository;
            PickupRequestForApiuserRepository = pickupRequestApi;
            DeliveryRequestRepository = deliveryRequestRepository;
            AccountsRepository = accountsRepository;
        }

        public List<PickUpInfoShowEntity> GetFrontendPickUpEntitiesByStationCode(string stationCode, DateTime start, DateTime end, string customerCode, int isPickUped)
        {
            start = start.AddHours(-8);
            end = end.AddDays(1).AddHours(-8);

            IEnumerable<PickUpRequestLog> pickUpRequestLog;
            TbStation[] stations = PickUpRequestRepository.GetAllStations();
            if (customerCode != null)
                pickUpRequestLog = PickUpRequestRepository.GetPickUpRequestByStationAndRequestTimePeriodAndCustomerCode(stationCode, start, end, customerCode);
            else if (stationCode != "-1")
                pickUpRequestLog = PickUpRequestRepository.GetPickUpRequestByStationAndRequestTimePeriod(stationCode, start, end);
            else
                pickUpRequestLog = PickUpRequestRepository.GetPickUpRequestByRequestTimePeriod(start, end);

            List<PickUpInfoShowEntity> showEntity = Mapper.Map<List<PickUpInfoShowEntity>>(pickUpRequestLog);

            foreach (var i in showEntity)
            {
                i.PickUpStation = stations.Where(s => s.StationCode == i.CustomerCode.Substring(0, 3)).FirstOrDefault()?.StationName ?? "";
            }

            return FillShowEntity(showEntity, start, end, isPickUped);
        }

        public List<PickUpInfoShowEntity> GetFrontendPickUpEntitiesBySendArea(string stationCode, DateTime start, DateTime end, string customerCode, int isPickUped)
        {
            // 甲配日期區間為 D-2 17:00 ~ D-1 17:00 (2021/05/05訂)
            start = start.AddDays(-1).AddHours(-7);
            end = end.AddHours(-7);

            List<PickUpInfoShowEntity> showEntity = new List<PickUpInfoShowEntity>();
            TbStation[] stations = PickUpRequestRepository.GetAllStations();

            // API客戶
            IEnumerable<PickUpInfoShowEntity> pickupRequestForApiusers;
            if (customerCode != null && stationCode != "-1" && stationCode != null)
                pickupRequestForApiusers = PickupRequestForApiuserRepository.GetByTimePeriodAndCustomerCodeAndStation(start, end, customerCode, stationCode);
            else if (stationCode != "-1" && stationCode != null)
                pickupRequestForApiusers = PickupRequestForApiuserRepository.GetByTimePeriodAndStation(start, end, stationCode);
            else if (customerCode != null)
                pickupRequestForApiusers = PickupRequestForApiuserRepository.GetByTimePeriodAndCustomerCode(start, end, customerCode);
            else
                pickupRequestForApiusers = PickupRequestForApiuserRepository.GetByTimePeriod(start, end);

            showEntity.AddRange(Mapper.Map<List<PickUpInfoShowEntity>>(pickupRequestForApiusers));

            foreach (var i in showEntity)
            {
                i.PickUpStation = stations.Where(s => s.StationCode == i.PickUpStation).FirstOrDefault()?.StationName ?? "";
            }

            return FillShowEntity(showEntity, start, end, isPickUped);
        }

        private List<PickUpInfoShowEntity> FillShowEntity(List<PickUpInfoShowEntity> showEntity, DateTime start, DateTime end, int isPickUped)
        {
            var customerCodes = showEntity.Select(e => e.CustomerCode).ToList();
            Dictionary<string, TbCustomer> customers = CustomerRepository.GetCustomersByCustomerCodes(customerCodes);
            var actualPickUpAmount = JoinedRepository.GetPickUpAmountByCustomersAndDate(customerCodes, start.Date, end.Date);
            string[] checkNumbers = showEntity.Where(s => s.CheckNumber != null && s.CheckNumber.Length > 0).Select(x => x.CheckNumber).ToArray();
            IEnumerable<TtDeliveryScanLog> scanLogs = PickUpRequestRepository.GetPickUpInfoByCheckNumber(checkNumbers);
            List<TtDeliveryScanLog> latestScanLogs = new List<TtDeliveryScanLog>();
            int number;
            int index = 0;

            for (int c = 0; c < checkNumbers.Length; c++)
            {
                if (scanLogs.Count() > 0)
                {
                    var pickUpDateTimeForCheckNumber = scanLogs.Where(s => s.CheckNumber == checkNumbers[c]);
                    if (pickUpDateTimeForCheckNumber.Count() > 0)
                    {
                        var latestPickUpDateTimeForCheckNumber = pickUpDateTimeForCheckNumber.Max(x => x.Cdate ?? new DateTime());
                        var latestScanLog = scanLogs.Where(s => s.CheckNumber == checkNumbers[c] && s.Cdate == latestPickUpDateTimeForCheckNumber).FirstOrDefault();
                        latestScanLogs.Add(latestScanLog);
                    }
                }
            }

            for (int i = 0; i < showEntity.Count; i++)
            {
                showEntity[i].PickUpAvailTime = GetEnumDescription((PickUpTime)(int.TryParse(showEntity[i].PickUpAvailTime, out number) ? number : 99));
                showEntity[i].VehicleType = GetEnumDescription((PickUpVechile)(int.TryParse(showEntity[i].VehicleType, out number) ? number : 99));
                showEntity[i].ShipType = GetEnumDescription((PackageType)(int.TryParse(showEntity[i].ShipType, out number) ? number : 99));

                if (customers.ContainsKey(showEntity[i].CustomerCode))
                {
                    showEntity[i].CustomerName = customers[showEntity[i].CustomerCode].CustomerName;

                    if (showEntity[i].CheckNumber == null || showEntity[i].CheckNumber.Length == 0)
                    {
                        showEntity[i].AssignedMd = customers[showEntity[i].CustomerCode].AssignedMd;
                        showEntity[i].AssignedSd = customers[showEntity[i].CustomerCode].AssignedSd;
                        showEntity[i].SendContact = customers[showEntity[i].CustomerCode].Contact1;
                        showEntity[i].SendPhone = customers[showEntity[i].CustomerCode].Telephone;
                        showEntity[i].SendAddress = customers[showEntity[i].CustomerCode].ShipmentsCity + customers[showEntity[i].CustomerCode].ShipmentsArea + customers[showEntity[i].CustomerCode].ShipmentsRoad;
                    }
                }

                if (showEntity[i].CheckNumber == null || showEntity[i].CheckNumber.Length == 0)
                {
                    if (actualPickUpAmount.ContainsKey(new Tuple<DateTime, string>(showEntity[i].NotifyDate.Date, showEntity[i].CustomerCode)))
                        showEntity[i].ActualPickUpPieces = actualPickUpAmount[new Tuple<DateTime, string>(showEntity[i].NotifyDate.Date, showEntity[i].CustomerCode)];
                }
                else
                {
                    var request = DeliveryRequestRepository.GetByCheckNumber(showEntity[i].CheckNumber);
                    showEntity[i].ActualPickUpPieces = (request.ShipDate != null) ? 1 : 0;
                    showEntity[i].SendContact = request.SendContact;
                    showEntity[i].SendPhone = request.SendTel;
                    showEntity[i].SendAddress = request.SendCity + request.SendArea + request.SendAddress;
                    showEntity[i].Ruser = request.OrangeR11Uuser;
                }

                if (showEntity[i].CheckNumber != null && showEntity[i].CheckNumber.Length > 0 && latestScanLogs.Count() > 0 && showEntity[i].ActualPickUpPieces == 0)
                {
                    var latestScanLogsByCheckNumber = latestScanLogs.Where(x => x.CheckNumber == showEntity[i].CheckNumber);
                    if (latestScanLogsByCheckNumber.Count() > 0)
                    {
                        showEntity[i].StatusInCode = latestScanLogsByCheckNumber.FirstOrDefault().ReceiveOption;
                    }
                }

                showEntity[i].IsAllowEdit = showEntity[i].ActualPickUpPieces == 0 && showEntity[i].CheckNumber != null && showEntity[i].CheckNumber.Length > 0;

                if (isPickUped == 0 && showEntity[i].ActualPickUpPieces == 0)
                {
                    index++;
                    showEntity[i].No = index;
                }
                else if (isPickUped == 1 && showEntity[i].ActualPickUpPieces > 0)
                {
                    index++;
                    showEntity[i].No = index;
                }
                else if (isPickUped == -1)
                {
                    index++;
                    showEntity[i].No = index;
                }
                else
                {
                    showEntity[i].No = 0;
                }

                if (showEntity[i].MdUpdateUser != null && showEntity[i].MdUpdateUser.Length > 0)
                {
                    string accountCode = showEntity[i].MdUpdateUser;
                    string accountName = AccountsRepository.GetByAccountCode(accountCode)?.UserName;

                    showEntity[i].MdUpdateUser = $"{accountCode} {accountName}";
                }
            }

            return showEntity.Where(s => s.No != 0).ToList();
        }

        private static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes = fi.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];

            if (attributes != null && attributes.Any())
            {
                return attributes.First().Description;
            }

            return value.ToString();
        }

        public IEnumerable<PickUpOptionsEntity> GetItemCodesOptionsAllowToEdit()
        {
            var options = PickUpRequestRepository.GetTbItemCodesAllowToEdit();
            return Mapper.Map<IEnumerable<PickUpOptionsEntity>>(options);
        }

        public bool UpdateRoInfo(IEnumerable<PickUpInfoShowEntity> updateData, string accountCode, string isNormal)
        {
            foreach (var s in updateData)   //整批寫入不會有OptionSelector
            {
                if (s.OptionSelector == null)
                {
                    s.OptionSelector = new PickUpOptionsEntity { OptionId = s.StatusInCode, Name = s.StatusInName };
                }
            }

            var statusChangedUpdateData = updateData.Where(u => u.OptionSelector.OptionId != null).ToList();
            if (statusChangedUpdateData.Count > 0)
            {
                IEnumerable<TtDeliveryScanLog> scanLogs = Mapper.Map<IEnumerable<TtDeliveryScanLog>>(statusChangedUpdateData);
                foreach (var s in scanLogs)
                {
                    s.Cdate = DateTime.Now;
                    s.ScanDate = DateTime.Now;
                    s.DriverCode = accountCode;
                    s.ScanItem = "5";
                }

                PickUpRequestRepository.InsertIntoScanLogRo(scanLogs);
                PickUpRequestRepository.UpdateDeliveryRequests(statusChangedUpdateData.Select(s => s.CheckNumber), accountCode);
            }

            if (updateData.First().ReassignMd != "0")
            {
                if (isNormal == "2")
                {
                    PickUpRequestLog log = Mapper.Map<PickUpRequestLog>(updateData.First());
                    log.MdUuser = accountCode;
                    PickUpRequestRepository.UpdatePickupRequest(log);
                }
                else
                {
                    PickupRequestForApiuser log = Mapper.Map<PickupRequestForApiuser>(updateData.First());
                    log.MdUuser = accountCode;
                    PickUpRequestRepository.UpdatePickupRequestForApi(log);
                }
            }

            return true;
        }
    }
}
