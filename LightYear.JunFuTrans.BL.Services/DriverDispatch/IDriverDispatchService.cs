﻿using LightYear.JunFuTrans.BL.BE.DriverDispatch;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.DriverDispatch
{
    public interface IDriverDispatchService
    {
        List<TbItemCode> GetScanItemOption(string scanItem);
        List<DriverDispatchEntity> GetDispatchPickUpList(string driverCode);
        List<CheckNumberListWithinDispathEntity> GetCheckNumberListWithinDispath(DispatchJsonEntity dispatchJsonEntity);
        Driver GetDriverMDSD(string driver);
        void UpdateMDSD(string customerCode, string checkNumber, string md, string sd);
    }
}
