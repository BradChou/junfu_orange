﻿
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.BE.BusinessReport;
using LightYear.JunFuTrans.BL.BE.DeliveryRequest;
using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.BL.BE.Statics;
using LightYear.JunFuTrans.BL.BE.StationArea;
using LightYear.JunFuTrans.BL.Services.DeliveryRequest;
using LightYear.JunFuTrans.BL.Services.StationWithArea;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRequest;
using LightYear.JunFuTrans.DA.Repositories.Station;
using LightYear.JunFuTrans.Utilities.Reports;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.Dashboard
{
    public class DashboardService : IDashboardService
    {
        public IDeliveryRequestService DeliveryRequestService { get; set; }
        public IStationRepository StationRepository { get; set; }

        public int TotalShouldDeliveryCount { get; set; }

        public int TotalNotYetNum { get; set; }

        public decimal SuccessRate { get; set; }

    public int TotalDPlus3Depot { get; set; }
        public DashboardService(
            IDeliveryRequestService deliveryRequestService,
            IStationRepository stationRepository
            )
        {
            this.DeliveryRequestService = deliveryRequestService;
            this.StationRepository = stationRepository;
            this.TotalShouldDeliveryCount = 0;
            this.TotalNotYetNum = 0;
            this.SuccessRate = 1;
            this.TotalDPlus3Depot = 0;
        }


        public List<DashboardStationStatisticsShowFrontendEntity> GetAllStationStatistics()
        {
            DateTime checkDateTime = DateTime.Today;
            List<DashboardStationStatisticsShowFrontendEntity> result = new List<DashboardStationStatisticsShowFrontendEntity>();
            List<TbStation> stationList = StationRepository.GetAll();
            //string[] stationScodeNeedToBeCount = new string[] { "10","13"};
            string[] stationScodeNeedToBeCount = new string[] { "10", "13", "14", "15", "16", "17", "18", "20", "21", "22", "23", "24", "30", "32", "33", "34", "35", "37", "38", "40", "42", "43", "50", "60", "62", "70", "72", "73", "80", "81", "82", "83", "85", "91", "92", "99" };
            foreach (var station in stationList)
                if (stationScodeNeedToBeCount.Contains(station.StationScode)) { //只抓全速配直營或加盟，濾掉營業處等等
                    {   
                        int total = this.DeliveryRequestService.GetDPlusOneShouldDeliveryCount(station.StationScode, checkDateTime);
                        int successNum = this.DeliveryRequestService.GetDPlusOneSuccessDeliveryCount(station.StationScode, checkDateTime);
                        decimal successRate = (total == 0) ? 0 : (successNum * 100 / total);
                        int dPlusThreeDepotCount = this.DeliveryRequestService.GetDepotCount(station.StationScode, checkDateTime)[3];

                        DashboardStationStatisticsShowFrontendEntity entity = new DashboardStationStatisticsShowFrontendEntity();
                        entity.Station = station.StationScode + station.StationName;
                        entity.ShouldDeliveryCount = total; 
                        entity.NonDeliveredFinishCount = (total - successNum);
                        entity.DPlusOneDeliveryRate = successRate;
                        entity.DPlusThreeDepotCount = dPlusThreeDepotCount;
                        System.Diagnostics.Debug.WriteLine(entity.Station);
                        System.Diagnostics.Debug.WriteLine(entity.ShouldDeliveryCount);
                        System.Diagnostics.Debug.WriteLine(entity.NonDeliveredFinishCount);
                        System.Diagnostics.Debug.WriteLine(entity.DPlusOneDeliveryRate);
                        System.Diagnostics.Debug.WriteLine(entity.DPlusThreeDepotCount);
                        result.Add(entity);
                    }
                }
            this.TotalShouldDeliveryCount = 1000;
            this.TotalNotYetNum = 300;
            this.SuccessRate = 87;
            this.TotalDPlus3Depot = 187;
            System.Diagnostics.Debug.WriteLine(this.TotalShouldDeliveryCount);
            System.Diagnostics.Debug.WriteLine(this.TotalNotYetNum);
            System.Diagnostics.Debug.WriteLine(this.SuccessRate);
            System.Diagnostics.Debug.WriteLine(this.TotalDPlus3Depot);
            return result;
        }
    }
}
