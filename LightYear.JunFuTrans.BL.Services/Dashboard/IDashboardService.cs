﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.DeliveryRequest;
using LightYear.JunFuTrans.BL.BE.Statics;
using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.BL.Services.Dashboard
{
    public interface IDashboardService
    {
        int TotalShouldDeliveryCount { get; set; }

        public int TotalNotYetNum { get; set; }

        public decimal SuccessRate { get; set; }

        public int TotalDPlus3Depot { get; set; }
        /// <summary>
        /// 取得各站所應配筆數，未配筆數，D+1配達率，D+3留庫未結筆數
        /// </summary>
        /// <returns></returns>
        /// 
        public List<DashboardStationStatisticsShowFrontendEntity> GetAllStationStatistics();
    }
}
