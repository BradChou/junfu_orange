﻿using AutoMapper;
using LightYear.JunFuTrans.BL.BE.DriverToStationPayment;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.Repositories.Account;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRequest;
using LightYear.JunFuTrans.DA.Repositories.DriverPayment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.DriverToStationPayment
{
    public class StationCollectMoneyService : IStationCollectMoneyService
    {
        public IDriverPaymentRepository DriverPaymentRepository { get; set; }

        public IDriverPaymentDetailRepository DriverPaymentDetailRepository { get; set; }

        public IDriverRepository DriverRepository { get; set; }

        public IDeliveryRequestRepository DeliveryRequestRepository { get; set; }

        public IDeliveryScanLogRepository DeliveryScanLogRepository { get; set; }

        public IMapper Mapper { get; set; }

        public StationCollectMoneyService(IDriverPaymentRepository driverPaymentRepository, IMapper mapper, IDriverPaymentDetailRepository driverPaymentDetailRepository,
            IDriverRepository driverResoritory, IDeliveryRequestRepository deliveryRequestRepository, IDeliveryScanLogRepository deliveryScanLogRepository)
        {
            DriverPaymentRepository = driverPaymentRepository;
            Mapper = mapper;
            DriverPaymentDetailRepository = driverPaymentDetailRepository;
            DriverRepository = driverResoritory;
            DeliveryRequestRepository = deliveryRequestRepository;
            DeliveryScanLogRepository = deliveryScanLogRepository;
        }

        public IEnumerable<StationCollectMoneyEntity> GetStationCollectMoneyByTimeAndStation(DateTime date, string stationScode)
        {
            var drivers = DriverRepository.GetDriversByStation(stationScode).ToList();
            var deliverySuccessScanLog = DeliveryScanLogRepository.GetDeliverySuccessByDriversAndDate(drivers.Select(d => d.DriverCode), date.Date);
            var matingCheckNumAndDriver = deliverySuccessScanLog.Select(s => new { DriverCode = s.DriverCode, CheckNumber = s.CheckNumber }).Distinct().ToList();
            Dictionary<string, int> checkNumberAndCollectionMoney = DeliveryRequestRepository.GetCollectionMoneyBatch(matingCheckNumAndDriver.Select(d => d.CheckNumber));

            var driverShouldPay = (from s in matingCheckNumAndDriver
                                   join d in drivers on s.DriverCode equals d.DriverCode
                                   group s by new { DriverCode = s.DriverCode.ToUpper(), DriverName = d.DriverName } into g
                                   select new StationCollectMoneyEntity
                                   {
                                       DriverCode = g.Key.DriverCode,
                                       DriverName = g.Key.DriverName,
                                       TotalShouldPay = g.Sum(d => checkNumberAndCollectionMoney[d.CheckNumber])
                                   }).ToList();
           
            // 取得司機上次的精算書
            List<StationCollectMoneyEntity> previousPaymentBalance = new List<StationCollectMoneyEntity>();
            foreach (var e in driverShouldPay)
            {
                previousPaymentBalance.Add(new StationCollectMoneyEntity
                {
                    DriverCode = e.DriverCode,
                    PreviousPaymentBalance = DriverPaymentRepository.GetPreviousPaymentBalanceBeforeTime(date, e.DriverCode)
                });
            }

            // 當日的精算書
            IEnumerable<DriverPaymentSheet> paymentSheets = DriverPaymentRepository.GetListByTimeAndStation(date.Date, stationScode);
            var paymentSheetData = from p in paymentSheets
                                   group p by p.DriverCode into g
                                   select new StationCollectMoneyEntity
                                   {
                                       DriverCode = g.Key,
                                       NowPayAmount = g.Max(s => s.AcutalPayAmount ?? 0),
                                       CurrentPaymentBalace = g.Max(s => s.PaymentBalance ?? 0),
                                       TransactionLastFive = g.Max(s => s.TransactionLastFive),
                                       Date = g.Min(s => s.CreateDate ?? DateTime.MinValue),
                                       CashOnDelivery = g.Sum(p => p.CashOnDeliveryMoney ?? 0),
                                       CheckOnDelivery = g.Sum(p => p.CheckOnDelivery ?? 0),
                                       TotalShouldPay = g.Sum(p => (p.CheckOnDelivery ?? 0) + (p.CashOnDeliveryMoney ?? 0)),
                                       PaidTotal = g.Sum(p => (p.RemittanceAmount ?? 0) + (p.CheckAmount ?? 0) + (p.CoinsMoney ?? 0))
                                   };

            List<StationCollectMoneyEntity> stationCollectMoneyEntities = new List<StationCollectMoneyEntity>();
            if (paymentSheetData.Any())
            {
                stationCollectMoneyEntities =
                 (from dsp in driverShouldPay
                  join ppb in previousPaymentBalance on dsp.DriverCode equals ppb.DriverCode into tempb
                  from ppb in tempb.DefaultIfEmpty()
                  join ps in paymentSheetData on dsp.DriverCode equals ps.DriverCode into temp
                  from ps in temp.DefaultIfEmpty()
                  select new StationCollectMoneyEntity
                  {
                      Date = date.Date,
                      DriverCode = dsp.DriverCode,
                      DriverName = dsp.DriverName,
                      TransactionLastFive = ps.TransactionLastFive,
                      CashOnDelivery = dsp.TotalShouldPay,
                      CheckOnDelivery = 0,  // 先假設沒有支票
                      TotalShouldPay = dsp.TotalShouldPay,
                      PaidTotal = ps.PaidTotal,
                      PreviousPaymentBalance = ppb.PreviousPaymentBalance,
                      HasCreatedSheet = ps != null,
                      CurrentPaymentBalace = ps.CurrentPaymentBalace,
                      AccumulatedPaymentBalance = ps.AccumulatedPaymentBalance,
                      NowPayAmount = ps.NowPayAmount
                  }).ToList();
            }
            else
            {
                stationCollectMoneyEntities =
                 (from dsp in driverShouldPay
                  join ppb in previousPaymentBalance on dsp.DriverCode equals ppb.DriverCode into tempb
                  from ppb in tempb.DefaultIfEmpty()
                  select new StationCollectMoneyEntity
                  {
                      Date = date.Date,
                      DriverCode = dsp.DriverCode,
                      DriverName = dsp.DriverName,                      
                      CashOnDelivery = dsp.TotalShouldPay, // 先假設沒有支票
                      CheckOnDelivery = 0,
                      TotalShouldPay = dsp.TotalShouldPay,
                      //TransactionLastFive = ps.TransactionLastFive,
                      //PaidTotal = ps.PaidTotal,
                      PreviousPaymentBalance = ppb.PreviousPaymentBalance,
                      HasCreatedSheet = false
                  }).ToList();
            }

            for (int i = 0; i < stationCollectMoneyEntities.Count(); i++)
            {
                if(stationCollectMoneyEntities[i].CurrentPaymentBalace == 0)
                    stationCollectMoneyEntities[i].CurrentPaymentBalace = stationCollectMoneyEntities[i].TotalShouldPay - stationCollectMoneyEntities[i].PaidTotal - stationCollectMoneyEntities[i].NowPayAmount;
            }

            return stationCollectMoneyEntities;
        }

        public int UpdatePaymentAndPaymentBalance(StationCollectMoneyEntity entity)
        {
            return DriverPaymentRepository.UpdateActualPaidMoney(entity.DriverCode, entity.Date, entity.NowPayAmount, entity.CurrentPaymentBalace, entity.AccumulatedPaymentBalance);
        }
    }
}
