﻿using AutoMapper;
using LightYear.JunFuTrans.BL.BE.DeliveryRequest;
using LightYear.JunFuTrans.BL.BE.DriverToStationPayment;
using LightYear.JunFuTrans.BL.Services.DeliveryRequest;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.Repositories.Account;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRequest;
using LightYear.JunFuTrans.DA.Repositories.DriverPayment;
using LightYear.JunFuTrans.DA.Repositories.Station;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using LightYear.JunFuTrans.DA.Repositories.Customer;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace LightYear.JunFuTrans.BL.Services.DriverToStationPayment
{
    public class DriverPaymentService : IDriverPaymentService
    {

        public IDeliveryScanLogRepository DeliveryScanLogRepository { get; set; }

        public IDeliveryRequestRepository DeliveryRequestRepository { get; set; }

        public IStationRepository StationRepository { get; set; }

        public IDeliveryRequestService DeliveryRequestService { get; set; }

        public IDriverRepository DriverRepository { get; set; }

        public IDriverPaymentRepository DriverPaymentRepository { get; set; }

        public IDriverPaymentDetailRepository DriverPaymentDetailRepository { get; private set; }

        public ICustomerRepository CustomerRepository { get; set; }

        public IMapper Mapper { get; private set; }

        public DriverPaymentService
            (IDriverPaymentRepository driverPaymentRepository, IDeliveryScanLogRepository deliveryScanLogRepository,
            IDeliveryRequestService deliveryRequestService, IStationRepository stationRepository, IDriverRepository driverRepository,
            IDriverPaymentDetailRepository driverPaymentDetailRepository, ICustomerRepository customerRepository, IMapper mapper, IDeliveryRequestRepository deliveryRequestRepository)
        {
            DriverPaymentRepository = driverPaymentRepository;
            DeliveryScanLogRepository = deliveryScanLogRepository;
            StationRepository = stationRepository;
            DeliveryRequestService = deliveryRequestService;
            DriverRepository = driverRepository;
            DriverPaymentDetailRepository = driverPaymentDetailRepository;
            CustomerRepository = customerRepository;
            Mapper = mapper;
            DeliveryRequestRepository = deliveryRequestRepository;
        }

        public ViewCreatedSheetEntity GetDriverTodayCreatedSheet(string driverCode)
        {
            TbDriver driver = DriverRepository.GetByDriverCode(driverCode);

            if (driver == null)
            {
                return null;
            }

            ViewCreatedSheetEntity driverTodayCreatedSheet = new ViewCreatedSheetEntity
            {
                DriverCode = driverCode,
                DriverName = driver.DriverName,
                StationScode = driver.Station
            };

            if (driver.Station != null && driver.Station.Length > 0)
            {
                driverTodayCreatedSheet.StationName = StationRepository.GetByScode(driver.Station).StationName;
            }

            // 今日成功配達
            List<string> todaysSuccessDeliveryCheckNum = DeliveryScanLogRepository.GetDriverDeliverySuccessByDriverAfterTime(driverCode, DateTime.Today);
            //IEnumerable<TcDeliveryRequest> todaysSuccessDeliveryRequest = DeliveryRequestRepository.GetByCheckNumberList(todaysSuccessDeliveryCheckNum);
            //IEnumerable<DriverPaymentDetailEntity> todaysDeliverySuccessDetailEntities = Mapper.Map<IEnumerable<DriverPaymentDetailEntity>>(todaysSuccessDeliveryRequest);
            DriverPaymentDetailEntity[] todaysDeliverySuccessDetailEntities = GetDetailEntityByChekNum(todaysSuccessDeliveryCheckNum);

            CategoryCountEntity ondeliveryCategoryCount = CalculateCategoryCount(todaysDeliverySuccessDetailEntities);

            // 司機應繳總和
            driverTodayCreatedSheet.TodaysShouldCollectMoney = ondeliveryCategoryCount.CashOnDeliveryMoney + ondeliveryCategoryCount.DestinationCashMoney + ondeliveryCategoryCount.DestinationCheckMoney
                + ondeliveryCategoryCount.PreOrderBagCashMoney + ondeliveryCategoryCount.PreOrderBagCheckMoney + ondeliveryCategoryCount.YuanFuMoney;

            driverTodayCreatedSheet.CategoryCount = ondeliveryCategoryCount;

            var todaysCreatedSheets = CheckTodaysCreated(driverCode);
            driverTodayCreatedSheet.TodaysPaymentSheet = todaysCreatedSheets.ToArray();

            int todaysPayedMoney = 0;
            foreach (var sheet in driverTodayCreatedSheet.TodaysPaymentSheet)
            {
                todaysPayedMoney += sheet.RemittanceAmount ?? 0;
                todaysPayedMoney += sheet.CheckAmount ?? 0;
                todaysPayedMoney += sheet.CoinsAmount ?? 0;
            }
            driverTodayCreatedSheet.TodaysPaidMoney = todaysPayedMoney;
            driverTodayCreatedSheet.Difference = driverTodayCreatedSheet.TodaysShouldCollectMoney - todaysPayedMoney;

            return driverTodayCreatedSheet;
        }

        public IEnumerable<DriverPaymentDetailEntity> GetDriverTodayCreatedDetails(string driverCode)
        {
            var paymentSheetIds = DriverPaymentRepository.GetPaymentSheetByDriverAndDate(driverCode, DateTime.Today).Select(s => s.Id);
            var paymentDetial = DriverPaymentDetailRepository.GetDetailBySheetIds(paymentSheetIds.ToList());

            return Mapper.Map<IEnumerable<DriverPaymentDetailEntity>>(paymentDetial);
        }

        public IEnumerable<DriverPaymentDetailEntity> GetPaymentDetialEntityExceptCreated(string driverCode)
        {
            var todayCreatedSheetCheckNum = GetDriverTodayCreatedDetails(driverCode).Select(d => d.CheckNumber).ToList();

            // 今日配達扣掉已製作精算書
            var todayDeliverySuccess = DeliveryScanLogRepository.GetDriverDeliverySuccessByDriverAfterTime(driverCode, DateTime.Today);
            var subtractionChechNums = todayDeliverySuccess.Except(todayCreatedSheetCheckNum).ToList();

            var driverPaymentDetailEntity = GetDetailEntityByChekNum(subtractionChechNums);

            return driverPaymentDetailEntity.Where(r => r.SubpoenaCategory != "11");
        }

        private IEnumerable<DriverPaymentEntity> CheckTodaysCreated(string driverCode)
        {
            IEnumerable<DriverPaymentSheet> sheets = DriverPaymentRepository.GetTodaySheetsByDriver(driverCode);
            IEnumerable<DriverPaymentEntity> driverPaymentEntities = Mapper.Map<IEnumerable<DriverPaymentEntity>>(sheets);
            foreach (var e in driverPaymentEntities)
            {
                e.PaidTotal = (e.RemittanceAmount ?? 0) + (e.CheckAmount ?? 0) + (e.CoinsAmount ?? 0);
            }
            return driverPaymentEntities;
        }

        public DriverPaymentDetailEntity[] GetDetailEntityByChekNum(List<string> checkNumList)
        {
            List<DriverPaymentDetailEntity> entitiesList = new List<DriverPaymentDetailEntity>();
            IEnumerable<TcDeliveryRequest> entities = DeliveryRequestRepository.GetByCheckNumberList(checkNumList);
            Dictionary<string, string> ticketLastFive = DriverPaymentDetailRepository.GetTicketLastFiveByCheckNubmers(checkNumList);
            foreach (var entity in entities)
            {
                if (entity != null)
                {
                    DriverPaymentDetailEntity detail = Mapper.Map<DriverPaymentDetailEntity>(entity);
                    if (ticketLastFive.ContainsKey(entity.CheckNumber))
                        detail.TicketOrRemittanceLastFive = ticketLastFive[entity.CheckNumber];

                    // 防止客戶打單時選錯類別
                    if (detail.CollectionMoney > 0 && detail.SubpoenaCategory == "11")
                    {
                        detail.SubpoenaCategory = "41";
                        detail.SubpoenaCategoryShowName = "11 元付";
                    }

                    if (detail.CheckNumber.StartsWith("600") && detail.SubpoenaCategory != "31")
                    {
                        detail.SubpoenaCategory = "31";
                        detail.SubpoenaCategoryShowName = "31 預購袋";
                    }

                    entitiesList.Add(detail);
                }
            }
            return entitiesList.ToArray();
        }

        public CategoryCountEntity CalculateCategoryCount(DriverPaymentDetailEntity[] detailEntity)
        {
            int yuanFuCount = 0;
            int yuanFuMoney = 0;

            int cashOnDeliveryCount = 0;
            int cashOnDeliveryMoney = 0;

            int destinationCollectCashCount = 0;
            int destinationCollectCheckCount = 0;
            int destinationCollectRemittanceCount = 0;
            int destinationCollectCash = 0;
            int destinationCollectCheck = 0;
            int destinationCollectRemittance = 0;

            int preOrderBagCashCount = 0;
            int preOrderBagCheckCount = 0;
            int preOrderBagRemittanceCount = 0;
            int preOrderBagCash = 0;
            int preOrderBagCheck = 0;
            int preOrderBagRemittance = 0;

            foreach (var detail in detailEntity)
            {
                if (detail.CheckNumber.StartsWith("600"))
                {
                    switch (detail.PaidMethod)
                    {
                        case "匯款":
                            preOrderBagRemittanceCount++;
                            preOrderBagRemittance += detail.CollectionMoney;
                            break;
                        case "支票":
                            preOrderBagCheckCount++;
                            preOrderBagCheck += detail.CollectionMoney;
                            break;
                        default:
                            preOrderBagCashCount++;
                            preOrderBagCash += detail.CollectionMoney;
                            break;
                    }
                    continue;
                }

                switch (detail.SubpoenaCategory)
                {
                    case "11":  // 元付
                        yuanFuCount++;
                        yuanFuMoney += detail.CollectionMoney;
                        break;
                    case "41":  // 代收
                        cashOnDeliveryCount++;
                        cashOnDeliveryMoney += detail.CollectionMoney;
                        break;
                    case "51":  // 到付
                        switch (detail.PaidMethod)
                        {
                            case "匯款":
                                destinationCollectRemittanceCount++;
                                destinationCollectRemittance += detail.CollectionMoney;
                                break;
                            case "支票":
                                destinationCollectCheckCount++;
                                destinationCollectCheck += detail.CollectionMoney;
                                break;
                            default:
                                destinationCollectCashCount++;
                                destinationCollectCash += detail.CollectionMoney;
                                break;
                        }
                        break;
                    case "31":  // 預購袋先用貨號判斷
                        switch (detail.PaidMethod)
                        {
                            case "匯款":
                                preOrderBagRemittanceCount++;
                                preOrderBagRemittance += detail.CollectionMoney;
                                break;
                            case "支票":
                                preOrderBagCheckCount++;
                                preOrderBagCheck += detail.CollectionMoney;
                                break;
                            default:
                                preOrderBagCashCount++;
                                preOrderBagCash += detail.CollectionMoney;
                                break;
                        }
                        break;
                }
            }

            CategoryCountEntity result = new CategoryCountEntity
            {
                YuanFuCount = yuanFuCount,
                YuanFuMoney = yuanFuMoney,

                CashOnDeliveryCount = cashOnDeliveryCount,
                CashOnDeliveryMoney = cashOnDeliveryMoney,

                DestinationCashCount = destinationCollectCashCount,
                DestinationCashMoney = destinationCollectCash,
                DestinationCheckCount = destinationCollectCheckCount,
                DestinationCheckMoney = destinationCollectCheck,
                DestinationRemittanceCount = destinationCollectRemittanceCount,
                DestinationRemittanceMoney = destinationCollectRemittance,

                PreOrderBagCashCount = preOrderBagCashCount,
                PreOrderBagCashMoney = preOrderBagCash,
                PreOrderBagCheckCount = preOrderBagCheckCount,
                PreOrderBagCheckMoney = preOrderBagCheck,
                PreOrderBagRemittanceCount = preOrderBagRemittanceCount,
                PreOrderBagRemittanceMoney = preOrderBagRemittance
            };

            return result;
        }

        public PrintFormatCategoryEntity CalculatePrintFormatCategory(DriverPaymentDetailEntity[] detailEntity, string category)
        {
            int cashCount = 0;
            int remittanceCount = 0;
            int checkCount = 0;
            int cashMoney = 0;
            int remittanceMoney = 0;
            int checkMoney = 0;

            foreach (var detail in detailEntity)
            {
                switch (detail.PaidMethod)
                {
                    case "匯款":
                        remittanceCount++;
                        remittanceMoney += detail.CollectionMoney;
                        break;
                    case "支票":
                        checkCount++;
                        checkMoney += detail.CollectionMoney;
                        break;
                    default:
                        cashCount++;
                        cashMoney += detail.CollectionMoney;
                        break;
                }
            }

            PrintFormatCategoryEntity result = new PrintFormatCategoryEntity
            {
                Category = category,
                CashCount = cashCount,
                CashMoney = cashMoney,
                RemittanceCount = remittanceCount,
                RemittanceMoney = remittanceMoney,
                CheckCount = checkCount,
                CheckMoney = checkMoney
            };

            return result;
        }

        public int SaveDriverPayment(DriverPaymentEntity entity)
        {
            var payment = this.Mapper.Map<DriverPaymentSheet>(entity);

            var paymentDetails = this.Mapper.Map<List<DriverPaymentDetail>>(entity.Details);

            int id = this.DriverPaymentRepository.Insert(payment);

            foreach (DriverPaymentDetail paymentDetail in paymentDetails)
            {
                paymentDetail.PaymentSheetId = id;

                this.DriverPaymentDetailRepository.Insert(paymentDetail);
            }

            return id;
        }

        public DriverPaymentEntity GetPaymentSheetById(int paymentId)
        {
            var paymentSheet = DriverPaymentRepository.GetPaymentSheetById(paymentId);
            var paymentSheetEntity = Mapper.Map<DriverPaymentEntity>(paymentSheet);
            List<DriverPaymentDetail> details = DriverPaymentDetailRepository.GetDetailBySheetId(paymentId);
            List<DriverPaymentDetailEntity> detailEntities = Mapper.Map<List<DriverPaymentDetailEntity>>(details);
            paymentSheetEntity.Details = detailEntities.ToArray();

            return paymentSheetEntity;
        }

        public IEnumerable<PaymentSheetPrintFormatEntity> GetPaymentSheetByDriverAndDate(string driverCode, DateTime date)
        {
            List<PaymentSheetPrintFormatEntity> result = new List<PaymentSheetPrintFormatEntity>();

            IEnumerable<DriverPaymentSheet> paymentSheets = DriverPaymentRepository.GetPaymentSheetByDriverAndDate(driverCode, date);
            var sheetIds = paymentSheets.Select(s => s.Id);
            IEnumerable<DriverPaymentDetail> paymentDetials = DriverPaymentDetailRepository.GetDetialsBySheetIds(sheetIds);
            var checkNumbers = paymentDetials.Select(d => d.CheckNumber).ToList();
            IEnumerable<DriverPaymentDetailEntity> detailEntity = GetDetailEntityByChekNum(checkNumbers);

            List<string> arriveStations = detailEntity.Select(d => d.AreaArriveCode).Distinct().ToList();

            foreach(var e in arriveStations)
            {
                result.AddRange(GenerateSheetInfo(detailEntity.Where(d => d.AreaArriveCode == e), paymentSheets, e));
            }

            return result;
        }

        private List<PaymentSheetPrintFormatEntity> GenerateSheetInfo(IEnumerable<DriverPaymentDetailEntity> detailEntity, IEnumerable<DriverPaymentSheet> paymentSheets, string areaArriveCode)
        {
            List<PaymentSheetPrintFormatEntity> result = new List<PaymentSheetPrintFormatEntity>();

            IEnumerable<DriverPaymentDetailEntity> detailEntityWithoutBag = detailEntity.Where(d => d.SubpoenaCategory != "31");
            IEnumerable<DriverPaymentDetailEntity> detailEntityForBag = detailEntity.Where(d => d.SubpoenaCategory == "31");

            PrintFormatCategoryEntity[] entityWithoutPreOrderBag = new PrintFormatCategoryEntity[]
                    { CalculatePrintFormatCategory(detailEntityWithoutBag.Where(d => d.SubpoenaCategory == "51").ToArray(), "應收帳款"),
                        CalculatePrintFormatCategory(detailEntityWithoutBag.Where(d => d.SubpoenaCategory == "41").ToArray(), "代收貨款") };

            PrintFormatCategoryEntity[] entityForPreOrderBag = new PrintFormatCategoryEntity[] { CalculatePrintFormatCategory(detailEntityForBag.ToArray(), "預購袋") };

            var paidHistories = Mapper.Map<List<PaidHistoryEntity>>(paymentSheets);

            PaymentSheetPrintFormatEntity CSSheet = new PaymentSheetPrintFormatEntity
            {
                DriverName = paymentSheets.LastOrDefault().DriverName,
                StationName = StationRepository.GetStationNameByScode(areaArriveCode),
                Details = detailEntityWithoutBag.ToArray(),
                CountSum = detailEntityWithoutBag.Count(),
                MoneySum = detailEntityWithoutBag.Sum(d => d.CollectionMoney),
                RemittanceSum = detailEntityWithoutBag.Where(d => d.PaidMethod == "匯款").Sum(d => d.CollectionMoney),
                CreateDate = paymentSheets.Select(s => s.CreateDate).Max() ?? DateTime.MinValue,
                CategoryCount = entityWithoutPreOrderBag,
                PaidHistories = paidHistories.ToArray(),
                ForWho = "所長/CS 收執聯",
                TotalShouldPay = entityWithoutPreOrderBag.Select(e => e.CashMoney + e.CheckMoney).Sum(),
                PreOrderBagTotalShouldPay = entityForPreOrderBag.Select(e => e.CashMoney + e.CheckMoney).Sum()
            };
            PaymentSheetPrintFormatEntity driverSheet = CSSheet.ShallowCopy();
            driverSheet.ForWho = "繳款人留存聯";

            result.Add(CSSheet);
            result.Add(driverSheet);

            if (detailEntityForBag.Any())
            {
                PaymentSheetPrintFormatEntity CSPreOrderBagSheet = CSSheet.ShallowCopy();
                CSPreOrderBagSheet.Details = detailEntityForBag.ToArray();
                CSPreOrderBagSheet.CountSum = detailEntityForBag.Count();
                CSPreOrderBagSheet.MoneySum = detailEntityForBag.Sum(d => d.CollectionMoney);
                CSPreOrderBagSheet.RemittanceSum = detailEntityForBag.Where(d => d.PaidMethod == "匯款").Sum(d => d.CollectionMoney);
                CSPreOrderBagSheet.CategoryCount = new PrintFormatCategoryEntity[] { CalculatePrintFormatCategory(detailEntityForBag.ToArray(), "預購袋") };
                
                PaymentSheetPrintFormatEntity driverPreOrderBagSheet = CSPreOrderBagSheet.ShallowCopy();
                driverPreOrderBagSheet.ForWho = "繳款人留存聯";

                result.Add(CSPreOrderBagSheet);
                result.Add(driverPreOrderBagSheet);
            }

            return result;
        }

        public List<DriverPaymentEntity> GetListByStationAndTime(DateTime start, DateTime end, string station)
        {
            if (station.Equals("全選"))
            {
                var allStation = DriverPaymentRepository.GetListByTime(start, end);
                return Mapper.Map<List<DriverPaymentEntity>>(allStation);
            }
            var data = DriverPaymentRepository.GetListByTimePeriodAndStation(start, end, station);
            return Mapper.Map<List<DriverPaymentEntity>>(data);
        }

        public DriverPaymentDetailEntity[] RemoveNoMoneyList(DriverPaymentDetailEntity[] detailEntity)
        {
            List<DriverPaymentDetailEntity> result = new List<DriverPaymentDetailEntity>();

            foreach (var item in detailEntity)
            {
                if (item.SubpoenaCategory == "41")
                    result.Add(item);
            }
            return result.ToArray();
        }

        public List<string> GetAllStationName()
        {
            List<TbStation> stationList = StationRepository.GetAll();
            List<string> stationNames = new List<string>();
            stationNames.Add("全選");
            foreach (var s in stationList)
            {
                stationNames.Add(s.StationName);
            }
            return stationNames;
        }

        public List<CashFlowDailyEntity> GetCashFlowDailyEntityListByStationAndTime(DateTime start, DateTime end, string stationSCode)
        {
            List<DriverPaymentSheet> driverPaymentSheets = new List<DriverPaymentSheet>();

            if (stationSCode == string.Empty || stationSCode == "-1")
            {
                driverPaymentSheets = this.DriverPaymentRepository.GetListByTimePeriod(start, end);
            }
            else
            {
                driverPaymentSheets = this.DriverPaymentRepository.GetListByTimePeriodAndStationScode(start, end, stationSCode);
            }

            var dicDriverPaymentSheets = driverPaymentSheets.ToDictionary(a => a.Id);

            List<int> driverPaymentSheetIds = driverPaymentSheets.Select(a => a.Id).ToList();

            var driverPaymentDetails = this.DriverPaymentDetailRepository.GetDetialsBySheetIds(driverPaymentSheetIds);

            var checkNumbers = driverPaymentDetails.Select(a => a.CheckNumber).Distinct().ToList();

            var deliveryRequestInfos = this.DeliveryRequestRepository.GetByCheckNumberList(checkNumbers);

            var dicDeliveryRequestInfos = deliveryRequestInfos.ToDictionary(a => a.CheckNumber);

            var sendStationSCodes = deliveryRequestInfos.Select(a => a.SendStationScode).Distinct().ToList();

            var sendStationInfos = this.StationRepository.GetTbStations(sendStationSCodes);

            var arriveStationSCodes = deliveryRequestInfos.Select(a => a.AreaArriveCode).Distinct().ToList();

            var arriveStationInfos = this.StationRepository.GetTbStations(arriveStationSCodes);

            var customerCodes = deliveryRequestInfos.Select(a => a.CustomerCode).Distinct().ToList();

            var customerInfos = this.CustomerRepository.GetCustomersByCustomerCodes(customerCodes);

            List<CashFlowDailyEntity> output = new List<CashFlowDailyEntity>();

            foreach (DriverPaymentDetail driverPaymentDetail in driverPaymentDetails)
            {
                string arriveStationName = string.Empty;
                string sendStationName = string.Empty;
                string senderName = string.Empty;
                string receiverName = string.Empty;
                string receiveType = "一般件";

                DriverPaymentSheet driverPaymentSheet = dicDriverPaymentSheets[driverPaymentDetail.PaymentSheetId];

                TcDeliveryRequest deliveryRequest = dicDeliveryRequestInfos[driverPaymentDetail.CheckNumber];

                if (deliveryRequest != null)
                {
                    if (deliveryRequest.SendStationScode != null && deliveryRequest.SendStationScode.Length > 0)
                    {
                        var sendStation = sendStationInfos[deliveryRequest.SendStationScode];
                        if (sendStation != null)
                        {
                            sendStationName = sendStation.StationName;
                        }
                    }

                    if (deliveryRequest.AreaArriveCode != null && deliveryRequest.AreaArriveCode.Length > 0)
                    {
                        var arriveStation = arriveStationInfos[deliveryRequest.AreaArriveCode];
                        if (arriveStation != null)
                        {
                            arriveStationName = arriveStation.StationName;
                        }
                    }

                    var customer = customerInfos[deliveryRequest.CustomerCode];

                    if (customer != null)
                    {
                        senderName = customer.CustomerName;
                    }

                    receiverName = deliveryRequest.ReceiveContact;

                    if ((string.Compare(driverPaymentDetail.CheckNumber, "103008900500") > 0 && string.Compare(driverPaymentDetail.CheckNumber, "103008904999") < 0) || deliveryRequest.CustomerCode == "F4900000102")
                    {
                        receiveType = "預購袋";
                    }
                }

                if (driverPaymentSheet.StationName != null && driverPaymentSheet.StationName.Length > 0)
                {
                    arriveStationName = driverPaymentSheet.StationName;
                }

                CashFlowDailyEntity cashFlowDailyEntity = new CashFlowDailyEntity
                {
                    DriverPaymentDetailId = driverPaymentDetail.Id,
                    DriverPaymentSheetId = driverPaymentDetail.PaymentSheetId,
                    ArriveStationName = arriveStationName,
                    DriverName = driverPaymentSheet.DriverName,
                    RemittanceAmount = Convert.ToDouble(driverPaymentSheet.RemittanceAmount ?? 0),
                    TransactionLastFive = driverPaymentSheet.TransactionLastFive,
                    CheckNumber = driverPaymentDetail.CheckNumber,
                    SendStationName = sendStationName,
                    SenderName = senderName,
                    ReceiverName = receiverName,
                    CollectionMoney = Convert.ToDouble(driverPaymentDetail.CollectionMoney ?? 0),
                    ReceiverType = receiveType
                };

                output.Add(cashFlowDailyEntity);
            }

            return output;
        }
    }
}
