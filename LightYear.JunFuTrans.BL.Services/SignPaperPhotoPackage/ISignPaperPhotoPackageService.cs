﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.BL.BE.SignPaperPhoto;

namespace LightYear.JunFuTrans.BL.Services.SignPaperPhotoPackage
{
    public interface ISignPaperPhotoPackageService
    {
        IEnumerable<SignPaperPhotoOutputEntity> GetArrivedDeliveryRequest(string customerCode, DateTime? printDateStart, DateTime? printDateEnd, DateTime? shipDateStart, DateTime? shipDateEnd);

        List<ProgressEntity> GetProgress(string loginAccountCode);
    }
}
