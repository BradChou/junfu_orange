﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.Repositories.SignPaperPhotoPackage;
using LightYear.JunFuTrans.DA.JunFuDb;
using AutoMapper;
using LightYear.JunFuTrans.BL.BE.SignPaperPhoto;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRequest;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using System.Linq;
using System.Net;
using System.IO;

namespace LightYear.JunFuTrans.BL.Services.SignPaperPhotoPackage
{
    public class SignPaperPhotoPackageService : ISignPaperPhotoPackageService
    {
        public ISignPaperPhotoPackageRepository SignPaperPhotoPackageRepository;
        public IDeliveryScanLogRepository DeliveryScanLogRepository;
        public IMapper Mapper;
        public IConfiguration Configuration;
        public SignPaperPhotoPackageService(ISignPaperPhotoPackageRepository signPaperPhotoPackageRepository, IMapper mapper, IDeliveryScanLogRepository deliveryScanLogRepository, IConfiguration configuration)
        {
            SignPaperPhotoPackageRepository = signPaperPhotoPackageRepository;
            Mapper = mapper;
            DeliveryScanLogRepository = deliveryScanLogRepository;
            Configuration = configuration;
        }

        public IEnumerable<SignPaperPhotoOutputEntity> GetArrivedDeliveryRequest(string customerCode, DateTime? printDateStart, DateTime? printDateEnd, DateTime? shipDateStart, DateTime? shipDateEnd)
        {
            if (shipDateStart != null && shipDateEnd != null && printDateStart != null && printDateEnd != null)
            {
                IEnumerable<SignPaperPhotoOutputEntity> result = SignPaperPhotoPackageRepository.GetRequestsByAccountCodeAndPrintDateAndShipDate(customerCode, (DateTime)printDateStart, (DateTime)printDateEnd, (DateTime)shipDateStart, (DateTime)shipDateEnd);

                return result;
            }
            else if (shipDateStart != null && shipDateEnd != null)
            {
                IEnumerable<SignPaperPhotoOutputEntity> result = SignPaperPhotoPackageRepository.GetRequestsByCustomerCodeAndShipDate(customerCode, (DateTime)shipDateStart, (DateTime)shipDateEnd);

                return result;
            }
            else
            {
                IEnumerable<SignPaperPhotoOutputEntity> result = SignPaperPhotoPackageRepository.GetRequestsByCustomerCodeAndPrintDate(customerCode, (DateTime)printDateStart, (DateTime)printDateEnd);

                return result;
            }
        }

        public List<ProgressEntity> GetProgress(string loginAccountCode)
        {
            var originalProgressData = SignPaperPhotoPackageRepository.GetCheckNumberPackageProcesses(loginAccountCode);
            List<ProgressEntity> result = new List<ProgressEntity>();
            foreach (var o in originalProgressData)
            {
                ProgressEntity entity = new ProgressEntity
                {
                    DataName = o.DataName,
                    CompletePercent = o.IsAccessDownload == true ? 100 : (int)((o.FileCopiedRecords + o.FileNotExists + o.FileCopiedFail) * 100 / (o.FileTotalRecords + 1)),      //故意讓還沒有完成最後步驟(壓縮)的檔案無法達到100%
                    DataPathUrl = o.DataPath is null ? "" : o.DataPath,
                    DataPathString = o.IsAccessDownload == true ? "點我下載" : "",
                    PredictRemaining = o.IsAccessDownload == true ? "" : RemainingTimeCalculator((DateTime)o.StartTime, (int)(o.FileCopiedRecords + o.FileNotExists + o.FileCopiedFail), (int)o.FileTotalRecords)
                };
                if (o.EndTime != null && o.EndTime > DateTime.Now.AddMinutes(-5))   //避免尚未公開的AWS檔案被觸及下載
                {
                    WebRequest request = WebRequest.Create(o.DataPath);
                    try
                    {
                        var response = request.GetResponse();
                    }
                    catch
                    {
                        entity.DataPathString = "";
                        entity.PredictRemaining = "正在開啟下載權限，請稍候...";
                    }
                }
                result.Add(entity);
                

            }
            return result;
        }

        private string CheckNumberPath(string checkNumber)      //計算檔案位置
        {
            int folderDeepth = checkNumber.Length / 5;        //每五個字一組
            string SubPath = Configuration.GetValue<string>("S3SignPaperPhoto");
            for (var d = 0; d < folderDeepth; d++)
            {
                SubPath += checkNumber.Substring(5 * d, 5) + "/";
            }

            string result = SubPath + checkNumber + ".jpg";

            return result;
        }

        private SignPaperPhotoAndDateTimeAndType GetSignPaperPhotoPath(string checkNumber)            //步驟1:先問資料庫S3上是否有，步驟2:問資料庫80是否有，步驟3:HttpClient問S3上是否有
        {
            SignPaperPhotoAndDateTimeAndType result = new SignPaperPhotoAndDateTimeAndType() { CheckNumber = checkNumber };

            var dataCatchedFromTaiChung = SignPaperPhotoPackageRepository.GetTaiChungSchedulePhotoById(checkNumber);
            if (dataCatchedFromTaiChung != null)
            {
                result.SourceType = 1;
                result.SignPaperPhotoPath = dataCatchedFromTaiChung.S3Path == null ? null : dataCatchedFromTaiChung.S3Path.Replace(Configuration.GetValue<string>("S3SignPaperPhotoIP"), Configuration.GetValue<string>("S3Url"));
            }

            SignPaperPhotoAndDateTimeAndType dataCatchedFrom80 = DeliveryScanLogRepository.CatchLatestScanNormalDeliveryTime(checkNumber);      //為了取得ScanDate，即使已經在步驟1取得檔案，也必須走步驟2
            if (dataCatchedFrom80.SignPaperPhotoPath != null && result.SignPaperPhotoPath == null)
            {
                result.SignPaperPhotoPath = Configuration.GetValue<string>("80Url") + dataCatchedFrom80.SignPaperPhotoPath;
                result.SourceType = 2;
                result.ScanDate = dataCatchedFrom80.ScanDate;
                return result;
            }
            else
            {
                result.ScanDate = dataCatchedFrom80.ScanDate;
            }

            if (result.SignPaperPhotoPath == null)
            {
                result.SignPaperPhotoPath = "";
            }

            /*     太耗時間，先停用，待CheckPhotoToS3File建立完成後再評估是否需要這段程式碼
            if (result.SignPaperPhotoPath is null)
            {
                HttpClient hc = new HttpClient();
                try
                {
                    var h = hc.GetAsync(new Uri(Configuration.GetValue<string>("S3Url") + CheckNumberPath(checkNumber)));
                    string res = h.Result.ReasonPhrase;
                    if (res == "OK")
                    {
                        result.SignPaperPhotoPath = Configuration.GetValue<string>("S3Url") + CheckNumberPath(checkNumber);
                        result.SourceType = 1;
                    }
                    else
                    {
                        result.SignPaperPhotoPath = "";
                    }
                }
                catch
                {

                }
            }
            */
            return result;
        }

        private string RemainingTimeCalculator(DateTime startTime, int processedFileRecords, int totalFileRecords) //預估剩餘時間目前僅估計複製階段，預估剩餘時數 = (剩餘筆數 / 已處理筆數 ) * 已花時間
        {
            TimeSpan ticksStart = new TimeSpan(startTime.Ticks);
            TimeSpan ticksNow = new TimeSpan(DateTime.Now.Ticks);
            TimeSpan timePeriod = ticksStart.Subtract(ticksNow).Duration();
            float remainingRecords = (float)(totalFileRecords - processedFileRecords);
            TimeSpan resultTicks = processedFileRecords == 0 ? new TimeSpan(0) : (remainingRecords / (float)processedFileRecords) * timePeriod;
            return resultTicks.Days.ToString() + "天" + resultTicks.Hours.ToString() + "小時" + resultTicks.Minutes + "分鐘" + resultTicks.Seconds + "秒";
        }

        private IEnumerable<CheckNumberWithPhoto> GetCaterpillarPhotoFileName(string[] checkNumbers)
        {
            return SignPaperPhotoPackageRepository.GetCaterpillarPhotoFileName(checkNumbers);
        }
    }
}
