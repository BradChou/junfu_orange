﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.CbmDataLog;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.BL.Services.CbmDataLog
{
    public interface ICbmDataLogService
    {
        bool InsertIntoCbmDataLog(string date, int station);
        IEnumerable<DA.JunFuDb.CbmDataLog> GetData(CbmDataLogInputEntity input);
        void UpdateCbmDataLog(CbmUpdateDataEntity entities);
    }
}
