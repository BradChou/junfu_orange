﻿using LightYear.JunFuTrans.BL.BE.FailDelivery;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.FailDelivery
{
    public interface IFailDeliveryService
    {
        List<FailDeliveryEntity> GetFailDeliveryList(FailDeliveryInputEntity failDeliveryInputEntity);
        List<StationEntity> GetAllStations();
        List<CustomerShortNameEntity> GetAllCustomers();
        List<CustomerDownListEntity> GetCustomerByStation(string station);
    }
}
