﻿using LightYear.JunFuTrans.DA.Repositories.Station;
using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.DeliveryException;
using LightYear.JunFuTrans.DA.Repositories.DeliveryException;
using AutoMapper;
using LightYear.JunFuTrans.BL.Services.DeliveryException;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System.Data;
using System.IO.IsolatedStorage;
using System.Linq;
using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.BL.Services.Barcode;
using Microsoft.Extensions.Configuration;
using System.Web;
using Microsoft.AspNetCore.Http;

namespace LightYear.JunFuTrans.BL.Services.DeliveryException
{
    public class DeliveryExceptionService : IDeliveryExceptionService
    {
        public IDeliveryExceptionRepository DeliveryRepository { get; set; }
        public IDeliveryExceptionRepository DeliveryExceptionRepository { get; set; }
        public IScanItemService ScanItemService { get; set; }
        public IMapper Mapper { get; private set; }
        private readonly IConfiguration config;
        public IStationRepository StationRepository { get; set; }
        public IHttpContextAccessor HttpContextAccessor { get; set; }

        public DeliveryExceptionService(IDeliveryExceptionRepository deliveryRepository, IMapper mapper, IDeliveryExceptionRepository deliveryExceptionRepository, IScanItemService scanItemService, IConfiguration config, IStationRepository stationRepository, IHttpContextAccessor httpContextAccessor)
        {
            this.DeliveryRepository = deliveryRepository;
            this.Mapper = mapper;
            this.DeliveryExceptionRepository = deliveryExceptionRepository;
            ScanItemService = scanItemService;
            this.config = config;
            StationRepository = stationRepository;
            HttpContextAccessor = httpContextAccessor;
        }
        public void SaveException(DeliveryExceptionEntity entity)               //請呼叫此方法Update時，entity務必填入Id以及CDate欄位
        {
            entity.UDate = DateTime.Now;

            if (entity.HandleStation == null)                                   //若處理站所未填入，則回報站所即是處理站所，否則查詢異常件會石沉大海
            {
                entity.HandleStation = entity.ReportStation;
            }

            var exception = this.Mapper.Map<ExceptionReport>(entity);

            var log = this.Mapper.Map<ExceptionReportLog>(entity);

            if (entity.Id == 0)
            {
                exception.Cdate = exception.Udate;

                log.Cdate = exception.Udate;

                this.DeliveryRepository.InsertDeliveryExceptionLog(log);

                this.DeliveryRepository.InsertDeliveryException(exception);
            }
            else
            {
                log.Id = 0;

                this.DeliveryRepository.InsertDeliveryExceptionLog(log);

                this.DeliveryRepository.UpdateDeliveryException(exception);
            }
        }

        public void DeleteException(string checkNumber, string handleStation, string handler_code, DateTime cdate)
        {
            ExceptionReportLog log = new ExceptionReportLog()
            {
                CheckNumber = checkNumber,
                HandleStation = handleStation,
                HandlerCode = handler_code,
                Cdate = cdate,
                Udate = DateTime.Now
            };

            this.DeliveryRepository.InsertDeliveryExceptionLog(log);

            this.DeliveryRepository.DeleteDeliveryException(checkNumber);
        }

        public List<DeliveryExceptionEntity> GetExceptionsProcessing(string station)
        {
            var gs = GetStation();

            foreach (var i in gs)                          //避免前端顯示代碼，這段將前端的中文站名轉為站所代碼
            {
                if (station == i.Name) 
                {
                    station = i.Code;
                    break;
                }
            }

            var data = this.DeliveryRepository.GetDeliveryExceptionsProcessing(station);

            var exceptionResult = this.Mapper.Map<List<DeliveryExceptionEntity>>(data);

            foreach (var i in exceptionResult)
            {
                foreach (var j in gs)
                {
                    if (i.ReportStation == j.Code)
                    {
                        i.ReportStation = j.Name;
                        break;
                    }
                }
            }

            foreach (var i in exceptionResult)
            {
                i.CreateDate = i.CDate.ToString().Split(' ')[0];
            }

            foreach (var i in exceptionResult)
            {
                if (i.IsClosed == true)
                    i.IsClosedVX = "V";
                else
                    i.IsClosedVX = "";
            }

            return exceptionResult;
        }

        public List<DeliveryExceptionEntity> GetExceptionsPending(string station)
        {
            var gs = GetStation();

            foreach (var i in gs)                          //避免前端顯示代碼，這段將前端的中文站名轉為站所代碼
            {
                if (station == i.Name)
                {
                    station = i.Code;
                    break;
                }
            }

            var data = this.DeliveryRepository.GetDeliveryExceptionsPending(station);

            var exceptionResult = this.Mapper.Map<List<DeliveryExceptionEntity>>(data);

            foreach (var i in exceptionResult)
            {
                foreach (var j in gs)
                {
                    if (i.ReportStation == j.Code)
                    {
                        i.ReportStation = j.Name;
                        break;
                    }
                }
            }

            foreach (var i in exceptionResult)
            {
                i.CreateDate = i.CDate.ToString().Split(' ')[0];
            }

            foreach (var i in exceptionResult)
            {
                if (i.IsClosed == true)
                    i.IsClosedVX = "V";
                else
                    i.IsClosedVX = "";
            }

            return exceptionResult;
        }

        public List<StationEntity> GetStation()
        {
            var data = this.DeliveryRepository.GetTbStations();

            var station = this.Mapper.Map<List<StationEntity>>(data);

            return station;
        }

        public List<CreateDeliveryExceptionEntity> GetExceptionsInfoByCN(string checkNumber)
        {
                var data = DeliveryRepository.GetDeliveryExceptionsInfoByCN(checkNumber);

                var exceptionResult = this.Mapper.Map<List<CreateDeliveryExceptionEntity>>(data);

                if (exceptionResult.FirstOrDefault().AreaArriveCode.IndexOf("F") == -1)                         //如果AreaArriveCode為Scode的形式，則改為Code的形式
                {
                    exceptionResult.FirstOrDefault().AreaArriveCode = "F" + exceptionResult.FirstOrDefault().AreaArriveCode.ToString();
                }

                if (exceptionResult.FirstOrDefault().SupplierCode.IndexOf("F") == -1)                         //如果SupplierCode為Scode的形式，則改為Code的形式
                {
                    exceptionResult.FirstOrDefault().SupplierCode = "F" + exceptionResult.FirstOrDefault().SupplierCode.ToString();
                }

                exceptionResult.FirstOrDefault().AreaArriveCode = DeliveryRepository.GetStationNameByCode(exceptionResult.FirstOrDefault().AreaArriveCode);

                exceptionResult.FirstOrDefault().SupplierCode = DeliveryRepository.GetStationNameByCode(exceptionResult.FirstOrDefault().SupplierCode);

                return exceptionResult;
        }
        public List<ExceptionReportAddScanLogEntity> GetExceptionFromScanLogs(DateTime start, DateTime end, string station)
        {
            end = end.AddDays(1);

            List<ExceptionReportAddScanLogEntity> exceptionFromScanLogEntities = this.DeliveryRepository.GetExceptions(start, end, station);

            var photoUrl = this.config.GetValue<string>("AddExceptionsPhotosUrl");

            foreach (var entity in exceptionFromScanLogEntities)
            {
                ExceptionOption exceptionOption = (ExceptionOption)Convert.ToInt32(entity.ExceptionStatus);
                SketchMatter sketchMatter = (SketchMatter)Convert.ToInt32(entity.SketchMatter);
                entity.SketchMatter = ScanItemService.GetEnumDescription(sketchMatter);
                entity.ExceptionStatus = ScanItemService.GetEnumDescription(exceptionOption);
                entity.FilePath = entity.FilePath.Length > 0 ? string.Concat(photoUrl, entity.FilePath) : "";
                entity.FilePathText = entity.FilePath.Length > 0 ? "查看圖片" : "無上傳檔案";
                //entity.FilePathText = entity.FilePath.Length == 0 ? "無上傳檔案" : "";
                //entity.ReportStation = StationRepository.GetStationNameByScode(entity.ReportStation);
            }
            return exceptionFromScanLogEntities;
        }
        public List<ExceptionReportAddScanLogEntity> GetExceptionFromScanLogsByAllStaionScode(DateTime start, DateTime end)
        {
            end = end.AddDays(1);

            List<ExceptionReportAddScanLogEntity> exceptionFromScanLogEntities = this.DeliveryRepository.GetExceptionsByAllStationScode(start, end);

            var photoUrl = this.config.GetValue<string>("AddExceptionsPhotosUrl");

            foreach (var entity in exceptionFromScanLogEntities)
            {
                ExceptionOption exceptionOption = (ExceptionOption)Convert.ToInt32(entity.ExceptionStatus);
                SketchMatter sketchMatter = (SketchMatter)Convert.ToInt32(entity.SketchMatter);
                entity.SketchMatter = ScanItemService.GetEnumDescription(sketchMatter);
                entity.ExceptionStatus = ScanItemService.GetEnumDescription(exceptionOption);
                entity.FilePath = entity.FilePath.Length > 0 ? string.Concat(photoUrl, entity.FilePath) : "";
                entity.FilePathText = entity.FilePath.Length > 0 ? "查看圖片" : "無上傳檔案";
                //entity.FilePathText = entity.FilePath.Length == 0 ? "無上傳檔案" : "";
                //entity.ReportStation = StationRepository.GetStationNameByScode(entity.ReportStation);
            }
            return exceptionFromScanLogEntities;
        }

        public List<ExceptionReportAddScanLogEntity> GetExceptionFromScanLogsByArea(DateTime start, DateTime end, string area)
        {
            end = end.AddDays(1);

            List<ExceptionReportAddScanLogEntity> exceptionFromScanLogEntities = this.DeliveryRepository.GetExceptionsByArea(start, end, area);

            var photoUrl = this.config.GetValue<string>("AddExceptionsPhotosUrl");

            foreach (var entity in exceptionFromScanLogEntities)
            {
                ExceptionOption exceptionOption = (ExceptionOption)Convert.ToInt32(entity.ExceptionStatus);
                SketchMatter sketchMatter = (SketchMatter)Convert.ToInt32(entity.SketchMatter);
                entity.SketchMatter = ScanItemService.GetEnumDescription(sketchMatter);
                entity.ExceptionStatus = ScanItemService.GetEnumDescription(exceptionOption);
                entity.FilePath = entity.FilePath.Length > 0 ? string.Concat(photoUrl, entity.FilePath) : "";
                entity.FilePathText = entity.FilePath.Length > 0 ? "查看圖片" : "無上傳檔案";
                //entity.FilePathText = entity.FilePath is null ? "" : "無上傳";            
                //entity.FilePathText = entity.FilePath.Length == 0 ? "無上傳檔案" : "";
                //entity.ReportStation = StationRepository.GetStationNameByScode(entity.ReportStation);
            }
            return exceptionFromScanLogEntities;
        }

        public List<ExceptionReportAddScanLogEntity> GetExceptionFromScanLogsByCk(string checknumber)
        {
            List<ExceptionReportAddScanLogEntity> exceptionFromScanLogEntities = this.DeliveryRepository.GetExceptionFromScanLogsByCk(checknumber);

            var photoUrl = this.config.GetValue<string>("AddExceptionsPhotosUrl");

            foreach (var entity in exceptionFromScanLogEntities) 
            {
                ExceptionOption exceptionOption = (ExceptionOption)Convert.ToInt32(entity.ExceptionStatus);
                SketchMatter sketchMatter = (SketchMatter)Convert.ToInt32(entity.SketchMatter);
                entity.SketchMatter = ScanItemService.GetEnumDescription(sketchMatter);
                entity.ExceptionStatus = ScanItemService.GetEnumDescription(exceptionOption);
                entity.FilePath = entity.FilePath.Length > 0 ? string.Concat(photoUrl,entity.FilePath) : "";
                entity.FilePathText = entity.FilePath.Length > 0 ? "查看圖片" : "無上傳檔案";               
                //entity.FilePathText = entity.FilePath.Length == 0 ? "無上傳檔案" : "";
                //entity.ReportStation = StationRepository.GetStationNameByScode(entity.ReportStation);
            }
            return exceptionFromScanLogEntities;
        }
    }  
}
