﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.DeliveryException;

namespace LightYear.JunFuTrans.BL.Services.DeliveryException
{
    public interface IDeliveryExceptionService
    {
        void DeleteException(string checkNumber, string handleStation, string handler_code, DateTime cdate);

        List<DeliveryExceptionEntity> GetExceptionsProcessing(string station);

        List<DeliveryExceptionEntity> GetExceptionsPending(string station);

        List<StationEntity> GetStation();

        void SaveException(DeliveryExceptionEntity entity);

        List<CreateDeliveryExceptionEntity> GetExceptionsInfoByCN(string checkNumber);

        List<ExceptionReportAddScanLogEntity> GetExceptionFromScanLogs(DateTime start, DateTime end, string station);

        List<ExceptionReportAddScanLogEntity> GetExceptionFromScanLogsByAllStaionScode(DateTime start, DateTime end);

        List<ExceptionReportAddScanLogEntity> GetExceptionFromScanLogsByArea(DateTime start, DateTime end, string area);

        List<ExceptionReportAddScanLogEntity> GetExceptionFromScanLogsByCk(string checknumber);
    }
}
