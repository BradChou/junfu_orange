﻿using LightYear.JunFuTrans.BL.BE.DeliveryRate;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.CustomerStationBinding
{
    public interface ICustomerStationBindingService
    {
        List<CustomerEntity> GetCustomersByStationCode(string stationCode);

        List<StationEntity> GetStationByCustomer(string customerCode);

        StationEntity GetStationBySCode(string stationSCode);
    }
}
