﻿using AutoMapper;
using LightYear.JunFuTrans.BL.BE.DeliveryRate;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.Repositories.Customer;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRate;
using LightYear.JunFuTrans.DA.Repositories.Station;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.CustomerStationBinding
{
    public class CustomerStationBindingService : ICustomerStationBindingService
    {
        public IStationRepository StationRepository { get; set; }

        public ICustomerRepository CustomerRepository { get; set; }

        public IMapper Mapper { get; set; }

        public CustomerStationBindingService(IStationRepository stationRepository,
            ICustomerRepository customerRepository, IMapper mapper)
        {
            StationRepository = stationRepository;
            CustomerRepository = customerRepository;
            Mapper = mapper;
        }

        public List<CustomerEntity> GetCustomersByStationCode(string stationCode)
        {
            List<TbCustomer> customers = CustomerRepository.GetCustomersByStation(stationCode);
            var customerEntities = Mapper.Map<List<TbCustomer>, List<CustomerEntity>>(customers);
            return customerEntities;
        }

        public List<StationEntity> GetStationByCustomer(string customerCode)
        {
            TbStation station = StationRepository.GetByStationCode(customerCode.Substring(0, 3));    // 客代的前三碼是 station code
            StationEntity stationEntity = Mapper.Map<TbStation, StationEntity>(station);
            List<StationEntity> result = new List<StationEntity>();
            result.Add(new StationEntity { StationName = "全公司", StationCode = "All", StationScode = "All" });
            result.Add(stationEntity);
            return result;
        }

        public StationEntity GetStationBySCode(string stationSCode)
        {
            TbStation station = StationRepository.GetByStationCode(stationSCode);
            StationEntity stationEntity = Mapper.Map<TbStation, StationEntity>(station);

            return stationEntity;

        }
    }
}
