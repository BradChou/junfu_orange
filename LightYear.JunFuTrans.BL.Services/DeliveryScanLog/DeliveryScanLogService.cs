﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.Repositories.DeliveryScanLog;

namespace LightYear.JunFuTrans.BL.Services.DeliveryScanLog
{
    public class DeliveryScanLogService : IDeliveryScanLogService
    {
        public IDeliveryScanLogRepository DeliveryScanLogRepository { get; set; }
        public DeliveryScanLogService(IDeliveryScanLogRepository deliveryScanLogRepository)
        {
            DeliveryScanLogRepository = deliveryScanLogRepository;
        }
        public bool InsertIntoScanLogSend(string driverCode, string checkNumber, string scanItem, string scanDate)
        {
            return DeliveryScanLogRepository.InsertIntoScanLogSend(driverCode, checkNumber, scanItem, scanDate);
        }
    }
}
