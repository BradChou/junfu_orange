﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.DeliveryScanLog
{
    public interface IDeliveryScanLogService
    {
        bool InsertIntoScanLogSend(string driverCode, string checkNumber, string scanItem, string scanDate);
    }
}
