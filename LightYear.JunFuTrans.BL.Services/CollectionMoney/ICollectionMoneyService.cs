﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.CollectionMoney;

namespace LightYear.JunFuTrans.BL.Services.CollectionMoney
{
    public interface ICollectionMoneyService
    {
        double GetCollectionMoney(string checkNumber);

        int SaveCollectionMoney(CollectionMoneyEntity collectionMoneyEntity);

    }
}
