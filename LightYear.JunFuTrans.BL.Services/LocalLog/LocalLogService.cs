﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Newtonsoft.Json;

namespace LightYear.JunFuTrans.BL.Services.LocalLog
{
    public class LocalLogService : ILocalLogService
    {
        public void Log<T>(string logPath, string logFileName, string result, T logContent)
        {
            DateTime now = DateTime.Now;
            logPath = logPath.EndsWith(@"\") ? logPath : logPath + @"\";
            logFileName = logFileName + string.Format("_{0}.txt", now.ToString("yyyyMMdd"));
            string logFileFullName = logPath + logFileName;

            if (!Directory.Exists(logPath))
            {
                Directory.CreateDirectory(logPath);
            }

            if (!File.Exists(logFileFullName))
            {
                var file = File.Create(logFileFullName);
                file.Close();
            }


            List<string> logContentInJson = new List<string>(){
                now.ToString("yyyy-MM-dd HH:mm:ss") + "\t" + result,
                "\t" + JsonConvert.SerializeObject(logContent),
                "\r\n"
            };

            try
            {
                File.AppendAllLines(logFileFullName, logContentInJson);
            }
            catch (Exception ex)
            {

            }
        }
    }
}
