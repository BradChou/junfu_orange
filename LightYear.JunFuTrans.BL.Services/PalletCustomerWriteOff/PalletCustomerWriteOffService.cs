﻿using AutoMapper;
using LightYear.JunFuTrans.BL.BE.PalletCustomerWriteOff;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.Repositories.Customer;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRequest;
using LightYear.JunFuTrans.DA.Repositories.PalletCustomerWriteOff;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using OfficeOpenXml.Style;
using System.Drawing;

namespace LightYear.JunFuTrans.BL.Services.PalletCustomerWriteOff
{
    public class PalletCustomerWriteOffService : IPalletCustomerWriteOffService
    {
        IPalletCustomerWriteOffRepository PalletCustomerWriteOffRepository;
        ICustomerRepository CustomerReposiroty;
        IMapper Mapper;
        IDeliveryRequestRepository DeliveryRequestRepository;
        IDeliveryRequestModifyRepository DeliveryRequestModifyRepository;

        public PalletCustomerWriteOffService(IPalletCustomerWriteOffRepository palletCustomerWriteOffRepository
                                             , IMapper mapper
                                             , ICustomerRepository customerReposiroty
                                             , IDeliveryRequestRepository deliveryRequestRepository
                                             , IDeliveryRequestModifyRepository deliveryRequestModifyRepository)
        {
            PalletCustomerWriteOffRepository = palletCustomerWriteOffRepository;
            Mapper = mapper;
            CustomerReposiroty = customerReposiroty;
            DeliveryRequestRepository = deliveryRequestRepository;
            DeliveryRequestModifyRepository = deliveryRequestModifyRepository;
        }

        /// <summary>
        /// 尚未結帳的帳單，加入應收金額，手動結帳使用
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="date"></param>
        /// <param name="shortCustomerCode"></param>
        /// <returns></returns>
        public List<PalletCustomerWriteOffEntity> GetNotWriteOffCustomerMonthDetail(DateTime startDate, DateTime date, string shortCustomerCode = null)
        {
            var publicVersionFee = PalletCustomerWriteOffRepository.GetPriceSuppliers();
            var latestDefineFee = PalletCustomerWriteOffRepository.GetLatestDefineFee();
            var defineCustomers = latestDefineFee.Select(a => a.CustomerCode).Distinct().ToList();
            var pricingCodeChinese = PalletCustomerWriteOffRepository.GetPricingCode();

            //論件自訂運價
            var piecesShippingFee = PalletCustomerWriteOffRepository.GetPalletPiecesShippingFeeDefine();
            var piecesCustomers = piecesShippingFee.Select(a => a.CustomerCode).Distinct().ToList();

            List<PalletCustomerWriteOffEntity> data = PalletCustomerWriteOffRepository.GetNotWriteOffCustomerMonthDetail(startDate, date, shortCustomerCode);

            List<PalletCustomerWriteOffEntity> checkNumberWithFee = new List<PalletCustomerWriteOffEntity>();

            int count = 1;

            #region 計價模式為專車 
            var truck = (from d in data
                         where d.DeliveryType == "05"
                         select new
                         {
                             CustomerCode = d.CustomerCode,
                             PrintDate = d.PrintDate,
                             SupplierDate = d.SupplierDate,
                             CustomerName = d.CustomerName,
                             CustomerShortName = d.CustomerShortName,
                             CheckNumber = d.CheckNumber,
                             DeliveryType = d.DeliveryType,
                             SupplierArea = d.SupplierArea,
                             SendCity = d.SendCity,
                             ArriveArea = d.ArriveArea,
                             ReceiveCity = d.ReceiveCity,
                             Pieces = d.Pieces,
                             Plates = d.Plates,
                             Payment = d.SupplierFee + d.AddFee,
                             AddFee = d.AddFee,
                         }).Distinct().ToList();

            var truckEntity = truck.Select(d => new PalletCustomerWriteOffEntity
            {
                CustomerCode = d.CustomerCode,
                PrintDate = d.PrintDate,
                SupplierDate = d.SupplierDate,
                CustomerName = d.CustomerName,
                CustomerShortName = d.CustomerShortName,
                CheckNumber = d.CheckNumber,
                DeliveryType = d.DeliveryType,
                SupplierArea = d.SupplierArea,
                SendCity = d.SendCity,
                ArriveArea = d.ArriveArea,
                ReceiveCity = d.ReceiveCity,
                Pieces = d.Pieces,
                Plates = d.Plates,
                AddFee = d.AddFee,
                Payment = d.Payment
            }).ToList();

            checkNumberWithFee.AddRange(truckEntity);
            #endregion 

            #region 論板自訂運價
            var defineCustomerFee = (from d in data
                                     join l in latestDefineFee on new { CustomerCode = d.CustomerCode, StartCity = d.SendCity, EndCity = d.ReceiveCity, Pricingcode = d.DeliveryType } equals
                                                                  new { CustomerCode = l.CustomerCode, StartCity = l.StartCity, EndCity = l.EndCity, Pricingcode = l.PricingCode } into temp
                                     from l in temp.DefaultIfEmpty(new TbPriceBusinessDefine { StartCity = default(string), EndCity = default(string) })
                                     where defineCustomers.Contains(d.CustomerCode)
                                     select new
                                     {
                                         CustomerCode = d.CustomerCode,
                                         PrintDate = d.PrintDate,
                                         SupplierDate = d.SupplierDate,
                                         CustomerName = d.CustomerName,
                                         CustomerShortName = d.CustomerShortName,
                                         CheckNumber = d.CheckNumber,
                                         DeliveryType = d.DeliveryType,
                                         SupplierArea = d.SupplierArea,
                                         SendCity = d.SendCity,
                                         ArriveArea = d.ArriveArea,
                                         ReceiveCity = d.ReceiveCity,
                                         Pieces = d.Pieces,
                                         Plates = d.Plates,
                                         AddFee = d.AddFee,
                                         Payment = d.DeliveryType == "02" ? //是否為論件
                           (d.Pieces == 1 ? d.Pieces * l.Plate1Price ?? 0 :
                           d.Pieces == 2 ? d.Pieces * l.Plate2Price ?? 0 :
                           d.Pieces == 3 ? d.Pieces * l.Plate3Price ?? 0 :
                           d.Pieces == 4 ? d.Pieces * l.Plate4Price ?? 0 :
                           d.Pieces == 5 ? d.Pieces * l.Plate5Price ?? 0 :
                           d.Pieces >= 6 ? d.Pieces * l.Plate6Price ?? 0 : 0) :
                           (d.Plates == 1 ? d.Plates * l.Plate1Price ?? 0 :
                           d.Plates == 2 ? d.Plates * l.Plate2Price ?? 0 :
                           d.Plates == 3 ? d.Plates * l.Plate3Price ?? 0 :
                           d.Plates == 4 ? d.Plates * l.Plate4Price ?? 0 :
                           d.Plates == 5 ? d.Plates * l.Plate5Price ?? 0 :
                           d.Plates >= 6 ? d.Plates * l.Plate6Price ?? 0 : 0)
                                     }).Distinct().ToList();

            var defineCustomerFeeEntity = defineCustomerFee.Where(a => a.Payment > 0).Select(d => new PalletCustomerWriteOffEntity
            //var defineCustomerFeeEntity = defineCustomerFee.Select(d => new PalletCustomerWriteOffEntity
            {
                CustomerCode = d.CustomerCode,
                PrintDate = d.PrintDate,
                SupplierDate = d.SupplierDate,
                CustomerName = d.CustomerName,
                CustomerShortName = d.CustomerShortName,
                CheckNumber = d.CheckNumber,
                DeliveryType = d.DeliveryType,
                SupplierArea = d.SupplierArea,
                SendCity = d.SendCity,
                ArriveArea = d.ArriveArea,
                ReceiveCity = d.ReceiveCity,
                Pieces = d.Pieces,
                Plates = d.Plates,
                AddFee = d.AddFee,
                Payment = d.CustomerShortName.Contains("退貨回板") ? 0 : d.Payment + d.AddFee
            }).ToList();

            var defineCustomerFeeEntity1 = defineCustomerFee.Where(a => a.Payment == 0).Select(d => new PalletCustomerWriteOffEntity
            {
                CustomerCode = d.CustomerCode,
                PrintDate = d.PrintDate,
                SupplierDate = d.SupplierDate,
                CustomerName = d.CustomerName,
                CustomerShortName = d.CustomerShortName,
                CheckNumber = d.CheckNumber,
                DeliveryType = d.DeliveryType,
                SupplierArea = d.SupplierArea,
                SendCity = d.SendCity,
                ArriveArea = d.ArriveArea,
                ReceiveCity = d.ReceiveCity,
                Pieces = d.Pieces,
                Plates = d.Plates,
                AddFee = d.AddFee,
                Payment = d.CustomerShortName.Contains("退貨回板") ? 0 : d.Payment + d.AddFee
            }).ToList();

            //#region 論板自訂運價找不到找論板公版運價
            var publicCustomerFeeNOtbPriceBusinessDefine = (from d in defineCustomerFeeEntity1
                                                            join l in publicVersionFee on new { StartCity = d.SendCity, EndCity = d.ReceiveCity, Pricingcode = d.DeliveryType } equals
                                                                                         new { StartCity = l.StartCity, EndCity = l.EndCity, Pricingcode = l.PricingCode } into temp
                                                            from l in temp.DefaultIfEmpty(new TbPriceSupplier { StartCity = default(string), EndCity = default(string) })
                                                            where defineCustomers.Contains(d.CustomerCode) && d.DeliveryType == "01" //&& !piecesCustomers.Contains(d.CustomerCode)
                                                            select new
                                                            {
                                                                CustomerCode = d.CustomerCode,
                                                                PrintDate = d.PrintDate,
                                                                SupplierDate = d.SupplierDate,
                                                                CustomerName = d.CustomerName,
                                                                CustomerShortName = d.CustomerShortName,
                                                                CheckNumber = d.CheckNumber,
                                                                DeliveryType = d.DeliveryType,
                                                                SupplierArea = d.SupplierArea,
                                                                SendCity = d.SendCity,
                                                                ArriveArea = d.ArriveArea,
                                                                ReceiveCity = d.ReceiveCity,
                                                                Pieces = d.Pieces,
                                                                Plates = d.Plates,
                                                                AddFee = d.AddFee,
                                                                Payment = d.CustomerShortName.Contains("退貨回板") ? (decimal)d.AddFee : (d.Plates == 1 ? d.Plates * l.Plate1Price ?? 0 :
                                                    d.Plates == 2 ? d.Plates * l.Plate2Price ?? 0 :
                                                    d.Plates == 3 ? d.Plates * l.Plate3Price ?? 0 :
                                                    d.Plates == 4 ? d.Plates * l.Plate4Price ?? 0 :
                                                    d.Plates == 5 ? d.Plates * l.Plate5Price ?? 0 :
                                                    d.Plates >= 6 ? d.Plates * l.Plate6Price ?? 0 : 0)
                                                            }).Distinct().ToList();

            defineCustomerFeeEntity1 = publicCustomerFeeNOtbPriceBusinessDefine.Select(d => new PalletCustomerWriteOffEntity
            {
                CustomerCode = d.CustomerCode,
                PrintDate = d.PrintDate,
                SupplierDate = d.SupplierDate,
                CustomerName = d.CustomerName,
                CustomerShortName = d.CustomerShortName,
                CheckNumber = d.CheckNumber,
                DeliveryType = d.DeliveryType,
                SupplierArea = d.SupplierArea,
                SendCity = d.SendCity,
                ArriveArea = d.ArriveArea,
                ReceiveCity = d.ReceiveCity,
                Pieces = d.Pieces,
                Plates = d.Plates,
                AddFee = d.AddFee,
                Payment = d.CustomerShortName.Contains("退貨回板") ? 0 : d.Payment + d.AddFee
            }).ToList();


            checkNumberWithFee.AddRange(defineCustomerFeeEntity);

            checkNumberWithFee.AddRange(defineCustomerFeeEntity1);
            #endregion

            #region 論件自訂運價
            var piecesFee = (from d in data
                             join l in piecesShippingFee on new { CustomerCode = d.CustomerCode, StartCity = d.SendCity, EndCity = d.ReceiveCity } equals
                                                          new { CustomerCode = l.CustomerCode, StartCity = l.StartCity, EndCity = l.EndCity } into temp
                             from l in temp.DefaultIfEmpty(new PalletPiecesShippingFeeDefine { StartCity = default(string), EndCity = default(string) })
                             where piecesCustomers.Contains(d.CustomerCode)
                             select new
                             {
                                 CustomerCode = d.CustomerCode,
                                 PrintDate = d.PrintDate,
                                 SupplierDate = d.SupplierDate,
                                 CustomerName = d.CustomerName,
                                 CustomerShortName = d.CustomerShortName,
                                 CheckNumber = d.CheckNumber,
                                 DeliveryType = d.DeliveryType,
                                 SupplierArea = d.SupplierArea,
                                 SendCity = d.SendCity,
                                 ArriveArea = d.ArriveArea,
                                 ReceiveCity = d.ReceiveCity,
                                 Pieces = d.Pieces,
                                 Plates = d.Plates,
                                 AddFee = d.AddFee,
                                 Payment = (d.Pieces > 0 && d.Pieces < 21 ? d.Pieces * l.Twenty ?? 0 :
                                            d.Pieces >= 21 && d.Pieces < 41 ? d.Pieces * l.Forty ?? 0 :
                                            d.Pieces >= 41 && d.Pieces < 61 ? d.Pieces * l.Sixty ?? 0 :
                                            d.Pieces >= 61 && d.Pieces < 81 ? d.Pieces * l.Eighty ?? 0 :
                                            d.Pieces >= 81 && d.Pieces < 101 ? d.Pieces * l.OneHundred ?? 0 :
                                            d.Pieces > 100 ? d.Pieces * l.AboveHundred ?? 0 : 0)
                             }).Distinct().ToList();

            var piecesFeeEntity = piecesFee.Select(d => new PalletCustomerWriteOffEntity
            {
                CustomerCode = d.CustomerCode,
                PrintDate = d.PrintDate,
                SupplierDate = d.SupplierDate,
                CustomerName = d.CustomerName,
                CustomerShortName = d.CustomerShortName,
                CheckNumber = d.CheckNumber,
                DeliveryType = d.DeliveryType,
                SupplierArea = d.SupplierArea,
                SendCity = d.SendCity,
                ArriveArea = d.ArriveArea,
                ReceiveCity = d.ReceiveCity,
                Pieces = d.Pieces,
                Plates = d.Plates,
                AddFee = d.AddFee,
                Payment = d.Payment + d.AddFee
            }).ToList();

            checkNumberWithFee.AddRange(piecesFeeEntity);
            #endregion

            #region 論板公版運價
            var publicCustomerFeeAAAAA = (from d in data
                                          join l in publicVersionFee on new { StartCity = d.SendCity, EndCity = d.ReceiveCity, Pricingcode = d.DeliveryType } equals
                                                                       new { StartCity = l.StartCity, EndCity = l.EndCity, Pricingcode = l.PricingCode } into temp
                                          from l in temp.DefaultIfEmpty(new TbPriceSupplier { StartCity = default(string), EndCity = default(string) })
                                          where !defineCustomers.Contains(d.CustomerCode) && d.DeliveryType == "01" && !piecesCustomers.Contains(d.CustomerCode)
                                          select new
                                          {
                                              CustomerCode = d.CustomerCode,
                                              PrintDate = d.PrintDate,
                                              SupplierDate = d.SupplierDate,
                                              CustomerName = d.CustomerName,
                                              CustomerShortName = d.CustomerShortName,
                                              CheckNumber = d.CheckNumber,
                                              DeliveryType = d.DeliveryType,
                                              SupplierArea = d.SupplierArea,
                                              SendCity = d.SendCity,
                                              ArriveArea = d.ArriveArea,
                                              ReceiveCity = d.ReceiveCity,
                                              Pieces = d.Pieces,
                                              Plates = d.Plates,
                                              AddFee = d.AddFee,
                                              Payment = d.CustomerShortName.Contains("退貨回板") ? (decimal)d.AddFee : (d.Plates == 1 ? d.Plates * l.Plate1Price ?? 0 :
                                                         d.Plates == 2 ? d.Plates * l.Plate2Price ?? 0 :
                                                         d.Plates == 3 ? d.Plates * l.Plate3Price ?? 0 :
                                                         d.Plates == 4 ? d.Plates * l.Plate4Price ?? 0 :
                                                         d.Plates == 5 ? d.Plates * l.Plate5Price ?? 0 :
                                                         d.Plates >= 6 ? d.Plates * l.Plate6Price ?? 0 : 0)
                                          }).Distinct().ToList();

            var publicCustomerFeeEntity10 = publicCustomerFeeAAAAA.Select(d => new PalletCustomerWriteOffEntity
            {
                CustomerCode = d.CustomerCode,
                PrintDate = d.PrintDate,
                SupplierDate = d.SupplierDate,
                CustomerName = d.CustomerName,
                CustomerShortName = d.CustomerShortName,
                CheckNumber = d.CheckNumber,
                DeliveryType = d.DeliveryType,
                SupplierArea = d.SupplierArea,
                SendCity = d.SendCity,
                ArriveArea = d.ArriveArea,
                ReceiveCity = d.ReceiveCity,
                Pieces = d.Pieces,
                Plates = d.Plates,
                AddFee = d.AddFee,
                Payment = d.CustomerShortName.Contains("退貨回板") ? 0 : d.Payment + d.AddFee
            }).ToList();

            checkNumberWithFee.AddRange(publicCustomerFeeEntity10);
            #endregion

            #region 論件公版運價
            var piecesPublicVersionEntity = GetPalletPiecesPublicVersionFee(data);
            checkNumberWithFee.AddRange(piecesPublicVersionEntity);
            #endregion

            decimal? truckIncome = 0;
            decimal businessTax = 0.05M;

            truckIncome = truck.Sum(a => a.Payment);

            var total = checkNumberWithFee.Sum(a => a.Payment);

            var totalPalletCount = checkNumberWithFee.Sum(a => a.Plates);

            var truckPalletCount = truck.Sum(a => a.Plates);

            var truckPiecesCount = truck.Sum(a => a.Pieces);

            var totalPiecesCount = checkNumberWithFee.Sum(a => a.Pieces);

            var palletIncome = total - truckIncome;

            var totalWithTax = Math.Round((total * (1 + businessTax)) ?? 0, 0, MidpointRounding.AwayFromZero);

            var tax = Math.Round((total * businessTax) ?? 0, 0, MidpointRounding.AwayFromZero);

            Payment payment = new Payment()
            {
                PalletIncome = palletIncome,
                TruckIncome = truckIncome,
                Tax = tax,
                Total = totalWithTax,
                TotalPalletCount = totalPalletCount,
                TotalPiecesCount = totalPiecesCount,
                TruckPalletCount = truckPalletCount,
                TruckPiecesCount = truckPiecesCount
            };

            var customers = PalletCustomerWriteOffRepository.FindCustomerEntities(shortCustomerCode);

            FindCustomerEntity findCustomerEntity = new FindCustomerEntity()
            {
                CustomerShortname = customers[0].CustomerShortname,
                CustomerName = customers[0].CustomerName,
                UniformNumber = customers[0].UniformNumber,
                Telephone = customers[0].Telephone,
                ShipmentsAddress = customers[0].ShipmentsAddress,
                ShipmentsEmail = customers[0].ShipmentsEmail,
                BillingSpecialNeeds = customers[0].BillingSpecialNeeds,
                ShipmentsPrincipal = customers[0].ShipmentsPrincipal,
            };

            checkNumberWithFee = (from c in checkNumberWithFee orderby c.PrintDate ascending select c).Distinct().ToList();

            foreach (var entity in checkNumberWithFee)
            {
                entity.Id = count++;
                entity.PaymentEntity = payment;
                entity.DeliveryType = pricingCodeChinese.Where(a => a.CodeId == entity.DeliveryType).FirstOrDefault()?.CodeName;
                entity.CustomersData = findCustomerEntity;
            }
            return checkNumberWithFee;
        }

        /// <summary>
        /// 客戶已結帳的帳單，加入應收金額
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="shortCustomerCode"></param>
        /// <returns></returns>
        public List<PalletCustomerWriteOffEntity> GetAlreadyWriteOffMonthCustomer(DateTime startDate, DateTime endDate, string shortCustomerCode = null)
        {
            var publicVersionFee = PalletCustomerWriteOffRepository.GetPriceSuppliers();
            var latestDefineFee = PalletCustomerWriteOffRepository.GetLatestDefineFee();
            var defineCustomers = latestDefineFee.Select(a => a.CustomerCode).Distinct().ToList();
            var pricingCodeChinese = PalletCustomerWriteOffRepository.GetPricingCode();

            //論件自訂運價
            var piecesShippingFee = PalletCustomerWriteOffRepository.GetPalletPiecesShippingFeeDefine();
            var piecesCustomers = piecesShippingFee.Select(a => a.CustomerCode).Distinct().ToList();

            //List<PalletCustomerWriteOffEntity> data = PalletCustomerWriteOffRepository.GetAlreadyWriteOffMonthCustomer(startDate, endDate.AddDays(1), shortCustomerCode);
            List<PalletCustomerWriteOffEntity> data = PalletCustomerWriteOffRepository.GetAlreadyWriteOffMonthCustomer(startDate, endDate, shortCustomerCode);


            List<PalletCustomerWriteOffEntity> checkNumberWithFee = new List<PalletCustomerWriteOffEntity>();

            int count = 1;

            #region 計價模式為專車
            var truck = (from d in data
                         where d.DeliveryType == "05"
                         select new
                         {
                             CustomerCode = d.CustomerCode,
                             PrintDate = d.PrintDate,
                             SupplierDate = d.SupplierDate,
                             CustomerName = d.CustomerName,
                             CustomerShortName = d.CustomerShortName,
                             CheckNumber = d.CheckNumber,
                             DeliveryType = d.DeliveryType,
                             SupplierArea = d.SupplierArea,
                             SendCity = d.SendCity,
                             ArriveArea = d.ArriveArea,
                             ReceiveCity = d.ReceiveCity,
                             Pieces = d.Pieces,
                             Plates = d.Plates,
                             Payment = d.SupplierFee + d.AddFee,
                             AddFee = d.AddFee
                         }).Distinct().ToList();

            var truckEntity = truck.Select(d => new PalletCustomerWriteOffEntity
            {
                CustomerCode = d.CustomerCode,
                PrintDate = d.PrintDate,
                SupplierDate = d.SupplierDate,
                CustomerName = d.CustomerName,
                CustomerShortName = d.CustomerShortName,
                CheckNumber = d.CheckNumber,
                DeliveryType = d.DeliveryType,
                SupplierArea = d.SupplierArea,
                SendCity = d.SendCity,
                ArriveArea = d.ArriveArea,
                ReceiveCity = d.ReceiveCity,
                Pieces = d.Pieces,
                Plates = d.Plates,
                AddFee = d.AddFee,
                Payment = d.Payment
            }).ToList();

            checkNumberWithFee.AddRange(truckEntity);
            #endregion

            #region 論板自訂運價
            var defineCustomerFee = (from d in data
                                     join l in latestDefineFee on new { CustomerCode = d.CustomerCode, StartCity = d.SendCity, EndCity = d.ReceiveCity, Pricingcode = d.DeliveryType } equals
                                                                  new { CustomerCode = l.CustomerCode, StartCity = l.StartCity, EndCity = l.EndCity, Pricingcode = l.PricingCode } into temp
                                     from l in temp.DefaultIfEmpty(new TbPriceBusinessDefine { StartCity = default(string), EndCity = default(string) })
                                     where defineCustomers.Contains(d.CustomerCode) && d.DeliveryType == "01"
                                     select new
                                     {
                                         CustomerCode = d.CustomerCode,
                                         PrintDate = d.PrintDate,
                                         SupplierDate = d.SupplierDate,
                                         CustomerName = d.CustomerName,
                                         CustomerShortName = d.CustomerShortName,
                                         CheckNumber = d.CheckNumber,
                                         DeliveryType = d.DeliveryType,
                                         SupplierArea = d.SupplierArea,
                                         SendCity = d.SendCity,
                                         ArriveArea = d.ArriveArea,
                                         ReceiveCity = d.ReceiveCity,
                                         Pieces = d.Pieces,
                                         Plates = d.Plates,
                                         AddFee = d.AddFee,
                                         Payment = d.DeliveryType == "02" ? //是否為論件
                                                    (d.Pieces == 1 ? d.Pieces * l.Plate1Price ?? 0 :
                                                    d.Pieces == 2 ? d.Pieces * l.Plate2Price ?? 0 :
                                                    d.Pieces == 3 ? d.Pieces * l.Plate3Price ?? 0 :
                                                    d.Pieces == 4 ? d.Pieces * l.Plate4Price ?? 0 :
                                                    d.Pieces == 5 ? d.Pieces * l.Plate5Price ?? 0 :
                                                    d.Pieces >= 6 ? d.Pieces * l.Plate6Price ?? 0 : 0) :
                                                    (d.Plates == 1 ? d.Plates * l.Plate1Price ?? 0 :
                                                    d.Plates == 2 ? d.Plates * l.Plate2Price ?? 0 :
                                                    d.Plates == 3 ? d.Plates * l.Plate3Price ?? 0 :
                                                    d.Plates == 4 ? d.Plates * l.Plate4Price ?? 0 :
                                                    d.Plates == 5 ? d.Plates * l.Plate5Price ?? 0 :
                                                    d.Plates >= 6 ? d.Plates * l.Plate6Price ?? 0 : 0)
                                     }).Distinct().ToList();

            var defineCustomerFeeEntity = defineCustomerFee.Where(a => a.Payment > 0).Select(d => new PalletCustomerWriteOffEntity
            //var defineCustomerFeeEntity = defineCustomerFee.Select(d => new PalletCustomerWriteOffEntity
            {
                CustomerCode = d.CustomerCode,
                PrintDate = d.PrintDate,
                SupplierDate = d.SupplierDate,
                CustomerName = d.CustomerName,
                CustomerShortName = d.CustomerShortName,
                CheckNumber = d.CheckNumber,
                DeliveryType = d.DeliveryType,
                SupplierArea = d.SupplierArea,
                SendCity = d.SendCity,
                ArriveArea = d.ArriveArea,
                ReceiveCity = d.ReceiveCity,
                Pieces = d.Pieces,
                Plates = d.Plates,
                AddFee = d.AddFee,
                Payment = d.CustomerShortName.Contains("退貨回板") ? 0 : d.Payment + d.AddFee
            }).ToList();

            var defineCustomerFeeEntity1 = defineCustomerFee.Where(a => a.Payment == 0).Select(d => new PalletCustomerWriteOffEntity
            {
                CustomerCode = d.CustomerCode,
                PrintDate = d.PrintDate,
                SupplierDate = d.SupplierDate,
                CustomerName = d.CustomerName,
                CustomerShortName = d.CustomerShortName,
                CheckNumber = d.CheckNumber,
                DeliveryType = d.DeliveryType,
                SupplierArea = d.SupplierArea,
                SendCity = d.SendCity,
                ArriveArea = d.ArriveArea,
                ReceiveCity = d.ReceiveCity,
                Pieces = d.Pieces,
                Plates = d.Plates,
                AddFee = d.AddFee,
                Payment = d.CustomerShortName.Contains("退貨回板") ? 0 : d.Payment + d.AddFee
            }).ToList();

            //#region 論板自訂運價找不到找論板公版運價
            var publicCustomerFeeNOtbPriceBusinessDefine = (from d in defineCustomerFeeEntity1
                                                            join l in publicVersionFee on new { StartCity = d.SendCity, EndCity = d.ReceiveCity, Pricingcode = d.DeliveryType } equals
                                                                                         new { StartCity = l.StartCity, EndCity = l.EndCity, Pricingcode = l.PricingCode } into temp
                                                            from l in temp.DefaultIfEmpty(new TbPriceSupplier { StartCity = default(string), EndCity = default(string) })
                                                            where defineCustomers.Contains(d.CustomerCode) && d.DeliveryType == "01" //&& !piecesCustomers.Contains(d.CustomerCode)
                                                            select new
                                                            {
                                                                CustomerCode = d.CustomerCode,
                                                                PrintDate = d.PrintDate,
                                                                SupplierDate = d.SupplierDate,
                                                                CustomerName = d.CustomerName,
                                                                CustomerShortName = d.CustomerShortName,
                                                                CheckNumber = d.CheckNumber,
                                                                DeliveryType = d.DeliveryType,
                                                                SupplierArea = d.SupplierArea,
                                                                SendCity = d.SendCity,
                                                                ArriveArea = d.ArriveArea,
                                                                ReceiveCity = d.ReceiveCity,
                                                                Pieces = d.Pieces,
                                                                Plates = d.Plates,
                                                                AddFee = d.AddFee,
                                                                Payment = d.CustomerShortName.Contains("退貨回板") ? (decimal)d.AddFee : (d.Plates == 1 ? d.Plates * l.Plate1Price ?? 0 :
                                                    d.Plates == 2 ? d.Plates * l.Plate2Price ?? 0 :
                                                    d.Plates == 3 ? d.Plates * l.Plate3Price ?? 0 :
                                                    d.Plates == 4 ? d.Plates * l.Plate4Price ?? 0 :
                                                    d.Plates == 5 ? d.Plates * l.Plate5Price ?? 0 :
                                                    d.Plates >= 6 ? d.Plates * l.Plate6Price ?? 0 : 0)
                                                            }).Distinct().ToList();

            defineCustomerFeeEntity1 = publicCustomerFeeNOtbPriceBusinessDefine.Select(d => new PalletCustomerWriteOffEntity
            {
                CustomerCode = d.CustomerCode,
                PrintDate = d.PrintDate,
                SupplierDate = d.SupplierDate,
                CustomerName = d.CustomerName,
                CustomerShortName = d.CustomerShortName,
                CheckNumber = d.CheckNumber,
                DeliveryType = d.DeliveryType,
                SupplierArea = d.SupplierArea,
                SendCity = d.SendCity,
                ArriveArea = d.ArriveArea,
                ReceiveCity = d.ReceiveCity,
                Pieces = d.Pieces,
                Plates = d.Plates,
                AddFee = d.AddFee,
                Payment = d.CustomerShortName.Contains("退貨回板") ? 0 : d.Payment + d.AddFee
            }).ToList();


            checkNumberWithFee.AddRange(defineCustomerFeeEntity);

            checkNumberWithFee.AddRange(defineCustomerFeeEntity1);
            #endregion

            #region 論件自訂運價
            var piecesFee = (from d in data
                             join l in piecesShippingFee on new { CustomerCode = d.CustomerCode, StartCity = d.SendCity, EndCity = d.ReceiveCity } equals
                                                          new { CustomerCode = l.CustomerCode, StartCity = l.StartCity, EndCity = l.EndCity } into temp
                             from l in temp.DefaultIfEmpty(new PalletPiecesShippingFeeDefine { StartCity = default(string), EndCity = default(string) })
                             where piecesCustomers.Contains(d.CustomerCode) && d.DeliveryType == "02"
                             select new
                             {
                                 CustomerCode = d.CustomerCode,
                                 PrintDate = d.PrintDate,
                                 SupplierDate = d.SupplierDate,
                                 CustomerName = d.CustomerName,
                                 CustomerShortName = d.CustomerShortName,
                                 CheckNumber = d.CheckNumber,
                                 DeliveryType = d.DeliveryType,
                                 SupplierArea = d.SupplierArea,
                                 SendCity = d.SendCity,
                                 ArriveArea = d.ArriveArea,
                                 ReceiveCity = d.ReceiveCity,
                                 Pieces = d.Pieces,
                                 Plates = d.Plates,
                                 AddFee = d.AddFee,
                                 Payment = (d.Pieces > 0 && d.Pieces < 21 ? d.Pieces * l.Twenty ?? 0 :
                                            d.Pieces >= 21 && d.Pieces < 41 ? d.Pieces * l.Forty ?? 0 :
                                            d.Pieces >= 41 && d.Pieces < 61 ? d.Pieces * l.Sixty ?? 0 :
                                            d.Pieces >= 61 && d.Pieces < 81 ? d.Pieces * l.Eighty ?? 0 :
                                            d.Pieces >= 81 && d.Pieces < 101 ? d.Pieces * l.OneHundred ?? 0 :
                                            d.Pieces > 100 ? d.Pieces * l.AboveHundred ?? 0 : 0)
                             }).Distinct().ToList();

            var piecesFeeEntity = piecesFee.Select(d => new PalletCustomerWriteOffEntity
            {
                CustomerCode = d.CustomerCode,
                PrintDate = d.PrintDate,
                SupplierDate = d.SupplierDate,
                CustomerName = d.CustomerName,
                CustomerShortName = d.CustomerShortName,
                CheckNumber = d.CheckNumber,
                DeliveryType = d.DeliveryType,
                SupplierArea = d.SupplierArea,
                SendCity = d.SendCity,
                ArriveArea = d.ArriveArea,
                ReceiveCity = d.ReceiveCity,
                Pieces = d.Pieces,
                Plates = d.Plates,
                AddFee = d.AddFee,
                Payment = d.Payment + d.AddFee
            }).ToList();

            checkNumberWithFee.AddRange(piecesFeeEntity);
            #endregion

            #region 論板公版運價
            var publicCustomerFee = (from d in data
                                     join l in publicVersionFee on new { StartCity = d.SendCity, EndCity = d.ReceiveCity, Pricingcode = d.DeliveryType } equals
                                                                  new { StartCity = l.StartCity, EndCity = l.EndCity, Pricingcode = l.PricingCode } into temp
                                     from l in temp.DefaultIfEmpty(new TbPriceSupplier { StartCity = default(string), EndCity = default(string), ClassLevel = 5 })
                                     where !defineCustomers.Contains(d.CustomerCode) && d.DeliveryType == "01" && !piecesCustomers.Contains(d.CustomerCode)
                                     select new
                                     {
                                         CustomerCode = d.CustomerCode,
                                         PrintDate = d.PrintDate,
                                         SupplierDate = d.SupplierDate,
                                         CustomerName = d.CustomerName,
                                         CustomerShortName = d.CustomerShortName,
                                         CheckNumber = d.CheckNumber,
                                         DeliveryType = d.DeliveryType,
                                         SupplierArea = d.SupplierArea,
                                         SendCity = d.SendCity,
                                         ArriveArea = d.ArriveArea,
                                         ReceiveCity = d.ReceiveCity,
                                         Pieces = d.Pieces,
                                         Plates = d.Plates,
                                         AddFee = d.AddFee,
                                         Payment = d.CustomerShortName.Contains("退貨回板") ? 0 : (d.Plates == 1 ? d.Plates * l.Plate1Price ?? 0 :
                                                    d.Plates == 2 ? d.Plates * l.Plate2Price ?? 0 :
                                                    d.Plates == 3 ? d.Plates * l.Plate3Price ?? 0 :
                                                    d.Plates == 4 ? d.Plates * l.Plate4Price ?? 0 :
                                                    d.Plates == 5 ? d.Plates * l.Plate5Price ?? 0 :
                                                    d.Plates >= 6 ? d.Plates * l.Plate6Price ?? 0 : 0)
                                     }).Distinct().ToList();

            var publicCustomerFeeEntity = publicCustomerFee.Select(d => new PalletCustomerWriteOffEntity
            {
                CustomerCode = d.CustomerCode,
                PrintDate = d.PrintDate,
                SupplierDate = d.SupplierDate,
                CustomerName = d.CustomerName,
                CustomerShortName = d.CustomerShortName,
                CheckNumber = d.CheckNumber,
                DeliveryType = d.DeliveryType,
                SupplierArea = d.SupplierArea,
                SendCity = d.SendCity,
                ArriveArea = d.ArriveArea,
                ReceiveCity = d.ReceiveCity,
                Pieces = d.Pieces,
                Plates = d.Plates,
                AddFee = d.AddFee,
                Payment = d.CustomerShortName.Contains("退貨回板") ? 0 : d.Payment + d.AddFee
            }).ToList();

            checkNumberWithFee.AddRange(publicCustomerFeeEntity);
            #endregion

            #region 論件公版運價
            var piecesPublicVersionEntity = GetPalletPiecesPublicVersionFee(data);
            checkNumberWithFee.AddRange(piecesPublicVersionEntity);
            #endregion

            decimal? truckIncome = 0;
            decimal businessTax = 0.05M;

            truckIncome = truck.Sum(a => a.Payment);

            var total = checkNumberWithFee.Sum(a => a.Payment);

            var totalPalletCount = checkNumberWithFee.Sum(a => a.Plates);

            var truckPalletCount = truck.Sum(a => a.Plates);

            var truckPiecesCount = truck.Sum(a => a.Pieces);

            var totalPiecesCount = checkNumberWithFee.Sum(a => a.Pieces);

            var palletIncome = total - truckIncome;

            var totalWithTax = Math.Round((total * (1 + businessTax)) ?? 0, 0, MidpointRounding.AwayFromZero);

            var tax = Math.Round((total * businessTax) ?? 0, 0, MidpointRounding.AwayFromZero);

            Payment payment = new Payment()
            {
                PalletIncome = palletIncome,
                TruckIncome = truckIncome,
                Tax = tax,
                Total = totalWithTax,
                TotalPalletCount = totalPalletCount,
                TotalPiecesCount = totalPiecesCount,
                TruckPalletCount = truckPalletCount,
                TruckPiecesCount = truckPiecesCount
            };

            var customers = PalletCustomerWriteOffRepository.FindCustomerEntities(shortCustomerCode);

            FindCustomerEntity findCustomerEntity = new FindCustomerEntity()
            {
                CustomerShortname = customers[0].CustomerShortname,
                CustomerName = customers[0].CustomerName,
                UniformNumber = customers[0].UniformNumber,
                Telephone = customers[0].Telephone,
                ShipmentsAddress = customers[0].ShipmentsAddress,
                ShipmentsEmail = customers[0].ShipmentsEmail,
                BillingSpecialNeeds = customers[0].BillingSpecialNeeds,
                ShipmentsPrincipal = customers[0].ShipmentsPrincipal,
            };

            checkNumberWithFee = (from c in checkNumberWithFee orderby c.PrintDate ascending select c).ToList();

            foreach (var entity in checkNumberWithFee)
            {
                entity.Id = count++;
                entity.PaymentEntity = payment;
                entity.DeliveryType = pricingCodeChinese.Where(a => a.CodeId == entity.DeliveryType).FirstOrDefault()?.CodeName;
                entity.CustomersData = findCustomerEntity;
                entity.CustomerName = findCustomerEntity.CustomerName;
            }

            //checkNumberWithFee.OrderBy(a => new { CustomerCode = Convert.ToInt32(a.CustomerCode), CheckNumber = Convert.ToInt32(a.CheckNumber)});
            return checkNumberWithFee;
        }

        /// <summary>
        /// 客戶已結帳的明細，加入應收金額
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="shortCustomerCode"></param>
        /// <returns></returns>
        public List<Detail> GetMonthCustomerAlreadyWriteOffDetail(DateTime startDate, DateTime endDate, string shortCustomerCode = null)
        {
            var publicVersionFee = PalletCustomerWriteOffRepository.GetPriceSuppliers();
            var latestDefineFee = PalletCustomerWriteOffRepository.GetLatestDefineFee();
            var defineCustomers = latestDefineFee.Select(a => a.CustomerCode).Distinct().ToList();

            //論件自訂運價
            var piecesShippingFee = PalletCustomerWriteOffRepository.GetPalletPiecesShippingFeeDefine();
            var piecesCustomers = piecesShippingFee.Select(a => a.CustomerCode).Distinct().ToList();

            //論件公版運價
            var piecesPublicVersion = PalletCustomerWriteOffRepository.GetShippingFeePublicVersions();

            //List<Detail> data = PalletCustomerWriteOffRepository.GetMonthCustomerAlreadyWriteOffDetail(startDate, endDate.AddDays(1), shortCustomerCode);
            List<Detail> data = PalletCustomerWriteOffRepository.GetMonthCustomerAlreadyWriteOffDetail(startDate, endDate, shortCustomerCode);

            List<Detail> checkNumberWithFee = new List<Detail>();

            int count = 1;

            #region 計價模式為專車
            var truck = (from d in data
                         where d.PricingCode == "05"
                         select new
                         {
                             CustomerCode = d.CustomerCode,
                             PrintDate = d.PrintDate,
                             PricingCode = d.PricingCode,
                             SupplierDate = d.SupplierDate,
                             CustomerName = d.CustomerName,
                             CustomerShortname = d.CustomerShortname,
                             CheckNumber = d.CheckNumber,
                             SendCity = d.SendCity,
                             ReceiveCity = d.ReceiveCity,
                             OrderNumber = d.OrderNumber,
                             ReceiveContact = d.ReceiveContact,
                             ReceiveAddress = d.ReceiveAddress,
                             SendContact = d.SendContact,
                             SendAddress = d.SendAddress,
                             LatestArriveOptionDriver = d.LatestArriveOptionDriver,
                             InvoiceDesc = d.InvoiceDesc,
                             Pieces = d.Pieces,
                             Plates = d.Plates,
                             AddFee = d.AddFee,
                             Payment = d.SupplierFee + d.AddFee,
                         }).Distinct().ToList();

            var truckEntity = truck.Select(d => new Detail
            {
                CustomerCode = d.CustomerCode,
                PrintDate = d.PrintDate,
                PricingCode = d.PricingCode,
                SupplierDate = d.SupplierDate,
                CustomerName = d.CustomerName,
                CustomerShortname = d.CustomerShortname,
                CheckNumber = d.CheckNumber,
                SendCity = d.SendCity,
                ReceiveCity = d.ReceiveCity,
                OrderNumber = d.OrderNumber,
                ReceiveContact = d.ReceiveContact,
                ReceiveAddress = d.ReceiveAddress,
                SendContact = d.SendContact,
                SendAddress = d.SendAddress,
                LatestArriveOptionDriver = d.LatestArriveOptionDriver,
                InvoiceDesc = d.InvoiceDesc,
                Pieces = d.Pieces,
                Plates = d.Plates,
                AddFee = d.AddFee,
                Payment = d.Payment
            }).ToList();

            checkNumberWithFee.AddRange(truckEntity);
            #endregion

            #region 論板自訂運價
            var defineCustomerFee = (from d in data
                                     join l in latestDefineFee on new { CustomerCode = d.CustomerCode, StartCity = d.SendCity, EndCity = d.ReceiveCity, Pricingcode = d.PricingCode } equals
                                                                  new { CustomerCode = l.CustomerCode, StartCity = l.StartCity, EndCity = l.EndCity, Pricingcode = l.PricingCode } into temp
                                     from l in temp.DefaultIfEmpty(new TbPriceBusinessDefine { StartCity = default(string), EndCity = default(string), })
                                     where defineCustomers.Contains(d.CustomerCode) && d.PricingCode == "01"
                                     select new
                                     {
                                         Id = d.Id,
                                         CustomerCode = d.CustomerCode,
                                         
                                         PrintDate = d.PrintDate,
                                         PricingCode = d.PricingCode,
                                         SupplierDate = d.SupplierDate,
                                         CustomerName = d.CustomerName,
                                         CustomerShortname = d.CustomerShortname,
                                         CheckNumber = d.CheckNumber,
                                         SendCity = d.SendCity,
                                         ReceiveCity = d.ReceiveCity,
                                         OrderNumber = d.OrderNumber,
                                         ReceiveContact = d.ReceiveContact,
                                         ReceiveAddress = d.ReceiveAddress,
                                         SendContact = d.SendContact,
                                         SendAddress = d.SendAddress,
                                         LatestArriveOptionDriver = d.LatestArriveOptionDriver,
                                         InvoiceDesc = d.InvoiceDesc,
                                         Pieces = d.Pieces,
                                         Plates = d.Plates,
                                         AddFee = d.AddFee,
                                         Payment = d.PricingCode == "02" ? //是否為論件
                                                    (d.Pieces == 1 ? d.Pieces * l.Plate1Price ?? 0 :
                                                    d.Pieces == 2 ? d.Pieces * l.Plate2Price ?? 0 :
                                                    d.Pieces == 3 ? d.Pieces * l.Plate3Price ?? 0 :
                                                    d.Pieces == 4 ? d.Pieces * l.Plate4Price ?? 0 :
                                                    d.Pieces == 5 ? d.Pieces * l.Plate5Price ?? 0 :
                                                    d.Pieces >= 6 ? d.Pieces * l.Plate6Price ?? 0 : 0) :
                                                    (d.Plates == 1 ? d.Plates * l.Plate1Price ?? 0 :
                                                    d.Plates == 2 ? d.Plates * l.Plate2Price ?? 0 :
                                                    d.Plates == 3 ? d.Plates * l.Plate3Price ?? 0 :
                                                    d.Plates == 4 ? d.Plates * l.Plate4Price ?? 0 :
                                                    d.Plates == 5 ? d.Plates * l.Plate5Price ?? 0 :
                                                    d.Plates >= 6 ? d.Plates * l.Plate6Price ?? 0 : 0)
                                     }).Distinct().ToList();



            var defineCustomerFeeEntity = defineCustomerFee.Where(a => a.Payment > 0).Select(d => new Detail
            //var defineCustomerFeeEntity = defineCustomerFee.Select(d => new Detail
            {
                CustomerCode = d.CustomerCode,
                PrintDate = d.PrintDate,
                PricingCode = d.PricingCode,
                SupplierDate = d.SupplierDate,
                CustomerName = d.CustomerName,
                CustomerShortname = d.CustomerShortname,
                CheckNumber = d.CheckNumber,
                SendCity = d.SendCity,
                ReceiveCity = d.ReceiveCity,
                OrderNumber = d.OrderNumber,
                ReceiveContact = d.ReceiveContact,
                ReceiveAddress = d.ReceiveAddress,
                SendContact = d.SendContact,
                SendAddress = d.SendAddress,
                LatestArriveOptionDriver = d.LatestArriveOptionDriver,
                InvoiceDesc = d.InvoiceDesc,
                Pieces = d.Pieces,
                Plates = d.Plates,
                AddFee = d.AddFee,
                Payment = d.CustomerShortname.Contains("退貨回板") ? 0 : d.Payment + d.AddFee
            }).Distinct().ToList();

            var defineCustomerFeeEntity1 = defineCustomerFee.Where(a => a.Payment == 0).Select(d => new Detail
            //var defineCustomerFeeEntity = defineCustomerFee.Select(d => new Detail
            {
                CustomerCode = d.CustomerCode,
                PrintDate = d.PrintDate,
                PricingCode=d.PricingCode,
                SupplierDate = d.SupplierDate,
                CustomerName = d.CustomerName,
                CustomerShortname = d.CustomerShortname,
                CheckNumber = d.CheckNumber,
                SendCity = d.SendCity,
                ReceiveCity = d.ReceiveCity,
                OrderNumber = d.OrderNumber,
                ReceiveContact = d.ReceiveContact,
                ReceiveAddress = d.ReceiveAddress,
                SendContact = d.SendContact,
                SendAddress = d.SendAddress,
                LatestArriveOptionDriver = d.LatestArriveOptionDriver,
                InvoiceDesc = d.InvoiceDesc,
                Pieces = d.Pieces,
                Plates = d.Plates,
                AddFee = d.AddFee,
                Payment = d.CustomerShortname.Contains("退貨回板") ? 0 : d.Payment + d.AddFee
            }).Distinct().ToList();

            //#region 論板自訂運價找不到找論板公版運價
            var publicCustomerFeeNOtbPriceBusinessDefine = (from d in defineCustomerFeeEntity1
                                                            join l in publicVersionFee on new { StartCity = d.SendCity, EndCity = d.ReceiveCity, Pricingcode = d.PricingCode } equals
                                                                                         new { StartCity = l.StartCity, EndCity = l.EndCity, Pricingcode = l.PricingCode } into temp
                                                            from l in temp.DefaultIfEmpty(new TbPriceSupplier { StartCity = default(string), EndCity = default(string) })
                                                            where defineCustomers.Contains(d.CustomerCode) && d.PricingCode == "01" //&& !piecesCustomers.Contains(d.CustomerCode)
                                                            select new
                                                            {
                                                                CustomerCode = d.CustomerCode,
                                                                PrintDate = d.PrintDate,
                                                                SupplierDate = d.SupplierDate,
                                                                CustomerName = d.CustomerName,
                                                                CustomerShortname = d.CustomerShortname,
                                                                CheckNumber = d.CheckNumber,
                                                                SendCity = d.SendCity,
                                                                ReceiveCity = d.ReceiveCity,
                                                                OrderNumber = d.OrderNumber,
                                                                ReceiveContact = d.ReceiveContact,
                                                                ReceiveAddress = d.ReceiveAddress,
                                                                SendContact = d.SendContact,
                                                                SendAddress = d.SendAddress,
                                                                LatestArriveOptionDriver = d.LatestArriveOptionDriver,
                                                                InvoiceDesc = d.InvoiceDesc,
                                                                Pieces = d.Pieces,
                                                                Plates = d.Plates,
                                                                AddFee = d.AddFee,
                                                                Payment = d.CustomerShortname.Contains("退貨回板") ? (decimal)d.AddFee : (d.Plates == 1 ? d.Plates * l.Plate1Price ?? 0 :
                                                    d.Plates == 2 ? d.Plates * l.Plate2Price ?? 0 :
                                                    d.Plates == 3 ? d.Plates * l.Plate3Price ?? 0 :
                                                    d.Plates == 4 ? d.Plates * l.Plate4Price ?? 0 :
                                                    d.Plates == 5 ? d.Plates * l.Plate5Price ?? 0 :
                                                    d.Plates >= 6 ? d.Plates * l.Plate6Price ?? 0 : 0)
                                                            }).Distinct().ToList();

            defineCustomerFeeEntity1 = publicCustomerFeeNOtbPriceBusinessDefine.Select(d => new Detail
            {
                CustomerCode = d.CustomerCode,
                PrintDate = d.PrintDate,
                SupplierDate = d.SupplierDate,
                CustomerName = d.CustomerName,
                CustomerShortname = d.CustomerShortname,
                CheckNumber = d.CheckNumber,
                SendCity = d.SendCity,
                ReceiveCity = d.ReceiveCity,
                OrderNumber = d.OrderNumber,
                ReceiveContact = d.ReceiveContact,
                ReceiveAddress = d.ReceiveAddress,
                SendContact = d.SendContact,
                SendAddress = d.SendAddress,
                LatestArriveOptionDriver = d.LatestArriveOptionDriver,
                InvoiceDesc = d.InvoiceDesc,
                Pieces = d.Pieces,
                Plates = d.Plates,
                AddFee = d.AddFee,
                Payment = d.CustomerShortname.Contains("退貨回板") ? 0 : d.Payment + d.AddFee
            }).ToList();

            checkNumberWithFee.AddRange(defineCustomerFeeEntity);
            checkNumberWithFee.AddRange(defineCustomerFeeEntity1);
            #endregion

            #region 論件自訂運價
            var piecesFee = (from d in data
                             join l in piecesShippingFee on new { CustomerCode = d.CustomerCode, StartCity = d.SendCity, EndCity = d.ReceiveCity } equals
                                                          new { CustomerCode = l.CustomerCode, StartCity = l.StartCity, EndCity = l.EndCity } into temp
                             from l in temp.DefaultIfEmpty(new PalletPiecesShippingFeeDefine { StartCity = default(string), EndCity = default(string) })
                             where piecesCustomers.Contains(d.CustomerCode)
                             select new
                             {
                                 CustomerCode = d.CustomerCode,
                                 PrintDate = d.PrintDate,
                                 SupplierDate = d.SupplierDate,
                                 CustomerName = d.CustomerName,
                                 CustomerShortname = d.CustomerShortname,
                                 CheckNumber = d.CheckNumber,
                                 SendCity = d.SendCity,
                                 ReceiveCity = d.ReceiveCity,
                                 OrderNumber = d.OrderNumber,
                                 ReceiveContact = d.ReceiveContact,
                                 ReceiveAddress = d.ReceiveAddress,
                                 SendContact = d.SendContact,
                                 SendAddress = d.SendAddress,
                                 LatestArriveOptionDriver = d.LatestArriveOptionDriver,
                                 InvoiceDesc = d.InvoiceDesc,
                                 Pieces = d.Pieces,
                                 Plates = d.Plates,
                                 AddFee = d.AddFee,
                                 Payment = (d.Pieces > 0 && d.Pieces < 21 ? d.Pieces * l.Twenty ?? 0 :
                                            d.Pieces >= 21 && d.Pieces < 41 ? d.Pieces * l.Forty ?? 0 :
                                            d.Pieces >= 41 && d.Pieces < 61 ? d.Pieces * l.Sixty ?? 0 :
                                            d.Pieces >= 61 && d.Pieces < 81 ? d.Pieces * l.Eighty ?? 0 :
                                            d.Pieces >= 81 && d.Pieces < 101 ? d.Plates * l.OneHundred ?? 0 :
                                            d.Pieces > 100 ? d.Pieces * l.AboveHundred ?? 0 : 0)
                             }).Distinct().ToList();

            var piecesFeeEntity = piecesFee.Select(d => new Detail
            {
                CustomerCode = d.CustomerCode,
                PrintDate = d.PrintDate,
                SupplierDate = d.SupplierDate,
                CustomerName = d.CustomerName,
                CustomerShortname = d.CustomerShortname,
                CheckNumber = d.CheckNumber,
                SendCity = d.SendCity,
                ReceiveCity = d.ReceiveCity,
                OrderNumber = d.OrderNumber,
                ReceiveContact = d.ReceiveContact,
                ReceiveAddress = d.ReceiveAddress,
                SendContact = d.SendContact,
                SendAddress = d.SendAddress,
                LatestArriveOptionDriver = d.LatestArriveOptionDriver,
                InvoiceDesc = d.InvoiceDesc,
                Pieces = d.Pieces,
                Plates = d.Plates,
                AddFee = d.AddFee,
                Payment = d.Payment + d.AddFee
            }).ToList();

            checkNumberWithFee.AddRange(piecesFeeEntity);
            #endregion

            #region 論板公版運價
            var publicCustomerFee = (from d in data
                                     join l in publicVersionFee on new { StartCity = d.SendCity, EndCity = d.ReceiveCity, Pricingcode = d.PricingCode } equals
                                                                  new { StartCity = l.StartCity, EndCity = l.EndCity, Pricingcode = l.PricingCode } into temp
                                     from l in temp.DefaultIfEmpty(new TbPriceSupplier { StartCity = default(string), EndCity = default(string), ClassLevel = 5 })
                                     where !defineCustomers.Contains(d.CustomerCode) && d.PricingCode == "01" && !piecesCustomers.Contains(d.CustomerCode)
                                     select new
                                     {
                                         CustomerCode = d.CustomerCode,
                                         PrintDate = d.PrintDate,
                                         SupplierDate = d.SupplierDate,
                                         CustomerName = d.CustomerName,
                                         CustomerShortname = d.CustomerShortname,
                                         CheckNumber = d.CheckNumber,
                                         SendCity = d.SendCity,
                                         ReceiveCity = d.ReceiveCity,
                                         OrderNumber = d.OrderNumber,
                                         ReceiveContact = d.ReceiveContact,
                                         ReceiveAddress = d.ReceiveAddress,
                                         SendContact = d.SendContact,
                                         SendAddress = d.SendAddress,
                                         LatestArriveOptionDriver = d.LatestArriveOptionDriver,
                                         InvoiceDesc = d.InvoiceDesc,
                                         Pieces = d.Pieces,
                                         Plates = d.Plates,
                                         AddFee = d.AddFee,
                                         Payment = d.CustomerShortname.Contains("退貨回板") ? 0 : (d.Plates == 1 ? d.Plates * l.Plate1Price ?? 0 :
                                                    d.Plates == 2 ? d.Plates * l.Plate2Price ?? 0 :
                                                    d.Plates == 3 ? d.Plates * l.Plate3Price ?? 0 :
                                                    d.Plates == 4 ? d.Plates * l.Plate4Price ?? 0 :
                                                    d.Plates == 5 ? d.Plates * l.Plate5Price ?? 0 :
                                                    d.Plates >= 6 ? d.Plates * l.Plate6Price ?? 0 : 0)
                                     }).Distinct().ToList();

            var publicCustomerFeeEntity = publicCustomerFee.Select(d => new Detail
            {
                CustomerCode = d.CustomerCode,
                PrintDate = d.PrintDate,
                SupplierDate = d.SupplierDate,
                CustomerName = d.CustomerName,
                CustomerShortname = d.CustomerShortname,
                CheckNumber = d.CheckNumber,
                SendCity = d.SendCity,
                ReceiveCity = d.ReceiveCity,
                OrderNumber = d.OrderNumber,
                ReceiveContact = d.ReceiveContact,
                ReceiveAddress = d.ReceiveAddress,
                SendContact = d.SendContact,
                SendAddress = d.SendAddress,
                LatestArriveOptionDriver = d.LatestArriveOptionDriver,
                InvoiceDesc = d.InvoiceDesc,
                Pieces = d.Pieces,
                Plates = d.Plates,
                AddFee = d.AddFee,
                Payment = d.CustomerShortname.Contains("退貨回板") ? 0 : d.Payment + d.AddFee
            }).ToList();

            checkNumberWithFee.AddRange(publicCustomerFeeEntity);
            #endregion

            #region 論件公版運價
            var piecesPublicVersionFee = (from d in data
                                          join l in piecesPublicVersion on new { StartCity = d.SendCity, EndCity = d.ReceiveCity } equals
                                                                       new { StartCity = l.StartCity, EndCity = l.EndCity } into temp
                                          from l in temp.DefaultIfEmpty(new PalletPiecesShippingFeePublicVersion
                                          { StartCity = default(string), EndCity = default(string) })
                                          where d.PricingCode == "02" && !piecesCustomers.Contains(d.CustomerCode)
                                                && !defineCustomers.Contains(d.CustomerCode)
                                          select new
                                          {
                                              CustomerCode = d.CustomerCode,
                                              PrintDate = d.PrintDate,
                                              SupplierDate = d.SupplierDate,
                                              CustomerName = d.CustomerName,
                                              CustomerShortname = d.CustomerShortname,
                                              CheckNumber = d.CheckNumber,
                                              SendCity = d.SendCity,
                                              ReceiveCity = d.ReceiveCity,
                                              OrderNumber = d.OrderNumber,
                                              ReceiveContact = d.ReceiveContact,
                                              ReceiveAddress = d.ReceiveAddress,
                                              SendContact = d.SendContact,
                                              SendAddress = d.SendAddress,
                                              LatestArriveOptionDriver = d.LatestArriveOptionDriver,
                                              InvoiceDesc = d.InvoiceDesc,
                                              Pieces = d.Pieces,
                                              Plates = d.Plates,
                                              AddFee = d.AddFee,
                                              Payment = (d.Pieces > 0 && d.Pieces < 21 ? d.Pieces * l.Twenty ?? 0 :
                                                         d.Pieces >= 21 && d.Pieces < 41 ? d.Pieces * l.Forty ?? 0 :
                                                         d.Pieces >= 41 && d.Pieces < 61 ? d.Pieces * l.Sixty ?? 0 :
                                                         d.Pieces >= 61 && d.Pieces < 81 ? d.Pieces * l.Eighty ?? 0 :
                                                         d.Pieces >= 81 && d.Pieces < 101 ? d.Plates * l.OneHundred ?? 0 :
                                                         d.Pieces > 100 ? d.Pieces * l.AboveHundred ?? 0 : 0)
                                          }).Distinct().ToList();

            var piecesPublicVersionFeeEntity = piecesPublicVersionFee.Select(d => new Detail
            {
                CustomerCode = d.CustomerCode,
                PrintDate = d.PrintDate,
                SupplierDate = d.SupplierDate,
                CustomerName = d.CustomerName,
                CustomerShortname = d.CustomerShortname,
                CheckNumber = d.CheckNumber,
                SendCity = d.SendCity,
                ReceiveCity = d.ReceiveCity,
                OrderNumber = d.OrderNumber,
                ReceiveContact = d.ReceiveContact,
                ReceiveAddress = d.ReceiveAddress,
                SendContact = d.SendContact,
                SendAddress = d.SendAddress,
                LatestArriveOptionDriver = d.LatestArriveOptionDriver,
                InvoiceDesc = d.InvoiceDesc,
                Pieces = d.Pieces,
                Plates = d.Plates,
                AddFee = d.AddFee,
                Payment = d.CustomerShortname.Contains("退貨回板") ? 0 : d.Payment + d.AddFee
            }).ToList();

            checkNumberWithFee.AddRange(piecesPublicVersionFeeEntity);
            #endregion

            decimal businessTax = 0.05M;

            var total = checkNumberWithFee.Sum(a => a.Payment);

            var totalPalletCount = checkNumberWithFee.Sum(a => a.Plates);

            var totalPiecesCount = checkNumberWithFee.Sum(a => a.Pieces);

            var totalWithTax = Math.Round((total * (1 + businessTax)) ?? 0, 0, MidpointRounding.AwayFromZero);

            var tax = Math.Round((total * businessTax) ?? 0, 0, MidpointRounding.AwayFromZero);

            Payment payment = new Payment()
            {
                Tax = tax,
                Total = totalWithTax,
                TotalPalletCount = totalPalletCount,
                TotalPiecesCount = totalPiecesCount,
            };

            checkNumberWithFee = (from c in checkNumberWithFee orderby c.PrintDate ascending select c).ToList();

            foreach (var entity in checkNumberWithFee)
            {
                entity.Id = count++;
                entity.PaymentEntity = payment;
            }

            return checkNumberWithFee;
        }

        public List<PalletCustomerRequestPaymentLog> GetHistoryByCustomerCodeAndMonths(DateTime startMonth, DateTime endMonth, string shortCustomerCode)
        {
            startMonth = new DateTime(startMonth.Year, startMonth.Month, 01);
            endMonth = new DateTime(endMonth.Year, endMonth.Month, 01).AddMonths(1);
            var paymentLogs = PalletCustomerWriteOffRepository.GetPalletCustomerRequestPaymentLogs(startMonth, endMonth, shortCustomerCode);

            decimal businessTax = 0.05M;
            int i = 1;
            foreach (var item in paymentLogs)
            {
                item.Id = i++;
                item.Payment = Math.Round(item.Payment / (1 + businessTax) ?? 0, 0, MidpointRounding.AwayFromZero);
            }

            return paymentLogs;
        }

        public List<PalletCustomerDropDownList> GetTbSuppliersAndCheckoutDate()
        {
            var data = PalletCustomerWriteOffRepository.GetTbSuppliersAndCheckoutDate();
            return data;
        }

        /// <summary>
        /// 取得該結帳日每一個客戶的已結帳應收金額與板數
        /// </summary>
        /// <param name="checkoutDate"></param>
        /// <returns></returns>
        public List<ShowInFrontend> GetTotalPaymentAndPlatesByOneSupplier(DateTime checkoutDate)
        {
            var supplier = GetTbSuppliersAndCheckoutDate();

            //var selfEmployed = supplier.Where(a => a.SupplierCode.Length == 8);

            //var distributor = supplier.Except(selfEmployed);

            //var TbCustomers = CustomerReposiroty.GetAll();

            //var findCustomerSupplierCodeAndName = ((from s in selfEmployed
            //                                        join c in TbCustomers on s.SupplierCode equals string.Concat(c.MasterCode, c.SecondId)
            //                                        select new { c.CustomerCode, s.SupplierCodeAndName }).Union(
            //                                       from s in distributor
            //                                       join c in TbCustomers on s.SupplierCode equals c.SupplierCode
            //                                       select new { c.CustomerCode, s.SupplierCodeAndName })).ToList();

            var paymentLogs = PalletCustomerWriteOffRepository.GetPalletCustomerRequestPaymentLogsByClosingDate(checkoutDate);

            var tcDeliveryRequests = PalletCustomerWriteOffRepository.GetOneMonthPalletTcDeliveryRequests(checkoutDate);

            List<ShowInFrontend> totalPaymentAndPlates = new List<ShowInFrontend>();

            decimal businessTax = 0.05M;

            int count = 1;
            int plates = 0;

            foreach (var item in paymentLogs)
            {
                var oneSupplier = tcDeliveryRequests.Where(a => a.PrintDate >= item.RequestStartDate && a.PrintDate < item.RequestEndDate.Value.AddDays(1)
                && (a.CustomerCode.Substring(0, 3).Equals(item.ShortCustomerCode) || a.CustomerCode.Substring(0, 8).Equals(item.ShortCustomerCode))).ToList();

                plates = oneSupplier.Sum(a => a.Plates) ?? 0;

                var supplierCodeAndName = supplier.Where(a => a.SupplierCode == item.ShortCustomerCode).FirstOrDefault()?.SupplierCodeAndName;

                ShowInFrontend one = new ShowInFrontend()
                {
                    StartDate = item.RequestStartDate.Value.Date,
                    Id = count++,
                    CustomerName = supplierCodeAndName == null ? "" : supplierCodeAndName.Substring(supplierCodeAndName.IndexOf("-") + 1),
                    CustomerCode = item.ShortCustomerCode,
                    Payment = Math.Round(item.Payment / (1 + businessTax) ?? 0, 0, MidpointRounding.AwayFromZero),
                    Plates = plates
                };

                totalPaymentAndPlates.Add(one);
            }

            return totalPaymentAndPlates;
        }

        /// <summary>
        /// 算論件公版運價
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public List<PalletCustomerWriteOffEntity> GetPalletPiecesPublicVersionFee(List<PalletCustomerWriteOffEntity> data)
        {
            var piecesPublicVersion = PalletCustomerWriteOffRepository.GetShippingFeePublicVersions();

            //論件自訂運價
            var piecesShippingFee = PalletCustomerWriteOffRepository.GetPalletPiecesShippingFeeDefine();
            var piecesCustomers = piecesShippingFee.Select(a => a.CustomerCode).Distinct().ToList();

            //論板自訂運價
            var latestDefineFee = PalletCustomerWriteOffRepository.GetLatestDefineFee();
            var defineCustomers = latestDefineFee.Select(a => a.CustomerCode).Distinct().ToList();

            var publicVersion = (from d in data
                                 join l in piecesPublicVersion on new { StartCity = d.SendCity, EndCity = d.ReceiveCity } equals
                                                              new { StartCity = l.StartCity, EndCity = l.EndCity } into temp
                                 from l in temp.DefaultIfEmpty(new PalletPiecesShippingFeePublicVersion
                                 { StartCity = default(string), EndCity = default(string) })
                                 where d.DeliveryType == "02" && !piecesCustomers.Contains(d.CustomerCode)
                                       && !defineCustomers.Contains(d.CustomerCode)
                                 select new
                                 {
                                     CustomerCode = d.CustomerCode,
                                     PrintDate = d.PrintDate,
                                     SupplierDate = d.SupplierDate,
                                     CustomerName = d.CustomerName,
                                     CheckNumber = d.CheckNumber,
                                     DeliveryType = d.DeliveryType,
                                     SupplierArea = d.SupplierArea,
                                     SendCity = d.SendCity,
                                     ArriveArea = d.ArriveArea,
                                     ReceiveCity = d.ReceiveCity,
                                     Pieces = d.Pieces,
                                     Plates = d.Plates,
                                     AddFee = d.AddFee,
                                     Payment = (d.Pieces > 0 && d.Pieces < 21 ? d.Pieces * l.Twenty ?? 0 :
                                                d.Pieces >= 21 && d.Pieces < 41 ? d.Pieces * l.Forty ?? 0 :
                                                d.Pieces >= 41 && d.Pieces < 61 ? d.Pieces * l.Sixty ?? 0 :
                                                d.Pieces >= 61 && d.Pieces < 81 ? d.Pieces * l.Eighty ?? 0 :
                                                d.Pieces >= 81 && d.Pieces < 101 ? d.Pieces * l.OneHundred ?? 0 :
                                                d.Pieces > 100 ? d.Pieces * l.AboveHundred ?? 0 : 0)
                                 }).Distinct().ToList();

            var publicVersionEntity = publicVersion.Select(d => new PalletCustomerWriteOffEntity
            {
                CustomerCode = d.CustomerCode,
                PrintDate = d.PrintDate,
                SupplierDate = d.SupplierDate,
                CustomerName = d.CustomerName,
                CheckNumber = d.CheckNumber,
                DeliveryType = d.DeliveryType,
                SupplierArea = d.SupplierArea,
                SendCity = d.SendCity,
                ArriveArea = d.ArriveArea,
                ReceiveCity = d.ReceiveCity,
                Pieces = d.Pieces,
                Plates = d.Plates,
                AddFee = d.AddFee,
                Payment = d.Payment + d.AddFee
            }).ToList();

            return publicVersionEntity;
        }

        public List<FindCustomerEntity> FindCustomerEntities(string shortCustomerCode)
        {
            return PalletCustomerWriteOffRepository.FindCustomerEntities(shortCustomerCode);
        }

        public void UpdateClosingDate(List<string> checknumbers, DateTime closingDate)
        {
            List<TcDeliveryRequest> tcDeliveryRequests = this.DeliveryRequestModifyRepository.SelectByCheckNumbers(checknumbers);

            foreach (var d in tcDeliveryRequests)
            {
                d.ClosingDate = closingDate;
                this.DeliveryRequestModifyRepository.Update(d);
            }
        }

        public void InsertPaymentLog(PalletCustomerWriteOffEntity palletCustomerWriteOffEntity, DateTime closingDate, string shortCustomerCode)
        {
            PalletCustomerRequestPaymentLog palletCustomerRequestPaymentLog = new PalletCustomerRequestPaymentLog()
            {
                ClosingDate = closingDate.AddMinutes(11), //手動結帳記號
                Payment = palletCustomerWriteOffEntity.PaymentEntity.Total,
                RequestEndDate = closingDate,
                ShortCustomerCode = shortCustomerCode,
                RequestStartDate = PalletCustomerWriteOffRepository.ForManualGetLastClosingDate(closingDate, shortCustomerCode)
            };

            PalletCustomerWriteOffRepository.InsertPalletCustomerRequestPaymentLog(palletCustomerRequestPaymentLog);
        }

        public DateTime ForManualGetLastMonthClosingDateOrLastManualClosingDate(DateTime closingDate, string shortCustomerCode)
        {
            return PalletCustomerWriteOffRepository.ForManualGetLastClosingDate(closingDate, shortCustomerCode);
        }

        public decimal[] GetLineChart(string customer, int year)
        {
            return PalletCustomerWriteOffRepository.GetLineChartArray(customer, year);
        }

        public byte[] ExportAlreadyWriteOffExcel(DateTime start, DateTime end, string shortCustomerCode)
        {
            var mainData = GetAlreadyWriteOffMonthCustomer(start, end, shortCustomerCode);

            if (mainData.Count > 0)
            {
                var output = Export(start, end, mainData);
                return output;
            }
            return new byte[10];
        }

        public byte[] ExportNotWriteOffExcel(DateTime start, DateTime end, string shortCustomerCode)
        {
            var mainData = GetNotWriteOffCustomerMonthDetail(start, end, shortCustomerCode);
            var output = Export(start, end, mainData);
            return output;
        }

        public DateTime ForManualGetLastClosingDate(DateTime closingDate, string shortCustomerCode)
        {
            return PalletCustomerWriteOffRepository.ForManualGetLastClosingDate(closingDate, shortCustomerCode);
        }

        public byte[] Export(DateTime start, DateTime end, List<PalletCustomerWriteOffEntity> mainData)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            using (ExcelPackage ep = new ExcelPackage())
            {
                ep.Workbook.Worksheets.Add("應收帳款");
                ExcelWorksheet sheet = ep.Workbook.Worksheets["應收帳款"];

                sheet.Cells.Style.Font.Name = "微軟正黑體";
                sheet.Cells.Style.Font.Size = 14;

                #region 客戶資訊
                sheet.Cells[1, 1, 1, 11].Merge = true;
                sheet.Cells[1, 1].Value = "峻富物流股份有限公司";
                sheet.Cells[1, 1].Style.Font.Size = 18;
                sheet.Cells[1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                sheet.Cells[2, 1, 2, 11].Merge = true;
                sheet.Cells[2, 1].Value = start.ToString("yyyy/MM/dd") + "~" + end.ToString("yyyy/MM/dd");
                sheet.Cells[2, 1].Style.Font.Size = 18;
                sheet.Cells[2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                sheet.Cells[3, 1, 3, 6].Merge = true;
                sheet.Cells[3, 1].Value = "客戶：" + mainData[0].CustomersData.CustomerName;
                sheet.Cells[3, 1].Style.Font.Size = 16;

                sheet.Cells[4, 1, 4, 4].Merge = true;
                sheet.Cells[4, 1].Value = "統一編號：" + mainData[0].CustomersData.UniformNumber;
                sheet.Cells[4, 1].Style.Font.Size = 16;

                sheet.Cells[5, 1, 5, 4].Merge = true;
                sheet.Cells[5, 1].Value = "聯絡電話：" + mainData[0].CustomersData.Telephone;
                sheet.Cells[5, 1].Style.Font.Size = 16;

                sheet.Cells[5, 5, 5, 11].Merge = true;
                sheet.Cells[5, 5].Value = "對帳人：" + mainData[0].CustomersData.ShipmentsPrincipal;
                sheet.Cells[5, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet.Cells[5, 5].Style.Font.Size = 16;

                sheet.Cells[6, 1, 6, 7].Merge = true;
                sheet.Cells[6, 1].Value = "地址：" + mainData[0].CustomersData.ShipmentsAddress;
                sheet.Cells[6, 1].Style.Font.Size = 16;

                sheet.Cells[6, 8, 6, 11].Merge = true;
                sheet.Cells[6, 8].Value = mainData[0].CustomersData.ShipmentsEmail;
                sheet.Cells[6, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet.Cells[6, 8].Style.Font.Size = 16;

                sheet.Cells[7, 1, 7, 4].Merge = true;
                sheet.Cells[7, 1].Value = "開立發票號碼：";
                sheet.Cells[7, 1].Style.Font.Size = 16;

                sheet.Cells[7, 5, 7, 11].Merge = true;
                sheet.Cells[7, 5].Value = mainData[0].CustomersData.BillingSpecialNeeds;
                sheet.Cells[7, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet.Cells[7, 5].Style.Font.Size = 16;
                #endregion

                #region 標題列
                sheet.Cells[8, 1].Value = "序號";
                sheet.Column(1).Width = 6;
                sheet.Cells[8, 2].Value = "發送日期";
                sheet.Column(2).Width = 15;
                sheet.Cells[8, 3].Value = "配送日期";
                sheet.Column(3).Width = 15;
                sheet.Cells[8, 4].Value = "配送模式";
                sheet.Column(4).Width = 13;
                sheet.Cells[8, 5].Value = "貨號";
                sheet.Column(5).Width = 16;
                sheet.Cells[8, 6].Value = "發送區";
                sheet.Column(6).Width = 27;
                sheet.Cells[8, 7].Value = "到著區";
                sheet.Column(7).Width = 27;
                sheet.Cells[8, 8].Value = "件數";
                sheet.Cells[8, 9].Value = "板數";
                sheet.Cells[8, 10].Value = "加收費";
                sheet.Cells[8, 11].Value = "貨件運費";
                sheet.Column(11).Width = 13;

                sheet.Cells[8, 1, 8, 11].Style.Fill.PatternType = ExcelFillStyle.Solid;                                                                                          //上色
                sheet.Cells[8, 1, 8, 11].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(248, 233, 160));
                sheet.Cells[8, 1].Style.Border.Left.Style = ExcelBorderStyle.Medium;
                sheet.Cells[8, 11].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                sheet.Cells[8, 1, 8, 11].Style.Border.Top.Style = ExcelBorderStyle.Medium;
                #endregion

                #region 主要貨號資訊
                for (int i = 0; i < mainData.Count; i++)
                {
                    sheet.Cells.Style.Font.Name = "微軟正黑體";
                    sheet.Cells.Style.Font.Size = 14;
                    sheet.Cells[9 + i, 1].Value = mainData[i].Id;
                    sheet.Cells[9 + i, 2].Value = mainData[i].PrintDate.Value.ToString("yyyy/MM/dd");
                    sheet.Cells[9 + i, 3].Value = mainData[i].SupplierDate.Value.ToString("yyyy/MM/dd");
                    sheet.Cells[9 + i, 4].Value = mainData[i].DeliveryType;
                    sheet.Cells[9 + i, 5].Value = mainData[i].CheckNumber;
                    sheet.Cells[9 + i, 6].Value = mainData[i].SupplierArea;
                    sheet.Cells[9 + i, 7].Value = mainData[i].ArriveArea;
                    sheet.Cells[9 + i, 8].Value = mainData[i].Pieces;
                    sheet.Cells[9 + i, 9].Value = mainData[i].Plates;
                    sheet.Cells[9 + i, 10].Value = mainData[i].AddFee;
                    sheet.Cells[9 + i, 11].Value = mainData[i].Payment;

                    sheet.Cells[9 + i, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheet.Cells[9 + i, 1, 9 + i, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    //sheet.Cells[9 + i, 1, 9 + i, 11].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    //sheet.Cells[9 + i, 1, 9 + i, 11].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    sheet.Cells[9 + i, 1].Style.Border.Left.Style = ExcelBorderStyle.Medium;
                    sheet.Cells[9 + i, 11].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                }
                #endregion

                #region 金額運算
                sheet.Cells[9 + mainData.Count, 7].Value = "棧板收入小計";
                sheet.Cells[9 + mainData.Count, 8].Formula = "SUMIF(D9:D" + (9 + mainData.Count - 1).ToString() + ",\"論板\",H9:H" + (9 + mainData.Count - 1).ToString() + ")";
                sheet.Cells[9 + mainData.Count, 9].Formula = "SUMIF(D9:D" + (9 + mainData.Count - 1).ToString() + ",\"論板\",I9:I" + (9 + mainData.Count - 1).ToString() + ")";
                sheet.Cells[9 + mainData.Count, 11].Formula = "SUMIF(D9:D" + (9 + mainData.Count - 1).ToString() + ",\"論板\",K9:K" + (9 + mainData.Count - 1).ToString() + ")";

                sheet.Cells[9 + mainData.Count + 1, 7].Value = "論件收入小計";
                sheet.Cells[9 + mainData.Count + 1, 8].Formula = "SUMIF(D9:D" + (9 + mainData.Count - 1).ToString() + ",\"論件\",H9:H" + (9 + mainData.Count - 1).ToString() + ")";
                sheet.Cells[9 + mainData.Count + 1, 9].Formula = "SUMIF(D9:D" + (9 + mainData.Count - 1).ToString() + ",\"論件\",I9:I" + (9 + mainData.Count - 1).ToString() + ")";
                sheet.Cells[9 + mainData.Count + 1, 11].Formula = "SUMIF(D9:D" + (9 + mainData.Count - 1).ToString() + ",\"論件\",K9:K" + (9 + mainData.Count - 1).ToString() + ")";

                sheet.Cells[9 + mainData.Count + 2, 7].Value = "專車收入小計";
                sheet.Cells[9 + mainData.Count + 2, 8].Formula = "SUMIF(D9:D" + (9 + mainData.Count - 1).ToString() + ",\"專車\",H9:H" + (9 + mainData.Count - 1).ToString() + ")";
                sheet.Cells[9 + mainData.Count + 2, 9].Formula = "SUMIF(D9:D" + (9 + mainData.Count - 1).ToString() + ",\"專車\",I9:I" + (9 + mainData.Count - 1).ToString() + ")";
                sheet.Cells[9 + mainData.Count + 2, 11].Formula = "SUMIF(D9:D" + (9 + mainData.Count - 1).ToString() + ",\"專車\",K9:K" + (9 + mainData.Count - 1).ToString() + ")";

                sheet.Cells[9 + mainData.Count + 3, 7].Value = "總計";
                sheet.Cells[9 + mainData.Count + 3, 8].Formula = "SUM(H9:H" + (9 + mainData.Count - 1).ToString() + ")";
                sheet.Cells[9 + mainData.Count + 3, 9].Formula = "SUM(I9:I" + (9 + mainData.Count - 1).ToString() + ")";
                sheet.Cells[9 + mainData.Count + 3, 11].Formula = "SUM(K" + (9 + mainData.Count).ToString() + ":K" + (9 + mainData.Count + 2).ToString() + ")";

                sheet.Cells[9 + mainData.Count + 4, 7].Value = "5%營業稅";
                sheet.Cells[9 + mainData.Count + 4, 11].Formula = "ROUND(K" + (9 + mainData.Count + 3).ToString() + "*0.05,0)";

                sheet.Cells[9 + mainData.Count + 5, 7].Value = "應收帳款";
                sheet.Cells[9 + mainData.Count + 5, 11].Formula = "SUM(K" + (9 + mainData.Count + 3).ToString() + ":K" + (9 + mainData.Count + 4).ToString() + ")";

                sheet.Cells[9 + mainData.Count, 1, 9 + mainData.Count + 5, 11].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                //sheet.Cells[9 + mainData.Count, 8, 9 + mainData.Count + 5, 11].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                //sheet.Cells[9 + mainData.Count, 8, 9 + mainData.Count + 5, 11].Style.Border.Left.Style = ExcelBorderStyle.Thin;

                sheet.Cells[9 + mainData.Count, 1, 9 + mainData.Count, 11].Style.Border.Top.Style = ExcelBorderStyle.Medium;
                sheet.Cells[9 + mainData.Count + 3, 1, 9 + mainData.Count + 3, 11].Style.Border.Top.Style = ExcelBorderStyle.Medium;
                sheet.Cells[9 + mainData.Count, 1, 9 + mainData.Count + 5, 1].Style.Border.Left.Style = ExcelBorderStyle.Medium;
                sheet.Cells[9 + mainData.Count, 11, 9 + mainData.Count + 5, 11].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                sheet.Cells[9 + mainData.Count + 5, 1, 9 + mainData.Count + 5, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;

                sheet.Cells.Style.Font.Name = "微軟正黑體";
                sheet.Cells.Style.Font.Size = 14;
                sheet.Cells[1, 1, 2, 1].Style.Font.Size = 18;
                sheet.Cells[3, 1, 7, 11].Style.Font.Size = 16;
                #endregion

                #region 匯款資訊與審核者
                sheet.Cells[9 + mainData.Count + 6, 1, 9 + mainData.Count + 6, 5].Merge = true;
                sheet.Cells[9 + mainData.Count + 7, 1, 9 + mainData.Count + 7, 5].Merge = true;
                sheet.Cells[9 + mainData.Count + 8, 1, 9 + mainData.Count + 8, 5].Merge = true;

                sheet.Cells[9 + mainData.Count + 6, 1].Value = "匯入行：國泰世華銀行六家分行 013-1391";
                sheet.Cells[9 + mainData.Count + 7, 1].Value = "帳號：139-03-000054-2";
                sheet.Cells[9 + mainData.Count + 8, 1].Value = "戶名：峻富物流股份有限公司";

                sheet.Row(9 + mainData.Count + 6).Height = 16;
                sheet.Row(9 + mainData.Count + 7).Height = 16;
                sheet.Row(9 + mainData.Count + 8).Height = 16;

                sheet.Cells[9 + mainData.Count + 10, 1].Value = "核准：";
                sheet.Cells[9 + mainData.Count + 10, 4].Value = "初審：";
                sheet.Cells[9 + mainData.Count + 10, 7].Value = "主管：";
                sheet.Cells[9 + mainData.Count + 10, 9].Value = "製表：";
                #endregion

                #region 頁尾
                sheet.HeaderFooter.EvenFooter.RightAlignedText = "第 " + ExcelHeaderFooter.PageNumber + " 頁，共 " + ExcelHeaderFooter.NumberOfPages + " 頁 印表日期 " + ExcelHeaderFooter.CurrentDate + "  " + ExcelHeaderFooter.CurrentTime;
                sheet.HeaderFooter.OddFooter.RightAlignedText = "第 " + ExcelHeaderFooter.PageNumber + " 頁，共 " + ExcelHeaderFooter.NumberOfPages + " 頁 印表日期 " + ExcelHeaderFooter.CurrentDate + "  " + ExcelHeaderFooter.CurrentTime;
                #endregion

                #region 列印設定
                decimal inch = 0.393700787M;
                sheet.PrinterSettings.PaperSize = ePaperSize.A4;
                sheet.PrinterSettings.Orientation = eOrientation.Portrait;
                sheet.PrinterSettings.HorizontalCentered = true;
                sheet.PrinterSettings.FitToPage = true;
                sheet.PrinterSettings.FitToWidth = 1;
                sheet.PrinterSettings.FitToHeight = 0;
                sheet.PrinterSettings.RepeatRows = new ExcelAddress("$1:$8");
                sheet.PrinterSettings.LeftMargin = 0.64M * inch;
                sheet.PrinterSettings.RightMargin = 0.64M * inch;
                sheet.PrinterSettings.TopMargin = 1.91M * inch;
                sheet.PrinterSettings.BottomMargin = 1.91M * inch;
                sheet.PrinterSettings.HeaderMargin = 0.76M * inch;
                sheet.PrinterSettings.FooterMargin = 0.76M * inch;

                #endregion

                return ep.GetAsByteArray();
            }
        }

        public Stream GenerateZipFile(List<InputJson> inputJsons)
        {
            var filenamesAndBytes = new Dictionary<string, byte[]>();

            foreach (var item in inputJsons)
            {
                byte[] oneExcel = ExportAlreadyWriteOffExcel(item.StartDate, item.EndDate, item.ShortCustomerCode);
                var customerName = FindCustomerEntities(item.ShortCustomerCode).FirstOrDefault()?.CustomerShortname;
                filenamesAndBytes.Add(DateTime.Now.ToString("yyyy_MM_dd") + customerName + "帳單.xlsx", oneExcel);
            }

            var zip = ZipData(filenamesAndBytes);
            var memoryStream = new MemoryStream(zip);

            return memoryStream;
        }
        public byte[] ZipData(Dictionary<string, byte[]> data)
        {
            using (var zipStream = new MemoryStream())
            {
                using (var zipArchive = new ZipArchive(zipStream, ZipArchiveMode.Update))
                {
                    foreach (var fileName in data.Keys)
                    {
                        var entry = zipArchive.CreateEntry(fileName);
                        using (var entryStream = entry.Open())
                        {
                            byte[] bytes = data[fileName];
                            entryStream.Write(bytes, 0, bytes.Length);
                        }
                    }
                }
                return zipStream.ToArray();
            }
        }
    }
}
