﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using LightYear.JunFuTrans.BL.BE.PayCustomerPayment;
using LightYear.JunFuTrans.DA.Repositories.Account;
using LightYear.JunFuTrans.DA.Repositories.Customer;
using LightYear.JunFuTrans.DA.Repositories.PayCustomerPayment;
using LightYear.JunFuTrans.DA.Repositories.Station;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.PayCustomerPayment
{
    public class PayCustomerPaymentService : IPayCustomerPaymentService
    {
        public IPayCustomerPaymentRepository PayCustomerPaymentRepository { get; set; }
        public IStationRepository StationRepository { get; private set; }
        public IDriverRepository DriverRepository { get; private set; }
        public ICustomerRepository CustomerRepository { get; set; }

        public PayCustomerPaymentService
           (IStationRepository stationRepository,
            IDriverRepository driverRepository,
            IPayCustomerPaymentRepository payCustomerPaymentRepository,
            ICustomerRepository customerRepository)
        {
            StationRepository = stationRepository;
            DriverRepository = driverRepository;
            PayCustomerPaymentRepository = payCustomerPaymentRepository;
            CustomerRepository = customerRepository;
        }

        public byte[] Export()
        {
            var memoryStream = new MemoryStream();
            using (var document = SpreadsheetDocument.Create(memoryStream, SpreadsheetDocumentType.Workbook))
            {
                var workbookPart = document.AddWorkbookPart();
                workbookPart.Workbook = new Workbook();

                var worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                worksheetPart.Worksheet = new Worksheet(new SheetData());

                var sheets = workbookPart.Workbook.AppendChild(new Sheets());
                sheets.Append(new Sheet()
                {
                    Id = workbookPart.GetIdOfPart(worksheetPart),
                    SheetId = 1,
                    Name = "工作表1"
                });

                // 從 Worksheet 取得要編輯的 SheetData
                var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

                // 建立資料列物件
                var row1 = new Row();
                // 在資料列中插入欄位
                row1.Append(
                    new Cell() { CellValue = new CellValue("發送站"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("到著站"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("客戶代號"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("貨號"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("代收金額"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("支付狀態(必填)"), DataType = CellValues.String }
                );

                var row = new Row();
                row.Append(
                    new Cell() { CellValue = new CellValue("大園倉"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("信義"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("F2900210002"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("103007510000"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("2500"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("已支付"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("範例"), DataType = CellValues.String }
                );

                // 插入資料列 
                sheetData.AppendChild(row);
                sheetData.InsertAfter(row1, row);

                DataValidations dataValidations = new DataValidations();

                //交易項目下拉選單
                DataValidation dataValidation = new DataValidation()
                {
                    Type = DataValidationValues.List,
                    AllowBlank = true,
                    SequenceOfReferences = new ListValue<StringValue>() { InnerText = "F:F" }
                };
                Formula1 formula = new Formula1();
                formula.Text = "\"" + "已支付,未支付" + "\"";

                dataValidation.Append(formula);
                dataValidations.Append(dataValidation);
                worksheetPart.Worksheet.AppendChild(dataValidations);

                workbookPart.Workbook.Save();
                document.Close();
            }
            return memoryStream.ToArray();
        }

        public string GetUniqueFileName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName) //只有檔名
                      + "_"
                      + Guid.NewGuid().ToString().Substring(0, 4)
                      + Path.GetExtension(fileName); //.副檔名
        }
        public void InsertBatch(List<DA.JunFuDb.PayCustomerPayment> tempEntities) 
        {
            var checknumbers = tempEntities.Select(a => a.CheckNumber).ToList();
            var checkToUpdate = PayCustomerPaymentRepository.IsInsertPayCustomerPaymentOrNot(checknumbers);

            if (checkToUpdate == null)
                PayCustomerPaymentRepository.InsertBatch(tempEntities);
            else
            {
                var needToUpdateCheckNumbers = checkToUpdate.Select(a => a.CheckNumber).ToList();
                var needToInsert = tempEntities.Where(a => !needToUpdateCheckNumbers.Contains(a.CheckNumber)).ToList();
                PayCustomerPaymentRepository.InsertBatch(needToInsert);

               var needToUpdate = tempEntities.Where(a => needToUpdateCheckNumbers.Contains(a.CheckNumber)).ToList();
                checkToUpdate.ForEach(a => a.UpdateDate = DateTime.Now);
                foreach (var n in checkToUpdate)
                {
                    n.IsPaid = needToUpdate.Where(a => a.CheckNumber.Equals(n.CheckNumber)).Select(a => a.IsPaid).FirstOrDefault();
                    PayCustomerPaymentRepository.Update(n);
                }
            }
        }

        public List<FrontendEntity> GetAll(DateTime start, DateTime end, string station = null, string customer = null)
        {
            var data = PayCustomerPaymentRepository.GetAll(start, end.AddDays(1), station, customer);
            var customers = CustomerRepository.GetAll();
            var stations = StationRepository.GetAll();

            foreach (var d in data)
            {
                d.Customer = d.Customer + "" + customers.Where(a=>a.CustomerCode == d.Customer).FirstOrDefault()?.CustomerName;
                d.SendStation = stations.Where(a => a.StationCode == d.SendStation).FirstOrDefault()?.StationName;
                d.AreaArriveCode = stations.Where(a => a.StationScode == d.AreaArriveCode).FirstOrDefault()?.StationName;
            }
            return data; 
        }

    }
}
