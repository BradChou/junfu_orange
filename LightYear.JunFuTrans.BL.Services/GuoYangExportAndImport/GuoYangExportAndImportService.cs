﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using LightYear.JunFuTrans.BL.BE.GuoYangExportAndImport;
using LightYear.JunFuTrans.DA.Repositories.Account;
using LightYear.JunFuTrans.DA.Repositories.GuoYangExportAndImport;
using LightYear.JunFuTrans.DA.Repositories.Station;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.GuoYangExportAndImport
{
    public class GuoYangExportAndImportService : IGuoYangExportAndImportService
    {
        public IGuoYangExportAndImportRepository GuoYangExportAndImportRepository { get; private set; }
        public IStationRepository StationRepository { get; private set; }
        public IDriverRepository DriverRepository { get; private set; }

        public GuoYangExportAndImportService
            (IGuoYangExportAndImportRepository guoYangExportAndImportRepository,
            IStationRepository stationRepository,
            IDriverRepository driverRepository)
        {
            GuoYangExportAndImportRepository = guoYangExportAndImportRepository;
            StationRepository = stationRepository;
            DriverRepository = driverRepository;
        }

        public byte[] Export()
        {
            var memoryStream = new MemoryStream();
            using (var document = SpreadsheetDocument.Create(memoryStream, SpreadsheetDocumentType.Workbook))
            {
                var workbookPart = document.AddWorkbookPart();
                workbookPart.Workbook = new Workbook();

                var worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                worksheetPart.Worksheet = new Worksheet(new SheetData());

                var sheets = workbookPart.Workbook.AppendChild(new Sheets());
                sheets.Append(new Sheet()
                {
                    Id = workbookPart.GetIdOfPart(worksheetPart),
                    SheetId = 1,
                    Name = "工作表1"
                });

                //ExcelPackage package = new ExcelPackage();
                //ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("工作表1");
                //using (var cells = worksheet.Cells[1, 1, 1, 4])
                //{
                //    cells.Style.Font.Bold = true;
                //    cells.Style.Fill.PatternType = ExcelFillStyle.Solid;
                //    cells.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                //}

                // 從 Worksheet 取得要編輯的 SheetData
                var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

                // 建立資料列物件
                var row = new Row();
                // 在資料列中插入欄位
                row.Append(
                    new Cell() { CellValue = new CellValue("貨號"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("件數"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("工號(只能填GY061)"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("配達日期(日期格式範例2020/9/22)"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("配達區分(只能填完成)"), DataType = CellValues.String }
                );

                //var row1 = new Row();
                //row1.Append(
                //    new Cell() { CellValue = new CellValue("100000000012"), DataType = CellValues.String },
                //    new Cell() { CellValue = new CellValue("1"), DataType = CellValues.String },
                //    new Cell() { CellValue = new CellValue("GY061"), DataType = CellValues.String },
                //    new Cell() { CellValue = new CellValue("2020-01-01"), DataType = CellValues.String },
                //    new Cell() { CellValue = new CellValue("完成"), DataType = CellValues.String }
                //);

                // 插入資料列 
                sheetData.AppendChild(row);
                //sheetData.InsertAfter(row1, row);

                workbookPart.Workbook.Save();
                document.Close();
            }
            return memoryStream.ToArray();
        }

        public void SetCaptionStyle(ExcelRange cell)
        {
            cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
            cell.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
            cell.Style.Font.Bold = true;
        }
        public string GetUniqueFileName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName) //只有檔名
                      + "_"
                      + Guid.NewGuid().ToString().Substring(0, 4)
                      + Path.GetExtension(fileName); //.副檔名
        }

        public List<FrontendEntity> GetByDateAndStationScode(DateTime start, DateTime end, string scode)
        {
            List<FrontendEntity> frontendEntities = GuoYangExportAndImportRepository.GetByDateAndStationScode(start, end, scode);

            foreach(var entity in frontendEntities)
            {
                entity.Transhipment = "GY061國陽";
                entity.DriverName = "PP990大園倉轉國陽";
                entity.StationName = "29大園倉";
            }
            return frontendEntities;
        }
    }
}
