﻿using LightYear.JunFuTrans.BL.BE.GuoYangExportAndImport;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.GuoYangExportAndImport
{
    public interface IGuoYangExportAndImportService
    {
        byte[] Export();

        void SetCaptionStyle(ExcelRange cell);

        string GetUniqueFileName(string fileName);

        List<FrontendEntity> GetByDateAndStationScode(DateTime start, DateTime end, string scode);
    }
}
