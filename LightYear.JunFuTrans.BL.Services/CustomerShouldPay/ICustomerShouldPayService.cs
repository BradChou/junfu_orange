﻿using LightYear.JunFuTrans.BL.BE.Barcode;
using LightYear.JunFuTrans.BL.BE.CustomerShouldPay;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.BL.Services.CustomerShouldPay
{
    public interface ICustomerShouldPayService
    {
        CustomerPaymentEntity GetCustomerPaymentByCustomerAndMonth(string customerCode, int year, int month);

        IEnumerable<CustomerPaymentEntity> GetByStationAndMonth(string stationCode, int year, int month, int isPaid);

        CustomerPaymentEntity GetByCustomerAndMonth(string customerCode, int year, int month);

        int UpdateAccountReceivable(CustomerPaymentEntity entity);

        CustomerShippingFee GetShippingFeeByCustomerCodeOrDefault(string customerCode);

        Dictionary<string, CustomerShippingFee> GetShippingFeeByCustomerCodesOrDefault(IEnumerable<string> customerCode);        

        int GetFeeByCustomerShippingFeeEntity(PickUpGoodTypeEntity type, CustomerShippingFee customerShippingFee);

        PickUpGoodTypeEntity MappingSizeToType(int threeSideSum);

        List<CustomerShippingFeeEntity> GetShippingFeeByStation(string stationCode);

        List<CustomerShippingFeeEntity> GetAllCustomerShippingFee();

        List<CustomerShippingFeeEntity> GetShippingFeeByCustomerCode(string customerCode);

        CustomerShippingFeeEntity UpdateShippingFee(CustomerShippingFeeEntity entity, string userAccount);

        string GetUniqueFileName(string fileName);

        void UpdateElectricReceipt(List<AccountReceivable> accountReceivables);
    }
}
