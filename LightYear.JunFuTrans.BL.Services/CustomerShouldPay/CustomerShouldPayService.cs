﻿using AutoMapper;
using LightYear.JunFuTrans.BL.BE.Barcode;
using LightYear.JunFuTrans.BL.BE.CustomerShouldPay;
using LightYear.JunFuTrans.BL.Services.Barcode;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.Repositories.Customer;
using LightYear.JunFuTrans.DA.Repositories.CustomerShouldPay;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRequest;
using LightYear.JunFuTrans.DA.Repositories.Station;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.BL.Services.CustomerShouldPay
{
    public class CustomerShouldPayService : ICustomerShouldPayService
    {
        public IDeliveryRequestRepository DeliveryRequestRepository { get; set; }

        public IDeliveryRequestJoinDeliveryScanLogRepository DeliveryRequestJoinScanLog { get; set; }

        public ICustomerRepository CustomerRepository { get; set; }

        public IStationRepository StationRepository { get; set; }

        public IScanItemService ScanItemService { get; set; }

        public IAccountReceivableRepository AccountReceivableRepository { get; set; }

        public ICustomerShippingFeeRepository CustomerShippingFeeRepository { get; set; }

        public IShippingFeeHistoryRepository ShippingFeeHistoryRepository { get; set; }

        public IMapper Mapper { get; set; }

        public CustomerShouldPayService(IDeliveryRequestRepository deliveryRequestRepository, IMapper mapper, ICustomerRepository customerRepository,
            IDeliveryRequestJoinDeliveryScanLogRepository deliveryRequestJoinScanLog, IStationRepository stationRepository, IScanItemService scanItemService
            , IAccountReceivableRepository accountReceivableRepository, ICustomerShippingFeeRepository customerShippingFeeRepository, IShippingFeeHistoryRepository shippingFeeHistoryRepository)
        {
            DeliveryRequestRepository = deliveryRequestRepository;
            Mapper = mapper;
            CustomerRepository = customerRepository;
            DeliveryRequestJoinScanLog = deliveryRequestJoinScanLog;
            StationRepository = stationRepository;
            ScanItemService = scanItemService;
            AccountReceivableRepository = accountReceivableRepository;
            CustomerShippingFeeRepository = customerShippingFeeRepository;
            ShippingFeeHistoryRepository = shippingFeeHistoryRepository;
        }

        #region 明細
        // 應收帳款明細
        public CustomerPaymentEntity GetCustomerPaymentByCustomerAndMonth(string customerCode, int year, int month)
        {
            var pickUpGoodTypeValues = (PickUpGoodTypeEntity[])Enum.GetValues(typeof(PickUpGoodTypeEntity));

            var pickUpGoodTypeDescriptions = new List<string>();
            foreach (var e in pickUpGoodTypeValues)
            {
                pickUpGoodTypeDescriptions.Add(ScanItemService.GetEnumDescription(e));
            }

            var accountReceivable = AccountReceivableRepository.GetByCustomerAndShipDate(customerCode, new DateTime(year, month, 1));



            return new CustomerPaymentEntity
            {
                InvoiceId = accountReceivable?.InvoiceId,
                CustomerCode = customerCode,
                CustomerName = CustomerRepository.GetByCustomerCode(customerCode).CustomerName,
                ShipDateStart = new DateTime(year, month, 1),
                ShipDateEnd = new DateTime(year, month, DateTime.DaysInMonth(year, month)),
                DetailEntities = GetDetialEntitiesByCustomerAndMonth(customerCode, year, month).ToArray(),
                PickUpGoodTypeValues = pickUpGoodTypeValues,
                PickUpGoodTypeDescriptions = pickUpGoodTypeDescriptions.ToArray(),
                ElectricReceipt = accountReceivable?.ElectronicReceipt
            };
        }

        private List<CustomerPaymentDetailEntity> GetDetialEntitiesByCustomerAndMonth(string customerCode, int year, int month)
        {
            IEnumerable<TcDeliveryRequest> data = DeliveryRequestJoinScanLog.GetRequestsAlreadyPickUpByCustomerAndMonth(customerCode, year, month);

            data = data.Where(r => !(r.CheckNumber.StartsWith("99") && r.OrderNumber.ToUpper().StartsWith("TH")));

            var stations = StationRepository.GetAll();
            var customers = CustomerRepository.GetAll();
            var calendars = CustomerShippingFeeRepository.GetAllTbCalendars();

            string[] holiday = new string[] { "SATURDAY", "SUNDAY", "HOLIDAY" };

            foreach (var da in data)
            {
                var arriveAssignDateKindOfDay = calendars.Where(a => string.Format("{0:d}", a.Date) == string.Format("{0:d}", da.ArriveAssignDate)).FirstOrDefault()?.KindOfDay;
                var holidayCustomer = customers.Where(a => a.CustomerCode == da.CustomerCode).FirstOrDefault()?.IsWeekendDelivered;

                if ((holidayCustomer.Equals(true) && da.HolidayDelivery.Equals(true) && da.AreaArriveCode != "99") ||
                        (holidayCustomer.Equals(true) && da.AreaArriveCode != "99" && holiday.Contains(arriveAssignDateKindOfDay)))
                {
                    da.HolidayDelivery = true;
                }
                else
                {
                    da.HolidayDelivery = false;
                }
            }

            var detailEntitiesList = Mapper.Map<List<CustomerPaymentDetailEntity>>(data);

            var detailEntities = (from d in detailEntitiesList
                                  
                                  
                                  select new CustomerPaymentDetailEntity
                                  {
                                      ShipDate = d.ShipDate,
                                      CheckNumber = d.CheckNumber,
                                      OrderId = d.OrderId,
                                      SupplyStationScode = d.SupplyStationScode,
                                      AreaArriveCode = d.AreaArriveCode,
                                      ReceiveMan = d.ReceiveMan,
                                      PickUpGoodTypeByDriver = d.PickUpGoodTypeByDriver,
                                      Weight = d.Weight,
                                      SideLengthSum = d.SideLengthSum,
                                      Length = d.Length,
                                      Width = d.Width,
                                      Height = d.Height,
                                      SubpoenaCategory = d.SubpoenaCategory,
                                      SubpoenaCategoryShowName = d.SubpoenaCategoryShowName,
                                      Price = d.Price,
                                      Pieces = d.Pieces,
                                      HolidayDelivery = d.HolidayDelivery == "True" ? "假日配送" : "一般配送"
                                  }).ToList();

            foreach(var d in detailEntities)
            {
                d.ArriveStationName = stations.Where(s => s.StationScode == d.AreaArriveCode).FirstOrDefault()?.StationName;
                d.SupplyStationName = stations.Where(s => s.StationScode == d.SupplyStationScode).FirstOrDefault()?.StationName;
            }

            for (int i = 0; i < detailEntities.Count; i++)
            {
                detailEntities[i].PickUpGoodTypeDescription = ScanItemService.GetEnumDescription(detailEntities[i].PickUpGoodTypeByDriver);

                // 丈量機大小對應規格
                detailEntities[i].GoodTypeByMachine = MappingSizeToType(detailEntities[i].SideLengthSum);

                detailEntities[i].GoodTypeByMachineShowName = ScanItemService.GetEnumDescription(detailEntities[i].GoodTypeByMachine);
            }

            return detailEntities;
        }

        #endregion

        #region 總表
        public IEnumerable<CustomerPaymentEntity> GetByStationAndMonth(string stationCode, int year, int month, int isPaid)
        {
            var customers = CustomerRepository.GetCustomersByStation(stationCode).Select(c => new { CustomerCode = c.CustomerCode, CustomerName = c.CustomerName, CustomerShortName = c.CustomerShortname, ProductType = c.ProductType });
            customers = customers.Where(c => c.ProductType != 2 && c.ProductType != 3);

            IEnumerable<TcDeliveryRequest> pickupedRequests = DeliveryRequestJoinScanLog.GetRequestsAlreadyPickUpByCustomerListAndMonth(customers.Select(c => c.CustomerCode), year, month);

            // 濾掉配異的逆物流
            pickupedRequests = pickupedRequests.Where(r => !(r.CheckNumber.StartsWith("99") && r.OrderNumber.ToUpper().StartsWith("TH")));

            var grouped = from p in pickupedRequests
                          group p by p.CustomerCode into g
                          select new { CustomerCode = g.Key, Peices = g.Count(), TotalShouldPay = g.Sum(r => r.Freight) };

            var accountReceivable = AccountReceivableRepository.GetByCustomersAndShipDateBatch(customers.Select(c => c.CustomerCode), new DateTime(year, month, 1));
            IEnumerable<CustomerPaymentEntity> dataInAccountReceivable = Mapper.Map<IEnumerable<CustomerPaymentEntity>>(accountReceivable);

            var result = (from g in grouped
                          join c in customers on g.CustomerCode equals c.CustomerCode
                          join a in dataInAccountReceivable on g.CustomerCode equals a.CustomerCode into ps
                          from a in ps.DefaultIfEmpty()
                          select new CustomerPaymentEntity
                          {
                              CustomerCode = g.CustomerCode,
                              CustomerName = c.CustomerName,
                              Peices = g.Peices,
                              TotalShouldPay = g.TotalShouldPay ?? 0,
                              IsPaid = a == null ? false : a.IsPaid,
                              PaidDate = a == null ? null : a.PaidDate,
                              Id = a == null ? 0 : a.Id,
                              InvoiceId = a == null ? null : a.InvoiceId,
                              ShipDateStart = new DateTime(year, month, 1),
                              ShipDateEnd = new DateTime(year, month, DateTime.DaysInMonth(year, month)),
                              ElectricReceipt = a == null ? null : a.ElectricReceipt
                          }).ToList();

            result = FillInvoiceId(result);

            for (int i = 0; i < result.Count; i++)
            {
                result[i].Year = result[i].InvoiceId.Substring(0, 3);
                result[i].Date = result[i].InvoiceId.Substring(0, 5) + DateTime.DaysInMonth(int.Parse(result[i].InvoiceId.Substring(0, 3)), int.Parse(result[i].InvoiceId.Substring(3, 2)));
                result[i].DanBiye = "20";
                result[i].CustomerCodeForExcel = result[i].CustomerCode.Replace("F", "");
                result[i].No = "0001";
                result[i].Tax = "";
                result[i].ContainTax = "1";
                result[i].PiecesForExcel = "1";
                result[i].TotalShouldPayWithoutTax = Math.Round((float)result[i].TotalShouldPay * 100 / 105).ToString();
            }

            InitializeAccountReceivable(result);

            if (isPaid == -1)
                return result;
            else if (isPaid == 0)
                return result.Where(c => c.IsPaid == false);
            else
                return result.Where(c => c.IsPaid == true);
        }

        private List<CustomerPaymentEntity> FillInvoiceId(List<CustomerPaymentEntity> entities)
        {
            int serialNumber = 0;
            string largestInvoiceId = entities.Select(a => a.InvoiceId).Max();
            if (largestInvoiceId != null && largestInvoiceId.Length > 0)
            {
                string largestFloatNum = largestInvoiceId.Substring(4, 2);
                int.TryParse(largestFloatNum, out serialNumber);
            }

            for (int i = 0; i < entities.Count(); i++)
            {
                if (entities[i].InvoiceId == null || entities[i].InvoiceId.Length == 0)
                {
                    serialNumber++;
                    entities[i].InvoiceId = CreateInvoiceId(entities[i].CustomerCode, entities[i].ShipDateStart, serialNumber);
                }
            }
            return entities;
        }

        private string CreateInvoiceId(string customerCode, DateTime startDate, int serialNumber)
        {
            string dt = TransAdToRoc(startDate);

            string customerCodeLastEight = customerCode.Substring(1, 7);

            // 三碼流水號
            serialNumber = (serialNumber > 999) ? serialNumber % 1000 : serialNumber;
            string serial = string.Format("{0,3:000}", serialNumber);

            // 檢查碼
            //long sum;
            string addUp = dt + serial + customerCodeLastEight;
            //bool success = long.TryParse(addUp, out sum);
            //string checkNum = (sum % 7).ToString();

            return addUp;
        }

        public CustomerPaymentEntity GetByCustomerAndMonth(string customerCode, int year, int month)
        {
            var customer = CustomerRepository.GetByCustomerCode(customerCode);
            if (customer.ProductType == 2 || customer.ProductType == 3)
            { 
                return new CustomerPaymentEntity
                {
                    CustomerCode = "此為超值箱、預購袋客代",
                    CustomerName = "",
                    Peices = 0,
                    TotalShouldPay = 0,
                    IsPaid = null,
                    PaidDate = null,
                    Id = 0,
                    InvoiceId = "",
                    ElectricReceipt = ""
                };
            }

            // 從 delivery request 抓件數和運價
            IEnumerable<TcDeliveryRequest> pickupedRequests = DeliveryRequestJoinScanLog.GetRequestsAlreadyPickUpByCustomerAndMonth(customerCode, year, month);

            pickupedRequests = pickupedRequests.Where(r => !(r.CheckNumber.StartsWith("99") && r.OrderNumber.ToUpper().StartsWith("TH")));

            var grouped = (from p in pickupedRequests
                           group p by p.CustomerCode into g
                           select new CustomerPaymentEntity
                           { CustomerCode = g.Key, Peices = g.Count(), TotalShouldPay = g.Sum(r => r.Freight ?? 0) }).FirstOrDefault();

            var accountReceivable = AccountReceivableRepository.GetByCustomerAndShipDate(customerCode, new DateTime(year, month, 1));
            CustomerPaymentEntity dataInAccountReceivable = Mapper.Map<CustomerPaymentEntity>(accountReceivable);

            if (dataInAccountReceivable == null)
                dataInAccountReceivable = new CustomerPaymentEntity();

            return new CustomerPaymentEntity
            {
                CustomerCode = customerCode,
                CustomerName = customer.CustomerName,
                Peices = grouped == null ? 0 : grouped.Peices,
                TotalShouldPay = grouped == null ? 0 : grouped.TotalShouldPay,
                IsPaid = dataInAccountReceivable.IsPaid,
                PaidDate = dataInAccountReceivable.PaidDate,
                Id = dataInAccountReceivable.Id,
                InvoiceId = dataInAccountReceivable.InvoiceId,
                ElectricReceipt = dataInAccountReceivable.ElectricReceipt ?? null
            };
        }

        public int UpdateAccountReceivable(CustomerPaymentEntity entity)
        {
            AccountReceivable accountReceivable = Mapper.Map<CustomerPaymentEntity, AccountReceivable>(entity);
            if (accountReceivable.Id == 0)
                return AccountReceivableRepository.Insert(accountReceivable);

            return AccountReceivableRepository.UpdatePaidInfo(accountReceivable);
        }

        public void InitializeAccountReceivable(IEnumerable<CustomerPaymentEntity> entities)
        {
            IEnumerable<AccountReceivable> accountReceivables = Mapper.Map<IEnumerable<AccountReceivable>>(entities);
            // insert
            var dataNotInDb = accountReceivables.Where(e => e.Id == 0).ToList();
            AccountReceivableRepository.BulkInsert(dataNotInDb);

            // update
            var dataInDb = accountReceivables.Where(e => e.Id != 0).ToList();
            AccountReceivableRepository.UpdateInvoiceId(dataInDb);
        }
        #endregion


        #region 客戶運價
        public CustomerShippingFee GetShippingFeeByCustomerCodeOrDefault(string customerCode)
        {
            var fee = CustomerShippingFeeRepository.GetCustomerShippingFeeByCustomerCode(customerCode);

            if (fee == null)
                fee = CustomerShippingFeeRepository.GetCustomerShippingFeeByCustomerCode("0");  // 其他客戶

            return fee;
        }

        public Dictionary<string, CustomerShippingFee> GetShippingFeeByCustomerCodesOrDefault(IEnumerable<string> customerCode)
        {
            var fee = CustomerShippingFeeRepository.GetCustomerShippingFeeByCustomerCodes(customerCode).Distinct();

            IEnumerable<string> customerCodesNotInShippingFee = customerCode.Except(fee.Select(f => f.CustomerCode));
            CustomerShippingFee elseCustomer = CustomerShippingFeeRepository.GetCustomerShippingFeeByCustomerCode("0");

            Dictionary<string, CustomerShippingFee> result = fee.ToLookup(f => f.CustomerCode, f => f).ToDictionary(f => f.Key, f => f.First());

            foreach (var e in customerCodesNotInShippingFee)
            {
                result.Add(e, elseCustomer);
            }

            return result;
        }


        public int GetFeeByCustomerShippingFeeEntity(PickUpGoodTypeEntity type, CustomerShippingFee customerShippingFee)
        {
            switch (type)
            {
                case PickUpGoodTypeEntity.NumberOne:
                    return customerShippingFee.No1Bag ?? 0;
                case PickUpGoodTypeEntity.NumberTwo:
                    return customerShippingFee.No2Bag ?? 0;
                case PickUpGoodTypeEntity.NumberThree:
                    return customerShippingFee.No3Bag ?? 0;
                case PickUpGoodTypeEntity.NumberFour:
                    return customerShippingFee.No4Bag ?? 0;
                case PickUpGoodTypeEntity.SSixty:
                    return customerShippingFee.S60Cm ?? 0;
                case PickUpGoodTypeEntity.SNinty:
                    return Decimal.ToInt32(customerShippingFee.S90Cm ?? 0);
                case PickUpGoodTypeEntity.SEleven:
                    return Decimal.ToInt32(customerShippingFee.S110Cm ?? 0);
                case PickUpGoodTypeEntity.STwelve:
                    return Decimal.ToInt32(customerShippingFee.S120Cm ?? 0);
                case PickUpGoodTypeEntity.SFifteen:
                    return Decimal.ToInt32(customerShippingFee.S150Cm ?? 0);
                case PickUpGoodTypeEntity.SEighteen:
                    return Decimal.ToInt32(customerShippingFee.S180 ?? 0);
                default:
                    return 0;
            }
        }

        public PickUpGoodTypeEntity MappingSizeToType(int threeSideSum)
        {
            int DeviationValueMillimeter = 50;
            threeSideSum -= DeviationValueMillimeter;

            if (threeSideSum > 1800)
                return PickUpGoodTypeEntity.SEighteen;
            else if (threeSideSum > 1500 && threeSideSum <= 1800)
                return PickUpGoodTypeEntity.SEighteen;
            else if (threeSideSum > 1200 && threeSideSum <= 1500)
                return PickUpGoodTypeEntity.SFifteen;
            else if (threeSideSum > 1100 && threeSideSum <= 1200)
                return PickUpGoodTypeEntity.STwelve;
            else if (threeSideSum > 900 && threeSideSum <= 1100)
                return PickUpGoodTypeEntity.SEleven;
            else if (threeSideSum > 600 && threeSideSum <= 900)
                return PickUpGoodTypeEntity.SNinty;
            else if (threeSideSum <= 600 && threeSideSum > 0)
                return PickUpGoodTypeEntity.SSixty;
            else
                return PickUpGoodTypeEntity.NumberFour;
        }
        #endregion
        private string TransAdToRoc(DateTime dt)        //只回傳民國年月
        {
            var rocCalendar = new System.Globalization.TaiwanCalendar();
            return rocCalendar.GetYear(dt).ToString() + string.Format("{0,2:00}" ,dt.Month);
        }
        #region 運價維護

        List<CustomerShippingFeeEntity> ICustomerShouldPayService.GetShippingFeeByStation(string stationCode)
        {
            var customerShippingFee = CustomerShippingFeeRepository.GetByStationCode(stationCode);

            return Mapper.Map<List<CustomerShippingFeeEntity>>(customerShippingFee);
        }

        List<CustomerShippingFeeEntity> ICustomerShouldPayService.GetAllCustomerShippingFee()
        {
            var customerShippingFee = CustomerShippingFeeRepository.GetAll();

            return Mapper.Map<List<CustomerShippingFeeEntity>>(customerShippingFee);
        }

        List<CustomerShippingFeeEntity> ICustomerShouldPayService.GetShippingFeeByCustomerCode(string customerCode)
        {
            var customerShippingFee = CustomerShippingFeeRepository.GetCustomerShippingFeeByCustomerCode(customerCode);

            List<CustomerShippingFee> customerShippingFeeList = new List<CustomerShippingFee> { customerShippingFee };

            return customerShippingFee == null ? null : Mapper.Map<List<CustomerShippingFeeEntity>>(customerShippingFeeList);
        }

        /// <summary>
        /// 更新運價表，把舊運價寫入 CustomerShippingFeeHistory
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="userAccount"></param>
        /// <returns></returns>
        public CustomerShippingFeeEntity UpdateShippingFee(CustomerShippingFeeEntity entity, string userAccount)
        {
            // 寫入歷史資料
            CustomerShippingFee oldData = CustomerShippingFeeRepository.GetCustomerShippingFeeByCustomerCode(entity.CustomerCode);
            var historyEntity = Mapper.Map<CustomerShippingFeeHistory>(oldData);
            historyEntity.UpdateDate = DateTime.Now;
            historyEntity.UpdateUser = userAccount;
            ShippingFeeHistoryRepository.InsertShippingFeeHistory(historyEntity);

            // 更新
            CustomerShippingFee shippingFee = Mapper.Map<CustomerShippingFee>(entity);
            CustomerShippingFeeRepository.UpdateShippingFee(shippingFee);

            // 如果生效日期已經過了，要去改 DeliveryRequests 的運價
            if(entity.EffectiveDate < DateTime.Now)
            {
                DeliveryRequestRepository.CallUpdateProcedure(entity.CustomerCode);
            }

            return entity;
        }
        #endregion

        #region 上傳電子發票

        public string GetUniqueFileName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName) //只有檔名
                      + "_"
                      + Guid.NewGuid().ToString().Substring(0, 4)
                      + Path.GetExtension(fileName); //.副檔名
        }

        public void UpdateElectricReceipt(List<AccountReceivable> accountReceivables)
        {
            AccountReceivableRepository.UpdateElectricReceipts(accountReceivables);
        }

        #endregion
    }
}
