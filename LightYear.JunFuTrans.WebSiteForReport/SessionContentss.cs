﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightYear.JunFuTrans.WebSiteForReport
{
    public class SessionContent
    {
		private const string PREFIX = "SessionContent_";

		public SessionContent()
		{
		}

        /// <summary>
        /// 使用者登入资料
        /// </summary>
        public static object CurrentUserData
        {
            get
            {
                try
                {
                    return (object)HttpContext.Current.Session[
                        PREFIX + "CURRENT_USER_DATA"];
                }
                catch (Exception)
                {
                    return null;
                }
            }

            set
            {
                HttpContext.Current.Session[
                    PREFIX + "CURRENT_USER_DATA"] = value;
            }
        }

    }
}