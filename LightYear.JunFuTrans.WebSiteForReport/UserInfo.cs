﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightYear.JunFuTrans.WebSiteForReport
{
    public class UserInfo
    {
        public string Station { get; set; }

        public string UserAccount { get; set; }

        public string UserName { get; set; }

        public int AccountType { get; set; }

        public string[] RoleName { get; set; }

        public int UsersId { get; set; }

        public int[] RoleId { get; set; }

        public DateTime CookieExpiration { get; set; }

        public string ActionStation { get; set; }

        public string StationShowName { get; set; }
    }
}