﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace LightYear.JunFuTrans.WebSiteForReport
{
    public class CurrentUser
    {
        public string UserReloginPageUrl { get; set; }

        public bool IsLoginOk { get; set; }

        public string LoginAccount { get; set; }

        public UserInfo UserInfoEntity { get; set; }

        public CurrentUser(HttpCookieCollection cookies)
        {
            //先初始登入頁
            this.UserReloginPageUrl = WebConfigurationManager.AppSettings["LoginUrl"];

            //先確認是否有登入的cookie
            if (cookies["OtherLogin"] != null)
            {
                string userInfoStr = HttpUtility.UrlDecode(cookies["OtherLogin"].Value);

                this.UserInfoEntity = JsonConvert.DeserializeObject<UserInfo>(userInfoStr);

                this.IsLoginOk = true;
                this.LoginAccount = this.UserInfoEntity.UserAccount;
            }
            else
            {
                GoReLogin();
            }

        }

        private void GoReLogin()
        {

            this.ClearData();

            string newUrl = string.Format("{0}?{1}={2}", this.UserReloginPageUrl, "ReturnUrl", HttpUtility.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));

            HttpContext.Current.Response.Redirect(newUrl);

        }


        private void ClearData()
        {
            this.IsLoginOk = false;
            this.LoginAccount = string.Empty;
            this.UserInfoEntity = new UserInfo();
        }
    }
}