﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DriverCollectionMoneyWriteOff
{
    public class DriverCollectionMoneyWriteOffEntity
    {
        public string ArriveStationName { get; set; } // 到著站所

        public string CheckNumber { get; set; } //貨號

        public string ScanItem { get; set; } //貨態

        public int? CollectionMoney { get; set; } //待收金額

        public bool IsDriverPaymentSheet { get; set; } 

        public string IsDriverPaymentSheetYN { get; set; } //是否製作成精算書(Y/N) 

        public string ReceiverType { get; set; } //代收類型

        public string DriverCode { get; set; } 

        public string DriverCodeAndName { get; set; } //作業人員

        public DateTime? ScanDate { get; set; } //作業時間
        
        public string CustomerCode { get; set; } //客代

        public string CustomerName { get; set; } //客代名稱

        public string ReceiveContact { get; set; } //收貨人



    }
}
