﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.CbmInfo
{
    public class CbmInfoEntity
    {
        public DateTime? PrintDate { get; set; }
        public string PrintDateFrontEnd { get; set; }
        public string CheckNumber { get; set; }

        public string Scale { get; set; }
        
        public string SidesSum { get; set; }

        public double? Length { get; set; }
        public double? Width { get; set; }
        public double? Height { get; set; }
        public double? Size { get; set; }
        public double? Weight { get; set; }
        public string DaYuanPicURL { get; set; }
        public string TaiChungPicURL { get; set; }
        public string DaYuanPicText { get; set; }
        public string TaiChungPicText { get; set; }

        public DateTime? MeasurementTime { get; set; }
    }

    public class CbmInfoInputEntity
    { 
        public string Station { get; set; }
        public string CustomerCode { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public string CheckNumber { get; set; }
    }
}
