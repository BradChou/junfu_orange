﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.SignPaperPhoto
{
    public class SignPaperPhotoInputEntity
    {
        public string CustomerCode { get; set; }
        public DateTime? PrintDateStart { get; set; }
        public DateTime? PrintDateEnd { get; set; }
        public DateTime? ShipDateStart { get; set; }
        public DateTime? ShipDateEnd { get; set; }
    }

    public class SignPaperPhotoOutputEntity
    {
        public int Id { get; set; }
        public string CheckNumber { get; set; }
        public string OrderNumber { get; set; }
        public string ReceiveContact { get; set; }
        public string ReceiveAddress { get; set; }
        public string PrintDate { get; set; }
        public string ShipDate { get; set; }
        public string ArriveDate { get; set; }
        public string SignPaperPath { get; set; }
        public string SignPaperString { get; set; }
        public int SourceType { get; set; }         //0:無，1:S3，2:80
    }

    public class RequestEntity
    {
        public string DataName { get; set; }
        public string AccountCode { get; set; }
        public CheckNumberWithPhotoAndType[] CheckNumbersAndPathsAndTypes { get; set; }
    }

    public class ProgressEntity
    { 
        public string DataName { get; set; }
        public int CompletePercent { get; set; }
        public string DataPathUrl { get; set; }
        public string DataPathString { get; set; }
        public string PredictRemaining { get; set; }    //預估剩餘時間
    }

    public class CheckNumberWithPhotoAndType
    {
        public string CheckNumber { get; set; }
        public string SignPhotoPath { get; set; }
        public int SourceType { get; set; }             //0:無，1:S3，2:80
    }

    public class CheckNumberWithPhoto
    { 
        public string CheckNumber { get; set; }
        public string SignPhotoPath { get; set; }
    }

    public class SignPaperPhotoAndDateTimeAndType
    {
        public string CheckNumber { get; set; }
        public string SignPaperPhotoPath { get; set; }
        public DateTime ScanDate { get; set; }
        public int SourceType { get; set; }         //0:無，1:S3，2:80
    }
}
