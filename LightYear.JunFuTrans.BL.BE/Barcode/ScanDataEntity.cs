﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LightYear.JunFuTrans.BL.BE
{
    public class ScanDataEntity
    {
        public ScanDataEntity()
        {
            ShipMode = "";
            Runs = "";
            SignFieldImage = "";
            SignFormImage = "";
        }

        public string ScanItem { get; set; }

        public string DriverCode { get; set; }

        public string CheckNumber { get; set; }

        public string ScanDate { get; set; }

        public string AreaArriveCode { get; set; }

        public string Platform { get; set; }

        public string CarNumber { get; set; }

        public string SowageRate { get; set; }

        public string Area { get; set; }

        public string ExceptionOption { get; set; }

        public string ArriveOption { get; set; }

        public string ShipMode { get; set; }

        public string GoodsType { get; set; }

        public string Pieces { get; set; }

        public string Weight { get; set; }

        public string Runs { get; set; }

        public string Plates { get; set; }

        public string SignFormImage { get; set; }

        public string SignFieldImage { get; set; }
    }
}
