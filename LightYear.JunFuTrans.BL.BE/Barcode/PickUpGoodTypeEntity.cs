﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Barcode
{
    /// <summary>
    /// 集貨貨件種類
    /// </summary>
    public enum PickUpGoodTypeEntity
    {
        /// <summary>
        /// S60
        /// </summary>
        [Description("S60")]
        SSixty = 6,

        /// <summary>
        /// S90
        /// </summary>
        [Description("S90")]
        SNinty = 9,

        /// <summary>
        /// S110
        /// </summary>
        [Description("S110")]
        SEleven = 11,

        /// <summary>
        /// S120
        /// </summary>
        [Description("S120")]
        STwelve = 12,

        /// <summary>
        /// S150
        /// </summary>
        [Description("S150")]
        SFifteen = 15,

        /// <summary>
        /// S180
        /// </summary>
        [Description("S180")]
        SEighteen = 18,

        /// <summary>
        /// 1號袋
        /// </summary>
        [Description("1號袋")]
        NumberOne = 1,

        /// <summary>
        /// 2號袋
        /// </summary>
        [Description("2號袋")]
        NumberTwo = 2,

        /// <summary>
        /// 3號袋
        /// </summary>
        [Description("3號袋")]
        NumberThree = 3,

        /// <summary>
        /// 4號袋
        /// </summary>
        [Description("4號袋")]
        NumberFour = 4,

        /// <summary>
        /// 沒量到
        /// </summary>
        [Description("")]
        None = 0

        ///// <summary>
        ///// S60
        ///// 箱裝
        ///// </summary>
        //[Description("箱裝")]
        //Box = 5,

        ///// <summary>
        ///// 超大袋
        ///// </summary>
        //[Description("超大袋")]
        //Super = 16
    }
}
