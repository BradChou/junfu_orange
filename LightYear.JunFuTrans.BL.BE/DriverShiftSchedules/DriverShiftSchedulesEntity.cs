﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DriverShiftSchedules
{
    public class InputEntity
    {
        public int Id { get; set; }

        public DateTime TakeOffDate { get; set; }

        public string StationScode { get; set; }

        public string StationName { get; set; }

        public string TakeOffDriverWithCode { get; set; }

        public string SubstituteDriverWithCode { get; set; }

        public DateTime? UpdateDate { get; set; }

        public string UpdateUser { get; set; }

    }
}
