﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.RemittanceAndCheckWriteOff
{
    public class RemittanceAndCheckWriteOffEntity
    {
        public decimal RequestId { get; set; }
        public string Station { get; set; } //到著站
        public string ReceiverType  { get; set; } //類型
        public string PaymentMethod { get; set; } //付款方式
        public string CheckNumber { get; set; } //貨號
        public int? Payment { get; set; } //金額
        public string? LastFiveNumber { get; set; } // 客戶帳號後五碼
        public string CustomerCodeAndName { get; set; } //客戶代號
        public DateTime? LatestArriveOptionDate { get; set; } //配達日期
        public bool? IsWriteOff { get; set; } //是否銷帳
        public string IsWriteOffChinese { get; set; } //是否銷帳顯示中文
        public DateTime? WriteOffDate { get; set; } //銷帳日期
    }
}
