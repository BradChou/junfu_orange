﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.CustomerShouldPay
{
    public class SearchFilter
    {
        public DateTime Date { get; set; }

        public string StationCode { get; set; }

        public string CustomerCode { get; set; }

        public int IsPaid { get; set; }
    }
}
