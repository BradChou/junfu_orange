﻿using LightYear.JunFuTrans.BL.BE.Barcode;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.CustomerShouldPay
{
    public class CustomerPaymentEntity
    {
        public CustomerPaymentEntity()
        {
            IsPaid = false;
            Id = 0;
        }

        public int Id { get; set; }

        public string CustomerCode { get; set; }

        public string CustomerName { get; set; }

        public string InvoiceId { get; set; }

        public int Peices { get; set; }

        public int TotalShouldPay { get; set; }

        public bool? IsPaid { get; set; }

        public string ElectricReceipt { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdateTime { get; set; }

        public DateTime? PaidDate { get; set; }

        public DateTime ShipDateStart { get; set; }

        public DateTime ShipDateEnd{ get; set; }


        public CustomerPaymentDetailEntity[] DetailEntities { get; set; }

        public string[] PickUpGoodTypeDescriptions { get; set; }

        public PickUpGoodTypeEntity[] PickUpGoodTypeValues { get; set; }

        public string Year { get; set; }

        public string Date { get; set; }

        public string DanBiye { get; set; } //單別

        public string No { get; set; }      //序號、產品代號

        public string Tax { get; set; }     //稅別

        public string ContainTax { get; set; }  //含稅否

        public string PiecesForExcel { get; set; }  //發票用的數量

        public string CustomerCodeForExcel { get; set; }        //去掉F

        public string TotalShouldPayWithoutTax { get; set; }    //應配去掉稅金
    }
}
