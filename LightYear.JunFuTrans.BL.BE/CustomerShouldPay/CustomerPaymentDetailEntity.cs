﻿using LightYear.JunFuTrans.BL.BE.Barcode;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.CustomerShouldPay
{
    public class CustomerPaymentDetailEntity
    {
        public DateTime? ShipDate { get; set; }

        public string CheckNumber { get; set; }

        public string OrderId { get; set; }

        public string SupplyStationScode { get; set; }

        public string SupplyStationName { get; set; }

        public string AreaArriveCode { get; set; }

        public string ArriveStationName { get; set; }

        public string ReceiveMan { get; set; }

        /// <summary>
        /// 司機填的規格
        /// </summary>
        public PickUpGoodTypeEntity PickUpGoodTypeByDriver { get; set; }

        /// <summary>
        /// 丈量機量的規格
        /// </summary>
        public PickUpGoodTypeEntity GoodTypeByMachine { get; set; }

        /// <summary>
        /// 丈量機量的規格(給人看的)
        /// </summary>
        public string GoodTypeByMachineShowName { get; set; }

        /// <summary>
        /// 丈量機量的三邊和
        /// </summary>
        public int SideLengthSum { get; set; }

        public int Length { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public string PickUpGoodTypeDescription { get; set; }

        public int Weight { get; set; }

        public string SubpoenaCategory { get; set; }

        public string SubpoenaCategoryShowName { get; set; }

        public int Price { get; set; }

        public int Pieces { get; set; }

        public string HolidayDelivery { get; set; }
    }
}
