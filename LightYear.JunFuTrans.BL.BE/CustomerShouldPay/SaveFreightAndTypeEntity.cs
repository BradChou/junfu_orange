﻿using LightYear.JunFuTrans.BL.BE.Barcode;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.CustomerShouldPay
{
    public class SaveFreightAndTypeEntity
    {
        public int Freight { get; set; }

        public string CheckNumber { get; set; }

        public string PickUpType { get; set; }
    }
}
