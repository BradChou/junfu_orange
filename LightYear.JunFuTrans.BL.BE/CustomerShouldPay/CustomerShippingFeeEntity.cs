﻿using LightYear.JunFuTrans.BL.BE.Barcode;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.CustomerShouldPay
{
    public class CustomerShippingFeeEntity
    {
        public int Id { get; set; }

        public string StationName { get; set; }

        public string CustomerCode { get; set; }

        public string CustomerName { get; set; }

        public int NoOne { get; set; }

        public int NoTwo { get; set; }

        public int NoThree { get; set; }

        public int NoFour { get; set; }

        public int S60 { get; set; }

        public int S90 { get; set; }

        public int S110 { get; set; }

        public int S120 { get; set; }

        public int S150 { get; set; }

        public int S180 { get; set; }

        public int HolidayPrice { get; set; }

        public int PieceRate { get; set; }

        public int LianYun { get; set; }

        public DateTime EffectiveDate { get; set; }
    }
}
