﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.ConstantData
{
    public class CityEntity
    {
        [JsonProperty("id")]
        public decimal Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("districts")]
        public List<DistrictEntity> Districts { get; set; }
    }

    public class DistrictEntity
    {
        [JsonProperty("id")]
        public decimal Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("zip_code")]
        public string ZipCode { get; set; }
    }
}
