﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.PickUpRequest
{
    enum PickUpRequestEnum
    {

    }

    /// <summary>
    /// 收件時間
    /// </summary>
    public enum PickUpTime
    {
        [Description("隨時")]
        AnyTime = 0,

        [Description("早上 9:00~12:00")]
        Morning = 1,

        [Description("下午 12:00~18:00")]
        AfterNoon = 2,

        [Description(null)]
        Null = 99
    }

    /// <summary>
    /// 承運載具
    /// </summary>
    public enum PickUpVechile
    {
        [Description("箱型貨車")]
        Car = 1,

        [Description("機車")]
        Bike = 2,

        [Description(null)]
        Null = 99
    }

    /// <summary>
    /// 貨件規格
    /// </summary>
    public enum PackageType
    {
        [Description("紙箱")]
        Box = 1,

        [Description("速配袋")]
        Bag = 2,

        [Description(null)]
        Null = 99
    }
}
