﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRequest
{
    public class TagImageBase64Entity
    {
        public TagImageBase64Entity()
        {
            this.Success = false;
        }

        public bool Success { get; set; }

        public string Message { get; set; }

        public string Image { get; set; }
    }
}
