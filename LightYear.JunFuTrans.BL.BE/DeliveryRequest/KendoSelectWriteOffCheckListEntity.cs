﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRequest
{
    public class KendoSelectWriteOffCheckListEntity
    {
        public KendoSelectWriteOffCheckListEntity()
        {
            this.results = new List<WriteOffCheckListEntity>();
            this.__count = 0;
        }

        public List<WriteOffCheckListEntity> results { get; set; }

        public int __count { get; set; }
    }
}
