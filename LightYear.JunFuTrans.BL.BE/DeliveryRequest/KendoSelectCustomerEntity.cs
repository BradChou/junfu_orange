﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRequest
{
    public class KendoSelectCustomerEntity
    {
        public KendoSelectCustomerEntity()
        {
            this.results = new List<CustomerEntity>();
        }

        public List<CustomerEntity> results { get; set; }

        public int __count { get; set; }
    }
}
