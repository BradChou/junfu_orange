﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.Enumeration;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRequest
{
    public class AccountsReceivableKeyEntity
    {
        public DateTime? SettleStartDate { get; set; }

        public DateTime? SettleEndDate { get; set; }

        public AccountsReceivableTypeConfig AccountsReceivableType { get; set; }

        public string CustomerCode { get; set; }

    }
}
