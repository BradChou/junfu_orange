﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRequest
{
    public class DeliveryRequestFrontendShowEntity
    {
        public Int64 Id { get; set; }

        public string CustomerCode { get; set; }

        public string CheckNumber { get; set; }

        public string ReceiverName { get; set; }

        public string ReceiveTel1 { get; set; }

        public string City { get; set; }

        public string Area { get; set; }

        public string Address { get; set; }

        public DateTime? ShipDate { get; set; }

        public string DeliveryDriverCode { get; set; }

        public string DeliveryShowName { get; set; }
    }
}
