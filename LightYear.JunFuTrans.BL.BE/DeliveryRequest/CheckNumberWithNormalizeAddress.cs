﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRequest
{
    public class CheckNumberWithNormalizeAddress
    {
        public string CheckNumber { get; set; }

        public string CustomerCode { get; set; }

        public string City { get; set; }

        public string Area { get; set; }

        public string Road { get; set; }
    }
}
