﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRequest
{
    public class WriteOffCheckListEntity
    {
        public WriteOffCheckListEntity()
        {
            this.Id = 0;
        }

        public int Id { get; set; }

        public string Information { get; set; }

        public string Type { get; set; }
    }
}
