﻿using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRequest
{
    public class DeliveryRequestEntity
    {
        public DeliveryRequestEntity()
        {
            this.Id = 0;
            this.ReceiveByArriveSiteFlag = false;
            this.Pieces = 0;
            this.Plates = 0;
            this.Cbm = 0;
            this.CbmSize = 0;
            this.CollectionMoney = 0;
            this.ArriveToPayFreight = 0;
            this.ArriveToPayAppend = 0;
            this.DonateInvoiceFlag = false;
            this.ElectronicInvoiceFlag = false;
            this.ReceiptFlag = false;
            this.PalletRecyclingFlag = false;
            this.PrintFlag = false;
            this.TurnBoard = false;
            this.UpStairs = false;
            this.DifficultDelivery = false;
            this.HctStatus = -1;
            this.PalletRequestId = 0;
            this.LessThanTruckload = true;
            this.RoundTrip = false;
            this.CurrentStatus = 0;
            this.ScanLogs = new List<DeliveryScanLogEntity>();
        }

        public Int64 Id { get; set; }

        public string PricingType { get; set; }

        public string CustomerCode { get; set; }

        public string CheckNumber { get; set; }

        public string CheckType { get; set; }

        public string OrderNumber { get; set; }

        public string ReceiveCustomerCode { get; set; }

        public string SubpoenaCategory { get; set; }

        public string ReceiveTel1 { get; set; }

        public string ReceiveTel1Ext { get; set; }

        public string ReceiveTel2 { get; set; }

        public string ReceiveTel2Ext { get; set; }

        public string ReceiverName { get; set; }

        public string Zip { get; set; }

        public string City { get; set; }

        public string Area { get; set; }

        public string Address { get; set; }

        public string AreaArriveCode { get; set; }

        public bool ReceiveByArriveSiteFlag { get; set; }

        public string ArriveAddress { get; set; }

        public int Pieces { get; set; }

        public int Plates { get; set; }

        public int Cbm { get; set; }

        public int CbmSize { get; set; }

        public decimal CollectionMoney { get; set; }

        public decimal ArriveToPayFreight { get; set; }

        public decimal ArriveToPayAppend { get; set; }

        public string SendContact { get; set; }

        public string SendTel { get; set; }

        public string SendZip { get; set; }

        public string SendCity { get; set; }

        public string SendArea { get; set; }

        public string SendAddress { get; set; }

        public bool DonateInvoiceFlag { get; set; }

        public bool ElectronicInvoiceFlag { get; set; }

        public string UniformNumber { get; set; }

        public string ArriveMobile { get; set; }

        public string ArriveEmail { get; set; }

        public string InvoiceMemo { get; set; }

        public string InvoiceDesc { get; set; }

        public string ProductCategory { get; set; }

        public string SpecialSend { get; set; } 

        public DateTime? ArriveAssignDate { get; set; }

        public string TimePeriod { get; set; }

        public bool ReceiptFlag { get; set; }

        public bool PalletRecyclingFlag { get; set; }

        public string PalletType { get; set; }

        public string SupplierCode { get; set; }

        public string SupplierName { get; set; }

        public DateTime? SupplierDate { get; set; }

        public string ReceiptNumber { get; set; }

        public decimal SupplierFee { get; set; }

        public decimal CSectionFee { get; set; }

        public decimal RemoteFee { get; set; }

        public decimal TotalFee { get; set; }

        public DateTime? PrintDate { get; set; }

        public bool PrintFlag { get; set; }

        public DateTime? CheckoutCloseDate { get; set; }

        public int AddTransfer { get; set; }

        public string SubCheckNumber { get; set; }

        public string ImportRandomCode { get; set; }

        public string CloseRandomCode { get; set; }

        public bool TurnBoard { get; set; }

        public bool UpStairs { get; set; }

        public bool DifficultDelivery { get; set; }

        public decimal TurnBoardFee { get; set; }

        public decimal UpStairsFee { get; set; }

        public decimal DifficultFee { get; set; }

        public DateTime? CancelDate { get; set; }

        public string CreateUser { get; set; }

        public DateTime CreateDate { get; set; }

        public string UpdateUser { get; set; }

        public DateTime? UpdateDate { get; set; }

        public int HctStatus { get; set; }

        public bool IsPallet { get; set; }

        public int PalletRequestId { get; set; }

        public bool LessThanTruckload { get; set; }

        public string Distributor { get; set; }

        public string Temperate { get; set; }

        public string DeliveryType { get; set; }

        public decimal? CbmLength { get; set; }
        
        public decimal? CbmWidth { get; set; }

        public decimal? CbmHeight { get; set; }

        public decimal? CbmWeight { get; set; }

        public decimal? CbmCont { get; set; }

        public string BagNo { get; set; }

        public string ArticleNumber { get; set; }

        public string SendPlatform { get; set; }

        public string ArticleName { get; set; }

        public bool RoundTrip { get; set; }

        public int CurrentStatus { get; set; }

        public string CurrentStatusName { get; set; }

        public DateTime? LastScanDateTime { get; set; }

        public DateTime? ShipDate { get; set; }

        public List<DeliveryScanLogEntity> ScanLogs { get; set; }
    }
}
