﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRequest
{
    public class GetDeliveryAnomalyShowEntity
    {
        public GetDeliveryAnomalyShowEntity()
        {
            this.Id = 0;
            this.No = 0;
            this.SendPices = 0;
            this.GetPices = 0;
        }

        public Decimal Id { get; set; }

        public int No { get; set; }

        public string ShipDate { get; set; }

        public string StationName { get; set; }

        public string CheckNumber { get; set; }

        public string SenderName { get; set; }

        public int SendPices { get; set; }

        public int GetPices { get; set; }

        public string ReceiverName { get; set; }

        public string ZhuArea { get; set; }

        public string PaiArea { get; set; }

        public string ArriveOption { get; set; }

        public string ScanItemName { get; set; }

        public string ArriveOptionDesc { get; set; }

        public DateTime? ScanDate { get; set; }

        public string ReceiveAddress { get; set; }

        public string ReceiveTel { get; set; }
    }
}
