﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance
{
    public class FrontEndShowEntity
    {
        public decimal Seq { get; set; }       
        public string CodeId { get; set; }
        public string CodeName { get; set; }
        public bool? ActiveFlag { get; set; }
        public string DutyDept { get; set; }
    }
}
