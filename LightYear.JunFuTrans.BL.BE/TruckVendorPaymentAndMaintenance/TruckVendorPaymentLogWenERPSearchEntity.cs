﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance
{
    public class TruckVendorPaymentLogWenERPSearchEntity
    {
        public string orderType { get; set; }
        public DateTime? startUpload { get; set; }
        public DateTime? endUpload { get; set; }
        public DateTime? startRegister { get; set; }
        public DateTime? endRegister { get; set; }
        public char[] downloadCheckedVal { get; set; }
        public bool? hasDownload { get; set; }
        public string duty { get; set; }
        public string createUser { get; set; }
    }
}
