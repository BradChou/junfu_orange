﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance
{
    public class TruckVendorPaymentLogFrontEndShowEntity
    {
        public int Id { get; set; }

        public DateTime UploadDate { get; set; }

        public string DutyDept { get; set; }

        public DateTime RegisterDate { get; set; }

        public string FeeType { get; set; }

        public string VendorName { get; set; }

        public string VendorUID { get; set; }

        public string CarNumber { get; set; }

        public int Payment { get; set; }
        public int Cost { get; set; }
        public int Spread { get; set; }
        public string Payable { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string PayableUID { get; set; }

        public string Memo { get; set; }

        public string Department { get; set; }

        public string CompanyType { get; set; }
        public bool? IsChecked { get; set; }
        public bool? IsActive { get; set; }
        public string StrChecked { get; set; }
        public string CreateUser { get; set; }

        public static implicit operator TruckVendorPaymentLogFrontEndShowEntity(TruckVendorPaymentLog data)
        {
            var strChecked = string.Empty;
            if (data.IsChecked != null)
            {
                strChecked = (bool)data.IsChecked == true ? "已核帳" : "未核帳";
            }
            if (data.IsActive != null)
            {
                strChecked = (bool)data.IsActive == true ? strChecked : strChecked + "(已作廢)";
            }

            return new TruckVendorPaymentLogFrontEndShowEntity
            {
                Id = data.Id,
                UploadDate = (DateTime)data.CreateDate,
                RegisterDate = (DateTime)data.RegisteredDate,
                FeeType = data.FeeType,
                VendorName = data.VendorName,
                Payable = data.Payable,
                Payment = (int)data.Payment,
                Cost = (int)data.Cost,
                Spread = (int)data.Spread,
                InvoiceNumber = data.InvoiceNumber,
                InvoiceDate = data.InvoiceDate,
                CarNumber = data.CarLicense,
                Memo = data.Memo,
                Department = data.Department,
                CompanyType = data.Company,
                IsChecked = data.IsChecked,
                IsActive = data.IsActive,
                CreateUser = data.CreateUser,
                StrChecked = strChecked
            };
        }

    }

    public class TruckVendorPaymentLogSearchEntity
    {
        public DateTime? startUpload { get; set; }
        public DateTime? endUpload { get; set; }
        public char[] checkAccountCheckedVal { get; set; }
        public bool? hasCheckAccount { get; set; }
        public string duty { get; set; }
        public string createUser { get; set; }
        public string feeType { get; set; }
        public string vendor { get; set; }
        public string noteSearch { get; set; }
        public string payble { get; set; }
    }

}
