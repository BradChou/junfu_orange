﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance
{
    public class TruckVendorPaymentLogDSERPShowEntity
    {
        public int Id { get; set; }
        public string FeeType { get; set; }
        public string Company { get; set; }
        public string VoucherCode { get; set; }
        public string DinShinItemCode { get; set; }
        public string SupplierUID { get; set; }
        public string CollectionPayCode { get; set; }
        public string CustomerUID { get; set; }
        public int Accounts { get; set; }
        public int CollectionAccounts { get; set; }
        public int Spread { get; set; }
        public string CarLicense { get; set; }
        public string Note02 { get; set; }
        public string Department { get; set; }
        public string CompanyUID { get; set; }
        public string InvoiceNumber { get; set; }
        //public DateTime? InvoiceDate { get; set; }
        public string InvoiceDate { get; set; }
        public string CreateUser { get; set; }
        public bool HasDownload { get; set; }
        public DateTime? DownloadDate { get; set; }
        public string DownloadUser { get; set; }
        public DateTime RegisteredDate { get; set; }

        public static implicit operator TruckVendorPaymentLogDSERPShowEntity(TruckVendorPaymentLogDSERP data)
        {
            var strInvoiceDate = string.Empty;
            if (data.InvoiceDate != null)
            {
                strInvoiceDate = ((DateTime)data.InvoiceDate).ToString("yyyyMMdd");
            }
            return new TruckVendorPaymentLogDSERPShowEntity
            {
                Id = data.Id,
                FeeType = data.FeeType,
                VoucherCode = data.VoucherCode,
                Company = data.Company,
                Accounts = data.Accounts,
                CollectionAccounts = data.CollectionAccounts,
                Spread = data.Spread,
                SupplierUID = data.SupplierUID,
                CustomerUID = data.CustomerUID,
                CollectionPayCode = data.CollectionPayCode,
                DinShinItemCode = data.DinShinItemCode,
                Note02 = data.Note02,
                Department = data.Department,
                CarLicense = data.CarLicense,
                CompanyUID = data.CompanyUID,
                InvoiceNumber = data.InvoiceNumber,
                InvoiceDate = strInvoiceDate,
                //InvoiceDate = data.InvoiceDate,
                CreateUser = data.CreateUser,
                HasDownload = data.HasDownload,
                DownloadDate = data.DownloadDate,
                DownloadUser = data.DownloadUser,
                RegisteredDate = data.RegisteredDate
            };
        }
    }
}
