﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance
{
    public class TruckVendorPaymentLogWenERPShowEntity
    {
        public int Id { get; set; }
        public string FeeType { get; set; }
        public DateTime? DownloadDate { get; set; }
        public string DownloadUser { get; set; }
        public string CreateUser { get; set; }
        public string Year { get; set; }
        public DateTime UploadDate { get; set; }
        public string InvoicingNumber { get; set; }
        public DateTime RegisteredDate { get; set; }
        public string DutyDept { get; set; }
        public string OrderType { get; set; }
        public string SupplierUID { get; set; }
        public string CustomerUID { get; set; }
        public string Number { get; set; }
        public string ProductCode { get; set; }
        public string TaxType { get; set; }
        public bool TaxInclude { get; set; }
        public string strTaxInclude { get; set; }
        public int Quantity { get; set; }
        public int Payment { get; set; }
        public int UnitPrice { get; set; }
        public string ElecInovoice { get; set; }
        public string PaperInovoice { get; set; }
        public DateTime? ReconciliationDate { get; set; }
        public DateTime? CheckoutDate { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string ManufactureCode { get; set; }
        public int? ProductionQuantity { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string VoucherType { get; set; }
        public string Note01 { get; set; }
        public string Note02 { get; set; }
        public string NoteMain { get; set; }
        public string SupplierContact { get; set; }
        public string InvoicingSpecial { get; set; }
        public string SupplierUniformID { get; set; }
        public string SupplierAddress { get; set; }
        public string PackageType { get; set; }
        public string ForeignCurrencyCode { get; set; }
        public double? ExchangeRate { get; set; }
        public double? ForeignCurrencyUnitPrice { get; set; }
        public double? ForeignCurrencyPayment { get; set; }
        public string OrderNumber { get; set; }
        public string PurchaseNumber { get; set; }
        public string SalesCode { get; set; }
        public string Department { get; set; }
        public string ProjectCode { get; set; }
        public string CarLicense { get; set; }
        public string Company { get; set; }
        public string VendorName { get; set; }

        public static implicit operator TruckVendorPaymentLogWenERPShowEntity(TruckVendorPaymentLogWenERP data)
        {
            var strTaxInclude = data.TaxInclude ? "1" : "0";
            return new TruckVendorPaymentLogWenERPShowEntity
            {
                Id = data.Id,
                FeeType = data.FeeType,
                Year = data.Year,
                DownloadDate = data.DownloadDate,
                DownloadUser = data.DownloadUser,
                RegisteredDate = data.RegisteredDate,
                InvoicingNumber = data.InvoicingNumber,
                Note01 = data.Note01,
                Note02 = data.Note02,
                NoteMain = data.NoteMain,
                CreateUser = data.CreateUser,
                CheckoutDate = data.CheckoutDate,
                ElecInovoice = data.ElecInovoice,
                PaperInovoice = data.PaperInovoice,
                ReconciliationDate = data.ReconciliationDate,
                PaymentDate = data.PaymentDate,
                UploadDate = data.CreateDate,
                InvoiceNumber = data.InvoiceNumber,
                InvoiceDate = data.InvoiceDate,
                ManufactureCode = data.ManufactureCode,
                Number = data.Number,
                OrderType = data.OrderType,
                ProductCode = data.ProductCode,
                Quantity = data.Quantity,
                TaxInclude = data.TaxInclude,
                strTaxInclude = strTaxInclude,
                TaxType = data.TaxType,
                ProductionQuantity = data.ProductionQuantity,
                Payment = data.Payment,
                UnitPrice = data.UnitPrice,
                SupplierUID = data.SupplierUID,
                CustomerUID = data.CustomerUID,
                VoucherType = data.VoucherType,
                SupplierContact = data.SupplierContact,
                InvoicingSpecial = data.InvoicingSpecial,
                SupplierUniformID = data.SupplierUniformID,
                SupplierAddress = data.SupplierAddress,
                PackageType = data.PackageType,
                ForeignCurrencyCode = data.ForeignCurrencyCode,
                ExchangeRate = data.ExchangeRate,
                ForeignCurrencyUnitPrice = data.ForeignCurrencyUnitPrice,
                ForeignCurrencyPayment = data.ForeignCurrencyPayment,
                OrderNumber = data.OrderNumber,
                PurchaseNumber = data.PurchaseNumber,
                SalesCode = data.SalesCode,
                Department = data.Department,
                ProjectCode = data.ProjectCode,
                CarLicense = data.CarLicense,
                Company = data.Company,
                VendorName = data.VendorName,
            };
        }
    }
}
