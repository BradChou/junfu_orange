﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance
{
    public class KendoSelectDeptEntity
    {
        public KendoSelectDeptEntity()
        {
            results = new List<JFDepartmentDropDownListEntity>();
        }

        public List<JFDepartmentDropDownListEntity> results { get; set; }

        public int __count { get; set; }
    }
}
