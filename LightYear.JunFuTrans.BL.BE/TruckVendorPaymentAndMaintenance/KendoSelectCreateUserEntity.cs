﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance
{
    public class KendoSelectCreateUserEntity
    {
        public KendoSelectCreateUserEntity()
        {
            results = new List<JFCreateUserDropDownListEntity>();
        }

        public List<JFCreateUserDropDownListEntity> results { get; set; }

        public int __count { get; set; }
    }
}
