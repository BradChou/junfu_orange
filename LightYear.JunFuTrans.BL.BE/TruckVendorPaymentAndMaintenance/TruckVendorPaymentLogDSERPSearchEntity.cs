﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance
{
    public class TruckVendorPaymentLogDSERPSearchEntity
    {
        public string voucherCode { get; set; }
        public string company { get; set; }
        public DateTime? startUpload { get; set; }
        public DateTime? endUpload { get; set; }
        public DateTime? startRegister { get; set; }
        public DateTime? endRegister { get; set; }
        public char[] downloadCheckedVal { get; set; }
        public bool? hasDownload { get; set; }
        public string duty { get; set; }
        public string createUser { get; set; }
        public string feeType { get; set; }
    }
}
