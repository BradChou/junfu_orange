﻿using LightYear.JunFuTrans.BL.BE.DriverDispatch;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.PalletACSectionShouldPay
{
    public class PalletACSectionShouldPayEntity
    {
        public decimal RequestId { get; set; }
        public DateTime? PrintDate { get; set; }
        public string CustomerCode { get; set; }
        public string TaskId { get; set; }
        public string CheckNumber { get; set; }
        public string UserName { get; set; }
        public string SendContact { get; set; }
        public string ReceiveContact { get; set; }
        public string SendCity { get; set; }
        public string ReceiveCity { get; set; }
        public string SendStation { get; set; }
        public string ArriveStation { get; set; }
        public int? Pieces { get; set; }
        public int? Plates { get; set; }
        public string SpecialNeed { get; set; }
        public string ASectionSupplier { get; set; }
        public DateTime? AShipDate { get; set; }
        public string ADriver { get; set; }
        public int ASectionPayment { get; set; }
        public string CSectionSupplier { get; set; }
        public string CSectionSupplierNo { get; set; }
        public string CDriver { get; set; }
        public int? CShippingFee { get; set; }
        public int? RemoteFee { get; set; }
        public int? CSectionSpecialFee { get; set; }
        public int CSectionPayment { get; set; }
        public string ArriveOption { get; set; }
        public int PalletACSectionShouldPayId { get; set; } 
        public string UpdateUser { get; set; }
        public string SendStationCode { get; set; } //發送站代碼
        public string ReceiveAddress { get; set; }
        public string CDeliveryDriver { get; set; }
        public DateTime? CDeliveryScanDate { get; set; }
        public int? CDeliveryScanCount { get; set; }
    }

    public class SuppliersEntity
    {
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
    }

    public class OneSupplierEntity
    {
        public DateTime? PrintDate { get; set; }
        public string CustomerCode { get; set; }
        public string TaskId { get; set; }
        public string CheckNumber { get; set; }
        public string SendContact { get; set; }
        public string ReceiveContact { get; set; }
        public string SendCity { get; set; }
        public string ReceiveCity { get; set; }
        public string SendStation { get; set; }
        public string ArriveStation { get; set; }
        public int? Pieces { get; set; }
        public int? Plates { get; set; }
        public string ASectionSupplier { get; set; }
        public string CSectionSupplier { get; set; }
        public string Route { get; set; }
        public int Payment { get; set; }
        public OneSupplierPayment MyPayment { get; set; }
    }

    public class OneSupplierPayment
    {
        public int Total { get; set; }
        public decimal Tax { get; set; }
        public decimal TotalPayment { get; set; }
    }

    public class ShowAllSupplierEntity
    {
        public string RequestMonth { get; set; }
        public string SupplierName { get; set; }
        public int ASectionPayment { get; set; }
        public int CSectionPayment { get; set; }
        public decimal Tax { get; set; }
        public decimal TotalPayment { get; set; }
    }

    public class ACSectionSupplierAndDriver
    {
        public string SupplierCodeAndName { get; set; }
        public string DriverCodeAndName { get; set; }
        public string CheckNumber { get; set; }
    }

    public class CSectionInfo 
    {
        public string CheckNumber { get; set; }
        public string Driver { get; set; }
        public DateTime? ScanDate { get; set; }
        public int ScanCount { get; set; }
    }

    public class CSectionOriginalShippingFee
    {
        public string SupplierNo { get; set; }
        public string SupplierArea { get; set; }
        public string ReceiveArea { get; set; }
        public int C1 { get; set; }
        public int C2 { get; set; }
        public int C3 { get; set; }
        public int C4 { get; set; }
        public int C5 { get; set; }
        public int C6 { get; set; }
    }

    public class ArriveOptionIsPaymentRequired
    {
        public ArriveOptionIsPaymentRequired()
        {
            PaymentRequired = true;
        }

        public string CodeId { get; set; }
        public string CodeName { get; set; }
        public bool PaymentRequired { get; set; }
    }

    public class InputJson
    {
        public string RequestMonth { get; set; }
        public string SupplierName { get; set; }
    }
}
