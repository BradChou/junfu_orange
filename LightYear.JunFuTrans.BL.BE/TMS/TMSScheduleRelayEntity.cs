﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.TMS
{
    public class TMSScheduleRelayEntity
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("arrive_at")]
        public string ArriveAt { get; set; }

        [JsonProperty("dispatch_at")]
        public string DispatchAt { get; set; }

        [JsonProperty("flow")]
        public string Flow { get; set; }
    }
}
