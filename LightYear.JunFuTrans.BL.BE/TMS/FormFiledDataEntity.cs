﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.BL.BE.TMS
{
    public class FormFiledDataEntity
    {
        public List<CarType> CarTypes { get; set; }

        public List<RunFlowType> RunFlowTypes { get; set; }

        public List<BSectionStop> BSectionStops { get; set; }

        public bool? IsCreatedFail { get; set; }
    }

}
