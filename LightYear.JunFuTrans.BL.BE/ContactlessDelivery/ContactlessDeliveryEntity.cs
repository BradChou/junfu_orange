﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.ContactlessDelivery
{
    public class ContactlessDeliveryEntity
    {
        public string CheckNumber { get; set; }
        public string SendContact { get; set; }
        public string ReceiveContact { get; set; }
        public string ReceiveTel1 { get; set; }
        public string ReceiveTel2 { get; set; }
        public int Pieces { get; set; }
        public int CollectionMoney { get; set; }
        public string Time { get; set; }
    }
}
