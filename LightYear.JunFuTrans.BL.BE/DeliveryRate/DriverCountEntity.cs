﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRate
{
    public class DriverCountEntity
    {
        public string DriverCode { get; set; }

        public int CountNumber { get; set; }
    }
}
