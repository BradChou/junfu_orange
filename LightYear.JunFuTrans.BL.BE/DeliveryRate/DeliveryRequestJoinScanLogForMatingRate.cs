﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRate
{
    public class DeliveryRequestJoinScanLogForMatingRate
    {
        public DateTime ShipDate { get; set; }

        public DateTime ScanDate { get; set; }

        public string ScanItem { get; set; }

        public string ArriveOption { get; set; }

        public string AreaArriveCode { get; set; }

        public string DriverCode { get; set; }

        public string CheckNumber { get; set; }
    }
}
