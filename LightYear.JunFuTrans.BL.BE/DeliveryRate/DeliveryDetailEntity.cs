﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRate
{
    public class DeliveryDetailEntity
    {
        public DateTime? ShipDate { get; set; }

        public string ScanItem { get; set; }

        public DateTime? ScanDate { get; set; }

        public string StationScode { get; set; }

        public string StationName { get; set; }

        public string DriverCode { get; set; }

        public string DriverName { get; set; }

        public string CheckNumber { get; set; }

        public string SendContact { get; set; }

        public string ReceiveContact { get; set; }

        public string ReceiveAddress { get; set; }

        public string ReceivePhone { get; set; }

        public int Pieces { get; set; }

        public DateTime? DestinationTime { get; set; }

        public DateTime? DeliveryTime { get; set; }

        public DateTime? DeliveryMatingTime { get; set; }

        public string ArriveOption { get; set; }

        public DateTime? WriteOffTime { get; set; }
    }
}
