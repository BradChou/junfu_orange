﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRate
{
    public class StationCountEntity
    {
        public string StationScode { get; set; }

        public int CountNumber { get; set; }
    }
}
