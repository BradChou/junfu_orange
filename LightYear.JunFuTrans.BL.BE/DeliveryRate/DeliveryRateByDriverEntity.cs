﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRate
{
    public class DeliveryRateByDriverEntity
    {
        public string StationName { get; set; }

        public string DriverCode { get; set; }

        public string DriverName { get; set; }

        public float OrderNumber { get; set; }    // 雖然取名叫number 但是有可能會顯示百分比例

        public float MatingNumBeforeNoon { get; set; }

        public float MatingNumBeforeFifteen { get; set; }

        public float MatingNumBeforeEighteen { get; set; }

        public float MatingNumBeforeTwenty { get; set; }

        public float MatingNumber { get; set; }

        public float NotWriteOffNumber { get; set; }

        public float DeliveryNumber { get; set; }
    }
}
