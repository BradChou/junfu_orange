﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRate
{
    public class StationEntity
    {
        public string StationScode { get; set; }

        public string StationCode { get; set; }

        public string StationName { get; set; }
    }
}
