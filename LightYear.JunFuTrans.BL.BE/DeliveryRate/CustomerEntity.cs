﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRate
{
    public class CustomerEntity
    {
        public string CustomerCode { get; set; }

        public string CustomerName { get; set; }
    }
}
