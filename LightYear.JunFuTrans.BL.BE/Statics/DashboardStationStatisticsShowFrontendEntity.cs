﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Statics
{
    public class DashboardStationStatisticsShowFrontendEntity
    {
        public Decimal Id { get; set; }

        public string Station { get; set; }

        public int ShouldDeliveryCount { get; set; }

        public int NonDeliveredFinishCount { get; set; }

        public decimal DPlusOneDeliveryRate { get; set; }

        public int DPlusThreeDepotCount { get; set; }
    }
}
