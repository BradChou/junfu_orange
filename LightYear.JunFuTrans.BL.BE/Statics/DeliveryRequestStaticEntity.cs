﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Statics
{
    public class DeliveryRequestStaticEntity
    {
        public DeliveryRequestStaticEntity()
        {
            this.TotalCount = 0;
            this.MomoCount = 0;
            this.NonMomoCount = 0;
        }

        public int TotalCount { get; set; }

        public int MomoCount { get; set; }

        public int NonMomoCount { get; set; }

        public string ZoneString { get; set; }
    }
}
