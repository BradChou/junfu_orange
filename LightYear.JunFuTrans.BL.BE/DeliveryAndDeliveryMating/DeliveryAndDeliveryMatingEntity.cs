﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryAndDeliveryMating
{
    public class DeliveryAndDeliveryMatingEntity
    {
        public int Id { get; set; } //序號
        public string CheckNumber { get; set; } //貨號
        public int? Pieces { get; set; } //配送件數
        public string? ArriveOption { get; set; } //配達區分
        public DateTime? ScanDate { get; set; } //配達時間
        public string SubpoenaCategory { get; set; } // 傳票類別
        public string DriverNameCode { get; set; } //作業司機
        public string ReceiveAddress { get; set; } //收件人地址
        public int CollectionMoney { get; set; }    //代收貨款
        public string ExcelSum { get; set; }    //為了讓Excel可以顯示"合計"
        public int? ListSum{ get; set; }         //為了讓Excel可以顯示合計數字
    }
    public class DriversEntity
    {
        public string DriverCode { get; set; }
        public string DriverName { get; set; }
    }

    public class StationEntity
    {
        public string Scode { get; set; }
        public string Name { get; set; }
    }

    public class ArriveOptionEntity
    {
        public string CodeId { get; set; }
        public string CodeName { get; set; }
    }
}
