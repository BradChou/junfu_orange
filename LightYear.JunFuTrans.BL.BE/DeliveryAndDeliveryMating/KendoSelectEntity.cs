﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryAndDeliveryMating
{
    public class KendoSelectEntity
    {
        public KendoSelectEntity()
        {
            this.results = new List<DeliveryAndDeliveryMatingEntity>();
            this.CountPieces = 0; //給他預設值
            this.CountArriveOptions = 0; //給他預設值
        }
        public List<DeliveryAndDeliveryMatingEntity> results { get; set; }
        public int CountPieces { get; set; }
        public int CountArriveOptions { get; set; }
    }

}
