﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryException
{
    public class DeliveryExceptionLogEntity
    {
        public int Id { get; set; }                 //PK
        public string CheckNumber { get; set; }     //託運單號
        public string HandleResult { get; set; }           //處理結果
        public string HandleStation { get; set; }   //處理站所
        public string HandlerCode { get; set; }      //處理人員代碼
        public DateTime CDate{get; set;}              //新增日期
        public DateTime UDate { get; set; }         //更新日期
    }
}
