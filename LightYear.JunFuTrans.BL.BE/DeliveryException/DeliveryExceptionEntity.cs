﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryException
{
    public class DeliveryExceptionEntity
    {
        public int Id { get; set; }              //primary key
        public DateTime CDate{get; set;}              //新增日期
        public DateTime UDate { get; set; }         //更新日期
        public string ReportStation { get; set; }         //回報所
        public string CheckNumber { get; set; }    //託運單號
        public string ReportMatter { get; set; }   //回報事項
        public string SketchMatter { get; set; }   //簡述
        public string HandleResult { get; set; }           //處理結果
        public string HandlerCode { get; set; }      //處理人員代碼
        public bool IsClosed { get; set; }         //已結案
        public float? UseTime { get; set; }      //處理工時
        public string RelativeCheckNumber { get; set; }       //關聯託運單號
        public string HandleStation { get; set; }              //處理站
        public string ReportName { get; set; }   //回報人員
        public string CreateDate { get; set; }      //新增日(由於新增日期可能用於往後計算處理工時，所以新增"新增日"，保留"新增日期")
        public string IsClosedVX { get; set; }      //用於查詢頁面，將true/false改為V/X
        public string picLeft { get; set; }
        public string picRight { get; set; }
        public string picFront { get; set; }
        public string picBack { get; set; }
        public string picTop { get; set; }
        public string picBottom { get; set; }
        public string SendStaion { get; set; }
        public string DesStaion { get; set; }
        public string OtherMatters { get; set; }

    }
    public class StationEntity
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
    public class ExceptionReportAddScanLogEntity
    {
        public int Id { get; set; }              //primary key
        public DateTime? CDate { get; set; }              //新增日期、到著日期
        public DateTime UDate { get; set; }         //更新日期
        public string ReportStation { get; set; }         //回報所
        public string CheckNumber { get; set; }    //託運單號
        public string ReportMatter { get; set; }   //回報事項
        public string SketchMatter { get; set; }   //簡述
        public string HandleResult { get; set; }           //處理結果
        public string HandlerCode { get; set; }      //處理人員代碼
        public bool IsClosed { get; set; }         //已結案
        public float? UseTime { get; set; }      //處理工時
        public string RelativeCheckNumber { get; set; }       //關聯託運單號
        public string HandleStation { get; set; }              //處理站
        public string ReportName { get; set; }   //回報人員
        public string CreateDate { get; set; }      //新增日(由於新增日期可能用於往後計算處理工時，所以新增"新增日"，保留"新增日期")
        public string IsClosedVX { get; set; }      //用於查詢頁面，將true/false改為V/X
        public string ExceptionStatus { get; set; }
        public string FilePath { get; set; } // 放檔案連結或圖片連結
        public string FilePathText { get; set; } //顯示前端的連結文字
        public string DesStation { get; set; } // 到著站
        public string SendStation { get; set; } // 發送站
        public string OtherMatters { get; set; } //其他

    }
}

