﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DriverDispatch
{
    public class DriverDispatchEntity
    {
        public string DriverCode { get; set; }
        public string PickUpType { get; set; }
        public string DispatchDate { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerTel { get; set; }
        public string CustomerAddress { get; set; }
        public int ShouldPickUpCount { get; set; }
        public int ActualPickUpCount { get; set; }
    }

    public class CheckNumberListWithinDispathEntity
    {
        public string CheckNumber { get; set; }
        public string SendContact { get; set; }
        public string ShipCategory { get; set; }
        public bool IsShip { get; set; }
        public int Pieces { get; set; }
    }

    public class DispatchJsonEntity
    {
        public string CustomerCode { get; set; }
        public string SendAddress { get; set; }
        public string MD { get; set; }
    }

    public class Option
    {
        public string OptionId { get; set; }
        public string OptionName { get; set; }
    }
    public class JsonScanItem
    {
        public string Scan { get; set; }
    }
    public class JsonDriverCode
    {
        public string Driver { get; set; }
    }

    public class JsonUpdateMDSD
    {
        public string MD { get; set; }
        public string SD { get; set; }
        public string cCode { get; set; }
        public string cNumber { get; set; }
    }

    public class Result
    {
        public string Code { get; set; }
        public string Reason { get; set; }
        public object Data { get; set; }
    }

    public class Driver
    {
        public Driver()
        {
            this.DriverType = "";
            this.DriverTypeCode = "";
        }
        public string DriverType { get; set; }
        public string DriverTypeCode { get; set; }
    }
}
