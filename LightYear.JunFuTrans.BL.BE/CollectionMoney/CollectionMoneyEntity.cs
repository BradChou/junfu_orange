﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.CollectionMoney
{
    public class CollectionMoneyEntity
    {
        public CollectionMoneyEntity()
        {
            this.Id = 0;
        }

        public int Id { get; set; }

        public string BankNumber { get; set; }

        public CollectionMoneyDetailEntity[] Details { get; set; }
    }
}
