﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.CollectionMoney
{
    public class CollectionMoneyDetailEntity
    {
        public CollectionMoneyDetailEntity()
        {
            this.CollectionMoneyId = 0;
            this.Id = 0;
        }

        /// <summary>
        /// 代收母單號id
        /// </summary>
        public int CollectionMoneyId { get; set; }

        /// <summary>
        /// 代收子單號id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 托運編號(barcode)
        /// </summary>
        public string CheckNumber { get; set; }

        /// <summary>
        /// 應代收金額
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// 實際代收金額
        /// </summary>
        public decimal ActualAmount { get; set; }
    }
}
