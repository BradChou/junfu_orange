﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.LogToDB
{
   public class LogToDBEntity
    {
    }
    public class LogToDBWebSiteEntity
    {
        public int? Id { get; set; }
        public string Action { get; set; }
        public string Header { get; set; }
        public string Request { get; set; }
        public string Response { get; set; }
        public string IP { get; set; }
        public string UserAgent { get; set; }
        public string URL { get; set; }
        public DateTime? Cdate { get; set; }

    }
}
