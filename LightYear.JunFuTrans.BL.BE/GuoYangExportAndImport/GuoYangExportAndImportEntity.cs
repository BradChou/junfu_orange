﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.GuoYangExportAndImport
{
    public class GuoYangExportAndImportEntity
    {
        public string CheckNumber { get; set; }
        public int Pieces { get; set; }
        public string DriverCode { get; set; }
        public DateTime ScanDate { get; set; }
        public string ArriveOption { get; set; }
    }
    public class TempEntity
    {
        public string CheckNumber { get; set; }
        public string Pieces { get; set; }
        public string DriverCode { get; set; }
        public string ScanDate { get; set; }
        public string ArriveOption { get; set; }
    }

    public class FrontendEntity
    {
        public string CheckNumber { get; set; }
        public string Transhipment { get; set; }
        public string DriverName { get; set; }
        public DateTime? PrintDate { get; set; }
        public string StationName { get; set; }
        public int? Pieces { get; set; }
    }
}
