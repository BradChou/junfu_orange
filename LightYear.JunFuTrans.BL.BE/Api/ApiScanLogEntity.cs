﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Api
{
    public class ApiScanLogEntity
    {
        public ApiScanLogEntity()
        {
            this.returnCheckNumber = string.Empty;
        }

        public string checkNumber { get; set; }

        public DateTime scanDate { get; set; }

        public string deliveryType { get; set; }

        public string scanName { get; set; }

        public string supplierName { get; set; }

        public string status { get; set; }

        public string itemCodes { get; set; }

        public string driverCode { get; set; }

        public string driverName { get; set; }

        public string stationName { get; set; }

        public string stationCode { get; set; }

        public string returnCheckNumber { get; set; }
    }
}
