﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace LightYear.JunFuTrans.BL.BE.Api
{
    public class ApiCbmInfoEntity
    {
        public ApiCbmInfoEntity()
        {
            this.CheckNumber = string.Empty;
            this.Size = string.Empty;
            this.Length = 0;
            this.Weight = 0;
            this.Width = 0;
            this.Height = 0;
            this.SidesSum = 0;
            this.Cbm = 0;
            this.MeasurementTime = DateTime.MinValue;
        }

        [JsonProperty("checkNumber")]
        public string CheckNumber { get; set; }

        [JsonProperty("size")]
        public string Size { get; set; }

        [JsonProperty("length")]
        public double Length { get; set; }

        [JsonProperty("width")]
        public double Width { get; set; }

        [JsonProperty("height")]
        public double Height { get; set; }

        [JsonProperty("sidesSum")]
        public double SidesSum { get; set; }

        [JsonProperty("cbm")]
        public double Cbm { get; set; }

        [JsonProperty("weight")]
        public double Weight { get; set; }

        [JsonProperty("measurementTime")]
        public DateTime MeasurementTime { get; set; }
    }
}
