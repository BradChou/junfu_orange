﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace LightYear.JunFuTrans.BL.BE.Api
{
    public class ApiPickupLogEntity
    {
        public ApiPickupLogEntity()
        {
            //設立預設值
            this.ShouldPickupPieces = 1;
            this.ActualPickupPieces = 0;
            this.FalseItem = "電聯不上";
            this.DeliveryType = "D";
            this.ScanName = "集貨";
        }

        [JsonProperty("customerCode")]
        public string CustomerCode { get; set; }

        [JsonProperty("shouldPickupPieces")]
        public int ShouldPickupPieces { get; set; }

        [JsonProperty("actualPickupPieces")]
        public int ActualPickupPieces { get; set; }

        [JsonProperty("shipDate")]
        public DateTime ShipDate { get; set; }

        [JsonProperty("checkNumber")]
        public string CheckNumber { get; set; }

        [JsonProperty("actualPickup")]
        public Boolean ActualPickup { get; set; }

        [JsonProperty("falseItem")]
        public string FalseItem { get; set; }

        [JsonProperty("scanDate")]
        public DateTime? ScanDate { get; set; }

        [JsonProperty("deliveryType")]
        public string DeliveryType { get; set; }

        [JsonProperty("scanName")]
        public string ScanName { get; set; }

        [JsonProperty("supplierCode")]
        public string SupplierCode { get; set; }

        [JsonProperty("driverType")]
        public string DriverType { get; set; }

        [JsonProperty("driverTypeCode")]
        public string DriverTypeCode { get; set; }

        [JsonProperty("driverCode")]
        public string DriverCode { get; set; }
    }

    public class ApiPickupLogEntityForJsonRequest
    {
        [JsonProperty(PropertyName = "statusDetail")]
        public List<ApiScanLogEntity> StatusDetail { get; set; }
    }
}
