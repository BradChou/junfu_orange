﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Api
{
    public class DeliveryStatusConfigEntity
    {
        public DeliveryStatusConfigEntity()
        {
            this.StartDate = DateTime.MinValue;

            this.EndDate = DateTime.MinValue;
        }

        public string CustomerCode { get; set; }

        public string ApiUrl { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
