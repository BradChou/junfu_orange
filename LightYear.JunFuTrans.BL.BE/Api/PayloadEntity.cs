﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Api
{
    public class PayloadEntity
    {
        public ApiUserEntity info { get; set; }

        public int exp { get; set; }
    }
}
