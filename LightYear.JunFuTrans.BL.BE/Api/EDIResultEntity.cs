﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Api
{
    public class EDIResultEntity
    {
        public EDIResultEntity()
        {

            this.Area = string.Empty;
            this.AreaId = string.Empty;
            this.Result = false;
        }

        public bool Result { get; set; }

        public string Msg { get; set; }

        public string CheckNumber { get; set; }

        public string OrderNumber { get; set; }

        public string Sd { get; set; }

        public string Md { get; set; }

        public string Putorder { get; set; }

        public string Area { get; set; }

        public string AreaId { get; set; }
    }
}
