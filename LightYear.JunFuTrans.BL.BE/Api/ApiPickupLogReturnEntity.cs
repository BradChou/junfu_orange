﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace LightYear.JunFuTrans.BL.BE.Api
{
    public class ApiPickupLogReturnEntity
    {
        public ApiPickupLogReturnEntity()
        {
            this.PickupInfo = new List<ApiPickupLogEntity>();
        }

        [JsonProperty("pickupInfo")]
        public List<ApiPickupLogEntity> PickupInfo { get; set; }
    }
}
