﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace LightYear.JunFuTrans.BL.BE.Api
{
    public class ApiCbmInfoReturnEntity
    {
        public ApiCbmInfoReturnEntity()
        {
            this.CbmInfo = new List<ApiCbmInfoEntity>();
        }

        [JsonProperty("cbmInfo")]
        public List<ApiCbmInfoEntity> CbmInfo { get; set; }
    }
}
