﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.ComponentModel;

namespace LightYear.JunFuTrans.BL.BE.Enumeration
{
    public enum ExceptionOption
    {
        破損 = 1,  // ttDeliveryScanLog.ExceptionOption and exception_report.report_matter
        短少 = 2,  // ttDeliveryScanLog.ExceptionOption
        多出 = 3,  // ttDeliveryScanLog.ExceptionOption
        喪失 = 4,  // ttDeliveryScanLog.ExceptionOption
        誤訂 = 5,  // ttDeliveryScanLog.ExceptionOption
        籠車修繕 = 12,           //exception_report.report_matter
        協尋通報 = 13,           //exception_report.report_matter
        喪失通報 = 14,           //exception_report.report_matter
        不明貨件 = 15,           //exception_report.report_matter
        超重超大 = 16,           //exception_report.report_matter
        單邊長大於60公分 = 17,           //exception_report.report_matter
        扁平式家俱 = 18,         //exception_report.report_matter
        S120發送佔比大於百分20 = 19,           //exception_report.report_matter
        S150以上貨件 = 20,  //exception_report.report_matter
        多打1 = 21,  //exception_report.report_matter
        液體, 酒精, 易燃, 危險品 = 22,  //exception_report.report_matter
        包裝異常 = 23,  //exception_report.report_matter
        破壞袋使用不當者 = 24,  //exception_report.report_matter
        假日配輸入異常 = 25,  //exception_report.report_matter
        超重大於20公斤 = 26,  //exception_report.report_matter
        籠車貨件堆疊異常 = 27,  //exception_report.report_matter
        籠車貨件誤卸流向混籠 = 28, //exception_report.report_matter
    }

    public enum SketchMatter
    {
        外箱淋濕 = 1,  //exception_report.sketch_matter
        外箱凹損 = 2,  //exception_report.sketch_matter
        內容物破損液體滲漏 = 3,  //exception_report.sketch_matter
        其他 = 4, //exception_report.sketch_matter
    }
}
