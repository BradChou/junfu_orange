﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.ComponentModel;

namespace LightYear.JunFuTrans.BL.BE.Enumeration
{
    public enum AccountType
    {
        [Description("管理人員")]
        Administrator = 0,
        [Description("司機、配送員")]
        Driver = 1,
        [Description("客戶")]
        Customer = 2,
    }
}
