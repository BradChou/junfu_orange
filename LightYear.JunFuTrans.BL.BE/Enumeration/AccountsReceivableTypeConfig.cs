﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace LightYear.JunFuTrans.BL.BE.Enumeration
{
    public enum AccountsReceivableTypeConfig
    {
        [Description("月結")]
        Monthly = 0,
        [Description("周結")]
        Weekly = 1,
        [Description("雙月結")]
        BiMonthly = 2,
        [Description("雙周結")]
        BiWeekly = 3,
        [Description("特別結")]
        Specially = 9
    }
}
