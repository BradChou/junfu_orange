﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Enumeration
{
    public enum PickUpOrNotTypeConfig
    {
            Total = 0,
            NotPickUpYet = 1,
            AlreadyPickUp = 2
    }
}
