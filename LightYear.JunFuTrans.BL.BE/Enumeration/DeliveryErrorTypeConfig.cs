﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Enumeration
{
    public enum DeliveryErrorTypeConfig
    {
        Total = 0,
        NotDeliveryMating = 1,
        DeliveryMatingError = 2
    }
}
