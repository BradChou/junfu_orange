﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Enumeration
{
    public enum ERPSearchType
    {
        OnlyUploadDate = 11,
        OnlyRegisterDate = 12,
        BothDate = 13,
        UserDepend = 21,
        DeptAllUser = 22,
        AllDept = 23,
        HasDownload = 31,
        HasNotDownload = 32,
        IgnoreDownload = 33,
        FeeTypeDepend = 41,
        AllFeeType = 42,
        HasCheckAccounted = 51,
        HasNotCheckAccounted = 52,
        IgnoreCheckAccount = 53,
        AllVendor = 61,
        VendorDepend = 62,
        AllPayable = 71,
        PayableDepend = 72,
    }
}
