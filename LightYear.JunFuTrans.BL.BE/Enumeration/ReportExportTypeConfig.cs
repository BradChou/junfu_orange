﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Enumeration
{
    public enum ReportExportType
    {
        PDF = 0,
        EXCELOPENXML = 1,
        EXCEL = 2,
        XML = 3,
        IMAGE = 4,
        HTML5 = 5,
        WORDOPENXML = 6,
        WORD = 7,
        CSV = 8
    }
}
