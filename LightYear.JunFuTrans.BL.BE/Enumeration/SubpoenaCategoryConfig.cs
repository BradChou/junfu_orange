﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Enumeration
{
    public enum SubpoenaCategoryConfig
    {
        元付 = 11,
        到付 = 21,
        預購袋 = 31,
        代收貨款 = 41,
        元付之到付追加 = 25,
        應收帳款 = 51
    }
}
