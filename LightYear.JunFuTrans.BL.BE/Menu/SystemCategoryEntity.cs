﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Menu
{
    public class SystemCategoryEntity
    {
        public SystemCategoryEntity()
        {
            this.Id = 0;
            this.IsMenu = true;
            this.DisplaySort = 0;
            this.SystemSubCategoryEntities = new List<SystemSubCategoryEntity>();
        }

        public int Id { get; set; }

        public string CategoryName { get; set; }

        public string CategoryEnName { get; set; }

        public bool IsMenu { get; set; }

        public int DisplaySort { get; set; }

        public List<SystemSubCategoryEntity> SystemSubCategoryEntities { get; set; }
    }
}
