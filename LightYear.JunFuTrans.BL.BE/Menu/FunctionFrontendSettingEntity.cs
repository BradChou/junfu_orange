﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Menu
{
    public class FunctionFrontendSettingEntity
    {
        public FunctionFrontendSettingEntity()
        {
            this.id = 0;
            this.spriteCssClass = "html";
            this.@checked = false;
            this.expanded = false;
            this.enabled = true;
        }

        public int id { get; set; }

        public string text { get; set; }

        public string spriteCssClass { get; set; }

        public bool @checked { get; set; }

        public bool expanded { get; set; }

        public bool enabled { get; set; }

        public List<FunctionFrontendSettingEntity> items { get; set; }
    }
}
