﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Menu
{
    public class SystemSubCategoryEntity
    {
        public SystemSubCategoryEntity()
        {
            this.Id = 0;
            this.CatetoryId = 0;
            this.SystemFunctionEntities = new List<SystemFunctionEntity>();
            this.DisplaySort = 0;
            this.IsMenu = true;
        }

        public int Id { get; set; }

        public int CatetoryId { get; set; }

        public string SubCategoryName { get; set; }

        public string SubCategoryEnName { get; set; }

        public bool IsMenu { get; set; }

        public int DisplaySort { get; set; }

        public List<SystemFunctionEntity> SystemFunctionEntities { get; set; }
    }
}
