﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.PickUpAndDeliveryMating
{
    public class PickUpAndDeliveryMatingEntity
    {
        public int Id { get; set; } //序號
        public string CheckNumber { get; set; } //貨號
        public string OrderNumber { get; set; } //貨號
        public string JobTitle { get; set; } //職稱
        public string  DriverCode { get; set; }
        public string DriverFullName { get; set; } //工號
        public DateTime? ScanDate { get; set; } //掃讀時間
        public string ScanItem { get; set; } //作業狀態
        public int CountPickUp { get; set; } //已集貨數量
        public int CountDeliveryMating { get; set; } //已配達數量
        public string  StationName { get; set; } //站所
        public int Pieces { get; set; }  // 件數
        public string CustomerCode { get; set; }  // 客代
        public string CustomerName { get; set; }  // 客戶名
    }

    public class PickUpAndDeliveryMatingFirstEntity
    {
        public string DriverCode { get; set; }
        public string DriverName { get; set; } 
        public int CountPickUp { get; set; } //已集貨數量
        public int CountDeliveryMating { get; set; } //已配達數量
        public string StationScode { get; set; } 
        public string StationName { get; set; } //站所
    }

}
