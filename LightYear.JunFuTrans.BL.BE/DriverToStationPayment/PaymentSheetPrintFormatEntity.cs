﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DriverToStationPayment
{
    public class PaymentSheetPrintFormatEntity
    {
        public PaymentSheetPrintFormatEntity()
        {
            CreateDate = DateTime.Now;
            Details = new DriverPaymentDetailEntity[0];
            CategoryCount = new PrintFormatCategoryEntity[0];
            CountSum = 0;
            MoneySum = 0;
            RemittanceSum = 0;
            PaidHistories = new PaidHistoryEntity[0];
        }

        public PaymentSheetPrintFormatEntity ShallowCopy()
        {
            return (PaymentSheetPrintFormatEntity) MemberwiseClone();
        }


        public string DriverCode { get; set; }

        public string DriverName { get; set; }

        public string StationName { get; set; }

        public string StationScode { get; set; }

        public DateTime CreateDate { get; set; }

        // 所長聯,司機聯
        public string ForWho { get; set; }

        public DriverPaymentDetailEntity[] Details { get; set; }

        public PrintFormatCategoryEntity[] CategoryCount { get; set; }

        public int CountSum { get; set; }

        public int MoneySum { get; set; }

        public int RemittanceSum { get; set; }

        public PaidHistoryEntity[] PaidHistories { get; set; }

        public int TotalShouldPay { get; set; }

        public int PreOrderBagTotalShouldPay { get; set; }
    }
}
