﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DriverToStationPayment
{
    public class CashFlowDailyEntity
    {
        public Int64 DriverPaymentDetailId { get; set; }

        public int DriverPaymentSheetId { get; set; }

        public string ArriveStationName { get; set; }

        public string DriverName { get; set; }

        public Double RemittanceAmount { get; set; }

        public string TransactionLastFive { get; set; }

        public string CheckNumber { get; set; }

        public string SendStationName { get; set; }

        public string SenderName { get; set; }

        public string ReceiverName { get; set; }

        public Double CollectionMoney { get; set; }

        public string ReceiverType { get; set; }
    }
}
