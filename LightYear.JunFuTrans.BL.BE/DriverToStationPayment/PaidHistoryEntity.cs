﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DriverToStationPayment
{
    public class PaidHistoryEntity
    {
       public string TranscationLastFive { get; set; }

        public int? RemittanceAmount { get; set; }

        public int? CheckAmount { get; set; }

        public int? CoinsAmount { get; set; }
    }
}
