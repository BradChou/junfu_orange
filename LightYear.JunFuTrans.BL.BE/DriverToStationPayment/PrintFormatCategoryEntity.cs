﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DriverToStationPayment
{
    public class PrintFormatCategoryEntity
    {
        public PrintFormatCategoryEntity()
        {
            CashCount = 0;
            RemittanceCount = 0;
            CheckCount = 0;
            CashMoney = 0;
            RemittanceMoney = 0;
            CheckMoney = 0;
        }

        public string Category { get; set; }

        public int CashCount { get; set; }

        public int RemittanceCount { get; set; }

        public int CheckCount { get; set; }

        public int CashMoney { get; set; }

        public int RemittanceMoney { get; set; }

        public int CheckMoney { get; set; }
    }
}
