﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DriverToStationPayment
{
    public class SearchFilterEntity
    {
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string StationScode { get; set; }

    }
}
