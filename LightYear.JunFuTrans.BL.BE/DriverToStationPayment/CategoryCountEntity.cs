﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DriverToStationPayment
{
    public class CategoryCountEntity
    {
        public int YuanFuCount { get; set; }
        public int YuanFuMoney { get; set; }  // 通常元付不會有錢，只是避免顧客打單時選錯類別

        // 代收貨款
        public int CashOnDeliveryCount { get; set; }
        public int CashOnDeliveryMoney { get; set; }

        // 應收帳款
        public int DestinationCashCount { get; set; }
        public int DestinationRemittanceCount { get; set; }
        public int DestinationCheckCount { get; set; }
        public int DestinationCashMoney { get; set; }
        public int DestinationRemittanceMoney { get; set; }
        public int DestinationCheckMoney { get; set; }

        // 預購袋
        public int PreOrderBagCashCount { get; set; }
        public int PreOrderBagRemittanceCount { get; set; }
        public int PreOrderBagCheckCount { get; set; }
        public int PreOrderBagCashMoney { get; set; }
        public int PreOrderBagRemittanceMoney { get; set; }
        public int PreOrderBagCheckMoney { get; set; }

        public int TotalShouldPay { get; set; }
    }
}
