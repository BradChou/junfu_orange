﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DriverToStationPayment
{
    public class StationCollectMoneyEntity
    {
        public int Id { get; set; }

        public string DriverCode { get; set; }

        public string DriverName { get; set; }

        public DateTime Date { get; set; }

        // 從精算書抓司機付錢紀錄
        public int RemittanceAmount { get; set; }

        public int CheckAmount { get; set; }

        public int CoinsAmount { get; set; }

        public string TransactionLastFive { get; set; }

        public bool HasCreatedSheet { get; set; }


        // 代收貨款
        public int CashOnDelivery { get; set; }

        // 應收帳款
        public int DestinationOnDelivery { get; set; }

        // 預購袋
        public int PreOrderBag { get; set; }

        public int CheckOnDelivery { get; set; }

        // 現金應繳 + 支票應繳
        public int TotalShouldPay { get; set; }

        // 匯款入金 + 零錢入金 + 支票
        public int PaidTotal { get; set; }        

        public int NowPayAmount { get; set; }

        public int CurrentPaymentBalace { get; set; }

        public int PreviousPaymentBalance { get; set; }

        public int AccumulatedPaymentBalance { get; set; }
    }
}
