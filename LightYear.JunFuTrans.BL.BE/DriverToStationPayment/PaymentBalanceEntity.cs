﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DriverToStationPayment
{
    public class PaymentBalanceEntity
    {
        public int CurrentPaymentBalance { get; set; }

        public int AccumulatedPaymentBalance { get; set; }
    }
}
