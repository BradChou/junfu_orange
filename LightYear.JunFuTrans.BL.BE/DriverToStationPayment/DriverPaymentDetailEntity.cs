﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DriverToStationPayment
{
    public class DriverPaymentDetailEntity
    {
        public int Id { get; set; }

        public string CheckNumber { get; set; }

        public string AreaArriveCode { get; set; }

        public string ArriveStation { get; set; }

        public string SubpoenaCategory { get; set; }

        public string SubpoenaCategoryShowName { get; set; }

        public int CategoryOrder { get; set; }

        public int CollectionMoney { get; set; }

        public string PaidMethod { get; set; }

        public string TicketOrRemittanceLastFive { get; set; }

        public int DriverPaymentId { get; set; }
    }
}
