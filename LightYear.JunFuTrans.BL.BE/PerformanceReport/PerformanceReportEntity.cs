﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.PerformanceReport
{
    public class PerformanceReportEntity
    {
        public int? Index { get; set; }                  //前端用
        public string AccountCode { get; set; }       //客戶代號
        public string UserName { get; set; }        //客戶名稱
        public List<int> PiecesPerDayList { get; set; }      //每日發送
        public int Sum { get; set; }                //合計
        public int AveragePerDay { get; set; }      //日平均
        public List<List<PerformanceReportDetailEntity>> DetailPerDayList { get; set; }     //記錄每天資料細節
    }

    public class PerformanceReportRepositoryEntity
    {
        public string AccountCode { get; set; }       //客戶代號(是資料表Customer中的CustomerCode)
        public string UserName { get; set; }        //客戶名稱
        public DateTime? ShipDate { get; set; }      //發送日
        public DateTime? PrintDate { get; set; }     //用ShipDate去尾數
        public PerformanceReportDetailEntity DetailEntity { get; set; }
    }

    public class FrontendInputEntity
    { 
        public string SendStation { get; set; }     //發送站(格式:"F20")
        public string AccountCode { get; set; }     //客戶代號
        public DateTime Start { get; set; }         //起始日
        public DateTime End { get; set; }           //結束日
    }
    public class AccountEntity
    { 
        public string AccountCode { get; set; }     //客戶代號
        public string AccountName { get; set; }     //客戶名稱
        public string AccountLoginStation { get; set; }     //帳戶登入資訊的站所
        public string AccountStation { get; set; }  //客戶所屬站所
        public bool IsMaster { get; set; }          //是否為峻富總公司
    }
    public class PerformanceReportDetailEntity      //紀錄Detail
    {
        public string CustomerCode { get; set; }    //
        public string CheckNumber { get; set; }
        public string DriverCode { get; set; }
        public string DriverName { get; set; }
        public string ReceiveContact { get; set; } //收件人
        public string TEL { get; set; }         //連絡電話
        public string City { get; set; }        //縣市
        public string Region { get; set; }      //鄉鎮市區
        public string Address { get; set; }     //地址
        public string GiveDate {get;set;}       //交寄日期
        public DateTime? ShipDate { get; set; }
    }
}
