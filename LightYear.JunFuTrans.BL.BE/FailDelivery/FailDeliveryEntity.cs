﻿using LightYear.JunFuTrans.BL.BE.DeliveryRequest;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.FailDelivery
{
    public class FailDeliveryEntity
    {
        public int? Index { get; set; }      //編號
        public DateTime? ShipDate { get; set; }  //發送日
        public string ShipDateString { get; set; }      //發送日留年月日
        public string SupplierCode { get; set; }      //發送站代碼
        public string SupplierStation { get; set; } //發送站
        public string ArriveCode { get; set; }    //到著站代碼
        public string ArriveStation { get; set; }//到著站
        public string CheckNumber { get; set; }     //貨號
        public string CustomerCode { get; set; }    //出貨人代碼
        public string CustomerShortName { get; set; }    //出貨人
        public int? SupplierPieces { get; set; }      //發送件數
        public int? ArrivePieces { get; set; }       //到著件數
        public string ReceiveContact { get; set; } //收件人
        public int AbnormalRemarks { get; set; }//異常說明(ex:貨件多出) 用編號方式，因為存入此屬性的來源資料表非常不固定，此屬性為代碼，代碼對照寫在Service
        public string AbnormalRemarksText { get; set; }//異常說明中文
        public string Remarks { get; set; }         //說明，目前用不到
        public string AccountStation { get; set; }     //登入帳戶的站所，用於篩選權限可看資料
        public string ExceptionOption { get; set; }     //異常狀態
    }

    public class FailDeliveryInputEntity
    {
        public string SupplierCode { get; set; }     //發送站代碼
        public string SupplierStation { get; set; }     //發送站
        public DateTime Start { get; set; }             //起始日，預設為當日
        public DateTime? StartInput { get; set; }       //輸入的起始日，可為空值
        public DateTime End { get; set; }               //結束日
        public DateTime? EndInput { get; set; }         //輸入的結束日，可為空值
        public string CustomerCode { get; set; }        //客戶代號
        public int AbnormalRemarks { get; set; }        //銷號狀況(ex:貨件多出)，目前用不到
        public string AbnormalRemarksText { get; set; } //上列的中文
        public bool IsMaster { get; set; }              //登入帳戶是否為總公司用於判定"全站所"選取對象
        public string AccountStation { get; set; }      //
    }
    public class StationEntity
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Scode { get; set; }
    }
    public class CustomerShortNameEntity
    {
        public string CustomerCode { get; set; }
        public string CustomerShortName { get; set; }
    }
    public class CustomerEntity
    {
        public string CustomerCode { get; set; }     //客戶代號
        public string CustomerName { get; set; }     //客戶名稱
        public string CustomerStation { get; set; }  //客戶所屬站所
        public bool IsMaster { get; set; }          //是否為峻富總公司
    }
    public class CustomerDownListEntity
    { 
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
    }
    public class KendoInputEntity
    {
        public string StartInput { get; set; }
        public string EndInput { get; set; }
        public string SupplierStation { get; set; }
        public string CustomerCode { get; set; }
        public string AbnormalreMarkstext { get; set; }
        public bool IsMaster { get; set; }
        public string AccountStation { get; set; }
}
}
