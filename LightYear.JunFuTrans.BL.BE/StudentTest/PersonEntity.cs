﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using LightYear.JunFuTrans.Utilities.DynamicData.Attributes;

namespace LightYear.JunFuTrans.BL.BE.StudentTest
{
    public class PersonEntity
    {
        public int? Id { get; set; }

        /// <summary>
        /// 學生姓名
        /// </summary>
        [DisplayNameCustom("學生姓名")]
        [UIType(UITypeEnum.Input, IsRequired = true, Editable = true)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Name { get; set; }

        /// <summary>
        /// 學生年齡
        /// </summary>
        [DisplayNameCustom("學生年齡")]
        [UIType(UITypeEnum.Input, IsRequired = true, Editable = true)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public int Age { get; set; }

        /// <summary>
        /// 學生姓別
        /// </summary>
        [DisplayNameCustom("學生性別")]
        [UIType(UITypeEnum.Input, IsRequired = true, Editable = true)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public bool Sex { get; set; }

    }
}
