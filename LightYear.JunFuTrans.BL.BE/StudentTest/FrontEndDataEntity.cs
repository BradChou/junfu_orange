﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.StudentTest
{
    public class FrontEndDataEntity
    {
        public FrontEndDataEntity()
        {
            this.Id = 0;
        }

        public int Id { get; set; }

        public string Field1 { get; set; }

        public string Field2 { get; set; }

        public string Field3 { get; set; }
    }
}
