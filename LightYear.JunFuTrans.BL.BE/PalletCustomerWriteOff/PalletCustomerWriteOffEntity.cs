﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.PalletCustomerWriteOff
{
    public class PalletCustomerWriteOffEntity
    {
        public int Id { get; set; }
        public DateTime? PrintDate { get; set; }
        public DateTime? SupplierDate { get; set; }
        public string DeliveryType { get; set; }
        public string CheckNumber { get; set; }
        public string SupplierArea { get; set; }
        public string SendCity { get; set; }
        public string ReceiveCity { get; set; }
        public string ArriveArea { get; set; }
        public int? Pieces { get; set; }
        public int? Plates { get; set; }
        public decimal? Payment { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerShortName { get; set; }
        public string CustomerName { get; set; }
        public int? AddFee { get; set; }
        public int? SupplierFee { get; set; }
        public int? CSectionFee { get; set; }
        public Payment PaymentEntity { get; set; }
        public FindCustomerEntity CustomersData { get; set; }

    }

    public class ShowInFrontend
    {
        public int Id { get; set; }
        public int? Plates { get; set; }
        public decimal? Payment { get; set; }
        public string CustomerCode { get; set; }   // mastercode + secondid
        public string CustomerName { get; set; }
        public DateTime? StartDate { get; set; }

    }
    public class Detail
    {
        public int Id { get; set; }
        public DateTime? PrintDate { get; set; }
        public DateTime? SupplierDate { get; set; }
        public string PricingCode { get; set; }
        public string CheckNumber { get; set; }
        public string SendCity { get; set; }
        public string ReceiveCity { get; set; }
        public string OrderNumber { get; set; }
        public string ReceiveContact { get; set; }
        public string ReceiveAddress { get; set; }
        public string SendContact { get; set; }
        public string SendAddress { get; set; }
        public string LatestArriveOptionDriver { get; set; }
        public string InvoiceDesc { get; set; }
        public int? Pieces { get; set; }
        public int? Plates { get; set; }
        public decimal? Payment { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerShortname { get; set; }
        public int? SupplierFee { get; set; }
        public int? AddFee { get; set; }
        public Payment PaymentEntity { get; set; }
    }
   
    public class PalletCustomerDropDownList
    {
        public string SupplierCode { get; set; }
        public string SupplierCodeAndName { get; set; }
        public int? CheckOutDate { get; set; }
    }
    public class Payment
    {
        public decimal? PalletIncome { get; set; }  // 棧板收入
        public decimal? TruckIncome { get; set; }   // 專車收入
        public decimal? Tax { get; set; }           // 營業稅
        public decimal? Total { get; set; }         // 應收帳款
        public int? TotalPalletCount { get; set; }   // 總板數
        public int? TotalPiecesCount { get; set; }   // 總件數
        public int? TruckPiecesCount { get; set; }   // 專車件數
        public int? TruckPalletCount { get; set; }   // 專車板數
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
    }

    public class FindCustomerEntity
    {
        public string CustomerShortname { get; set; }
        public string CustomerName { get; set; }
        public string UniformNumber { get; set; }
        public string Telephone { get; set; }
        public string ShipmentsAddress { get; set; }
        public string ShipmentsEmail { get; set; }
        public string BillingSpecialNeeds { get; set; }
        public string ShipmentsPrincipal { get; set; }
        public string PricingCode { get; set; }
    }

    public class InputJson
    {
        public string ShortCustomerCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
