﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.StationArea
{
    public class StationAreaEntity
    {
        public int? Id { get; set; }

        public string Area { get; set; }

        public string StationCode { get; set; }

        public string StationSCode { get; set; }

        public string StationName { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        public string UpdateUser { get; set; }

        public string ShowName { get; set; }
    }
}
