﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.StationArea
{
    public class KendoSelectStationEntity
    {
        public KendoSelectStationEntity()
        {
            this.results = new List<StationAreaEntity>();
        }

        public List<StationAreaEntity> results { get; set; }

        public int __count { get; set; }
    }
}
