﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.StationArea
{
    public class KendoSelectAreaEntity
    {
        public KendoSelectAreaEntity()
        {
            results = new List<string>();
        }

        public List<string> results { get; set; }

        public int __count { get; set; }
    }
}
