﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.StationArea
{
    public class OrgAreaMutiEditEntity
    {
        public string OrgAreaIds { get; set; }

        public string EditValue { get; set; }
    }
}
