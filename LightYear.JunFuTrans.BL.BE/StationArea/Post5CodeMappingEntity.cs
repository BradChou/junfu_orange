﻿using System;
using System.Collections.Generic;
using System.Security.Policy;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.StationArea
{
    public class Post5CodeMappingEntity
    {

        public int Id { get; set; }

        public string Office { get; set; }

        public string Zip3A { get; set; }

        public string ZipCode { get; set; }

        public string City { get; set; }

        public string Area { get; set; }

        public string Road { get; set; }

        public string Scoop { get; set; }

        public string StationScode { get; set; }

        public string StationName { get; set; }

        public string MDNo { get; set; }

        public string SDNo { get; set; }

        public string PutOrder { get; set; }
    }
}
