﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.PayCustomerPayment
{
    public class TempEntity
    {
        public string CheckNumber { get; set; }
        public bool IsPaid { get; set; }
    }

    public class FrontendEntity
    {
        public string CheckNumber { get; set; }
        public string SendStation { get; set; }
        public string AreaArriveCode { get; set; }
        public string IsPaid { get; set; }
        public string Customer { get; set; }
        public int Payment { get; set; }
    }
}
