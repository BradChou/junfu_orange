﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Account
{
    public class RoleFunctionMappingEntity
    {

        public RoleFunctionMappingEntity()
        {
            this.Id = 0;
            this.FunctionId = 0;
            this.RoleId = 0;
            this.FunctionValue = 0;
        }

        public int Id { get; set; }

        public int RoleId { get; set; }

        public int FunctionId { get; set; }

        public int FunctionValue { get; set; }
    }
}
