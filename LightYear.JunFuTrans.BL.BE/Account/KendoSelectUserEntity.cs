﻿using System;
using System.Collections.Generic;
using System.Security.Policy;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Account
{
    public class KendoSelectUserEntity
    {
        public KendoSelectUserEntity()
        {
            this.results = new List<UserInfoEntity>();
        }

        public List<UserInfoEntity> results { get; set; }

        public int __count { get; set; }

    }
}
