﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.Enumeration;

namespace LightYear.JunFuTrans.BL.BE.Account
{
    public class UserInfoEntity
    {
        public UserInfoEntity()
        {
            this.AccountType = AccountType.Customer;
            this.RoleName = new List<string>();
            this.RoleId = new List<int>();
            this.StationShowName = string.Empty;
            Company = string.Empty;
        }

        public string Station { get; set; }

        public string UserAccount { get; set; }

        public string UserName { get; set; }

        public AccountType AccountType { get; set; }

        public List<string> RoleName { get; set; }

        public int UsersId { get; set; }

        public List<int> RoleId { get; set; }

        public string ShowName { get; set; }

        public DateTime CookieExpiration { get; set; }

        public string ActionStation { get; set; }

        public string StationShowName { get; set; }

        public string Company { get; set; }

        public int? SupplierId { get; set; }
    }
}
