﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Account
{
    public class SkyEyesTokenInfoEntity
    {
        public SimpleAccountCodeEntity info { get; set; }

        public Int64 exp { get; set; }
    }
}
