﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.Enumeration;

namespace LightYear.JunFuTrans.BL.BE.Account
{
    public class UserInfoWithAddressEntity
    {
        public UserInfoWithAddressEntity()
        {
            this.AccountType = AccountType.Customer;
            this.RoleName = new List<string>();
            this.RoleId = new List<int>();
            this.StationShowName = string.Empty;
        }

        public string Station { get; set; }

        public string UserAccount { get; set; }

        public string UserName { get; set; }

        public AccountType AccountType { get; set; }

        public List<string> RoleName { get; set; }

        public int UsersId { get; set; }

        public List<int> RoleId { get; set; }

        public string ShowName { get; set; }

        public DateTime CookieExpiration { get; set; }

        public string ActionStation { get; set; }

        public string StationShowName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Company { get; set; }

        public string County { get; set; }

        public string City { get; set; }

        public string Address { get; set; }

        public string ZipPostalCode { get; set; }

        public string PhoneNumber { get; set; }
        public string TaxIdNumber { get; set; }

    }
}
