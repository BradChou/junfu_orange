﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Account
{
    public class UserFunctionMappingEntity
    {
        public UserFunctionMappingEntity()
        {
            this.Id = 0;
            this.UserId = 0;
            this.UserType = 0;
            this.FunctionValue = 0;
            this.FunctionId = 0;
        }

        public int Id { get; set; }

        public int UserId { get; set; }

        public int UserType { get; set; }

        public int FunctionId { get; set; }

        public int FunctionValue { get; set; }
    }
}
