﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Account
{
    public class RoleInfoEntity
    {
        public RoleInfoEntity()
        {
            this.RoleId = 0;
        }

        public int RoleId { get; set; }

        public string Name { get; set; }

        public string EnName { get; set; }

        public string Company { get; set; }
    }
}
