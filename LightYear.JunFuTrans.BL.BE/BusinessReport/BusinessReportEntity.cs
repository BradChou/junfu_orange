﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.BusinessReport
{
    public class BusinessReportEntity
    {
        public string CheckNumber { get; set; }     //託運單號
        public string SupplierCode { get; set; }       //站所代碼
        public string SupplierStation { get; set; }
        public int Pieces { get; set; }             //件數
        public DateTime ShipDate { get; set; }      //出貨日
        public string AreaArriveCode { get; set; }         //站所簡碼
        public string AreaArriveStation { get; set; }         //站所
        public string Area { get; set; }             //站所區屬 例如:北一區
        public string ScanItem { get; set; }        //掃讀項目
        public string CustomerCode { get; set; }        //區分momom用
    }
    public class AreaStationEntity
    {
        public string Area { get; set; }            //區屬
        public string StationCode { get; set; }     //站所代碼
        public string StationName { get; set; }     //站所名稱
    }
    public class BusinessReportRepositoryEntity
    {
        public string CheckNumber { get; set; }     //託運單號
        public string AreaArriveCode { get; set; }
        public string SupplierCode { get; set; }
        public DateTime? ShipDate { get; set; }
        public DateTime? PrintDate { get; set; }
        public int? Pieces { get; set; }
        public string ScanItem { get; set; }
        public string CustomerCode { get; set; }        //用於分辨是否為momo
    }
    public class StartAndEndEntity
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public bool DetailOrNot { get; set; }           //是否顯示站所細節
        public string AreaArriveCode {get;set;}    //到著站所
    }
    public class StationEntity
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int Owner { get; set; }
    }
}
