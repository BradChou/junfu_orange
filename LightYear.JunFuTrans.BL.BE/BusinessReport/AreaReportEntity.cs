﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.BusinessReport
{
    public class AreaReportEntity
    {
        public string IndexOrStationCode { get; set; }     //可能是序號(在區屬劃分時)，也可能是所代號(在站所劃分時)
        public string Area { get; set; }                //發送區屬
        public List<int> SumList { get; set; }     //彈性天數的總件數
        public List<int> WeekList { get; set; }     //每週合計
        public int Total { get; set; }        //全區間合計
        public int Average { get; set; }      //日平均
    }

    public class AreaWeekReportEntity
    { 
        public List<int> WeekSumList { get; set; }  //一週件數總計
    }
}
