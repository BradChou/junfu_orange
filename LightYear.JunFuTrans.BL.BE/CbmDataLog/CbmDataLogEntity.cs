﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.BL.BE.CbmDataLog
{
    public class CbmDataLogInputEntity
    {
        public DateTime CbmScanStartDate { get; set; }
        public DateTime CbmScanEndDate { get; set; }
    }

    public class CbmUpdateDataEntity
    { 
        public List<DA.JunFuDb.CbmDataLog> Models { get; set; }
    }
}
