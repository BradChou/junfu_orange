﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRequest;
using Newtonsoft.Json;
using LightYear.JunFuTrans.Mappers.StationWithArea;
using LightYear.JunFuTrans.Mappers.DeliveryRequest;
using LightYear.JunFuTrans.DA.Repositories.Station;
using LightYear.JunFuTrans.BL.BE.SignPaperPhoto;
using System.Net;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using System.IO.Compression;
using Microsoft.EntityFrameworkCore;

namespace AWSSignPaperPackage
{
    public class Function
    {
        private static IConfiguration Configs()
        {
            var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            return config;
        }
        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        static void Main(string[] args)
        {
            var configs = Configs();

            string originalPath = configs["OriginalPath"];
            if (!Directory.Exists(originalPath))
            {
                Directory.CreateDirectory(originalPath);
            }
            DirectoryInfo dirs = new DirectoryInfo(originalPath);

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<DeliveryRequestMappingProfile>();
                cfg.AddProfile<DeliveryScanLogMappingProfile>();
                cfg.AddProfile<StationAreaMappingProfile>();
            });

            string updatedPath = configs["UpdatedPath"];//@"../../../../SignPaperPackageJsonUpdated";
            if (!Directory.Exists(updatedPath))
            {
                Directory.CreateDirectory(updatedPath);
            }
            DirectoryInfo dirsUpdated = new DirectoryInfo(updatedPath);

            FileInfo[] files = dirs.GetFiles();
            for (var i = 0; i < files.Length; i++)
            {
                try
                {
                    FunctionHandler(File.ReadAllText(files[i].FullName));
                    var s = files[i].FullName.Replace(originalPath.Replace("/", "\\"), updatedPath.Replace("/", "\\"));
                    File.Move(files[i].FullName, files[i].FullName.Replace(originalPath.Replace("/","\\"), updatedPath.Replace("/", "\\")));
                }
                catch (Exception e)
                { 
                
                }
            }
        }

        public static string FunctionHandler(string input)
        {
            //使用橘子的Service範例

            string _80Url = @"http://220.128.210.180:10080/JUNFU_APP_WebService/PHOTO/";
            string S3Url = @"https://storage-for-station.s3.us-east-2.amazonaws.com/";
            string S3IPUrl = @"\\52.15.86.18\storage-for-station\";


            var configs = Configs();

            string ConnectionStringsJunFuDbContext = configs["ConnectionStrings:JunFuDbContext"];
            string ConnectionStringsJunFuTransDbContext = configs["ConnectionStrings:JunFuTransDbContext"];

            DbContextOptions<JunFuDbContext> options = new DbContextOptionsBuilder<JunFuDbContext>().UseSqlServer(ConnectionStringsJunFuDbContext).Options;
            DbContextOptions<JunFuTransDbContext> options2 = new DbContextOptionsBuilder<JunFuTransDbContext>().UseSqlServer(ConnectionStringsJunFuTransDbContext).Options;
            JunFuDbContext junFuDbContext = new JunFuDbContext(options);
            JunFuTransDbContext junFuTransDbContext = new JunFuTransDbContext(options2);
            IDeliveryRequestRepository deliveryRequestRepository = new DeliveryRequestRepository(junFuDbContext, junFuTransDbContext);

            StationAreaRepository stationAreaRepository = new StationAreaRepository(junFuTransDbContext, junFuDbContext);

            StationRepository stationRepository = new StationRepository(junFuDbContext, junFuTransDbContext);

            OrgAreaRepository orgAreaRepository = new OrgAreaRepository(junFuDbContext, junFuTransDbContext);

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<DeliveryRequestMappingProfile>();
                cfg.AddProfile<DeliveryScanLogMappingProfile>();
                cfg.AddProfile<StationAreaMappingProfile>();
            });

            var mapper = config.CreateMapper();

            //var data = deliveryRequestRepository.GetByCheckNumber(input);

            //string result = JsonConvert.SerializeObject(data);

            RequestEntity processEntity = JsonConvert.DeserializeObject<RequestEntity>(input);

            string result = "";

            string copyResult2 = string.Empty;
            int successItems = 0;
            int failItems = 0;
            int notExistItems = 0;

            int checkedFilePercent = 0;

            try
            {
                int id = 0;
                CheckNumberPackageProcess process = new CheckNumberPackageProcess()
                {
                    FileTotalRecords = processEntity.CheckNumbersAndPathsAndTypes.Length,
                    FileCopiedRecords = 0,
                    FileCopiedFail = 0,
                    FileNotExists = 0,
                    StartTime = DateTime.Now,
                    AccountCode = processEntity.AccountCode,
                    IsAccessDownload = false,
                    DataName = processEntity.DataName
                };
                junFuDbContext.CheckNumberPackageProcesses.Add(process);
                junFuDbContext.SaveChanges();
                id = process.RequestId;

                /*if (!CheckIsFolderExists("SignPaperPhotoPackageStoreRoom/"))          //這段註解掉的內容是由AWS提供，目前還不知道有甚麼優點，但會造成某些錯誤，所以改用下面方法
                {
                    CreateFolder("SignPaperPhotoPackageStoreRoom/");        //外層資料夾
                }

                if (!CheckIsFolderExists("SignPaperPhotoPackageStoreRoom/" + id + "/"))
                {
                    CreateFolder("SignPaperPhotoPackageStoreRoom/" + id + "/");
                }*/

                if (!Directory.Exists(S3IPUrl + "SignPaperPhotoPackageStoreRoom/"))
                {
                    Directory.CreateDirectory(S3IPUrl + "SignPaperPhotoPackageStoreRoom/");
                }

                if (!Directory.Exists(S3IPUrl + "SignPaperPhotoPackageStoreRoom/" + id + "/"))
                {
                    Directory.CreateDirectory(S3IPUrl + "SignPaperPhotoPackageStoreRoom/" + id + "/");
                }

                for (var i = 0; i < processEntity.CheckNumbersAndPathsAndTypes.Length; i++)
                {
                    string sourSubPath = "SignPaperPhoto/photo/";
                    string desSubPath = "SignPaperPhotoPackageStoreRoom/";

                    if (processEntity.CheckNumbersAndPathsAndTypes[i].SourceType == 2)          //請參考entity定義，2是簽單在80上
                    {
                        string[] filePathSplit = processEntity.CheckNumbersAndPathsAndTypes[i].SignPhotoPath.Split("/");
                        string fileName = filePathSplit[filePathSplit.Length - 1];
                        int folderDeepth = fileName.Length / 5;        //每五個字一組，與下方不同，抓取的是path

                        for (var d = 0; d < folderDeepth; d++)
                        {
                            sourSubPath += fileName.Substring(5 * d, 5) + "/";
                            if (!Directory.Exists(S3IPUrl + sourSubPath))
                            {
                                Directory.CreateDirectory(S3IPUrl + sourSubPath);
                            }
                        }

                        string sourceFileFullName = sourSubPath + fileName;
                        string destFileFullName = desSubPath + id + "/" + fileName;

                        try
                        {
                            using (WebClient wc = new WebClient())
                            {
                                wc.DownloadFile(processEntity.CheckNumbersAndPathsAndTypes[i].SignPhotoPath, S3IPUrl + sourceFileFullName);     //在SignPaperPhoto也備存一份
                            }
                        }
                        catch           //
                        {

                        }

                        try
                        {
                            using (WebClient wc = new WebClient())
                            {
                                wc.DownloadFile(processEntity.CheckNumbersAndPathsAndTypes[i].SignPhotoPath, S3IPUrl + destFileFullName);
                            }
                            successItems += 1;
                        }
                        catch (Exception ex)
                        {
                            failItems += 1;
                        }
                    }
                    else
                    {
                        int folderDeepth = processEntity.CheckNumbersAndPathsAndTypes[i].CheckNumber.Length / 5;        //每五個字一組，與上方不同，抓取的是checkNumber
                        for (var d = 0; d < folderDeepth; d++)
                        {
                            sourSubPath += processEntity.CheckNumbersAndPathsAndTypes[i].CheckNumber.Substring(5 * d, 5) + "/";
                        }

                        string sourceFileFullName = sourSubPath + processEntity.CheckNumbersAndPathsAndTypes[i].CheckNumber + ".jpg";
                        string destFileFullName = desSubPath + id + "/" + processEntity.CheckNumbersAndPathsAndTypes[i].CheckNumber + ".jpg";

                        if (!CheckIsFileExists(S3IPUrl + sourceFileFullName))
                        {
                            notExistItems += 1;

                            CheckNumberPackageFailItem item = new CheckNumberPackageFailItem()
                            {
                                AccountCode = processEntity.AccountCode,
                                RelativeRequestId = id,
                                CheckNumber = processEntity.CheckNumbersAndPathsAndTypes[i].CheckNumber,
                                Cdate = DateTime.Now,
                                ExpectedPath = sourceFileFullName,
                                IsFailInCopyProcess = false,
                                IsNotExists = true
                            };
                            junFuDbContext.CheckNumberPackageFailItems.Add(item);
                        }
                        else
                        {
                            try
                            {
                                File.Copy(S3IPUrl + sourceFileFullName, S3IPUrl + destFileFullName);

                                successItems += 1;
                            }
                            catch (Exception e)
                            {
                                failItems += 1;

                                CheckNumberPackageFailItem item = new CheckNumberPackageFailItem()
                                {
                                    AccountCode = processEntity.AccountCode,
                                    RelativeRequestId = id,
                                    CheckNumber = processEntity.CheckNumbersAndPathsAndTypes[i].CheckNumber,
                                    Cdate = DateTime.Now,
                                    ExpectedPath = sourceFileFullName,
                                    IsFailInCopyProcess = true,
                                    IsNotExists = false
                                };
                                junFuDbContext.CheckNumberPackageFailItems.Add(item);
                            }
                        }
                    }
                    //copyResult2 = result1.HttpStatusCode.ToString();

                    if ((i + 1) * 100 / processEntity.CheckNumbersAndPathsAndTypes.Length > checkedFilePercent)       //每多1%再寫入資料庫
                    {
                        var obj = junFuDbContext.CheckNumberPackageProcesses.Find(id);
                        obj.FileCopiedFail = failItems;
                        obj.FileCopiedRecords = successItems;
                        obj.FileNotExists = notExistItems;
                        checkedFilePercent = i * 100 / processEntity.CheckNumbersAndPathsAndTypes.Length;

                        if (i == processEntity.CheckNumbersAndPathsAndTypes.Length - 1)
                        {
                            ZipFile.CreateFromDirectory(S3IPUrl + "SignPaperPhotoPackageStoreRoom/" + id + "/", S3IPUrl + "SignPaperPhotoPackageStoreRoom/" + id + ".zip");
                            obj.EndTime = DateTime.Now;
                            obj.IsAccessDownload = true;
                        }

                        if (processEntity.CheckNumbersAndPathsAndTypes.Length == (i + 1))
                        {
                            obj.DataPath = S3Url + desSubPath + id.ToString() + ".zip";
                        }
                        junFuDbContext.SaveChanges();
                    }


                }
            }
            catch (Exception e)
            {
                copyResult2 = e.Message;
            }


            return result + "==>>" + copyResult2;
        }

        private static bool CheckIsFileExists(string folderPath)
        {
            bool isExists = false;

            try
            {
                if (File.Exists(folderPath))
                {
                    isExists = true;
                }
            }
            catch
            {
                isExists = false;
            }

            return isExists;
        }

        /*private void CreateFolder(string folderPath)
        {
            bool isExists = false;
            string accessKeyID = "AKIATV5XU24LGQ3WUFXH";
            string secretKey = "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";

            var credentials = new BasicAWSCredentials(accessKeyID, secretKey);

            AmazonS3Client client = new AmazonS3Client(credentials, RegionEndpoint.USEast2);

            PutObjectRequest putRequest = new PutObjectRequest()
            {
                BucketName = "storage-for-station",
                Key = folderPath
            };

            var response = client.PutObjectAsync(putRequest);
        }*/
    }
}
