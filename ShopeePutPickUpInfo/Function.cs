using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LightYear.JunFuTrans.BL.BE.Api;
using LightYear.JunFuTrans.BL.Services.DeliveryRequest;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRequest;
using LightYear.JunFuTrans.DA.Repositories.Station;
using LightYear.JunFuTrans.DA.Repositories.Account;
using Amazon.Lambda.Core;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.Repositories.PickUpRequest;
using LightYear.JunFuTrans.DA.Repositories.Customer;
using AutoMapper;
using Newtonsoft.Json;
using LightYear.JunFuTrans.Mappers.DeliveryRequest;
using LightYear.JunFuTrans.Mappers.StationWithArea;
using LightYear.JunFuTrans.Utilities.Api;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace ShopeePutPickUpInfo
{
    public class Function
    {
        
        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public string FunctionHandler(string input, ILambdaContext context)
        {
            //初始化Service
            JunFuDbContext junFuDbContext = new JunFuDbContext();

            JunFuTransDbContext junFuTransDbContext = new JunFuTransDbContext();

            DeliveryRequestModifyRepository deliveryRequestModifyRepository = new DeliveryRequestModifyRepository(junFuDbContext, junFuTransDbContext);

            PickUpRequestRepository pickUpRequestRepository = new PickUpRequestRepository(junFuDbContext);

            CheckNumberSDMappingRepository checkNumberSDMappingRepository = new CheckNumberSDMappingRepository(junFuTransDbContext);

            OrgAreaRepository orgAreaRepository = new OrgAreaRepository(junFuDbContext, junFuTransDbContext);

            StationRepository stationRepository = new StationRepository(junFuDbContext, junFuTransDbContext);

            CustomerRepository customerRepository = new CustomerRepository(junFuDbContext);

            DriverRepository driverRepository = new DriverRepository(junFuDbContext);

            DeliveryScanLogRepository deliveryScanLogRepository = new DeliveryScanLogRepository(junFuDbContext);

            PickupRequestForApiuserRepository pickupRequestForApiuserRepository = new PickupRequestForApiuserRepository(junFuDbContext);

            CheckNumberPreheadRepository checkNumberPreheadRepository = new CheckNumberPreheadRepository(junFuDbContext);

            var config = new MapperConfiguration(cfg => {
                cfg.AddProfile<DeliveryRequestMappingProfile>();
                cfg.AddProfile<DeliveryScanLogMappingProfile>();
                cfg.AddProfile<StationAreaMappingProfile>();
            });

            var mapper = config.CreateMapper();

            DeliveryRequestSimpleService deliveryRequestSimpleService = new DeliveryRequestSimpleService(deliveryRequestModifyRepository, pickUpRequestRepository, checkNumberSDMappingRepository, orgAreaRepository, stationRepository, customerRepository, driverRepository, deliveryScanLogRepository, pickupRequestForApiuserRepository, mapper, checkNumberPreheadRepository);

            string customerCode = "F2900210002";

            string apiUrl = "https://api.fs-express.com.tw/v1/api/TestPost";

            Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();


            //取得來自輸入設定檔
            try
            {
                DeliveryStatusConfigEntity deliveryStatusConfigEntity = JsonConvert.DeserializeObject<DeliveryStatusConfigEntity>(input);

              
                if (deliveryStatusConfigEntity.CustomerCode.Length > 0)
                {
                    customerCode = deliveryStatusConfigEntity.CustomerCode;
                }

                if (deliveryStatusConfigEntity.ApiUrl.Length > 0)
                {
                    apiUrl = deliveryStatusConfigEntity.ApiUrl;
                }


                var result = deliveryRequestSimpleService.GetPickupLogReturnEntityByCustomer(customerCode);

                string strResult = JsonConvert.SerializeObject(result);

                PostDataToWebClass postDataToWebClass = new PostDataToWebClass(apiUrl, strResult, keyValuePairs);

                string postResult = postDataToWebClass.PostData("application/json");

                return postResult;

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

            //return input?.ToUpper();
        }
    }
}
