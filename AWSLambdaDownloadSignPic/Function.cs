using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRequest;
using Newtonsoft.Json;
using Amazon.Lambda.Core;
using AutoMapper;
using LightYear.JunFuTrans.Mappers.StationWithArea;
using LightYear.JunFuTrans.Mappers.DeliveryRequest;
using LightYear.JunFuTrans.DA.Repositories.Account;
using LightYear.JunFuTrans.DA.Repositories.Station;
using LightYear.JunFuTrans.DA.Repositories.Customer;
using LightYear.JunFuTrans.DA.Repositories.DeliveryException;
using LightYear.JunFuTrans.Utilities.Reports;
using Microsoft.Extensions.Configuration;
using LightYear.JunFuTrans.BL.Services.StationWithArea;
using Amazon.Runtime;
using Amazon.S3;
using Amazon;
using Amazon.S3.Model;
using LightYear.JunFuTrans.BL.BE.SignPaperPhoto;
using System.Net;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace AWSLambdaDownloadSignPic
{
    public class Function
    {
        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public string FunctionHandler(string input, ILambdaContext context)
        {
            //使用橘子的Service範例

            string _80Url = @"http://220.128.210.180:10080/JUNFU_APP_WebService/PHOTO/";
            string S3Url = @"https://storage-for-station.s3.us-east-2.amazonaws.com/";
            string S3IPUrl = @"\\52.15.86.18\storage-for-station\";

            JunFuDbContext junFuDbContext = new JunFuDbContext();

            JunFuTransDbContext junFuTransDbContext = new JunFuTransDbContext();

            IDeliveryRequestRepository deliveryRequestRepository = new DeliveryRequestRepository(junFuDbContext, junFuTransDbContext);

            StationAreaRepository stationAreaRepository = new StationAreaRepository(junFuTransDbContext);

            StationRepository stationRepository = new StationRepository(junFuDbContext, junFuTransDbContext);

            OrgAreaRepository orgAreaRepository = new OrgAreaRepository(junFuDbContext, junFuTransDbContext);

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<DeliveryRequestMappingProfile>();
                cfg.AddProfile<DeliveryScanLogMappingProfile>();
                cfg.AddProfile<StationAreaMappingProfile>();
            });

            var mapper = config.CreateMapper();

            //var data = deliveryRequestRepository.GetByCheckNumber(input);

            //string result = JsonConvert.SerializeObject(data);

            RequestEntity processEntity = JsonConvert.DeserializeObject<RequestEntity>(input);

            string result = "";

            //對S3檔案變動範例

            string accessKeyID = "AKIATV5XU24LGQ3WUFXH";
            string secretKey = "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";

            var credentials = new BasicAWSCredentials(accessKeyID, secretKey);

            var s3Client = new AmazonS3Client(credentials, RegionEndpoint.USEast2);

            string copyResult2 = string.Empty;
            int successItems = 0;
            int failItems = 0;
            int notExistItems = 0;

            int checkedFilePercent = 0;

            try
            {
                int id = 0;
                CheckNumberPackageProcess process = new CheckNumberPackageProcess()
                {
                    FileTotalRecords = processEntity.CheckNumbersAndPathsAndTypes.Length,
                    FileCopiedRecords = 0,
                    FileCopiedFail = 0,
                    FileNotExists = 0,
                    StartTime = DateTime.Now,
                    AccountCode = processEntity.AccountCode,
                    IsAccessDownload = false,
                    DataName = processEntity.DataName
                };
                junFuDbContext.CheckNumberPackageProcesses.Add(process);
                junFuDbContext.SaveChanges();
                id = process.RequestId;

                /*if (!CheckIsFolderExists("SignPaperPhotoPackageStoreRoom/"))          //這段註解掉的內容是由AWS提供，目前還不知道有甚麼優點，但會造成某些錯誤，所以改用下面方法
                {
                    CreateFolder("SignPaperPhotoPackageStoreRoom/");        //外層資料夾
                }

                if (!CheckIsFolderExists("SignPaperPhotoPackageStoreRoom/" + id + "/"))
                {
                    CreateFolder("SignPaperPhotoPackageStoreRoom/" + id + "/");
                }*/

                if (!Directory.Exists(S3IPUrl + "SignPaperPhotoPackageStoreRoom/"))         
                {
                    Directory.CreateDirectory(S3IPUrl + "SignPaperPhotoPackageStoreRoom/");
                }

                if (!Directory.Exists(S3IPUrl + "SignPaperPhotoPackageStoreRoom/" + id + "/"))
                {
                    Directory.CreateDirectory(S3IPUrl + "SignPaperPhotoPackageStoreRoom/" + id + "/");
                }

                for (var i = 0; i < processEntity.CheckNumbersAndPathsAndTypes.Length; i++)
                {
                    string sourSubPath = "SignPaperPhoto/photo/";
                    string desSubPath = "SignPaperPhotoPackageStoreRoom/";

                    if (processEntity.CheckNumbersAndPathsAndTypes[i].SourceType == 2)          //請參考entity定義，2是簽單在80上
                    {
                        string[] filePathSplit = processEntity.CheckNumbersAndPathsAndTypes[i].SignPhotoPath.Split("/");
                        string fileName = filePathSplit[filePathSplit.Length - 1];
                        int folderDeepth = fileName.Length / 5;        //每五個字一組，與下方不同，抓取的是path

                        for (var d = 0; d < folderDeepth; d++)
                        {
                            sourSubPath += fileName.Substring(5 * d, 5) + "/";
                            if (!Directory.Exists(S3IPUrl + sourSubPath))
                            {
                                Directory.CreateDirectory(S3IPUrl + sourSubPath);
                            }
                        }

                        string sourceFileFullName = sourSubPath + fileName;
                        string destFileFullName = desSubPath + id + "/" + fileName;

                        try
                        {
                            using (WebClient wc = new WebClient())
                            {
                                wc.DownloadFile(processEntity.CheckNumbersAndPathsAndTypes[i].SignPhotoPath, S3IPUrl + sourceFileFullName);     //在SignPaperPhoto也備存一份
                            }
                        }
                        catch           //
                        {

                        }

                        try
                        {
                            using (WebClient wc = new WebClient())
                            {
                                wc.DownloadFile(processEntity.CheckNumbersAndPathsAndTypes[i].SignPhotoPath, S3IPUrl + destFileFullName);
                            }
                            successItems += 1;
                        }
                        catch (Exception ex)
                        {
                            failItems += 1;
                        }
                    }
                    else
                    {
                        int folderDeepth = processEntity.CheckNumbersAndPathsAndTypes[i].CheckNumber.Length / 5;        //每五個字一組，與上方不同，抓取的是checkNumber
                        for (var d = 0; d < folderDeepth; d++)
                        {
                            sourSubPath += processEntity.CheckNumbersAndPathsAndTypes[i].CheckNumber.Substring(5 * d, 5) + "/";
                        }

                        string sourceFileFullName = sourSubPath + processEntity.CheckNumbersAndPathsAndTypes[i].CheckNumber + ".jpg";
                        string destFileFullName = desSubPath + id + "/" + processEntity.CheckNumbersAndPathsAndTypes[i].CheckNumber + ".jpg";

                        if (!CheckIsFolderExists(sourceFileFullName))
                        {
                            notExistItems += 1;

                            CheckNumberPackageFailItem item = new CheckNumberPackageFailItem()
                            {
                                AccountCode = processEntity.AccountCode,
                                RelativeRequestId = id,
                                CheckNumber = processEntity.CheckNumbersAndPathsAndTypes[i].CheckNumber,
                                Cdate = DateTime.Now,
                                ExpectedPath = sourceFileFullName,
                                IsFailInCopyProcess = false,
                                IsNotExists = true
                            };
                            junFuDbContext.CheckNumberPackageFailItems.Add(item);
                        }
                        else
                        {
                            CopyObjectRequest request = new CopyObjectRequest
                            {
                                SourceBucket = "storage-for-station",
                                SourceKey = sourceFileFullName,
                                DestinationBucket = "storage-for-station",
                                DestinationKey = destFileFullName
                            };

                            try
                            {
                                var createResponse = s3Client.CopyObjectAsync(request);

                                var result1 = createResponse.Result;

                                successItems += 1;
                            }
                            catch (Exception e)
                            {
                                failItems += 1;

                                CheckNumberPackageFailItem item = new CheckNumberPackageFailItem()
                                {
                                    AccountCode = processEntity.AccountCode,
                                    RelativeRequestId = id,
                                    CheckNumber = processEntity.CheckNumbersAndPathsAndTypes[i].CheckNumber,
                                    Cdate = DateTime.Now,
                                    ExpectedPath = sourceFileFullName,
                                    IsFailInCopyProcess = true,
                                    IsNotExists = false
                                };
                                junFuDbContext.CheckNumberPackageFailItems.Add(item);
                            }
                        }
                    }
                    //copyResult2 = result1.HttpStatusCode.ToString();

                    if ((i + 1) * 100 / processEntity.CheckNumbersAndPathsAndTypes.Length > checkedFilePercent)       //每多1%再寫入資料庫
                    {
                        var obj = junFuDbContext.CheckNumberPackageProcesses.Find(id);
                        obj.FileCopiedFail = failItems;
                        obj.FileCopiedRecords = successItems;
                        obj.FileNotExists = notExistItems;
                        checkedFilePercent = i * 100 / processEntity.CheckNumbersAndPathsAndTypes.Length;

                        if (i == processEntity.CheckNumbersAndPathsAndTypes.Length - 1)
                        {
                            obj.EndTime = DateTime.Now;
                            obj.IsAccessDownload = true;
                        }

                        junFuDbContext.SaveChanges();
                    }


                }

                //s3Client.CopyObjectAsync()
            }
            catch (AmazonS3Exception e)
            {
                copyResult2 = e.Message;
            }


            return result + "==>>" + copyResult2;
        }

        private bool CheckIsFolderExists(string folderPath)
        {
            bool isExists = false;
            string accessKeyID = "AKIATV5XU24LGQ3WUFXH";
            string secretKey = "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";

            var credentials = new BasicAWSCredentials(accessKeyID, secretKey);

            AmazonS3Client client = new AmazonS3Client(credentials, RegionEndpoint.USEast2);

            GetObjectRequest getRequest = new GetObjectRequest()
            {
                BucketName = "storage-for-station",
                Key = folderPath
            };

            try
            {
                var response = client.GetObjectAsync(getRequest);
                if (response.Result.HttpStatusCode.ToString() == "OK")      //記錄AWS判定資料夾存在的回傳訊息
                {
                    isExists = true;
                }
            }
            catch
            {
                isExists = false;
            }

            return isExists;
        }

        private void CreateFolder(string folderPath)
        {
            bool isExists = false;
            string accessKeyID = "AKIATV5XU24LGQ3WUFXH";
            string secretKey = "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";

            var credentials = new BasicAWSCredentials(accessKeyID, secretKey);

            AmazonS3Client client = new AmazonS3Client(credentials, RegionEndpoint.USEast2);

            PutObjectRequest putRequest = new PutObjectRequest()
            {
                BucketName = "storage-for-station",
                Key = folderPath
            };

            var response = client.PutObjectAsync(putRequest);
        }
    }
}
