﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LightYear.JunFuTrans.BL.BE.Api;
using LightYear.JunFuTrans.BL.Services.DeliveryRequest;
using LightYear.JunFuTrans.BL.Services.LocalLog;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRequest;
using LightYear.JunFuTrans.DA.Repositories.Station;
using LightYear.JunFuTrans.DA.Repositories.Account;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.Repositories.PickUpRequest;
using LightYear.JunFuTrans.DA.Repositories.Customer;
using LightYear.JunFuTrans.DA.Repositories.CbmSize;
using LightYear.JunFuTrans.DA.Repositories.ShopeeLog;
using AutoMapper;
using Newtonsoft.Json;
using LightYear.JunFuTrans.Mappers.DeliveryRequest;
using LightYear.JunFuTrans.Mappers.StationWithArea;
using LightYear.JunFuTrans.Utilities.Api;
using Microsoft.Extensions.Configuration;
using LightYear.JunFuTrans.Mappers.ShopeeLog;
using Microsoft.EntityFrameworkCore;
using JunFuTrans.DA.JunFuTrans.DA;
using JunFuTrans.DA.JunFuTrans.condition;
using RestSharp;
using System.Net;

namespace ShopeePickUpInfo
{
    class Program
    {
        private static IConfiguration Configs()
        {
            var config = new ConfigurationBuilder().AddJsonFile("app.json").Build();
            return config;
        }

        static void Log(string action, string message)
        {
            string ShopeeStatusDetailLogType = "2002";
            shopee_api_log_DA _shopee_api_log_DA = new shopee_api_log_DA();
            _shopee_api_log_DA.Insertshopee_api_log(new shopee_api_log { type = ShopeeStatusDetailLogType, action = action, message = message, cdate = DateTime.Now });

        }

        static void Main(string[] args)
        {

            Console.WriteLine("開始執行！");
            Log("Start", "OK");
            var configs = Configs();

            string ConnectionStringsJunFuDbContext = configs["ConnectionStrings:JunFuDbContext"];
            string ConnectionStringsJunFuTransDbContext = configs["ConnectionStrings:JunFuTransDbContext"];

            DbContextOptions<JunFuDbContext> options = new DbContextOptionsBuilder<JunFuDbContext>().UseSqlServer(ConnectionStringsJunFuDbContext).Options;
            DbContextOptions<JunFuTransDbContext> options2 = new DbContextOptionsBuilder<JunFuTransDbContext>().UseSqlServer(ConnectionStringsJunFuTransDbContext).Options;

            JunFuDbContext junFuDbContext = new JunFuDbContext(options);

            JunFuTransDbContext junFuTransDbContext = new JunFuTransDbContext(options2);

            DeliveryRequestModifyRepository deliveryRequestModifyRepository = new DeliveryRequestModifyRepository(junFuDbContext, junFuTransDbContext);

            PickUpRequestRepository pickUpRequestRepository = new PickUpRequestRepository(junFuDbContext);

            CheckNumberSDMappingRepository checkNumberSDMappingRepository = new CheckNumberSDMappingRepository(junFuTransDbContext);

            OrgAreaRepository orgAreaRepository = new OrgAreaRepository(junFuDbContext, junFuTransDbContext);

            StationRepository stationRepository = new StationRepository(junFuDbContext, junFuTransDbContext);

            CustomerRepository customerRepository = new CustomerRepository(junFuDbContext);

            DriverRepository driverRepository = new DriverRepository(junFuDbContext);

            DeliveryScanLogRepository deliveryScanLogRepository = new DeliveryScanLogRepository(junFuDbContext);

            PickupRequestForApiuserRepository pickupRequestForApiuserRepository = new PickupRequestForApiuserRepository(junFuDbContext);

            CheckNumberPreheadRepository checkNumberPreheadRepository = new CheckNumberPreheadRepository(junFuDbContext);

            LocalLogService localLogService = new LocalLogService();

            CbmSizeRepository cbmSizeRepository = new CbmSizeRepository(junFuDbContext);

            ShopeePickupInfoRepository shopeePickupInfoRepository = new ShopeePickupInfoRepository(junFuTransDbContext);

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<DeliveryRequestMappingProfile>();
                cfg.AddProfile<DeliveryScanLogMappingProfile>();
                cfg.AddProfile<StationAreaMappingProfile>();
                cfg.AddProfile<ShopeeLogMappingProfile>();
            });

            var mapper = config.CreateMapper();

            DeliveryRequestSimpleService deliveryRequestSimpleService = new DeliveryRequestSimpleService(deliveryRequestModifyRepository, pickUpRequestRepository, checkNumberSDMappingRepository, orgAreaRepository, stationRepository, customerRepository, driverRepository, deliveryScanLogRepository, pickupRequestForApiuserRepository, mapper, checkNumberPreheadRepository, cbmSizeRepository);


            string customerCode = configs["CustomerCode"];

            string apiUrl = configs["PickUpUrl"];

            string localPath = configs["LogPath"];

            string input = configs["ParametersString"].Substring(1, configs["ParametersString"].Length - 2);        //去掉頭尾"{","}"，以防string.Format出錯
            input = "{" + string.Format(input, customerCode, apiUrl) + "}";

            Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();

            if (args.Length > 0)
            {
                input = args[0];
            }

            //取得來自輸入設定檔
            try
            {
                DeliveryStatusConfigEntity deliveryStatusConfigEntity = JsonConvert.DeserializeObject<DeliveryStatusConfigEntity>(input);


                if (deliveryStatusConfigEntity.CustomerCode.Length > 0)
                {
                    customerCode = deliveryStatusConfigEntity.CustomerCode;
                }

                if (deliveryStatusConfigEntity.ApiUrl.Length > 0)
                {
                    apiUrl = deliveryStatusConfigEntity.ApiUrl;
                }


                var result = deliveryRequestSimpleService.GetPickupLogReturnEntityByCustomer(customerCode);

                List<List<ApiPickupLogEntity>> listGroup = new List<List<ApiPickupLogEntity>>();
                int c = 50;//50為一組
                int j = c;
                for (int i = 0; i < result.PickupInfo.Count; i += c)
                {
                    List<ApiPickupLogEntity> cList = new List<ApiPickupLogEntity>();
                    cList = result.PickupInfo.Take(j).Skip(i).ToList();
                    j += c;
                    listGroup.Add(cList);
                }
                int cc = 0;

                foreach (var list in listGroup)
                {
                    ApiPickupLogReturnEntity apiPickupLogReturnEntity = new ApiPickupLogReturnEntity();
                    apiPickupLogReturnEntity.PickupInfo = list;
                    string strResult = JsonConvert.SerializeObject(apiPickupLogReturnEntity);

                    Log("API URL", apiUrl);
                    Log("數量", (cc + 1).ToString() + " 到 " + (cc + list.Count()).ToString());
                    cc = cc + list.Count();
                    Log("request", strResult);
                    //Call API

                    try
                    {
                        string postResult = string.Empty;
                        var client = new RestClient(apiUrl);
                        var request = new RestRequest().AddJsonBody(strResult);
                        var response = client.Post<object>(request);

                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            Log("response", response.Content);
                            postResult = response.Content;
                        }
                        else
                        {
                            Log("StatusCode", response.StatusCode.ToString());
                            if (response.ErrorMessage != null)
                            {
                                Log("ErrorMessage", response.ErrorMessage);
                            }

                        }

                        //PostDataToWebClass postDataToWebClass = new PostDataToWebClass(apiUrl, strResult, keyValuePairs);

                        //Console.WriteLine("已送出訊息，等待回應中...");

                        //string postResult = postDataToWebClass.PostData("application/json");
                        Log("開始TXTLOG", localPath);
                        localLogService.Log(localPath, "PickUpInfo", postResult, result);
                        Log("結束TXTLOG", "OK");
                        //Console.WriteLine(postResult);
                    }
                    catch (Exception e)
                    {
                        Log("API Exception Message", e.Message.ToString());
                        Log("API Exception StackTrace", e.StackTrace.ToString());
                        Log("API Exception Source", e.Source.ToString());
                    }
                }

                //寫入log
                Log("開始DB LOG", "ShopeePickUpInfo");
                var shopeePickupInfos = mapper.Map<List<ShopeePickupInfo>>(result.PickupInfo);
                shopeePickupInfoRepository.InsertDatas(shopeePickupInfos);
                Log("結束DB LOG", "OK");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Log("ex", ex.ToString());
            }
            Log("End", "OK");


            //return input?.ToUpper();

        }
    }
}
