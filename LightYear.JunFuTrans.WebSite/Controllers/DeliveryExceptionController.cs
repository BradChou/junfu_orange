﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using LightYear.JunFuTrans.BL.Services.DeliveryException;
using LightYear.JunFuTrans.BL.BE.DeliveryException;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using LightYear.JunFuTrans.BL.BE.Account;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class DeliveryExceptionController : Controller
    {
        private IWebHostEnvironment webHostEnvironment;
        IDeliveryExceptionService DeliveryExceptionService;
        private readonly IConfiguration config;

        public DeliveryExceptionController(IDeliveryExceptionService deliveryExceptionService, IWebHostEnvironment webHostEnvironment, IConfiguration config)
        {
            DeliveryExceptionService = deliveryExceptionService;
            this.webHostEnvironment = webHostEnvironment;
            this.config = config;
        }

        public IActionResult AddExceptions()
        {
            //取得登入資料
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            ViewBag.LoginName = userInfoEntity.UserName;
            ViewBag.LoginStation = userInfoEntity.Station;
            return View();
        }


        public IActionResult DeliveryExceptionSearcher()
        {
            return View();
        }

        public IActionResult GetExceptionsProcessing(string json, string callback = "callback")
        {
            Station station = JsonConvert.DeserializeObject<Station>(json);

            List<DeliveryExceptionEntity> deliveryExceptionEntities = this.DeliveryExceptionService.GetExceptionsProcessing(station.station);

            string exceptions = JsonConvert.SerializeObject(deliveryExceptionEntities);

            string output = string.Format("{0}({1})", callback, exceptions);

            return Content(output);
        }

        public IActionResult GetExceptionsPending(string json, string callback = "callback")
        {
            Station station = JsonConvert.DeserializeObject<Station>(json);

            List<DeliveryExceptionEntity> deliveryExceptionEntities = this.DeliveryExceptionService.GetExceptionsPending(station.station);

            string exceptions = JsonConvert.SerializeObject(deliveryExceptionEntities);

            string output = string.Format("{0}({1})", callback, exceptions);

            return Content(output);
        }
        public IActionResult Destroy(string models, string callback = "callback")
        {

            List<DeliveryExceptionEntity> deliveryExceptionEntities = JsonConvert.DeserializeObject<List<DeliveryExceptionEntity>>(models);

            string deletecheckNumber = deliveryExceptionEntities[4].CheckNumber;
            string handleStation = deliveryExceptionEntities[12].HandleStation;
            string handler_code = deliveryExceptionEntities[8].HandlerCode;
            DateTime cdate = deliveryExceptionEntities[1].CDate;

            this.DeliveryExceptionService.DeleteException(deletecheckNumber, handleStation, handler_code, cdate);

            string output = string.Format("{0}({1})", callback, models);

            return Content(output);
        }

        public IActionResult GetExceptionsInfoByCN(string CN)
        {
            List<CreateDeliveryExceptionEntity> createDeliveryExceptionEntity = DeliveryExceptionService.GetExceptionsInfoByCN(CN);
            string Info = JsonConvert.SerializeObject(createDeliveryExceptionEntity);
            return Json(Info);
        }
        [HttpPost]
        public async Task<IActionResult> SaveAndFileUpload(List<IFormFile> file, string json)
        {
            long size = file.Sum(f => f.Length);
            long fileSizeLimit = config.GetValue<long>("UploadFileSizeLimit");
            string picUrl = string.Empty;
            foreach (var formFile in file)
            {
                if (formFile.Length > 0 && formFile.Length < fileSizeLimit) //1 MB = 1048576 Bytes (in binary)
                {
                    var savePath = this.config.GetValue<string>("UploadFileUrl");
                    string uniqueFileName = GetUniqueFileName(formFile.FileName);
                    string fullPath = string.Format("{0}/{1}", savePath, uniqueFileName);
                    picUrl = fullPath;
                    using (FileStream stream = new FileStream(fullPath, FileMode.Append))
                    {
                        await formFile.CopyToAsync(stream);
                    }
                }
            }
            DeliveryExceptionEntity deliveryExceptionEntities = JsonConvert.DeserializeObject<DeliveryExceptionEntity>(json);

            if (picUrl !="")
            {
                deliveryExceptionEntities.picTop = picUrl.Substring(3);
            }
            DeliveryExceptionService.SaveException(deliveryExceptionEntities);
            return View();
        }
        private string GetUniqueFileName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName) //只有檔名
                      + "_"
                      + Guid.NewGuid().ToString().Substring(0, 4)
                      + Path.GetExtension(fileName); //.副檔名
        }

        public IActionResult GetStation(string callback = "callback")
        {
            List<StationEntity> stations = DeliveryExceptionService.GetStation();
            string data = JsonConvert.SerializeObject(stations);
            string output = string.Format("{0}({1})", callback, data);
            return Content(output);
        }

        public class Station
        {
            public string station;
        }

        public IActionResult GetExceptionFromScanLogs(DateTime start , DateTime end , string stationscode = "", string area = "", string checknumber = "", string callback = "callback")
        {
            List<ExceptionReportAddScanLogEntity> Entities = new List<ExceptionReportAddScanLogEntity>();

            if (checknumber != null && checknumber.Length > 0)
            {
                Entities = DeliveryExceptionService.GetExceptionFromScanLogsByCk(checknumber);
            }
            else
            {
                if (stationscode != null && stationscode.Equals("-1"))//全選
                {
                    Entities = DeliveryExceptionService.GetExceptionFromScanLogsByAllStaionScode(start, end);
                }
                else if (stationscode != null && stationscode.Equals("0")) //區域內全選
                {
                    Entities = DeliveryExceptionService.GetExceptionFromScanLogsByArea(start, end, area);
                }
                else
                {
                    Entities = DeliveryExceptionService.GetExceptionFromScanLogs(start, end, stationscode);
                }

            }
            int i = 1;
            foreach (var item in Entities)
            {
                item.Id = i++;
            }
            
            string performance = JsonConvert.SerializeObject(Entities);
            string output = string.Format("{0}({1})", callback, performance);
            return Content(output);

        }
    }
}
