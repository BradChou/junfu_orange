﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using LightYear.JunFuTrans.BL.Services.CollectionMoney;
using LightYear.JunFuTrans.BL.BE.CollectionMoney;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class CollectionMoneyController : Controller
    {
        ICollectionMoneyService CollectionMoneyService;

        public CollectionMoneyController(ICollectionMoneyService collectionMoneyService)
        {
            this.CollectionMoneyService = collectionMoneyService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult List()
        {
            return View();
        }

        public IActionResult Update(string models, string callback = "callback")
        {
            //修改目前先不動作

            string output = string.Format("{0}({1})", callback, models);

            return Content(output);
        }

        public IActionResult GetAll(string callback = "callback")
        {

            string output = string.Format("{0}([])", callback);

            return Content(output);
        }

        public IActionResult Destroy(string models, string callback = "callback")
        {
            string output = string.Format("{0}({1})", callback, models);

            return Content(output);
        }

        public IActionResult GetCollectionAmount(string barcode)
        {
            double amount = this.CollectionMoneyService.GetCollectionMoney(barcode);

            return Json(new { CheckNumber = barcode, Amount = amount, ActualAmount = amount });
        }

        public IActionResult SaveCollectionMoney([FromBody] CollectionMoneyEntity collectionMoneyEntity)
        {
            int collectionMoneyId = this.CollectionMoneyService.SaveCollectionMoney(collectionMoneyEntity);

            collectionMoneyEntity.Id = collectionMoneyId;

            var data = JsonConvert.SerializeObject(collectionMoneyEntity);

            return Content(data);
        }
    }
}