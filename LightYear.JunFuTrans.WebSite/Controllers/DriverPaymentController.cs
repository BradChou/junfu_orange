﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using LightYear.JunFuTrans.BL.BE.DriverToStationPayment;
using LightYear.JunFuTrans.BL.Services.DriverToStationPayment;
using Newtonsoft.Json;
using LightYear.JunFuTrans.DA.Repositories.Station;
using Microsoft.AspNetCore.Authorization;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.DA.Repositories.DriverPayment;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRequest;
using LightYear.JunFuTrans.DA.Repositories.Account;
using Microsoft.AspNetCore.Http;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class DriverPaymentController : Controller
    {
        public IDriverPaymentService DriverPaymentService { get; set; }

        public IStationRepository StationRepository { get; set; }

        public IDeliveryScanLogRepository DeliveryScanLogRepository { get; set; }

        public IDriverRepository DriverRepository { get; set; }

        public DriverPaymentController(IDriverPaymentService driverPaymentService, IStationRepository stationRepository,
            IDeliveryScanLogRepository deliveryScanLogRepository, IDriverRepository driverRepository)

        {
            DriverPaymentService = driverPaymentService;
            StationRepository = stationRepository;
            DeliveryScanLogRepository = deliveryScanLogRepository;
            DriverRepository = driverRepository;
        }

        [Authorize]
        public IActionResult TodaysSummary()
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            string driverCode = userInfoEntity.UserAccount;

            ViewCreatedSheetEntity driverEntity = DriverPaymentService.GetDriverTodayCreatedSheet(driverCode);

            // 不是司機導回首頁
            if (driverEntity == null)
                return RedirectToAction("Index", "Home", null);

            return View(driverEntity);
        }

        [Authorize]
        public IActionResult EnterTransactionInfo(string driverCode, string difference)
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            string realDriverCode = userInfoEntity.UserAccount;

            if (driverCode != null && driverCode.Length > 0)
            {
                realDriverCode = driverCode;
            }

            var driver = DriverPaymentService.GetDriverTodayCreatedSheet(realDriverCode);

            if (driver == null)
                return RedirectToAction("Index", "Home", null);

            ViewBag.DriverCode = driverCode;
            ViewBag.StationScode = driver.StationScode;
            ViewBag.Difference = int.Parse(difference);

            return View();
        }

        #region 輸入頁底下的明細
        public IActionResult GetDetials(string driverCode, string callback = "callback")
        {
            DriverPaymentDetailEntity[] detailEntities = DriverPaymentService.GetPaymentDetialEntityExceptCreated(driverCode)
                .OrderBy(e => e.CategoryOrder).ToArray();

            string output = string.Format("{0}({1})", callback, JsonConvert.SerializeObject(detailEntities));

            return Content(output);
        }

        public IActionResult UpdateDetials(string model, string callback = "callback")
        {
            IEnumerable<DriverPaymentDetailEntity> entities = JsonConvert.DeserializeObject<IEnumerable<DriverPaymentDetailEntity>>(model);
            var last = entities.Last();

            // 存進session
            var dataInSession = HttpContext.Session.GetString("TorRLastFive");
            Dictionary<string, string> ticketOrRemittanceLastFive;    // 貨號對應票據末五碼

            if (dataInSession == null)
                ticketOrRemittanceLastFive = new Dictionary<string, string>();
            else
                ticketOrRemittanceLastFive = JsonConvert.DeserializeObject<Dictionary<string, string>>(dataInSession);

            if (ticketOrRemittanceLastFive.ContainsKey(last.CheckNumber))
                ticketOrRemittanceLastFive[last.CheckNumber] = last.TicketOrRemittanceLastFive;
            else
                ticketOrRemittanceLastFive.Add(last.CheckNumber, last.TicketOrRemittanceLastFive);

            HttpContext.Session.SetString("TorRLastFive", JsonConvert.SerializeObject(ticketOrRemittanceLastFive));

            // call webservice
            JunFuWebServiceInService.WebServiceSoapClient webServiceSoapClient = new JunFuWebServiceInService.WebServiceSoapClient(JunFuWebServiceInService.WebServiceSoapClient.EndpointConfiguration.WebServiceSoap);
            webServiceSoapClient.UpdatePaidMethodAsync(last.CheckNumber, last.PaidMethod);

            string output = string.Format("{0}({1})", callback, JsonConvert.SerializeObject(last));
            return Content(output);
        }

        public IActionResult UpdateShouldCollectMoneyDifference(string driverCode)
        {
            DriverPaymentDetailEntity[] detailEntities = DriverPaymentService.GetPaymentDetialEntityExceptCreated(driverCode).ToArray();

            CategoryCountEntity count = DriverPaymentService.CalculateCategoryCount(detailEntities);

            int difference = count.CashOnDeliveryMoney + count.DestinationCashMoney + count.DestinationCheckMoney
                + count.PreOrderBagCashMoney + count.PreOrderBagCheckMoney;

            return Content(JsonConvert.SerializeObject(new { difference = difference }));
        }
        #endregion

        public IActionResult ViewCreated(string driverCode)
        {
            ViewCreatedSheetEntity driverPayedInfo = DriverPaymentService.GetDriverTodayCreatedSheet(driverCode);
            ViewBag.Today = DateTime.Today;
            return View(driverPayedInfo);
        }

        public IActionResult SaveData([Bind("DriverCode, StationScode, TranscationLastFive, RemittanceAmount, CheckAmount, CoinsAmount")] DriverPaymentEntity driverPaymentEntity)
        {
            // 一分鐘之內不可以重複做
            var lastSaveTime = HttpContext.Session.GetString("SaveTime");
            if (lastSaveTime != null && DateTime.Parse(lastSaveTime).AddMinutes(1) > DateTime.Now)
            {
                return RedirectToAction("EnterTransactionInfo");
            }

            HttpContext.Session.SetString("SaveTime", DateTime.Now.ToString());

            if (!ModelState.IsValid)
            {
                return View("EnterTransactionInfo");
            }

            DriverPaymentDetailEntity[] detailEntities = DriverPaymentService.GetPaymentDetialEntityExceptCreated(driverPaymentEntity.DriverCode).ToArray();

            //CategoryCountEntity countAndMoney = DriverPaymentService.CalculateCategoryCount(detailEntities);
            //detailEntities = DriverPaymentService.RemoveNoMoneyList(detailEntities);

            string torRLastFive = HttpContext.Session.GetString("TorRLastFive");
            if (torRLastFive != null)
            {
                Dictionary<string, string> lastFive = JsonConvert.DeserializeObject<Dictionary<string, string>>(torRLastFive);
                foreach (var e in detailEntities)
                {
                    if (lastFive.ContainsKey(e.CheckNumber))
                        e.TicketOrRemittanceLastFive = lastFive[e.CheckNumber];
                }
            }

            DriverPaymentEntity entity = new DriverPaymentEntity
            {
                DriverCode = driverPaymentEntity.DriverCode,
                StationScode = driverPaymentEntity.StationScode,
                DriverName = DriverRepository.GetDriverNameByDriverCode(driverPaymentEntity.DriverCode),
                StationName = StationRepository.GetStationNameByScode(driverPaymentEntity.StationScode),
                TranscationLastFive = driverPaymentEntity.TranscationLastFive,
                RemittanceAmount = driverPaymentEntity.RemittanceAmount,
                CoinsAmount = driverPaymentEntity.CoinsAmount,
                CheckAmount = driverPaymentEntity.CheckAmount,
                Details = detailEntities
            };

            int response = DriverPaymentService.SaveDriverPayment(entity);

            return RedirectToAction("ViewCreated", new { driverCode = driverPaymentEntity.DriverCode });
        }

        // 列印頁
        public IActionResult ViewPaymentSheetByDriverAndDate(string driverCode, DateTime date)
        {
            List<PaymentSheetPrintFormatEntity> entity = DriverPaymentService.GetPaymentSheetByDriverAndDate(driverCode, date).ToList();
            return View(entity);
        }

        public IActionResult PrintEmptySheet(string driverCode)
        {
            var driver = DriverRepository.GetByDriverCode(driverCode);
            string stationScode = driver.Station;
            string stationName = StationRepository.GetStationNameByScode(stationScode);
            string driverName = driver.DriverName;

            PrintFormatCategoryEntity[] category = new PrintFormatCategoryEntity[]{
                new PrintFormatCategoryEntity()
                {
                    Category = "應收帳款"
                },
                new PrintFormatCategoryEntity()
                {
                    Category = "代收貨款"
                }
            };

            List<PaymentSheetPrintFormatEntity> entity = new List<PaymentSheetPrintFormatEntity>()
            {
                new PaymentSheetPrintFormatEntity(){
                    DriverName = driverName,
                    StationName = stationName,
                    CategoryCount = category,
                    ForWho = "所長/CS 收執聯"
                },
                new PaymentSheetPrintFormatEntity(){
                    DriverName = driverName,
                    StationName = stationName,
                    CategoryCount = category,
                    ForWho = "繳款人留存聯"
                }
            };
            return View(entity);
        }

        #region 查詢精算書紀錄 by 站所
        // 查詢歷史資料 by 時間, 站所
        public IActionResult ViewList()
        {
            return View();
        }

        public IActionResult GetListByTimeAndStation(string json, string callback = "callback")
        {
            SearchFilterEntity filter = JsonConvert.DeserializeObject<SearchFilterEntity>(json);
            var startTime = DateTime.Parse(filter.StartTime);
            var endTime = DateTime.Parse(filter.EndTime);
            List<DriverPaymentEntity> paymentEntities = DriverPaymentService.GetListByStationAndTime(startTime, endTime, filter.StationScode);
            string paymentSheets = JsonConvert.SerializeObject(paymentEntities);
            string output = string.Format("{0}({1})", callback, paymentSheets);
            return Content(output);
        }

        // 之後要改成公版
        public IActionResult GetAllStation(string callback = "callback")
        {
            List<string> stations = DriverPaymentService.GetAllStationName();
            string serializeStations = JsonConvert.SerializeObject(stations);
            string output = string.Format("{0}({1})", callback, serializeStations);

            return Content(output);
        }
        #endregion

        public IActionResult GetCashFlowDailyData()
        {
            return View();
        }

        public IActionResult GetCashFlowDailyInfo(string station, DateTime start, DateTime end, string callback = "callback")
        {
            DateTime realEnd = end.AddDays(1);

            var data = this.DriverPaymentService.GetCashFlowDailyEntityListByStationAndTime(start, realEnd, station);

            string entities = JsonConvert.SerializeObject(data);

            string output = string.Format("{0}({1})", callback, entities);

            return Content(output);
        }
    }
}