﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.BE.DeliveryException;
using LightYear.JunFuTrans.BL.BE.StationArea;
using LightYear.JunFuTrans.BL.Services.CustomerStationBinding;
using LightYear.JunFuTrans.BL.Services.StationWithArea;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class StationAreaController : Controller
    {
        IStationAreaService StationAreaService { get; set; }

        ICustomerStationBindingService CustomerStationBindingService { get; set; }

        public StationAreaController(IStationAreaService stationAreaService, ICustomerStationBindingService customerStationBindingService)
        {
            StationAreaService = stationAreaService;
            CustomerStationBindingService = customerStationBindingService;
        }

        [Authorize]
        public IActionResult List()
        {
            return View();
        }

        public IActionResult GetAll(string callback = "callback")
        {
            List<StationAreaEntity> stationAreaEntities = StationAreaService.GetAll();
            string stationArea = JsonConvert.SerializeObject(stationAreaEntities);
            string output = string.Format("{0}({1})", callback, stationArea);

            return Content(output);
        }

        public IActionResult Insert(string model, string callback = "callback")
        {
            StationAreaEntity stationAreaEntity = JsonConvert.DeserializeObject<StationAreaEntity>(model);

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            stationAreaEntity.UpdateUser = userInfoEntity.UserAccount;

            stationAreaEntity = StationAreaService.Insert(stationAreaEntity);
            string jsonEntity = JsonConvert.SerializeObject(stationAreaEntity);

            string output = string.Format("{0}({1})", callback, jsonEntity);

            return Content(output);
        }

        public IActionResult Update(string model, string callback = "callback")
        {
            StationAreaEntity stationAreaEntity = JsonConvert.DeserializeObject<StationAreaEntity>(model);

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            stationAreaEntity.UpdateUser = userInfoEntity.UserAccount;

            StationAreaService.Update(stationAreaEntity);
            string jsonEntity = JsonConvert.SerializeObject(stationAreaEntity);

            string output = string.Format("{0}({1})", callback, jsonEntity);

            return Content(output);
        }

        public IActionResult Delete(string model, string callback = "callback")
        {
            StationAreaEntity stationAreaEntity = JsonConvert.DeserializeObject<StationAreaEntity>(model);
            StationAreaService.Delete(stationAreaEntity.Id ?? 0);
            string jsonEntity = JsonConvert.SerializeObject(stationAreaEntity);
            string output = string.Format("{0}({1})", callback, jsonEntity);

            return Content(output);
        }

        public IActionResult GetPost5Code(string station, string callback = "callback")
        {
            var data = this.StationAreaService.GetPost5CodeMappingEntityByStationSCode(station);
            string stationArea = JsonConvert.SerializeObject(data);
            string output = string.Format("{0}({1})", callback, stationArea);

            return Content(output);
        }

        [Authorize]
        public IActionResult Post5MappingSD()
        {
            //取得登入資料
            
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            string station = userInfoEntity.Station;

            if( station == null || station.Length == 0 )
            {
                station = "-9999";
            }

            if( station.StartsWith("F") )
            {
                station = "";
            }

            ViewBag.LoginName = userInfoEntity.UserName;
            ViewBag.UsersId = userInfoEntity.UsersId;
            ViewBag.UserAccount = userInfoEntity.UserAccount;
            ViewBag.Station = station;

            return View();
        }

        public IActionResult EditPost5Mapping(string models, string callback = "callback")
        {
            List<Post5CodeMappingEntity> post5CodeMappingEntities = JsonConvert.DeserializeObject<List<Post5CodeMappingEntity>>(models);

            this.StationAreaService.SavePost5CodeMappingEntity(post5CodeMappingEntities[0]);

            string output = string.Format("{0}({1})", callback, models);

            return Content(output);
        }

        public IActionResult BatchEditPost5Mapping([FromBody] OrgAreaMutiEditEntity orgAreaMutiEditEntity, string type = "0")
        {
            var data = JsonConvert.SerializeObject(orgAreaMutiEditEntity);

            var allIds = orgAreaMutiEditEntity.OrgAreaIds.Split(',');

            List<int> mutiIds = new List<int>();

            foreach(string eachId in allIds)
            {
                try
                {
                    int dataId = Convert.ToInt32(eachId);

                    mutiIds.Add(dataId);
                }
                catch
                { 
                    //dummy
                }

            }

            this.StationAreaService.BatchChangOrgData(mutiIds, orgAreaMutiEditEntity.EditValue, type);

            return Content(data);
        }
    }
}