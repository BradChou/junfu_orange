﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.BE.DeliveryRate;
using LightYear.JunFuTrans.BL.BE.DriverToStationPayment;
using LightYear.JunFuTrans.BL.Services.DeliveryRate;
using LightYear.JunFuTrans.BL.Services.DriverToStationPayment;
using LightYear.JunFuTrans.DA.Repositories.DriverPayment;
using LightYear.JunFuTrans.DA.Repositories.Station;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class StationCollectMoneyController : Controller
    {
        public IStationCollectMoneyService StationCollectMoneyService { get; set; }

        public IDeliveryRateService DeliveryRateService { get; set; }

        public IStationRepository StationRepository { get; set; }

        public IDriverPaymentRepository DriverPaymentRepository { get; set; }

        public StationCollectMoneyController(IStationCollectMoneyService stationCollectMoneyService, IDeliveryRateService deliveryRateService,
            IStationRepository stationRepository, IDriverPaymentRepository driverPaymentRepository)
        {
            StationCollectMoneyService = stationCollectMoneyService;
            DeliveryRateService = deliveryRateService;
            StationRepository = stationRepository;
            DriverPaymentRepository = driverPaymentRepository;
        }

        public IActionResult Search()
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            string stationScode = userInfoEntity.Station;
            string stationName = StationRepository.GetStationNameByScode(stationScode);

            ViewBag.StationScode = stationScode;
            ViewBag.StationShow = stationScode + " " + stationName;

            return View();
        }

        public IActionResult GetListByTimeAndStation(DateTime date, string stationScode, string callback = "callback")
        {
            IEnumerable<StationCollectMoneyEntity> stationCollectMoneyEntities = StationCollectMoneyService.GetStationCollectMoneyByTimeAndStation(date.Date, stationScode);
            string serializedEntities = JsonConvert.SerializeObject(stationCollectMoneyEntities);

            string output = string.Format("{0}({1})", callback, serializedEntities);

            return Content(output);
        }

        public IActionResult UpdatePaymentBalance(string models, string callback = "callback")
        {
            IEnumerable<StationCollectMoneyEntity> entities = JsonConvert.DeserializeObject<IEnumerable<StationCollectMoneyEntity>>(models);

            var updateEntity = entities.Last();
            updateEntity.CurrentPaymentBalace = updateEntity.TotalShouldPay - updateEntity.PaidTotal - updateEntity.NowPayAmount;
            updateEntity.AccumulatedPaymentBalance = updateEntity.CurrentPaymentBalace + updateEntity.PreviousPaymentBalance;
            updateEntity.Date = updateEntity.Date.AddHours(8);

            int affectedRow = StationCollectMoneyService.UpdatePaymentAndPaymentBalance(updateEntity);
            var modified = JsonConvert.SerializeObject(updateEntity);

            string output = string.Format("{0}({1})", callback, modified);
            return Content(output);
        }

        public IActionResult InsertPaymentBalance([FromBody] IEnumerable<StationCollectMoneyEntity> entity)
        {
            if (entity == null || !entity.Any())
                return Content(JsonConvert.SerializeObject(new { result = "查無資料" }));

            if (entity.First().Date < DateTime.Today)
            {
                return Content(JsonConvert.SerializeObject(new { result = "只能編輯今日資料" }));
            }

            int row = 0;

            DateTime date = entity.ToList()[0].Date;
            Dictionary<string, PaymentBalanceEntity> driverCodeWithPaymentBalance = new Dictionary<string, PaymentBalanceEntity>();
            foreach (var e in entity)
            {
                if (e == null)
                    continue;

                driverCodeWithPaymentBalance.Add(e.DriverCode,
                    new PaymentBalanceEntity
                    {
                        CurrentPaymentBalance = e.CurrentPaymentBalace,
                        AccumulatedPaymentBalance = e.CurrentPaymentBalace + e.PreviousPaymentBalance
                    });
            }
            row = DriverPaymentRepository.SavePaymentBalanceData(date, driverCodeWithPaymentBalance);

            return Content(JsonConvert.SerializeObject(new { row = row }));
        }
    }
}
