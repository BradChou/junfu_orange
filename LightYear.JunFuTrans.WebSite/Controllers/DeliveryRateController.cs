﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.BE.DeliveryRate;
using LightYear.JunFuTrans.BL.Services.DeliveryRate;
using LightYear.JunFuTrans.BL.Services.StationWithArea;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.Repositories.Station;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class DeliveryRateController : Controller
    {
        public IDeliveryRateService DeliveryRateService { get; set; }

        public IStationAreaService StationAreaService { get; set; }

        public IStationRepository StationRepository { get; set; }

        public DeliveryRateController(IDeliveryRateService deliveryRateService, IStationAreaService stationAreaService, IStationRepository stationRepository)
        {
            DeliveryRateService = deliveryRateService;
            StationAreaService = stationAreaService;
            StationRepository = stationRepository;
        }

        public IActionResult DeliveryRate()
        {
            return View();
        }


        public IActionResult GetDeliveryRate(string filter, string callback = "callback")
        {
            DeliveryRateSearchFilter f = JsonConvert.DeserializeObject<DeliveryRateSearchFilter>(filter);

            DateTime start = DateTime.Parse(f.Start).AddHours(5);
            DateTime end = DateTime.Parse(f.End).AddHours(5);

            string stationScode = StationRepository.GetStationSCodeByStationCode(f.Station);

            List<DeliveryRateEntity> entities;
            if (f.Station.Equals("-1") && f.Customer.Equals(""))
            {
                entities = DeliveryRateService.GetDeliveryRateEntitiesByTimePeriod(start, end);
            }
            else if (f.Station.Equals("0")) // 選區域，區域內站所全選
            {
                entities = DeliveryRateService.GetDeliveryRateEntitiesByAreaAndTimePeriod(f.Area, start, end);
            }
            else if (!f.Customer.Equals("")) // 選客戶，站所全選
            {
                entities = DeliveryRateService.GetDeliveryRateEntityByCustomerAndTimePeriod(start, end, f.Customer);
            }
            else // 單一站所
            {
                entities = DeliveryRateService.GetDeliveryRateEntityByStationAndTimePeriod(start, end, stationScode);
            }
            
            entities = AddTotalRow(entities);    // 加上加總

            if (f.ShowPercentage)
            {
                List<DeliveryRateEntity> percentEntities = new List<DeliveryRateEntity>();
                foreach (var e in entities)
                {
                    percentEntities.Add(DeliveryRateService.ConvertToPercent(e));
                }
                entities = percentEntities;
            }

            string serializeEntity = JsonConvert.SerializeObject(entities);
            string output = string.Format("{0}({1})", callback, serializeEntity);

            return Content(output);
        }

        public IActionResult GetTodaysDeliveryRateOnUsersAuthority(string callback = "callback")
        {
            // 取得登入者所屬站所
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfo = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            string stationScode = userInfo.Station;

            var owner = StationRepository.GetByScode(stationScode).Owner;
            DeliveryRateEntity deliveryRate;
            if(owner == 0)  // 一般站所
            {
                deliveryRate = DeliveryRateService.GetTodaysDeliveryRateByStation(stationScode);
            }
            else // 不是一般站所就抓全公司的資料
            {
                deliveryRate = DeliveryRateService.GetTodaysDeliveryRate();
            }

            string serializedStr = JsonConvert.SerializeObject(deliveryRate);
            string output = string.Format("{0}({1})", callback, serializedStr);
            return Content(output);
        }

        public IActionResult GetDeliveryRateByDriver(string filter, string callback = "callback")
        {
            DeliveryRateSearchFilter f = JsonConvert.DeserializeObject<DeliveryRateSearchFilter>(filter);
            DateTime start = DateTime.Parse(f.Start);
            DateTime end = DateTime.Parse(f.End);
            List<DeliveryRateByDriverEntity> entities = DeliveryRateService.GetEntitiesByStationSCode(f.Station, start, end);

            entities = AddTotalRow(entities);

            if (f.ShowPercentage)
            {
                List<DeliveryRateByDriverEntity> percentageEntities = new List<DeliveryRateByDriverEntity>();
                foreach (var element in entities)
                {
                    percentageEntities.Add(DeliveryRateService.ConvertToPercent(element));
                }
                entities = percentageEntities;
            }

            string serializeEntity = JsonConvert.SerializeObject(entities);
            string output = string.Format("{0}({1})", callback, serializeEntity);

            return Content(output);
        }

        public IActionResult GetDeliveryDetial(string filter, string callback = "callback")
        {
            DeliveryRateSearchFilter f = JsonConvert.DeserializeObject<DeliveryRateSearchFilter>(filter);
            DateTime start = DateTime.Parse(f.Start);
            DateTime end = DateTime.Parse(f.End);

            List<DeliveryDetailEntity> deliveryDetailEntities = DeliveryRateService.GetDeliveryDetailsByDriverCodeAndDeliveryDate(f.DriverCode, start, end);

            string serializeEntity = JsonConvert.SerializeObject(deliveryDetailEntities);
            string output = string.Format("{0}({1})", callback, serializeEntity);

            return Content(output);
        }

        private List<DeliveryRateEntity> AddTotalRow(List<DeliveryRateEntity> deliveryRateEntities)
        {
            DeliveryRateEntity sum = new DeliveryRateEntity { StationName = "加總" };
            foreach (DeliveryRateEntity d in deliveryRateEntities)
            {
                sum.OrdersNumber += d.OrdersNumber;
                sum.MatingNumber += d.MatingNumber;
                sum.MatingNumBeforeNoon += d.MatingNumBeforeNoon;
                sum.MatingNumBeforeTwenty += d.MatingNumBeforeTwenty;
                sum.MatingNumBeforeFifteen += d.MatingNumBeforeFifteen;
                sum.MatingNumBeforeEighteen += d.MatingNumBeforeEighteen;
                sum.NotWriteOffNumber += d.NotWriteOffNumber;
                sum.NotDeliverNum += d.NotDeliverNum;
                sum.DeliveryNumber += d.DeliveryNumber;
            }
            deliveryRateEntities.Add(sum);
            return deliveryRateEntities;
        }

        private List<DeliveryRateByDriverEntity> AddTotalRow(List<DeliveryRateByDriverEntity> deliveryRateByDriverEntities)
        {
            DeliveryRateByDriverEntity sum = new DeliveryRateByDriverEntity { StationName = "加總" };
            foreach(var d in deliveryRateByDriverEntities)
            {
                sum.OrderNumber += d.OrderNumber;
                sum.NotWriteOffNumber += d.NotWriteOffNumber;
                sum.MatingNumber += d.MatingNumber;
                sum.MatingNumBeforeTwenty += d.MatingNumBeforeTwenty;
                sum.MatingNumBeforeNoon += d.MatingNumBeforeNoon;
                sum.MatingNumBeforeFifteen += d.MatingNumBeforeFifteen;
                sum.MatingNumBeforeEighteen += d.MatingNumBeforeEighteen;
                sum.DeliveryNumber += d.DeliveryNumber;
            }
            deliveryRateByDriverEntities.Add(sum);

            return deliveryRateByDriverEntities;
        }                
    }
}