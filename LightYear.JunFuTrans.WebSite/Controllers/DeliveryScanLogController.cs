﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LightYear.JunFuTrans.BL.Services.DeliveryScanLog;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class DeliveryScanLogController : Controller
    {
        IDeliveryScanLogService DeliveryScanLogService;
        public DeliveryScanLogController(IDeliveryScanLogService deliveryScanLogService)
        {
            DeliveryScanLogService = deliveryScanLogService;
        }
        public IActionResult InsertIntoScanLogSend(string driverCode, string checkNumber, string scanItem, string scanDate)        //掃發送，scanDate日期格式:"yyyyMMddHHmmss"
        {
            bool isSuccess = DeliveryScanLogService.InsertIntoScanLogSend(driverCode, checkNumber, scanItem, scanDate);
            return Content(isSuccess.ToString());
        }
    }
}
