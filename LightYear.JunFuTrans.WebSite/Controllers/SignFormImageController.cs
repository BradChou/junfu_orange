﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using LightYear.JunFuTrans.BL.Services.SignFormImage;
using LightYear.JunFuTrans.BL.BE.SignForm;
using Newtonsoft.Json;
using System.Net.Http;
using LightYear.JunFuTrans.WebSite.Controllers;
using LightYear.JunFuTrans.BL.Services.Reports;
using LightYear.JunFuTrans.BL.Services.DeliveryRequest;
using System.IO;
using LightYear.JunFuTrans.BL.BE.Enumeration;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class SignFormImageController : Controller
    {
        ISignFormImageService SignFormImageService;
        IReportPrintService ReportPrintService;
        IDeliveryRequestService DeliveryRequestService;
        DeliveryRequestController deliveryRequestController;


        public SignFormImageController(ISignFormImageService signFormImageService, IReportPrintService reportPrintService, IDeliveryRequestService deliveryRequestService)
        {
            SignFormImageService = signFormImageService;
            ReportPrintService = reportPrintService;
            DeliveryRequestService = deliveryRequestService;
            deliveryRequestController = new DeliveryRequestController(ReportPrintService, DeliveryRequestService);
        }

        [Authorize]
        public IActionResult SignFormImage()
        {
            return View();
        }

        [Authorize]
        public IActionResult GetSheet(string json, string callback = "callback")
        {
            SignFormInputEntity input = JsonConvert.DeserializeObject<SignFormInputEntity>(json);
            IEnumerable<SignFormEntity> outputData = SignFormImageService.GetData(input.DeliveryDateTimeStart, input.DeliveryDateTimeEnd, input.CustomerCode, input.AreaArriveStation, input.SignPaperExistType);
            string jsonCbmEntities = JsonConvert.SerializeObject(outputData);
            string output = string.Format("{0}({1})", callback, jsonCbmEntities);

            return Content(output);
        }
    }
}
