﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LightYear.JunFuTrans.BL.Services.DriverCollectionMoneyWriteOff;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using LightYear.JunFuTrans.BL.BE.DriverCollectionMoneyWriteOff;
using LightYear.JunFuTrans.BL.BE.Account;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class DriverCollectionMoneyWriteOffController : Controller
    {
        IDriverCollectionMoneyWriteOffService DriverCollectionMoneyWriteOffService;

        public DriverCollectionMoneyWriteOffController(IDriverCollectionMoneyWriteOffService driverCollectionMoneyWriteOffService)
        {
            this.DriverCollectionMoneyWriteOffService = driverCollectionMoneyWriteOffService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult GetAllByStationAndDatetime(string scode, DateTime start, DateTime end, string callback = "callback")
        {
            List<DriverCollectionMoneyWriteOffEntity> Entities = DriverCollectionMoneyWriteOffService.GetAllByStationAndDatetime(scode, start, end);
            string performance = JsonConvert.SerializeObject(Entities);
            string output = string.Format("{0}({1})", callback, performance);
            return Content(output);

        }

    }
}