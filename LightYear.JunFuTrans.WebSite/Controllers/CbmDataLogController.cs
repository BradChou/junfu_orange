﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LightYear.JunFuTrans.BL.Services.CbmDataLog;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using LightYear.JunFuTrans.BL.BE.CbmDataLog;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class CbmDataLogController : Controller
    {
        ICbmDataLogService CbmDataLogService;
        public CbmDataLogController(ICbmDataLogService cbmDataLogService)
        {
            CbmDataLogService = cbmDataLogService;
        }
        public IActionResult InsertIntoCbmDataLog(string date, int station)      //參數格式:yyyyMMddHHmmss，若輸入20210208100000，則會抓取20210207100000~20210208100000，station:29:大園、49:台中
        {
            return Content(CbmDataLogService.InsertIntoCbmDataLog(date, station).ToString());
        }

        [Authorize]
        public IActionResult CbmDataLog()
        {
            return View();
        }

        public IActionResult GetData(string json, string callback = "callback")
        {
            CbmDataLogInputEntity input = JsonConvert.DeserializeObject<CbmDataLogInputEntity>(json);
            IEnumerable<DA.JunFuDb.CbmDataLog> cbmData = CbmDataLogService.GetData(input);
            string jsonCbmEntities = JsonConvert.SerializeObject(cbmData);
            string output = string.Format("{0}({1})", callback, jsonCbmEntities);

            return Content(output);
        }

        public IActionResult UpdateData(string json, string callback = "callback")
        {
            CbmUpdateDataEntity updatedData = JsonConvert.DeserializeObject<CbmUpdateDataEntity>(json);
            CbmDataLogService.UpdateCbmDataLog(updatedData);
            /*frontendDataEntities[0].Id = id;
            string returnModel = JsonConvert.SerializeObject(frontendDataEntities);
            string output = string.Format("{0}({1})", callback, returnModel);
            return Content(output);*/
            return Content("");
        }
    }
}
