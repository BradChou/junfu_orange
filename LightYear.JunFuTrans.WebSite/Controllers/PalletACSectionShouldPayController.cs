﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.BE.PalletACSectionShouldPay;
using LightYear.JunFuTrans.BL.Services.PalletACSectionShouldPay;
using LightYear.JunFuTrans.DA.JunFuDb;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class PalletACSectionShouldPayController : Controller
    {
        IPalletACSectionShouldPayService PalletACSectionShouldPayService;
        public IMapper Mapper { get; private set; }

        public PalletACSectionShouldPayController(IPalletACSectionShouldPayService palletACSectionShouldPayService, IMapper mapper)
        {
            PalletACSectionShouldPayService = palletACSectionShouldPayService;
            this.Mapper = mapper;
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult AllSuppliers()
        {
            return View();
        }
        public IActionResult OneSupplier(string requestMonth, string supplierCode)
        {
            ViewBag.Supplier = supplierCode;
            ViewData["SupplierCode"] = supplierCode.Substring(0, 3);
            ViewBag.SupplierCode = supplierCode.Substring(0, 3);
            ViewBag.Month = requestMonth + " 應付帳款";
            var endYear = Convert.ToInt32(requestMonth.Substring(0, requestMonth.IndexOf("年")));
            var endMonth = Convert.ToInt32(requestMonth.Substring(requestMonth.IndexOf("年") + 1, requestMonth.IndexOf("月") - requestMonth.IndexOf("年") - 1));
            var endDate = new DateTime(endYear, endMonth, 25);
            var startDate = endDate.AddMonths(-1).AddDays(1);
            ViewBag.EndDate = endDate.ToString("d", CultureInfo.CreateSpecificCulture("ja-JP"));
            ViewBag.DateRange = "計費期間 " + startDate.ToString("d", CultureInfo.CreateSpecificCulture("ja-JP")) + "-" + endDate.ToString("d", CultureInfo.CreateSpecificCulture("ja-JP"));
            return View();
        }

        public IActionResult GetSuppliers()
        {
            return Json(PalletACSectionShouldPayService.GetSuppliers());
        }

        public IActionResult GetCSectionFee()
        {
            return Json(PalletACSectionShouldPayService.GetCSectionFee());
        }

        public IActionResult GetACSectionShouldPayDetails(DateTime start, DateTime end, string supplierCode, string checkNumber, string taskId, string callback = "callback")
        {
            List<PalletACSectionShouldPayEntity> data = PalletACSectionShouldPayService.GetACSectionShouldPayDetails(start, end, supplierCode, checkNumber, taskId);

            string jsonData = JsonConvert.SerializeObject(data);

            string output = string.Format("{0}({1})", callback, jsonData);

            return Content(output);
        }

        public IActionResult GetAllSupplierACSectionShouldPay(DateTime start, string supplierCode, string callback = "callback")
        {
            start = new DateTime(start.Year, start.Month, 25);

            var lastStart = start.AddMonths(-1).AddDays(1);

            List<ShowAllSupplierEntity> data = PalletACSectionShouldPayService.GetAllSupplierACSectionShouldPay(lastStart, start, supplierCode);

            string jsonData = JsonConvert.SerializeObject(data);

            string output = string.Format("{0}({1})", callback, jsonData);

            return Content(output);
        }

        public IActionResult GetOneSupplierACSectionShouldPay(DateTime end, string supplierCode, string callback = "callback")
        {
            end = new DateTime(end.Year, end.Month, 25);

            var start = end.AddMonths(-1).AddDays(1);

            List<OneSupplierEntity> data = PalletACSectionShouldPayService.GetOneSupplierACSectionShouldPay(start, end, supplierCode);


            string jsonData = JsonConvert.SerializeObject(data);

            string output = string.Format("{0}({1})", callback, jsonData);

            return Content(output);
        }

        public IActionResult Save(string models, string callback = "callback")
        {
            List<PalletACSectionShouldPayEntity> palletACSectionShouldPayEntity = JsonConvert.DeserializeObject<List<PalletACSectionShouldPayEntity>>(models);

            var palletAcSectionShouldPay = this.Mapper.Map<PalletAcSectionShouldPay>(palletACSectionShouldPayEntity[0]);

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            int id = 0;

            if (palletAcSectionShouldPay.Id == 0)
            {
                palletAcSectionShouldPay.UpdateUser = userInfoEntity.UserAccount;

                id = PalletACSectionShouldPayService.Insert(palletAcSectionShouldPay);
            }
            else
            {
                palletAcSectionShouldPay.UpdateUser = userInfoEntity.UserAccount;

                PalletACSectionShouldPayService.Update(palletAcSectionShouldPay);
            }

            palletACSectionShouldPayEntity[0].CSectionPayment = (palletACSectionShouldPayEntity[0].CShippingFee + palletACSectionShouldPayEntity[0].RemoteFee + palletACSectionShouldPayEntity[0].CSectionSpecialFee) ?? 0;

            palletACSectionShouldPayEntity[0].UpdateUser = userInfoEntity.UserAccount;

            palletACSectionShouldPayEntity[0].PalletACSectionShouldPayId = id;

            models = JsonConvert.SerializeObject(palletACSectionShouldPayEntity);

            string output = string.Format("{0}({1})", callback, models);

            return Content(output);
        }

        public IActionResult ExportFile(string json)
        {
            InputJson ans = JsonConvert.DeserializeObject<InputJson>(json);

            var endYear = Convert.ToInt32(ans.RequestMonth.Substring(0, ans.RequestMonth.IndexOf("年")));
            var endMonth = Convert.ToInt32(ans.RequestMonth.Substring(ans.RequestMonth.IndexOf("年") + 1, ans.RequestMonth.IndexOf("月") - ans.RequestMonth.IndexOf("年") - 1));
            var endDate = new DateTime(endYear, endMonth, 25);

            try
            {
                var bytes = PalletACSectionShouldPayService.Export(endDate, ans.SupplierName);
                return File(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", DateTime.Now.ToString("yyyy/MM/dd") + "_" + ans.SupplierName + "_" + endMonth + "月份請款單.xlsx");
            }
            catch (Exception e)
            {
                string resultCode = "400";
                string errorMessage = e.Message;
                object obj = new { resultCode, errorMessage };
                string response = JsonConvert.SerializeObject(obj);
                return Content(response);
            }
        }

        public IActionResult GenerateZipFile(string forZipJson)
        {
            try
            {
                List<InputJson> ans = JsonConvert.DeserializeObject<List<InputJson>>(forZipJson);
                var zipStream = PalletACSectionShouldPayService.GenerateZipFile(ans);
                var endMonth = Convert.ToInt32(ans[0].RequestMonth.Substring(ans[0].RequestMonth.IndexOf("年") + 1, ans[0].RequestMonth.IndexOf("月") - ans[0].RequestMonth.IndexOf("年") - 1));
                return File(zipStream, "application/octet-stream", DateTime.Now.ToString("yyyy/MM/dd") + "_全部區配商_" + endMonth + "月份請款單.zip");
            }
            catch (Exception e)
            {
                string resultCode = "400";
                string errorMessage = e.Message;
                object obj = new { resultCode, errorMessage };
                string response = JsonConvert.SerializeObject(obj);
                return Content(response);
            }
        }
    }
}