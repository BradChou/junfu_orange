﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LightYear.JunFuTrans.BL.Services.PerformanceReport;
using LightYear.JunFuTrans.BL.BE.StudentTest;
using LightYear.JunFuTrans.BL.BE.PerformanceReport;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using LightYear.JunFuTrans.BL.BE.Account;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class PerformanceReportController : Controller
    {
        IPerformanceReportService PerformanceReportService;
        public PerformanceReportController(IPerformanceReportService performanceReportService)
        {
            PerformanceReportService = performanceReportService;
        }

        public IActionResult GetPerformanceReport(string json, string callback = "callback")
        {
            FrontendInputEntity entity = JsonConvert.DeserializeObject<FrontendInputEntity>(json);

            List<PerformanceReportEntity> performanceReportEntities = this.PerformanceReportService.PerformanceReportList(entity);

            string performance = JsonConvert.SerializeObject(performanceReportEntities);

            string output = string.Format("{0}({1})", callback, performance);

            return Content(output);
        }

        public IActionResult GetAccountByStation(string json, string callback = "callback")
        {
            string station = json.Split('"')[3];

            List<AccountEntity> accountEntities = this.PerformanceReportService.GetAccountByStation(station);

            string performance = JsonConvert.SerializeObject(accountEntities);

            string output = string.Format("{0}({1})", callback, performance);

            return Content(output);
        }

        public IActionResult GetAllAccount(string json, string callback = "callback")
        {
            AccountEntity accountCode = JsonConvert.DeserializeObject<AccountEntity>(json);

            List<AccountEntity> accountList = this.PerformanceReportService.GetAllAccount(accountCode);

            string sta = JsonConvert.SerializeObject(accountList);

            string output = string.Format("{0}({1})", callback, sta);

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            AccountEntity accountEntity = new AccountEntity()
            {
                AccountCode = userInfoEntity.UserAccount
            };

            var account = PerformanceReportService.GetAccountData(accountEntity);

            ViewBag.AccountCode = account.AccountCode;
            ViewBag.AccountName = account.AccountName;
            ViewBag.AccountStation = account.AccountStation;
            ViewBag.IsMaster = account.IsMaster;

            return Content(output);
        }

        [Authorize]
        public IActionResult PerformanceReport()
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            AccountEntity accountEntity = new AccountEntity()
            {
                AccountCode = userInfoEntity.UserAccount,
                AccountLoginStation = userInfoEntity.Station
            };

            var account = PerformanceReportService.GetAccountData(accountEntity);

            ViewBag.AccountCode = account.AccountCode;
            ViewBag.AccountName = account.AccountName;
            ViewBag.AccountStation = account.AccountStation;
            ViewBag.IsMaster = account.IsMaster;

            return View();
        }
    }
}
