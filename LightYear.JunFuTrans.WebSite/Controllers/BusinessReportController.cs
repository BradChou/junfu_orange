﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LightYear.JunFuTrans.BL.Services.BusinessReport;
using Newtonsoft.Json;
using LightYear.JunFuTrans.BL.BE.BusinessReport;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class BusinessReportController : Controller
    {
        IBusinessReportService BusinessReportService;
        public BusinessReportController(IBusinessReportService businessReportService)
        {
            BusinessReportService = businessReportService;
        }

        public IActionResult BusinessReport(string json, string callback = "callback")
        {
            StartAndEndEntity startAndEnd = JsonConvert.DeserializeObject<StartAndEndEntity>(json);

            List<AreaReportEntity> areaReportEntity = this.BusinessReportService.FrontendData(startAndEnd.Start, startAndEnd.End, startAndEnd.DetailOrNot, startAndEnd.AreaArriveCode);

            string exceptions = JsonConvert.SerializeObject(areaReportEntity);

            string output = string.Format("{0}({1})", callback, exceptions);

            return Content(output);
        }

        public IActionResult GetStation()
        {
            return Json(BusinessReportService.StationEntities());
        }

        [Authorize]
        public IActionResult BusinessReporter()
        {
            return View();
        }
    }
}
