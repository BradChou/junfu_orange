﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using LightYear.JunFuTrans.BL.BE.ConstantData;
using LightYear.JunFuTrans.BL.Services.ConstantData;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.WebSite.Models;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class ConstantDataController : Controller
    {
        public IRegionService RegionService;

        private readonly IConfiguration config;

        public ConstantDataController(IRegionService RegionService)
        {
            this.RegionService = RegionService;
        }

        [HttpGet]
        public IActionResult GetCitys()
        {
            APIResponse<List<CityEntity>> res = null;
            List<CityEntity> citys = new List<CityEntity>();

            List<TbPostCity> postCitys = this.RegionService.GetCityList();
            List<TbPostCityArea> postDistricts = this.RegionService.GetDistrictList();

            foreach (var postCity in postCitys)
            {
                var newCity = new CityEntity
                {
                    Id = postCity.Seq,
                    Name = postCity.City,
                    Districts = new List<DistrictEntity>(),
                };

                foreach (var postDistrict in postDistricts)
                {
                    if (postDistrict.City == newCity.Name)
                    {
                        newCity.Districts.Add(new DistrictEntity
                        {
                            Id = postDistrict.Seq,
                            Name = postDistrict.Area,
                            ZipCode = postDistrict.Zip,
                        });
                    }
                }

                citys.Add(newCity);
            }

            if (!citys.Any())
            {
                res = new APIResponse<List<CityEntity>>
                {
                    Code = 404,
                    Data = null,
                    Message = "查無資料"
                };
            }
            else
            {
                res = new APIResponse<List<CityEntity>>
                {
                    Code = 200,
                    Data = citys,
                    Message = "成功"
                };
            }

            string jsonStr = JsonConvert.SerializeObject(res);

            return Content(jsonStr);
        }
    }
}
