﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LightYear.JunFuTrans.BL.BE.Account;
using Newtonsoft.Json;
using LightYear.JunFuTrans.BL.Services.Account;
using LightYear.JunFuTrans.BL.Services.SystemFunction;
using Microsoft.Extensions.Configuration;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class WebLoginController : Controller
    {
        readonly IAccountService AccountService;
        readonly ISystemFunctionService SystemFunctionService;

        private readonly IConfiguration config;

        public WebLoginController(IAccountService accountService, ISystemFunctionService systemFunctionService, IConfiguration config)
        {
            AccountService = accountService;
            SystemFunctionService = systemFunctionService;
            this.config = config;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult GetLogin([FromBody] UserLoginEntity userLoginEntity )
        {
            // UserInfoEntity userInfoEntity  = this.AccountService.CommonLogin(userLoginEntity.Account, userLoginEntity.Password);
            UserInfoWithAddressEntity userInfoEntity = this.AccountService.LoginForNop(userLoginEntity.Account, userLoginEntity.Password);

            string returnData = JsonConvert.SerializeObject(userInfoEntity);

            return Content(returnData);
        }

        public IActionResult GetAction([FromBody] string value)
        {
            return Content(value);
        }

        public IActionResult GetInfo(string getData = "")
        {
            return Content(getData);
        }
    }
}
