﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LightYear.JunFuTrans.DA.LightYearTest;
using LightYear.JunFuTrans.BL.Services.StudentTest;
using LightYear.JunFuTrans.BL.Services.Reports;
using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.BL.BE.StudentTest;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Specialized;
using System.Text;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class TestStudentController : Controller
    {
        IStudentService StudentService;
        IFrontEndDataService FrontEndDataService;
        IReportPrintService ReportPrintService;

        public TestStudentController(IStudentService studentService, IFrontEndDataService frontEndDataService, IReportPrintService reportPrintService)
        {
            StudentService = studentService;
            FrontEndDataService = frontEndDataService;
            ReportPrintService = reportPrintService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult GetStudent(int id)
        {

            PersonEntity student = StudentService.GetStudent(id);

            return View(student);
        }

        [Authorize]
        public IActionResult Students()
        {
            return View();
        }

        public IActionResult GetAll(string callback = "callback")
        {
            List<PersonEntity> personEntities = this.StudentService.GetAllStudents();

            string students = JsonConvert.SerializeObject(personEntities);

            string output = string.Format("{0}({1})", callback, students);

            return Content(output);
        }

        public IActionResult Create(string models,  string callback = "callback")
        {

            List<PersonEntity> personEntities = JsonConvert.DeserializeObject<List<PersonEntity>>(models);

            this.StudentService.SaveStudent(personEntities[0]);

            string output = string.Format("{0}({1})", callback, models);

            return Content(output);
        }

        public IActionResult Destroy(string models, string callback = "callback")
        {

            List<PersonEntity> personEntities = JsonConvert.DeserializeObject<List<PersonEntity>>(models);

            int deleteId = personEntities[0].Id ?? 0;

            this.StudentService.DeleteStudent(deleteId);

            string output = string.Format("{0}({1})", callback, models);

            return Content(output);
        }

        [Authorize]
        public IActionResult FrontEndDatas()
        {
            return View();
        }

        public IActionResult GetFrontDataAll(string callback = "callback")
        {
            List<FrontEndDataEntity> frontendEntities = this.FrontEndDataService.GetAllFrontEndDatas();

            string frontends = JsonConvert.SerializeObject(frontendEntities);

            string output = string.Format("{0}({1})", callback, frontends);

            return Content(output);
        }

        public IActionResult CreateFrontData(string models, string callback = "callback")
        {

            List<FrontEndDataEntity> frontendDataEntities = JsonConvert.DeserializeObject<List<FrontEndDataEntity>>(models);

            int id = this.FrontEndDataService.SaveFrontEndData(frontendDataEntities[0]);

            frontendDataEntities[0].Id = id;

            string returnModel = JsonConvert.SerializeObject(frontendDataEntities);

            string output = string.Format("{0}({1})", callback, returnModel);

            return Content(output);
        }

        public IActionResult DestroyFrontData(string models, string callback = "callback")
        {

            List<FrontEndDataEntity> frontendDataEntities = JsonConvert.DeserializeObject<List<FrontEndDataEntity>>(models);

            int deleteId = frontendDataEntities[0].Id;

            this.FrontEndDataService.DeleteFrontData(deleteId);

            string output = string.Format("{0}({1})", callback, models);

            return Content(output);
        }

        public IActionResult TestPrint()
        {
            NameValueCollection reportParams = new NameValueCollection();

            reportParams["Date"] = DateTime.Today.AddDays(-4).ToString("yyyy-MM-dd");
            reportParams["deliver_status"] = "5";
            reportParams["deadline"] = DateTime.Today.AddDays(4).ToString("yyyy-MM-dd");
            reportParams["station_id"] = "22";

            ReportExportType reportExportType = ReportExportType.PDF;

            var pdfStream = this.ReportPrintService.GetNomalReport(reportParams, "六大歷程掃讀明細", reportExportType);

            string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

            return new FileStreamResult(pdfStream, mimeStr);
        }

        public IActionResult TestToken(string token)
        {
            var json = Encoding.UTF8.GetString(Convert.FromBase64String(token));

            return Content(json);
        }
    }
}