﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LightYear.JunFuTrans.BL.Services.FailDelivery;
using LightYear.JunFuTrans.BL.BE.FailDelivery;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.Services.PerformanceReport;
using LightYear.JunFuTrans.BL.BE.PerformanceReport;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class FailDeliveryController : Controller
    {
        IFailDeliveryService FailDeliveryService;
        public FailDeliveryController(IFailDeliveryService failDeliveryService, IPerformanceReportService performanceReportService)
        {
            FailDeliveryService = failDeliveryService;
            PerformanceReportService = performanceReportService;
        }

        public IPerformanceReportService PerformanceReportService { get; set; }

        [Authorize]
        public IActionResult GetFailDelivery(string json, string callback = "callback")
        {
            FailDeliveryInputEntity entity = JsonConvert.DeserializeObject<FailDeliveryInputEntity>(json);

            List<FailDeliveryEntity> failDeliveryEntities = this.FailDeliveryService.GetFailDeliveryList(entity);

            string deliveryEntities = JsonConvert.SerializeObject(failDeliveryEntities);

            string output = string.Format("{0}({1})", callback, deliveryEntities);

            return Content(output);
        }
        public IActionResult GetAllStations(string callback = "callback")
        {
            return Json(FailDeliveryService.GetAllStations());
        }
        public IActionResult FailDelivery()
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            AccountEntity accountEntity = new AccountEntity()
            {
                AccountCode = userInfoEntity.UserAccount
            };

            var account = PerformanceReportService.GetAccountData(accountEntity);

            ViewBag.AccountCode = account.AccountCode;
            ViewBag.AccountName = account.AccountName;
            ViewBag.AccountStation = account.AccountStation;
            ViewBag.IsMaster = account.IsMaster;

            return View();
        }
        public IActionResult GetAllCustomers(string json, string callback = "callback")       //從performanceReportController抄來的
        {
            AccountEntity accountCode = JsonConvert.DeserializeObject<AccountEntity>(json);

            List<AccountEntity> accountList = this.PerformanceReportService.GetAllAccount(accountCode);

            string sta = JsonConvert.SerializeObject(accountList);

            string output = string.Format("{0}({1})", callback, sta);

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            AccountEntity accountEntity = new AccountEntity()
            {
                AccountCode = userInfoEntity.UserAccount
            };

            var account = PerformanceReportService.GetAccountData(accountEntity);

            ViewBag.AccountCode = account.AccountCode;
            ViewBag.AccountName = account.AccountName;
            ViewBag.AccountStation = account.AccountStation;
            ViewBag.IsMaster = account.IsMaster;

            return Content(output);
        }
        public IActionResult GetCustomerByStation(string station, string callback = "callback")
        {
            List<CustomerDownListEntity> custoemrDownListList = this.FailDeliveryService.GetCustomerByStation(station);

            string customers = JsonConvert.SerializeObject(custoemrDownListList);

            string output = string.Format("{0}({1})", callback, customers);

            return Content(output);
        }
    }
}
