﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LightYear.JunFuTrans.BL.Services.CbmInfoSearcher;
using Microsoft.AspNetCore.Authorization;
using LightYear.JunFuTrans.BL.BE.CbmInfo;
using Newtonsoft.Json;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class CbmInfoController : Controller
    {
        ICbmInfoSearcherService CbmInfoSearcherService;
        public CbmInfoController(ICbmInfoSearcherService cbmInfoService)
        {
            CbmInfoSearcherService = cbmInfoService;
        }

        [Authorize]
        public IActionResult GetCbmInfo(string json, string callback = "callback")
        {
            CbmInfoInputEntity cbmInfoInputEntities = JsonConvert.DeserializeObject<CbmInfoInputEntity>(json);
            cbmInfoInputEntities.End = cbmInfoInputEntities.End?.AddDays(1);

            CbmInfoEntity[] cbmInfoEntities = CbmInfoSearcherService.GetCbmInfoSearcher(cbmInfoInputEntities);
            string jsonCbmEntities = JsonConvert.SerializeObject(cbmInfoEntities);
            string output = string.Format("{0}({1})", callback, jsonCbmEntities);

            return Content(output);
        }

        [Authorize]
        public IActionResult CbmInfo()
        {
            return View();
        }
    }
}
