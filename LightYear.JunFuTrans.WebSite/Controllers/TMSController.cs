﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using LightYear.JunFuTrans.BL.Services.TMS;
using LightYear.JunFuTrans.BL.BE.TMS;
using LightYear.JunFuTrans.DA.Repositories.TMS;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.WebSite.Models;
using LightYear.JunFuTrans.BL.BE.Account;
using Microsoft.AspNetCore.Authorization;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class TMSController : Controller
    {
        public ITMSScheduleService TMSScheduleService;

        public ITMSPalletMatrixService TMSPalletMatrixService;

        private readonly IConfiguration config;

        public TMSController(ITMSScheduleService TMSScheduleService, ITMSPalletMatrixService tMSPalletMatrixService)
        {
            this.TMSScheduleService = TMSScheduleService;
            TMSPalletMatrixService = tMSPalletMatrixService;
        }
        public IActionResult BSectionStation()
        {
            List<CarType> carTypes = this.TMSScheduleService.GetCarTypes();

            List<RunFlowType> runFlowTypes = this.TMSScheduleService.GetRunFlowTypes();

            List<BSectionStop> bSectionStops = this.TMSScheduleService.GetBSectionStops();

            FormFiledDataEntity formFiledDataEntity = new FormFiledDataEntity
            {
                CarTypes = carTypes,
                RunFlowTypes = runFlowTypes,
                BSectionStops = bSectionStops
            };

            return View(formFiledDataEntity);
        }

        public IActionResult FixedSchedule()
        {
            List<CarType> carTypes = this.TMSScheduleService.GetCarTypes();

            List<RunFlowType> runFlowTypes = this.TMSScheduleService.GetRunFlowTypes();

            List<BSectionStop> bSectionStops = this.TMSScheduleService.GetBSectionStops();

            FormFiledDataEntity formFiledDataEntity = new FormFiledDataEntity
            {
                CarTypes = carTypes,
                RunFlowTypes = runFlowTypes,
                BSectionStops = bSectionStops,
            };

            return View(formFiledDataEntity);
        }

        #region 板數統計
        public IActionResult PlateCount()
        {
            return View();
        }

        public IActionResult GetPlateCountData(string printDate)
        {
            var stationPositionEntities = TMSPalletMatrixService.GetStationPositionTitle();

            var countEntity = TMSPalletMatrixService.GetPalletCountByPrintDate(DateTime.Parse(printDate));

            TmsPalletCountViewEntity viewEntity = new TmsPalletCountViewEntity
            {
                Stations = stationPositionEntities,
                CountEntity = countEntity
            };

            return PartialView("_CountTablePartial", viewEntity);
        }
        #endregion

        #region 籠車統計
        [Authorize]
        public IActionResult RollBoxCount()
        {
            return View();
        }

        public IActionResult GetRollBoxCount(string shipDate, string callback = "callback")
        {
            var data = TMSPalletMatrixService.GetBoxCountEntities(DateTime.Parse(shipDate));
            string strData = JsonConvert.SerializeObject(data);
            string output = string.Format("{0}({1})", callback, strData);
            return Content(output);
        }

        public IActionResult UpdateRollBoxCount(string models, string callback = "callback")
        {
            List<TmsBoxCountEntity> entities = JsonConvert.DeserializeObject<List<TmsBoxCountEntity>>(models);

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            string accountCode = userInfoEntity.UserAccount;

            TMSPalletMatrixService.UpdateBoxCountEntityBatch(entities, accountCode);

            string output = string.Format("{0}({1})", callback, models);

            return Content(output);
        } 
        #endregion

        [HttpGet]
        public IActionResult GetStations()
        {
            APIResponse<List<StationEntity>> res = null;
            List<StationEntity> stations = new List<StationEntity>();

            List<BSectionStop> bSectionStops = this.TMSScheduleService.GetBSectionStops();

            foreach (var stop in bSectionStops)
            {
                List<TbSupplier> suppliers = this.TMSScheduleService.GetSuppliersByStationID(stop.Id);

                var newStationEntity = new StationEntity
                {
                    Id = stop.Id,
                    Name = stop.Name,
                    CityId = stop.CityId,
                    DistrictId = stop.DistrictId,
                    Address = stop.Address,
                    IsActive = stop.IsActive,
                    Position = stop.Position,
                    Suppliers = new List<SupplierEntity>(),
                };

                if (suppliers.Count() > 0)
                {
                    foreach (var supplier in suppliers)
                    {
                        newStationEntity.Suppliers.Add(new SupplierEntity
                        {
                            Id = supplier.SupplierId,
                            Name = supplier.SupplierName,
                            Code = supplier.SupplierCode,
                        });
                    }
                }

                stations.Add(newStationEntity);
            }

            if (!stations.Any())
            {
                res = new APIResponse<List<StationEntity>>
                {
                    Code = 404,
                    Data = null,
                    Message = "查無資料"
                };
            }
            else
            {
                res = new APIResponse<List<StationEntity>>
                {
                    Code = 200,
                    Data = stations,
                    Message = "成功"
                };
            }

            string jsonStr = JsonConvert.SerializeObject(res);

            return Content(jsonStr);
        }

        [HttpGet]
        public IActionResult GetSuppliers()
        {
            APIResponse<List<SupplierEntity>> res = null;
            List<SupplierEntity> suppliers = new List<SupplierEntity>();

            List<TbSupplier> tbSuppliers = this.TMSScheduleService.GetSuppliers();

            foreach (var tbSupplier in tbSuppliers)
            {
                var supplier = new SupplierEntity
                {
                    Id = tbSupplier.SupplierId,
                    Name = tbSupplier.SupplierName,
                    Code = tbSupplier.SupplierCode,
                };

                suppliers.Add(supplier);
            }

            if (!suppliers.Any())
            {
                res = new APIResponse<List<SupplierEntity>>
                {
                    Code = 404,
                    Data = null,
                    Message = "查無資料"
                };
            }
            else
            {
                res = new APIResponse<List<SupplierEntity>>
                {
                    Code = 200,
                    Data = suppliers,
                    Message = "成功"
                };
            }

            string jsonStr = JsonConvert.SerializeObject(res);

            return Content(jsonStr);
        }

        [HttpGet]
        public IActionResult GetFixedSchedule(string type)
        {

            List<TMSScheduleEntity> schedule = null;
            APIResponse<List<TMSScheduleEntity>> res = null;

            if (Enum.IsDefined(typeof(OperationStatus), type))
            {
                schedule = this.TMSScheduleService.GetSchedule((OperationStatus)Enum.Parse(typeof(OperationStatus), type));
                if (!schedule.Any())
                {
                    res = new APIResponse<List<TMSScheduleEntity>>
                    {
                        Code = 404,
                        Data = schedule,
                        Message = "查無資料"
                    };
                }
                res = new APIResponse<List<TMSScheduleEntity>>
                {
                    Code = 200,
                    Data = schedule,
                    Message = "成功"
                };

            }
            else
            {
                res = new APIResponse<List<TMSScheduleEntity>>
                {
                    Code = 400,
                    Data = null,
                    Message = "輸入參數錯誤"
                };
            }

            string jsonStr = JsonConvert.SerializeObject(res);

            return Content(jsonStr);
        }

        private List<RelayStop> ParseRelays(string flow, IFormCollection form)
        {
            int relayCount = Int32.Parse(form[flow + "-relay-count"]);
            var relays = new List<RelayStop>();
            for (int i = 0; i < relayCount; i++)
            {
                string id = i.ToString();
                if (!String.IsNullOrEmpty(form[flow + "-relay-" + id]))
                {
                    relays.Add(new RelayStop(form[flow + "-relay-" + id], form[flow + "-relay-arrive-at-" + id], form[flow + "-relay-dispatch-at-" + id]));
                }
            }
            return relays;
        }

        private List<int> ParseSuppliers(IFormCollection form)
        {
            List<int> suppliers = new List<int>();

            for (int i = 0; i < form.Count - 8; i++)
            {
                string id = i.ToString();
                if (!String.IsNullOrEmpty(form["supplier-" + id]))
                {
                    suppliers.Add(Int32.Parse(form["supplier-" + id]));
                }
            }
            return suppliers;
        }

        private BSectionFixedRun ParseRun(IFormCollection form)
        {
            BSectionFixedRun newRunData = new BSectionFixedRun
            {
                FromId = Int32.Parse(form["from"]),
                ToId = Int32.Parse(form["to"]),
                CarTypeId = Int32.Parse(form["car-type"]),
                RunFlowTypeId = Int32.Parse(form["run-type"]),
                DoesSundayDispatch = form["dispatch-sun"] == "on",
                DoesMondayDispatch = form["dispatch-mon"] == "on",
                DoesTuesdayDispatch = form["dispatch-tue"] == "on",
                DoesWednesdayDispatch = form["dispatch-wed"] == "on",
                DoesThursdayDispatch = form["dispatch-thu"] == "on",
                DoesFridayDispatch = form["dispatch-fri"] == "on",
                DoesSaturdayDispatch = form["dispatch-sat"] == "on",
                GoStartAt = Convert.ToDateTime("2000-01-01 " + form["go-start-at"]),
                GoEndAt = Convert.ToDateTime("2000-01-01 " + form["go-end-at"]),
                BackStartAt = Convert.ToDateTime("2000-01-01 " + form["back-start-at"]),
                BackEndAt = Convert.ToDateTime("2000-01-01 " + form["back-end-at"]),
                IsActive = form["status"] == "Normal",
                IsLtl = form["attribute"] == "with-ltl",
                IsDeleted = false,
                CreatedAt = DateTime.Now,
            };

            if (!String.IsNullOrWhiteSpace(form["id"]) && !String.IsNullOrWhiteSpace(form["run-number"]))
            {
                newRunData.Id = Int32.Parse(form["id"]);
                newRunData.RunNumber = Int32.Parse(form["run-number"]);
            }

            return newRunData;
        }

        private BSectionStop ParseStation(IFormCollection form)
        {
            BSectionStop station = new BSectionStop
            {
                Name = form["station-name"],
                CityId = Int32.Parse(form["city"]),
                DistrictId = Int32.Parse(form["district"]),
                Address = form["address"],
                Position = form["position"],
                IsActive = form["status"] == "enable",
            };

            if (!String.IsNullOrWhiteSpace(form["id"]))
            {
                station.Id = Int32.Parse(form["id"]);
            }

            return station;
        }

        [HttpPost]
        public IActionResult UpdateARun(IFormCollection form)
        {
            string id = form["id"];
            string runNumber = form["run-number"];

            if (String.IsNullOrWhiteSpace(id) || String.IsNullOrWhiteSpace(runNumber))
            {
                string error = JsonConvert.SerializeObject(
                    new APIResponse<bool>
                    {
                        Code = 400,
                        Data = false,
                        Message = "輸入參數錯誤"
                    });
                return Content(error);
            }

            var goRelays = this.ParseRelays("go", form);
            var backRelays = this.ParseRelays("back", form);
            var newRunData = this.ParseRun(form);

            BSectionFixedRun updatedRun = this.TMSScheduleService.UpdateARun(newRunData, goRelays, backRelays);

            if (updatedRun == null)
            {
                string error = JsonConvert.SerializeObject(
                    new APIResponse<bool>
                    {
                        Code = 404,
                        Data = false,
                        Message = "查無目標班次"
                    });
                return Content(error);
            }

            TMSScheduleEntity newRunEntity = this.TMSScheduleService.ConvertToScheduleEntity(updatedRun);

            string jsonStr = JsonConvert.SerializeObject(
                new APIResponse<TMSScheduleEntity>
                {
                    Code = 200,
                    Data = newRunEntity,
                    Message = "成功"
                });
            return Content(jsonStr);
        }

        [HttpPost]
        public IActionResult CreateNewRun(IFormCollection form)
        {
            var goRelays = this.ParseRelays("go", form);
            var backRelays = this.ParseRelays("back", form);
            var newRunData = this.ParseRun(form);

            BSectionFixedRun newRun = this.TMSScheduleService.CreateNewRun(newRunData, goRelays, backRelays);

            TMSScheduleEntity newRunEntity = this.TMSScheduleService.ConvertToScheduleEntity(newRun);

            var jsonStr = JsonConvert.SerializeObject(
                new APIResponse<TMSScheduleEntity>
                {
                    Code = 200,
                    Data = newRunEntity,
                    Message = "成功"
                }
            );
            return Content(jsonStr);
        }

        [HttpPost]
        public IActionResult CreateNewStation(IFormCollection form)
        {
            var supplierIDs = this.ParseSuppliers(form);
            var station = this.ParseStation(form);

            BSectionStop newStop = this.TMSScheduleService.CreateNewBSectionStop(station, supplierIDs);

            StationEntity data = new StationEntity { };
            int code = 200;
            string message = "成功";

            if (newStop != null)
            {
                data = new StationEntity
                {
                    Id = newStop.Id,
                    Name = newStop.Name,
                    CityId = newStop.CityId,
                    DistrictId = newStop.DistrictId,
                    Address = newStop.Address,
                    Position = newStop.Position,
                    IsActive = newStop.IsActive,
                    Suppliers = new List<SupplierEntity>(),
                };

                List<TbSupplier> suppliers = this.TMSScheduleService.GetSuppliersByStationID(newStop.Id);

                if (suppliers.Count() > 0)
                {
                    foreach (var supplier in suppliers)
                    {
                        data.Suppliers.Add(new SupplierEntity
                        {
                            Id = supplier.SupplierId,
                            Name = supplier.SupplierName,
                            Code = supplier.SupplierCode,
                        });
                    }
                }
            }
            else
            {
                code = 400;
                message = "建立失敗";
            }

            var jsonStr = JsonConvert.SerializeObject(
                new APIResponse<StationEntity>
                {
                    Code = code,
                    Data = data,
                    Message = message
                }
            );
            return Content(jsonStr);
        }

        [HttpPost]
        public IActionResult UpdateStation(IFormCollection form)
        {
            var supplierIDs = this.ParseSuppliers(form);
            var station = this.ParseStation(form);

            BSectionStop newStop = this.TMSScheduleService.UpdateBSectionStop(station, supplierIDs);

            StationEntity data = new StationEntity { };
            int code = 200;
            string message = "成功";

            if (newStop != null)
            {
                data = new StationEntity
                {
                    Id = newStop.Id,
                    Name = newStop.Name,
                    CityId = newStop.CityId,
                    DistrictId = newStop.DistrictId,
                    Address = newStop.Address,
                    Position = newStop.Position,
                    IsActive = newStop.IsActive,
                    Suppliers = new List<SupplierEntity>(),
                };

                List<TbSupplier> suppliers = this.TMSScheduleService.GetSuppliersByStationID(newStop.Id);

                if (suppliers.Count() > 0)
                {
                    foreach (var supplier in suppliers)
                    {
                        data.Suppliers.Add(new SupplierEntity
                        {
                            Id = supplier.SupplierId,
                            Name = supplier.SupplierName,
                            Code = supplier.SupplierCode,
                        });
                    }
                }
            }
            else
            {
                code = 400;
                message = "更新失敗";
            }

            var jsonStr = JsonConvert.SerializeObject(
                new APIResponse<StationEntity>
                {
                    Code = code,
                    Data = data,
                    Message = message
                }
            );
            return Content(jsonStr);
        }
    }
}
