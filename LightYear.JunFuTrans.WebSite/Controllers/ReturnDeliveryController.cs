﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.BL.Services.DeliveryRequest;
using Newtonsoft.Json;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class ReturnDeliveryController : Controller
    {
        readonly IDeliveryRequestService DeliveryRequestService;

        private readonly IConfiguration config;

        public ReturnDeliveryController(IDeliveryRequestService deliveryRequestService, IConfiguration configuration)
        {
            this.DeliveryRequestService = deliveryRequestService;
            this.config = configuration;
        }
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        public IActionResult GetReturnDelivery()
        {
            return View();
        }
        [Authorize]
        public IActionResult GetReturnDeliveryGrid(string station, string customer, DateTime start, DateTime end, PickUpOrNotTypeConfig checkType, string callback = "callback")
        {
            string entities = string.Empty;
            DateTime realEnd = end.AddDays(1);

            if (customer != null && customer.Length > 0)
            {
                var data = this.DeliveryRequestService.GetFrontendReturnDeliveryListByCustomerCode(customer, start, realEnd, checkType);

                entities = JsonConvert.SerializeObject(data);
            }
            else if (station != null && station.Length > 0)
            {
                var data = (station == "-1") ? //station=-1 為選擇全部站所
                           this.DeliveryRequestService.GetFrontendReturnDeliveryListByDateZone(start, realEnd, checkType) :
                           this.DeliveryRequestService.GetFrontendReturnDeliveryListBySendStationCode(station, start, realEnd, checkType);

                entities = JsonConvert.SerializeObject(data);
            }

            string output = string.Format("{0}({1})", callback, entities);

            return Content(output);
        }
    }
}
