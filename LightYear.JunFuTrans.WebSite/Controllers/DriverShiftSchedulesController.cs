﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.BE.DriverShiftSchedules;
using LightYear.JunFuTrans.BL.Services.DriverShiftSchedules;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class DriverShiftSchedulesController : Controller
    {
        IDriverShiftSchedulesService DriverShiftSchedulesService { get; set; }

        public DriverShiftSchedulesController(IDriverShiftSchedulesService driverShiftSchedulesService)
        {
            this.DriverShiftSchedulesService = driverShiftSchedulesService;
        }

        public IActionResult Index()
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            ViewBag.LoginStation = userInfoEntity.StationShowName;
            ViewBag.Station = userInfoEntity.Station;
            ViewBag.ShowName = userInfoEntity.ShowName;

            int count = DriverShiftSchedulesService.GetDriversCount(userInfoEntity.Station);
            ViewBag.CountDriver = count;

            return View();
        }

        public IActionResult GetTodayDriverCount(DateTime date, string callback = "callback")
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            string stationScode = userInfoEntity.Station;

            int count = DriverShiftSchedulesService.GetDriversCount(stationScode);

            int takeOffCount = DriverShiftSchedulesService.GetTakeOffDriversCount(stationScode, date);

            int answer = count - takeOffCount;

            string serializedStr = JsonConvert.SerializeObject(answer);
            string output = string.Format("{0}({1})", callback, serializedStr);
            return Content(output);
        }

        public IActionResult GetAllByStationAndDate(string scode, DateTime date, string callback = "callback")
        {
            List<InputEntity> Entities = this.DriverShiftSchedulesService.GetAllByStationAndDate(scode, date);

            string data = JsonConvert.SerializeObject(Entities);

            string output = string.Format("{0}({1})", callback, data);

            return Content(output);
        }

        public IActionResult Destroy(string models, string callback = "callback")
        {
            List<DriverShiftArrangement> Entities = JsonConvert.DeserializeObject<List<DriverShiftArrangement>>(models);

            int deleteId = Entities[0].Id;

            this.DriverShiftSchedulesService.Delete(deleteId);

            string output = string.Format("{0}({1})", callback, models);

            return Content(output);
        }

        public IActionResult Save(string models, string callback = "callback")
        {
            List<InputEntity> driverShiftArrangements = JsonConvert.DeserializeObject<List<InputEntity>>(models);

            driverShiftArrangements[0].TakeOffDate = new DateTime(driverShiftArrangements[0].TakeOffDate.Year, driverShiftArrangements[0].TakeOffDate.Month, driverShiftArrangements[0].TakeOffDate.Day);

            this.DriverShiftSchedulesService.Save(driverShiftArrangements[0]);

            string output = string.Format("{0}({1})", callback, models);

            return Content(output);

        }

        public IActionResult GetAllByStationScode(string scode,string callback = "callback")
        {
            List<DriverShiftArrangement> driverShiftArrangements = this.DriverShiftSchedulesService.GetAllByStationScode(scode);

            string data = JsonConvert.SerializeObject(driverShiftArrangements);

            string output = string.Format("{0}({1})", callback, data);

            return Content(output);

        }
    }
}