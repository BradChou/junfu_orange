﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.Services.Account;
using LightYear.JunFuTrans.BL.Services.SystemFunction;
using Newtonsoft.Json;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using System.Text;
using Microsoft.AspNetCore.Http;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    
    public class AccountController : Controller
    {
        readonly IAccountService AccountService;
        readonly ISystemFunctionService SystemFunctionService;

        private readonly IConfiguration config;

        private readonly ILogger<AccountController> _logger;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AccountController(IAccountService accountService, ISystemFunctionService systemFunctionService, IConfiguration config
            ,ILogger<AccountController> logger, IHttpContextAccessor httpContextAccessor)
        {
            AccountService = accountService;
            SystemFunctionService = systemFunctionService;
            this.config = config;
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Login(string ReturnUrl = "")
        {
            string domain = _httpContextAccessor.HttpContext.Request.Host.Value;

            if (domain == "newerp.junfu.asia" || domain == "newerp2021.junfu.asia")
                return RedirectToAction("LoginForJF");

            ViewBag.ReturnUrl = ReturnUrl;
            return View();
        }

        public IActionResult LoginForJF(string ReturnUrl = "")
        {
            ViewBag.ReturnUrl = ReturnUrl;
            return View();
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();

            CookieOptions options = new CookieOptions
            {
                Expires = DateTime.Now.AddDays(-1),
                Domain = this.config.GetValue<string>("CookieDomain")
            };
            Response.Cookies.Append("OtherLogin", "", options);

            return RedirectToAction("Login", "Account");//導至登入頁
        }

        public async Task<IActionResult> AccessLogin(string account, string password, string returnUrl, bool isFSE = true)
        {
            var userInfoEntity = AccountService.CommonLogin(account, password);

            if (userInfoEntity == null)
            {
                ViewBag.LoginMessage = "帳號密碼錯誤！！";
                return View("Login");
            }

            userInfoEntity.CookieExpiration = DateTime.Now.AddDays(1);

            if (!isFSE)
                userInfoEntity.Company = "2";

            var loginData = JsonConvert.SerializeObject(userInfoEntity);

            Claim[] claims = new[] { new Claim("Account", userInfoEntity.UserAccount), new Claim("UserInfo", loginData) };
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);//Scheme必填
            ClaimsPrincipal principal = new ClaimsPrincipal(claimsIdentity);

            //double loginExpireMinute = this.config.GetValue<double>("LoginExpireMinute");
            await HttpContext.SignInAsync(principal,
                new AuthenticationProperties()
                {
                    IsPersistent = false, //IsPersistent = false，瀏覽器關閉即刻登出
                                          //用戶頁面停留太久，逾期時間，在此設定的話會覆蓋Startup.cs裡的逾期設定
                    /* ExpiresUtc = DateTime.Now.AddMinutes(loginExpireMinute) */
                });

            //寫入cookie給跨網域使用
            //先刪除原有的cookie
            if( Request.Cookies["OtherLogin"] != null )
            {
                Response.Cookies.Delete("OtherLogin");
            }

            CookieOptions options = new CookieOptions
            {
                Expires = DateTime.Now.AddDays(1),
                Domain = this.config.GetValue<string>("CookieDomain")
            };
            Response.Cookies.Append("OtherLogin", loginData, options);

            //加上 Url.IsLocalUrl 防止Open Redirect漏洞
            if (!string.IsNullOrEmpty(returnUrl) && ( Url.IsLocalUrl(returnUrl) || returnUrl.IndexOf(options.Domain) > 0))
            {
                return Redirect(returnUrl);//導到原始要求網址
            }
            else if(isFSE)
            {
                return RedirectToAction("Index", "Home");//到登入後的第一頁，自行決定
            }
            else
            {                
                return RedirectToAction("IndexForJF", "Home");
            }
        }

        [Authorize]
        public IActionResult AfterLogin()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<ul>");

            foreach (Claim claim in HttpContext.User.Claims)
            {
                sb.AppendLine($@"<li> claim.Type:{claim.Type} , claim.Value:{ claim.Value}</li>");
            }
            sb.AppendLine("</ul>");

            ViewBag.msg = sb.ToString();

            return View();
        }

        [Authorize]
        public IActionResult Roles()
        {
            return View();
        }

        [Authorize]
        public IActionResult RolesForJF()
        {
            return View();
        }

        public IActionResult GetAllUsersByRoleId(string callback = "callback", int roleId = 0)
        {
            List<UserSimpleForFrontend> userInfoEntities = this.AccountService.GetUserSimpleByRoleId(roleId);

            string roleEntitiesString = JsonConvert.SerializeObject(userInfoEntities);

            string output = string.Format("{0}({1})", callback, roleEntitiesString);

            return Content(output);
        }

        public IActionResult GetAllRoles(string callback = "callback", string company = "")
        {
            List<RoleInfoEntity> roleInfoEntities = this.AccountService.GetAllRoles(company);

            string roleEntitiesString = JsonConvert.SerializeObject(roleInfoEntities);

            string output = string.Format("{0}({1})", callback, roleEntitiesString);

            return Content(output);
        }

        public IActionResult EditRoles(string models, string callback = "callback", string company = "")
        {
            List<RoleInfoEntity> roleEntities = JsonConvert.DeserializeObject<List<RoleInfoEntity>>(models);

            RoleInfoEntity roleInfoEntity = roleEntities[0];
            if (company.Length > 0)
                roleInfoEntity.Company = company;

            this.AccountService.SaveRole(ref roleInfoEntity);

            roleEntities[0].RoleId = roleInfoEntity.RoleId;

            string returnModel = JsonConvert.SerializeObject(roleEntities);

            string output = string.Format("{0}({1})", callback, returnModel);

            return Content(output);
        }

        public IActionResult DestroyRoles(string models, string callback = "callback")
        {

            List<RoleInfoEntity> roleInfoEntities = JsonConvert.DeserializeObject<List<RoleInfoEntity>>(models);

            int deleteId = roleInfoEntities[0].RoleId;

            this.AccountService.DeleteRole(deleteId);

            string output = string.Format("{0}({1})", callback, models);

            return Content(output);
        }

        public IActionResult RoleFunction(int roleId, string message = "", string company = "")
        {
            var data = this.SystemFunctionService.GetFrontendFunctionInfos(roleId, company);
            var roleInfo = this.AccountService.GetRole(roleId);

            if(roleInfo != null && roleInfo.Name != null)
            {
                ViewBag.RoleName = roleInfo.Name;
            }

            ViewBag.modelData = JsonConvert.SerializeObject(data);
            ViewBag.RoleId = roleId.ToString();
            ViewBag.Company = company;
            ViewBag.message = message;

            return View();
        }

        public IActionResult UserFunction(int userId, int accountType, string message = "", string company = "")
        {
            var data = this.SystemFunctionService.GetFrontendUserFunctionInfos(userId, accountType, company);
            var userInfo = this.AccountService.GetUserById(userId, accountType);

            if (userInfo != null && userInfo.ShowName != null)
            {
                ViewBag.UserName = userInfo.ShowName;
            }

            ViewBag.modelData = JsonConvert.SerializeObject(data);
            ViewBag.UserId = userId.ToString();
            ViewBag.AccountType = accountType.ToString();
            ViewBag.message = message;
            ViewBag.Company = company;

            return View();
        }

        public IActionResult UserRoles(int roleUserId, int roleAccountType, string message = "", string company = "")
        {
            var data = this.AccountService.GetAllRolesFrontendSettingByUserIdAndAccountType(roleUserId, roleAccountType, company);
            var userInfo = this.AccountService.GetUserById(roleUserId, roleAccountType);

            if (userInfo != null && userInfo.ShowName != null)
            {
                ViewBag.UserName = userInfo.ShowName;
            }

            ViewBag.modelData = JsonConvert.SerializeObject(data);
            ViewBag.UserId = roleUserId;
            ViewBag.AccountType = roleAccountType;
            ViewBag.message = message;
            ViewBag.Company = company;

            return View();
        }

        public IActionResult SaveUserRole(int userId, int accountType, string userFunctionInfo, string company = "")
        {
            var functionData = (userFunctionInfo == null) ? new int[0] : Array.ConvertAll(userFunctionInfo.Split(','), s => int.Parse(s));

            if (userId > 0)
            {
                this.AccountService.SaveUserRole(userId, accountType, functionData);
            }

            return RedirectToAction("UserRoles", new { roleUserId = userId, roleAccountType = accountType, message = "儲存成功！", company = company });
        }

        public IActionResult SaveRoleFunction(string roleFunctionInfo, int roleId = 0, string company = "")
        {
            var functionData = (roleFunctionInfo == null) ? new int[0] : Array.ConvertAll(roleFunctionInfo.Split(','), s => int.Parse(s));

            if( roleId > 0 )
            {
                this.SystemFunctionService.SaveRoleFunction(roleId, functionData);
            }

            return RedirectToAction("RoleFunction", new { roleId, message="儲存成功！", company });
        }

        public IActionResult SaveUserFunction(string userFunctionInfo, int userId = 0, int accountType = 1, string company = "")
        {
            var functionData = (userFunctionInfo == null) ? new int[0] : Array.ConvertAll(userFunctionInfo.Split(','), s => int.Parse(s));

            if (userId > 0)
            {
                this.SystemFunctionService.SaveUserFunction(userId, accountType, functionData);
            }

            return RedirectToAction("UserFunction", new { userId, accountType, message = "儲存成功！", company = company });
        }

        public IActionResult SetUserFunction()
        {
            //do nothing

            return View();
        }

        public IActionResult SetJFUserFunction()
        {
            return View();
        }

        /// <summary>
        /// 取得一般使用者資料
        /// </summary>
        /// <returns></returns>
        public IActionResult GetUserData()
        {
            var data = Request.QueryString.Value;

            var getQuery = QueryHelpers.ParseQuery(data);

            var callBack = getQuery["$callback"].ToString();

            //取得開始字串
            string startWith = string.Empty;

            if (getQuery.ContainsKey("$filter"))
            {
                var innerData = getQuery["$filter"].ToString();

                if (innerData.StartsWith("startswith"))
                {
                    startWith = innerData.Substring(innerData.IndexOf('\'') + 1, innerData.LastIndexOf('\'') - innerData.IndexOf('\'') - 1);
                }
            }

            var getReturn = this.AccountService.GetStartWith(startWith);

            var objectStr = JsonConvert.SerializeObject(getReturn);

            var output = string.Format("{0}({{d:{1} }})", callBack, objectStr );

            return Content(output);
        }

        public IActionResult GetJFUserData(string managerType = "1")
        {
            var data = Request.QueryString.Value;

            var getQuery = QueryHelpers.ParseQuery(data);

            var callBack = getQuery["$callback"].ToString();

            //取得開始字串
            string startWith = string.Empty;

            if (getQuery.ContainsKey("$filter"))
            {
                var innerData = getQuery["$filter"].ToString();

                if (innerData.StartsWith("startswith"))
                {
                    startWith = innerData.Substring(innerData.IndexOf('\'') + 1, innerData.LastIndexOf('\'') - innerData.IndexOf('\'') - 1);
                }
            }

            var getReturn = AccountService.GetJFStartWith(startWith, managerType);

            var objectStr = JsonConvert.SerializeObject(getReturn);

            var output = string.Format("{0}({{d:{1} }})", callBack, objectStr);

            return Content(output);
        }

        public IActionResult RoleUsers(int roleId = 0)
        {
            ViewBag.RoleId = roleId;

            RoleInfoEntity roleInfoEntity = this.AccountService.GetRole(roleId);

            if(roleInfoEntity != null && roleInfoEntity.Name != null)
            {
                ViewBag.RoleName = roleInfoEntity.Name;
            }

            return View();
        }

        /// <summary>
        /// 取得司機資料
        /// </summary>
        /// <returns></returns>
        public IActionResult GetDriverData()
        {
            var data = Request.QueryString.Value;

            var getQuery = QueryHelpers.ParseQuery(data);

            var callBack = getQuery["$callback"].ToString();

            //取得開始字串
            string startWith = string.Empty;

            if (getQuery.ContainsKey("$filter"))
            {
                var innerData = getQuery["$filter"].ToString();

                if (innerData.StartsWith("startswith"))
                {
                    startWith = innerData.Substring(innerData.IndexOf('\'') + 1, innerData.LastIndexOf('\'') - innerData.IndexOf('\'') - 1);
                }
            }

            var getReturn = this.AccountService.GetDriversStartWith(startWith);

            var objectStr = JsonConvert.SerializeObject(getReturn);

            var output = string.Format("{0}({{d:{1} }})", callBack, objectStr);

            return Content(output);
        }
    }
}