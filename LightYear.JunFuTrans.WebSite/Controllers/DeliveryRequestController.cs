﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LightYear.JunFuTrans.DA.LightYearTest;
using LightYear.JunFuTrans.BL.Services.StudentTest;
using LightYear.JunFuTrans.BL.Services.Reports;
using LightYear.JunFuTrans.BL.Services.DeliveryRequest;
using LightYear.JunFuTrans.BL.BE.DeliveryRequest;
using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.BL.BE.StudentTest;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Specialized;
using System.IO;
using System.Drawing;
using JunFuTrans.DA.JunFuTrans.DA;
using LightYear.JunFuTrans.BL.BE.LogToDB;
using System.Text;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class DeliveryRequestController : Controller
    {
        IReportPrintService ReportPrintService;
        IDeliveryRequestService DeliveryRequestService;

        public DeliveryRequestController(IReportPrintService reportPrintService, IDeliveryRequestService deliveryRequestService)
        {
            this.ReportPrintService = reportPrintService;
            this.DeliveryRequestService = deliveryRequestService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult GetTagImage(string checkNumber = "103007511175", string token = "")
        {
            //配區
            string[] checkNumbers = { checkNumber };
            this.DeliveryRequestService.SaveDeliveryRequestMDSDByCheckNumbers(checkNumbers);

            var data = this.DeliveryRequestService.GetTagImageBase64Entity(checkNumber, token);

            return Json(data);
        }

        public IActionResult PrintTagWithBase64(string checkNumber = "103004646797")
        {
            NameValueCollection reportParams = new NameValueCollection
            {
                ["check_number"] = checkNumber
            };

            //配區
            string[] checkNumbers = { checkNumber };
            this.DeliveryRequestService.SaveDeliveryRequestMDSDByCheckNumbers(checkNumbers);

            ReportExportType reportExportType = ReportExportType.IMAGE;

            var pdfStream = this.ReportPrintService.GetNomalReport(reportParams, "LTReelLabel", reportExportType);

            byte[] bytes;
            using (var memoryStream = new MemoryStream())
            {
                pdfStream.CopyTo(memoryStream);
                bytes = memoryStream.ToArray();
            }

            string base64 = Convert.ToBase64String(bytes);

            ViewBag.Base64Data = base64;

            return View();
        }

        public IActionResult PrintTag(string checkNumber = "103004646797", string imageType = "PNG")
        {

            NameValueCollection reportParams = new NameValueCollection
            {
                ["check_number"] = checkNumber
            };

            //配區
            string[] checkNumbers = { checkNumber };
            this.DeliveryRequestService.SaveDeliveryRequestMDSDByCheckNumbers(checkNumbers);

            ReportExportType reportExportType = ReportExportType.IMAGE;

            var pdfStream = this.ReportPrintService.GetNomalReport(reportParams, "LTReelLabel", reportExportType, imageType);

            string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType, imageType);

            //return new FileStreamResult(pdfStream, mimeStr);

            return File(pdfStream, mimeStr, string.Format("{0}.{1}", checkNumber, imageType));
        }

        public IActionResult PrintTagPDF(string checkNumber = "103004646797")
        {
            try
            {

                NameValueCollection reportParams = new NameValueCollection
                {
                    ["check_number"] = checkNumber
                };

                //配區
                string[] checkNumbers = { checkNumber };
                this.DeliveryRequestService.SaveDeliveryRequestMDSDByCheckNumbers(checkNumbers);

                ReportExportType reportExportType = ReportExportType.PDF;

                var pdfStream = this.ReportPrintService.GetNomalReport(reportParams, "LTReelLabel", reportExportType);

                string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

                return new FileStreamResult(pdfStream, mimeStr);
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }


        }

        public IActionResult PrintMutiTag(string[] checkNumber)
        {
            Dictionary<string, string[]> reportParams = new Dictionary<string, string[]>
            {
                ["check_number"] = checkNumber
            };

            //配區
            this.DeliveryRequestService.SaveDeliveryRequestMDSDByCheckNumbers(checkNumber);

            ReportExportType reportExportType = ReportExportType.IMAGE;

            var pdfStream = this.ReportPrintService.GetNomalReport(reportParams, "LTReelLabel", reportExportType);

            string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

            //return new FileStreamResult(pdfStream, mimeStr);

            return File(pdfStream, mimeStr, string.Format("{0}.TIF", checkNumber[0]));
        }

        public IActionResult PrintMutiTagPDF(string[] checkNumber)
        {
            Dictionary<string, string[]> reportParams = new Dictionary<string, string[]>
            {
                ["check_number"] = checkNumber
            };

            //配區
            this.DeliveryRequestService.SaveDeliveryRequestMDSDByCheckNumbers(checkNumber);

            ReportExportType reportExportType = ReportExportType.PDF;

            var pdfStream = this.ReportPrintService.GetNomalReport(reportParams, "LTReelLabel", reportExportType);

            string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

            return new FileStreamResult(pdfStream, mimeStr);

        }

        public IActionResult PrintMutiTagPDFA4(string[] checkNumber)
        {
            List<string> checkNumbers1 = new List<string>();

            List<string> checkNumbers2 = new List<string>();

            int i = 0;

            foreach (string eachCheckNumber in checkNumber)
            {
                if ((i % 2) == 0)
                {
                    checkNumbers1.Add(eachCheckNumber);
                }
                else
                {
                    checkNumbers2.Add(eachCheckNumber);
                }
                i++;
            }

            Dictionary<string, string[]> reportParams = new Dictionary<string, string[]>()
            {
                ["check_number1"] = checkNumbers1.ToArray(),
                ["check_number2"] = checkNumbers2.ToArray()
            };

            //寫入配區
            this.DeliveryRequestService.SaveDeliveryRequestMDSDByCheckNumbers(checkNumber);

            ReportExportType reportExportType = ReportExportType.PDF;

            var pdfStream = this.ReportPrintService.GetNomalReport(reportParams, "LTReport2", reportExportType);

            string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

            return new FileStreamResult(pdfStream, mimeStr);
        }

        public IActionResult PrintMutiTagPDFByIds(string Ids)
        {
            if (Ids != null && Ids.Length > 0)
            {
                ReportExportType reportExportType = ReportExportType.PDF;

                var pdfStream = DoReport(Ids, "LTReelLabelByIds", reportExportType);

                string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

                return File(pdfStream, mimeStr, "LTReelLabel.PDF");
            }
            else
            {
                return Content("沒選擇託運單");
            }
        }

        public IActionResult PrintPalletA4(string Ids, bool showSender = true)
        {
            string[] strIds = Ids.Split(',');

            ReportExportType reportExportType = ReportExportType.PDF;

            Dictionary<string, string[]> reportParams;

            reportParams = new Dictionary<string, string[]>()
            {
                ["request_ids"] = strIds,
                ["show_sender"] = new string[] { showSender.ToString() }
            };

            var pdfStream = this.ReportPrintService.GetNomalReport(reportParams, "PalletLabel", reportExportType);

            string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

            return File(pdfStream, mimeStr, "PalletLabel.PDF");
        }

        public IActionResult PrintPalletA4WithPosition(string Ids, bool showSender = true, bool showAddress = true, int position = 0)
        {
            string[] strIds = Ids.Split(',');

            ReportExportType reportExportType = ReportExportType.PDF;

            Dictionary<string, string[]> reportParams;

            reportParams = new Dictionary<string, string[]>()
            {
                ["request_ids"] = strIds,
                ["show_sender"] = new string[] { showSender.ToString() },
                ["show_address"] = new string[] { showAddress.ToString() },
                ["position"] = new string[] { position.ToString() }
            };

            var pdfStream = this.ReportPrintService.GetNomalReport(reportParams, "PalletA4LabelWithPosition", reportExportType);

            string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

            return File(pdfStream, mimeStr, "PalletA4Label.PDF");
        }

        public IActionResult PrintPalletSingleA4(string Ids, bool showSender = true, bool showAddress = true)
        {
            string[] strIds = Ids.Split(',');

            ReportExportType reportExportType = ReportExportType.PDF;

            Dictionary<string, string[]> reportParams;

            reportParams = new Dictionary<string, string[]>()
            {
                ["request_ids"] = strIds,
                ["show_sender"] = new string[] { showSender.ToString() },
                ["show_address"] = new string[] { showAddress.ToString() }
            };

            var pdfStream = this.ReportPrintService.GetNomalReport(reportParams, "PalletA4SingleLable", reportExportType);

            string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

            return File(pdfStream, mimeStr, "PalletA4SingleLabel.PDF");
        }

        public IActionResult PrintPalletReel(string Ids, bool showSender = true, bool showAddress = true)
        {
            string[] strIds = Ids.Split(',');

            ReportExportType reportExportType = ReportExportType.PDF;

            Dictionary<string, string[]> reportParams;

            reportParams = new Dictionary<string, string[]>()
            {
                ["request_ids"] = strIds,
                ["show_sender"] = new string[] { showSender.ToString() },
                ["show_address"] = new string[] { showAddress.ToString() }
            };

            var pdfStream = this.ReportPrintService.GetNomalReport(reportParams, "PalletReelLable", reportExportType);

            string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

            return File(pdfStream, mimeStr, "PalletReelLabel.PDF");
        }

        public IActionResult PrintPalletA4WithPositionAndNumber(string Ids, string printNumber = "null", bool showSender = true, bool showAddress = true, int position = 0)
        {
            string[] strIds = Ids.Split(',');

            ReportExportType reportExportType = ReportExportType.PDF;

            Dictionary<string, string[]> reportParams;

            reportParams = new Dictionary<string, string[]>()
            {
                ["request_ids"] = strIds,
                ["show_sender"] = new string[] { showSender.ToString() },
                ["show_address"] = new string[] { showAddress.ToString() },
                ["position"] = new string[] { position.ToString() },
                ["print_number"] = new string[] { printNumber.ToString() }
            };

            var pdfStream = this.ReportPrintService.GetNomalReport(reportParams, "PalletA4LabelWithPositionAndNumber", reportExportType);

            string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

            return File(pdfStream, mimeStr, "PalletA4Label.PDF");
        }

        public IActionResult PrintPalletSingleA4WithNumber(string Ids, string printNumber = "null", bool showSender = true, bool showAddress = true)
        {
            string[] strIds = Ids.Split(',');

            ReportExportType reportExportType = ReportExportType.PDF;

            Dictionary<string, string[]> reportParams;

            reportParams = new Dictionary<string, string[]>()
            {
                ["request_ids"] = strIds,
                ["show_sender"] = new string[] { showSender.ToString() },
                ["show_address"] = new string[] { showAddress.ToString() },
                ["print_number"] = new string[] { printNumber.ToString() }
            };

            var pdfStream = this.ReportPrintService.GetNomalReport(reportParams, "PalletA4SingleLableWithNumber", reportExportType);

            string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

            return File(pdfStream, mimeStr, "PalletA4SingleLabel.PDF");
        }

        public IActionResult PrintPalletA4ReturnLabel(string Ids, string printNumber = "1", bool showSender = true, bool showAddress = true)
        {
            string[] strIds = Ids.Split(',');

            ReportExportType reportExportType = ReportExportType.PDF;

            Dictionary<string, string[]> reportParams;

            reportParams = new Dictionary<string, string[]>()
            {
                ["request_ids"] = strIds,
                ["show_sender"] = new string[] { showSender.ToString() },
                ["show_address"] = new string[] { showAddress.ToString() },
                ["print_number"] = new string[] { printNumber.ToString() }

            };

            var pdfStream = this.ReportPrintService.GetNomalReport(reportParams, "PalletA4ReturnLabel", reportExportType);

            string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

            return File(pdfStream, mimeStr, "PalletA4ReturnLabel.PDF");
        }

        public IActionResult PrintPalletA4ReturnLabelWithPositionAndNumber(string Ids, string printNumber = "1", bool showSender = true, bool showAddress = true, int position = 0)
        {
            string[] strIds = Ids.Split(',');

            ReportExportType reportExportType = ReportExportType.PDF;

            Dictionary<string, string[]> reportParams;

            reportParams = new Dictionary<string, string[]>()
            {
                ["request_ids"] = strIds,
                ["show_sender"] = new string[] { showSender.ToString() },
                ["show_address"] = new string[] { showAddress.ToString() },
                ["print_number"] = new string[] { printNumber.ToString() },
                ["position"] = new string[] { position.ToString() },

            };

            var pdfStream = this.ReportPrintService.GetNomalReport(reportParams, "PalletA4ReturnLabelWithPositionAndNumber", reportExportType);

            string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

            return File(pdfStream, mimeStr, "PalletA4ReturnLabelWithPositionAndNumber.PDF");
        }

        public IActionResult PrintPalletReelReturnLabel(string Ids, string printNumber = "1", bool showSender = true, bool showAddress = true)
        {
            string[] strIds = Ids.Split(',');

            ReportExportType reportExportType = ReportExportType.PDF;

            Dictionary<string, string[]> reportParams;

            reportParams = new Dictionary<string, string[]>()
            {
                ["request_ids"] = strIds,
                ["show_sender"] = new string[] { showSender.ToString() },
                ["show_address"] = new string[] { showAddress.ToString() },
                ["print_number"] = new string[] { printNumber.ToString() }
 

            };

            var pdfStream = this.ReportPrintService.GetNomalReport(reportParams, "PalletReelReturnLabel", reportExportType);

            string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

            return File(pdfStream, mimeStr, "PalletReelReturnLabel.PDF");
        }

        public IActionResult PrintPalletReelWithNumber(string Ids, string printNumber = "null", bool showSender = true, bool showAddress = true)
        {
            string[] strIds = Ids.Split(',');

            ReportExportType reportExportType = ReportExportType.PDF;

            Dictionary<string, string[]> reportParams;

            reportParams = new Dictionary<string, string[]>()
            {
                ["request_ids"] = strIds,
                ["show_sender"] = new string[] { showSender.ToString() },
                ["show_address"] = new string[] { showAddress.ToString() },
                ["print_number"] = new string[] { printNumber.ToString() }
            };

            var pdfStream = this.ReportPrintService.GetNomalReport(reportParams, "PalletReelLableWithNumber", reportExportType);

            string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

            return File(pdfStream, mimeStr, "PalletReelLabel.PDF");
        }

        #region 捲筒過時標籤
        public IActionResult PrintMutiTagPDFByIdsAndSender(string Ids, bool showSender = true)
        {
            if (Ids != null && Ids.Length > 0)
            {
                ReportExportType reportExportType = ReportExportType.PDF;

                var pdfStream = DoReport(Ids, "LTReelLabelByIds_hide_sender", reportExportType, "-1", showSender);

                string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

                return File(pdfStream, mimeStr, "LTReelLabel.PDF");
            }
            else
            {
                return Content("沒選擇託運單");
            }
        }

        public IActionResult PrintLTReelLabelTest(string Ids, bool showSender = true)
        {
            if (Ids != null && Ids.Length > 0)
            {
                ReportExportType reportExportType = ReportExportType.PDF;

                var pdfStream = DoReport(Ids, "LTReelLabelByIds_holiday_test", reportExportType, "-1", showSender);

                string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

                return File(pdfStream, mimeStr, "LTReelLabel.PDF");
            }
            else
            {
                return Content("沒選擇託運單");
            }
        }

        public IActionResult PrintMutiTagPDFA4ByIds(string Ids)
        {
            if (Ids != null && Ids.Length > 0)
            {
                ReportExportType reportExportType = ReportExportType.PDF;

                var pdfStream = DoReport(Ids, "LTReport2_1ByIds", reportExportType);

                string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

                return File(pdfStream, mimeStr, "LTReport.PDF");
            }
            else
            {
                return Content("沒選擇託運單");
            }
        }
        #endregion
        public IActionResult PrintLTReelLabel2_0(string Ids, bool showSender = true)
        {
            if (Ids != null && Ids.Length > 0)
            {
                ReportExportType reportExportType = ReportExportType.PDF;

                var pdfStream = DoReport(Ids, "LTReelLabelByIds_2_0", reportExportType, "-1", showSender);

                string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

                return File(pdfStream, mimeStr, "LTReelLabel.PDF");
            }
            else
            {
                return Content("沒選擇託運單");
            }
        }

        public IActionResult PrintLTReelLabel3_0(string Ids, bool showSender = true)        //國陽版本
        {
            if (Ids != null && Ids.Length > 0)
            {
                ReportExportType reportExportType = ReportExportType.PDF;

                var pdfStream = DoReport(Ids, "LTReelLabelByIds_3_0", reportExportType, "-1", showSender);

                string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

                return File(pdfStream, mimeStr, "LTReelLabel.PDF");
            }
            else
            {
                return Content("沒選擇託運單");
            }
        }


        [Authorize]
        public IActionResult PrintPelicanTag()
        {
            return View();
        }

        public IActionResult PrintPelicanTags(string checkNumbers, DateTime date)
        {

            LogToDBWebSiteEntity _LogToDBWebSiteEntity = new LogToDBWebSiteEntity();
            JunFuWebSiteLog_DA _JunFuWebSiteLog_DA = new JunFuWebSiteLog_DA();
            Pelican_request_DA _Pelican_request_DA = new Pelican_request_DA();
            _LogToDBWebSiteEntity.Cdate = DateTime.Now;
            try
            {
                string resultStr = JsonConvert.SerializeObject(new { checkNumbers, date });
                _LogToDBWebSiteEntity.Request = resultStr;
                _LogToDBWebSiteEntity.IP = HttpContext.Connection.RemoteIpAddress == null ? string.Empty : HttpContext.Connection.RemoteIpAddress.ToString();
                _LogToDBWebSiteEntity.URL = Request.Path;
                _LogToDBWebSiteEntity.UserAgent = Request.Headers["User-Agent"].ToString();
            }
            catch
            {

            }

            if (checkNumbers != null && checkNumbers.Length > 0)
            {
                ReportExportType reportExportType = ReportExportType.PDF;

                var pdfStream = DoPelicanReport(checkNumbers, "Pelican", reportExportType, true);

                string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);
                //紀錄
                //報表值
                string result = JsonConvert.SerializeObject(_Pelican_request_DA.GetPelican_requestByjf_check_number(checkNumbers));

                _LogToDBWebSiteEntity.Response = result;

                _JunFuWebSiteLog_DA.InsertLogToDBWebSiteEntity(_LogToDBWebSiteEntity);

                return File(pdfStream, mimeStr, "Pelican.PDF");
            }
            else
            {
                ReportExportType reportExportType = ReportExportType.PDF;

                var pdfStream = DoPelicanReport(date.ToString("yyyy-MM-dd"), "Pelican", reportExportType, false);

                string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);
                //紀錄
                _JunFuWebSiteLog_DA.InsertLogToDBWebSiteEntity(_LogToDBWebSiteEntity);

                return File(pdfStream, mimeStr, "Pelican.PDF");
                //***從Service獲取checkNumberList
            }
        }

        private Stream DoPelicanReport(string input, string reportName, ReportExportType reportExportType, bool isUsedCheckNumbers)
        {
            Dictionary<string, string[]> reportParams = new Dictionary<string, string[]>();
            if (isUsedCheckNumbers)
            {
                reportParams = new Dictionary<string, string[]>()
                {
                    ["check_number"] = new string[1] { input }
                };
            }
            else
            {
                reportParams = new Dictionary<string, string[]>()
                {
                    ["check_number"] = new string[0],
                    ["date"] = new string[1] { input }
                };
            }

            var pdfStream = this.ReportPrintService.GetNomalReport(reportParams, reportName, reportExportType);

            return pdfStream;
        }

        public IActionResult PrintLifeTransTag(string checkNumbers)
        {
            ReportExportType reportExportType = ReportExportType.PDF;

            Dictionary<string, string[]> reportParams;

            reportParams = new Dictionary<string, string[]>()
            {
                ["check_number"] = new string[] { checkNumbers }
            };

            var pdfStream = this.ReportPrintService.GetNomalReport(reportParams, "LifeTransReelLabel", reportExportType);

            string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

            return File(pdfStream, mimeStr, "LifeTrans.PDF");

        }

        #region A4過時標籤
        public IActionResult PrintMutiTagPDFA4ByIdsAndPosition(string Ids, string position = "0")
        {
            if (Ids != null && Ids.Length > 0)
            {
                ReportExportType reportExportType = ReportExportType.PDF;

                var pdfStream = DoReportWithPosition(Ids, "LTReport2_1ByIds_customerized", reportExportType, position);

                string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

                return File(pdfStream, mimeStr, "LTReport.PDF");
            }
            else
            {
                return Content("沒選擇託運單");
            }
        }

        public IActionResult PrintMutiTagPDFA4ByIdsAndSender(string Ids, string position = "0", bool showSender = true)
        {
            if (Ids != null && Ids.Length > 0)
            {
                ReportExportType reportExportType = ReportExportType.PDF;

                var pdfStream = DoReport(Ids, "LTReport2_1ByIds_hide_sender", reportExportType, position, showSender);

                string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

                return File(pdfStream, mimeStr, "LTReport.PDF");
            }
            else
            {
                return Content("沒選擇託運單");
            }
        }

        public IActionResult PrintLTA4LabelForTest(string Ids, string position = "0", bool showSender = true)
        {
            if (Ids != null && Ids.Length > 0)
            {
                ReportExportType reportExportType = ReportExportType.PDF;

                var pdfStream = DoReport(Ids, "LTReport2_1ByIds_holiday_test", reportExportType, position, showSender);

                string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

                return File(pdfStream, mimeStr, "LTReport.PDF");
            }
            else
            {
                return Content("沒選擇託運單");
            }
        }
        #endregion
        public IActionResult PrintLTA4Label2_0(string Ids, string position = "0", bool showSender = true)
        {
            if (Ids != null && Ids.Length > 0)
            {
                ReportExportType reportExportType = ReportExportType.PDF;

                var pdfStream = DoReport(Ids, "LTA4LabelByIds_2_0", reportExportType, position, showSender);

                string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

                return File(pdfStream, mimeStr, "LTReport.PDF");
            }
            else
            {
                return Content("沒選擇託運單");
            }
        }

        public IActionResult PrintLTA4Label2_1(string Ids, string position = "0", bool showSender = true)
        {
            if (Ids != null && Ids.Length > 0)
            {
                //Ids太大時會產生問題，先用session來處理
                string infoKey = DeliveryRequestService.SetAreaArriveCodeForLabelEntitiesByIds(Ids);

                ReportExportType reportExportType = ReportExportType.PDF;

                var pdfStream = DoReport(Ids, "LTA4LabelByIds_2_1", reportExportType, position, showSender, infoKey);

                string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

                return File(pdfStream, mimeStr, "LTA4LabelByIds_2_1.PDF");
            }
            else
            {
                return Content("沒選擇託運單");
            }
        }

        public IActionResult PrintLTA4Label3_0(string Ids, string position = "0", bool showSender = true)       //國陽版本
        {
            if (Ids != null && Ids.Length > 0)
            {
                ReportExportType reportExportType = ReportExportType.PDF;

                var pdfStream = DoReport(Ids, "LTA4LabelByIds_3_0", reportExportType, position, showSender);

                string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

                return File(pdfStream, mimeStr, "LTReport.PDF");
            }
            else
            {
                return Content("沒選擇託運單");
            }
        }

        public IActionResult PrintMutiTagPDF2SizeByIds(string Ids)
        {
            if (Ids != null && Ids.Length > 0)
            {
                ReportExportType reportExportType = ReportExportType.PDF;

                var pdfStream = DoReport(Ids, "LTReport1_3ByIds", reportExportType);

                string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

                return File(pdfStream, mimeStr, "LTReport1_3.PDF");
            }
            else
            {
                return Content("沒選擇託運單");
            }
        }

        public IActionResult PrintMutiTagPDFForSBByIds(string Ids)
        {
            if (Ids != null && Ids.Length > 0)
            {
                ReportExportType reportExportType = ReportExportType.PDF;

                var pdfStream = DoReport(Ids, "LTReelLabel_SBByIds", reportExportType);

                string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

                return File(pdfStream, mimeStr, "LTReelLabelSB.PDF");
            }
            else
            {
                return Content("沒選擇託運單");
            }
        }

        public IActionResult PrintReturnTagByIds(string Ids)
        {
            try
            {
                if (Ids != null && Ids.Length > 0)
                {
                    ReportExportType reportExportType = ReportExportType.PDF;

                    var pdfStream = DoReport(Ids, "Report2", reportExportType);

                    string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

                    return File(pdfStream, mimeStr, "Report2.PDF");
                }
                else
                {
                    return Content("沒選擇託運單");
                }
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }
        }

        public IActionResult PrintReturnTagByIds2_0(string Ids)
        {
            try
            {
                if (Ids != null && Ids.Length > 0)
                {
                    ReportExportType reportExportType = ReportExportType.PDF;

                    var pdfStream = DoReport(Ids, "ReturnForm_2_0", reportExportType);

                    string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

                    return File(pdfStream, mimeStr, "Report2.PDF");
                }
                else
                {
                    return Content("沒選擇託運單");
                }
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }
        }

        public IActionResult PrintPalletSignReceiptTagByIds(string ids)
        {
            try
            {

                if (ids != null && ids.Length > 0)
                {
                    ReportExportType reportExportType = ReportExportType.PDF;

                    var pdfStream = DoReport(ids, "PalletSignReceipt", reportExportType);

                    string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

                    return File(pdfStream, mimeStr, "棧板配送簽單.PDF");
                }
                else
                {
                    return Content("沒選擇簽收單");
                }
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }
        }

        public IActionResult PrintDeliverySignReceiptTagByCheckNumbers(string ids)
        {
            try
            {

                if (ids != null && ids.Length > 0)
                {
                    ReportExportType reportExportType = ReportExportType.PDF;

                    var pdfStream = DoReport(ids, "DeliverySignReceipt", reportExportType);

                    string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

                    return File(pdfStream, mimeStr, "零擔配送簽單.PDF");
                }
                else
                {
                    return Content("沒選擇簽收單");
                }
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }
        }

        private Stream DoReportWithPosition(string Ids, string reportName, ReportExportType reportExportType, string position = "-1")
        {
            string[] strIds = Ids.Split(',');

            List<string> transIds = new List<string>();

            SetMdSdData(Ids);

            foreach (string eachId in strIds)
            {
                try
                {
                    transIds.Add(eachId);
                }
                catch
                { }
            }

            Dictionary<string, string[]> reportParams;
            if (position == "-1")
            {
                reportParams = new Dictionary<string, string[]>()
                {
                    ["request_ids"] = transIds.ToArray()
                };
            }
            else
            {
                reportParams = new Dictionary<string, string[]>()
                {
                    ["request_ids"] = transIds.ToArray(),
                    ["position"] = new string[1] { position }
                };
            }

            var pdfStream = this.ReportPrintService.GetNomalReport(reportParams, reportName, reportExportType);

            return pdfStream;
        }

        private Stream DoReport(string Ids, string reportName, ReportExportType reportExportType, string position = "-1", bool showSender = true, string infoKey = "")
        {
            string[] strIds = Ids.Split(',');

            List<string> transIds = new List<string>();

            SetMdSdData(Ids);

            foreach (string eachId in strIds)
            {
                try
                {
                    transIds.Add(eachId);
                }
                catch
                { }
            }

            Dictionary<string, string[]> reportParams;

            if (showSender && position == "-1")
            {
                reportParams = new Dictionary<string, string[]>()
                {
                    ["request_ids"] = transIds.ToArray(),
                };
            }
            else if (showSender && position != "-1")
            {
                reportParams = new Dictionary<string, string[]>()
                {
                    ["request_ids"] = transIds.ToArray(),
                    ["position"] = new string[1] { position }
                };

            }
            else if (!showSender && position == "-1")
            {
                reportParams = new Dictionary<string, string[]>()
                {
                    ["request_ids"] = transIds.ToArray(),
                    ["show_sender"] = new string[1] { showSender.ToString() }
                };
            }
            else
            {
                reportParams = new Dictionary<string, string[]>()
                {
                    ["request_ids"] = transIds.ToArray(),
                    ["position"] = new string[1] { position },
                    ["show_sender"] = new string[1] { showSender.ToString() }
                };
            }

            if (infoKey.Length > 0)
            {
                string[] keyValue = { infoKey };
                reportParams.Add("infoKey", keyValue);
            }

            var pdfStream = this.ReportPrintService.GetNomalReport(reportParams, reportName, reportExportType);

            return pdfStream;
        }

        [Authorize]
        public IActionResult AccountsReceivable()
        {

            return View();
        }

        public IActionResult TestSaveMD(string Ids)
        {
            if (Ids != null && Ids.Length > 0)
            {
                SetMdSdData(Ids);

                return Content("成功");
            }
            else
            {
                return Content("沒選擇託運單");
            }
        }

        private void SetMdSdData(string Ids)
        {
            if (Ids != null && Ids.Length > 0)
            {
                string[] strIds = Ids.Split(',');

                List<decimal> transIds = new List<decimal>();

                foreach (string eachId in strIds)
                {
                    try
                    {
                        decimal eachNum = Convert.ToInt64(eachId);

                        transIds.Add(eachNum);
                    }
                    catch
                    { }
                }

                this.DeliveryRequestService.SaveDeliveryRequestMDSD(transIds.ToArray());
            }
        }

        public IActionResult PrintConsignmentSummaryPDFByIds(string ids)
        {
            try
            {

                if (ids != null && ids.Length > 0)
                {
                    ReportExportType reportExportType = ReportExportType.PDF;

                    var pdfStream = DoReport(ids, "FSEConsignmentSummary", reportExportType);

                    string mimeStr = this.ReportPrintService.GetReportTypeMimeString(reportExportType);

                    return File(pdfStream, mimeStr, "FSE託運總表.PDF");
                }
                else
                {
                    return Content("沒選擇明細貨號");
                }
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }
        }
    }
}