﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ExcelDataReader;
using LightYear.JunFuTrans.BL.BE.GuoYangExportAndImport;
using LightYear.JunFuTrans.BL.Services.GuoYangExportAndImport;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Configuration;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class GuoYangExportAndImportController : Controller
    {
        IGuoYangExportAndImportService GuoYangExportAndImportService;
        private readonly IConfiguration config;

        public GuoYangExportAndImportController(IGuoYangExportAndImportService guoYangExportAndImportService, IConfiguration config)
        {
            this.GuoYangExportAndImportService = guoYangExportAndImportService;
            this.config = config;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost, ActionName("Export")]
        public IActionResult Export()
        {
            bool resultCode = true;
            string errorMessage = string.Format("沒有問題");

            try
            {
                var bytes = GuoYangExportAndImportService.Export();
                return File(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "國陽空白貨態單.xlsx");
            }
            catch (Exception e)
            {
                resultCode = false;
                errorMessage = e.Message;
            }
            string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
            return Content(response);
        }

        [HttpPost]
        public async Task<IActionResult> ImportFile(IFormFile file)
        {
            bool resultCode = true;
            string errorMessage = string.Format("沒有問題");

            try
            {
                List<GuoYangExportAndImportEntity> Entities = new List<GuoYangExportAndImportEntity>();
                TempEntity tempEntity = new TempEntity();
                string fullPath = string.Empty;

                if (file.Length > 0)
                {
                    var savePath = this.config.GetValue<string>("UploadFileUrl");
                    string uniqueFileName = GuoYangExportAndImportService.GetUniqueFileName(file.FileName);
                    fullPath = string.Format("{0}/{1}", savePath, uniqueFileName);
                    using (FileStream stream = new FileStream(fullPath, FileMode.Append))
                    {
                        await file.CopyToAsync(stream);
                    }

                    System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
                    using (var stream = System.IO.File.Open(fullPath, FileMode.Open, FileAccess.Read))
                    {
                        using (var reader = ExcelReaderFactory.CreateReader(stream))
                        {
                            var result = reader.AsDataSet();

                            var rows = result.Tables[0].Rows.Count;//獲取總行數
                            //var columns = result.Tables[0].Columns.Count;//獲取總列數

                            if (rows > 1) //除了標題行外有資料才讀取
                            {
                                for (int i = 1; i < rows; i++) // 避開標題行，從第二行開始讀取
                                {
                                    GuoYangExportAndImportEntity Entity = new GuoYangExportAndImportEntity(); // i每加1，就new一個Entity
                                    int resultint;
                                    Entity.CheckNumber = result.Tables[0].Rows[i][0].ToString();
                                    Entity.Pieces = int.TryParse(result.Tables[0].Rows[i][1].ToString(), out resultint) ? resultint : 0; //int.TryParse 回傳bool值
                                    Entity.DriverCode = result.Tables[0].Rows[i][2].ToString().Substring(0, 5);
                                    Entity.ScanDate = Convert.ToDateTime(result.Tables[0].Rows[i][3].ToString());
                                    Entities.Add(Entity);
                                }
                            }
                            else
                            {
                                return Content("沒有資料無法上傳");
                            }

                            //while (reader.Read()) //Each row of the file //時好時壞的寫法
                            //{
                            //    if (reader.Depth != 0)
                            //    {
                            //        GuoYangExportAndImportEntity Entity = new GuoYangExportAndImportEntity();
                            //        Entity.CheckNumber = reader.GetValue(0).ToString();
                            //        Entity.Pieces = Convert.ToInt32(reader.GetValue(1));
                            //        Entity.DriverCode = reader.GetString(2).Substring(0, 5);
                            //        Entity.ScanDate = Convert.ToDateTime(reader.GetValue(3));
                            //        //Entity.ArriveOption = reader.GetString(7);
                            //        Entities.Add(Entity);
                            //    }
                            //    else //(reader.Depth == 0) //存放標題並忽略標題值
                            //    {
                            //        tempEntity.CheckNumber = reader.GetValue(0).ToString();
                            //        tempEntity.Pieces = reader.GetValue(1).ToString();
                            //        tempEntity.DriverCode = reader.GetValue(2).ToString();
                            //        tempEntity.ScanDate = reader.GetValue(3).ToString();
                            //        tempEntity.ArriveOption = reader.GetValue(4).ToString();
                            //    }
                            //}
                        }
                    }
                }

                JunFuWebServiceInService.WebServiceSoapClient webServiceSoapClient = new JunFuWebServiceInService.WebServiceSoapClient(JunFuWebServiceInService.WebServiceSoapClient.EndpointConfiguration.WebServiceSoap);

                for (int i = 0; i < Entities.Count; i++)
                {
                    await webServiceSoapClient.InsertDeliveryScanLogFromGuoYangAsync(Entities[i].Pieces, Entities[i].ScanDate, Entities[i].DriverCode, Entities[i].CheckNumber);
                }

                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }

                return Ok("上傳成功");
            }
            catch (Exception e)
            {
                resultCode = false;
                errorMessage = e.Message;
            }
            string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
            return Content(response);
        }

        public IActionResult GetByDateAndStationScode(DateTime start, DateTime end, string scode, string callback ="callback")
        {
            List<FrontendEntity> Entities = this.GuoYangExportAndImportService.GetByDateAndStationScode(start, end, scode);

            string data = JsonConvert.SerializeObject(Entities);

            string output = string.Format("{0}({1})", callback, data);

            return Content(output);
        }
    }
}