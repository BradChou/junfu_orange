﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ExcelDataReader;
using LightYear.JunFuTrans.BL.Services.PayCustomerPayment;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Configuration;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using LightYear.JunFuTrans.BL.BE.PayCustomerPayment;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class PayCustomerPaymentController : Controller
    {
        IPayCustomerPaymentService PayCustomerPaymentService;
        private readonly IConfiguration config;

        public PayCustomerPaymentController(IPayCustomerPaymentService payCustomerPaymentService, IConfiguration config)
        {
            this.PayCustomerPaymentService = payCustomerPaymentService;
            this.config = config;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost, ActionName("Export")]
        public IActionResult Export()
        {
            bool resultCode = true;
            string errorMessage = string.Format("沒有問題");

            try
            {
                var bytes = PayCustomerPaymentService.Export();
                return File(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "代收貨款支付明細表.xlsx");
            }
            catch (Exception e)
            {
                resultCode = false;
                errorMessage = e.Message;
            }
            string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
            return Content(response);
        }

        [HttpPost]
        public async Task<IActionResult> ImportFile(IFormFile file)
        {
            bool resultCode = true;
            string errorMessage = "";

            try
            {
                List<DA.JunFuDb.PayCustomerPayment> Entities = new List<DA.JunFuDb.PayCustomerPayment>();
                
                string fullPath = string.Empty;

                if (file.Length > 0)
                {
                    var savePath = this.config.GetValue<string>("UploadFileUrl");
                    string uniqueFileName = PayCustomerPaymentService.GetUniqueFileName(file.FileName);
                    fullPath = string.Format("{0}/{1}", savePath, uniqueFileName);
                    using (FileStream stream = new FileStream(fullPath, FileMode.Append))
                    {
                        await file.CopyToAsync(stream);
                    }

                    System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
                    using (var stream = System.IO.File.Open(fullPath, FileMode.Open, FileAccess.Read))
                    {
                        using (var reader = ExcelReaderFactory.CreateReader(stream))
                        {
                            var result = reader.AsDataSet();

                            var rows = result.Tables[0].Rows.Count;//獲取總行數
                            
                            if (rows > 2) //除了標題、範例列外有資料才讀取
                            {
                                for (int i = 2; i < rows; i++) // 避開標題行，從第3列開始讀取
                                {
                                    DA.JunFuDb.PayCustomerPayment tempEntity = new DA.JunFuDb.PayCustomerPayment();
                                    tempEntity.CheckNumber = result.Tables[0].Rows[i][3].ToString();
                                    tempEntity.IsPaid = result.Tables[0].Rows[i][5].ToString() == "已支付" ? true:false;
                                    tempEntity.CreateDate = DateTime.Now;
                                    Entities.Add(tempEntity);
                                }
                            }
                            else
                            {
                                return Content("沒有資料無法上傳");
                            }

                        }
                    }
                }

                PayCustomerPaymentService.InsertBatch(Entities);
                
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }
            }
            catch (Exception e)
            {
                resultCode = false;
                errorMessage = e.Message;
            }
            TempData["Response"] = string.Concat("上傳", resultCode ? "成功。" : "失敗。", errorMessage);
            return Redirect("Index");
        }

        public IActionResult GetByDateAndStationCodeOrCustomerCode(DateTime start, DateTime end, string station, string customer, string callback ="callback")
        {
            List<FrontendEntity> Entities = this.PayCustomerPaymentService.GetAll(start, end, station, customer);

            string data = JsonConvert.SerializeObject(Entities);

            string output = string.Format("{0}({1})", callback, data);

            return Content(output);
        }
    }
}