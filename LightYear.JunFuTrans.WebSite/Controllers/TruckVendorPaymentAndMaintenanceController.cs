﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using LightYear.JunFuTrans.BL.Services.TruckVendorPaymentAndMaintenance;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.IO;
using LightYear.JunFuTrans.BL.Services.GuoYangExportAndImport;
using ExcelDataReader;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;
using System.Globalization;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance;
using JunFuTrans.DA.JunFuTrans.DA;
using JunFuTrans.DA.JunFuTrans.condition;
using System.Text.RegularExpressions;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class TruckVendorPaymentAndMaintenanceController : Controller
    {
        ITruckVendorPaymentService TruckVendorPaymentService { get; set; }
        private readonly IConfiguration config;
        IGuoYangExportAndImportService GuoYangExportAndImportService { get; set; }

        public ITbItemCodesRepository TbItemCodesRepository { get; private set; }

        public ITruckVendorMaintenanceRepository TruckVendorMaintenanceRepository { get; set; }

        public ITruckVendorPaymentRepository TruckVendorPaymentRepository { get; set; }

        public IMapper Mapper { get; set; }


        public TruckVendorPaymentAndMaintenanceController(ITruckVendorPaymentService truckVendorPaymentService, IConfiguration config,
            IGuoYangExportAndImportService guoYangExportAndImportService, IMapper mapper, ITbItemCodesRepository itemCodesRepository,
            ITruckVendorMaintenanceRepository truckVendorMaintenanceRepository, ITruckVendorPaymentRepository truckVendorPaymentRepository)
        {
            TruckVendorPaymentService = truckVendorPaymentService;
            this.config = config;
            GuoYangExportAndImportService = guoYangExportAndImportService;
            Mapper = mapper;
            TbItemCodesRepository = itemCodesRepository;
            TruckVendorMaintenanceRepository = truckVendorMaintenanceRepository;
            TruckVendorPaymentRepository = truckVendorPaymentRepository;
        }
        public IActionResult Index()
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            ViewBag.UserAccount = userInfoEntity.UserAccount;

            //string truckVendorId = TruckVendorPaymentService.GetTruckVendorByTruckVendorAccountCode(userInfoEntity.UserAccount)?.Id.ToString();
            string truckVendorId = string.Empty;
            if (userInfoEntity.SupplierId != null)
            {
                var supplier = TruckVendorPaymentService.GetSupplierById((int)userInfoEntity.SupplierId);
                if (supplier != null)
                {
                    truckVendorId = supplier.supplier_id.ToString();
                }
            }
            ViewBag.TruckVendorId = truckVendorId ?? "";

            return View();
        }
        /// <summary>
        /// 維持舊版
        /// </summary>
        /// <returns></returns>
        public IActionResult Index_Old()
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            ViewBag.UserAccount = userInfoEntity.UserAccount;

            string truckVendorId = TruckVendorPaymentService.GetTruckVendorByTruckVendorAccountCode(userInfoEntity.UserAccount)?.Id.ToString();

            ViewBag.TruckVendorId = truckVendorId ?? "";

            return View();
        }
        public IActionResult GetSuppliersAccountCode()
        {
            return Json(TruckVendorPaymentService.GetSuppliersAccount().Select(a => a.AccountCode).ToList());
        }

        public IActionResult Upload()
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            if (isSupplierAccount(userInfoEntity))
            {
                TempData["Message"] = "本帳號權限無法進入此頁面。";
                return RedirectToAction("ErrorPage");
            }
            return View();
        }

        public IActionResult GetAllByMonthAndTruckVendor(DateTime month, string vendor, string callback = "callback")
        {
            DateTime start = new DateTime(month.Year, month.Month, 01, 00, 00, 00);

            DateTime end = start.AddMonths(1);

            var Entities = TruckVendorPaymentService.GetAllByMonthAndTruckVendor(start, end, vendor);

            string data = JsonConvert.SerializeObject(Entities);

            string output = string.Format("{0}({1})", callback, data);

            return Content(output);
        }

        public IActionResult GetAllByMonthAndTruckVendor_Old(DateTime month, string vendor, string callback = "callback")
        {
            DateTime start = new DateTime(month.Year, month.Month, 01, 00, 00, 00);

            DateTime end = start.AddMonths(1);

            var Entities = TruckVendorPaymentService.GetAllByMonthAndTruckVendor_Old(start, end, vendor);

            string data = JsonConvert.SerializeObject(Entities);

            string output = string.Format("{0}({1})", callback, data);

            return Content(output);
        }

        public IActionResult GetDisbursement(DateTime month, string vendor, string callback = "callback")
        {
            DateTime start = new DateTime(month.Year, month.Month, 01, 00, 00, 00);

            DateTime end = start.AddMonths(1);

            var Entities = TruckVendorPaymentService.GetDisbursementByMonthAndTruckVendor(start, end, vendor);

            string data = JsonConvert.SerializeObject(Entities);

            string output = string.Format("{0}({1})", callback, data);

            return Content(output);
        }
        public IActionResult GetDisbursement_Old(DateTime month, string vendor, string callback = "callback")
        {
            DateTime start = new DateTime(month.Year, month.Month, 01, 00, 00, 00);

            DateTime end = start.AddMonths(1);

            var Entities = TruckVendorPaymentService.GetDisbursementByMonthAndTruckVendor_Old(start, end, vendor);

            string data = JsonConvert.SerializeObject(Entities);

            string output = string.Format("{0}({1})", callback, data);

            return Content(output);
        }
        public IActionResult Save(string models, string callback = "callback")
        {
            List<TruckVendorPaymentLog> truckVendorPaymentLog = JsonConvert.DeserializeObject<List<TruckVendorPaymentLog>>(models);

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            for (int i = 0; i < truckVendorPaymentLog.Count; i++)
            {
                if (truckVendorPaymentLog[i].Id == 0)
                {
                    truckVendorPaymentLog[i].CreateUser = userInfoEntity.UserAccount;
                }
                else
                {
                    truckVendorPaymentLog[i].UpdateUser = userInfoEntity.UserAccount;
                }
            }

            var ids = TruckVendorPaymentService.Save(truckVendorPaymentLog);

            for (int i = 0; i < ids.Count; i++)
            {
                truckVendorPaymentLog[i].Id = ids[i];
                truckVendorPaymentLog[i].VendorName = TruckVendorPaymentService.GetTruckVendors().Where(a => a.Id.ToString().Equals(truckVendorPaymentLog[i].VendorName)).FirstOrDefault()?.VendorName;
                truckVendorPaymentLog[i].Company = TruckVendorPaymentService.GetCompanies().Where(a => a.Id.ToString().Equals(truckVendorPaymentLog[i].Company)).FirstOrDefault()?.Name;
                truckVendorPaymentLog[i].FeeType = TruckVendorPaymentService.GetFeeTypes().Where(a => a.Id.ToString().Equals(truckVendorPaymentLog[i].FeeType)).FirstOrDefault()?.Name;
                truckVendorPaymentLog[i].RegisteredDate = truckVendorPaymentLog[i].RegisteredDate.Value.AddHours(-8);
            }

            models = JsonConvert.SerializeObject(truckVendorPaymentLog);

            string output = string.Format("{0}({1})", callback, models);
            return Content(output);
        }

        public IActionResult Update(string models, string callback = "callback")
        {
            List<TruckVendorPaymentLog> truckVendorPaymentLog = JsonConvert.DeserializeObject<List<TruckVendorPaymentLog>>(models);

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            truckVendorPaymentLog[0].UpdateUser = userInfoEntity.UserAccount;

            TruckVendorPaymentService.Update(truckVendorPaymentLog[0]);

            string output = string.Format("{0}({1})", callback, models);

            return Content(output);
        }

        public IActionResult GetAllTruckVendors()
        {
            //return Json(TruckVendorPaymentService.GetTruckVendors());
            return Json(TruckVendorPaymentService.GetSupplier());
        }

        public IActionResult GetAllTruckVendors_Old()
        {
            return Json(TruckVendorPaymentService.GetTruckVendors());
        }

        public IActionResult GetFeeTypes()
        {
            return Json(TruckVendorPaymentService.GetFeeTypes());
        }

        public IActionResult GetCompanies()
        {
            return Json(TruckVendorPaymentService.GetCompanies());
        }
        public IActionResult GetDepartments()
        {
            return Json(TruckVendorPaymentService.GetDepartments());
        }

        public IActionResult GetIncomeAndExpenditure(DateTime month, string vendor)
        {
            DateTime start = new DateTime(month.Year, month.Month, 01, 00, 00, 00);

            DateTime end = start.AddMonths(1);

            var Income = TruckVendorPaymentService.GetIncomeAndExpenditure(start, end, vendor);

            string data = JsonConvert.SerializeObject(Income);

            return Json(data);
        }
        public IActionResult GetIncomeAndExpenditure_Old(DateTime month, string vendor)
        {
            DateTime start = new DateTime(month.Year, month.Month, 01, 00, 00, 00);

            DateTime end = start.AddMonths(1);

            var Income = TruckVendorPaymentService.GetIncomeAndExpenditure_Old(start, end, vendor);

            string data = JsonConvert.SerializeObject(Income);

            return Json(data);
        }
        /// <summary>
        /// 車老闆上傳明細
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> ImportFile(IFormFile file)
        {
            bool resultCode = true;
            var resultCount = 0;
            var resultTotalPayment = 0;
            var resultTotalCost = 0;
            var resultSuccess = string.Empty;
            string errorMessage = "";
            List<string> errorMessages = new List<string>();
            List<bool> resultCodeList = new List<bool>();

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            if (isSupplierAccount(userInfoEntity))
            {
                TempData["Message"] = "本帳號權限無法進入此頁面。";
                return RedirectToAction("ErrorPage");
            }

            string importRandomCode = Guid.NewGuid().ToString().Substring(0, 10);
            TempData["importRandomCode"] = importRandomCode;

            var truckVendors = TruckVendorPaymentService.GetTruckVendors();

            try
            {
                List<TruckVendorPaymentLog> Entities = new List<TruckVendorPaymentLog>();
                string fullPath = string.Empty;

                if (file.Length > 0)
                {
                    var savePath = this.config.GetValue<string>("UploadFileUrl");
                    string uniqueFileName = GuoYangExportAndImportService.GetUniqueFileName(file.FileName);
                    fullPath = string.Format("{0}/{1}", savePath, uniqueFileName);
                    using (FileStream stream = new FileStream(fullPath, FileMode.Append))
                    {
                        await file.CopyToAsync(stream);
                    }

                    using (var stream = System.IO.File.Open(fullPath, FileMode.Open, FileAccess.Read))
                    {
                        using (var reader = ExcelReaderFactory.CreateReader(stream))
                        {
                            var result = reader.AsDataSet();

                            var rows = result.Tables[0].Rows.Count;//獲取總列數

                            if (rows > 2) //除了標題列與範例列外有資料才讀取
                            {
                                for (int i = 2; i < rows; i++) // 避開標題與範例列，從第三列開始讀取
                                {
                                    TruckVendorPaymentLog Entity = new TruckVendorPaymentLog(); // i每加1，就new一個Entity

                                    decimal resultint;
                                    DateTime tmpDate;
                                    if (result.Tables[0].Columns.Count < 17)
                                    {
                                        resultCode = false;
                                        resultCodeList.Add(resultCode);
                                        errorMessage = "上傳檔案版本不符，請下載新版Excel填寫資料後重新上傳。";
                                        errorMessages.Add(errorMessage);
                                    }
                                    if (String.IsNullOrEmpty(result.Tables[0].Rows[i][0].ToString()))
                                    {
                                        resultCode = false;
                                        resultCodeList.Add(resultCode);
                                        errorMessage = "第" + (i - 1 + 2).ToString() + "列發生日期未填，請更正後重新上傳。";
                                        errorMessages.Add(errorMessage);
                                    }

                                    if (DateTime.TryParse(result.Tables[0].Rows[i][0].ToString(), out tmpDate) == true)
                                    {
                                        resultCode = true;
                                        resultCodeList.Add(resultCode);
                                    }
                                    else
                                    {
                                        resultCode = false;
                                        resultCodeList.Add(resultCode);
                                        errorMessage = "第" + (i - 1 + 2).ToString() + "列發生日期格式有誤，請更正後重新上傳。";
                                        errorMessages.Add(errorMessage);
                                    }

                                    Entity.RegisteredDate = Convert.ToDateTime(result.Tables[0].Rows[i][0].ToString());

                                    Entity.FeeType = result.Tables[0].Rows[i][1].ToString();
                                    if (String.IsNullOrEmpty(Entity.FeeType))
                                    {
                                        resultCode = false;
                                        resultCodeList.Add(resultCode);
                                        errorMessage = "第" + (i - 1 + 2).ToString() + "列交易項目未填，請更正後重新上傳。";
                                        errorMessages.Add(errorMessage);
                                    }

                                    var feeTypes = TruckVendorPaymentService.GetFeeTypes();

                                    var id = feeTypes.Where(a => a.Name == Entity.FeeType).FirstOrDefault()?.Id;

                                    if (id == null)
                                    {
                                        resultCode = false;
                                        resultCodeList.Add(resultCode);
                                        errorMessage = "第" + (i - 1 + 2).ToString() + "列交易項目有誤，請更正後重新上傳。";
                                        errorMessages.Add(errorMessage);
                                    }

                                    Entity.Payment = Convert.ToInt32(Math.Round(Decimal.TryParse(result.Tables[0].Rows[i][4].ToString(), out resultint) ? resultint : 0));
                                    Entity.Cost = Convert.ToInt32(Math.Round(Decimal.TryParse(result.Tables[0].Rows[i][7].ToString(), out resultint) ? resultint : 0));

                                    if (Entity.Payment == 0 && Entity.Cost == 0)
                                    {
                                        resultCode = false;
                                        resultCodeList.Add(resultCode);
                                        errorMessage = "第" + (i - 1 + 2).ToString() + "資料錯誤，收入與支出至少需有一欄值不為0。";
                                        errorMessages.Add(errorMessage);
                                    }

                                    Entity.Spread = Convert.ToInt32(Math.Round(Decimal.TryParse(result.Tables[0].Rows[i][8].ToString(), out resultint) ? resultint : 0));

                                    if (Entity.Spread < 0)
                                    {
                                        resultCode = false;
                                        resultCodeList.Add(resultCode);
                                        errorMessage = "第" + (i - 1 + 2).ToString() + "價差為負數，請修正收入與支出再重新上傳。";
                                        errorMessages.Add(errorMessage);
                                    }
                                    //if (String.IsNullOrEmpty(Entity.VendorName) && String.IsNullOrEmpty(Entity.Payable))
                                    //{
                                    //    resultCode = false;
                                    //    resultCodeList.Add(resultCode);
                                    //    errorMessage = "第" + (i - 1 + 2).ToString() + "資料錯誤，應收與應付對象至少需填一欄。";
                                    //    errorMessages.Add(errorMessage);
                                    //}

                                    Entity.VendorName = result.Tables[0].Rows[i][16].ToString();
                                    Entity.Payable = result.Tables[0].Rows[i][17].ToString();
                                    var vendorUID = result.Tables[0].Rows[i][3].ToString();
                                    var payableUID = result.Tables[0].Rows[i][6].ToString();
                                    if (Entity.Payment != 0)
                                    {
                                        if (String.IsNullOrEmpty(Entity.VendorName))
                                        {
                                            resultCode = false;
                                            resultCodeList.Add(resultCode);
                                            errorMessage = "第" + (i - 1 + 2).ToString() + "列應收對象未填，請更正後重新上傳。";
                                            errorMessages.Add(errorMessage);
                                        }
                                        if (String.IsNullOrEmpty(vendorUID))
                                        {
                                            resultCode = false;
                                            resultCodeList.Add(resultCode);
                                            errorMessage = "第" + (i - 1 + 2).ToString() + "列統一編號/身份證字號未填，請更新應收對象資料後重新上傳。";
                                            errorMessages.Add(errorMessage);
                                        }
                                        if (!TruckVendorPaymentService.HasSupplier(Entity.VendorName))
                                        {
                                            resultCode = false;
                                            resultCodeList.Add(resultCode);
                                            errorMessage = "第" + (i - 1 + 2).ToString() + "列應收對象錯誤，請更新基本資料後重新填寫並上傳。";
                                            errorMessages.Add(errorMessage);
                                        }
                                    }
                                    if (Entity.Cost != 0)
                                    {
                                        if (String.IsNullOrEmpty(Entity.Payable))
                                        {
                                            resultCode = false;
                                            resultCodeList.Add(resultCode);
                                            errorMessage = "第" + (i - 1 + 2).ToString() + "列應付對象未填，請更正後重新上傳。";
                                            errorMessages.Add(errorMessage);
                                        }
                                        if (String.IsNullOrEmpty(payableUID))
                                        {
                                            resultCode = false;
                                            resultCodeList.Add(resultCode);
                                            errorMessage = "第" + (i - 1 + 2).ToString() + "列統一編號/身份證字號未填，請更新應付對象資料後重新上傳。";
                                            errorMessages.Add(errorMessage);
                                        }
                                        if (!TruckVendorPaymentService.HasSupplier(Entity.Payable))
                                        {
                                            resultCode = false;
                                            resultCodeList.Add(resultCode);
                                            errorMessage = "第" + (i - 1 + 2).ToString() + "列應付對象錯誤，請更新基本資料後重新填寫並上傳。";
                                            errorMessages.Add(errorMessage);
                                        }
                                    }

                                    Entity.VendorName = result.Tables[0].Rows[i][16].ToString();

                                    Entity.InvoiceNumber = result.Tables[0].Rows[i][9].ToString().ToUpper();
                                    //發票正規表達
                                    var invoicePattern = "[a-zA-Z]{2}[0-9]{8}";
                                    if (!String.IsNullOrEmpty(Entity.InvoiceNumber) && !Regex.Match(Entity.InvoiceNumber, invoicePattern).Success)
                                    {
                                        resultCode = false;
                                        resultCodeList.Add(resultCode);
                                        errorMessage = "第" + (i - 1 + 2).ToString() + "列發票格式有誤(格式為AA12345678)，請更新發票後重新上傳。";
                                        errorMessages.Add(errorMessage);
                                    }
                                    //  判段依據 feeytpeRelate表中的HasInvoice 且 支出要>0
                                    var hasInvoice = TruckVendorPaymentService.FeetypeHasInvoice(int.Parse(id));
                                    if (String.IsNullOrEmpty(Entity.InvoiceNumber) && hasInvoice && Entity.Cost > 0)
                                    {
                                        resultCode = false;
                                        resultCodeList.Add(resultCode);
                                        errorMessage = "第" + (i - 1 + 2).ToString() + "列發票未填，請更新發票後重新上傳。";
                                        errorMessages.Add(errorMessage);
                                    }

                                    if (!String.IsNullOrEmpty(Entity.InvoiceNumber) && String.IsNullOrEmpty(result.Tables[0].Rows[i][10].ToString()))
                                    {
                                        resultCode = false;
                                        resultCodeList.Add(resultCode);
                                        errorMessage = "第" + (i - 1 + 2).ToString() + "列發生發票日期未填，請更正後重新上傳。";
                                        errorMessages.Add(errorMessage);
                                    }
                                    else if (!String.IsNullOrEmpty(Entity.InvoiceNumber) && !String.IsNullOrEmpty(result.Tables[0].Rows[i][10].ToString()))
                                    {
                                        if (DateTime.TryParse(result.Tables[0].Rows[i][10].ToString(), out tmpDate) == true)
                                        {
                                            if (tmpDate.Year < 1900)
                                            {
                                                resultCode = false;
                                                resultCodeList.Add(resultCode);
                                                errorMessage = "第" + (i - 1 + 2).ToString() + "列發票日期請更正為西元年份後請更正後重新上傳。";
                                                errorMessages.Add(errorMessage);
                                            }
                                            else
                                            {
                                                resultCode = true;
                                                resultCodeList.Add(resultCode);
                                            }
                                        }
                                        else
                                        {
                                            resultCode = false;
                                            resultCodeList.Add(resultCode);
                                            errorMessage = "第" + (i - 1 + 2).ToString() + "列發票日期格式有誤，請更正後重新上傳。";
                                            errorMessages.Add(errorMessage);
                                        }
                                        Entity.InvoiceDate = Convert.ToDateTime(result.Tables[0].Rows[i][10].ToString());
                                    }


                                    Entity.CarLicense = result.Tables[0].Rows[i][11].ToString() ?? "";

                                    //if (TruckVendorPaymentService.FeetypeIsPayable(Entity.FeeType))
                                    //{
                                    //    if (String.IsNullOrEmpty(Entity.Payable))
                                    //    {
                                    //        resultCode = false;
                                    //        resultCodeList.Add(resultCode);
                                    //        errorMessage = "第" + (i - 1 + 2).ToString() + "應付對象未填，請更正後重新上傳。";
                                    //        errorMessages.Add(errorMessage);
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    if (!String.IsNullOrEmpty(Entity.Payable))
                                    //    {
                                    //        resultCode = false;
                                    //        resultCodeList.Add(resultCode);
                                    //        errorMessage = "第" + (i - 1 + 2).ToString() + "應付對象不需填寫，請更正後重新上傳。";
                                    //        errorMessages.Add(errorMessage);
                                    //    }
                                    //}

                                    Entity.Memo = result.Tables[0].Rows[i][12].ToString();
                                    if (String.IsNullOrEmpty(Entity.Memo))
                                    {
                                        resultCode = false;
                                        resultCodeList.Add(resultCode);
                                        errorMessage = "第" + (i - 1 + 2).ToString() + "列摘要未填，請更正後重新上傳。";
                                        errorMessages.Add(errorMessage);
                                    }
                                    Entity.Department = result.Tables[0].Rows[i][14].ToString();
                                    if (String.IsNullOrEmpty(Entity.Department))
                                    {
                                        resultCode = false;
                                        resultCodeList.Add(resultCode);
                                        errorMessage = "第" + (i - 1 + 2).ToString() + "列部門未填，請更正後重新上傳。";
                                        errorMessages.Add(errorMessage);
                                    }
                                    if (!TruckVendorPaymentService.HasDepartments(Entity.Department))
                                    {
                                        resultCode = false;
                                        resultCodeList.Add(resultCode);
                                        errorMessage = "第" + (i - 1 + 2).ToString() + "列部門錯誤，請更新基本資料後重新填寫並上傳。";
                                        errorMessages.Add(errorMessage);
                                    }
                                    Entity.Company = result.Tables[0].Rows[i][15].ToString();
                                    if (String.IsNullOrEmpty(Entity.Company))
                                    {
                                        resultCode = false;
                                        resultCodeList.Add(resultCode);
                                        errorMessage = "第" + (i - 1 + 2).ToString() + "列公司別(車行)未填，請更正後重新上傳。";
                                        errorMessages.Add(errorMessage);
                                    }
                                    if (!TruckVendorPaymentService.HasCompanies(Entity.Company))
                                    {
                                        resultCode = false;
                                        resultCodeList.Add(resultCode);
                                        errorMessage = "第" + (i - 1 + 2).ToString() + "列公司別(車行)錯誤，請更新基本資料後重新填寫並上傳。";
                                        errorMessages.Add(errorMessage);
                                    }
                                    Entity.CreateUser = userInfoEntity.UserAccount;
                                    Entity.ImportRandomCode = importRandomCode;
                                    Entity.IsWen = true;
                                    Entity.IsActive = true;
                                    Entity.IsChecked = false;

                                    if (resultCode == true)
                                    {
                                        Entities.Add(Entity);
                                        resultCount++;
                                        resultTotalPayment += (int)Entity.Payment;
                                        resultTotalCost += (int)Entity.Cost;
                                    }
                                }
                            }
                            else
                            {
                                return Content("沒有資料無法上傳");
                            }
                        }
                    }
                }

                if (!resultCodeList.Contains(false))
                    TruckVendorPaymentService.Save(Entities);

                if (System.IO.File.Exists(fullPath))
                    System.IO.File.Delete(fullPath);
                //return View();
            }
            catch (Exception e)
            {
                resultCode = false;
                errorMessages.Add(e.Message);
            }
            resultSuccess = $"成功，共 {resultCount} 筆，收入未稅共 {resultTotalPayment} 元，支出未稅共 {resultTotalCost} 元。";
            TempData["Response"] = string.Concat("上傳", resultCodeList.Contains(false) ? "失敗。" : resultSuccess, string.Join("", errorMessages));

            return RedirectToAction("Upload");
        }

        [HttpPost, ActionName("Export")]
        public IActionResult Export()
        {
            bool resultCode = true;
            string errorMessage = string.Format("沒有問題");

            try
            {
                var bytes = TruckVendorPaymentService.Export();
                return File(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "供應商空白檔案.xlsx");
            }
            catch (Exception e)
            {
                resultCode = false;
                errorMessage = e.Message;
            }
            string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
            return Content(response);
        }

        public async Task<IActionResult> ExportFileByUrl()
        {
            bool resultCode = true;
            string errorMessage = string.Format("沒有問題");

            try
            {
                var path = Path.Combine(
                     Directory.GetCurrentDirectory(), "DownloadFiles",
                      "供應商空白檔案_第五版.xlsx");

                var memory = new MemoryStream();
                using (var stream = new FileStream(path, FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;
                return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "供應商空白檔案_第五版.xlsx");
            }
            catch (Exception e)
            {
                resultCode = false;
                errorMessage = e.Message;
            }
            string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
            return Content(response);

        }

        #region 維護
        public IActionResult Maintenance()
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            if (isSupplierAccount(userInfoEntity))
            {
                TempData["Message"] = "本帳號權限無法進入此頁面。";
                return RedirectToAction("ErrorPage");
            }
            return View();
        }

        public async Task<IActionResult> SaveExpenditureFeeType(string models, string callback = "callback")
        {
            List<FrontEndShowEntity> entities = JsonConvert.DeserializeObject<List<FrontEndShowEntity>>(models);

            switch (entities[0].DutyDept)
            {
                case "業務部":
                    entities[0].DutyDept = "1";
                    break;
                case "財務處":
                    entities[0].DutyDept = "2";
                    break;
                case "車管部":
                    entities[0].DutyDept = "3";
                    break;
                case "行政部":
                    entities[0].DutyDept = "4";
                    break;
                case "分析部":
                    entities[0].DutyDept = "5";
                    break;
                default:
                    entities[0].DutyDept = "0";
                    break;
            }

            await TruckVendorPaymentService.SaveExpenditureFeeType(entities[0]);

            foreach (var e in entities)
            {
                e.DutyDept = TruckVendorMaintenanceRepository.GetDeptNameByCodeId(e.DutyDept);
            }

            string output = string.Format("{0}({1})", callback, JsonConvert.SerializeObject(entities));

            return Content(output);
        }

        public async Task<IActionResult> SaveIncomeFeeType(string models, string callback = "callback")
        {
            List<FrontEndShowEntity> entities = JsonConvert.DeserializeObject<List<FrontEndShowEntity>>(models);

            switch (entities[0].DutyDept)
            {
                case "業務部":
                    entities[0].DutyDept = "1";
                    break;
                case "財務處":
                    entities[0].DutyDept = "2";
                    break;
                case "車管部":
                    entities[0].DutyDept = "3";
                    break;
                case "行政部":
                    entities[0].DutyDept = "4";
                    break;
                case "分析部":
                    entities[0].DutyDept = "5";
                    break;
                default:
                    entities[0].DutyDept = "0";
                    break;
            }
            await TruckVendorPaymentService.SaveIncomeFeeType(entities[0]);

            foreach (var e in entities)
            {
                e.DutyDept = TruckVendorMaintenanceRepository.GetDeptNameByCodeId(e.DutyDept);
            }

            string output = string.Format("{0}({1})", callback, JsonConvert.SerializeObject(entities));

            return Content(output);
        }

        public IActionResult GetIncomeFeeType(string callback = "callback")
        {
            var vendor = TruckVendorPaymentService.GetIncomeFeeType();

            string data = JsonConvert.SerializeObject(vendor);

            string output = string.Format("{0}({1})", callback, data);

            return Content(output);
        }

        public IActionResult GetExpenditureFeeType(string callback = "callback")
        {
            var vendor = TruckVendorPaymentService.GetExpenditureFeeType();

            string data = JsonConvert.SerializeObject(vendor);

            string output = string.Format("{0}({1})", callback, data);

            return Content(output);
        }
        #endregion

        public IActionResult GetTruckVendorForMainten(string callback = "callback")
        {
            var vendor = TruckVendorPaymentService.GetTruckVendorsForMaintenance();

            string data = JsonConvert.SerializeObject(vendor);

            string output = string.Format("{0}({1})", callback, data);

            return Content(output);
        }

        public IActionResult SaveTruckVendor(string models, string callback = "callback")
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            List<TruckVendor> truckVendor = JsonConvert.DeserializeObject<List<TruckVendor>>(models);

            if (truckVendor[0].Id == 0)
            {
                truckVendor[0].CreateUser = userInfoEntity.UserAccount;
                TruckVendorPaymentService.SaveTruckVendor(truckVendor[0]);
            }
            else
            {
                truckVendor[0].UpdateUser = userInfoEntity.UserAccount;
                TruckVendorPaymentService.SaveTruckVendor(truckVendor[0]);
            }

            string output = string.Format("{0}({1})", callback, models);

            return Content(output);
        }

        public IActionResult GetCompanyForMainten(string callback = "callback")
        {
            var vendor = TruckVendorPaymentService.GetCompanyTbItemCodes();

            string data = JsonConvert.SerializeObject(vendor);

            string output = string.Format("{0}({1})", callback, data);

            return Content(output);
        }

        public async Task<IActionResult> SaveCompany(string models, string callback = "callback")
        {
            List<TbItemCode> tbItemCodes = JsonConvert.DeserializeObject<List<TbItemCode>>(models);

            //JunFuWebServiceInService.WebServiceSoapClient webServiceSoapClient = new JunFuWebServiceInService.WebServiceSoapClient(JunFuWebServiceInService.WebServiceSoapClient.EndpointConfiguration.WebServiceSoap);

            //await webServiceSoapClient.SaveCompanyAsync(tbItemCodes[0].CodeName.Trim(), tbItemCodes[0].ActiveFlag ?? false, tbItemCodes[0].Seq);

            if (tbItemCodes != null && tbItemCodes.Count() > 0)
            {
                tbItemCodes_DAL _tbItemCodes_DAL = new tbItemCodes_DAL();

                string CodeName = tbItemCodes[0].CodeName.Trim();
                bool ActiveFlag = tbItemCodes[0].ActiveFlag ?? false;
                decimal Seq = tbItemCodes[0].Seq;

                if (Seq == 0)
                {
                    await _tbItemCodes_DAL.InsertCompany(CodeName);
                }
                else
                {
                    tbItemCodes_Condition _tbItemCodes_Condition = new tbItemCodes_Condition();
                    _tbItemCodes_Condition.code_name = CodeName;
                    _tbItemCodes_Condition.active_flag = ActiveFlag;
                    _tbItemCodes_Condition.seq = Seq;
                    await _tbItemCodes_DAL.UpdatetbItemCodes(_tbItemCodes_Condition);

                }

            }

            string output = string.Format("{0}({1})", callback, models);

            return Content(output);
        }

        public IActionResult GetLatestUploadData(string ImportRandomCode = "", string callback = "callback")
        {
            var data = TruckVendorPaymentService.GetLatestUploadData(ImportRandomCode);

            string entity = JsonConvert.SerializeObject(data);

            string output = string.Format("{0}({1})", callback, entity);

            return Content(output);
        }

        #region 編輯
        [Authorize]
        public IActionResult Edit()
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            string role = userInfoEntity.RoleName.First();
            if (isSupplierAccount(userInfoEntity))
            {
                TempData["Message"] = "本帳號權限無法進入此頁面。";
                return RedirectToAction("ErrorPage");
            }
            int codeId;
            switch (role)
            {
                case "業務部":
                    codeId = 1;
                    break;
                case "財務處":
                    codeId = 2;
                    break;
                case "車管部":
                    codeId = 3;
                    break;
                case "行政部":
                    codeId = 4;
                    break;
                case "分析部":
                    codeId = 5;
                    break;
                default:
                    codeId = 0;
                    break;
            }

            ViewBag.Department = codeId;

            return View();
        }

        public IActionResult GetEditPageFrontEntity(string start, string end, string duty, string createUser, char[] checkAccountCheckedVal, string feeType, string vendor, string payable, string noteSearch, string callback = "callback")
        {
            List<TruckVendorPaymentLogFrontEndShowEntity> data;

            var searchCondition = new TruckVendorPaymentLogSearchEntity()
            {
                startUpload = DateTime.Parse(start),
                endUpload = DateTime.Parse(end),
                duty = duty,
                createUser = createUser,
                noteSearch = noteSearch,
                feeType = feeType,
                vendor = vendor,
                payble = payable
            };

            if (checkAccountCheckedVal.Count() % 2 != 0)
            {
                switch (checkAccountCheckedVal[0])
                {
                    case '0':
                        searchCondition.hasCheckAccount = false;
                        break;
                    case '1':
                        searchCondition.hasCheckAccount = true;
                        break;
                    default:
                        break;
                }
            }
            //if (duty == "*")
            //{
            //    data = TruckVendorPaymentService.GetFrontEndEntityByTimePeriod(DateTime.Parse(start), DateTime.Parse(end).AddDays(1));
            //}
            //else if (start == null || end == null)
            //{
            //    data = TruckVendorPaymentService.GetFrontEndEntityByDuty(duty);
            //}
            //else
            //{
            //    data = TruckVendorPaymentService.GetFrontEndEntityByTimePeriodAndDuty(DateTime.Parse(start), DateTime.Parse(end).AddDays(1), duty);
            //}

            //if (createUser != "*" && createUser != null)
            //{
            //    data = TruckVendorPaymentService.AddCreateUserInEntity(data, createUser);
            //}
            //if (!string.IsNullOrEmpty(noteSearch))
            //{
            //    data = TruckVendorPaymentService.AddNoteSearchInEntity(data, noteSearch);
            //}

            //foreach (var item in data)
            //{
            //    if (item.IsChecked != null)
            //    {
            //        item.StrChecked = (bool)item.IsChecked == true ? "已核帳" : "未核帳";
            //    }
            //    if (item.IsActive != null)
            //    {
            //        item.StrChecked = (bool)item.IsActive == true ? item.StrChecked : item.StrChecked + "(已作廢)";
            //    }
            //}

            data = TruckVendorPaymentService.GetFrontEndEntityBySearchModel(searchCondition);

            string entity = JsonConvert.SerializeObject(data);

            string output = string.Format("{0}({1})", callback, entity);

            return Content(output);
        }

        public IActionResult UpdateVendorPaymentLog(string models, string callback = "callback")
        {
            TruckVendorPaymentLogFrontEndShowEntity truckVendorPaymentEntity = JsonConvert.DeserializeObject<TruckVendorPaymentLogFrontEndShowEntity>(models);

            TruckVendorPaymentLog truckVendorPaymentLog = Mapper.Map<TruckVendorPaymentLog>(truckVendorPaymentEntity);

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            truckVendorPaymentLog.UpdateUser = userInfoEntity.UserAccount;
            truckVendorPaymentLog.UpdateDate = DateTime.Now;

            TruckVendorPaymentService.UpdateVendorLog(truckVendorPaymentLog);

            string output = string.Format("{0}({1})", callback, models);

            return Content(output);
        }

        public IActionResult DeleteVendorPaymentLog(string models, string callback = "callback")
        {
            //TruckVendorPaymentLogFrontEndShowEntity truckVendorPaymentEntity = JsonConvert.DeserializeObject<TruckVendorPaymentLogFrontEndShowEntity>(models);

            //TruckVendorPaymentService.Delete(truckVendorPaymentEntity);

            //string output = string.Format("{0}", callback);

            //return Content(output);
            TruckVendorPaymentLogFrontEndShowEntity truckVendorPaymentEntity = JsonConvert.DeserializeObject<TruckVendorPaymentLogFrontEndShowEntity>(models);

            TruckVendorPaymentLog truckVendorPaymentLog = Mapper.Map<TruckVendorPaymentLog>(truckVendorPaymentEntity);

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            truckVendorPaymentLog.UpdateUser = userInfoEntity.UserAccount;
            truckVendorPaymentLog.UpdateDate = DateTime.Now;

            TruckVendorPaymentService.Delete(truckVendorPaymentLog, userInfoEntity.UserAccount);

            string output = string.Format("{0}({1})", callback, models);

            return Content(output);
        }

        public IActionResult BatchDeleteVendorPaymentLog(string ids)
        {
            string[] aryIds = JsonConvert.DeserializeObject<string[]>(ids);

            int[] intAryIds = Array.ConvertAll(aryIds, s => int.Parse(s));

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            var updateLogs = TruckVendorPaymentRepository.GetByIds(intAryIds);
            try
            {
                TruckVendorPaymentService.DeleteBatch(intAryIds, userInfoEntity.UserAccount);
            }
            catch (Exception e)
            {
                throw;
            }
            return Content("success");
        }
        #endregion

        #region 文中報表編輯
        public IActionResult EditWenERP()
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            string role = userInfoEntity.RoleName.First();
            if (isSupplierAccount(userInfoEntity))
            {
                TempData["Message"] = "本帳號權限無法進入此頁面。";
                return RedirectToAction("ErrorPage");
            }
            int codeId;
            switch (role)
            {
                case "業務部":
                    codeId = 1;
                    break;
                case "財務處":
                    codeId = 2;
                    break;
                case "車管部":
                    codeId = 3;
                    break;
                case "行政部":
                    codeId = 4;
                    break;
                case "分析部":
                    codeId = 5;
                    break;
                default:
                    codeId = 0;
                    break;
            }

            ViewBag.Department = codeId;

            return View();
        }

        //public IActionResult GetEditWenERPPageFrontEntity(string startUpload, string endUpload, string duty, string callback = "callback")
        public IActionResult GetEditWenERPPageFrontEntity(string orderType, string startDateUpload, string endDateUpload, string startDateRegister, string endDateRegister, char[] downloadCheckedVal, string duty, string createUser, string callback = "callback")
        {
            List<TruckVendorPaymentLogWenERPShowEntity> data;
            var searchCondition = new TruckVendorPaymentLogWenERPSearchEntity()
            {
                orderType = orderType,
                downloadCheckedVal = downloadCheckedVal,
                duty = duty,
                createUser = createUser
            };
            if (startDateUpload != null && endDateUpload != null)
            {
                searchCondition.startUpload = DateTime.Parse(startDateUpload);
                searchCondition.endUpload = DateTime.Parse(endDateUpload);
            }
            if (startDateRegister != null && endDateRegister != null)
            {
                searchCondition.startRegister = DateTime.Parse(startDateRegister);
                searchCondition.endRegister = DateTime.Parse(endDateRegister);
            }
            if (downloadCheckedVal.Count() % 2 != 0)
            {
                switch (downloadCheckedVal[0])
                {
                    case '0':
                        searchCondition.hasDownload = false;
                        break;
                    case '1':
                        searchCondition.hasDownload = true;
                        break;
                    default:
                        break;
                }
            }

            data = TruckVendorPaymentService.GetWenERPEntityBySearchModel(searchCondition);
            //if (duty == "*")
            //{
            //    data = TruckVendorPaymentService.GetWenERPEntityByTimePeriod(DateTime.Parse(startUpload), DateTime.Parse(endUpload).AddDays(1));
            //}
            //else if (startUpload == null || endUpload == null)
            //{
            //    data = TruckVendorPaymentService.GetWenERPEntityByDuty(duty);
            //}
            //else
            //{
            //    data = TruckVendorPaymentService.GetWenERPEntityByTimePeriodAndDuty(DateTime.Parse(startUpload), DateTime.Parse(endUpload).AddDays(1), duty);
            //}

            string entity = JsonConvert.SerializeObject(data);

            string output = string.Format("{0}({1})", callback, entity);

            return Content(output);
        }

        public IActionResult ExportMergedExcel(string ids)
        {

            bool resultCode = true;
            string errorMessage = string.Format("沒有問題");
            string[] aryIds = JsonConvert.DeserializeObject<string[]>(ids);

            try
            {
                var bytes = TruckVendorPaymentService.ExportMergedInformation(aryIds);
                return File(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "車老闆文中系統匯入檔.xlsx");
            }
            catch (Exception e)
            {
                resultCode = false;
                errorMessage = e.Message;
            }
            string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
            return Content(response);
        }
        public IActionResult UpdateVendorPaymentLogERP(string models, string callback = "callback")
        {
            TruckVendorPaymentLogFrontEndShowEntity truckVendorPaymentEntity = JsonConvert.DeserializeObject<TruckVendorPaymentLogFrontEndShowEntity>(models);

            TruckVendorPaymentLog truckVendorPaymentLog = Mapper.Map<TruckVendorPaymentLog>(truckVendorPaymentEntity);

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            truckVendorPaymentLog.UpdateUser = userInfoEntity.UserAccount;
            truckVendorPaymentLog.UpdateDate = DateTime.Now;

            TruckVendorPaymentService.UpdateVendorLog(truckVendorPaymentLog);

            string output = string.Format("{0}({1})", callback, models);

            return Content(output);
        }

        public IActionResult WenERPDownloadRecord(string ids)
        {
            string[] aryIds = JsonConvert.DeserializeObject<string[]>(ids);

            int[] intAryIds = Array.ConvertAll(aryIds, s => int.Parse(s));

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            TruckVendorPaymentService.UpdateWenERPDownloadRecord(intAryIds, userInfoEntity.UserAccount);
            return Content("success");
        }
        public IActionResult WenERPCheckAccount(string ids)
        {
            string[] aryIds = JsonConvert.DeserializeObject<string[]>(ids);

            int[] intAryIds = Array.ConvertAll(aryIds, s => int.Parse(s));

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            TruckVendorPaymentService.WenERPCheckAccount(intAryIds, userInfoEntity.UserAccount);
            return Content("success");
        }
        public IActionResult ExportBasicInformation()
        {
            bool resultCode = true;
            string errorMessage = string.Format("沒有問題");

            try
            {
                var bytes = TruckVendorPaymentService.ExportBasicInformation();
                return File(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "基本資料對照表.xlsx");
            }
            catch (Exception e)
            {
                resultCode = false;
                errorMessage = e.Message;
            }
            string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
            return Content(response);
        }

        #endregion

        #region 鼎新報表編輯
        public IActionResult EditDSERPPayable()
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            string role = userInfoEntity.RoleName.First();
            if (isSupplierAccount(userInfoEntity))
            {
                TempData["Message"] = "本帳號權限無法進入此頁面。";
                return RedirectToAction("ErrorPage");
            }
            int codeId;
            switch (role)
            {
                case "業務部":
                    codeId = 1;
                    break;
                case "財務處":
                    codeId = 2;
                    break;
                case "車管部":
                    codeId = 3;
                    break;
                case "行政部":
                    codeId = 4;
                    break;
                case "分析部":
                    codeId = 5;
                    break;
                default:
                    codeId = 0;
                    break;
            }

            ViewBag.Department = codeId;

            return View();
        }
        public IActionResult EditDSERPRecievable()
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            string role = userInfoEntity.RoleName.First();

            int codeId;
            switch (role)
            {
                case "業務部":
                    codeId = 1;
                    break;
                case "財務處":
                    codeId = 2;
                    break;
                case "車管部":
                    codeId = 3;
                    break;
                case "行政部":
                    codeId = 4;
                    break;
                case "分析部":
                    codeId = 5;
                    break;
                default:
                    codeId = 0;
                    break;
            }

            ViewBag.Department = codeId;

            return View();
        }

        //public IActionResult GetEditWenERPPageFrontEntity(string startUpload, string endUpload, string duty, string callback = "callback")
        public IActionResult GetEditDSERPPageFrontEntity(string voucherCode, string company, string startDateUpload, string endDateUpload, string startDateRegister, string endDateRegister, char[] downloadCheckedVal, string duty, string createUser, string feeType, string callback = "callback")
        {
            List<TruckVendorPaymentLogDSERPShowEntity> data;
            var searchCondition = new TruckVendorPaymentLogDSERPSearchEntity()
            {
                voucherCode = voucherCode,
                company = company,
                downloadCheckedVal = downloadCheckedVal,
                duty = duty,
                createUser = createUser,
                feeType = feeType
            };
            if (startDateUpload != null && endDateUpload != null)
            {
                searchCondition.startUpload = DateTime.Parse(startDateUpload);
                searchCondition.endUpload = DateTime.Parse(endDateUpload);
            }
            if (startDateRegister != null && endDateRegister != null)
            {
                searchCondition.startRegister = DateTime.Parse(startDateRegister);
                searchCondition.endRegister = DateTime.Parse(endDateRegister);
            }
            if (downloadCheckedVal.Count() % 2 != 0)
            {
                switch (downloadCheckedVal[0])
                {
                    case '0':
                        searchCondition.hasDownload = false;
                        break;
                    case '1':
                        searchCondition.hasDownload = true;
                        break;
                    default:
                        break;
                }
            }

            data = TruckVendorPaymentService.GetDSERPEntityBySearchModel(searchCondition);

            //if (duty == "*")
            //{
            //    data = TruckVendorPaymentService.GetWenERPEntityByTimePeriod(DateTime.Parse(startUpload), DateTime.Parse(endUpload).AddDays(1));
            //}
            //else if (startUpload == null || endUpload == null)
            //{
            //    data = TruckVendorPaymentService.GetWenERPEntityByDuty(duty);
            //}
            //else
            //{
            //    data = TruckVendorPaymentService.GetWenERPEntityByTimePeriodAndDuty(DateTime.Parse(startUpload), DateTime.Parse(endUpload).AddDays(1), duty);
            //}

            string entity = JsonConvert.SerializeObject(data);

            string output = string.Format("{0}({1})", callback, entity);

            return Content(output);
        }

        public IActionResult DSERPDownloadRecord(string ids)
        {
            string[] aryIds = JsonConvert.DeserializeObject<string[]>(ids);

            int[] intAryIds = Array.ConvertAll(aryIds, s => int.Parse(s));

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            TruckVendorPaymentService.UpdateDSERPDownloadRecord(intAryIds, userInfoEntity.UserAccount);
            return Content("success");
        }

        public IActionResult DSERPMergeDownload(string ids)
        {
            string[] aryIds = JsonConvert.DeserializeObject<string[]>(ids);

            int[] intAryIds = Array.ConvertAll(aryIds, s => int.Parse(s));

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            var downloadId = TruckVendorPaymentService.UpdateDSERPMergeRecord(intAryIds, userInfoEntity.UserAccount);

            bool resultCode = true;
            string errorMessage = string.Format("沒有問題");

            string response = JsonConvert.SerializeObject(new { resultcode = resultCode, downloadId = downloadId, resultdesc = errorMessage });
            return Content(response);
        }

        public IActionResult ExportDSMergedExcel(string downloadId, string fileName)
        {

            bool resultCode = true;
            string errorMessage = string.Format("沒有問題");

            try
            {
                var bytes = TruckVendorPaymentService.ExportDSMergedInformation(downloadId);
                return File(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
            }
            catch (Exception e)
            {
                resultCode = false;
                errorMessage = e.Message;
            }
            string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
            return Content(response);
        }

        #endregion

        bool isSupplierAccount(UserInfoEntity user)
        {
            if (user != null)
            {
                if (user.SupplierId != null)
                {
                    return true;
                }
            }
            return false;
        }
        public IActionResult ErrorPage()
        {
            ViewBag.msg = TempData["Message"];
            return View();
        }
    }
}