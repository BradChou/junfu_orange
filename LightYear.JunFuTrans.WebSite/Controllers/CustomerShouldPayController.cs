﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ExcelDataReader;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.BE.Barcode;
using LightYear.JunFuTrans.BL.BE.CustomerShouldPay;
using LightYear.JunFuTrans.BL.Services.CustomerShouldPay;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.Repositories.CustomerShouldPay;
using LightYear.JunFuTrans.DA.Repositories.Station;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.Operations;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class CustomerShouldPayController : Controller
    {
        public ICustomerShouldPayService CustomerShouldPayService { get; set; }

        public IStationRepository StationRepository { get; set; }

        private readonly IConfiguration config;

        public CustomerShouldPayController(ICustomerShouldPayService customerShouldPayService, IStationRepository stationRepository, IConfiguration config)
        {
            CustomerShouldPayService = customerShouldPayService;
            StationRepository = stationRepository;
            this.config = config;
        }


        #region 客戶應收帳款
        public IActionResult Search()
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            string stationScode = userInfoEntity.Station;

            if (!stationScode.StartsWith("F"))
                ViewBag.Station = StationRepository.GetStationCodeByStationScode(stationScode);
            return View();
        }

        public IActionResult GetCustomerShouldPayEntity([FromQuery] string filter, string callback = "callback")
        {
            SearchFilter f = JsonConvert.DeserializeObject<SearchFilter>(filter);
            List<CustomerPaymentEntity> data = new List<CustomerPaymentEntity>();

            if (f.CustomerCode == "")
            {
                data = CustomerShouldPayService.GetByStationAndMonth(f.StationCode, f.Date.Year, f.Date.Month, f.IsPaid).ToList();
            }

            else
            {
                //CustomerShouldPayService.InitializeFreightByCustomers(f.CustomerCode, f.Date.Year, f.Date.Month);
                data.Add(CustomerShouldPayService.GetByCustomerAndMonth(f.CustomerCode, f.Date.Year, f.Date.Month));
            }

            string serializedEntity = JsonConvert.SerializeObject(data);
            string output = string.Format("{0}({1})", callback, serializedEntity);

            return Content(output);
        }

        public IActionResult UpdateAccountReceivable(string models, string stringDate, string callback = "callback")
        {
            IEnumerable<CustomerPaymentEntity> entity = JsonConvert.DeserializeObject<IEnumerable<CustomerPaymentEntity>>(models);
            CustomerPaymentEntity modified = entity.Last();

            // 抓回來是UTC，要再加8小時
            if (modified.PaidDate != null)
                modified.PaidDate = modified.PaidDate?.AddHours(8);

            DateTime date = DateTime.Parse(stringDate);
            modified.ShipDateStart = new DateTime(date.Year, date.Month, 1);
            modified.ShipDateEnd = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));

            CustomerShouldPayService.UpdateAccountReceivable(modified);

            string output = string.Format("{0}({1})", callback, models);
            return Content(output);
        }

        public IActionResult ViewDetailByCustomer(string customerCode, int year, int month)
        {
            CustomerPaymentEntity data = CustomerShouldPayService.GetCustomerPaymentByCustomerAndMonth(customerCode, year, month);

            return View(data);
        }

        public async Task<IActionResult> Save([FromBody] SaveFreightAndTypeEntity entity)
        {
            //int pickUpType = (int)Enum.Parse(typeof(PickUpGoodTypeEntity), entity.PickUpType);
            JunFuWebServiceInService.WebServiceSoapClient webServiceSoapClient = new JunFuWebServiceInService.WebServiceSoapClient(JunFuWebServiceInService.WebServiceSoapClient.EndpointConfiguration.WebServiceSoap);
            //string response = await webServiceSoapClient.SaveFreightAndTypeAsync(entity.CheckNumber, entity.Freight, pickUpType.ToString());
            string[] checkNum = { entity.CheckNumber };
            int[] freight = { entity.Freight };
            string response = await webServiceSoapClient.SaveFreightBatchAsync(checkNum, freight);
            return Content(response);
        }

        public IActionResult GetFreightByCustomerAndType(string customerCode, string type)
        {
            CustomerShippingFee shippingFee = CustomerShouldPayService.GetShippingFeeByCustomerCodeOrDefault(customerCode);
            PickUpGoodTypeEntity pickUpType = (PickUpGoodTypeEntity)Enum.Parse(typeof(PickUpGoodTypeEntity), type);
            int fee = CustomerShouldPayService.GetFeeByCustomerShippingFeeEntity(pickUpType, shippingFee);

            string output = JsonConvert.SerializeObject(new { freight = fee });

            return Content(output);
        } 
        #endregion

        public IActionResult ShippingFeeEditor()
        {
            return View();
        }

        public IActionResult GetShippingFeeEntity(string filter, string callback = "callback")
        {
            var searchFilter = JsonConvert.DeserializeObject<SearchFilter>(filter);

            List<CustomerShippingFeeEntity> result;

            if(searchFilter.CustomerCode.Length > 0)
            {
                result = CustomerShouldPayService.GetShippingFeeByCustomerCode(searchFilter.CustomerCode);
            }
            else if(searchFilter.StationCode.Length > 0 && searchFilter.StationCode != "-1")
            {
                result = CustomerShouldPayService.GetShippingFeeByStation(searchFilter.StationCode);
            }
            else
            {
                result = CustomerShouldPayService.GetAllCustomerShippingFee();
            }

            if (result == null)
            {
                result = CustomerShouldPayService.GetShippingFeeByCustomerCode("0");
            }

            string output = string.Format("{0}({1})", callback, JsonConvert.SerializeObject(result));

            return Content(output);
        }

        public IActionResult UpdateShippingFee(string models, string callback = "callback")
        {
            List<CustomerShippingFeeEntity> entities = JsonConvert.DeserializeObject<List<CustomerShippingFeeEntity>>(models);

            var lastOne = entities.Last();
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            string account = userInfoEntity.UserAccount;

            var returnEntity = CustomerShouldPayService.UpdateShippingFee(lastOne, account);

            string output = string.Format("{0}({1})", callback, JsonConvert.SerializeObject(returnEntity));

            return Content(output);
        }

        [HttpPost]
        public async Task<IActionResult> ImportFile(IFormFile file)
        {
            bool resultCode = true;
            string errorMessage = string.Format("沒有問題");

            try
            {
                List<AccountReceivable> Entities = new List<AccountReceivable>();
                string fullPath = string.Empty;

                if (file.Length > 0)
                {
                    var savePath = this.config.GetValue<string>("UploadFileUrl");
                    string uniqueFileName = CustomerShouldPayService.GetUniqueFileName(file.FileName);
                    fullPath = string.Format("{0}/{1}", savePath, uniqueFileName);
                    using (FileStream stream = new FileStream(fullPath, FileMode.Append))
                    {
                        await file.CopyToAsync(stream);
                    }

                    System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
                    using (var stream = System.IO.File.Open(fullPath, FileMode.Open, FileAccess.Read))
                    {
                        using (var reader = ExcelReaderFactory.CreateReader(stream))
                        {
                            var result = reader.AsDataSet();

                            var rows = result.Tables[0].Rows.Count;//獲取總行數

                            if (rows > 1) //除了標題行外有資料才讀取
                            {
                                for (int i = 1; i < rows; i++) // 避開標題行，從第二行開始讀取
                                {
                                    AccountReceivable Entity = new AccountReceivable(); // i每加1，就new一個Entity
                                    Entity.ElectronicReceipt = result.Tables[0].Rows[i][0].ToString();
                                    Entity.InvoiceId = result.Tables[0].Rows[i][11].ToString();
                                    Entities.Add(Entity);
                                }
                            }
                            else
                            {
                                return Content("沒有資料無法上傳");
                            }

                        }
                    }
                }

                CustomerShouldPayService.UpdateElectricReceipt(Entities);

                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }

                return View();
            }
            catch (Exception e)
            {
                resultCode = false;
                errorMessage = e.Message;
            }
            string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
            return Content(response);
        }

        public class Success 
        {
            public string ShowSuccessText { get; set; }
            public string ReturnToIndex { get; set; }
        }
    }
}
