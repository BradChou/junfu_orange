﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LightYear.JunFuTrans.BL.Services.ContactlessDelivery;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class ContactlessDeliveryController : Controller
    {
        public IContactlessDeliveryService ContactlessDeliveryService { get; set; }

        public ContactlessDeliveryController(IContactlessDeliveryService contactlessDeliveryService)
        {
            ContactlessDeliveryService = contactlessDeliveryService;
        }

        public IActionResult Index(string checknumber)
        {
            var scanDate = ContactlessDeliveryService.CheckIfScanOrNot(checknumber);
            ViewBag.ScanDate = scanDate; 
            ViewBag.CheckNumber = checknumber;
            return View();
        }
        public IActionResult CheckReceiveContactTel(string checknumber, string tel)
        {
            var data = ContactlessDeliveryService.CheckReceiveContactTel(checknumber, tel);
            string serializedStr = JsonConvert.SerializeObject(data);
            return Content(serializedStr);
        }

        public IActionResult GetReceiveContactDataByChecknumber(string checknumber)
        {
            var data = ContactlessDeliveryService.CheckReceiveContactTel(checknumber);
            string serializedStr = JsonConvert.SerializeObject(data);
            return Content(serializedStr);
        }

        public IActionResult CheckLatestDeliveryDriverNullOrNot(string checknumber)
        {
            var data = ContactlessDeliveryService.CheckLatestDeliveryDriverNullOrNot(checknumber);
            string serializedStr = JsonConvert.SerializeObject(data);
            return Content(serializedStr);
        }

        public IActionResult InsertIntoScanLog(string checknumber, string pieces)
        {
            var scanDate = ContactlessDeliveryService.CheckIfScanOrNot(checknumber);

            if (!string.IsNullOrEmpty(scanDate))
            {
                return Content(JsonConvert.SerializeObject(scanDate)); 
            }
            else
            {
                var result = ContactlessDeliveryService.InsertContactlessDeliveryCheckNumberIntoScanLog(checknumber, pieces);
                return Content(JsonConvert.SerializeObject(result.ToString()));
            }
        }

        public IActionResult SignPaper(string checknumber)
        {
            var scanDate = ContactlessDeliveryService.CheckIfScanOrNot(checknumber);
            ViewBag.ScanDate = scanDate;
            ViewBag.CheckNumber = checknumber;
            return View();
        }
    }
}