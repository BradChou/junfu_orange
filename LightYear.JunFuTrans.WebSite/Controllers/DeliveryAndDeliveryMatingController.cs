﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.Services.DeliveryAndDeliveryMating;
using LightYear.JunFuTrans.BL.BE.DeliveryAndDeliveryMating;
using LightYear.JunFuTrans.BL.Services.DeliveryException;
using Microsoft.AspNetCore.Authorization;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class DeliveryAndDeliveryMatingController : Controller
    {
        private IWebHostEnvironment webHostEnvironment;
        IDeliveryAndDeliveryMatingService DeliveryAndDeliveryMatingService;
        private readonly IConfiguration config;

        public DeliveryAndDeliveryMatingController(IDeliveryAndDeliveryMatingService deliveryAndDeliveryMatingService, IWebHostEnvironment webHostEnvironment, IConfiguration config, IDeliveryExceptionService deliveryExceptionService)
        {
            DeliveryAndDeliveryMatingService = deliveryAndDeliveryMatingService;
            this.webHostEnvironment = webHostEnvironment;
            this.config = config;
        }

        [Authorize]
        public IActionResult DriverView()
        {
            //取得登入資料
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            ViewBag.LoginName = userInfoEntity.UserName;
            ViewBag.UserAccount = userInfoEntity.UserAccount;
            ViewBag.Station = userInfoEntity.Station;
            
            //另一種傳遞的方法
            //List<KendoSelectEntity> kendoSelectEntities = DeliveryAndDeliveryMatingService.GetAllByDriverCodeStationScodeArriveOption(userInfoEntity.UserAccount, userInfoEntity.Station);
            //string countpieces = string.Empty;
            //string countarriveroption = string.Empty;
            //foreach(KendoSelectEntity kendoSelectEntity in kendoSelectEntities)
            //{
            //    countpieces = string.Format(",{0}{1}",kendoSelectEntity.CountPieces,countpieces);
            //    countarriveroption = string.Format(",{0}{1}",kendoSelectEntity.CountArriveOptions,countarriveroption);
            //}
            //countpieces = (countpieces.Length > 0) ? countpieces.Substring(1) : string.Empty;
            //countarriveroption = (countarriveroption.Length > 0) ? countarriveroption.Substring(1) : string.Empty;
            //ViewBag.countpieces = countpieces;
            //ViewBag.countarriveoption = countarriveroption;
            return View();
        }
               
        public IActionResult GetAll(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption, string callback = "callback")
        {
            try
            {

                var Entities = DeliveryAndDeliveryMatingService.GetAll(start, end, driverCode, stationscode, arriveOption);

                if (Entities.Count() > 1)
                {
                    TempData["Sum"] = Entities[Entities.Count() - 1].ListSum;
                }

                string performance = JsonConvert.SerializeObject(Entities);
                string output = string.Format("{0}({1})", callback, performance);
                return Content(output);
            }
            catch(Exception ex)
            {
                return Content(ex.ToString());
            }
        }

        public IActionResult GetArriveOptions()
        {
            return Json(DeliveryAndDeliveryMatingService.GetArriveOptions());
        }

        public IActionResult GetAllDrivers(string stationscode = "")
        {
            stationscode = stationscode ?? string.Empty;

            if (stationscode.Length > 0 && stationscode != "" && stationscode != null)
            {
                return Json(DeliveryAndDeliveryMatingService.GetDriversByStation(stationscode));
            }
            else
            {
                return Json(DeliveryAndDeliveryMatingService.GetAllDrivers());
            }
        }

        public IActionResult GetCountPieces(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption, string callback = "callback") 
        {
            int count = DeliveryAndDeliveryMatingService.GetCountPieces(start, end, driverCode, stationscode, arriveOption);
            string serializedStr = JsonConvert.SerializeObject(count);
            string output = string.Format("{0}({1})", callback, serializedStr);
            return Content(output);
        }

        public IActionResult GetCountArriveOptions(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption, string callback = "callback")
        {
            int count = DeliveryAndDeliveryMatingService.GetCountArriveOptions(start, end, driverCode, stationscode, arriveOption);
            string serializedStr = JsonConvert.SerializeObject(count);
            string output = string.Format("{0}({1})", callback, serializedStr);
            return Content(output);
        }

        public IActionResult GetSumOfCollectionMoney(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption, string callback = "callback")
        {
            int count = DeliveryAndDeliveryMatingService.GetAll(start, end, driverCode, stationscode, arriveOption).Sum(x=>x.CollectionMoney);
            string serializedStr = JsonConvert.SerializeObject(count);
            string output = string.Format("{0}({1})", callback, serializedStr);
            return Content(output);
        }
    }        
}
