﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LightYear.JunFuTrans.BL.Services;
using LightYear.JunFuTrans.BL.Services.Intermodal;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class IntermodalController : Controller
    {
        IIntermodalService IntermodalService;
        public IntermodalController(IIntermodalService intermodalService)
        {
            IntermodalService = intermodalService;
        }
        public IActionResult InsertGuoYang(List<string> requestIds)
        {
            bool isSuccess = IntermodalService.GetDeliverRequestByCheckNumbers(requestIds);

            return Content(isSuccess.ToString());
        }

        public IActionResult GuoYangSchedule(int step = -1)
        {
            bool isSuccess = IntermodalService.GuoYangSchedule(step);

            return Content(isSuccess.ToString());
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}
