﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using LightYear.JunFuTrans.WebSite.Models;
using LightYear.JunFuTrans.BL.Services.SystemFunction;
using Microsoft.AspNetCore.Authorization;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        public ISystemFunctionService SystemFunctionService;

        public HomeController(ILogger<HomeController> logger, ISystemFunctionService systemFunctionService)
        {
            _logger = logger;

            this.SystemFunctionService = systemFunctionService;
        }

        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        public IActionResult IndexForJF()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
