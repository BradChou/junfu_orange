﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.Services.PickUpAndDeliveryMating;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.Repositories.Station;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class PickUpAndDeliveryMatingController : Controller
    {
        IPickUpAndDeliveryMatingService PickUpAndDeliveryMatingService;

        IStationRepository StationRepository;

        public PickUpAndDeliveryMatingController(IPickUpAndDeliveryMatingService pickUpAndDeliveryMatingService, IStationRepository stationRepository)
        {
            this.PickUpAndDeliveryMatingService = pickUpAndDeliveryMatingService;
            StationRepository = stationRepository;
        }

        public IActionResult StationList()
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            ViewBag.UserAccount = userInfoEntity.UserAccount;
            ViewBag.Station = userInfoEntity.Station;
            ViewBag.LoginStation = userInfoEntity.StationShowName;

            string[] stationCodeArray = new string[] { "FGM01", "FMD01", "FHD01", "FBD01", "FBM01", "FBM02" };

            if (stationCodeArray.Contains(userInfoEntity.Station))
            {
                ViewBag.CanSelect = true;
            }
            else
            {
                ViewBag.CanSelect = false;
            }

            return View();
        }

        // 明細頁面
        public IActionResult StationListDetail(string driverCode, DateTime month, string stationScode)
        {
            month = new DateTime(month.Year, month.Month, 1);
            ViewBag.DriverCode = driverCode;
            ViewBag.Month = month.ToString("yyyy/MM/dd");
            ViewBag.Station = stationScode;

            if(stationScode != null && stationScode.Length > 0)
                ViewBag.StationName = StationRepository.GetStationNameByScode(stationScode);
            return View();
        }

        // 站所的明細
        public IActionResult GetAllByStationScodeTime(string scode, DateTime month, string callback = "callback")
        {
            month = new DateTime(month.Year, month.Month, 01, 00, 00, 00);
            var Entities = PickUpAndDeliveryMatingService.GetDetailByStationScodeAndTime(scode, month);
            string performance = JsonConvert.SerializeObject(Entities);
            string output = string.Format("{0}({1})", callback, performance);
            return Content(output);
        }

        // 司機的明細
        public IActionResult GetDetialByDriverAndTime(string driverCode, DateTime month, string callback = "callback")
        {
            month = new DateTime(month.Year, month.Month, 01, 00, 00, 00);
            var Entities = PickUpAndDeliveryMatingService.GetDetailByDriver(driverCode, month);
            string performance = JsonConvert.SerializeObject(Entities);
            string output = string.Format("{0}({1})", callback, performance);
            return Content(output);
        }

        // 總表
        public IActionResult GetDriverAllCountByMonthStationScode(string area, string scode, DateTime month, string callback = "callback")
        {
            month = new DateTime(month.Year, month.Month, 01, 00, 00, 00);
            var Entities = PickUpAndDeliveryMatingService.GetDriverAllCountByMonthStationScode(area, scode, month);
            string performance = JsonConvert.SerializeObject(Entities);
            string output = string.Format("{0}({1})", callback, performance);
            return Content(output);
        }
    }
}