using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Autofac.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using LightYear.JunFuTrans.BL.Services.StudentTest;
using LightYear.JunFuTrans.DA.LightYearTest;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.JunFuTransReadOnlyDb;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using LightYear.JunFuTrans.WebSite.Handler;
using Microsoft.AspNetCore.Server.Kestrel.Core;

namespace LightYear.JunFuTrans.WebSite
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            //註冊資料庫連線
            services.AddDbContext<LightYearTestDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("LightYearTestDbContext")));

            services.AddDbContext<JunFuDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("JunFuDbContext")));

            services.AddDbContext<JunFuTransDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("JunFuTransDbContext")));

            services.AddDbContext<JunFuTransReadOnlyDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("JunFuTransReadOnlyDbContext")));

            //從組態讀取登入逾時設定
            double LoginExpireMinute = this.Configuration.GetValue<double>("LoginExpireMinute");
            //註冊 CookieAuthentication，Scheme必填
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(option =>
            {
                //或許要從組態檔讀取，後續再修改
                option.LoginPath = new PathString("/Account/Login");//登入頁
                option.LogoutPath = new PathString("/Account/Logout");//登出Action
                //用戶頁面停留太久，登入逾期，或Controller中用戶登入時機點也可以設定↓
                option.ExpireTimeSpan = TimeSpan.FromMinutes(LoginExpireMinute);//沒給預設14天
            });
            services.Configure<FormOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.MultipartHeadersLengthLimit = int.MaxValue;
                options.MultipartHeadersCountLimit = int.MaxValue;
                options.BufferBodyLengthLimit = long.MaxValue;
                options.KeyLengthLimit = int.MaxValue;
                options.MultipartBodyLengthLimit = long.MaxValue;
                options.MultipartBoundaryLengthLimit = int.MaxValue;
                options.ValueCountLimit = int.MaxValue;
                options.ValueLengthLimit = int.MaxValue;
            });
            // 將 Session 存在 ASP.NET Core 記憶體中
            services.AddDistributedMemoryCache();
            services.AddSession();

            services.AddControllersWithViews().AddRazorRuntimeCompilation();

            services.AddMvc().AddXmlSerializerFormatters();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.Configure<IISServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseSession();
            //先執行驗證
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseLoggingMiddleware();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });            
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new AutofacModule());
        }
    }

    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            //// Read autofac settings from config
            var autofacConfig = new ConfigurationBuilder();
            autofacConfig.AddJsonFile("autofac.json");

            var configRoot = autofacConfig.Build();

            // Register the ConfigurationModule with Autofac.
            var module = new ConfigurationModule(configRoot);
            builder.RegisterModule(module);

        }
    }
}
