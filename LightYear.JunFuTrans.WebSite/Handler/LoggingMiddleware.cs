﻿using LightYear.JunFuTrans.BL.BE.Account;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightYear.JunFuTrans.DA.Repositories.JunFuOrangeLogs;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;

namespace LightYear.JunFuTrans.WebSite.Handler
{
    public class LoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<LoggingMiddleware> _logger;
        public IJunFuOrangeLogRepository JunFuOrangeLogRepository { get; set; }

        //public LoggingMiddleware(RequestDelegate next, ILogger<LoggingMiddleware> logger)
        public LoggingMiddleware(RequestDelegate next)
        {
            _next = next;
            //_logger = logger;
            //JunFuOrangeLogRepository = junFuOrangeLogRepository;
        }

        /// <summary>
        /// 任務調用
        /// </summary>
        /// <param name="context">HTTP 的上下文</param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context, IJunFuOrangeLogRepository _JunFuOrangeLogRepository)
        //public async Task Invoke(HttpContext context, JunFuTransDbContext db)
        {
            var guid = GetGuid();
            DateTime startTime = DateTime.Now;
            var account = string.Empty;
            //避免沒有帳號的情況
            if (context.User.Claims.Count() > 0)
            {
                var claim = context.User.Claims.First(c => c.Type == "UserInfo");
                var userInfoStr = claim.Value;
                UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
                account = userInfoEntity.UserAccount;
            }
            /*
             請注意，HTTP Request Body 是個特殊的 Stream，只能被讀取一次
             且當 HTTP Conetxt 經過 MVC 中介程序後，HTTP Request Body 會因為被讀取過而消失
             如果希望在整個 Pipeline 流程中保留原始的 HTTP Request Body 資料，請在 MVC 中介程序之前，暫存相關資料
             以便後續使用
             */
            var log = $"{context.Request.Path}, {context.Request.Method}, {ReadRequestBody(context)}";

            //var account = context.Items["Account"] == null ? string.Empty : (string)context.Items["Account"];
            var Schema = context.Request.Scheme;

            var Host = context.Request.Host.ToUriComponent();

            var Path = context.Request.Path.HasValue ? context.Request.Path.Value : string.Empty;

            var QueryString = context.Request.QueryString.HasValue ? context.Request.QueryString.Value : string.Empty;

            var RequestHeader = context.Request.Headers;

            var RequestBody = "";
            RequestBody = ReadRequestBody(context);
            context.Request.Body.Position = 0;
            var decodeRequestBody = System.Uri.UnescapeDataString(RequestBody);
            var decodeQueryString = System.Uri.UnescapeDataString(QueryString);
            if (decodeRequestBody.Contains("&password="))
            {
                var tmpList = decodeRequestBody.Split('&');
                var resultList = new List<string>();
                foreach (var sub in tmpList)
                {
                    if (sub.Contains("password="))
                    {
                        resultList.Add("password=*****");
                    }
                    else
                    {
                        resultList.Add(sub);
                    }
                }
                decodeRequestBody = String.Join("&", resultList);
            }

            await _next(context);
            DateTime endTime = DateTime.Now;

            var junFuOrangeLog = new JunFuOrangeLog();
            junFuOrangeLog.Account = account;
            junFuOrangeLog.Guid = guid;
            junFuOrangeLog.QueryString = decodeQueryString;
            junFuOrangeLog.Path = Path;
            junFuOrangeLog.RequestHeader = RequestHeader.ToString();
            junFuOrangeLog.RequestBody = decodeRequestBody;
            junFuOrangeLog.StartDate = startTime;
            junFuOrangeLog.EndDate = endTime;
            try
            {
                await _JunFuOrangeLogRepository.InsertAsync(junFuOrangeLog);
            }
            catch (Exception e)
            {
                throw;
            }
            



            ////抓response body
            //string responseContent;

            //var originalBodyStream = context.Response.Body;
            //using (var fakeResponseBody = new MemoryStream())
            //{
            //    context.Response.Body = fakeResponseBody;

            //    fakeResponseBody.Seek(0, SeekOrigin.Begin);
            //    using (var reader = new StreamReader(fakeResponseBody))
            //    {
            //        responseContent = await reader.ReadToEndAsync();
            //        fakeResponseBody.Seek(0, SeekOrigin.Begin);

            //        await fakeResponseBody.CopyToAsync(originalBodyStream);
            //    }
            //}
            ///response
            //var junFuOrangeLog = new JunFuOrangeLog();
            //junFuOrangeLog.Account = account;
            //junFuOrangeLog.Guid = "123";
            //junFuOrangeLog.QueryString = QueryString;
            //junFuOrangeLog.Path = Path;
            //junFuOrangeLog.RequestHeader = RequestHeader.ToString();
            //junFuOrangeLog.RequestBody = RequestBody;
            //junFuOrangeLog.StartDate = DateTime.Now;
            //junFuOrangeLog.EndDate = DateTime.Now;
            //await _JunFuOrangeLogRepository.InsertAsync(junFuOrangeLog);

            //try
            //{
            //    db.JunFuOrangeLogs.Add(new JunFuOrangeLog()
            //    {
            //        Account = "test",
            //        Guid = "123",
            //        Path = "test",
            //        QueryString = "test",
            //        RequestBody = "test",
            //        RequestHeader = "test",
            //        ResponseBody = "test",
            //        StartDate = DateTime.Now,
            //        EndDate = DateTime.Now,
            //        CreateDate = DateTime.Now

            //    });
            //    await db.SaveChangesAsync();
            //}
            //catch (Exception e)
            //{

            //    throw;
            //}
            //await _next(context);
            //_logger.LogTrace(log);

            //await _next(context);
        }
        /// <summary>
        /// 讀取 HTTP Request 的 Body 資料
        /// </summary>
        /// <param name="context">HTTP 的上下文</param>
        /// <returns></returns>
        private string ReadRequestBody(HttpContext context)
        {
            // 確保 HTTP Request 可以多次讀取
            context.Request.EnableBuffering();
            // 讀取 HTTP Request Body 內容
            // 注意！要設定 leaveOpen 屬性為 true 使 StreamReader 關閉時，HTTP Request 的 Stream 不會跟著關閉
            using (var bodyReader = new StreamReader(stream: context.Request.Body,
                                                      encoding: Encoding.UTF8,
                                                      detectEncodingFromByteOrderMarks: false,
                                                      bufferSize: 1024,
                                                      leaveOpen: true))
            {
                var body = bodyReader.ReadToEnd();

                // 將 HTTP Request 的 Stream 起始位置歸零
                context.Request.Body.Position = 0;

                return body;
            }
        }


        private static string GetGuid()
        {

            var guid = Guid.NewGuid().ToString("N");

            return guid;
        }
    }

    public static class LoggingMiddlewareExtensions
    {
        /// <summary>在中介程序中收集 HTTP Request 資訊</summary>
        /// <param name="builder">中介程序建構器</param>
        /// <returns></returns>
        public static IApplicationBuilder UseLoggingMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<LoggingMiddleware>();
        }
    }
}