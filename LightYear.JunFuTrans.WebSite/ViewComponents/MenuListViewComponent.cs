﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LightYear.JunFuTrans.BL.Services.SystemFunction;
using LightYear.JunFuTrans.BL.BE.Menu;
using System.Security.Claims;
using LightYear.JunFuTrans.BL.BE.Account;
using Newtonsoft.Json;

namespace LightYear.JunFuTrans.WebSite.ViewComponents
{
    public class MenuListViewComponent : ViewComponent
    {

        public ISystemFunctionService SystemFunctionService;

        public MenuListViewComponent(ISystemFunctionService systemFunctionService)
        {
            this.SystemFunctionService = systemFunctionService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var items = await GetItemsAsync();
            return View(items);
        }

        private Task<FrontendMenuEntity> GetItemsAsync()
        {
            UserInfoEntity userInfoEntity = new UserInfoEntity();

            foreach(Claim claim in HttpContext.User.Claims)
            {
                if( claim.Type == "UserInfo")
                {
                    string userInfoString = claim.Value;

                    userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoString);
                }
            }

            return Task.Run(() => this.SystemFunctionService.GetCategoryEntitiesByUserInfo(userInfoEntity));
        }
    }
}
