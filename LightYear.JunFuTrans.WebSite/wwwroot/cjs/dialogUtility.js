var nineyi = window.nineyi || {};
(function (nineyi) {
    var dialogUtility = nineyi.dialogUtility || (nineyi.dialogUtility = {});
    (function (dialogUtility) {
        /**
         * alert
         * @param args
         */
        dialogUtility.alert = function (args) {

            var message = args.message || '';
            var timeout = args.timeout || null;
            var okText = args.okText || "ok";
            var okCallback = args.okCallback;
            
            $.blockUI({
                message: "<div style='height:60px;padding:20px'>" + message + "</div><hr><button id='confirm-ok' style='background-color:#469be5;color:white'>" + okText + "</button>",
                timeout: timeout,
                css: {
                    width: '24%',
                    top: '40%',
                    left: '38%',
                    textAlign: 'center',
                    backgroundColor: 'white',
                    padding: '15px',
                    cursor: 'wait'
                },
                baseZ: 40000,
            });

            $("#confirm-ok").click(function () {
                typeof okCallback === 'function' && okCallback();
                $.unblockUI();
            });
        };
        /**
         * confirm
         * @param args
         */
        dialogUtility.confirm = function (args) {

            var message = args.message || '';
            var okText = args.okText || "ok";
            var cancelText = args.cancelText || "cancel";
            var okCallback = args.okCallback;
            var cancelCallback = args.cancelCallback;

            $.blockUI({
                message: "<div style='height:60px;padding:20px'>" + message + "</div><hr><button id='confirm-ok' style='background-color:#469be5;color:white'>" + okText + "</button>" + "<button id='confirm-cancel' style='background-color:#469be5;color:white'>" + cancelText + "</button>",
                css: {
                    width: '24%',
                    top: '40%',
                    left: '38%',
                    textAlign: 'center',
                    backgroundColor: 'white',
                    padding: '15px',
                    cursor: 'wait'
                },
                baseZ: 40000,
            });

            $("#confirm-ok").click(function () {
                typeof okCallback === 'function' && okCallback();
            });
            $("#confirm-cancel").click(function () {
                typeof cancelCallback === 'function' && cancelCallback();                
                $.unblockUI();
            });
        };

        dialogUtility.numberPrompt = function (args) {

            var label = args.label || '';
            var max = args.max;
            var min = args.min;
            var defaultValue = args.defaultValue || 0;
            var message = args.message || '';
            var okText = args.okText || "ok";
            var cancelText = args.cancelText || "cancel";
            var okCallback = args.okCallback;
            var cancelCallback = args.cancelCallback;

            $.blockUI({
                message: "<div style='height:60px;padding:20px'>" + message + "</div>" + label + "<input type='text' id='number-prompt'></div><hr><button id='confirm-ok' style='background-color:#469be5;color:white'>" + okText + "</button>" + "<button id='confirm-cancel' style='background-color:#469be5;color:white'>" + cancelText + "</button>",
                css: {
                    width: '24%',
                    top: '40%',
                    left: '38%',
                    textAlign: 'center',
                    backgroundColor: 'white',
                    padding: '15px',
                    cursor: 'wait'
                },
                baseZ: 40000,
            });

            $("#number-prompt").kendoNumericTextBox({
                min: min,
                max: max,
                value: defaultValue
            });

            $("#confirm-ok").click(function () {
                typeof okCallback === 'function' && okCallback();
            });
            $("#confirm-cancel").click(function () {
                typeof cancelCallback === 'function' && cancelCallback();
                $.unblockUI();
            });
        };
        /**
         * custom
         * @param args
         */
        dialogUtility.custom = function (args) {
            // 實作 custom dialog 內容功能，這邊要視你要依賴的模組的做法(kendo || jquery UI)
        }
    })(dialogUtility)
})(nineyi);