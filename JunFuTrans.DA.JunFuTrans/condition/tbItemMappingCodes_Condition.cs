﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JunFuTrans.DA.JunFuTrans.condition
{
    public class tbItemMappingCodes_Condition
    {
		public int mapping_id { get; set; }
		public string mapping_bclass { get; set; }
		public string mapping_sclass { get; set; }
		public string mapping_a_id { get; set; }
		public string mapping_b_id { get; set; }
		public bool active_flag { get; set; }
		public string memo { get; set; }
	}
}
