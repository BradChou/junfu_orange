﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JunFuTrans.DA.JunFuTrans.condition
{
    public class Pelican_request_Condition
    {
        public int request_id { get; set; }
        public int log_id { get; set; }
        public string pelican_check_number { get; set; }
        public int jf_request_id { get; set; }
        public string jf_check_number { get; set; }
        public string delivery_type { get; set; }
        public DateTime cdate { get; set; }
        public DateTime udate { get; set; }
        public string cuser { get; set; }
        public string uuser { get; set; }
        public int fail_times { get; set; }
        public int fail_log_id { get; set; }
        public int fail_at { get; set; }
        public string order_number { get; set; }
        public int pieces { get; set; }
        public string send_contact { get; set; }
        public string send_tel { get; set; }
        public string send_city { get; set; }
        public string send_area { get; set; }
        public string send_address { get; set; }
        public string receive_contact { get; set; }
        public string receive_tel { get; set; }
        public string receive_city { get; set; }
        public string receive_area { get; set; }
        public string receive_address { get; set; }
        public int assigned_time_period { get; set; }
        public int collection_money { get; set; }
        public string remark { get; set; }
        public string jf_customer_code { get; set; }
        public string jf_send_zip3 { get; set; }
        public string jf_receive_zip3 { get; set; }
        public string pelican_receive_zip5 { get; set; }
        public string pelican_receive_area { get; set; }
        public DateTime arrive_assign_date { get; set; }
        public bool round_trip { get; set; }
        public int dr_request_id { get; set; }
        public string roundtrip_checknumber { get; set; }
    }
}
