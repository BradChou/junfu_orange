﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JunFuTrans.DA.JunFuTrans.condition
{
    public class shopee_api_log
    {
        public int? id { get; set; }
        public string type { get; set; }
        public string action { get; set; }
        public string message { get; set; }
        public DateTime? cdate { get; set; }

    }
}
