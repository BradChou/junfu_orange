﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JunFuTrans.DA.JunFuTrans.condition
{
   public class tbItemCodes_Condition
    {
		public decimal seq { get; set; }
		public string code_bclass { get; set; }
		public string code_sclass { get; set; }
		public string code_id { get; set; }
		public string code_name { get; set; }
		public bool active_flag { get; set; }
		public string memo { get; set; }
	}
}
