﻿using Dapper;
using JunFuTrans.DA.JunFuTrans.condition;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace JunFuTrans.DA.JunFuTrans.DA
{
    public class MeasureScanLog_DAL
    {
        private static IConfiguration Configs()
        {
            try
            {
                return new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            }
            catch
            {
                return new ConfigurationBuilder().AddJsonFile("app.json").Build();
            }


        }
        IConfiguration configs = Configs();
    


        public void InsertMeasureScanLog(MeasureScanLog_Condition _condition)
        {

            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                string sql = @"INSERT INTO [MeasureScanLog] (
	DataSource
    ,Scancode
	,Length
	,Width
	,Height
	,Size
	,STATUS
	,ScanTime
	,Picture
	,cdate
	)
VALUES (
	@DataSource
    ,@Scancode
	,@Length
	,@Width
	,@Height
	,@Size
	,@STATUS
	,@ScanTime
	,@Picture
	,@cdate
	)";
                cn.Execute(sql, _condition);
            }

        }
    }
}
