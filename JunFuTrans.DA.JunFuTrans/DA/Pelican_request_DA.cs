﻿using Dapper;
using JunFuTrans.DA.JunFuTrans.condition;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace JunFuTrans.DA.JunFuTrans.DA
{
    public class Pelican_request_DA
    {
        private static IConfiguration Configs()
        {
            try
            {
                return new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            }
            catch
            {
                return new ConfigurationBuilder().AddJsonFile("app.json").Build();
            }


        }
        IConfiguration configs = Configs();


        public Pelican_request_Condition GetPelican_requestByjf_check_number(string jf_check_number)
        {

            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[Pelican_request] WITH (NOLOCK)
                        WHERE jf_check_number = @jf_check_number
                    ";
                return cn.QueryFirstOrDefault<Pelican_request_Condition>(sql, new { jf_check_number });
            }

        }

    
    }
}
