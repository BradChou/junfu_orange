﻿using Dapper;
using JunFuTrans.DA.JunFuTrans.condition;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JunFuTrans.DA.JunFuTrans.DA
{
    public class tbItemMappingCodes_DAL
    {
        private static IConfiguration Configs()
        {
            try
            {
                return new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            }
            catch
            {
                return new ConfigurationBuilder().AddJsonFile("app.json").Build();
            }


        }
        IConfiguration configs = Configs();

        public async Task InsertIncomeFeeType(tbItemMappingCodes_Condition _condition)
        {

            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                string sql = @"
                  INSERT INTO [dbo].[tbItemMappingCodes] ([mapping_bclass] ,[mapping_sclass] ,[mapping_a_id] ,[mapping_b_id] ,[active_flag] ,[memo]) 
                    VALUES('1', 'jf_dept_fee_type', @mapping_a_id, @mapping_b_id, @active_flag, '車老闆收支項目權責部門')";
                await cn.ExecuteAsync(sql, _condition);
            }

        }

        public async Task UpdatetbItemCodes(tbItemMappingCodes_Condition _condition)
        {

            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                string sql = @"
UPDATE [dbo].[tbItemMappingCodes] SET [mapping_a_id] = @mapping_a_id,
                        [mapping_b_id] = @mapping_b_id ,[active_flag] = @active_flag 
                        WHERE mapping_id = @mapping_id  

";
                await cn.ExecuteAsync(sql, _condition);
            }

        }
    }
}
