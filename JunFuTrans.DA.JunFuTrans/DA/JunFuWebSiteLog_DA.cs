﻿using Dapper;
using LightYear.JunFuTrans.BL.BE.LogToDB;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace JunFuTrans.DA.JunFuTrans.DA
{
    public class JunFuWebSiteLog_DA
    {
        private static IConfiguration Configs()
        {
            var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            return config;
        }
        IConfiguration configs = Configs();


        public void InsertLogToDBWebSiteEntity(LogToDBWebSiteEntity _condition)
        {

            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                string sql = @" INSERT INTO [JunFuWebSiteLog] ([Action],[Header],[Request],[Response],[IP],[UserAgent],[URL],[Cdate]) VALUES (@Action,@Header,@Request,@Response,@IP,@UserAgent,@URL,@Cdate)";
                cn.Execute(sql, _condition);
            }

        }
    }
}
