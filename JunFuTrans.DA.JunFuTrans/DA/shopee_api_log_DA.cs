﻿using Dapper;
using JunFuTrans.DA.JunFuTrans.condition;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace JunFuTrans.DA.JunFuTrans.DA
{
    public class shopee_api_log_DA
    {
        private static IConfiguration Configs()
        {
            var config = new ConfigurationBuilder().AddJsonFile("app.json").Build();
            return config;
        }
        IConfiguration configs = Configs();
     

        public void Insertshopee_api_log(shopee_api_log _condition)
        {
           
            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuTransDbContext"]))
            {
                string sql = @"INSERT INTO [shopee_api_log](type,action,message,cdate) VALUES (@type,@action,@message,@cdate)"; 
                cn.Execute(sql, _condition);
            }

        }
    }
}
