﻿using Dapper;
using JunFuTrans.DA.JunFuTrans.condition;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JunFuTrans.DA.JunFuTrans.DA
{
    public class tbItemCodes_DAL
    {
        private static IConfiguration Configs()
        {
            try
            {
                return new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            }
            catch
            {
                return new ConfigurationBuilder().AddJsonFile("app.json").Build();
            }


        }
        IConfiguration configs = Configs();

        public async Task InsertCompany(string code_name)
        {

            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                try
                {
                    string sql = @"
                    declare @code_id int;
                    set @code_id = (select top 1 code_id from tbItemCodes where code_bclass = '6' and code_sclass = 'CD' and code_id<> '99' order by cast(code_id as int) desc);
                    insert into tbItemCodes (code_bclass, code_sclass ,code_id, code_name, active_flag, memo) values ('6', 'CD', @code_id + 1, @code_name, 1, '車行')
	";
                    await cn.ExecuteAsync(sql, new { code_name });
                }
                catch (Exception e)
                {
                
                }
                
            }

        }
        public async Task InsertIncomeFeeType(string code_name)
        {

            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                string sql = @"
                   declare @code_id int;
                    set @code_id = (select top 1 code_id from tbItemCodes where code_bclass = '6' and code_sclass = 'fee_type' and code_id<> '99' order by cast(code_id as int) desc);
                    insert into tbItemCodes (code_bclass, code_sclass ,code_id, code_name, active_flag, memo) values ('6', 'fee_type', @code_id + 1, @code_name, 1, '費用類別-收入')
	";
                await cn.ExecuteAsync(sql, new { code_name });
            }

        }

        public async Task InsertExpenditureFeeType(string code_name)
        {

            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                string sql = @"
                 declare @code_id int;
                    set @code_id = (select top 1 code_id from tbItemCodes where code_bclass = '6' and code_sclass = 'fee_type' and code_id<> '99' order by cast(code_id as int) desc);
                    insert into tbItemCodes (code_bclass, code_sclass ,code_id, code_name, active_flag, memo) values ('6', 'fee_type', @code_id + 1, @code_name, 1, '費用類別-支出')
	";
                await cn.ExecuteAsync(sql, new { code_name });
            }

        }

        public async Task UpdatetbItemCodes(tbItemCodes_Condition _condition)
        {

            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                string sql = @"
  update tbItemCodes set code_name = @code_name, active_flag = @active_flag where seq = @seq";
                await cn.ExecuteAsync(sql, _condition);
            }

        }


    }
}
