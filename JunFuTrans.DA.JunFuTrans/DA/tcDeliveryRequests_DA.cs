﻿using Dapper;
using JunFuTrans.DA.JunFuTrans.condition;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace JunFuTrans.DA.JunFuTrans.DA
{
    public class tcDeliveryRequests_DA
    {
        private static IConfiguration Configs()
        { 
            try
            {
                return new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            }
            catch 
            {
                return new ConfigurationBuilder().AddJsonFile("app.json").Build();
            }
            
            
        }
        IConfiguration configs = Configs();
     

        public List<tcDeliveryRequests_Condition> GettcDeliveryRequestsBycheck_number(string check_number)
        {

            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[tcDeliveryRequests] WITH (NOLOCK)
                        WHERE check_number = @check_number
                    "; 
               return cn.Query<tcDeliveryRequests_Condition>(sql, new { check_number }).AsList();
            }

        }
      

    }
}
