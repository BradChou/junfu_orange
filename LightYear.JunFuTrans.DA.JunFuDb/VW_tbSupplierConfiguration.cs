﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
    public class VW_tbSupplierConfiguration : IEntityTypeConfiguration<VW_tbSupplier>
    {
        public void Configure(EntityTypeBuilder<VW_tbSupplier> builder)
        {
            builder.ToView("VW_tbSupplier", "dbo");
            builder.HasNoKey();

            builder.Property(x => x.supplier_id).HasColumnName(@"supplier_id").HasColumnType("numeric(18, 0)").IsRequired(true);
            builder.Property(x => x.IDNO).HasColumnName(@"IDNO").HasColumnType("nvarchar(10)").IsRequired(true).HasMaxLength(10);
            builder.Property(x => x.vendorName).HasColumnName(@"supplier_name").HasColumnType("nvarchar(20)").HasMaxLength(20);
            builder.Property(x => x.headquartersType).HasColumnName(@"headquartersType").HasColumnType("bit").IsRequired(false);
            builder.Property(x => x.headquarters).HasColumnName(@"headquarters").HasColumnType("nvarchar(50)").IsRequired(false).HasMaxLength(50);

        }
    }
}
