// ------------------------------------------------------------------------------------------------
// This code was generated by EntityFramework Reverse POCO Generator (http://www.reversepoco.co.uk/).
// Created by Simon Hughes (https://about.me/simon.hughes).
//
// Registered to: Light-Year Technology
// Company      : Light-Year Technology
// Licence Type : Commercial
// Licences     : 1
// Valid until  : 21 五月 2025
//
// Do not make changes directly to this file - edit the template instead.
//
// The following connection settings were used to generate this file:
//     Connection String Name: "JunFuDbContext"
//     Connection String:      "Data Source=junfurdsdb.cioqpfywhsfk.us-east-2.rds.amazonaws.com,1433;Initial Catalog=JunFuReal;Persist Security Info=True;User ID=admin;password=**zapped**;;MultiSubnetFailover=Yes;"
// ------------------------------------------------------------------------------------------------
// Database Edition       : Standard Edition (64-bit)
// Database Engine Edition: Standard
// Database Version       : 13.0.5820.21

// <auto-generated>
// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable NotAccessedVariable
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantCast
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// ReSharper disable UsePatternMatching
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
    // ttDeliveryRequestsRecord
    /// <summary>
    /// 托運記錄表
    /// </summary>
    public class TtDeliveryRequestsRecord
    {
        /// <summary>
        /// 主鍵
        /// </summary>
        public long Id { get; set; } // id (Primary key)

        /// <summary>
        /// 建檔日期
        /// </summary>
        public DateTime? CreateDate { get; set; } // create_date

        /// <summary>
        /// 建檔人員
        /// </summary>
        public string CreateUser { get; set; } // create_user (length: 20)

        /// <summary>
        /// 修改日期
        /// </summary>
        public DateTime? ModifyDate { get; set; } // modify_date

        /// <summary>
        /// 修改人員
        /// </summary>
        public string ModifyUser { get; set; } // modify_user (length: 20)

        /// <summary>
        /// 刪除日期
        /// </summary>
        public DateTime? DeleteDate { get; set; } // delete_date

        /// <summary>
        /// 刪除人員
        /// </summary>
        public string DeleteUser { get; set; } // delete_user (length: 20)

        /// <summary>
        /// 托運序號(ttDeliveryRequest 的主鍵)
        /// </summary>
        public decimal RequestId { get; set; } // request_id

        /// <summary>
        /// 計價方式(待定)
        /// </summary>
        public string PricingType { get; set; } // pricing_type (length: 2)

        /// <summary>
        /// 客戶編號
        /// </summary>
        public string CustomerCode { get; set; } // customer_code (length: 20)

        /// <summary>
        /// 條碼號
        /// </summary>
        public string CheckNumber { get; set; } // check_number (length: 20)

        /// <summary>
        /// 託運類別(號碼待定)
        /// </summary>
        public string CheckType { get; set; } // check_type (length: 10)

        /// <summary>
        /// 訂單號碼
        /// </summary>
        public string OrderNumber { get; set; } // order_number (length: 20)

        /// <summary>
        /// 收件人客代
        /// </summary>
        public string ReceiveCustomerCode { get; set; } // receive_customer_code (length: 20)

        /// <summary>
        /// 不明
        /// </summary>
        public string SubpoenaCategory { get; set; } // subpoena_category (length: 10)

        /// <summary>
        /// 收件人電話
        /// </summary>
        public string ReceiveTel1 { get; set; } // receive_tel1 (length: 20)

        /// <summary>
        /// 收件人分機
        /// </summary>
        public string ReceiveTel1Ext { get; set; } // receive_tel1_ext (length: 10)

        /// <summary>
        /// 收件人電話2
        /// </summary>
        public string ReceiveTel2 { get; set; } // receive_tel2 (length: 20)

        /// <summary>
        /// 收件連絡人
        /// </summary>
        public string ReceiveContact { get; set; } // receive_contact (length: 20)

        /// <summary>
        /// 收件縣市
        /// </summary>
        public string ReceiveCity { get; set; } // receive_city (length: 10)

        /// <summary>
        /// 收件區(鄉鎮)
        /// </summary>
        public string ReceiveArea { get; set; } // receive_area (length: 10)

        /// <summary>
        /// 收件地址
        /// </summary>
        public string ReceiveAddress { get; set; } // receive_address (length: 50)

        /// <summary>
        /// 到著區碼
        /// </summary>
        public string AreaArriveCode { get; set; } // area_arrive_code (length: 3)

        /// <summary>
        /// 到著標記(待定義)
        /// </summary>
        public bool? ReceiveByArriveSiteFlag { get; set; } // receive_by_arrive_site_flag

        /// <summary>
        /// 到著地址
        /// </summary>
        public string ArriveAddress { get; set; } // arrive_address (length: 100)

        /// <summary>
        /// 件數
        /// </summary>
        public int? Pieces { get; set; } // pieces

        /// <summary>
        /// 板數
        /// </summary>
        public int? Plates { get; set; } // plates
        public int? Cbm { get; set; } // cbm

        /// <summary>
        /// 收款金額
        /// </summary>
        public int? CollectionMoney { get; set; } // collection_money

        /// <summary>
        /// 到著收取運費
        /// </summary>
        public int? ArriveToPayFreight { get; set; } // arrive_to_pay_freight

        /// <summary>
        /// 到著收取其它費用
        /// </summary>
        public int? ArriveToPayAppend { get; set; } // arrive_to_pay_append

        /// <summary>
        /// 寄件連絡人
        /// </summary>
        public string SendContact { get; set; } // send_contact (length: 20)

        /// <summary>
        /// 寄件連絡電話
        /// </summary>
        public string SendTel { get; set; } // send_tel (length: 20)

        /// <summary>
        /// 寄件縣市
        /// </summary>
        public string SendCity { get; set; } // send_city (length: 10)

        /// <summary>
        /// 寄件區(鄉鎮)
        /// </summary>
        public string SendArea { get; set; } // send_area (length: 10)

        /// <summary>
        /// 寄件地址
        /// </summary>
        public string SendAddress { get; set; } // send_address (length: 50)

        /// <summary>
        /// 捐贈發票標記
        /// </summary>
        public bool? DonateInvoiceFlag { get; set; } // donate_invoice_flag

        /// <summary>
        /// 電子發票標記
        /// </summary>
        public bool? ElectronicInvoiceFlag { get; set; } // electronic_invoice_flag
        public string UniformNumbers { get; set; } // uniform_numbers (length: 8)

        /// <summary>
        /// 到著連絡人手機
        /// </summary>
        public string ArriveMobile { get; set; } // arrive_mobile (length: 20)

        /// <summary>
        /// 到著連絡人email
        /// </summary>
        public string ArriveEmail { get; set; } // arrive_email (length: 50)

        /// <summary>
        /// 發票備註
        /// </summary>
        public string InvoiceMemo { get; set; } // invoice_memo (length: 100)

        /// <summary>
        /// 發票說明
        /// </summary>
        public string InvoiceDesc { get; set; } // invoice_desc (length: 200)

        /// <summary>
        /// 產品類別(待定義)
        /// </summary>
        public string ProductCategory { get; set; } // product_category (length: 10)
        public string SpecialSend { get; set; } // special_send (length: 10)

        /// <summary>
        /// 指定配達日
        /// </summary>
        public DateTime? ArriveAssignDate { get; set; } // arrive_assign_date

        /// <summary>
        /// 指定配達時段
        /// </summary>
        public string TimePeriod { get; set; } // time_period (length: 10)

        /// <summary>
        /// 收件註記(未定義)
        /// </summary>
        public bool? ReceiptFlag { get; set; } // receipt_flag

        /// <summary>
        /// 棧板回收註記
        /// </summary>
        public bool? PalletRecyclingFlag { get; set; } // pallet_recycling_flag

        /// <summary>
        /// 供應商簡碼
        /// </summary>
        public string SupplierCode { get; set; } // supplier_code (length: 3)

        /// <summary>
        /// 供應商名稱
        /// </summary>
        public string SupplierName { get; set; } // supplier_name (length: 20)

        /// <summary>
        /// 供應商日期(未定義)
        /// </summary>
        public DateTime? SupplierDate { get; set; } // supplier_date
        public int? ReceiptNumbe { get; set; } // receipt_numbe

        /// <summary>
        /// 供應商費用
        /// </summary>
        public int? SupplierFee { get; set; } // supplier_fee
        public int? CsectionFee { get; set; } // csection_fee
        public int? RemoteFee { get; set; } // remote_fee

        /// <summary>
        /// 總費用
        /// </summary>
        public int? TotalFee { get; set; } // total_fee

        /// <summary>
        /// 列印日期
        /// </summary>
        public DateTime? PrintDate { get; set; } // print_date

        /// <summary>
        /// 列印註記
        /// </summary>
        public bool? PrintFlag { get; set; } // print_flag
        public DateTime? CheckoutCloseDate { get; set; } // checkout_close_date
        public string SubCheckNumber { get; set; } // sub_check_number (length: 3)

        /// <summary>
        /// 進站隨機碼
        /// </summary>
        public string ImportRandomCode { get; set; } // import_randomCode (length: 10)

        /// <summary>
        /// 結案隨機碼
        /// </summary>
        public string CloseRandomCode { get; set; } // close_randomCode (length: 10)

        /// <summary>
        /// 異動時間
        /// </summary>
        public DateTime? RecordDate { get; set; } // record_date

        /// <summary>
        /// I(Insert-新增) / U(Update-修改) / D(刪除，假刪視同刪除)
        /// </summary>
        public string RecordAction { get; set; } // record_action (length: 1)

        /// <summary>
        /// 異動說明
        /// </summary>
        public string RecordMemo { get; set; } // record_memo (length: 50)

        public TtDeliveryRequestsRecord()
        {
            RecordDate = DateTime.Now;
        }
    }

}
// </auto-generated>

