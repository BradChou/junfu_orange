// ------------------------------------------------------------------------------------------------
// This code was generated by EntityFramework Reverse POCO Generator (http://www.reversepoco.co.uk/).
// Created by Simon Hughes (https://about.me/simon.hughes).
//
// Registered to: Light-Year Technology
// Company      : Light-Year Technology
// Licence Type : Commercial
// Licences     : 1
// Valid until  : 21 五月 2025
//
// Do not make changes directly to this file - edit the template instead.
//
// The following connection settings were used to generate this file:
//     Connection String Name: "JunFuDbContext"
//     Connection String:      "Data Source=junfurdsdb.cioqpfywhsfk.us-east-2.rds.amazonaws.com,1433;Initial Catalog=JunFuReal;Persist Security Info=True;User ID=admin;password=**zapped**;;MultiSubnetFailover=Yes;"
// ------------------------------------------------------------------------------------------------
// Database Edition       : Standard Edition (64-bit)
// Database Engine Edition: Standard
// Database Version       : 13.0.5820.21

// <auto-generated>
// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable NotAccessedVariable
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantCast
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// ReSharper disable UsePatternMatching
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
    // tbDeliveryNumberSetting
    /// <summary>
    /// 配送號碼設定
    /// </summary>
    public class TbDeliveryNumberSetting
    {
        /// <summary>
        /// 序號
        /// </summary>
        public int Seq { get; set; } // seq (Primary key)

        /// <summary>
        /// 客代編號 (關連 tbCustomer.customer_code)
        /// </summary>
        public string CustomerCode { get; set; } // customer_code (length: 20)

        /// <summary>
        /// 起始貨號
        /// </summary>
        public long? BeginNumber { get; set; } // begin_number

        /// <summary>
        /// 結束貨號
        /// </summary>
        public long? EndNumber { get; set; } // end_number

        /// <summary>
        /// 目前取號
        /// </summary>
        public long? CurrentNumber { get; set; } // current_number

        /// <summary>
        /// 異動人員
        /// </summary>
        public string Cuser { get; set; } // cuser (length: 10)

        /// <summary>
        /// 異動時間
        /// </summary>
        public DateTime? Cdate { get; set; } // cdate

        /// <summary>
        /// 是否啟用
        /// </summary>
        public int? IsActive { get; set; } // IsActive
        public int? Num { get; set; } // num

        public TbDeliveryNumberSetting()
        {
            CurrentNumber = 0;
            Cdate = DateTime.Now;
            IsActive = 1;
        }
    }

}
// </auto-generated>

