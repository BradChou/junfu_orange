// ------------------------------------------------------------------------------------------------
// This code was generated by EntityFramework Reverse POCO Generator (http://www.reversepoco.co.uk/).
// Created by Simon Hughes (https://about.me/simon.hughes).
//
// Registered to: Light-Year Technology
// Company      : Light-Year Technology
// Licence Type : Commercial
// Licences     : 1
// Valid until  : 21 五月 2025
//
// Do not make changes directly to this file - edit the template instead.
//
// The following connection settings were used to generate this file:
//     Connection String Name: "JunFuDbContext"
//     Connection String:      "Data Source=junfurdsdb.cioqpfywhsfk.us-east-2.rds.amazonaws.com,1433;Initial Catalog=JunFuReal;Persist Security Info=True;User ID=admin;password=**zapped**;;MultiSubnetFailover=Yes;"
// ------------------------------------------------------------------------------------------------
// Database Edition       : Standard Edition (64-bit)
// Database Engine Edition: Standard
// Database Version       : 13.0.5820.21

// <auto-generated>
// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable NotAccessedVariable
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantCast
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// ReSharper disable UsePatternMatching
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
    // tbFunctions
    /// <summary>
    /// 服務功能
    /// </summary>
    public class TbFunction
    {
        /// <summary>
        /// 功能編號
        /// </summary>
        public decimal FuncId { get; set; } // func_id (Primary key)

        /// <summary>
        /// 功能代碼
        /// </summary>
        public string FuncCode { get; set; } // func_code (length: 10)

        /// <summary>
        /// 功能名稱
        /// </summary>
        public string FuncName { get; set; } // func_name (length: 30)

        /// <summary>
        /// 大寫代碼
        /// </summary>
        public string UpperLevelCode { get; set; } // upper_level_code (length: 10)

        /// <summary>
        /// 層別
        /// </summary>
        public int? Level { get; set; } // Level

        /// <summary>
        /// 功能連結
        /// </summary>
        public string FuncLink { get; set; } // func_link (length: 50)

        /// <summary>
        /// css類別
        /// </summary>
        public string Cssclass { get; set; } // cssclass (length: 50)

        /// <summary>
        /// 廠商
        /// </summary>
        public string Cuser { get; set; } // cuser (length: 20)

        /// <summary>
        /// 廠商使用日期
        /// </summary>
        public DateTime? Cdate { get; set; } // cdate

        /// <summary>
        /// 使用者
        /// </summary>
        public string Uuser { get; set; } // uuser (length: 20)

        /// <summary>
        /// 使用者使用日期
        /// </summary>
        public DateTime? Udate { get; set; } // udate

        /// <summary>
        /// 分類
        /// </summary>
        public int? Sort { get; set; } // sort

        /// <summary>
        /// 類型 0:棧板 1:零擔
        /// </summary>
        public int? Type { get; set; } // type
        public bool? Visible { get; set; } // visible

        public TbFunction()
        {
            Type = 0;
            Visible = true;
        }
    }

}
// </auto-generated>

