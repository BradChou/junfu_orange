// ------------------------------------------------------------------------------------------------
// This code was generated by EntityFramework Reverse POCO Generator (http://www.reversepoco.co.uk/).
// Created by Simon Hughes (https://about.me/simon.hughes).
//
// Registered to: Light-Year Technology
// Company      : Light-Year Technology
// Licence Type : Commercial
// Licences     : 1
// Valid until  : 21 五月 2025
//
// Do not make changes directly to this file - edit the template instead.
//
// The following connection settings were used to generate this file:
//     Connection String Name: "JunFuDbContext"
//     Connection String:      "Data Source=junfurdsdb.cioqpfywhsfk.us-east-2.rds.amazonaws.com,1433;Initial Catalog=JunFuReal;Persist Security Info=True;User ID=admin;password=**zapped**;;MultiSubnetFailover=Yes;"
// ------------------------------------------------------------------------------------------------
// Database Edition       : Standard Edition (64-bit)
// Database Engine Edition: Standard
// Database Version       : 13.0.5820.21

// <auto-generated>
// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable NotAccessedVariable
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantCast
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// ReSharper disable UsePatternMatching
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
    // tbIORecords
    public class TbIoRecordConfiguration : IEntityTypeConfiguration<TbIoRecord>
    {
        public void Configure(EntityTypeBuilder<TbIoRecord> builder)
        {
            builder.ToTable("tbIORecords", "dbo");
            builder.HasKey(x => x.Id).HasName("PK_tbIORecords").IsClustered();

            builder.Property(x => x.Id).HasColumnName(@"id").HasColumnType("int").IsRequired().ValueGeneratedOnAdd().UseIdentityColumn();
            builder.Property(x => x.Cuser).HasColumnName(@"cuser").HasColumnType("nvarchar(20)").IsRequired().HasMaxLength(20);
            builder.Property(x => x.Cdate).HasColumnName(@"cdate").HasColumnType("datetime").IsRequired(false);
            builder.Property(x => x.Uuser).HasColumnName(@"uuser").HasColumnType("nvarchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.Udate).HasColumnName(@"udate").HasColumnType("datetime").IsRequired(false);
            builder.Property(x => x.Type).HasColumnName(@"type").HasColumnType("int").IsRequired();
            builder.Property(x => x.ChangeTable).HasColumnName(@"changeTable").HasColumnType("nvarchar(20)").IsRequired().HasMaxLength(20);
            builder.Property(x => x.FromWhere).HasColumnName(@"fromWhere").HasColumnType("nvarchar(50)").IsRequired(false).HasMaxLength(50);
            builder.Property(x => x.RandomCode).HasColumnName(@"randomCode").HasColumnType("nvarchar(10)").IsRequired(false).HasMaxLength(10);
            builder.Property(x => x.Memo).HasColumnName(@"memo").HasColumnType("nvarchar(100)").IsRequired().HasMaxLength(100);
            builder.Property(x => x.SuccessNum).HasColumnName(@"successNum").HasColumnType("int").IsRequired(false);
            builder.Property(x => x.FailNum).HasColumnName(@"failNum").HasColumnType("int").IsRequired(false);
            builder.Property(x => x.TotalNum).HasColumnName(@"totalNum").HasColumnType("int").IsRequired();
            builder.Property(x => x.StartTime).HasColumnName(@"startTime").HasColumnType("datetime").IsRequired(false);
            builder.Property(x => x.EndTime).HasColumnName(@"endTime").HasColumnType("datetime").IsRequired(false);
            builder.Property(x => x.Descript).HasColumnName(@"descript").HasColumnType("nvarchar(1000)").IsRequired(false).HasMaxLength(1000);
            builder.Property(x => x.Result).HasColumnName(@"result").HasColumnType("bit").IsRequired(false);
            builder.Property(x => x.CustomerCode).HasColumnName(@"customer_code").HasColumnType("nvarchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.PrintDate).HasColumnName(@"print_date").HasColumnType("datetime").IsRequired(false);
            builder.Property(x => x.Status).HasColumnName(@"status").HasColumnType("int").IsRequired(false);

            builder.HasIndex(x => x.Id).HasName("NonClusteredIndex-20200604-165131");
        }
    }

}
// </auto-generated>

