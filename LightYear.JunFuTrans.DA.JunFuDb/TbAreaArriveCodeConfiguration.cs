// ------------------------------------------------------------------------------------------------
// This code was generated by EntityFramework Reverse POCO Generator (http://www.reversepoco.co.uk/).
// Created by Simon Hughes (https://about.me/simon.hughes).
//
// Registered to: Light-Year Technology
// Company      : Light-Year Technology
// Licence Type : Commercial
// Licences     : 1
// Valid until  : 21 五月 2025
//
// Do not make changes directly to this file - edit the template instead.
//
// The following connection settings were used to generate this file:
//     Connection String Name: "JunFuDbContext"
//     Connection String:      "Data Source=junfurdsdb.cioqpfywhsfk.us-east-2.rds.amazonaws.com,1433;Initial Catalog=JunFuReal;Persist Security Info=True;User ID=admin;password=**zapped**;;MultiSubnetFailover=Yes;"
// ------------------------------------------------------------------------------------------------
// Database Edition       : Standard Edition (64-bit)
// Database Engine Edition: Standard
// Database Version       : 13.0.5820.21

// <auto-generated>
// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable NotAccessedVariable
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantCast
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// ReSharper disable UsePatternMatching
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
    // tbAreaArriveCodes
    public class TbAreaArriveCodeConfiguration : IEntityTypeConfiguration<TbAreaArriveCode>
    {
        public void Configure(EntityTypeBuilder<TbAreaArriveCode> builder)
        {
            builder.ToTable("tbAreaArriveCodes", "dbo");
            builder.HasKey(x => x.ArriveId).HasName("PK_tbAreaArriveCode").IsClustered();

            builder.Property(x => x.ArriveId).HasColumnName(@"arrive_id").HasColumnType("numeric(18,0)").IsRequired().ValueGeneratedOnAdd();
            builder.Property(x => x.SupplierCode).HasColumnName(@"supplier_code").HasColumnType("nvarchar(3)").IsRequired(false).HasMaxLength(3);
            builder.Property(x => x.SupplierName).HasColumnName(@"supplier_name").HasColumnType("nvarchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.ArriveCode).HasColumnName(@"arrive_code").HasColumnType("nvarchar(5)").IsRequired(false).HasMaxLength(5);
            builder.Property(x => x.ArriveAddress).HasColumnName(@"arrive_address").HasColumnType("nvarchar(50)").IsRequired(false).HasMaxLength(50);
            builder.Property(x => x.City).HasColumnName(@"city").HasColumnType("nvarchar(10)").IsRequired(false).HasMaxLength(10);
            builder.Property(x => x.Area).HasColumnName(@"area").HasColumnType("nvarchar(10)").IsRequired(false).HasMaxLength(10);
        }
    }

}
// </auto-generated>

