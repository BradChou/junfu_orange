// ------------------------------------------------------------------------------------------------
// This code was generated by EntityFramework Reverse POCO Generator (http://www.reversepoco.co.uk/).
// Created by Simon Hughes (https://about.me/simon.hughes).
//
// Registered to: Light-Year Technology
// Company      : Light-Year Technology
// Licence Type : Commercial
// Licences     : 1
// Valid until  : 21 五月 2025
//
// Do not make changes directly to this file - edit the template instead.
//
// The following connection settings were used to generate this file:
//     Connection String Name: "JunFuDbContext"
//     Connection String:      "Data Source=junfurdsdb.cioqpfywhsfk.us-east-2.rds.amazonaws.com,1433;Initial Catalog=JunFuReal;Persist Security Info=True;User ID=admin;password=**zapped**;;MultiSubnetFailover=Yes;"
// ------------------------------------------------------------------------------------------------
// Database Edition       : Standard Edition (64-bit)
// Database Engine Edition: Standard
// Database Version       : 13.0.5820.21

// <auto-generated>
// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable NotAccessedVariable
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantCast
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// ReSharper disable UsePatternMatching
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
    // oil_price_balance
    public class OilPriceBalanceConfiguration : IEntityTypeConfiguration<OilPriceBalance>
    {
        public void Configure(EntityTypeBuilder<OilPriceBalance> builder)
        {
            builder.ToTable("oil_price_balance", "dbo");
            builder.HasKey(x => new { x.StepDistance, x.MinOilPrice, x.MaxOilPrice, x.OnePalletPriceBalance, x.TwoPalletPriceBalance, x.ThreePalletPriceBalance, x.FourPalletPriceBalance, x.FivePalletPriceBalance, x.SixPalletPriceBalance });

            builder.Property(x => x.StepDistance).HasColumnName(@"step_distance").HasColumnType("tinyint").IsRequired().ValueGeneratedNever();
            builder.Property(x => x.MinOilPrice).HasColumnName(@"min_oil_price").HasColumnType("float").IsRequired().ValueGeneratedNever();
            builder.Property(x => x.MaxOilPrice).HasColumnName(@"max_oil_price").HasColumnType("float").IsRequired().ValueGeneratedNever();
            builder.Property(x => x.OnePalletPriceBalance).HasColumnName(@"one_pallet_price_balance").HasColumnType("smallint").IsRequired().ValueGeneratedNever();
            builder.Property(x => x.TwoPalletPriceBalance).HasColumnName(@"two_pallet_price_balance").HasColumnType("smallint").IsRequired().ValueGeneratedNever();
            builder.Property(x => x.ThreePalletPriceBalance).HasColumnName(@"three_pallet_price_balance").HasColumnType("smallint").IsRequired().ValueGeneratedNever();
            builder.Property(x => x.FourPalletPriceBalance).HasColumnName(@"four_pallet_price_balance").HasColumnType("smallint").IsRequired().ValueGeneratedNever();
            builder.Property(x => x.FivePalletPriceBalance).HasColumnName(@"five_pallet_price_balance").HasColumnType("smallint").IsRequired().ValueGeneratedNever();
            builder.Property(x => x.SixPalletPriceBalance).HasColumnName(@"six_pallet_price_balance").HasColumnType("smallint").IsRequired().ValueGeneratedNever();
        }
    }

}
// </auto-generated>

