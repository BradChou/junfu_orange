// ------------------------------------------------------------------------------------------------
// This code was generated by EntityFramework Reverse POCO Generator (http://www.reversepoco.co.uk/).
// Created by Simon Hughes (https://about.me/simon.hughes).
//
// Registered to: Light-Year Technology
// Company      : Light-Year Technology
// Licence Type : Commercial
// Licences     : 1
// Valid until  : 21 五月 2025
//
// Do not make changes directly to this file - edit the template instead.
//
// The following connection settings were used to generate this file:
//     Connection String Name: "JunFuDbContext"
//     Connection String:      "Data Source=junfurdsdb.cioqpfywhsfk.us-east-2.rds.amazonaws.com,1433;Initial Catalog=JunFuReal;Persist Security Info=True;User ID=admin;password=**zapped**;;MultiSubnetFailover=Yes;"
// ------------------------------------------------------------------------------------------------
// Database Edition       : Standard Edition (64-bit)
// Database Engine Edition: Standard
// Database Version       : 13.0.5820.21

// <auto-generated>
// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable NotAccessedVariable
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantCast
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// ReSharper disable UsePatternMatching
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
    // ttAssetsOil_bak
    /// <summary>
    /// 資產油巴克表
    /// </summary>
    public class TtAssetsOilBak
    {
        public int Id { get; set; } // id (Primary key)

        /// <summary>
        /// 車輛編號 關連(ttAssets.a_id)
        /// </summary>
        public int? AId { get; set; } // a_id

        /// <summary>
        /// 油卡
        /// </summary>
        public string Oil { get; set; } // oil (length: 2)

        /// <summary>
        /// 油卡編號
        /// </summary>
        public string CardId { get; set; } // card_id (length: 10)

        /// <summary>
        /// 啟用日期
        /// </summary>
        public DateTime? ActiveDate { get; set; } // active_date

        /// <summary>
        /// 停用日期
        /// </summary>
        public DateTime? StopDate { get; set; } // stop_date

        /// <summary>
        /// 日期
        /// </summary>
        public DateTime? Cdate { get; set; } // cdate

        public TtAssetsOilBak()
        {
            Cdate = DateTime.Now;
        }
    }

}
// </auto-generated>

