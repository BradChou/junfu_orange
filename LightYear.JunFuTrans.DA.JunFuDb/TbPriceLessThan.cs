// ------------------------------------------------------------------------------------------------
// This code was generated by EntityFramework Reverse POCO Generator (http://www.reversepoco.co.uk/).
// Created by Simon Hughes (https://about.me/simon.hughes).
//
// Registered to: Light-Year Technology
// Company      : Light-Year Technology
// Licence Type : Commercial
// Licences     : 1
// Valid until  : 21 五月 2025
//
// Do not make changes directly to this file - edit the template instead.
//
// The following connection settings were used to generate this file:
//     Connection String Name: "JunFuDbContext"
//     Connection String:      "Data Source=junfurdsdb.cioqpfywhsfk.us-east-2.rds.amazonaws.com,1433;Initial Catalog=JunFuReal;Persist Security Info=True;User ID=admin;password=**zapped**;;MultiSubnetFailover=Yes;"
// ------------------------------------------------------------------------------------------------
// Database Edition       : Standard Edition (64-bit)
// Database Engine Edition: Standard
// Database Version       : 13.0.5820.21

// <auto-generated>
// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable NotAccessedVariable
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantCast
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// ReSharper disable UsePatternMatching
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
    // tbPriceLessThan
    /// <summary>
    /// 運貨價格
    /// </summary>
    public class TbPriceLessThan
    {
        public long Id { get; set; } // id (Primary key)

        /// <summary>
        /// 起始城市
        /// </summary>
        public string StartCity { get; set; } // start_city (length: 10)

        /// <summary>
        /// 終點城市
        /// </summary>
        public string EndCity { get; set; } // end_city (length: 10)

        /// <summary>
        /// 材積大小
        /// </summary>
        public int? Cbmsize { get; set; } // Cbmsize

        /// <summary>
        /// 首件運費
        /// </summary>
        public int? FirstPrice { get; set; } // first_price

        /// <summary>
        /// 續件運費
        /// </summary>
        public int? AddPrice { get; set; } // add_price

        /// <summary>
        /// 費用級別 (1: 經理  2:協理  3:總經理)
        /// </summary>
        public string Business { get; set; } // business (length: 10)

        /// <summary>
        /// 日期
        /// </summary>
        public DateTime? Cdate { get; set; } // cdate

        /// <summary>
        /// 建立人員
        /// </summary>
        public string Cuser { get; set; } // cuser (length: 20)

        /// <summary>
        /// 啟用日期
        /// </summary>
        public DateTime? EnableDate { get; set; } // Enable_date

        /// <summary>
        /// 顧客代碼
        /// </summary>
        public string CustomerCode { get; set; } // customer_code (length: 12)

        public TbPriceLessThan()
        {
            Cdate = DateTime.Now;
        }
    }

}
// </auto-generated>

