// ------------------------------------------------------------------------------------------------
// This code was generated by EntityFramework Reverse POCO Generator (http://www.reversepoco.co.uk/).
// Created by Simon Hughes (https://about.me/simon.hughes).
//
// Registered to: Light-Year Technology
// Company      : Light-Year Technology
// Licence Type : Commercial
// Licences     : 1
// Valid until  : 21 五月 2025
//
// Do not make changes directly to this file - edit the template instead.
//
// The following connection settings were used to generate this file:
//     Connection String Name: "JunFuDbContext"
//     Connection String:      "Data Source=junfurdsdb.cioqpfywhsfk.us-east-2.rds.amazonaws.com,1433;Initial Catalog=JunFuReal;Persist Security Info=True;User ID=admin;password=**zapped**;;MultiSubnetFailover=Yes;"
// ------------------------------------------------------------------------------------------------
// Database Edition       : Standard Edition (64-bit)
// Database Engine Edition: Standard
// Database Version       : 13.0.5820.21

// <auto-generated>
// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable NotAccessedVariable
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantCast
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// ReSharper disable UsePatternMatching
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
    // b_section_extra_run
    public class BSectionExtraRunConfiguration : IEntityTypeConfiguration<BSectionExtraRun>
    {
        public void Configure(EntityTypeBuilder<BSectionExtraRun> builder)
        {
            builder.ToTable("b_section_extra_run", "dbo");
            builder.HasKey(x => x.Id).HasName("PK__b_sectio__3213E83F556DB4DC").IsClustered();

            builder.Property(x => x.Id).HasColumnName(@"id").HasColumnType("int").IsRequired().ValueGeneratedOnAdd().UseIdentityColumn();
            builder.Property(x => x.FromId).HasColumnName(@"from_id").HasColumnType("int").IsRequired();
            builder.Property(x => x.ToId).HasColumnName(@"to_id").HasColumnType("int").IsRequired();
            builder.Property(x => x.CarTypeId).HasColumnName(@"car_type_id").HasColumnType("int").IsRequired();
            builder.Property(x => x.RunFlowTypeId).HasColumnName(@"run_flow_type_id").HasColumnType("int").IsRequired();
            builder.Property(x => x.GoStartAt).HasColumnName(@"go_start_at").HasColumnType("datetime").IsRequired();
            builder.Property(x => x.GoEndAt).HasColumnName(@"go_end_at").HasColumnType("datetime").IsRequired();
            builder.Property(x => x.BackStartAt).HasColumnName(@"back_start_at").HasColumnType("datetime").IsRequired();
            builder.Property(x => x.BackEndAt).HasColumnName(@"back_end_at").HasColumnType("datetime").IsRequired();
            builder.Property(x => x.IsLtl).HasColumnName(@"is_ltl").HasColumnType("bit").IsRequired();
            builder.Property(x => x.IsDeleted).HasColumnName(@"is_deleted").HasColumnType("bit").IsRequired(false);
            builder.Property(x => x.CreatedAt).HasColumnName(@"created_at").HasColumnType("datetime").IsRequired();

            // Foreign keys
            builder.HasOne(a => a.CarType).WithMany(b => b.BSectionExtraRuns).HasForeignKey(c => c.CarTypeId).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK__b_section__car_t__21D6CC45");
            builder.HasOne(a => a.From).WithMany(b => b.BSectionExtraRuns_FromId).HasForeignKey(c => c.FromId).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK__b_section__from___22CAF07E");
            builder.HasOne(a => a.RunFlowType).WithMany(b => b.BSectionExtraRuns).HasForeignKey(c => c.RunFlowTypeId).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK__b_section__run_f__23BF14B7");
            builder.HasOne(a => a.To).WithMany(b => b.BSectionExtraRuns_ToId).HasForeignKey(c => c.ToId).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK__b_section__to_id__24B338F0");
        }
    }

}
// </auto-generated>

