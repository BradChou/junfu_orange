// ------------------------------------------------------------------------------------------------
// This code was generated by EntityFramework Reverse POCO Generator (http://www.reversepoco.co.uk/).
// Created by Simon Hughes (https://about.me/simon.hughes).
//
// Registered to: Light-Year Technology
// Company      : Light-Year Technology
// Licence Type : Commercial
// Licences     : 1
// Valid until  : 21 五月 2025
//
// Do not make changes directly to this file - edit the template instead.
//
// The following connection settings were used to generate this file:
//     Connection String Name: "JunFuDbContext"
//     Connection String:      "Data Source=junfurdsdb.cioqpfywhsfk.us-east-2.rds.amazonaws.com,1433;Initial Catalog=JunFuReal;Persist Security Info=True;User ID=admin;password=**zapped**;;MultiSubnetFailover=Yes;"
// ------------------------------------------------------------------------------------------------
// Database Edition       : Standard Edition (64-bit)
// Database Engine Edition: Standard
// Database Version       : 13.0.5820.21

// <auto-generated>
// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable NotAccessedVariable
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantCast
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// ReSharper disable UsePatternMatching
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
    // ttTransferLog
    public class TtTransferLogConfiguration : IEntityTypeConfiguration<TtTransferLog>
    {
        public void Configure(EntityTypeBuilder<TtTransferLog> builder)
        {
            builder.ToTable("ttTransferLog", "dbo");
            builder.HasKey(x => x.TransferId).HasName("PK_ttTransferLog").IsClustered();

            builder.Property(x => x.TransferId).HasColumnName(@"TransferID").HasColumnType("int").IsRequired().ValueGeneratedOnAdd().UseIdentityColumn();
            builder.Property(x => x.StationCode).HasColumnName(@"station_code").HasColumnType("nvarchar(10)").IsRequired(false).HasMaxLength(10);
            builder.Property(x => x.DriverCode).HasColumnName(@"driver_code").HasColumnType("nvarchar(25)").IsRequired(false).HasMaxLength(25);
            builder.Property(x => x.ScanningDt).HasColumnName(@"scanning_dt").HasColumnType("datetime").IsRequired(false);
            builder.Property(x => x.CheckNumber).HasColumnName(@"check_number").HasColumnType("nvarchar(50)").IsRequired(false).HasMaxLength(50);
            builder.Property(x => x.CreateDt).HasColumnName(@"create_dt").HasColumnType("datetime").IsRequired(false);
            builder.Property(x => x.Transfer).HasColumnName(@"Transfer").HasColumnType("nvarchar(20)").IsRequired(false).HasMaxLength(20);
        }
    }

}
// </auto-generated>

