// ------------------------------------------------------------------------------------------------
// This code was generated by EntityFramework Reverse POCO Generator (http://www.reversepoco.co.uk/).
// Created by Simon Hughes (https://about.me/simon.hughes).
//
// Registered to: Light-Year Technology
// Company      : Light-Year Technology
// Licence Type : Commercial
// Licences     : 1
// Valid until  : 21 五月 2025
//
// Do not make changes directly to this file - edit the template instead.
//
// The following connection settings were used to generate this file:
//     Connection String Name: "JunFuDbContext"
//     Connection String:      "Data Source=junfurdsdb.cioqpfywhsfk.us-east-2.rds.amazonaws.com,1433;Initial Catalog=JunFuReal;Persist Security Info=True;User ID=admin;password=**zapped**;;MultiSubnetFailover=Yes;"
// ------------------------------------------------------------------------------------------------
// Database Edition       : Standard Edition (64-bit)
// Database Engine Edition: Standard
// Database Version       : 13.0.5820.21

// <auto-generated>
// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable NotAccessedVariable
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantCast
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// ReSharper disable UsePatternMatching
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
    // pick_up_request_log
    public class PickUpRequestLogConfiguration : IEntityTypeConfiguration<PickUpRequestLog>
    {
        public void Configure(EntityTypeBuilder<PickUpRequestLog> builder)
        {
            builder.ToTable("pick_up_request_log", "dbo");
            builder.HasKey(x => x.PickUpId).HasName("PK__pick_up___C57ECFBCE436EAAA").IsClustered();

            builder.Property(x => x.PickUpId).HasColumnName(@"pick_up_id").HasColumnType("int").IsRequired().ValueGeneratedOnAdd().UseIdentityColumn();
            builder.Property(x => x.CustomerCode).HasColumnName(@"customer_code").HasColumnType("nvarchar(7)").IsRequired(false).HasMaxLength(7);
            builder.Property(x => x.PickUpPieces).HasColumnName(@"pick_up_pieces").HasColumnType("int").IsRequired(false);
            builder.Property(x => x.Remark).HasColumnName(@"remark").HasColumnType("nvarchar(max)").IsRequired(false);
            builder.Property(x => x.PackageType).HasColumnName(@"package_type").HasColumnType("nvarchar(2)").IsRequired(false).HasMaxLength(2);
            builder.Property(x => x.VechileType).HasColumnName(@"vechile_type").HasColumnType("nvarchar(2)").IsRequired(false).HasMaxLength(2);
            builder.Property(x => x.Cdate).HasColumnName(@"cdate").HasColumnType("datetime").IsRequired(false);
            builder.Property(x => x.Udate).HasColumnName(@"udate").HasColumnType("datetime").IsRequired(false);
            builder.Property(x => x.PickUpDate).HasColumnName(@"pick_up_date").HasColumnType("nvarchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.RequestCustomerCode).HasColumnName(@"request_customer_code").HasColumnType("nvarchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.CheckNumber).HasColumnName(@"check_number").HasColumnType("nvarchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.ReassignMd).HasColumnName(@"reassign_md").HasColumnType("varchar(10)").IsRequired(false).IsUnicode(false).HasMaxLength(10);
            builder.Property(x => x.MdUuser).HasColumnName(@"md_uuser").HasColumnType("nvarchar(20)").IsRequired(false).HasMaxLength(20);
        }
    }

}
// </auto-generated>

