// ------------------------------------------------------------------------------------------------
// This code was generated by EntityFramework Reverse POCO Generator (http://www.reversepoco.co.uk/).
// Created by Simon Hughes (https://about.me/simon.hughes).
//
// Registered to: Light-Year Technology
// Company      : Light-Year Technology
// Licence Type : Commercial
// Licences     : 1
// Valid until  : 21 五月 2025
//
// Do not make changes directly to this file - edit the template instead.
//
// The following connection settings were used to generate this file:
//     Connection String Name: "JunFuDbContext"
//     Connection String:      "Data Source=junfurdsdb.cioqpfywhsfk.us-east-2.rds.amazonaws.com,1433;Initial Catalog=JunFuReal;Persist Security Info=True;User ID=admin;password=**zapped**;;MultiSubnetFailover=Yes;"
// ------------------------------------------------------------------------------------------------
// Database Edition       : Standard Edition (64-bit)
// Database Engine Edition: Standard
// Database Version       : 13.0.5820.21

// <auto-generated>
// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable NotAccessedVariable
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantCast
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// ReSharper disable UsePatternMatching
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
    // supplier_freight_detail
    public class SupplierFreightDetailConfiguration : IEntityTypeConfiguration<SupplierFreightDetail>
    {
        public void Configure(EntityTypeBuilder<SupplierFreightDetail> builder)
        {
            builder.ToTable("supplier_freight_detail", "dbo");
            builder.HasKey(x => new { x.SupplyArea, x.ReceiveArea, x.SupplierFreightId });

            builder.Property(x => x.SupplyArea).HasColumnName(@"supply_area").HasColumnType("nvarchar(20)").IsRequired().HasMaxLength(20).ValueGeneratedNever();
            builder.Property(x => x.ReceiveArea).HasColumnName(@"receive_area").HasColumnType("nvarchar(20)").IsRequired().HasMaxLength(20).ValueGeneratedNever();
            builder.Property(x => x.A).HasColumnName(@"A").HasColumnType("int").IsRequired(false);
            builder.Property(x => x.B1).HasColumnName(@"B1").HasColumnType("int").IsRequired(false);
            builder.Property(x => x.B2).HasColumnName(@"B2").HasColumnType("int").IsRequired(false);
            builder.Property(x => x.B3).HasColumnName(@"B3").HasColumnType("int").IsRequired(false);
            builder.Property(x => x.B4).HasColumnName(@"B4").HasColumnType("int").IsRequired(false);
            builder.Property(x => x.B5).HasColumnName(@"B5").HasColumnType("int").IsRequired(false);
            builder.Property(x => x.B6).HasColumnName(@"B6").HasColumnType("int").IsRequired(false);
            builder.Property(x => x.C1).HasColumnName(@"C1").HasColumnType("int").IsRequired(false);
            builder.Property(x => x.C2).HasColumnName(@"C2").HasColumnType("int").IsRequired(false);
            builder.Property(x => x.C3).HasColumnName(@"C3").HasColumnType("int").IsRequired(false);
            builder.Property(x => x.C4).HasColumnName(@"C4").HasColumnType("int").IsRequired(false);
            builder.Property(x => x.C5).HasColumnName(@"C5").HasColumnType("int").IsRequired(false);
            builder.Property(x => x.C6).HasColumnName(@"C6").HasColumnType("int").IsRequired(false);
            builder.Property(x => x.SupplierFreightId).HasColumnName(@"supplier_freight_id").HasColumnType("int").IsRequired().ValueGeneratedNever();
        }
    }

}
// </auto-generated>

