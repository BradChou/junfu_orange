﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
    public class VW_tbSupplier
    {
        public int supplier_id { get; set; }
        public string IDNO { get; set; }
        public string vendorName { get; set; }
        public bool? headquartersType { get; set; }
        public string headquarters { get; set; }
    }
}
