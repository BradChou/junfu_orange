// ------------------------------------------------------------------------------------------------
// This code was generated by EntityFramework Reverse POCO Generator (http://www.reversepoco.co.uk/).
// Created by Simon Hughes (https://about.me/simon.hughes).
//
// Registered to: Light-Year Technology
// Company      : Light-Year Technology
// Licence Type : Commercial
// Licences     : 1
// Valid until  : 21 五月 2025
//
// Do not make changes directly to this file - edit the template instead.
//
// The following connection settings were used to generate this file:
//     Connection String Name: "JunFuDbContext"
//     Connection String:      "Data Source=junfurdsdb.cioqpfywhsfk.us-east-2.rds.amazonaws.com,1433;Initial Catalog=JunFuReal;Persist Security Info=True;User ID=admin;password=**zapped**;;MultiSubnetFailover=Yes;"
// ------------------------------------------------------------------------------------------------
// Database Edition       : Standard Edition (64-bit)
// Database Engine Edition: Standard
// Database Version       : 13.0.5820.21

// <auto-generated>
// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable NotAccessedVariable
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantCast
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// ReSharper disable UsePatternMatching
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
    // TEST_FSDX
    public class Auxiliary_TestFsdx
    {
        public int RowNumber { get; set; } // RowNumber (Primary key)
        public int? EventClass { get; set; } // EventClass
        public string TextData { get; set; } // TextData (length: 1073741823)
        public string ApplicationName { get; set; } // ApplicationName (length: 128)
        public string NtUserName { get; set; } // NTUserName (length: 128)
        public string LoginName { get; set; } // LoginName (length: 128)
        public int? Cpu { get; set; } // CPU
        public long? Reads { get; set; } // Reads
        public long? Writes { get; set; } // Writes
        public long? Duration { get; set; } // Duration
        public int? ClientProcessId { get; set; } // ClientProcessID
        public int? Spid { get; set; } // SPID
        public DateTime? StartTime { get; set; } // StartTime
        public DateTime? EndTime { get; set; } // EndTime
        public byte[] BinaryData { get; set; } // BinaryData (length: 2147483647)
    }

}
// </auto-generated>

