﻿using Autofac;
using AutoMapper;
using System.Reflection;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.Modules.BL
{

    /// <summary>
    /// AutoMapper module
    /// </summary>
    public class MapperModule : Autofac.Module
    {
        /// <summary>
        /// Load
        /// </summary>
        /// <param name="builder">ContainerBuilder</param>
        protected override void Load(ContainerBuilder builder)
        {
            //// Register Mappers
            var mapperDll = Assembly.Load("LightYear.JunFuTrans.Mappers");

            builder.RegisterAssemblyTypes(mapperDll)
                   .Where(i => i.Name.EndsWith("MappingProfile"))
                   .As(i => i.BaseType);

            builder.Register(componentContext =>
            {
                var profiles = componentContext.Resolve<IEnumerable<Profile>>();
                var config = new MapperConfiguration(
                    cfg =>
                    {
                        foreach (var profile in profiles)
                        {
                            cfg.AddProfile(profile as Profile);
                        }
                    });
                var mapper = config.CreateMapper();

                return mapper;
            })
            .As<IMapper>()
            .SingleInstance();
        }
    }
}
