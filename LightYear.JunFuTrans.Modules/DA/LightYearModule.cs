﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
using LightYear.JunFuTrans.DA.LightYearTest;

namespace LightYear.JunFuTrans.Modules.DA
{
    public class LightYearModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //base.Load(builder);

            var connectionStringKey = "LightYearTestDbContext";

            builder.RegisterType<LightYearTestDbContext>()
                .AsSelf()
                .WithParameter("connectionString", connectionStringKey)
                .InstancePerLifetimeScope();
        }

    }
}
