﻿using System;
using System.Reflection;
using Autofac;

namespace LightYear.JunFuTrans.Modules.DA
{
    public class RepositoriesModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //base.Load(builder);
            var repository = Assembly.Load("LightYear.JunFuTrans.DA.Repositories");

            builder.RegisterAssemblyTypes(repository)
                   .AsImplementedInterfaces();
        }

    }
}
