﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.Modules.DA
{
    public class JunFuTransDbModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //base.Load(builder);

            var connectionStringKey = "JunFuTransDbContext";

            builder.RegisterType<JunFuTransDbContext>()
                .AsSelf()
                .WithParameter("connectionString", connectionStringKey)
                .InstancePerLifetimeScope();
        }
    }
}
