﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
using LightYear.JunFuTrans.DA.JunFuTransReadOnlyDb;

namespace LightYear.JunFuTrans.Modules.DA
{
    public class JunFuTransReadOnlyDbModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //base.Load(builder);

            var connectionStringKey = "JunFuTransReadOnlyDbContext";

            builder.RegisterType<JunFuTransReadOnlyDbContext>()
                .AsSelf()
                .WithParameter("connectionString", connectionStringKey)
                .InstancePerLifetimeScope();
        }
    }
}
