﻿CREATE view VW_TruckVendorPaymentLogWenERPＭergeData AS

WITH MergeData AS(
	select Year,RegisteredDate,FeeType,OrderType,SupplierID,SupplierUID,CustomerUID
	,Number,ProductCode,TaxType,TaxInclude,Quantity,SUM(Payment) as Payment,SUM(UnitPrice) as UnitPrice
	,ElecInovoice,PaperInovoice,ReconciliationDate,CheckoutDate,PaymentDate,ManufactureCode
	,ProductionQuantity,InvoiceNumber,InvoiceDate,VoucherType,Note01
		, STUFF((
			  SELECT ',' + T.Note02
			  FROM dbo.TruckVendorPaymentLogWenERP T
			  WHERE
				T.Year = A.Year AND T.RegisteredDate = A.RegisteredDate AND T.FeeType = A.FeeType AND T.OrderType = A.OrderType 
				AND T.SupplierID = A.SupplierID AND T.SupplierUID = A.CustomerUID AND T.Number = A.Number
				AND T.ProductCode = A.ProductCode AND T.TaxType = A.TaxType AND T.TaxInclude = A.TaxInclude AND T.Quantity = A.Quantity
				AND T.ElecInovoice = A.ElecInovoice AND T.PaperInovoice = A.PaperInovoice 
				AND ISNULL(T.ReconciliationDate,'') = ISNULL(A.ReconciliationDate,'')
				AND ISNULL(T.CheckoutDate,'') = ISNULL(A.CheckoutDate,'') AND ISNULL(T.PaymentDate,'') = ISNULL(A.PaymentDate,'') AND ISNULL(T.ManufactureCode,'') = ISNULL(A.ManufactureCode ,'')
				AND ISNULL(T.ProductionQuantity,'') = ISNULL(A.ProductionQuantity,'') AND ISNULL(T.InvoiceNumber,'') = ISNULL(A.InvoiceNumber,'') AND ISNULL(T.InvoiceDate,'') = ISNULL(A.InvoiceDate ,'')
				AND ISNULL(T.VoucherType,'') = ISNULL(A.VoucherType,'') AND ISNULL(T.Note01,'') = ISNULL(A.Note01,'') AND ISNULL(T.NoteMain,'') = ISNULL(A.NoteMain ,'')
				AND ISNULL(T.SupplierContact,'') = ISNULL(A.SupplierContact,'') AND ISNULL(T.InvoicingSpecial,'') = ISNULL(A.InvoicingSpecial,'') AND ISNULL(T.SupplierUniformID,'') = ISNULL(A.SupplierUniformID,'')
				AND ISNULL(T.SupplierAddress,'') = ISNULL(A.SupplierAddress,'') AND ISNULL(T.PackageType,'') = ISNULL(A.PackageType,'') AND ISNULL(T.ForeignCurrencyCode,'') = ISNULL(A.ForeignCurrencyCode,'')
				AND ISNULL(T.ForeignCurrencyPayment,'') = ISNULL(A.ForeignCurrencyPayment,'') AND ISNULL(T.OrderNumber,'') = ISNULL(A.OrderNumber,'')
				AND ISNULL(T.PurchaseNumber,'') = ISNULL(A.PurchaseNumber,'') AND ISNULL(T.SalesCode,'') = ISNULL(A.SalesCode,'') AND ISNULL(T.Department,'') = ISNULL(A.Department ,'')
				AND ISNULL(T.ProjectCode,'') = ISNULL(A.ProjectCode,'') AND ISNULL(T.CarLicense,'') = ISNULL(A.CarLicense,'') AND ISNULL(T.Company,'') = ISNULL(A.Company,'') AND ISNULL(T.VendorName,'') = ISNULL(A.VendorName ,'')
			  FOR XML PATH('')), 1, 1, '') as Note02 
	,NoteMain,SupplierContact,InvoicingSpecial,SupplierUniformID,SupplierAddress,PackageType,ForeignCurrencyCode
	,ExchangeRate,ForeignCurrencyUnitPrice,ForeignCurrencyPayment,OrderNumber,PurchaseNumber,SalesCode
	,Department,ProjectCode,CarLicense,Company,VendorName
	from TruckVendorPaymentLogWenERP A with(nolock)
	-------------------------條件 啟用 + 有下載紀錄-----------------------------
	where IsActive = 1 AND HasDownload = 1
	--AND RegisteredDate between '2021-12-15' AND '2021-12-15'--觀察測試
	-------------------------條件 啟用 + 有下載紀錄-----------------------------
	group by  Year,RegisteredDate,FeeType,OrderType,SupplierID,SupplierUID,CustomerUID
	,Number,ProductCode,TaxType,TaxInclude,Quantity
	,ElecInovoice,PaperInovoice,ReconciliationDate,CheckoutDate,PaymentDate,ManufactureCode
	,ProductionQuantity,InvoiceNumber,InvoiceDate,VoucherType,Note01
	,NoteMain,SupplierContact,InvoicingSpecial,SupplierUniformID,SupplierAddress,PackageType,ForeignCurrencyCode
	,ExchangeRate,ForeignCurrencyUnitPrice,ForeignCurrencyPayment,OrderNumber,PurchaseNumber,SalesCode
	,Department,ProjectCode,CarLicense,Company,VendorName
),tbNumber AS(
SELECT * FROM(
	SELECT 
	(ROW_NUMBER() OVER (partition by Year,RegisteredDate,FeeType,OrderType,SupplierID,SupplierUID,CustomerUID
		,Number,ProductCode,TaxType,TaxInclude,Quantity
		,ElecInovoice,PaperInovoice,ReconciliationDate,CheckoutDate,PaymentDate,ManufactureCode
		,ProductionQuantity,InvoiceNumber,InvoiceDate,VoucherType,Note01
		,NoteMain,SupplierContact,InvoicingSpecial,SupplierUniformID,SupplierAddress,PackageType,ForeignCurrencyCode
		,ExchangeRate,ForeignCurrencyUnitPrice,ForeignCurrencyPayment,OrderNumber,PurchaseNumber,SalesCode
		,Department,ProjectCode,CarLicense,Company,VendorName ORDER BY id ASC)) as rowid
	,* FROM TruckVendorPaymentLogWenERP　with(nolock)
)NR
WHERE rowid = 1
)
SELECT tbN.InvoicingNumber,MD.* FROM MergeData MD
left join tbNumber tbN on
MD.Year = tbN.Year AND MD.RegisteredDate = tbN.RegisteredDate AND MD.FeeType = tbN.FeeType AND MD.OrderType = tbN.OrderType 
AND MD.SupplierID = tbN.SupplierID AND MD.SupplierUID = tbN.CustomerUID AND MD.Number = tbN.Number
AND MD.ProductCode = tbN.ProductCode AND MD.TaxType = tbN.TaxType AND MD.TaxInclude = tbN.TaxInclude AND MD.Quantity = tbN.Quantity
AND MD.ElecInovoice = tbN.ElecInovoice AND MD.PaperInovoice = tbN.PaperInovoice 
AND ISNULL(MD.ReconciliationDate,'') = ISNULL(tbN.ReconciliationDate,'')
AND ISNULL(MD.CheckoutDate,'') = ISNULL(tbN.CheckoutDate,'') AND ISNULL(MD.PaymentDate,'') = ISNULL(tbN.PaymentDate,'') AND ISNULL(MD.ManufactureCode,'') = ISNULL(tbN.ManufactureCode ,'')
AND ISNULL(MD.ProductionQuantity,'') = ISNULL(tbN.ProductionQuantity,'') AND ISNULL(MD.InvoiceNumber,'') = ISNULL(tbN.InvoiceNumber,'') AND ISNULL(MD.InvoiceDate,'') = ISNULL(tbN.InvoiceDate ,'')
AND ISNULL(MD.VoucherType,'') = ISNULL(tbN.VoucherType,'') AND ISNULL(MD.Note01,'') = ISNULL(tbN.Note01,'') AND ISNULL(MD.NoteMain,'') = ISNULL(tbN.NoteMain ,'')
AND ISNULL(MD.SupplierContact,'') = ISNULL(tbN.SupplierContact,'') AND ISNULL(MD.InvoicingSpecial,'') = ISNULL(tbN.InvoicingSpecial,'') AND ISNULL(MD.SupplierUniformID,'') = ISNULL(tbN.SupplierUniformID,'')
AND ISNULL(MD.SupplierAddress,'') = ISNULL(tbN.SupplierAddress,'') AND ISNULL(MD.PackageType,'') = ISNULL(tbN.PackageType,'') AND ISNULL(MD.ForeignCurrencyCode,'') = ISNULL(tbN.ForeignCurrencyCode,'')
AND ISNULL(MD.ForeignCurrencyPayment,'') = ISNULL(tbN.ForeignCurrencyPayment,'') AND ISNULL(MD.OrderNumber,'') = ISNULL(tbN.OrderNumber,'')
AND ISNULL(MD.PurchaseNumber,'') = ISNULL(tbN.PurchaseNumber,'') AND ISNULL(MD.SalesCode,'') = ISNULL(tbN.SalesCode,'') AND ISNULL(MD.Department,'') = ISNULL(tbN.Department ,'')
AND ISNULL(MD.ProjectCode,'') = ISNULL(tbN.ProjectCode,'') AND ISNULL(MD.CarLicense,'') = ISNULL(tbN.CarLicense,'') AND ISNULL(MD.Company,'') = ISNULL(tbN.Company,'') AND ISNULL(MD.VendorName,'') = ISNULL(tbN.VendorName ,'')
--select * from TruckVendorPaymentLogWenERP