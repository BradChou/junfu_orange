﻿CREATE View VW_TruckVendorPaymentLogDSERPＭergeData AS
--WITH fee as (
--select * from JunFuReal.dbo.tbItemCodes 
--where code_sclass = 'fee_type'  and active_flag = '1'
--)
SELECT Company , VoucherCode , DinShinItemCode , SupplierUID
 ,  SUM(Accounts)　Accounts ,  SUM(CollectionAccounts) CollectionAccounts , SUM(Spread) Spread
 , CollectionPayCode , ISNULL(CustomerUID , '')　CustomerUID , CarLicense 
 , CASE WHEN Company = 'CH' THEN 'C' + Department 
 WHEN Company = 'GL' THEN 'G' + Department 
 ELSE Department END Department
 , RegisteredDate , CreateUser , FeeType,CompanyUID
 , InvoiceNumber , InvoiceDate
 , FORMAT(RegisteredDate,'yyyy/MM/dd') + fee.code_name as Note02
 ,  STUFF((
	  SELECT ',' +T.Note02-- REPLACE(T.Note02, ' ', '_')
	  FROM dbo.DSERPMergeDataSnapShot T
	  WHERE
		T.Company = A.Company  AND T.VoucherCode = A.VoucherCode  AND T.DinShinItemCode = A.DinShinItemCode 
		 AND T.SupplierUID = A.SupplierUID  AND T.CollectionPayCode = A.CollectionPayCode 
		 AND ISNULL(T.CustomerUID,'') = ISNULL(A.CustomerUID,'')  AND T.CarLicense = A.CarLicense  AND T.Department = A.Department 
		 AND T.RegisteredDate = A.RegisteredDate  AND T.CreateUser = A.CreateUser AND T.FeeType = A.FeeType
		 AND ISNULL(T.InvoiceNumber,'') = ISNULL(A.InvoiceNumber,'')  AND ISNULL(T.InvoiceDate,'') = ISNULL(A.InvoiceDate,'')
		 AND A.CompanyUID = T.CompanyUID AND A.MergeRandomCode = T.MergeRandomCode
		 AND A.HasDownload = T.HasDownload AND A.DownloadRandomCode = T.DownloadRandomCode
	  FOR XML PATH('')) ,  1 ,  1 ,  '') as Note02_OLD
  ,HasDownload,MergeRandomCode,DownloadRandomCode
FROM DSERPMergeDataSnapShot A
left join
(
select * from JunFuReal.dbo.tbItemCodes 
where code_sclass = 'fee_type'  and active_flag = '1'
)
fee on A.FeeType = fee.code_id
where A.IsActive = 1 AND ISNULL(A.MergeRandomCode,'' ) <> ''
group by 
Company , VoucherCode , DinShinItemCode , SupplierUID , CollectionPayCode , ISNULL(CustomerUID , '')
 , CarLicense , Department , RegisteredDate , CreateUser  , FeeType,CompanyUID , InvoiceNumber , InvoiceDate 
 ,MergeRandomCode,HasDownload,DownloadRandomCode,fee.code_name