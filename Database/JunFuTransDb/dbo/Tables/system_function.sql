﻿CREATE TABLE [dbo].[system_function]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [sub_category_id] INT NOT NULL, 
    [function_name] NVARCHAR(100) NOT NULL, 
    [function_en_name] VARCHAR(100) NOT NULL, 
    [function_url] VARCHAR(100) NOT NULL, 
    [create_date] DATETIME NOT NULL, 
    [update_date] DATETIME NOT NULL, 
    [is_menu] BIT NOT NULL DEFAULT 1, 
    [display_sort] INT NOT NULL DEFAULT 99
)
