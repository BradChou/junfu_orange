﻿CREATE TABLE [dbo].[JunFuOrangeLog] (
    [Id]            INT            IDENTITY (1, 1) NOT NULL,
    [Guid]          VARCHAR (50)   NOT NULL,
    [Account]       NVARCHAR (50)  NULL,
    [RequestBody]   NVARCHAR (MAX) NOT NULL,
    [ResponseBody]  NVARCHAR (MAX) NOT NULL,
    [RequestHeader] NVARCHAR (MAX) NOT NULL,
    [Path]          NVARCHAR (500) NOT NULL,
    [QueryString]   NVARCHAR (MAX) NOT NULL,
    [StartDate]     DATETIME       NOT NULL,
    [EndDate]       DATETIME       NOT NULL,
    [CreateDate]    DATETIME2 (7)  CONSTRAINT [DF_Table_1_CreateTime] DEFAULT (sysdatetime()) NOT NULL,
    CONSTRAINT [PK_JunFuOrangeLog] PRIMARY KEY CLUSTERED ([Id] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JunFuOrangeLog', @level2type = N'COLUMN', @level2name = N'CreateDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'結束時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JunFuOrangeLog', @level2type = N'COLUMN', @level2name = N'EndDate';


GO



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'參數', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JunFuOrangeLog', @level2type = N'COLUMN', @level2name = N'QueryString';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'程式路徑', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JunFuOrangeLog', @level2type = N'COLUMN', @level2name = N'Path';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RequestHeader', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JunFuOrangeLog', @level2type = N'COLUMN', @level2name = N'RequestHeader';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Response內容', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JunFuOrangeLog', @level2type = N'COLUMN', @level2name = N'ResponseBody';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Request內容', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JunFuOrangeLog', @level2type = N'COLUMN', @level2name = N'RequestBody';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'帳號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JunFuOrangeLog', @level2type = N'COLUMN', @level2name = N'Account';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'GUID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JunFuOrangeLog', @level2type = N'COLUMN', @level2name = N'Guid';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JunFuOrangeLog', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'開始時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JunFuOrangeLog', @level2type = N'COLUMN', @level2name = N'StartDate';

