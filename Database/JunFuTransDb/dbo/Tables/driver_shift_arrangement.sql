﻿CREATE TABLE [dbo].[driver_shift_arrangement] (
    [Id]                     INT           IDENTITY (1, 1) NOT NULL,
    [take_off_date]          DATETIME      DEFAULT (((2020)-(8))-(20)) NOT NULL,
    [station_scode]          NVARCHAR (5)  NULL,
    [station_name]           NVARCHAR (10) NULL,
    [take_off_driver_code]   NVARCHAR (10) NULL,
    [take_off_driver]        NVARCHAR (20) NULL,
    [substitute_driver_code] NVARCHAR (10) NULL,
    [substitute_driver]      NVARCHAR (20) NULL,
    [update_date]            DATETIME      NULL,
    [update_user]            NVARCHAR (20) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);




