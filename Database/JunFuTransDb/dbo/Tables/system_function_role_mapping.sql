﻿CREATE TABLE [dbo].[system_function_role_mapping]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [role_id] INT NOT NULL, 
    [function_id] INT NOT NULL, 
    [create_date] DATETIME NOT NULL, 
    [update_date] DATETIME NOT NULL, 
    [function_value] INT NOT NULL DEFAULT 2
)

GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'1:唯讀 2:讀寫 3:管理',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'system_function_role_mapping',
    @level2type = N'COLUMN',
    @level2name = N'function_value'