﻿CREATE TABLE [dbo].[driver_payment_sheet] (
    [Id]                          INT           IDENTITY (1, 1) NOT NULL,
    [create_date]                 DATETIME      NULL,
    [driver_code]                 NVARCHAR (10) NOT NULL,
    [driver_name]                 NVARCHAR (10) NULL,
    [station]                     NVARCHAR (10) NULL,
    [yuanfu_count]                INT           NULL,
    [yuanfu_money]                INT           NULL,
    [cash_on_delivery_count]      INT           CONSTRAINT [DF_driver_payment_sheet_cash_on_delivery_count] DEFAULT ((0)) NULL,
    [cash_on_delivery_money]      INT           CONSTRAINT [DF_driver_payment_sheet_cash_on_delivery_money] DEFAULT ((0)) NULL,
    [destination_collect_count]   INT           CONSTRAINT [DF_driver_payment_sheet_destination_collect_count] DEFAULT ((0)) NULL,
    [destination_collect_money]   INT           CONSTRAINT [DF_driver_payment_sheet_destination_collect_money] DEFAULT ((0)) NULL,
    [transaction_last_five]       NCHAR (5)     NULL,
    [remittance_amount]           INT           CONSTRAINT [remittance_amount] DEFAULT ((0)) NULL,
    [remittance_on_delivery]      INT           CONSTRAINT [DF_driver_payment_sheet_remittance_on_delivery] DEFAULT ((0)) NULL,
    [check_on_delivery]           INT           CONSTRAINT [DF_driver_payment_sheet_check_on_delivery] DEFAULT ((0)) NULL,
    [check_amount]                INT           CONSTRAINT [DF_driver_payment_sheet_check_amount] DEFAULT ((0)) NULL,
    [acutal_pay_amount]           INT           NULL,
    [payment_balance]             INT           NULL,
    [station_scode]               NVARCHAR (15) NULL,
    [station_name]                NVARCHAR (10) NULL,
    [coins_money]                 INT           CONSTRAINT [DF_driver_payment_sheet_coins_money] DEFAULT ((0)) NULL,
    [is_valid]                    BIT           CONSTRAINT [DF_driver_payment_sheet_is_valid] DEFAULT ((1)) NULL,
    [accumulated_payment_balance] INT           NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);









GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'司機的匯款明細末五碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'driver_payment_sheet', @level2type = N'COLUMN', @level2name = N'transaction_last_five';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'代收貨款付款方式為匯款的總金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'driver_payment_sheet', @level2type = N'COLUMN', @level2name = N'remittance_on_delivery';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'司機的匯款金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'driver_payment_sheet', @level2type = N'COLUMN', @level2name = N'remittance_amount';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'應收帳款金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'driver_payment_sheet', @level2type = N'COLUMN', @level2name = N'destination_collect_money';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'應收帳款數量', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'driver_payment_sheet', @level2type = N'COLUMN', @level2name = N'destination_collect_count';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'付款方式為支票的總金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'driver_payment_sheet', @level2type = N'COLUMN', @level2name = N'check_on_delivery';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'司機付的支票', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'driver_payment_sheet', @level2type = N'COLUMN', @level2name = N'check_amount';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'代收貨款付現總金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'driver_payment_sheet', @level2type = N'COLUMN', @level2name = N'cash_on_delivery_money';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'代收貨款數量', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'driver_payment_sheet', @level2type = N'COLUMN', @level2name = N'cash_on_delivery_count';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'前期差/餘額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'driver_payment_sheet', @level2type = N'COLUMN', @level2name = N'payment_balance';


GO



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'司機付的零錢金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'driver_payment_sheet', @level2type = N'COLUMN', @level2name = N'coins_money';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'現繳繳款金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'driver_payment_sheet', @level2type = N'COLUMN', @level2name = N'acutal_pay_amount';




GO


