﻿CREATE TABLE [dbo].[TruckVendorPaymentLogDSERP] (
    [id]                      INT            IDENTITY (1, 1) NOT NULL,
    [TruckVendorPaymentLogId] INT            NOT NULL,
    [FeeType]                 NVARCHAR (20)  CONSTRAINT [DF_TruckVendorPaymentLogDSERP_FeeType] DEFAULT ((0)) NOT NULL,
    [RegisteredDate]          DATETIME       NOT NULL,
    [UploadDate]              DATETIME       NOT NULL,
    [Company]                 VARCHAR (10)   CONSTRAINT [DF_TruckVendorPaymentLogDSERP_Company] DEFAULT (N'CH') NOT NULL,
    [VoucherCode]             VARCHAR (10)   NOT NULL,
    [DinShinItemCode]         VARCHAR (10)   NOT NULL,
    [SupplierUID]             VARCHAR (10)   NULL,
    [Accounts]                INT            CONSTRAINT [DF_TruckVendorPaymentLogDSERP_Cost] DEFAULT ((0)) NOT NULL,
    [CollectionPayCode]       VARCHAR (1)    CONSTRAINT [DF_TruckVendorPaymentLogDSERP_CollectionPayCode] DEFAULT (N'N') NOT NULL,
    [CustomerUID]             VARCHAR (50)   NULL,
    [CollectionAccounts]      INT            CONSTRAINT [DF_TruckVendorPaymentLogDSERP_Payment] DEFAULT ((0)) NOT NULL,
    [Spread]                  INT            CONSTRAINT [DF_TruckVendorPaymentLogDSERP_Spread] DEFAULT ((0)) NOT NULL,
    [CarLicense]              VARCHAR (20)   NULL,
    [Note02]                  NVARCHAR (MAX) NULL,
    [Department]              VARCHAR (50)   NOT NULL,
    [CompanyUID]              VARCHAR (10)   NOT NULL,
    [InvoiceNumber]           VARCHAR (20)   NULL,
    [InvoiceDate]             DATETIME       NULL,
    [CreateDate]              DATETIME       CONSTRAINT [DF_TruckVendorPaymentLogDSERP_CreateDate] DEFAULT (getdate()) NOT NULL,
    [CreateUser]              NVARCHAR (10)  NOT NULL,
    [UpdateDate]              DATETIME       NULL,
    [UpdateUser]              NVARCHAR (10)  NULL,
    [IsActive]                BIT            CONSTRAINT [DF_TruckVendorPaymentLogDSERP_IsActive] DEFAULT ((1)) NOT NULL,
    [HasDownload]             BIT            CONSTRAINT [DF_TruckVendorPaymentLogDSERP_HasDownload] DEFAULT ((0)) NOT NULL,
    [DownloadDate]            DATETIME       NULL,
    [DownloadUser]            NVARCHAR (10)  NULL,
    [MergeRandomCode]         NVARCHAR (10)  CONSTRAINT [DF_TruckVendorPaymentLogDSERP_MergeRandomCode] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_TruckVendorPaymentLogDSERP] PRIMARY KEY CLUSTERED ([id] ASC)
);
















GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'發票號碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'InvoiceNumber';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'公司別 (車行)關聯的統一編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'CompanyUID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'部門編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'Department';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'明細備註二', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'Note02';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'CarLicense';


GO



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車主(客戶代號)應收對象統一編號或身分證字號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'CustomerUID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'代付碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'CollectionPayCode';


GO



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'客戶/供應商 (原始交易對象)應付對象統一編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'SupplierUID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'憑單單別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'VoucherCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'入帳公司別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'Company';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TruckVendorPaymentLog 原表id', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'TruckVendorPaymentLogId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車老闆鼎新板PK', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'更新人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'UpdateUser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'更新日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'UpdateDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否啟用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'IsActive';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否已下載', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'HasDownload';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'下載人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'DownloadUser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'下載日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'DownloadDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'CreateUser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'CreateDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'原資料上傳日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'UploadDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'發生日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'RegisteredDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'鼎新品號類別代號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'DinShinItemCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'代付轉收金額 (未稅金額)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'CollectionAccounts';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'未稅金額 (收/付款金額)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'Accounts';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'支出項目/費用類別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogDSERP', @level2type = N'COLUMN', @level2name = N'FeeType';

