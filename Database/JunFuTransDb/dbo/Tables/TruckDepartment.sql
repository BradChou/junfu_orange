﻿CREATE TABLE [dbo].[TruckDepartment] (
    [id]             INT           IDENTITY (1, 1) NOT NULL,
    [DepartmentCode] NVARCHAR (50) NOT NULL,
    [DepartmentName] NVARCHAR (50) NOT NULL,
    [CreateUser]     NVARCHAR (20) NOT NULL,
    [CreateDate]     DATETIME      NOT NULL,
    [UpdateUser]     NVARCHAR (20) NULL,
    [UpdateDate]     DATETIME      NULL,
    [IsActive]       BIT           NOT NULL,
    CONSTRAINT [PK_TruckDepartment] PRIMARY KEY CLUSTERED ([id] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否啟用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckDepartment', @level2type = N'COLUMN', @level2name = N'IsActive';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'更新日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckDepartment', @level2type = N'COLUMN', @level2name = N'UpdateDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'更新人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckDepartment', @level2type = N'COLUMN', @level2name = N'UpdateUser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckDepartment', @level2type = N'COLUMN', @level2name = N'CreateDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckDepartment', @level2type = N'COLUMN', @level2name = N'CreateUser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'部門名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckDepartment', @level2type = N'COLUMN', @level2name = N'DepartmentName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'部門代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckDepartment', @level2type = N'COLUMN', @level2name = N'DepartmentCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckDepartment', @level2type = N'COLUMN', @level2name = N'id';

