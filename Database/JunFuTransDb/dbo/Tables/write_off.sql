﻿CREATE TABLE [dbo].[write_off] (
    [check_number]            VARCHAR (20) NOT NULL,
    [write_off_check_list_id] INT          NULL,
    [udate]                   DATETIME     NULL,
    [station_code]            VARCHAR (50) NULL,
    [cdate]                   DATETIME     NULL,
    [emp_code]                VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([check_number] ASC)
);




GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-write_off_check_list_id]
    ON [dbo].[write_off]([write_off_check_list_id] ASC);

