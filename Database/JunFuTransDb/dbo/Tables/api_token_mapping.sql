﻿CREATE TABLE [dbo].[api_token_mapping]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [token] VARCHAR(50) NOT NULL, 
    [customer_code] VARCHAR(50) NOT NULL, 
    [create_date] DATETIME NOT NULL, 
    [update_date] DATETIME NOT NULL, 
    [agency_code] VARCHAR(50) NULL
)
