﻿CREATE TABLE [dbo].[TruckVendorPaymentLog] (
    [id]                 INT            IDENTITY (1, 1) NOT NULL,
    [registered_date]    DATETIME       NULL,
    [fee_type]           NVARCHAR (20)  NULL,
    [vendor_name]        NVARCHAR (20)  NULL,
    [InvoiceNumber]      VARCHAR (20)   NULL,
    [InvoiceDate]        DATETIME       NULL,
    [car_license]        NVARCHAR (20)  NULL,
    [payment]            INT            NULL,
    [cost]               INT            CONSTRAINT [DF_TruckVendorPaymentLog_Cost] DEFAULT ((0)) NOT NULL,
    [spread]             INT            CONSTRAINT [DF_TruckVendorPaymentLog_Spread] DEFAULT ((0)) NOT NULL,
    [payable]            NVARCHAR (20)  NULL,
    [memo]               NVARCHAR (MAX) NULL,
    [company]            NVARCHAR (20)  NULL,
    [department_code]    NVARCHAR (50)  NULL,
    [create_date]        DATETIME       NULL,
    [update_date]        DATETIME       NULL,
    [update_user]        NVARCHAR (20)  NULL,
    [create_user]        NVARCHAR (10)  NULL,
    [import_random_code] NVARCHAR (20)  NULL,
    [IsWen]              BIT            CONSTRAINT [DF_TruckVendorPaymentLog_IsWen] DEFAULT ((1)) NULL,
    [IsActive]           BIT            CONSTRAINT [DF_TruckVendorPaymentLog_IsActive] DEFAULT ((1)) NULL,
    [IsChecked]          BIT            CONSTRAINT [DF_TruckVendorPaymentLog_IsChecked] DEFAULT ((0)) NULL,
    CONSTRAINT [PK__TruckVen__3213E83F434720CD] PRIMARY KEY CLUSTERED ([id] ASC)
);






















GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車老闆帳務表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLog';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'供應商名稱/車老闆名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLog', @level2type = N'COLUMN', @level2name = N'vendor_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'立案/發生日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLog', @level2type = N'COLUMN', @level2name = N'registered_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLog', @level2type = N'COLUMN', @level2name = N'payment';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'摘要', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLog', @level2type = N'COLUMN', @level2name = N'memo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'支出項目/費用類別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLog', @level2type = N'COLUMN', @level2name = N'fee_type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'公司別/車行', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLog', @level2type = N'COLUMN', @level2name = N'company';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLog', @level2type = N'COLUMN', @level2name = N'car_license';


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-registered_date]
    ON [dbo].[TruckVendorPaymentLog]([registered_date] ASC);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-import_random_code]
    ON [dbo].[TruckVendorPaymentLog]([import_random_code] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'部門代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLog', @level2type = N'COLUMN', @level2name = N'department_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否可轉為文中', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLog', @level2type = N'COLUMN', @level2name = N'IsWen';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否已核帳', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLog', @level2type = N'COLUMN', @level2name = N'IsChecked';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否啟用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLog', @level2type = N'COLUMN', @level2name = N'IsActive';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'價差', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLog', @level2type = N'COLUMN', @level2name = N'spread';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'成本', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLog', @level2type = N'COLUMN', @level2name = N'cost';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'應付對象(tbSupplier)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLog', @level2type = N'COLUMN', @level2name = N'payable';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'發票號碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLog', @level2type = N'COLUMN', @level2name = N'InvoiceNumber';

