﻿CREATE TABLE [dbo].[user_role_mapping]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [user_id] INT NOT NULL, 
    [role_id] INT NOT NULL, 
    [user_type] INT NOT NULL DEFAULT 2, 
    [create_date] DATETIME NULL, 
    [update_date] DATETIME NULL
)

GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'1:天眼user 2:天眼driver 3:新架構user',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'user_role_mapping',
    @level2type = N'COLUMN',
    @level2name = N'user_type'