﻿CREATE TABLE [dbo].[payment_write_off] (
    [Id]             INT          IDENTITY (1, 1) NOT NULL,
    [request_id]     NUMERIC (18) DEFAULT ((1)) NOT NULL,
    [is_write_off]   BIT          NULL,
    [write_off_date] DATETIME     NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


