﻿CREATE TABLE [dbo].[check_number_sd_mapping] (
    [id]           BIGINT       IDENTITY (1, 1) NOT NULL,
    [check_number] VARCHAR (50) NOT NULL,
    [org_area_id]  INT          NULL,
    [md]           VARCHAR (50) NULL,
    [sd]           VARCHAR (50) NULL,
    [put_order]    VARCHAR (50) NULL,
    [request_id]   BIGINT       NOT NULL,
    [cdate]        DATETIME     NOT NULL,
    [udate]        DATETIME     NOT NULL,
    CONSTRAINT [PK_check_number_sd_mapping] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_check_number_sd_mapping-request_id]
    ON [dbo].[check_number_sd_mapping]([request_id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_check_number_sd_mapping-check_number]
    ON [dbo].[check_number_sd_mapping]([check_number] ASC);

