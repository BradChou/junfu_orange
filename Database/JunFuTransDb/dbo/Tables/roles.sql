﻿CREATE TABLE [dbo].[roles]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [role_name] NVARCHAR(50) NOT NULL, 
    [role_en_name] NVARCHAR(50) NULL, 
    [create_date] DATETIME NOT NULL, 
    [update_date] DATETIME NULL
)
