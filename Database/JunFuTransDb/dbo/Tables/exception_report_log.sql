﻿CREATE TABLE [dbo].[exception_report_log]
(
	[id] INT NOT NULL IDENTITY PRIMARY KEY, 
    [check_number] NVARCHAR(20) NOT NULL, 
    [handle_result] NVARCHAR(MAX) NULL, 
    [handle_station] NVARCHAR(10) NOT NULL, 
    [handler_code] NVARCHAR(10) NULL, 
    [cdate] DATETIME NOT NULL, 
    [udate] DATETIME NOT NULL  
)
