﻿CREATE TABLE [dbo].[orgArea] (
    [id]            INT            NOT NULL,
    [OFFICE]        NVARCHAR (255) NULL,
    [ZIP3A]         NVARCHAR (255) NULL,
    [ZIPCODE]       NVARCHAR (255) NULL,
    [CITY]          NVARCHAR (30)  NULL,
    [AREA]          NVARCHAR (30)  NULL,
    [AREA1]         NVARCHAR (255) NULL,
    [ROAD]          NVARCHAR (50)  NULL,
    [SCOOP]         NVARCHAR (255) NULL,
    [EVEN]          FLOAT (53)     NULL,
    [CMP_LABLE]     NVARCHAR (255) NULL,
    [LANE]          FLOAT (53)     NULL,
    [LANE1]         NVARCHAR (255) NULL,
    [ALLEY]         FLOAT (53)     NULL,
    [ALLEY1]        NVARCHAR (255) NULL,
    [NO_BGN]        FLOAT (53)     NULL,
    [NO_BGN1]       NVARCHAR (255) NULL,
    [NO_END]        FLOAT (53)     NULL,
    [NO_END1]       NVARCHAR (255) NULL,
    [FLOOR]         NVARCHAR (255) NULL,
    [FLOOR1]        NVARCHAR (255) NULL,
    [ROAD_NO]       NVARCHAR (255) NULL,
    [ROAD1]         NVARCHAR (255) NULL,
    [EROAD]         NVARCHAR (50)  NULL,
    [RMK]           NVARCHAR (255) NULL,
    [RMK1]          NVARCHAR (255) NULL,
    [ZIP3RMK]       FLOAT (53)     NULL,
    [ECITY]         NVARCHAR (255) NULL,
    [EAREA]         NVARCHAR (255) NULL,
    [UWORD]         NVARCHAR (255) NULL,
    [ISN]           NVARCHAR (255) NULL,
    [station_scode] VARCHAR (50)   NULL,
    [station_name]  VARCHAR (50)   NULL,
    [md_no]         NVARCHAR (50)  NULL,
    [sd_no]         NVARCHAR (50)  NULL,
    [put_order]     NVARCHAR (50)  NULL,
    CONSTRAINT [PK_orgArea] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_orgArea-city-area-eroad]
    ON [dbo].[orgArea]([CITY] ASC, [AREA] ASC, [EROAD] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_orgArea-city-area-road]
    ON [dbo].[orgArea]([CITY] ASC, [AREA] ASC, [ROAD] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_orgArea-station-name]
    ON [dbo].[orgArea]([station_name] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_orgArea-station_scode]
    ON [dbo].[orgArea]([station_scode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_orgArea-zipcode]
    ON [dbo].[orgArea]([ZIPCODE] ASC);

