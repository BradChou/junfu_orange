﻿CREATE TABLE [dbo].[system_sub_category]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [category_id] INT NOT NULL, 
    [sub_category_name] NVARCHAR(50) NOT NULL, 
    [sub_category_en_name] VARCHAR(50) NOT NULL, 
    [create_date] DATETIME NOT NULL, 
    [update_date] DATETIME NOT NULL, 
    [is_menu] BIT NOT NULL DEFAULT 1, 
    [display_sort] INT NOT NULL DEFAULT 99
)
