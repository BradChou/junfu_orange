﻿CREATE TABLE [dbo].[payment] (
    [payment_id]     INT            IDENTITY (1, 1) NOT NULL,
    [payment_number] VARCHAR (50)   NOT NULL,
    [payment_date]   DATETIME       NOT NULL,
    [receive_number] VARCHAR (50)   NULL,
    [money_transfer] DECIMAL (18)   CONSTRAINT [DF_payment_money_transfer] DEFAULT ((0)) NOT NULL,
    [handling_fee]   DECIMAL (18)   CONSTRAINT [DF_payment_handling_fee] DEFAULT ((0)) NOT NULL,
    [other_fee]      DECIMAL (18)   CONSTRAINT [DF_payment_other_fee] DEFAULT ((0)) NOT NULL,
    [other_fee_memo] NVARCHAR (255) NULL,
    [create_date]    DATETIME       NULL,
    [create_id]      INT            NULL,
    [update_date]    DATETIME       NULL,
    [update_id]      INT            NULL,
    [is_delete]      BIT            CONSTRAINT [DF_payment_is_delete] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_payment] PRIMARY KEY CLUSTERED ([payment_id] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'其它費用說明', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'payment', @level2type = N'COLUMN', @level2name = N'other_fee_memo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'其它費用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'payment', @level2type = N'COLUMN', @level2name = N'other_fee';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'手續費', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'payment', @level2type = N'COLUMN', @level2name = N'handling_fee';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'匯款金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'payment', @level2type = N'COLUMN', @level2name = N'money_transfer';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'匯款單號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'payment', @level2type = N'COLUMN', @level2name = N'receive_number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'匯款日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'payment', @level2type = N'COLUMN', @level2name = N'payment_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'代收單號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'payment', @level2type = N'COLUMN', @level2name = N'payment_number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'代收對帳作業-母單', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'payment';

