﻿CREATE TABLE [dbo].[cbm_update_log] (
    [check_number] VARCHAR (20)  NULL,
    [cbmLength]    FLOAT (53)    NULL,
    [cbmWidth]     FLOAT (53)    NULL,
    [cbmHeight]    FLOAT (53)    NULL,
    [cbmCont]      FLOAT (53)    NULL,
    [data_from]    VARCHAR (10)  NULL,
    [cdate]        DATETIME      NULL,
    [pic_path]     VARCHAR (MAX) NULL,
    [data_name]    VARCHAR (MAX) NULL
);

