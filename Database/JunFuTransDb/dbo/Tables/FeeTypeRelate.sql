﻿CREATE TABLE [dbo].[FeeTypeRelate] (
    [id]                    INT            IDENTITY (1, 1) NOT NULL,
    [CodeId]                INT            NOT NULL,
    [CodeName]              NVARCHAR (100) NOT NULL,
    [HasInvoice]            BIT            CONSTRAINT [DF_FeeTypeRelate_IsPayable] DEFAULT ((1)) NOT NULL,
    [IsActive]              BIT            CONSTRAINT [DF_FeeTypeRelate_IsActive] DEFAULT ((1)) NOT NULL,
    [PayableCode]           VARCHAR (10)   NOT NULL,
    [ReceivableCode]        VARCHAR (10)   NOT NULL,
    [DinShinItemCode]       VARCHAR (10)   NOT NULL,
    [DinShinItemName]       NVARCHAR (50)  NOT NULL,
    [StockPayCode]          VARCHAR (10)   NULL,
    [StockPayName]          NVARCHAR (50)  NULL,
    [RevenueCode]           VARCHAR (10)   NOT NULL,
    [RevenueName]           NVARCHAR (50)  NOT NULL,
    [CollectionReceiveCode] VARCHAR (10)   NULL,
    [CollectionReceiveName] NVARCHAR (50)  NULL,
    [InvoiceSubjectName]    NVARCHAR (50)  NULL,
    CONSTRAINT [PK_FeeTypeRelate] PRIMARY KEY CLUSTERED ([id] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否有應付對象', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeeTypeRelate', @level2type = N'COLUMN', @level2name = N'HasInvoice';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'交易項目名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeeTypeRelate', @level2type = N'COLUMN', @level2name = N'CodeName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'交易項目代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeeTypeRelate', @level2type = N'COLUMN', @level2name = N'CodeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'交易項目應付對象PK', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeeTypeRelate', @level2type = N'COLUMN', @level2name = N'id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否啟用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeeTypeRelate', @level2type = N'COLUMN', @level2name = N'IsActive';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'存貨/分用科目名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeeTypeRelate', @level2type = N'COLUMN', @level2name = N'StockPayName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'存貨/費用科目', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeeTypeRelate', @level2type = N'COLUMN', @level2name = N'StockPayCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收入科目名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeeTypeRelate', @level2type = N'COLUMN', @level2name = N'RevenueName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收入科目', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeeTypeRelate', @level2type = N'COLUMN', @level2name = N'RevenueCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'應收憑單單別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeeTypeRelate', @level2type = N'COLUMN', @level2name = N'ReceivableCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'應付憑單單別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeeTypeRelate', @level2type = N'COLUMN', @level2name = N'PayableCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'銷項發票品名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeeTypeRelate', @level2type = N'COLUMN', @level2name = N'InvoiceSubjectName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'鼎新品號類別名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeeTypeRelate', @level2type = N'COLUMN', @level2name = N'DinShinItemName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'鼎新品號類別代號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeeTypeRelate', @level2type = N'COLUMN', @level2name = N'DinShinItemCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'代收科目名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeeTypeRelate', @level2type = N'COLUMN', @level2name = N'CollectionReceiveName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'代收科目', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeeTypeRelate', @level2type = N'COLUMN', @level2name = N'CollectionReceiveCode';

