﻿CREATE TABLE [dbo].[TruckVendor] (
    [id]          INT           IDENTITY (1, 1) NOT NULL,
    [vendor_name] NVARCHAR (50) NULL,
    [create_user] NVARCHAR (20) NULL,
    [create_date] DATETIME      NULL,
    [update_user] NVARCHAR (20) NULL,
    [update_date] DATETIME      NULL,
    [is_active]   BIT           NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'供應商/車老闆名稱表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendor';

