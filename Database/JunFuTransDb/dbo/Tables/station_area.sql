﻿CREATE TABLE [dbo].[station_area] (
    [Id]            INT           IDENTITY (1, 1) NOT NULL,
    [area]          NVARCHAR (10) NULL,
    [station_code]  NVARCHAR (5)  NULL,
    [station_scode] NVARCHAR (5)  NULL,
    [station_name]  NVARCHAR (10) NULL,
    [create_date]   DATETIME      NULL,
    [update_date]   DATETIME      NULL,
    [update_user]   NVARCHAR (10) NULL,
    [owner]         INT           NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


