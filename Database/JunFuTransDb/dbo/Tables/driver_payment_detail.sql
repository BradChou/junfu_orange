﻿CREATE TABLE [dbo].[driver_payment_detail] (
    [Id]                             INT           IDENTITY (1, 1) NOT NULL,
    [check_number]                   NVARCHAR (20) NULL,
    [subpoena_category]              NVARCHAR (10) NULL,
    [collection_money]               INT           NULL,
    [payment_sheet_id]               INT           NOT NULL,
    [collection_money_remittance]    INT           NULL,
    [collection_money_check]         INT           NULL,
    [paid_method]                    VARCHAR (10)   NULL,
    [ticket_or_remittance_last_five] VARCHAR (5)   NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'支票票據/客戶匯款帳號末五碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'driver_payment_detail', @level2type = N'COLUMN', @level2name = N'ticket_or_remittance_last_five';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'付款方式', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'driver_payment_detail', @level2type = N'COLUMN', @level2name = N'paid_method';

