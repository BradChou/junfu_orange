﻿CREATE TABLE [dbo].[system_function_user_mapping]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [user_id] INT NOT NULL, 
    [user_type] INT NOT NULL, 
    [function_id] INT NOT NULL, 
    [create_date] DATETIME NOT NULL, 
    [update_date] DATETIME NULL, 
    [function_value] INT NOT NULL DEFAULT 2
)
