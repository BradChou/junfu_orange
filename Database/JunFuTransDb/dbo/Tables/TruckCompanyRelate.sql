﻿CREATE TABLE [dbo].[TruckCompanyRelate] (
    [id]         INT            IDENTITY (1, 1) NOT NULL,
    [CodeId]     INT            NOT NULL,
    [CodeName]   NVARCHAR (100) NOT NULL,
    [IsActive]   BIT            CONSTRAINT [DF_TruckCompanyRelate_IsActive] DEFAULT ((1)) NOT NULL,
    [CompanyUID] VARCHAR (10)   NOT NULL,
    CONSTRAINT [PK_TruckCompanyRelate] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車行統編', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckCompanyRelate', @level2type = N'COLUMN', @level2name = N'CompanyUID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否啟用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckCompanyRelate', @level2type = N'COLUMN', @level2name = N'IsActive';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車行名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckCompanyRelate', @level2type = N'COLUMN', @level2name = N'CodeName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車行代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckCompanyRelate', @level2type = N'COLUMN', @level2name = N'CodeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車老闆車行關聯PK', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckCompanyRelate', @level2type = N'COLUMN', @level2name = N'id';

