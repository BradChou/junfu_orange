﻿CREATE TABLE [dbo].[account_receivable] (
    [Id]                 INT           IDENTITY (1, 1) NOT NULL,
    [invoice_id]         VARCHAR (20)  NULL,
    [customer_code]      NVARCHAR (11) NULL,
    [customer_name]      NVARCHAR (50) NULL,
    [peices]             INT           NULL,
    [is_paid]            BIT           NULL,
    [paid_date]          DATE          NULL,
    [electronic_receipt] NVARCHAR (50) NULL,
    [create_date] DATETIME NULL, 
    [update_date] DATETIME NULL, 
    [ship_date_start] DATE NULL, 
    [ship_date_end] DATE NULL, 
    PRIMARY KEY CLUSTERED ([Id] ASC)
);



GO



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'結帳貨件總數', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'account_receivable', @level2type = N'COLUMN', @level2name = N'peices';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'支付日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'account_receivable', @level2type = N'COLUMN', @level2name = N'paid_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'支付狀態(以支付/未支付)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'account_receivable', @level2type = N'COLUMN', @level2name = N'is_paid';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'請款編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'account_receivable', @level2type = N'COLUMN', @level2name = N'invoice_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'電子發票號碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'account_receivable', @level2type = N'COLUMN', @level2name = N'electronic_receipt';

