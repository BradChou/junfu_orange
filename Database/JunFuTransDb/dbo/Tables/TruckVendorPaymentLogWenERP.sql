﻿CREATE TABLE [dbo].[TruckVendorPaymentLogWenERP] (
    [id]                       INT            IDENTITY (1, 1) NOT NULL,
    [TruckVendorPaymentLogId]  INT            NOT NULL,
    [Year]                     VARCHAR (4)    NOT NULL,
    [InvoicingNumber]          VARCHAR (11)   NULL,
    [SerialNumber]             VARCHAR (5)    NOT NULL,
    [RegisteredDate]           DATETIME       NOT NULL,
    [FeeType]                  NVARCHAR (20)  NOT NULL,
    [OrderType]                VARCHAR (10)   NOT NULL,
    [SupplierID]               INT            NOT NULL,
    [SupplierUID]              VARCHAR (10)   NOT NULL,
    [CustomerUID]              VARCHAR (10)   NOT NULL,
    [Number]                   VARCHAR (10)   NOT NULL,
    [ProductCode]              VARCHAR (10)   NOT NULL,
    [TaxType]                  VARCHAR (5)    NOT NULL,
    [TaxInclude]               BIT            CONSTRAINT [DF_TruckVendorPaymentLogWenERP_TaxInclude] DEFAULT ((0)) NOT NULL,
    [Quantity]                 INT            CONSTRAINT [DF_TruckVendorPaymentLogWenERP_Quantity] DEFAULT ((1)) NOT NULL,
    [Payment]                  INT            CONSTRAINT [DF_TruckVendorPaymentLogWenERP_Payment] DEFAULT ((0)) NOT NULL,
    [UnitPrice]                INT            CONSTRAINT [DF_TruckVendorPaymentLogWenERP_UnitPrice] DEFAULT ((0)) NOT NULL,
    [ElecInovoice]             VARCHAR (1)    NOT NULL,
    [PaperInovoice]            VARCHAR (1)    NOT NULL,
    [ReconciliationDate]       DATETIME       NULL,
    [CheckoutDate]             DATETIME       NULL,
    [PaymentDate]              DATETIME       NULL,
    [ManufactureCode]          VARCHAR (20)   NULL,
    [ProductionQuantity]       INT            NULL,
    [InvoiceNumber]            VARCHAR (20)   NULL,
    [InvoiceDate]              DATETIME       NULL,
    [VoucherType]              VARCHAR (20)   NULL,
    [Note01]                   NVARCHAR (MAX) NULL,
    [Note02]                   NVARCHAR (MAX) NULL,
    [NoteMain]                 NVARCHAR (MAX) NULL,
    [SupplierContact]          VARCHAR (20)   NULL,
    [InvoicingSpecial]         NVARCHAR (200) NULL,
    [SupplierUniformID]        VARCHAR (20)   NULL,
    [SupplierAddress]          NVARCHAR (200) NULL,
    [PackageType]              VARCHAR (20)   NULL,
    [ForeignCurrencyCode]      VARCHAR (20)   NULL,
    [ExchangeRate]             FLOAT (53)     NULL,
    [ForeignCurrencyUnitPrice] FLOAT (53)     NULL,
    [ForeignCurrencyPayment]   FLOAT (53)     NULL,
    [OrderNumber]              VARCHAR (20)   NULL,
    [PurchaseNumber]           VARCHAR (20)   NULL,
    [SalesCode]                VARCHAR (20)   NULL,
    [Department]               NVARCHAR (50)  NOT NULL,
    [ProjectCode]              NVARCHAR (50)  NULL,
    [CarLicense]               NVARCHAR (20)  NULL,
    [Company]                  NVARCHAR (20)  NOT NULL,
    [VendorName]               NVARCHAR (20)  NOT NULL,
    [CreateDate]               DATETIME       CONSTRAINT [DF_TruckVendorPaymentLogWenERP_CreateDate] DEFAULT (getdate()) NOT NULL,
    [CreateUser]               NVARCHAR (10)  NOT NULL,
    [UpdateDate]               DATETIME       NULL,
    [UpdateUser]               NVARCHAR (10)  NULL,
    [IsActive]                 BIT            CONSTRAINT [DF_TruckVendorPaymentLogWenERP_IsActive] DEFAULT ((1)) NOT NULL,
    [HasDownload]              BIT            CONSTRAINT [DF_TruckVendorPaymentLogWenERP_HasDownload] DEFAULT ((0)) NOT NULL,
    [DownloadDate]             DATETIME       NULL,
    [DownloadUser]             NVARCHAR (10)  NULL,
    CONSTRAINT [PK_TruckVendorPaymentLogWenERP] PRIMARY KEY CLUSTERED ([id] ASC)
);














GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車主', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'VendorName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車行', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'Company';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'CarLicense';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'專案\項目編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'ProjectCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'業務員代號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'SalesCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'採購單號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'PurchaseNumber';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'訂單單號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'OrderNumber';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'外幣未稅金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'ForeignCurrencyPayment';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'外幣未稅單價', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'ForeignCurrencyUnitPrice';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'外幣代號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'ForeignCurrencyCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'包裝別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'PackageType';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號(客戶供應商地址)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'SupplierAddress';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號(客戶供應商統編)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'SupplierUniformID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'進銷特殊欄位', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'InvoicingSpecial';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號(客供商聯絡人)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'SupplierContact';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'主檔備註', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'NoteMain';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'明細備註2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'Note02';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'明細備註', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'Note01';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'傳票類別,支出項目寫 20-3 (銷貨)，收入項目寫 10 (進貨)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'VoucherType';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'發票號碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'InvoiceNumber';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'生產數量', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'ProductionQuantity';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'製成品代號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'ManufactureCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收款日', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'PaymentDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'結帳日', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'CheckoutDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'對帳日', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'ReconciliationDate';


GO



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'電子發票註記', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'ElecInovoice';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'未稅單價', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'UnitPrice';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'未稅金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'Payment';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'數量,一律寫 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'Quantity';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'單價是否含稅,一律寫 0 (不含稅)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'TaxInclude';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'稅別,法人寫 3 (三聯式發票)，自然人預設寫 6 (不開發票)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'TaxType';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'產品代號,支出項目寫 ZAA0030002，收入項目抓 ZAA0010001', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'ProductCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號,一律寫 001', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'Number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'客戶供應商/請款客戶 ID 抓 tbsupplier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'SupplierID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'單別(支出項目寫 20 (銷貨)，收入項目寫 10 (進貨))', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'OrderType';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'發生日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'RegisteredDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'流水號(5)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'SerialNumber';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'進銷單號碼(暫時不用)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'InvoicingNumber';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'年度', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'Year';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'更新人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'UpdateUser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'更新日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'UpdateDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TruckVendorPaymentLog 原表id', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'TruckVendorPaymentLogId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否啟用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'IsActive';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否已下載', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'HasDownload';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'CreateUser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'CreateDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'下載人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'DownloadUser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'下載日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'DownloadDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'列印紙本電子發票', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'PaperInovoice';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'支出項目/費用類別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLogWenERP', @level2type = N'COLUMN', @level2name = N'FeeType';


GO



GO


