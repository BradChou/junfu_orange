﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE spGetFunction
	-- Add the parameters for the stored procedure here
	@is_menu int
AS
BEGIN
	select 
		sub_category_id,
		function_name,
		function_url
	from 
		system_function
	where
		is_menu = @is_menu
END