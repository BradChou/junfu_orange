﻿CREATE TABLE [dbo].[ttPriceBusinessDefineLog] (
    [log_id]              NUMERIC (18)  IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [customer_code]       NVARCHAR (12) NULL,
    [tariffs_effect_date] DATE          NULL,
    [tariffs_type]        CHAR (1)      NULL,
    [cdate]               DATETIME      NOT NULL,
    [cuser]               NVARCHAR (20) NULL,
    CONSTRAINT [PK_ttPriceBusinessDefineLog] PRIMARY KEY CLUSTERED ([log_id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPriceBusinessDefineLog', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPriceBusinessDefineLog', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'關稅類型', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPriceBusinessDefineLog', @level2type = N'COLUMN', @level2name = N'tariffs_type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'關稅效應', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPriceBusinessDefineLog', @level2type = N'COLUMN', @level2name = N'tariffs_effect_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'顧客代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPriceBusinessDefineLog', @level2type = N'COLUMN', @level2name = N'customer_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'客戶訂價表日誌', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPriceBusinessDefineLog';

