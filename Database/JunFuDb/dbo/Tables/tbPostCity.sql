﻿CREATE TABLE [dbo].[tbPostCity] (
    [seq]  NUMERIC (18)  IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [city] NVARCHAR (10) NULL,
    CONSTRAINT [PK_tbPostCity] PRIMARY KEY CLUSTERED ([seq] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'寄送縣市', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPostCity', @level2type = N'COLUMN', @level2name = N'city';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPostCity', @level2type = N'COLUMN', @level2name = N'seq';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'寄送縣市', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPostCity';

