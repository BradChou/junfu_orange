﻿CREATE TABLE [dbo].[tbReceiver] (
    [receiver_id]   NUMERIC (18)   IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [customer_code] NVARCHAR (20)  NULL,
    [receiver_code] NVARCHAR (20)  NULL,
    [receiver_name] NVARCHAR (20)  NULL,
    [tel]           NVARCHAR (20)  NULL,
    [tel_ext]       NVARCHAR (10)  NULL,
    [tel2]          NVARCHAR (20)  NULL,
    [address_city]  NVARCHAR (10)  NULL,
    [address_area]  NVARCHAR (10)  NULL,
    [address_road]  NVARCHAR (50)  NULL,
    [memo]          NVARCHAR (100) NULL,
    [cuser]         NVARCHAR (20)  NULL,
    [cdate]         DATETIME       NULL,
    [uuser]         NVARCHAR (20)  NULL,
    [udate]         DATETIME       NULL,
    CONSTRAINT [PK_tbReceiver] PRIMARY KEY CLUSTERED ([receiver_id] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'修改日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbReceiver', @level2type = N'COLUMN', @level2name = N'udate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'修改人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbReceiver', @level2type = N'COLUMN', @level2name = N'uuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建檔日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbReceiver', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建檔人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbReceiver', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'備註', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbReceiver', @level2type = N'COLUMN', @level2name = N'memo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收貨地址', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbReceiver', @level2type = N'COLUMN', @level2name = N'address_road';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收貨區(鄉鎮)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbReceiver', @level2type = N'COLUMN', @level2name = N'address_area';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收貨縣市', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbReceiver', @level2type = N'COLUMN', @level2name = N'address_city';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收貨單位電話2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbReceiver', @level2type = N'COLUMN', @level2name = N'tel2';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收貨單位分機', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbReceiver', @level2type = N'COLUMN', @level2name = N'tel_ext';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收貨單位電話', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbReceiver', @level2type = N'COLUMN', @level2name = N'tel';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收貨單位', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbReceiver', @level2type = N'COLUMN', @level2name = N'receiver_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收件簡碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbReceiver', @level2type = N'COLUMN', @level2name = N'receiver_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'客戶代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbReceiver', @level2type = N'COLUMN', @level2name = N'customer_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收件地址記錄表(b2b)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbReceiver';

