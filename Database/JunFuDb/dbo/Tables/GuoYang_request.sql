﻿CREATE TABLE [dbo].[GuoYang_request] (
    [id]                   INT            IDENTITY (1, 1) NOT NULL,
    [fse_check_number]     VARCHAR (20)   NULL,
    [guoyang_check_number] VARCHAR (30)   NULL,
    [cdate]                DATETIME       NULL,
    [active_flag]          BIT            NULL,
    [schedule_id]          INT            NULL,
    [receive_contact]      NVARCHAR (40)  NULL,
    [receive_address]      NVARCHAR (120) NULL,
    [receive_tel]          NVARCHAR (20)  NULL,
    [e_mail]               NVARCHAR (50)  NULL,
    [detail_check_number]  VARCHAR (30)   NULL,
    [collection_money]     INT            NULL,
    [remark]               NVARCHAR (200) NULL,
    [is_echo]              BIT            NULL,
    [is_closed]            BIT            NULL,
    [is_uploaded]          BIT            NULL,
    [upload_file_name]     NVARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);







