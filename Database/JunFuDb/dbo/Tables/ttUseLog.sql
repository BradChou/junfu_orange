﻿CREATE TABLE [dbo].[ttUseLog] (
    [id]           BIGINT         IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [account_code] NVARCHAR (20)  NULL,
    [UseItem]      NVARCHAR (100) NULL,
    [Memo]         NVARCHAR (200) NULL,
    [IP]           VARCHAR (30)   NULL,
    [cdate]        DATETIME       NULL,
    CONSTRAINT [PK_ttUseLog] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-account_code-cdate]
    ON [dbo].[ttUseLog]([account_code] ASC, [cdate] ASC)
    INCLUDE([UseItem], [IP]);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'創建日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttUseLog', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttUseLog', @level2type = N'COLUMN', @level2name = N'IP';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'註解', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttUseLog', @level2type = N'COLUMN', @level2name = N'Memo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用項目', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttUseLog', @level2type = N'COLUMN', @level2name = N'UseItem';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'帳戶代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttUseLog', @level2type = N'COLUMN', @level2name = N'account_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttUseLog', @level2type = N'COLUMN', @level2name = N'id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用紀錄', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttUseLog';

