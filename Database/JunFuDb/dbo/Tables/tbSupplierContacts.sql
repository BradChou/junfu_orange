﻿CREATE TABLE [dbo].[tbSupplierContacts] (
    [contact_id]    NUMERIC (18)  IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [supplier_code] NVARCHAR (3)  NULL,
    [contact_name]  NVARCHAR (20) NULL,
    [contact_email] NVARCHAR (50) NULL,
    CONSTRAINT [PK_tbSupplierContacts] PRIMARY KEY CLUSTERED ([contact_id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'聯絡人信箱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSupplierContacts', @level2type = N'COLUMN', @level2name = N'contact_email';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'聯絡人名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSupplierContacts', @level2type = N'COLUMN', @level2name = N'contact_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區配商代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSupplierContacts', @level2type = N'COLUMN', @level2name = N'supplier_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSupplierContacts', @level2type = N'COLUMN', @level2name = N'contact_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區配商聯絡人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSupplierContacts';

