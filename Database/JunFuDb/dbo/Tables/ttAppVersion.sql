﻿CREATE TABLE [dbo].[ttAppVersion] (
    [ver_id]       NUMERIC (18)  IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [app_id]       VARCHAR (20)  NULL,
    [app_nme]      VARCHAR (30)  NULL,
    [version_code] INT           NULL,
    [version_name] VARCHAR (30)  NULL,
    [version_url]  VARCHAR (120) NULL,
    [use_flag]     INT           NULL,
    [version_date] DATETIME      NULL,
    CONSTRAINT [PK_ttAppVersion] PRIMARY KEY CLUSTERED ([ver_id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'版本日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAppVersion', @level2type = N'COLUMN', @level2name = N'version_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否用標記', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAppVersion', @level2type = N'COLUMN', @level2name = N'use_flag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'連結', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAppVersion', @level2type = N'COLUMN', @level2name = N'version_url';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'版本', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAppVersion', @level2type = N'COLUMN', @level2name = N'version_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'版本代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAppVersion', @level2type = N'COLUMN', @level2name = N'version_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'app名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAppVersion', @level2type = N'COLUMN', @level2name = N'app_nme';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'app帳號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAppVersion', @level2type = N'COLUMN', @level2name = N'app_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'版本編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAppVersion', @level2type = N'COLUMN', @level2name = N'ver_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'應用程式版本', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAppVersion';

