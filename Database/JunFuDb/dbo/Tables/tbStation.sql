﻿CREATE TABLE [dbo].[tbStation] (
    [id]               INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [station_code]     NVARCHAR (20)  NULL,
    [station_scode]    NVARCHAR (20)  NULL,
    [station_name]     NVARCHAR (100) NULL,
    [active_flag]      BIT            NULL,
    [BusinessDistrict] NVARCHAR (20)  NULL,
    [udate]            DATETIME       NULL,
    [uuser]            NVARCHAR (20)  NULL,
    [owner]            INT            NULL,
    CONSTRAINT [PK_tbStation] PRIMARY KEY CLUSTERED ([id] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbStation', @level2type = N'COLUMN', @level2name = N'uuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbStation', @level2type = N'COLUMN', @level2name = N'udate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'經營處', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbStation', @level2type = N'COLUMN', @level2name = N'BusinessDistrict';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'啟用標記', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbStation', @level2type = N'COLUMN', @level2name = N'active_flag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'到著站名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbStation', @level2type = N'COLUMN', @level2name = N'station_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'到著站代碼數字', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbStation', @level2type = N'COLUMN', @level2name = N'station_scode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'到著站代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbStation', @level2type = N'COLUMN', @level2name = N'station_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbStation', @level2type = N'COLUMN', @level2name = N'id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'到著站', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbStation';

