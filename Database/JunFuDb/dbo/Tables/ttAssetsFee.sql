﻿CREATE TABLE [dbo].[ttAssetsFee] (
    [id]                  BIGINT        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [fee_type]            VARCHAR (2)   NULL,
    [dept_id]             NVARCHAR (10) NULL,
    [a_id]                VARCHAR (10)  NULL,
    [date]                DATETIME      NULL,
    [fee_sdate]           DATETIME      NULL,
    [fee_edate]           DATETIME      NULL,
    [oil]                 VARCHAR (50)  NULL,
    [company]             NVARCHAR (50) NULL,
    [items]               VARCHAR (10)  NULL,
    [detail]              VARCHAR (20)  NULL,
    [milage]              FLOAT (53)    NULL,
    [litre]               FLOAT (53)    NULL,
    [price]               FLOAT (53)    NULL,
    [buy_price]           FLOAT (53)    NULL,
    [receipt_price]       FLOAT (53)    NULL,
    [price_notax]         FLOAT (53)    NULL,
    [buy_price_notax]     FLOAT (53)    NULL,
    [receipt_price_notax] FLOAT (53)    NULL,
    [quant]               FLOAT (53)    NULL,
    [total_price]         FLOAT (53)    NULL,
    [memo]                NVARCHAR (30) NULL,
    [starttime]           DATETIME      NULL,
    [cdate]               DATETIME      NULL,
    [cuser]               VARCHAR (20)  NULL,
    [udate]               DATETIME      NULL,
    [uuser]               VARCHAR (20)  NULL,
    CONSTRAINT [PK_ttAssetsFee] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'修改人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'uuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'修改時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'udate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建檔人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建檔時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'費用發生時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'starttime';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'備註', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'memo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'總價', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'total_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'購買數量', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'quant';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'未稅價(發票價)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'receipt_price_notax';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'未稅價(買價)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'buy_price_notax';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'未稅價(定價)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'price_notax';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'發票價', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'receipt_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'買價(油)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'buy_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'定價(油)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'公升數(油)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'litre';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'里程數', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'milage';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'費用類別說明', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'detail';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'費用類別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'items';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'提供服務公司', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'company';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'加油公司', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'oil';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'費用發生迄止日', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'fee_edate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'費用發生起始日', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'fee_sdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'費用發生日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資產代碼(車牌)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'a_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'費用類別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'fee_type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'主鍵', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsFee', @level2type = N'COLUMN', @level2name = N'id';

