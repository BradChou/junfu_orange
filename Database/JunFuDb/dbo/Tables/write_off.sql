﻿CREATE TABLE [dbo].[write_off] (
    [check_number]   VARCHAR (20) NOT NULL,
    [write_off_id]   INT          NULL,
    [udate]          DATETIME     NULL,
    [write_off_type] NVARCHAR (5) NULL,
    [cdate]          DATETIME     NULL,
    CONSTRAINT [PK__write_of__1C48229EDC2AE1B8] PRIMARY KEY CLUSTERED ([check_number] ASC)
);

