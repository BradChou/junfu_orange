﻿CREATE TABLE [dbo].[tbDeliveryNumberSetting] (
    [seq]            INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [customer_code]  NVARCHAR (20) NULL,
    [begin_number]   BIGINT        NULL,
    [end_number]     BIGINT        NULL,
    [current_number] BIGINT        NULL,
    [cuser]          NVARCHAR (10) NULL,
    [cdate]          DATETIME      NULL,
    [IsActive]       INT           NULL,
    [num]            INT           NULL,
    CONSTRAINT [PK_tbDeliveryNumberSetting] PRIMARY KEY CLUSTERED ([seq] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否啟用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDeliveryNumberSetting', @level2type = N'COLUMN', @level2name = N'IsActive';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'廠商使用日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDeliveryNumberSetting', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'廠商', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDeliveryNumberSetting', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'現在配送編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDeliveryNumberSetting', @level2type = N'COLUMN', @level2name = N'current_number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'配送結束編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDeliveryNumberSetting', @level2type = N'COLUMN', @level2name = N'end_number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'配送起始編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDeliveryNumberSetting', @level2type = N'COLUMN', @level2name = N'begin_number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'客戶代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDeliveryNumberSetting', @level2type = N'COLUMN', @level2name = N'customer_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDeliveryNumberSetting', @level2type = N'COLUMN', @level2name = N'seq';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'配送號碼設定', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDeliveryNumberSetting';

