﻿CREATE TABLE [dbo].[ttLoan] (
    [id]                  INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [car_license]         NVARCHAR (10) NULL,
    [car_price]           INT           NULL,
    [first_payment]       INT           NULL,
    [loan_price]          INT           NULL,
    [annual_rate]         FLOAT (53)    NULL,
    [loan_period]         FLOAT (53)    NULL,
    [annual_pay_count]    INT           NULL,
    [loan_startdate]      DATETIME      NULL,
    [installment_price]   INT           NULL,
    [installment_count]   INT           NULL,
    [paid_count]          INT           NULL,
    [interest]            INT           NULL,
    [loan_company]        NVARCHAR (20) NULL,
    [memo]                NVARCHAR (50) NULL,
    [car_price_s]         INT           NULL,
    [first_payment_s]     INT           NULL,
    [loan_price_s]        INT           NULL,
    [loan_startdate_s]    DATETIME      NULL,
    [annual_rate_s]       FLOAT (53)    NULL,
    [loan_period_s]       FLOAT (53)    NULL,
    [annual_pay_count_s]  INT           NULL,
    [installment_price_s] INT           NULL,
    [installment_count_s] INT           NULL,
    [paid_count_s]        INT           NULL,
    [interest_s]          INT           NULL,
    [deposit_s]           INT           NULL,
    [cuser]               VARCHAR (10)  NULL,
    [cdate]               DATETIME      NULL,
    [uuser]               VARCHAR (10)  NULL,
    [udate]               DATETIME      NULL,
    CONSTRAINT [PK_ttLoan] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'udate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'uuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'公司使用日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'公司使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'支付訂金s', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'deposit_s';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'利息s', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'interest_s';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'已付數量s', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'paid_count_s';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'分期數量s', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'installment_count_s';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'分期付款價錢s', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'installment_price_s';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'每年付款數量s', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'annual_pay_count_s';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貸款年限s', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'loan_period_s';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'年利率s', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'annual_rate_s';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貸款開始日期s', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'loan_startdate_s';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貸款價錢s', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'loan_price_s';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'頭期款s', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'first_payment_s';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貨車價錢s', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'car_price_s';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'註解', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'memo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貸款公司', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'loan_company';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'利息', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'interest';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'已付數量', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'paid_count';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'分期數量', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'installment_count';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'分期付款價錢', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'installment_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貸款開始日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'loan_startdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'每年付款數量', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'annual_pay_count';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貸款年限', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'loan_period';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'年利率', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'annual_rate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貸款價錢', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'loan_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'頭期款', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'first_payment';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貨車價錢', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'car_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車牌號碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'car_license';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan', @level2type = N'COLUMN', @level2name = N'id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貸款', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttLoan';

