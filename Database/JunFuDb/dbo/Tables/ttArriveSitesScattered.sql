﻿CREATE TABLE [dbo].[ttArriveSitesScattered] (
    [seq]          INT           NOT NULL,
    [post_city]    NVARCHAR (10) NULL,
    [post_area]    NVARCHAR (10) NULL,
    [zip]          NVARCHAR (3)  NULL,
    [station_code] NVARCHAR (10) NULL,
    CONSTRAINT [PK_ttArriveSitesScattered_] PRIMARY KEY CLUSTERED ([seq] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'站代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttArriveSitesScattered', @level2type = N'COLUMN', @level2name = N'station_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'郵遞區號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttArriveSitesScattered', @level2type = N'COLUMN', @level2name = N'zip';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'郵寄地區', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttArriveSitesScattered', @level2type = N'COLUMN', @level2name = N'post_area';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'郵寄城市', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttArriveSitesScattered', @level2type = N'COLUMN', @level2name = N'post_city';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'配達站分散', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttArriveSitesScattered';

