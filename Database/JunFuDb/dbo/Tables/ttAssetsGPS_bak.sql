﻿CREATE TABLE [dbo].[ttAssetsGPS_bak] (
    [id]          INT         IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [a_id]        INT         NULL,
    [GPS]         VARCHAR (2) NULL,
    [active_time] DATETIME    NULL,
    [cdate]       DATETIME    NULL,
    CONSTRAINT [PK_ttAssetsGPS] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'廠商使用日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsGPS_bak', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'啟用時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsGPS_bak', @level2type = N'COLUMN', @level2name = N'active_time';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'備有GPS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsGPS_bak', @level2type = N'COLUMN', @level2name = N'GPS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車子編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsGPS_bak', @level2type = N'COLUMN', @level2name = N'a_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsGPS_bak', @level2type = N'COLUMN', @level2name = N'id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貨車GPS備份', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsGPS_bak';

