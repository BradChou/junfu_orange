﻿CREATE TABLE [dbo].[tbPostCityArea] (
    [seq]  NUMERIC (18)  IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [city] NVARCHAR (10) NULL,
    [area] NVARCHAR (10) NULL,
    [zip]  NVARCHAR (3)  NULL,
    CONSTRAINT [PK_tbPostCityArea] PRIMARY KEY CLUSTERED ([seq] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'郵遞區號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPostCityArea', @level2type = N'COLUMN', @level2name = N'zip';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'地區', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPostCityArea', @level2type = N'COLUMN', @level2name = N'area';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'城市', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPostCityArea', @level2type = N'COLUMN', @level2name = N'city';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'寄送城市地區表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPostCityArea';

