﻿CREATE TABLE [dbo].[tbMemberCollectionPrice] (
    [seq] INT IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [min] INT NULL,
    [max] INT NULL,
    CONSTRAINT [PK_tbMemberCollectionPrice] PRIMARY KEY CLUSTERED ([seq] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最大值', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbMemberCollectionPrice', @level2type = N'COLUMN', @level2name = N'max';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最小值', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbMemberCollectionPrice', @level2type = N'COLUMN', @level2name = N'min';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbMemberCollectionPrice', @level2type = N'COLUMN', @level2name = N'seq';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'會員群價錢', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbMemberCollectionPrice';

