﻿CREATE TABLE [dbo].[ttKuoYangScanLog] (
    [log_id]       DECIMAL (18)   IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [check_number] NVARCHAR (20)  NULL,
    [order_number] NVARCHAR (30)  NULL,
    [flag_type]    BIT            NULL,
    [cdate]        DATETIME       NULL,
    [udate]        DATETIME       NULL,
    [ImportFile]   NVARCHAR (100) NULL,
    CONSTRAINT [PK_ttKuoYangScanLog] PRIMARY KEY CLUSTERED ([log_id] ASC)
);

