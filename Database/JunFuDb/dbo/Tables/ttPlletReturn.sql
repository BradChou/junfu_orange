﻿CREATE TABLE [dbo].[ttPlletReturn] (
    [id]                INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [request_id]        BIGINT        NULL,
    [return_cnt]        INT           NULL,
    [return_date]       DATETIME      NULL,
    [return_request_id] BIGINT        NULL,
    [udate]             DATETIME      NULL,
    [uuser]             NVARCHAR (10) NULL,
    CONSTRAINT [PK_ttPlletReturn] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPlletReturn', @level2type = N'COLUMN', @level2name = N'uuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPlletReturn', @level2type = N'COLUMN', @level2name = N'udate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'退還要求編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPlletReturn', @level2type = N'COLUMN', @level2name = N'return_request_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'退還日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPlletReturn', @level2type = N'COLUMN', @level2name = N'return_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'退還數量', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPlletReturn', @level2type = N'COLUMN', @level2name = N'return_cnt';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'要求編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPlletReturn', @level2type = N'COLUMN', @level2name = N'request_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPlletReturn', @level2type = N'COLUMN', @level2name = N'id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'棧板退還', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPlletReturn';

