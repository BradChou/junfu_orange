﻿CREATE TABLE [dbo].[ttAppLog] (
    [log_id]       NUMERIC (18)   IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [app_project]  VARCHAR (30)   NULL,
    [app_fun_name] VARCHAR (40)   NULL,
    [app_type]     VARCHAR (10)   NULL,
    [app_memo]     VARCHAR (300)  NULL,
    [app_log]      VARCHAR (2200) NULL,
    [cdate]        DATETIME       NULL,
    CONSTRAINT [PK_ttAppLog] PRIMARY KEY CLUSTERED ([log_id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAppLog', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日誌', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAppLog', @level2type = N'COLUMN', @level2name = N'app_log';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'備忘錄', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAppLog', @level2type = N'COLUMN', @level2name = N'app_memo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'狀態', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAppLog', @level2type = N'COLUMN', @level2name = N'app_type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'別名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAppLog', @level2type = N'COLUMN', @level2name = N'app_fun_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'應用程式專案', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAppLog', @level2type = N'COLUMN', @level2name = N'app_project';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'應用程式日誌', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAppLog';

