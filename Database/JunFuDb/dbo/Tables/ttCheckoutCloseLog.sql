﻿CREATE TABLE [dbo].[ttCheckoutCloseLog] (
    [log_id]        NUMERIC (18)  IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [type]          INT           NULL,
    [src]           NVARCHAR (50) NULL,
    [customer_code] NVARCHAR (50) NULL,
    [supplier_code] NVARCHAR (50) NULL,
    [customer_type] CHAR (1)      NULL,
    [close_begdate] DATE          NULL,
    [close_enddate] DATE          NULL,
    [close_type]    CHAR (1)      NULL,
    [supplier_fee]  INT           NULL,
    [cscetion_fee]  INT           NULL,
    [randomCode]    NVARCHAR (10) NOT NULL,
    [remote_fee]    INT           NULL,
    [price]         INT           NULL,
    [latest]        BIT           NULL,
    [cdate]         DATE          NULL,
    [cuser]         NVARCHAR (10) NULL,
    CONSTRAINT [PK_ttCheckoutCloseLog] PRIMARY KEY CLUSTERED ([log_id] ASC)
);

