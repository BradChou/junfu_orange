﻿CREATE TABLE [dbo].[CheckNumberPackageProcess] (
    [request_id]          INT            IDENTITY (1, 1) NOT NULL,
    [file_size_kb]        VARCHAR (MAX)  NULL,
    [file_total_records]  INT            NULL,
    [file_copied_records] INT            NULL,
    [file_not_exists]     INT            NULL,
    [file_copied_fail]    INT            NULL,
    [start_time]          DATETIME       NULL,
    [end_time]            DATETIME       NULL,
    [account_code]        VARCHAR (100)  NULL,
    [isAccessDownload]    BIT            NULL,
    [data_name]           NVARCHAR (MAX) NULL,
    [data_path]           NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([request_id] ASC)
);










GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'本次需求簽單總筆數', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CheckNumberPackageProcess', @level2type = N'COLUMN', @level2name = N'file_total_records';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'本次簽單檔案大小總計', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CheckNumberPackageProcess', @level2type = N'COLUMN', @level2name = N'file_size_kb';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'本次需求簽單已發現不存在筆數', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CheckNumberPackageProcess', @level2type = N'COLUMN', @level2name = N'file_not_exists';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'本次需求簽單已取得筆數', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CheckNumberPackageProcess', @level2type = N'COLUMN', @level2name = N'file_copied_records';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'複製檔案過程中失敗筆數', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CheckNumberPackageProcess', @level2type = N'COLUMN', @level2name = N'file_copied_fail';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'提出本次下載需求的帳號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CheckNumberPackageProcess', @level2type = N'COLUMN', @level2name = N'account_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'檔案複製完成時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CheckNumberPackageProcess', @level2type = N'COLUMN', @level2name = N'end_time';

