﻿CREATE TABLE [dbo].[ttDispatchTaskDetail_B] (
    [id]        INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [t_id]      INT           NULL,
    [date]      DATETIME      NULL,
    [warehouse] NVARCHAR (10) NULL,
    [supplier]  NVARCHAR (10) NULL,
    [plates]    INT           NULL,
    CONSTRAINT [PK_ttDispatchTaskDetail_B] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'板材', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTaskDetail_B', @level2type = N'COLUMN', @level2name = N'plates';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'供應商代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTaskDetail_B', @level2type = N'COLUMN', @level2name = N'supplier';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'倉庫代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTaskDetail_B', @level2type = N'COLUMN', @level2name = N'warehouse';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'發送日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTaskDetail_B', @level2type = N'COLUMN', @level2name = N'date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'作業編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTaskDetail_B', @level2type = N'COLUMN', @level2name = N't_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTaskDetail_B', @level2type = N'COLUMN', @level2name = N'id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'發送作業細節B', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTaskDetail_B';

