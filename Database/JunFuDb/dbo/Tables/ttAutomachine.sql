﻿CREATE TABLE [dbo].[ttAutomachine] (
    [Automachine_id] NUMERIC (18)   IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [request_id]     NUMERIC (20)   NOT NULL,
    [check_number]   NVARCHAR (20)  NULL,
    [pic]            NVARCHAR (MAX) NULL,
    [cdate]          DATETIME       NULL,
    CONSTRAINT [PK_ttAutomachine] PRIMARY KEY CLUSTERED ([Automachine_id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'公司使用日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAutomachine', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'圖片檔', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAutomachine', @level2type = N'COLUMN', @level2name = N'pic';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貨號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAutomachine', @level2type = N'COLUMN', @level2name = N'check_number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'請求序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAutomachine', @level2type = N'COLUMN', @level2name = N'request_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'自動機器序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAutomachine', @level2type = N'COLUMN', @level2name = N'Automachine_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'自動機器', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAutomachine';

