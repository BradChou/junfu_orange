﻿CREATE TABLE [dbo].[ttItemCodesFieldArea] (
    [seq]          NUMERIC (18)   IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Account_Code] NVARCHAR (20)  NULL,
    [code_name]    NVARCHAR (50)  NULL,
    [active_flag]  BIT            NULL,
    [memo]         NVARCHAR (255) NULL,
    [cuser]        NVARCHAR (20)  NULL,
    [cdate]        DATETIME       NULL,
    [uuser]        NVARCHAR (20)  NULL,
    [udate]        DATETIME       NULL,
    CONSTRAINT [PK_ttItemCodesFieldArea] PRIMARY KEY CLUSTERED ([seq] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttItemCodesFieldArea', @level2type = N'COLUMN', @level2name = N'udate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttItemCodesFieldArea', @level2type = N'COLUMN', @level2name = N'uuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'公司使用者日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttItemCodesFieldArea', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'公司使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttItemCodesFieldArea', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'標註', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttItemCodesFieldArea', @level2type = N'COLUMN', @level2name = N'memo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'啟用標記', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttItemCodesFieldArea', @level2type = N'COLUMN', @level2name = N'active_flag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'代碼名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttItemCodesFieldArea', @level2type = N'COLUMN', @level2name = N'code_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'帳戶代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttItemCodesFieldArea', @level2type = N'COLUMN', @level2name = N'Account_Code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttItemCodesFieldArea', @level2type = N'COLUMN', @level2name = N'seq';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貨追區域', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttItemCodesFieldArea';

