﻿CREATE TABLE [dbo].[tbAssetsAccountShareChange] (
    [id]           INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [a_id]         INT           NULL,
    [car_license]  NVARCHAR (10) NULL,
    [account_dept] VARCHAR (2)   NULL,
    [share]        INT           NULL,
    [udate]        DATETIME      NULL,
    [uuser]        VARCHAR (10)  NULL,
    CONSTRAINT [PK_tbAssetsAccountShareChange] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'變更人員', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAssetsAccountShareChange', @level2type = N'COLUMN', @level2name = N'uuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'變更日', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAssetsAccountShareChange', @level2type = N'COLUMN', @level2name = N'udate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'比例', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAssetsAccountShareChange', @level2type = N'COLUMN', @level2name = N'share';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'拆帳月(定義待定)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAssetsAccountShareChange', @level2type = N'COLUMN', @level2name = N'account_dept';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車牌', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAssetsAccountShareChange', @level2type = N'COLUMN', @level2name = N'car_license';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資產序號(ttAssets 主鍵)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAssetsAccountShareChange', @level2type = N'COLUMN', @level2name = N'a_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'主鍵', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAssetsAccountShareChange', @level2type = N'COLUMN', @level2name = N'id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資產(貨車)會計分擔表(log)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAssetsAccountShareChange';

