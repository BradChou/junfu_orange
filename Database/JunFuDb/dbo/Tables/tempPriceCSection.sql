﻿CREATE TABLE [dbo].[tempPriceCSection] (
    [id]           NUMERIC (18)    IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [start_city]   NVARCHAR (10)   NULL,
    [end_city]     NVARCHAR (10)   NULL,
    [class_level]  INT             NULL,
    [pricing_code] NVARCHAR (2)    NULL,
    [plate1_price] NUMERIC (18, 2) NULL,
    [plate2_price] NUMERIC (18, 2) NULL,
    [plate3_price] NUMERIC (18, 2) NULL,
    [plate4_price] NUMERIC (18, 2) NULL,
    [plate5_price] NUMERIC (18, 2) NULL,
    [plate6_price] NUMERIC (18, 2) NULL,
    CONSTRAINT [PK_tempPriceCSection] PRIMARY KEY CLUSTERED ([id] ASC)
);

