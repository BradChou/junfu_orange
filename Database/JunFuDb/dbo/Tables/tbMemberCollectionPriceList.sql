﻿CREATE TABLE [dbo].[tbMemberCollectionPriceList] (
    [seq]             INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [customer_code]   NVARCHAR (20) NULL,
    [Handling_fee]    INT           NULL,
    [order_fee]       FLOAT (53)    NULL,
    [CollectionPrice] INT           NULL,
    CONSTRAINT [PK_tbMemberCollectionPriceList] PRIMARY KEY CLUSTERED ([seq] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'價格型別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbMemberCollectionPriceList', @level2type = N'COLUMN', @level2name = N'CollectionPrice';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'訂購費', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbMemberCollectionPriceList', @level2type = N'COLUMN', @level2name = N'order_fee';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'手續費', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbMemberCollectionPriceList', @level2type = N'COLUMN', @level2name = N'Handling_fee';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'客戶代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbMemberCollectionPriceList', @level2type = N'COLUMN', @level2name = N'customer_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbMemberCollectionPriceList', @level2type = N'COLUMN', @level2name = N'seq';

