﻿CREATE TABLE [dbo].[ttAssetsExpenditure] (
    [id]            BIGINT         IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [type]          INT            NULL,
    [fee_type]      VARCHAR (2)    NULL,
    [fee_type_name] NVARCHAR (50)  NULL,
    [oil]           VARCHAR (10)   NULL,
    [fee_date]      NVARCHAR (7)   NULL,
    [company]       NVARCHAR (50)  NULL,
    [pay_kind]      VARCHAR (2)    NULL,
    [paper]         VARCHAR (20)   NULL,
    [pay_date]      DATETIME       NULL,
    [pay_account]   NVARCHAR (20)  NULL,
    [pay_num]       NVARCHAR (20)  NULL,
    [memo]          NVARCHAR (MAX) NULL,
    [money]         FLOAT (53)     NULL,
    [price]         FLOAT (53)     NULL,
    [tax]           FLOAT (53)     NULL,
    [idate]         DATETIME       NULL,
    [iuser]         VARCHAR (20)   NULL,
    [cdate]         DATETIME       NULL,
    [cuser]         VARCHAR (20)   NULL,
    [udate]         DATETIME       NULL,
    [uuser]         VARCHAR (20)   NULL,
    CONSTRAINT [PK_ttAssetsExpenditure] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsExpenditure', @level2type = N'COLUMN', @level2name = N'uuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsExpenditure', @level2type = N'COLUMN', @level2name = N'udate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'公司使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsExpenditure', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'公司使用日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsExpenditure', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'i使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsExpenditure', @level2type = N'COLUMN', @level2name = N'iuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'i日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsExpenditure', @level2type = N'COLUMN', @level2name = N'idate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'稅', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsExpenditure', @level2type = N'COLUMN', @level2name = N'tax';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'價錢', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsExpenditure', @level2type = N'COLUMN', @level2name = N'price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'總價', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsExpenditure', @level2type = N'COLUMN', @level2name = N'money';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'註解', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsExpenditure', @level2type = N'COLUMN', @level2name = N'memo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'付款數目', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsExpenditure', @level2type = N'COLUMN', @level2name = N'pay_num';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'付款帳戶', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsExpenditure', @level2type = N'COLUMN', @level2name = N'pay_account';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'付款日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsExpenditure', @level2type = N'COLUMN', @level2name = N'pay_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'紙張', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsExpenditure', @level2type = N'COLUMN', @level2name = N'paper';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'付款種類', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsExpenditure', @level2type = N'COLUMN', @level2name = N'pay_kind';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'加油站公司名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsExpenditure', @level2type = N'COLUMN', @level2name = N'company';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'費用日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsExpenditure', @level2type = N'COLUMN', @level2name = N'fee_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'加油站', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsExpenditure', @level2type = N'COLUMN', @level2name = N'oil';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'費用類型名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsExpenditure', @level2type = N'COLUMN', @level2name = N'fee_type_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'費用類型', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsExpenditure', @level2type = N'COLUMN', @level2name = N'fee_type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'類型', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsExpenditure', @level2type = N'COLUMN', @level2name = N'type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsExpenditure', @level2type = N'COLUMN', @level2name = N'id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貨車支出費用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsExpenditure';

