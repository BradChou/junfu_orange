﻿CREATE TABLE [dbo].[ttDeliveryScanLog] (
    [log_id]           NUMERIC (18)  IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [driver_code]      NVARCHAR (10) NULL,
    [check_number]     NVARCHAR (20) NULL,
    [scan_item]        VARCHAR (2)   NULL,
    [scan_date]        DATETIME      NULL,
    [area_arrive_code] NVARCHAR (10) NULL,
    [platform]         NVARCHAR (20) NULL,
    [car_number]       NVARCHAR (20) NULL,
    [sowage_rate]      NVARCHAR (20) NULL,
    [area]             NVARCHAR (20) NULL,
    [ship_mode]        NVARCHAR (2)  NULL,
    [goods_type]       NVARCHAR (2)  NULL,
    [pieces]           INT           NULL,
    [weight]           FLOAT (53)    NULL,
    [runs]             INT           NULL,
    [plates]           INT           NULL,
    [exception_option] NVARCHAR (20) NULL,
    [arrive_option]    NVARCHAR (20) NULL,
    [sign_form_image]  NVARCHAR (50) NULL,
    [sign_field_image] NVARCHAR (50) NULL,
    [deliveryupload]   DATETIME      NULL,
    [photoupload]      DATETIME      NULL,
    [cdate]            DATETIME      NULL,
    [cuser]            NVARCHAR (10) NULL,
    [tracking_number]  NVARCHAR (20) NULL,
    [Del_status]       INT           NULL,
    [VoucherMoney]     INT           NULL,
    [CashMoney]        INT           NULL,
    CONSTRAINT [PK_ttDeliveryScanLog] PRIMARY KEY CLUSTERED ([log_id] ASC)
);






GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-scan_item-arrive_option-check_number]
    ON [dbo].[ttDeliveryScanLog]([scan_item] ASC, [arrive_option] ASC)
    INCLUDE([driver_code], [check_number], [scan_date]);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-scan_date]
    ON [dbo].[ttDeliveryScanLog]([scan_date] ASC)
    INCLUDE([check_number]);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-check_number-scan_date]
    ON [dbo].[ttDeliveryScanLog]([check_number] ASC, [scan_date] ASC, [scan_item] ASC, [arrive_option] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貨追號碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'tracking_number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'新增的使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'新增日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'照片上傳', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'photoupload';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'配送上傳', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'deliveryupload';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'簽署區域圖檔', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'sign_field_image';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'簽署表格圖檔', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'sign_form_image';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'配達項目1.客戶不在2.約定再配3.正常配交4.拒收5.地址錯誤6.查無此人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'arrive_option';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'例外選項', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'exception_option';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'板材', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'plates';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'趟數', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'runs';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'重量', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'weight';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'片', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'pieces';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'商品類型', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'goods_type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'運送模式', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'ship_mode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區域', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'area';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'拖欠率', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'sowage_rate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貨車號碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'car_number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'月台', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'platform';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'配送站所', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'area_arrive_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'掃描日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'scan_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'掃描項目1.到著2.配送3.4.配達5.集貨6.卸集7.發送', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'scan_item';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貨號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'check_number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'司機代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'driver_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog', @level2type = N'COLUMN', @level2name = N'log_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貨追歷程log', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryScanLog';

