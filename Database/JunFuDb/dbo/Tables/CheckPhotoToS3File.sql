﻿CREATE TABLE [dbo].[CheckPhotoToS3File] (
    [file_id]               INT            IDENTITY (1, 1) NOT NULL,
    [file_name]             NVARCHAR (255) NULL,
    [delivery_request_id]   INT            NULL,
    [folder_name]           NVARCHAR (255) NULL,
    [file_create_date]      DATETIME       NULL,
    [file_latest_edit_time] DATETIME       NULL,
    [is_uploaded]           BIT            NULL,
    [cdate]                 DATETIME       NULL,
    [upload_time]           DATETIME       NULL,
    [s3_path]               NVARCHAR (255) NULL,
    [err_msg]               NVARCHAR (MAX) NULL,
    CONSTRAINT [PK__CheckPho__07D884C6BC7A5BA1] PRIMARY KEY CLUSTERED ([file_id] ASC),
    CONSTRAINT [FK_CheckPhotoToS3File_CheckPhotoToS3File] FOREIGN KEY ([file_id]) REFERENCES [dbo].[CheckPhotoToS3File] ([file_id])
);




GO
CREATE NONCLUSTERED INDEX [IX_file_name]
    ON [dbo].[CheckPhotoToS3File]([file_name] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_is_uploaded]
    ON [dbo].[CheckPhotoToS3File]([is_uploaded] ASC);

