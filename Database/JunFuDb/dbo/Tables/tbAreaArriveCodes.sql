﻿CREATE TABLE [dbo].[tbAreaArriveCodes] (
    [arrive_id]      NUMERIC (18)  IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [supplier_code]  NVARCHAR (3)  NULL,
    [supplier_name]  NVARCHAR (20) NULL,
    [arrive_code]    NVARCHAR (5)  NULL,
    [arrive_address] NVARCHAR (50) NULL,
    [city]           NVARCHAR (10) NULL,
    [area]           NVARCHAR (10) NULL,
    CONSTRAINT [PK_tbAreaArriveCode] PRIMARY KEY CLUSTERED ([arrive_id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'地區', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAreaArriveCodes', @level2type = N'COLUMN', @level2name = N'area';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'城市', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAreaArriveCodes', @level2type = N'COLUMN', @level2name = N'city';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'配達地址', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAreaArriveCodes', @level2type = N'COLUMN', @level2name = N'arrive_address';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'配達代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAreaArriveCodes', @level2type = N'COLUMN', @level2name = N'arrive_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區配商名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAreaArriveCodes', @level2type = N'COLUMN', @level2name = N'supplier_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區配商代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAreaArriveCodes', @level2type = N'COLUMN', @level2name = N'supplier_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'地區配達代碼表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAreaArriveCodes';

