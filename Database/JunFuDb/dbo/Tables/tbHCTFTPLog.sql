﻿CREATE TABLE [dbo].[tbHCTFTPLog] (
    [id]           BIGINT         IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [request_id]   BIGINT         NULL,
    [logdate]      DATETIME       NULL,
    [check_number] NVARCHAR (10)  NULL,
    [message]      NVARCHAR (200) NULL,
    [type]         NVARCHAR (20)  NULL,
    [cuser]        VARCHAR (20)   NULL,
    CONSTRAINT [PK_tbHCTFTPLog] PRIMARY KEY CLUSTERED ([id] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbHCTFTPLog', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'型態', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbHCTFTPLog', @level2type = N'COLUMN', @level2name = N'type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'訊息', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbHCTFTPLog', @level2type = N'COLUMN', @level2name = N'message';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貨號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbHCTFTPLog', @level2type = N'COLUMN', @level2name = N'check_number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbHCTFTPLog', @level2type = N'COLUMN', @level2name = N'logdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'需求ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbHCTFTPLog', @level2type = N'COLUMN', @level2name = N'request_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'新竹物流登入日誌', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbHCTFTPLog';

