﻿CREATE TABLE [dbo].[tbIORecords] (
    [id]            INT             IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [cuser]         NVARCHAR (20)   NOT NULL,
    [cdate]         DATETIME        NULL,
    [uuser]         NVARCHAR (20)   NULL,
    [udate]         DATETIME        NULL,
    [type]          INT             NOT NULL,
    [changeTable]   NVARCHAR (20)   NOT NULL,
    [fromWhere]     NVARCHAR (50)   NULL,
    [randomCode]    NVARCHAR (10)   NULL,
    [memo]          NVARCHAR (100)  NOT NULL,
    [successNum]    INT             NULL,
    [failNum]       INT             NULL,
    [totalNum]      INT             NOT NULL,
    [startTime]     DATETIME        NULL,
    [endTime]       DATETIME        NULL,
    [descript]      NVARCHAR (1000) NULL,
    [result]        BIT             NULL,
    [customer_code] NVARCHAR (20)   NULL,
    [print_date]    DATETIME        NULL,
    [status]        INT             NULL,
    CONSTRAINT [PK_tbIORecords] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'狀態', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbIORecords', @level2type = N'COLUMN', @level2name = N'status';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'列印日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbIORecords', @level2type = N'COLUMN', @level2name = N'print_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'操作人代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbIORecords', @level2type = N'COLUMN', @level2name = N'customer_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'結果', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbIORecords', @level2type = N'COLUMN', @level2name = N'result';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'操作說明', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbIORecords', @level2type = N'COLUMN', @level2name = N'descript';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'操作結束時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbIORecords', @level2type = N'COLUMN', @level2name = N'endTime';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'操作開始時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbIORecords', @level2type = N'COLUMN', @level2name = N'startTime';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'總數量', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbIORecords', @level2type = N'COLUMN', @level2name = N'totalNum';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'操作失敗數', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbIORecords', @level2type = N'COLUMN', @level2name = N'failNum';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'操作成功數', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbIORecords', @level2type = N'COLUMN', @level2name = N'successNum';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'備註', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbIORecords', @level2type = N'COLUMN', @level2name = N'memo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'隨機代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbIORecords', @level2type = N'COLUMN', @level2name = N'randomCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'操作來源', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbIORecords', @level2type = N'COLUMN', @level2name = N'fromWhere';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'修改資料表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbIORecords', @level2type = N'COLUMN', @level2name = N'changeTable';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'匯入類別(定義待定)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbIORecords', @level2type = N'COLUMN', @level2name = N'type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'修改日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbIORecords', @level2type = N'COLUMN', @level2name = N'udate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'修改人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbIORecords', @level2type = N'COLUMN', @level2name = N'uuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建檔日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbIORecords', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'操作人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbIORecords', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'主鍵', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbIORecords', @level2type = N'COLUMN', @level2name = N'id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資料匯入記錄表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbIORecords';

