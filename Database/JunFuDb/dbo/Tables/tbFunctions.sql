﻿CREATE TABLE [dbo].[tbFunctions] (
    [func_id]          NUMERIC (18)  IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [func_code]        VARCHAR (10)  NULL,
    [func_name]        NVARCHAR (30) NULL,
    [upper_level_code] VARCHAR (10)  NULL,
    [Level]            INT           NULL,
    [func_link]        VARCHAR (50)  NULL,
    [cssclass]         NVARCHAR (50) NULL,
    [cuser]            VARCHAR (20)  NULL,
    [cdate]            DATETIME      NULL,
    [uuser]            VARCHAR (20)  NULL,
    [udate]            DATETIME      NULL,
    [sort]             INT           NULL,
    [type]             INT           NULL,
    [visible]          BIT           NULL,
    CONSTRAINT [PK_tbFunctions] PRIMARY KEY CLUSTERED ([func_id] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'型別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctions', @level2type = N'COLUMN', @level2name = N'type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'分類', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctions', @level2type = N'COLUMN', @level2name = N'sort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者使用日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctions', @level2type = N'COLUMN', @level2name = N'udate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctions', @level2type = N'COLUMN', @level2name = N'uuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'廠商使用日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctions', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'廠商', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctions', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'css類別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctions', @level2type = N'COLUMN', @level2name = N'cssclass';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'功能連結', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctions', @level2type = N'COLUMN', @level2name = N'func_link';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'層別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctions', @level2type = N'COLUMN', @level2name = N'Level';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'大寫代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctions', @level2type = N'COLUMN', @level2name = N'upper_level_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'功能名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctions', @level2type = N'COLUMN', @level2name = N'func_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'功能代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctions', @level2type = N'COLUMN', @level2name = N'func_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'功能編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctions', @level2type = N'COLUMN', @level2name = N'func_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'服務功能', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctions';

