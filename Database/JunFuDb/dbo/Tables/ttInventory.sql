﻿CREATE TABLE [dbo].[ttInventory] (
    [Inventory_ID] INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [station_code] NVARCHAR (10) NULL,
    [driver_code]  NVARCHAR (25) NULL,
    [scanning_dt]  DATETIME      NULL,
    [check_number] NVARCHAR (50) NULL,
    [create_dt]    DATETIME      NULL,
    CONSTRAINT [PK_ttInventory] PRIMARY KEY CLUSTERED ([Inventory_ID] ASC)
);

