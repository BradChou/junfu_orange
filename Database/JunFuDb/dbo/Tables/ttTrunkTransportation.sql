﻿CREATE TABLE [dbo].[ttTrunkTransportation] (
    [id]          BIGINT        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [car_license] VARCHAR (10)  NULL,
    [tel]         VARCHAR (10)  NULL,
    [driver_name] NVARCHAR (10) NULL,
    [sdate]       DATETIME      NULL,
    [edate]       DATETIME      NULL,
    [cdate]       DATETIME      NULL,
    CONSTRAINT [PK_ttTrunkTransportation] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建檔時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttTrunkTransportation', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'到達時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttTrunkTransportation', @level2type = N'COLUMN', @level2name = N'edate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'出發時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttTrunkTransportation', @level2type = N'COLUMN', @level2name = N'sdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'司機姓名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttTrunkTransportation', @level2type = N'COLUMN', @level2name = N'driver_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'司機電話', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttTrunkTransportation', @level2type = N'COLUMN', @level2name = N'tel';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車牌', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttTrunkTransportation', @level2type = N'COLUMN', @level2name = N'car_license';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'主鍵', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttTrunkTransportation', @level2type = N'COLUMN', @level2name = N'id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貨車運輸表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttTrunkTransportation';

