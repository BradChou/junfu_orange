﻿CREATE TABLE [dbo].[ttDeliveryDisposeLog] (
    [log_id]       NUMERIC (18)   IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [check_number] NVARCHAR (20)  NULL,
    [status_type]  NVARCHAR (20)  NULL,
    [dispose_desc] NVARCHAR (100) NULL,
    [cdate]        DATETIME       NULL,
    [cuser]        NVARCHAR (10)  NULL,
    CONSTRAINT [PK_ttDeliveryDisposeLog] PRIMARY KEY CLUSTERED ([log_id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryDisposeLog', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryDisposeLog', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'狀態說明', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryDisposeLog', @level2type = N'COLUMN', @level2name = N'dispose_desc';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'狀態：0:以補配達,1:延後配送或電聯中,2:拒收或交貨異常,3:交貨異常,4:自行輸入TEST,5:轉郵,6:貨件遺失,7:退件,8:延遲、誤掃、拒收,9:約期配送,10:查無此人,11:測試
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryDisposeLog', @level2type = N'COLUMN', @level2name = N'status_type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貨號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryDisposeLog', @level2type = N'COLUMN', @level2name = N'check_number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'發送取消日誌', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryDisposeLog';

