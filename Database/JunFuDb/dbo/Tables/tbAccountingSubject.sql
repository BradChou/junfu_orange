﻿CREATE TABLE [dbo].[tbAccountingSubject] (
    [AcntId]   VARCHAR (10)  NOT NULL,
    [AcntName] NVARCHAR (20) NULL,
    CONSTRAINT [PK_tbAccountingSubject] PRIMARY KEY CLUSTERED ([AcntId] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'會計科目', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAccountingSubject', @level2type = N'COLUMN', @level2name = N'AcntName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'會計科目編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAccountingSubject', @level2type = N'COLUMN', @level2name = N'AcntId';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'會計科目', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAccountingSubject';

