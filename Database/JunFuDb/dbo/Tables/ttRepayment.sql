﻿CREATE TABLE [dbo].[ttRepayment] (
    [id]                INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [loan_id]           INT           NULL,
    [type]              INT           NULL,
    [due_date]          DATETIME      NULL,
    [pay_date]          DATETIME      NULL,
    [opening_balance]   INT           NULL,
    [installment_price] INT           NULL,
    [principal]         INT           NULL,
    [interest]          INT           NULL,
    [end_balance]       INT           NULL,
    [total_interest]    INT           NULL,
    [paid]              BIT           NULL,
    [paid_price]        INT           NULL,
    [memo]              NVARCHAR (50) NULL,
    [cuser]             VARCHAR (10)  NULL,
    [cdate]             DATETIME      NULL,
    CONSTRAINT [PK_ttRepayment] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttRepayment', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttRepayment', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'備忘錄', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttRepayment', @level2type = N'COLUMN', @level2name = N'memo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'支付價格', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttRepayment', @level2type = N'COLUMN', @level2name = N'paid_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否已支付', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttRepayment', @level2type = N'COLUMN', @level2name = N'paid';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'利息', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttRepayment', @level2type = N'COLUMN', @level2name = N'total_interest';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'期末餘額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttRepayment', @level2type = N'COLUMN', @level2name = N'end_balance';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'利息', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttRepayment', @level2type = N'COLUMN', @level2name = N'interest';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'本金', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttRepayment', @level2type = N'COLUMN', @level2name = N'principal';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'分期付款金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttRepayment', @level2type = N'COLUMN', @level2name = N'installment_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'期初餘額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttRepayment', @level2type = N'COLUMN', @level2name = N'opening_balance';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'支付日', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttRepayment', @level2type = N'COLUMN', @level2name = N'pay_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'到期日', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttRepayment', @level2type = N'COLUMN', @level2name = N'due_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'類別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttRepayment', @level2type = N'COLUMN', @level2name = N'type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貸款ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttRepayment', @level2type = N'COLUMN', @level2name = N'loan_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'還款餘額表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttRepayment';

