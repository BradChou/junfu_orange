﻿CREATE TABLE [dbo].[tbPriceLessThan_] (
    [id]          BIGINT        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [start_city]  NVARCHAR (10) NULL,
    [end_city]    NVARCHAR (10) NULL,
    [Cbmsize]     INT           NULL,
    [first_price] INT           NULL,
    [add_price]   INT           NULL,
    [business]    NVARCHAR (10) NULL,
    [cdate]       DATETIME      NULL,
    [cuser]       NVARCHAR (20) NULL,
    [Enable_date] DATETIME      NULL,
    CONSTRAINT [PK_tbPriceLessThan] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'啟用時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceLessThan_', @level2type = N'COLUMN', @level2name = N'Enable_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceLessThan_', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceLessThan_', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'買賣別：1, 2, 3', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceLessThan_', @level2type = N'COLUMN', @level2name = N'business';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'加價', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceLessThan_', @level2type = N'COLUMN', @level2name = N'add_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'起始價', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceLessThan_', @level2type = N'COLUMN', @level2name = N'first_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'材積大小', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceLessThan_', @level2type = N'COLUMN', @level2name = N'Cbmsize';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'終點城市', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceLessThan_', @level2type = N'COLUMN', @level2name = N'end_city';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'起始城市', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceLessThan_', @level2type = N'COLUMN', @level2name = N'start_city';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'運貨價格', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceLessThan_';

