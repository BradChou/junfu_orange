﻿CREATE TABLE [dbo].[tbFunctionsAuth] (
    [auth_id]      NUMERIC (18)  IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [manager_type] CHAR (1)      NULL,
    [account_code] VARCHAR (20)  NULL,
    [func_code]    VARCHAR (10)  NULL,
    [view_auth]    BIT           NULL,
    [insert_auth]  BIT           NULL,
    [modify_auth]  BIT           NULL,
    [delete_auth]  BIT           NULL,
    [etc_auth]     BIT           NULL,
    [func_type]    INT           NULL,
    [cuser]        NVARCHAR (20) NULL,
    [cdate]        DATETIME      NULL,
    [uuser]        NVARCHAR (20) NULL,
    [udate]        DATETIME      NULL,
    CONSTRAINT [PK_tbFunctionsAuth] PRIMARY KEY CLUSTERED ([auth_id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'修改日', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctionsAuth', @level2type = N'COLUMN', @level2name = N'udate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'修改人員', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctionsAuth', @level2type = N'COLUMN', @level2name = N'uuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建檔日', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctionsAuth', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建檔人員', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctionsAuth', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'其它權限', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctionsAuth', @level2type = N'COLUMN', @level2name = N'etc_auth';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'刪除權限', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctionsAuth', @level2type = N'COLUMN', @level2name = N'delete_auth';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'修改權限', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctionsAuth', @level2type = N'COLUMN', @level2name = N'modify_auth';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'新增權限', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctionsAuth', @level2type = N'COLUMN', @level2name = N'insert_auth';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'讀取權限', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctionsAuth', @level2type = N'COLUMN', @level2name = N'view_auth';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'功能代碼(參照 tbFunctions)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctionsAuth', @level2type = N'COLUMN', @level2name = N'func_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'帳號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctionsAuth', @level2type = N'COLUMN', @level2name = N'account_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'管理類別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctionsAuth', @level2type = N'COLUMN', @level2name = N'manager_type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'主鍵', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctionsAuth', @level2type = N'COLUMN', @level2name = N'auth_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'權限設定表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbFunctionsAuth';

