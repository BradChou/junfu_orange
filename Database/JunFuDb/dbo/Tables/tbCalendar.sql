﻿CREATE TABLE [dbo].[tbCalendar] (
    [Date]           DATETIME      NOT NULL,
    [Year]           INT           NOT NULL,
    [Quarter]        INT           NOT NULL,
    [Month]          INT           NOT NULL,
    [Week]           INT           NOT NULL,
    [Day]            INT           NOT NULL,
    [DayOfYear]      INT           NOT NULL,
    [Weekday]        INT           NOT NULL,
    [Fiscal_Year]    INT           NOT NULL,
    [Fiscal_Quarter] INT           NOT NULL,
    [Fiscal_Month]   INT           NOT NULL,
    [KindOfDay]      VARCHAR (10)  NOT NULL,
    [Description]    VARCHAR (50)  NULL,
    [uuser]          NVARCHAR (20) NULL,
    [udate]          DATETIME      NULL,
    CONSTRAINT [PK__tbCalend__77387D062FFCE9E1] PRIMARY KEY CLUSTERED ([Date] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日曆(工作日及假日)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCalendar';

