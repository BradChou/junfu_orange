﻿CREATE TABLE [dbo].[tbPriceCSectionA05] (
    [id]           NUMERIC (18)    IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [class_level]  INT             NULL,
    [pricing_code] NVARCHAR (2)    NULL,
    [plate1_price] NUMERIC (18, 2) NULL,
    [plate2_price] NUMERIC (18, 2) NULL,
    [plate3_price] NUMERIC (18, 2) NULL,
    [plate4_price] NUMERIC (18, 2) NULL,
    [plate5_price] NUMERIC (18, 2) NULL,
    [plate6_price] NUMERIC (18, 2) NULL,
    CONSTRAINT [PK_tbPriceCSectionA05] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'板材價錢6', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceCSectionA05', @level2type = N'COLUMN', @level2name = N'plate6_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'板材價錢5', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceCSectionA05', @level2type = N'COLUMN', @level2name = N'plate5_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'板材價錢4', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceCSectionA05', @level2type = N'COLUMN', @level2name = N'plate4_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'板材價錢3', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceCSectionA05', @level2type = N'COLUMN', @level2name = N'plate3_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'板材價錢2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceCSectionA05', @level2type = N'COLUMN', @level2name = N'plate2_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'板材價錢1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceCSectionA05', @level2type = N'COLUMN', @level2name = N'plate1_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'價格代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceCSectionA05', @level2type = N'COLUMN', @level2name = N'pricing_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'層級', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceCSectionA05', @level2type = N'COLUMN', @level2name = N'class_level';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceCSectionA05', @level2type = N'COLUMN', @level2name = N'id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'C部分價錢A05', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceCSectionA05';

