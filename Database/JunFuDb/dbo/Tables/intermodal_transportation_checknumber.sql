﻿CREATE TABLE [dbo].[intermodal_transportation_checknumber] (
    [id]            INT           IDENTITY (1, 1) NOT NULL,
    [company]       VARCHAR (MAX) NULL,
    [customer_code] VARCHAR (100) NULL,
    [number_start]  VARCHAR (100) NULL,
    [number_end]    VARCHAR (100) NULL,
    [number_next]   VARCHAR (100) NULL,
    [cdate]         DATETIME      NULL,
    [udate]         DATETIME      NULL,
    [active_flag]   BIT           NULL,
    [delivery_type] VARCHAR (10)  NULL,
    [priority]      INT           NULL,
    [cuser]         VARCHAR (100) NULL,
    [uuser]         VARCHAR (100) NULL,
    [company_id]    INT           NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);

