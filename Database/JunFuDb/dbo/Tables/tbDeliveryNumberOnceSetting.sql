﻿CREATE TABLE [dbo].[tbDeliveryNumberOnceSetting] (
    [seq]            INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [customer_code]  NVARCHAR (20) NULL,
    [begin_number]   BIGINT        NULL,
    [end_number]     BIGINT        NULL,
    [num]            BIGINT        NULL,
    [cuser]          NVARCHAR (10) NULL,
    [cdate]          DATETIME      NULL,
    [IsActive]       INT           NULL,
    [current_number] BIGINT        NULL,
    CONSTRAINT [PK_tbDeliveryNumberOnceSetting] PRIMARY KEY CLUSTERED ([seq] ASC)
);

