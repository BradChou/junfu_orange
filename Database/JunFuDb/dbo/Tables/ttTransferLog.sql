﻿CREATE TABLE [dbo].[ttTransferLog] (
    [TransferID]   INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [station_code] NVARCHAR (10) NULL,
    [driver_code]  NVARCHAR (25) NULL,
    [scanning_dt]  DATETIME      NULL,
    [check_number] NVARCHAR (50) NULL,
    [create_dt]    DATETIME      NULL,
    [Transfer]     NVARCHAR (20) NULL,
    CONSTRAINT [PK_ttTransferLog] PRIMARY KEY CLUSTERED ([TransferID] ASC)
);

