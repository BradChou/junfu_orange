﻿CREATE TABLE [dbo].[tbItemCodes] (
    [seq]         NUMERIC (18)   IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [code_bclass] CHAR (1)       NULL,
    [code_sclass] VARCHAR (20)   NULL,
    [code_id]     NVARCHAR (20)  NULL,
    [code_name]   NVARCHAR (50)  NULL,
    [active_flag] BIT            NULL,
    [memo]        NVARCHAR (255) NULL,
    CONSTRAINT [PK_tbItemCodes] PRIMARY KEY CLUSTERED ([seq] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'備忘錄', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbItemCodes', @level2type = N'COLUMN', @level2name = N'memo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否活耀', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbItemCodes', @level2type = N'COLUMN', @level2name = N'active_flag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'代碼意義', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbItemCodes', @level2type = N'COLUMN', @level2name = N'code_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'代碼ＩＤ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbItemCodes', @level2type = N'COLUMN', @level2name = N'code_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'等級代碼-小項', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbItemCodes', @level2type = N'COLUMN', @level2name = N'code_sclass';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'等級代碼-大項', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbItemCodes', @level2type = N'COLUMN', @level2name = N'code_bclass';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'項目代碼表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbItemCodes';

