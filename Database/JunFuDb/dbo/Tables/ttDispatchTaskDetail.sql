﻿CREATE TABLE [dbo].[ttDispatchTaskDetail] (
    [id]         INT    IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [t_id]       INT    NULL,
    [request_id] BIGINT NULL,
    CONSTRAINT [PK_ttDispatchTaskDetail] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-request_id]
    ON [dbo].[ttDispatchTaskDetail]([request_id] ASC);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-tid]
    ON [dbo].[ttDispatchTaskDetail]([t_id] ASC)
    INCLUDE([request_id]);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'關連的配送資料序號(關連 tcDeliveryRequests)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTaskDetail', @level2type = N'COLUMN', @level2name = N'request_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'派車任務主檔序號(ttDispatchTask 主鍵)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTaskDetail', @level2type = N'COLUMN', @level2name = N't_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'主鍵', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTaskDetail', @level2type = N'COLUMN', @level2name = N'id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'派車任務明細表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTaskDetail';

