﻿CREATE TABLE [dbo].[cbm_data_log] (
    [id]           INT           IDENTITY (1, 1) NOT NULL,
    [file_name]    VARCHAR (100) NULL,
    [file_row]     INT           NULL,
    [cdate]        DATETIME      NULL,
    [check_number] NVARCHAR (20) NULL,
    [length]       FLOAT (53)    NULL,
    [width]        FLOAT (53)    NULL,
    [height]       FLOAT (53)    NULL,
    [cbm]          FLOAT (53)    NULL,
    [s3_pic_uri]   VARCHAR (400) NULL,
    [scan_time]    DATETIME      NULL,
    [data_source]  VARCHAR (30)  NULL,
    [scan_result]  VARCHAR (30)  NULL,
    [is_log]       BIT           NULL,
    [uuser]        VARCHAR (100) NULL,
    [udate]        DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);

