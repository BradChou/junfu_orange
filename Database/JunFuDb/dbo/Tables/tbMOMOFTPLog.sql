﻿CREATE TABLE [dbo].[tbMOMOFTPLog] (
    [id]           BIGINT        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [logdate]      DATETIME      NULL,
    [check_number] NVARCHAR (20) NULL,
    [type]         NVARCHAR (10) NULL,
    [type_option]  NVARCHAR (10) NULL,
    [cuser]        NVARCHAR (20) NULL,
    CONSTRAINT [PK_tbMOMOFTPLog] PRIMARY KEY CLUSTERED ([id] ASC)
);

