﻿CREATE TABLE [dbo].[ttDeliveryRequestsRecord] (
    [id]                          BIGINT         IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [create_date]                 DATETIME       NULL,
    [create_user]                 NVARCHAR (20)  NULL,
    [modify_date]                 DATETIME       NULL,
    [modify_user]                 NVARCHAR (20)  NULL,
    [delete_date]                 DATETIME       NULL,
    [delete_user]                 NVARCHAR (20)  NULL,
    [request_id]                  NUMERIC (18)   NOT NULL,
    [pricing_type]                NVARCHAR (2)   NULL,
    [customer_code]               NVARCHAR (20)  NULL,
    [check_number]                NVARCHAR (20)  NULL,
    [check_type]                  CHAR (10)      NULL,
    [order_number]                NVARCHAR (20)  NULL,
    [receive_customer_code]       NVARCHAR (20)  NULL,
    [subpoena_category]           NVARCHAR (10)  NULL,
    [receive_tel1]                NVARCHAR (20)  NULL,
    [receive_tel1_ext]            NVARCHAR (10)  NULL,
    [receive_tel2]                NVARCHAR (20)  NULL,
    [receive_contact]             NVARCHAR (20)  NULL,
    [receive_city]                NVARCHAR (10)  NULL,
    [receive_area]                NVARCHAR (10)  NULL,
    [receive_address]             NVARCHAR (50)  NULL,
    [area_arrive_code]            NVARCHAR (3)   NULL,
    [receive_by_arrive_site_flag] BIT            NULL,
    [arrive_address]              NVARCHAR (100) NULL,
    [pieces]                      INT            NULL,
    [plates]                      INT            NULL,
    [cbm]                         INT            NULL,
    [collection_money]            INT            NULL,
    [arrive_to_pay_freight]       INT            NULL,
    [arrive_to_pay_append]        INT            NULL,
    [send_contact]                NVARCHAR (20)  NULL,
    [send_tel]                    NVARCHAR (20)  NULL,
    [send_city]                   NVARCHAR (10)  NULL,
    [send_area]                   NVARCHAR (10)  NULL,
    [send_address]                NVARCHAR (50)  NULL,
    [donate_invoice_flag]         BIT            NULL,
    [electronic_invoice_flag]     BIT            NULL,
    [uniform_numbers]             NVARCHAR (8)   NULL,
    [arrive_mobile]               NVARCHAR (20)  NULL,
    [arrive_email]                NVARCHAR (50)  NULL,
    [invoice_memo]                NVARCHAR (100) NULL,
    [invoice_desc]                NVARCHAR (200) NULL,
    [product_category]            NVARCHAR (10)  NULL,
    [special_send]                NVARCHAR (10)  NULL,
    [arrive_assign_date]          DATETIME       NULL,
    [time_period]                 CHAR (10)      NULL,
    [receipt_flag]                BIT            NULL,
    [pallet_recycling_flag]       BIT            NULL,
    [supplier_code]               NVARCHAR (3)   NULL,
    [supplier_name]               NVARCHAR (20)  NULL,
    [supplier_date]               DATETIME       NULL,
    [receipt_numbe]               INT            NULL,
    [supplier_fee]                INT            NULL,
    [csection_fee]                INT            NULL,
    [remote_fee]                  INT            NULL,
    [total_fee]                   INT            NULL,
    [print_date]                  DATETIME       NULL,
    [print_flag]                  BIT            NULL,
    [checkout_close_date]         DATETIME       NULL,
    [sub_check_number]            VARCHAR (3)    NULL,
    [import_randomCode]           NVARCHAR (10)  NULL,
    [close_randomCode]            NVARCHAR (10)  NULL,
    [record_date]                 DATETIME       NULL,
    [record_action]               CHAR (1)       NULL,
    [record_memo]                 NVARCHAR (50)  NULL,
    CONSTRAINT [PK_ttDeliveryRequestsRecord] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'記錄備註', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'record_memo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'記錄類別(待定義)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'record_action';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'記錄日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'record_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'結案隨機碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'close_randomCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'進站隨機碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'import_randomCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'列印註記', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'print_flag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'列印日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'print_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'總費用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'total_fee';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'供應商費用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'supplier_fee';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'供應商日期(未定義)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'supplier_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'供應商名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'supplier_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'供應商簡碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'supplier_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'棧板回收註記', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'pallet_recycling_flag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收件註記(未定義)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'receipt_flag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'指定配達時段', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'time_period';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'指定配達日', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'arrive_assign_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'產品類別(待定義)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'product_category';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'發票說明', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'invoice_desc';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'發票備註', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'invoice_memo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'到著連絡人email', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'arrive_email';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'到著連絡人手機', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'arrive_mobile';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'電子發票標記', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'electronic_invoice_flag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'捐贈發票標記', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'donate_invoice_flag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'寄件地址', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'send_address';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'寄件區(鄉鎮)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'send_area';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'寄件縣市', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'send_city';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'寄件連絡電話', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'send_tel';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'寄件連絡人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'send_contact';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'到著收取其它費用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'arrive_to_pay_append';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'到著收取運費', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'arrive_to_pay_freight';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收款金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'collection_money';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'板數', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'plates';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'件數', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'pieces';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'到著地址', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'arrive_address';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'到著標記(待定義)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'receive_by_arrive_site_flag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'到著區碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'area_arrive_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收件地址', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'receive_address';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收件區(鄉鎮)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'receive_area';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收件縣市', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'receive_city';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收件連絡人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'receive_contact';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收件人電話2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'receive_tel2';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收件人分機', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'receive_tel1_ext';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收件人電話', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'receive_tel1';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'不明', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'subpoena_category';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收件人客代', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'receive_customer_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'訂單號碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'order_number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'託運類別(號碼待定)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'check_type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'條碼號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'check_number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'客戶編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'customer_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'計價方式(待定)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'pricing_type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'托運序號(ttDeliveryRequest 的主鍵)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'request_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'刪除人員', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'delete_user';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'刪除日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'delete_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'修改人員', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'modify_user';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'修改日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'modify_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建檔人員', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'create_user';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建檔日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'create_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'主鍵', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord', @level2type = N'COLUMN', @level2name = N'id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'托運記錄表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryRequestsRecord';

