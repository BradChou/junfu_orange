﻿CREATE TABLE [dbo].[ttDispatchTask] (
    [t_id]                INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [task_type]           INT            NULL,
    [task_id]             VARCHAR (20)   NULL,
    [task_date]           DATETIME       NULL,
    [serial]              VARCHAR (3)    NULL,
    [supplier]            NVARCHAR (3)   NULL,
    [supplier_no]         NVARCHAR (10)  NULL,
    [task_route]          NVARCHAR (20)  NULL,
    [driver]              NVARCHAR (10)  NULL,
    [car_license]         NVARCHAR (10)  NULL,
    [plates]              INT            NULL,
    [price]               INT            NULL,
    [status]              INT            NULL,
    [checkout_close_date] DATETIME       NULL,
    [close_randomCode]    NVARCHAR (10)  NULL,
    [memo]                NVARCHAR (100) NULL,
    [c_user]              NVARCHAR (10)  NULL,
    [c_time]              DATETIME       NULL,
    CONSTRAINT [PK_ttDispatchTask] PRIMARY KEY CLUSTERED ([t_id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建檔日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTask', @level2type = N'COLUMN', @level2name = N'c_time';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建檔人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTask', @level2type = N'COLUMN', @level2name = N'c_user';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'備註說明', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTask', @level2type = N'COLUMN', @level2name = N'memo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'結案代碼(定義待定)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTask', @level2type = N'COLUMN', @level2name = N'close_randomCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'結案日', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTask', @level2type = N'COLUMN', @level2name = N'checkout_close_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'狀態(定義待定)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTask', @level2type = N'COLUMN', @level2name = N'status';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'價格', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTask', @level2type = N'COLUMN', @level2name = N'price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'板數', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTask', @level2type = N'COLUMN', @level2name = N'plates';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車牌', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTask', @level2type = N'COLUMN', @level2name = N'car_license';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'司機', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTask', @level2type = N'COLUMN', @level2name = N'driver';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'供應商代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTask', @level2type = N'COLUMN', @level2name = N'supplier_no';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'供應商簡碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTask', @level2type = N'COLUMN', @level2name = N'supplier';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'當日的流水號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTask', @level2type = N'COLUMN', @level2name = N'serial';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'派車日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTask', @level2type = N'COLUMN', @level2name = N'task_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'派車代碼(年月日加3碼流水號)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTask', @level2type = N'COLUMN', @level2name = N'task_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'派車類別(待定義)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTask', @level2type = N'COLUMN', @level2name = N'task_type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'派車任務表主鍵', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTask', @level2type = N'COLUMN', @level2name = N't_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'派車任務表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDispatchTask';

