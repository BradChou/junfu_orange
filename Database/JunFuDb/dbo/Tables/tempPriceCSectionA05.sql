﻿CREATE TABLE [dbo].[tempPriceCSectionA05] (
    [id]           NUMERIC (18)    IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [class_level]  INT             NULL,
    [pricing_code] NVARCHAR (2)    NULL,
    [plate1_price] NUMERIC (18, 2) NULL,
    [plate2_price] NUMERIC (18, 2) NULL,
    [plate3_price] NUMERIC (18, 2) NULL,
    [plate4_price] NUMERIC (18, 2) NULL,
    [plate5_price] NUMERIC (18, 2) NULL,
    [plate6_price] NUMERIC (18, 2) NULL,
    CONSTRAINT [PK_tempPriceCSectionA05] PRIMARY KEY CLUSTERED ([id] ASC)
);

