﻿CREATE TABLE [dbo].[tbPriceBusinessDefine] (
    [id]            NUMERIC (18)    IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [start_city]    NVARCHAR (10)   NULL,
    [end_city]      NVARCHAR (10)   NULL,
    [customer_code] NVARCHAR (10)   NULL,
    [active_date]   DATE            NULL,
    [pricing_code]  NVARCHAR (2)    NULL,
    [plate1_price]  NUMERIC (18, 2) NULL,
    [plate2_price]  NUMERIC (18, 2) NULL,
    [plate3_price]  NUMERIC (18, 2) NULL,
    [plate4_price]  NUMERIC (18, 2) NULL,
    [plate5_price]  NUMERIC (18, 2) NULL,
    [plate6_price]  NUMERIC (18, 2) NULL,
    CONSTRAINT [PK_tbPriceBusinessDefine] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'六板價', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceBusinessDefine', @level2type = N'COLUMN', @level2name = N'plate6_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'五板價', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceBusinessDefine', @level2type = N'COLUMN', @level2name = N'plate5_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'四板價', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceBusinessDefine', @level2type = N'COLUMN', @level2name = N'plate4_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'三板價', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceBusinessDefine', @level2type = N'COLUMN', @level2name = N'plate3_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'二板價', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceBusinessDefine', @level2type = N'COLUMN', @level2name = N'plate2_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'一板價', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceBusinessDefine', @level2type = N'COLUMN', @level2name = N'plate1_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'定價代碼(說明待定)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceBusinessDefine', @level2type = N'COLUMN', @level2name = N'pricing_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'運送日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceBusinessDefine', @level2type = N'COLUMN', @level2name = N'active_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'客戶代碼(對應tbCustomers)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceBusinessDefine', @level2type = N'COLUMN', @level2name = N'customer_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'迄止縣市', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceBusinessDefine', @level2type = N'COLUMN', @level2name = N'end_city';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'起始縣市', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceBusinessDefine', @level2type = N'COLUMN', @level2name = N'start_city';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'主鍵', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceBusinessDefine', @level2type = N'COLUMN', @level2name = N'id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'客戶訂價表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceBusinessDefine';

