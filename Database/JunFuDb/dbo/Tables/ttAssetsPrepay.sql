﻿CREATE TABLE [dbo].[ttAssetsPrepay] (
    [p_id]        BIGINT        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [car_license] NVARCHAR (10) NULL,
    [fee_type]    VARCHAR (2)   NULL,
    [prepay]      BIT           NULL,
    CONSTRAINT [PK_ttAssetsPrepay] PRIMARY KEY CLUSTERED ([p_id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否預付', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsPrepay', @level2type = N'COLUMN', @level2name = N'prepay';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'費用類別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsPrepay', @level2type = N'COLUMN', @level2name = N'fee_type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車牌號碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsPrepay', @level2type = N'COLUMN', @level2name = N'car_license';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資產預付編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsPrepay', @level2type = N'COLUMN', @level2name = N'p_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資產預付', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsPrepay';

