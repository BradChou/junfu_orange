﻿CREATE TABLE [dbo].[write_off_check_list] (
    [id]          INT           NOT NULL,
    [information] NVARCHAR (10) NULL,
    [type]        NVARCHAR (5)  NULL,
    CONSTRAINT [PK__write_of__3213E83F88F91858] PRIMARY KEY CLUSTERED ([id] ASC)
);

