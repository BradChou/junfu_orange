﻿CREATE TABLE [dbo].[tcCbmSize] (
    [id]      INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CbmID]   NVARCHAR (10) NULL,
    [CbmSize] NVARCHAR (10) NULL,
    CONSTRAINT [PK_tcCbmSize] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'材積尺寸', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcCbmSize', @level2type = N'COLUMN', @level2name = N'CbmSize';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'材積編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcCbmSize', @level2type = N'COLUMN', @level2name = N'CbmID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcCbmSize', @level2type = N'COLUMN', @level2name = N'id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'材積尺寸', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcCbmSize';

