﻿CREATE TABLE [dbo].[ttInsurance] (
    [id]              INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [form_id]         NVARCHAR (20) NULL,
    [assets_id]       BIGINT        NULL,
    [Insurance_name]  NVARCHAR (50) NULL,
    [company]         NVARCHAR (20) NULL,
    [kind]            NVARCHAR (10) NULL,
    [sdtate]          DATETIME      NULL,
    [edate]           DATETIME      NULL,
    [original_price]  INT           NULL,
    [price]           INT           NULL,
    [amount_paid]     INT           NULL,
    [apportion]       INT           NULL,
    [apportion_price] INT           NULL,
    [memo]            NVARCHAR (50) NULL,
    [cuser]           NVARCHAR (20) NULL,
    [cdate]           DATETIME      NULL,
    [uuser]           NVARCHAR (20) NULL,
    [udate]           DATETIME      NULL,
    CONSTRAINT [PK_ttInsurance] PRIMARY KEY CLUSTERED ([id] ASC)
);

