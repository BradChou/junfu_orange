﻿CREATE TABLE [dbo].[CheckNumberPackageFailItems] (
    [id]                      INT           IDENTITY (1, 1) NOT NULL,
    [relative_request_id]     INT           NULL,
    [check_number]            VARCHAR (30)  NULL,
    [cdate]                   DATETIME      NULL,
    [expected_path]           VARCHAR (MAX) NULL,
    [account_code]            VARCHAR (100) NULL,
    [is_fail_in_copy_process] BIT           NULL,
    [is_not_exists]           BIT           NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);



