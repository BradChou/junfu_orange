﻿CREATE TABLE [dbo].[ttArriveSitesScattered__] (
    [seq]               NUMERIC (18)  IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [post_city]         NVARCHAR (10) NULL,
    [post_area]         NVARCHAR (10) NULL,
    [zip]               NVARCHAR (3)  NULL,
    [supplier_code]     NVARCHAR (3)  NULL,
    [area_arrive_code_] NVARCHAR (3)  NULL,
    [station_code]      NVARCHAR (10) NULL,
    [area_arrive_code]  NVARCHAR (10) NULL,
    CONSTRAINT [PK_ttArriveSitesScattered] PRIMARY KEY CLUSTERED ([seq] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'地區配達代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttArriveSitesScattered__', @level2type = N'COLUMN', @level2name = N'area_arrive_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'站代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttArriveSitesScattered__', @level2type = N'COLUMN', @level2name = N'station_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'地區配達代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttArriveSitesScattered__', @level2type = N'COLUMN', @level2name = N'area_arrive_code_';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區配商代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttArriveSitesScattered__', @level2type = N'COLUMN', @level2name = N'supplier_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'郵遞區號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttArriveSitesScattered__', @level2type = N'COLUMN', @level2name = N'zip';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'郵寄地區', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttArriveSitesScattered__', @level2type = N'COLUMN', @level2name = N'post_area';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'郵寄城市', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttArriveSitesScattered__', @level2type = N'COLUMN', @level2name = N'post_city';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'配達站分散_', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttArriveSitesScattered__';

