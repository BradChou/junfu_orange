﻿CREATE TABLE [dbo].[ttPersonCorrespond] (
    [id]           INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Account_Code] NVARCHAR (20)  NULL,
    [Kinds]        BIGINT         NULL,
    [Str]          NVARCHAR (MAX) NULL,
    [cuser]        NVARCHAR (20)  NULL,
    [cdate]        DATETIME       NULL,
    [uuser]        NVARCHAR (20)  NULL,
    [udate]        DATETIME       NULL,
    CONSTRAINT [PK_ttPersonCorrespond] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPersonCorrespond', @level2type = N'COLUMN', @level2name = N'udate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPersonCorrespond', @level2type = N'COLUMN', @level2name = N'uuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'公司使用者日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPersonCorrespond', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'公司使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPersonCorrespond', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'字串', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPersonCorrespond', @level2type = N'COLUMN', @level2name = N'Str';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'種類', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPersonCorrespond', @level2type = N'COLUMN', @level2name = N'Kinds';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPersonCorrespond', @level2type = N'COLUMN', @level2name = N'Account_Code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPersonCorrespond', @level2type = N'COLUMN', @level2name = N'id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者對照表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPersonCorrespond';

