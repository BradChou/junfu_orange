﻿CREATE TABLE [dbo].[tbPriceSupplier] (
    [id]           NUMERIC (18)    IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [start_city]   NVARCHAR (10)   NULL,
    [end_city]     NVARCHAR (10)   NULL,
    [class_level]  INT             NULL,
    [pricing_code] NVARCHAR (2)    NULL,
    [plate1_price] NUMERIC (18, 2) NULL,
    [plate2_price] NUMERIC (18, 2) NULL,
    [plate3_price] NUMERIC (18, 2) NULL,
    [plate4_price] NUMERIC (18, 2) NULL,
    [plate5_price] NUMERIC (18, 2) NULL,
    [plate6_price] NUMERIC (18, 2) NULL,
    CONSTRAINT [PK_tbPriceSupplier] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'六板價格', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceSupplier', @level2type = N'COLUMN', @level2name = N'plate6_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'五板價格', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceSupplier', @level2type = N'COLUMN', @level2name = N'plate5_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'四板價格', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceSupplier', @level2type = N'COLUMN', @level2name = N'plate4_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'三板價格', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceSupplier', @level2type = N'COLUMN', @level2name = N'plate3_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'二板價格', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceSupplier', @level2type = N'COLUMN', @level2name = N'plate2_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'一板價格', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceSupplier', @level2type = N'COLUMN', @level2name = N'plate1_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'價格代號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceSupplier', @level2type = N'COLUMN', @level2name = N'pricing_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'等級', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceSupplier', @level2type = N'COLUMN', @level2name = N'class_level';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'終點城市', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceSupplier', @level2type = N'COLUMN', @level2name = N'end_city';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'起始城市', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceSupplier', @level2type = N'COLUMN', @level2name = N'start_city';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceSupplier', @level2type = N'COLUMN', @level2name = N'id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區配商價格表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbPriceSupplier';

