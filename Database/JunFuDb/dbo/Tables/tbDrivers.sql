﻿CREATE TABLE [dbo].[tbDrivers] (
    [driver_id]       NUMERIC (18)   IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [driver_code]     NVARCHAR (20)  NULL,
    [driver_name]     NVARCHAR (20)  NULL,
    [driver_mobile]   NVARCHAR (20)  NULL,
    [supplier_code]   NVARCHAR (3)   NULL,
    [emp_code]        NVARCHAR (20)  NULL,
    [site_code]       NVARCHAR (10)  NULL,
    [area]            NVARCHAR (10)  NULL,
    [login_password]  NVARCHAR (50)  NULL,
    [login_date]      DATETIME       NULL,
    [active_flag]     BIT            NULL,
    [external_driver] BIT            NULL,
    [push_id]         NVARCHAR (100) NULL,
    [wgs_x]           VARCHAR (10)   NULL,
    [wgs_y]           VARCHAR (9)    NULL,
    [station]         VARCHAR (10)   NULL,
    [cdate]           DATETIME       NULL,
    [cuser]           NVARCHAR (20)  NULL,
    [udate]           DATETIME       NULL,
    [uuser]           NVARCHAR (20)  NULL,
    [office]          NVARCHAR (20)  NULL,
    CONSTRAINT [PK_tbDrivers] PRIMARY KEY CLUSTERED ([driver_id] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辦公室', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDrivers', @level2type = N'COLUMN', @level2name = N'office';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDrivers', @level2type = N'COLUMN', @level2name = N'uuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDrivers', @level2type = N'COLUMN', @level2name = N'udate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDrivers', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDrivers', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'站所', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDrivers', @level2type = N'COLUMN', @level2name = N'station';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'WGS座標–Y', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDrivers', @level2type = N'COLUMN', @level2name = N'wgs_y';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'WGS座標–Ｘ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDrivers', @level2type = N'COLUMN', @level2name = N'wgs_x';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'全為NULL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDrivers', @level2type = N'COLUMN', @level2name = N'push_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否為外部司機', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDrivers', @level2type = N'COLUMN', @level2name = N'external_driver';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否活動', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDrivers', @level2type = N'COLUMN', @level2name = N'active_flag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'登入日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDrivers', @level2type = N'COLUMN', @level2name = N'login_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'登入密碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDrivers', @level2type = N'COLUMN', @level2name = N'login_password';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'地區', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDrivers', @level2type = N'COLUMN', @level2name = N'area';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'所在地代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDrivers', @level2type = N'COLUMN', @level2name = N'site_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'EMP代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDrivers', @level2type = N'COLUMN', @level2name = N'emp_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'配商代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDrivers', @level2type = N'COLUMN', @level2name = N'supplier_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'手機', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDrivers', @level2type = N'COLUMN', @level2name = N'driver_mobile';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'司機姓名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDrivers', @level2type = N'COLUMN', @level2name = N'driver_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'司機代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDrivers', @level2type = N'COLUMN', @level2name = N'driver_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'司機總表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbDrivers';


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-driver_code]
    ON [dbo].[tbDrivers]([driver_code] ASC)
    INCLUDE([supplier_code], [station]);

