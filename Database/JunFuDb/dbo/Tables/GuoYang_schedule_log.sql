﻿CREATE TABLE [dbo].[GuoYang_schedule_log] (
    [id]         INT            IDENTITY (1, 1) NOT NULL,
    [cdate]      DATETIME       NULL,
    [is_success] BIT            NULL,
    [start_time] DATETIME       NULL,
    [end_time]   DATETIME       NULL,
    [step]       INT            NULL,
    [remark]     NVARCHAR (400) NULL,
    [file_name]  NVARCHAR (400) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);

