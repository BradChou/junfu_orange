﻿create procedure [sp_MSins_dbotempPriceBusiness]
    @c1 numeric(18,0),
    @c2 nvarchar(10),
    @c3 nvarchar(10),
    @c4 nvarchar(2),
    @c5 numeric(18,2),
    @c6 numeric(18,2),
    @c7 numeric(18,2),
    @c8 numeric(18,2),
    @c9 numeric(18,2),
    @c10 numeric(18,2)
as
begin  
	insert into [dbo].[tempPriceBusiness] (
		[id],
		[start_city],
		[end_city],
		[pricing_code],
		[plate1_price],
		[plate2_price],
		[plate3_price],
		[plate4_price],
		[plate5_price],
		[plate6_price]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10	) 
end