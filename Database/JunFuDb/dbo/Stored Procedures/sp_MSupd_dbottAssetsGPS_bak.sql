﻿create procedure [sp_MSupd_dbottAssetsGPS_bak]
		@c1 int = NULL,
		@c2 int = NULL,
		@c3 varchar(2) = NULL,
		@c4 datetime = NULL,
		@c5 datetime = NULL,
		@pkc1 int = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttAssetsGPS_bak] set
		[a_id] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [a_id] end,
		[GPS] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [GPS] end,
		[active_time] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [active_time] end,
		[cdate] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [cdate] end
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttAssetsGPS_bak]', @param2=@primarykey_text, @param3=13233
		End
end