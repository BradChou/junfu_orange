﻿create procedure [sp_MSins_dbowrite_off_check_list]
    @c1 int,
    @c2 nvarchar(10),
    @c3 nvarchar(5)
as
begin  
	insert into [dbo].[write_off_check_list] (
		[id],
		[information],
		[type]
	) values (
		@c1,
		@c2,
		@c3	) 
end