﻿create procedure [sp_MSins_dbotcDeliveryRequestsHistory]
    @c1 numeric(18,0),
    @c2 nvarchar(2),
    @c3 nvarchar(20),
    @c4 nvarchar(20),
    @c5 char(10),
    @c6 nvarchar(20),
    @c7 nvarchar(20),
    @c8 nvarchar(10),
    @c9 nvarchar(20),
    @c10 nvarchar(10),
    @c11 nvarchar(20),
    @c12 nvarchar(20),
    @c13 nvarchar(10),
    @c14 nvarchar(10),
    @c15 nvarchar(50),
    @c16 nvarchar(3),
    @c17 bit,
    @c18 nvarchar(100),
    @c19 int,
    @c20 int,
    @c21 int,
    @c22 int,
    @c23 int,
    @c24 int,
    @c25 nvarchar(20),
    @c26 nvarchar(20),
    @c27 nvarchar(10),
    @c28 nvarchar(10),
    @c29 nvarchar(50),
    @c30 bit,
    @c31 bit,
    @c32 nvarchar(8),
    @c33 nvarchar(20),
    @c34 nvarchar(50),
    @c35 nvarchar(100),
    @c36 nvarchar(200),
    @c37 nvarchar(10),
    @c38 nvarchar(10),
    @c39 datetime,
    @c40 char(10),
    @c41 bit,
    @c42 bit,
    @c43 nvarchar(20),
    @c44 datetime,
    @c45 nvarchar(20),
    @c46 datetime,
    @c47 nvarchar(3),
    @c48 nvarchar(20),
    @c49 datetime,
    @c50 int,
    @c51 int,
    @c52 int,
    @c53 int,
    @c54 int,
    @c55 datetime,
    @c56 bit,
    @c57 datetime
as
begin  
	insert into [dbo].[tcDeliveryRequestsHistory] (
		[request_id],
		[pricing_type],
		[customer_code],
		[check_number],
		[check_type],
		[order_number],
		[receive_customer_code],
		[subpoena_category],
		[receive_tel1],
		[receive_tel1_ext],
		[receive_tel2],
		[receive_contact],
		[receive_city],
		[receive_area],
		[receive_address],
		[area_arrive_code],
		[receive_by_arrive_site_flag],
		[arrive_address],
		[pieces],
		[plates],
		[cbm],
		[collection_money],
		[arrive_to_pay_freight],
		[arrive_to_pay_append],
		[send_contact],
		[send_tel],
		[send_city],
		[send_area],
		[send_address],
		[donate_invoice_flag],
		[electronic_invoice_flag],
		[uniform_numbers],
		[arrive_mobile],
		[arrive_email],
		[invoice_memo],
		[invoice_desc],
		[product_category],
		[special_send],
		[arrive_assign_date],
		[time_period],
		[receipt_flag],
		[pallet_recycling_flag],
		[cuser],
		[cdate],
		[uuser],
		[udate],
		[supplier_code],
		[supplier_name],
		[supplier_date],
		[receipt_numbe],
		[supplier_fee],
		[csection_fee],
		[remote_fee],
		[total_fee],
		[print_date],
		[print_flag],
		[checkout_close_date]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12,
		@c13,
		@c14,
		@c15,
		@c16,
		@c17,
		@c18,
		@c19,
		@c20,
		@c21,
		@c22,
		@c23,
		@c24,
		@c25,
		@c26,
		@c27,
		@c28,
		@c29,
		@c30,
		@c31,
		@c32,
		@c33,
		@c34,
		@c35,
		@c36,
		@c37,
		@c38,
		@c39,
		@c40,
		@c41,
		@c42,
		@c43,
		@c44,
		@c45,
		@c46,
		@c47,
		@c48,
		@c49,
		@c50,
		@c51,
		@c52,
		@c53,
		@c54,
		@c55,
		@c56,
		@c57	) 
end