﻿create procedure [sp_MSins_dbottDispatchTaskDetail_B]
    @c1 int,
    @c2 int,
    @c3 datetime,
    @c4 nvarchar(10),
    @c5 nvarchar(10),
    @c6 int
as
begin  
	insert into [dbo].[ttDispatchTaskDetail_B] (
		[id],
		[t_id],
		[date],
		[warehouse],
		[supplier],
		[plates]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6	) 
end