﻿create procedure [sp_MSupd_dbottAssetsDeptChange]
		@c1 int = NULL,
		@c2 bigint = NULL,
		@c3 nvarchar(10) = NULL,
		@c4 nvarchar(50) = NULL,
		@c5 nvarchar(50) = NULL,
		@c6 nvarchar(50) = NULL,
		@c7 nvarchar(20) = NULL,
		@c8 datetime = NULL,
		@c9 nvarchar(20) = NULL,
		@c10 datetime = NULL,
		@pkc1 int = NULL,
		@bitmap binary(2)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttAssetsDeptChange] set
		[a_id] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [a_id] end,
		[car_license] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [car_license] end,
		[dept] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [dept] end,
		[NewDept] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [NewDept] end,
		[memo] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [memo] end,
		[cuser] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [cuser] end,
		[cdate] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [cdate] end,
		[uuser] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [uuser] end,
		[udate] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [udate] end
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttAssetsDeptChange]', @param2=@primarykey_text, @param3=13233
		End
end