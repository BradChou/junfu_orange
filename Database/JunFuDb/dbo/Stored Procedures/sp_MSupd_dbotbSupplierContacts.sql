﻿create procedure [sp_MSupd_dbotbSupplierContacts]
		@c1 numeric(18,0) = NULL,
		@c2 nvarchar(3) = NULL,
		@c3 nvarchar(20) = NULL,
		@c4 nvarchar(50) = NULL,
		@pkc1 numeric(18,0) = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[tbSupplierContacts] set
		[supplier_code] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [supplier_code] end,
		[contact_name] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [contact_name] end,
		[contact_email] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [contact_email] end
	where [contact_id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[contact_id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbSupplierContacts]', @param2=@primarykey_text, @param3=13233
		End
end