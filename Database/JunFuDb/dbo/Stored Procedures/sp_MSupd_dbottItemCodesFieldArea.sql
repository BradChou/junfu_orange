﻿create procedure [sp_MSupd_dbottItemCodesFieldArea]
		@c1 numeric(18,0) = NULL,
		@c2 nvarchar(20) = NULL,
		@c3 nvarchar(50) = NULL,
		@c4 bit = NULL,
		@c5 nvarchar(255) = NULL,
		@c6 nvarchar(20) = NULL,
		@c7 datetime = NULL,
		@c8 nvarchar(20) = NULL,
		@c9 datetime = NULL,
		@pkc1 numeric(18,0) = NULL,
		@bitmap binary(2)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttItemCodesFieldArea] set
		[Account_Code] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [Account_Code] end,
		[code_name] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [code_name] end,
		[active_flag] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [active_flag] end,
		[memo] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [memo] end,
		[cuser] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [cuser] end,
		[cdate] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [cdate] end,
		[uuser] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [uuser] end,
		[udate] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [udate] end
	where [seq] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[seq] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttItemCodesFieldArea]', @param2=@primarykey_text, @param3=13233
		End
end