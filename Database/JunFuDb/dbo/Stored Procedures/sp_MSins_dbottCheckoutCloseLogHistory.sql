﻿create procedure [sp_MSins_dbottCheckoutCloseLogHistory]
    @c1 numeric(18,0),
    @c2 varchar(10),
    @c3 varchar(3),
    @c4 char(1),
    @c5 date,
    @c6 date,
    @c7 char(1),
    @c8 date,
    @c9 nvarchar(10)
as
begin  
	insert into [dbo].[ttCheckoutCloseLogHistory] (
		[log_id],
		[customer_code],
		[supplier_code],
		[customer_type],
		[close_begdate],
		[close_enddate],
		[close_type],
		[cdate],
		[cuser]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9	) 
end