﻿create procedure [sp_MSins_dbotbAssetsAccountShare]
    @c1 bigint,
    @c2 int,
    @c3 nvarchar(10),
    @c4 varchar(2),
    @c5 int,
    @c6 datetime,
    @c7 varchar(10)
as
begin  
	insert into [dbo].[tbAssetsAccountShare] (
		[id],
		[a_id],
		[car_license],
		[account_dept],
		[share],
		[udate],
		[uuser]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7	) 
end