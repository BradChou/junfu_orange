﻿create procedure [sp_MSupd_dbowrite_off]
		@c1 varchar(20) = NULL,
		@c2 int = NULL,
		@c3 datetime = NULL,
		@c4 nvarchar(5) = NULL,
		@c5 datetime = NULL,
		@pkc1 varchar(20) = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
if (substring(@bitmap,1,1) & 1 = 1)
begin 
update [dbo].[write_off] set
		[check_number] = case substring(@bitmap,1,1) & 1 when 1 then @c1 else [check_number] end,
		[write_off_id] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [write_off_id] end,
		[udate] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [udate] end,
		[write_off_type] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [write_off_type] end,
		[cdate] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [cdate] end
	where [check_number] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[check_number] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[write_off]', @param2=@primarykey_text, @param3=13233
		End
end  
else
begin 
update [dbo].[write_off] set
		[write_off_id] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [write_off_id] end,
		[udate] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [udate] end,
		[write_off_type] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [write_off_type] end,
		[cdate] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [cdate] end
	where [check_number] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[check_number] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[write_off]', @param2=@primarykey_text, @param3=13233
		End
end 
end