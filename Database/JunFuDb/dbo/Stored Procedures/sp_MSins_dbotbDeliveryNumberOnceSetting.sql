﻿create procedure [sp_MSins_dbotbDeliveryNumberOnceSetting]
    @c1 int,
    @c2 nvarchar(20),
    @c3 bigint,
    @c4 bigint,
    @c5 bigint,
    @c6 nvarchar(10),
    @c7 datetime,
    @c8 int,
    @c9 bigint
as
begin  
	insert into [dbo].[tbDeliveryNumberOnceSetting] (
		[seq],
		[customer_code],
		[begin_number],
		[end_number],
		[num],
		[cuser],
		[cdate],
		[IsActive],
		[current_number]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9	) 
end