﻿create procedure [sp_MSupd_dbotbAccounts]
		@c1 numeric(18,0) = NULL,
		@c2 nvarchar(20) = NULL,
		@c3 nvarchar(50) = NULL,
		@c4 bit = NULL,
		@c5 nvarchar(3) = NULL,
		@c6 nvarchar(4) = NULL,
		@c7 nvarchar(7) = NULL,
		@c8 char(1) = NULL,
		@c9 nvarchar(20) = NULL,
		@c10 nvarchar(20) = NULL,
		@c11 nvarchar(20) = NULL,
		@c12 nvarchar(20) = NULL,
		@c13 nvarchar(50) = NULL,
		@c14 nvarchar(20) = NULL,
		@c15 datetime = NULL,
		@c16 nvarchar(20) = NULL,
		@c17 datetime = NULL,
		@c18 bit = NULL,
		@c19 nvarchar(20) = NULL,
		@pkc1 numeric(18,0) = NULL,
		@bitmap binary(3)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[tbAccounts] set
		[account_code] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [account_code] end,
		[password] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [password] end,
		[active_flag] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [active_flag] end,
		[head_code] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [head_code] end,
		[body_code] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [body_code] end,
		[master_code] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [master_code] end,
		[manager_type] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [manager_type] end,
		[manager_unit] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [manager_unit] end,
		[job_title] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [job_title] end,
		[emp_code] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [emp_code] end,
		[user_name] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [user_name] end,
		[user_email] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [user_email] end,
		[cuser] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [cuser] end,
		[cdate] = case substring(@bitmap,2,1) & 64 when 64 then @c15 else [cdate] end,
		[uuser] = case substring(@bitmap,2,1) & 128 when 128 then @c16 else [uuser] end,
		[udate] = case substring(@bitmap,3,1) & 1 when 1 then @c17 else [udate] end,
		[account_type] = case substring(@bitmap,3,1) & 2 when 2 then @c18 else [account_type] end,
		[customer_code] = case substring(@bitmap,3,1) & 4 when 4 then @c19 else [customer_code] end
	where [account_id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[account_id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbAccounts]', @param2=@primarykey_text, @param3=13233
		End
end