﻿create procedure [sp_MSins_dbottReturnDeliveryPickupLog]
    @c1 bigint,
    @c2 nvarchar(20),
    @c3 varchar(2),
    @c4 datetime,
    @c5 varchar(20),
    @c6 nvarchar(50),
    @c7 nvarchar(50),
    @c8 datetime,
    @c9 datetime,
    @c10 varchar(2)
as
begin  
	insert into [dbo].[ttReturnDeliveryPickupLog] (
		[seq],
		[check_number],
		[scan_item],
		[scan_date],
		[driver_code],
		[sign_form_image],
		[sign_field_image],
		[next_pickup_time],
		[cdate],
		[re_code]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10	) 
end