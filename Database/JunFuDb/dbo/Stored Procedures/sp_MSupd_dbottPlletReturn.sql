﻿create procedure [sp_MSupd_dbottPlletReturn]
		@c1 int = NULL,
		@c2 bigint = NULL,
		@c3 int = NULL,
		@c4 datetime = NULL,
		@c5 bigint = NULL,
		@c6 datetime = NULL,
		@c7 nvarchar(10) = NULL,
		@pkc1 int = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttPlletReturn] set
		[request_id] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [request_id] end,
		[return_cnt] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [return_cnt] end,
		[return_date] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [return_date] end,
		[return_request_id] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [return_request_id] end,
		[udate] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [udate] end,
		[uuser] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [uuser] end
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttPlletReturn]', @param2=@primarykey_text, @param3=13233
		End
end