﻿create procedure [sp_MSins_dbotbCalendar]
    @c1 datetime,
    @c2 int,
    @c3 int,
    @c4 int,
    @c5 int,
    @c6 int,
    @c7 int,
    @c8 int,
    @c9 int,
    @c10 int,
    @c11 int,
    @c12 varchar(10),
    @c13 varchar(50),
    @c14 nvarchar(20),
    @c15 datetime
as
begin  
	insert into [dbo].[tbCalendar] (
		[Date],
		[Year],
		[Quarter],
		[Month],
		[Week],
		[Day],
		[DayOfYear],
		[Weekday],
		[Fiscal_Year],
		[Fiscal_Quarter],
		[Fiscal_Month],
		[KindOfDay],
		[Description],
		[uuser],
		[udate]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12,
		@c13,
		@c14,
		@c15	) 
end