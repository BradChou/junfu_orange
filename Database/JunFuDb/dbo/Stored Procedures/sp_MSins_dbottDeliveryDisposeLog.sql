﻿create procedure [sp_MSins_dbottDeliveryDisposeLog]
    @c1 numeric(18,0),
    @c2 nvarchar(20),
    @c3 nvarchar(20),
    @c4 nvarchar(100),
    @c5 datetime,
    @c6 nvarchar(10)
as
begin  
	insert into [dbo].[ttDeliveryDisposeLog] (
		[log_id],
		[check_number],
		[status_type],
		[dispose_desc],
		[cdate],
		[cuser]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6	) 
end