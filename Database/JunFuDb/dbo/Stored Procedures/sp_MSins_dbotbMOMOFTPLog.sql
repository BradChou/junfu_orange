﻿create procedure [sp_MSins_dbotbMOMOFTPLog]
    @c1 bigint,
    @c2 datetime,
    @c3 nvarchar(20),
    @c4 nvarchar(10),
    @c5 nvarchar(10),
    @c6 nvarchar(20)
as
begin  
	insert into [dbo].[tbMOMOFTPLog] (
		[id],
		[logdate],
		[check_number],
		[type],
		[type_option],
		[cuser]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6	) 
end