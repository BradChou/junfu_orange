﻿create procedure [sp_MSins_dbottArriveSites]
    @c1 numeric(18,0),
    @c2 nvarchar(10),
    @c3 nvarchar(10),
    @c4 nvarchar(3),
    @c5 nvarchar(3)
as
begin  
	insert into [dbo].[ttArriveSites] (
		[seq],
		[post_city],
		[post_area],
		[zip],
		[supplier_code]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5	) 
end