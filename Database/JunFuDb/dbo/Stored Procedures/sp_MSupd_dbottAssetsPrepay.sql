﻿create procedure [sp_MSupd_dbottAssetsPrepay]
		@c1 bigint = NULL,
		@c2 nvarchar(10) = NULL,
		@c3 varchar(2) = NULL,
		@c4 bit = NULL,
		@pkc1 bigint = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttAssetsPrepay] set
		[car_license] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [car_license] end,
		[fee_type] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [fee_type] end,
		[prepay] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [prepay] end
	where [p_id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[p_id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttAssetsPrepay]', @param2=@primarykey_text, @param3=13233
		End
end