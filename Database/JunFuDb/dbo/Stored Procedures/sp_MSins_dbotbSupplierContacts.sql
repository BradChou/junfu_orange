﻿create procedure [sp_MSins_dbotbSupplierContacts]
    @c1 numeric(18,0),
    @c2 nvarchar(3),
    @c3 nvarchar(20),
    @c4 nvarchar(50)
as
begin  
	insert into [dbo].[tbSupplierContacts] (
		[contact_id],
		[supplier_code],
		[contact_name],
		[contact_email]
	) values (
		@c1,
		@c2,
		@c3,
		@c4	) 
end