﻿create procedure [sp_MSupd_dbotbDrivers]
		@c1 numeric(18,0) = NULL,
		@c2 nvarchar(20) = NULL,
		@c3 nvarchar(20) = NULL,
		@c4 nvarchar(20) = NULL,
		@c5 nvarchar(3) = NULL,
		@c6 nvarchar(20) = NULL,
		@c7 nvarchar(10) = NULL,
		@c8 nvarchar(10) = NULL,
		@c9 nvarchar(50) = NULL,
		@c10 datetime = NULL,
		@c11 bit = NULL,
		@c12 bit = NULL,
		@c13 nvarchar(100) = NULL,
		@c14 varchar(10) = NULL,
		@c15 varchar(9) = NULL,
		@c16 varchar(10) = NULL,
		@c17 datetime = NULL,
		@c18 nvarchar(20) = NULL,
		@c19 datetime = NULL,
		@c20 nvarchar(20) = NULL,
		@c21 nvarchar(20) = NULL,
		@pkc1 numeric(18,0) = NULL,
		@bitmap binary(3)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[tbDrivers] set
		[driver_code] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [driver_code] end,
		[driver_name] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [driver_name] end,
		[driver_mobile] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [driver_mobile] end,
		[supplier_code] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [supplier_code] end,
		[emp_code] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [emp_code] end,
		[site_code] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [site_code] end,
		[area] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [area] end,
		[login_password] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [login_password] end,
		[login_date] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [login_date] end,
		[active_flag] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [active_flag] end,
		[external_driver] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [external_driver] end,
		[push_id] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [push_id] end,
		[wgs_x] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [wgs_x] end,
		[wgs_y] = case substring(@bitmap,2,1) & 64 when 64 then @c15 else [wgs_y] end,
		[station] = case substring(@bitmap,2,1) & 128 when 128 then @c16 else [station] end,
		[cdate] = case substring(@bitmap,3,1) & 1 when 1 then @c17 else [cdate] end,
		[cuser] = case substring(@bitmap,3,1) & 2 when 2 then @c18 else [cuser] end,
		[udate] = case substring(@bitmap,3,1) & 4 when 4 then @c19 else [udate] end,
		[uuser] = case substring(@bitmap,3,1) & 8 when 8 then @c20 else [uuser] end,
		[office] = case substring(@bitmap,3,1) & 16 when 16 then @c21 else [office] end
	where [driver_id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[driver_id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbDrivers]', @param2=@primarykey_text, @param3=13233
		End
end