﻿create procedure [sp_MSupd_dbottDeliveryScanLog]
		@c1 numeric(18,0) = NULL,
		@c2 nvarchar(10) = NULL,
		@c3 nvarchar(20) = NULL,
		@c4 varchar(2) = NULL,
		@c5 datetime = NULL,
		@c6 nvarchar(10) = NULL,
		@c7 nvarchar(20) = NULL,
		@c8 nvarchar(20) = NULL,
		@c9 nvarchar(20) = NULL,
		@c10 nvarchar(20) = NULL,
		@c11 nvarchar(2) = NULL,
		@c12 nvarchar(2) = NULL,
		@c13 int = NULL,
		@c14 float = NULL,
		@c15 int = NULL,
		@c16 int = NULL,
		@c17 nvarchar(20) = NULL,
		@c18 nvarchar(20) = NULL,
		@c19 nvarchar(50) = NULL,
		@c20 nvarchar(50) = NULL,
		@c21 datetime = NULL,
		@c22 datetime = NULL,
		@c23 datetime = NULL,
		@c24 nvarchar(10) = NULL,
		@c25 nvarchar(20) = NULL,
		@c26 int = NULL,
		@c27 int = NULL,
		@c28 int = NULL,
		@pkc1 numeric(18,0) = NULL,
		@bitmap binary(4)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttDeliveryScanLog] set
		[driver_code] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [driver_code] end,
		[check_number] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [check_number] end,
		[scan_item] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [scan_item] end,
		[scan_date] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [scan_date] end,
		[area_arrive_code] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [area_arrive_code] end,
		[platform] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [platform] end,
		[car_number] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [car_number] end,
		[sowage_rate] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [sowage_rate] end,
		[area] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [area] end,
		[ship_mode] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [ship_mode] end,
		[goods_type] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [goods_type] end,
		[pieces] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [pieces] end,
		[weight] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [weight] end,
		[runs] = case substring(@bitmap,2,1) & 64 when 64 then @c15 else [runs] end,
		[plates] = case substring(@bitmap,2,1) & 128 when 128 then @c16 else [plates] end,
		[exception_option] = case substring(@bitmap,3,1) & 1 when 1 then @c17 else [exception_option] end,
		[arrive_option] = case substring(@bitmap,3,1) & 2 when 2 then @c18 else [arrive_option] end,
		[sign_form_image] = case substring(@bitmap,3,1) & 4 when 4 then @c19 else [sign_form_image] end,
		[sign_field_image] = case substring(@bitmap,3,1) & 8 when 8 then @c20 else [sign_field_image] end,
		[deliveryupload] = case substring(@bitmap,3,1) & 16 when 16 then @c21 else [deliveryupload] end,
		[photoupload] = case substring(@bitmap,3,1) & 32 when 32 then @c22 else [photoupload] end,
		[cdate] = case substring(@bitmap,3,1) & 64 when 64 then @c23 else [cdate] end,
		[cuser] = case substring(@bitmap,3,1) & 128 when 128 then @c24 else [cuser] end,
		[tracking_number] = case substring(@bitmap,4,1) & 1 when 1 then @c25 else [tracking_number] end,
		[Del_status] = case substring(@bitmap,4,1) & 2 when 2 then @c26 else [Del_status] end,
		[VoucherMoney] = case substring(@bitmap,4,1) & 4 when 4 then @c27 else [VoucherMoney] end,
		[CashMoney] = case substring(@bitmap,4,1) & 8 when 8 then @c28 else [CashMoney] end
	where [log_id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[log_id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttDeliveryScanLog]', @param2=@primarykey_text, @param3=13233
		End
end