﻿create procedure [sp_MSins_dbotbAreaArriveCodes]
    @c1 numeric(18,0),
    @c2 nvarchar(3),
    @c3 nvarchar(20),
    @c4 nvarchar(5),
    @c5 nvarchar(50),
    @c6 nvarchar(10),
    @c7 nvarchar(10)
as
begin  
	insert into [dbo].[tbAreaArriveCodes] (
		[arrive_id],
		[supplier_code],
		[supplier_name],
		[arrive_code],
		[arrive_address],
		[city],
		[area]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7	) 
end