﻿create procedure [sp_MSins_dbottArriveSitesScattered__]
    @c1 numeric(18,0),
    @c2 nvarchar(10),
    @c3 nvarchar(10),
    @c4 nvarchar(3),
    @c5 nvarchar(3),
    @c6 nvarchar(3),
    @c7 nvarchar(10),
    @c8 nvarchar(10)
as
begin  
	insert into [dbo].[ttArriveSitesScattered__] (
		[seq],
		[post_city],
		[post_area],
		[zip],
		[supplier_code],
		[area_arrive_code_],
		[station_code],
		[area_arrive_code]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8	) 
end