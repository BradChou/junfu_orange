﻿create procedure [sp_MSupd_dbotbMemberCollectionPrice]
		@c1 int = NULL,
		@c2 int = NULL,
		@c3 int = NULL,
		@pkc1 int = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[tbMemberCollectionPrice] set
		[min] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [min] end,
		[max] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [max] end
	where [seq] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[seq] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbMemberCollectionPrice]', @param2=@primarykey_text, @param3=13233
		End
end