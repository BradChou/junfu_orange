﻿create procedure [sp_MSupd_dbotbPriceLessThan_]
		@c1 bigint = NULL,
		@c2 nvarchar(10) = NULL,
		@c3 nvarchar(10) = NULL,
		@c4 int = NULL,
		@c5 int = NULL,
		@c6 int = NULL,
		@c7 nvarchar(10) = NULL,
		@c8 datetime = NULL,
		@c9 nvarchar(20) = NULL,
		@c10 datetime = NULL,
		@pkc1 bigint = NULL,
		@bitmap binary(2)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[tbPriceLessThan_] set
		[start_city] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [start_city] end,
		[end_city] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [end_city] end,
		[Cbmsize] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [Cbmsize] end,
		[first_price] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [first_price] end,
		[add_price] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [add_price] end,
		[business] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [business] end,
		[cdate] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [cdate] end,
		[cuser] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [cuser] end,
		[Enable_date] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [Enable_date] end
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbPriceLessThan_]', @param2=@primarykey_text, @param3=13233
		End
end