﻿create procedure [sp_MSins_dbottDispatchTask]
    @c1 int,
    @c2 int,
    @c3 varchar(20),
    @c4 datetime,
    @c5 varchar(3),
    @c6 nvarchar(3),
    @c7 nvarchar(10),
    @c8 nvarchar(20),
    @c9 nvarchar(10),
    @c10 nvarchar(10),
    @c11 int,
    @c12 int,
    @c13 int,
    @c14 datetime,
    @c15 nvarchar(10),
    @c16 nvarchar(100),
    @c17 nvarchar(10),
    @c18 datetime
as
begin  
	insert into [dbo].[ttDispatchTask] (
		[t_id],
		[task_type],
		[task_id],
		[task_date],
		[serial],
		[supplier],
		[supplier_no],
		[task_route],
		[driver],
		[car_license],
		[plates],
		[price],
		[status],
		[checkout_close_date],
		[close_randomCode],
		[memo],
		[c_user],
		[c_time]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12,
		@c13,
		@c14,
		@c15,
		@c16,
		@c17,
		@c18	) 
end