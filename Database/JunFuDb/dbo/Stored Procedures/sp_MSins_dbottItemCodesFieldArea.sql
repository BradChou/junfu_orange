﻿create procedure [sp_MSins_dbottItemCodesFieldArea]
    @c1 numeric(18,0),
    @c2 nvarchar(20),
    @c3 nvarchar(50),
    @c4 bit,
    @c5 nvarchar(255),
    @c6 nvarchar(20),
    @c7 datetime,
    @c8 nvarchar(20),
    @c9 datetime
as
begin  
	insert into [dbo].[ttItemCodesFieldArea] (
		[seq],
		[Account_Code],
		[code_name],
		[active_flag],
		[memo],
		[cuser],
		[cdate],
		[uuser],
		[udate]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9	) 
end