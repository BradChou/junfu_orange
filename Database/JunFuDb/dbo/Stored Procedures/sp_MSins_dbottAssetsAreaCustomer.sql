﻿create procedure [sp_MSins_dbottAssetsAreaCustomer]
    @c1 bigint,
    @c2 int,
    @c3 nvarchar(10),
    @c4 bit
as
begin  
	insert into [dbo].[ttAssetsAreaCustomer] (
		[id],
		[area_id],
		[customer_code],
		[chk]
	) values (
		@c1,
		@c2,
		@c3,
		@c4	) 
end