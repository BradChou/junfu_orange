﻿create procedure [sp_MSupd_dbottRepayment]
		@c1 int = NULL,
		@c2 int = NULL,
		@c3 int = NULL,
		@c4 datetime = NULL,
		@c5 datetime = NULL,
		@c6 int = NULL,
		@c7 int = NULL,
		@c8 int = NULL,
		@c9 int = NULL,
		@c10 int = NULL,
		@c11 int = NULL,
		@c12 bit = NULL,
		@c13 int = NULL,
		@c14 nvarchar(50) = NULL,
		@c15 varchar(10) = NULL,
		@c16 datetime = NULL,
		@pkc1 int = NULL,
		@bitmap binary(2)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttRepayment] set
		[loan_id] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [loan_id] end,
		[type] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [type] end,
		[due_date] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [due_date] end,
		[pay_date] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [pay_date] end,
		[opening_balance] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [opening_balance] end,
		[installment_price] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [installment_price] end,
		[principal] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [principal] end,
		[interest] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [interest] end,
		[end_balance] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [end_balance] end,
		[total_interest] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [total_interest] end,
		[paid] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [paid] end,
		[paid_price] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [paid_price] end,
		[memo] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [memo] end,
		[cuser] = case substring(@bitmap,2,1) & 64 when 64 then @c15 else [cuser] end,
		[cdate] = case substring(@bitmap,2,1) & 128 when 128 then @c16 else [cdate] end
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttRepayment]', @param2=@primarykey_text, @param3=13233
		End
end