﻿create procedure [sp_MSupd_dbottLoan]
		@c1 int = NULL,
		@c2 nvarchar(10) = NULL,
		@c3 int = NULL,
		@c4 int = NULL,
		@c5 int = NULL,
		@c6 float = NULL,
		@c7 float = NULL,
		@c8 int = NULL,
		@c9 datetime = NULL,
		@c10 int = NULL,
		@c11 int = NULL,
		@c12 int = NULL,
		@c13 int = NULL,
		@c14 nvarchar(20) = NULL,
		@c15 nvarchar(50) = NULL,
		@c16 int = NULL,
		@c17 int = NULL,
		@c18 int = NULL,
		@c19 datetime = NULL,
		@c20 float = NULL,
		@c21 float = NULL,
		@c22 int = NULL,
		@c23 int = NULL,
		@c24 int = NULL,
		@c25 int = NULL,
		@c26 int = NULL,
		@c27 int = NULL,
		@c28 varchar(10) = NULL,
		@c29 datetime = NULL,
		@c30 varchar(10) = NULL,
		@c31 datetime = NULL,
		@pkc1 int = NULL,
		@bitmap binary(4)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttLoan] set
		[car_license] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [car_license] end,
		[car_price] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [car_price] end,
		[first_payment] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [first_payment] end,
		[loan_price] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [loan_price] end,
		[annual_rate] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [annual_rate] end,
		[loan_period] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [loan_period] end,
		[annual_pay_count] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [annual_pay_count] end,
		[loan_startdate] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [loan_startdate] end,
		[installment_price] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [installment_price] end,
		[installment_count] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [installment_count] end,
		[paid_count] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [paid_count] end,
		[interest] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [interest] end,
		[loan_company] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [loan_company] end,
		[memo] = case substring(@bitmap,2,1) & 64 when 64 then @c15 else [memo] end,
		[car_price_s] = case substring(@bitmap,2,1) & 128 when 128 then @c16 else [car_price_s] end,
		[first_payment_s] = case substring(@bitmap,3,1) & 1 when 1 then @c17 else [first_payment_s] end,
		[loan_price_s] = case substring(@bitmap,3,1) & 2 when 2 then @c18 else [loan_price_s] end,
		[loan_startdate_s] = case substring(@bitmap,3,1) & 4 when 4 then @c19 else [loan_startdate_s] end,
		[annual_rate_s] = case substring(@bitmap,3,1) & 8 when 8 then @c20 else [annual_rate_s] end,
		[loan_period_s] = case substring(@bitmap,3,1) & 16 when 16 then @c21 else [loan_period_s] end,
		[annual_pay_count_s] = case substring(@bitmap,3,1) & 32 when 32 then @c22 else [annual_pay_count_s] end,
		[installment_price_s] = case substring(@bitmap,3,1) & 64 when 64 then @c23 else [installment_price_s] end,
		[installment_count_s] = case substring(@bitmap,3,1) & 128 when 128 then @c24 else [installment_count_s] end,
		[paid_count_s] = case substring(@bitmap,4,1) & 1 when 1 then @c25 else [paid_count_s] end,
		[interest_s] = case substring(@bitmap,4,1) & 2 when 2 then @c26 else [interest_s] end,
		[deposit_s] = case substring(@bitmap,4,1) & 4 when 4 then @c27 else [deposit_s] end,
		[cuser] = case substring(@bitmap,4,1) & 8 when 8 then @c28 else [cuser] end,
		[cdate] = case substring(@bitmap,4,1) & 16 when 16 then @c29 else [cdate] end,
		[uuser] = case substring(@bitmap,4,1) & 32 when 32 then @c30 else [uuser] end,
		[udate] = case substring(@bitmap,4,1) & 64 when 64 then @c31 else [udate] end
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttLoan]', @param2=@primarykey_text, @param3=13233
		End
end