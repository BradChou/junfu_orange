﻿create procedure [sp_MSins_dbottAutomachine]
    @c1 numeric(18,0),
    @c2 numeric(20,0),
    @c3 nvarchar(20),
    @c4 nvarchar(max),
    @c5 datetime
as
begin  
	insert into [dbo].[ttAutomachine] (
		[Automachine_id],
		[request_id],
		[check_number],
		[pic],
		[cdate]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5	) 
end