﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE LYGetDetail
	-- Add the parameters for the stored procedure here
	@start datetime,
	@end datetime
AS
BEGIN
	
declare @temp table
(
	[print_date] Date,
	[ship_date] Datetime,
	[ship_datetime] Datetime,
	[check_number] varchar(15),
	[send_station] varchar(15),
	[send_area] nvarchar(10),
	[receive_area] nvarchar(10),
	[work_station] nvarchar(10),
	[arrive_station] nvarchar(10),
	[area_arrive_code] nvarchar(10),
	[send_contact] nvarchar(40),
	[supplier_code] nvarchar(20),
	[pieces] int,
	[collection_money] int,
	[send_item] nvarchar(2),
	[arrive_date] date
)

declare @tempLog table
(
	[check_number] varchar(20),
	[driver_code] nvarchar(20),
	[scan_datetime] datetime,
	[scan_item] varchar(2)
)

declare @result table
(
	[print_date] Date,
	[ship_date] Date,
	[check_number] varchar(15),
	[send_station] varchar(15),
	[send_area] nvarchar(10),
	[receive_area] nvarchar(10),
	[work_station] nvarchar(10),
	[arrive_station] nvarchar(10),
	[area_arrive_code] nvarchar(10),
	[send_contact] nvarchar(40),
	[supplier_code] nvarchar(20),
	[pieces] int,
	[collection_money] int,
	[send_item] nvarchar(2),
	[arrive_date] date,
	[driver_code] nvarchar(20),
	[scan_datetime] datetime,
	[scan_item] varchar(2)
)

declare @Station table
(
	[station_code] nvarchar(20),
	[station_scode] nvarchar(20),
	[station_name] nvarchar(100)
)

insert into @temp (print_date, ship_date, ship_datetime, check_number,supplier_code, send_area, receive_area, area_arrive_code, send_contact, pieces, collection_money, send_item,arrive_date)
select
		c.print_date as print_date,
		case t.scan_item
			when 5 then DATEADD(hour, -5, c.ship_date)
			when 6 then DATEADD(hour, -5, c.ship_date)
			when 7 then DATEADD(hour, -5, c.ship_date)
			when 1 then DATEADD(day, -1, DATEADD(hour, -5, c.ship_date))
			when 2 then DATEADD(day, -1, DATEADD(hour, -5, c.ship_date))
			when 3 then DATEADD(day, -1, DATEADD(hour, -5, c.ship_date))
			end
			as ship_date,
		c.ship_date,
	    c.check_number as check_number,
		supplier_code = substring(c.supplier_code, 2,5), 
		c.send_area as send_area,
		c.receive_area as receive_area,
		c.area_arrive_code as area_arrive_code,
		c.send_contact as send_contact,
		c.pieces as pieces,
		c.collection_money as collection_money,
		send_item = '集貨',
		case t.scan_item
			when 5 then DATEADD(day,1, c.ship_date)
			when 6 then DATEADD(day,1, c.ship_date)
			when 7 then DATEADD(day,1, c.ship_date)
			when 1 then c.ship_date
			when 2 then c.ship_date
			when 3 then c.ship_date
			end
			as arrive_date
from tcDeliveryRequests c left join ttDeliveryScanLog t on c.check_number = t.check_number
where 
		LEN(c.check_number)=12  and 
	  (c.check_number not between '102000000000' AND '103000000000') and
      (c.check_number < '990000000000') and 
	  ((t.scan_item > 4 and c.ship_date>= @start and c.ship_date < @end and c.ship_date = t.scan_date)or (t.scan_item < 4 and c.ship_date>= DATEADD(day, 1, @start) and c.ship_date < DATEADD(day, 1, @end) and c.ship_date = t.scan_date))
order by c.check_number 

insert into @tempLog
select
check_number = t.check_number,
driver_code = d.driver_code,
scan_datetime = d.scan_date,
scan_item = d.scan_item
from @temp t left join ttDeliveryScanLog d on t.check_number = d.check_number where t.ship_datetime = d.scan_date 
order by check_number

insert into @Station
select
	station_code = s.station_code,
	station_scode = s.station_scode,
	station_name = s.station_name
from tbStation s

update @temp
set
	send_station = station_name,
	work_station = station_name
from @temp t left join tbStation s on t.supplier_code = s.station_scode

update @temp
set
	arrive_station = station_name
from @temp t left join tbStation s on t.area_arrive_code = s.station_scode

update @tempLog
set
	driver_code += d.driver_name
from @templog l left join tbDrivers d on l.driver_code = d.driver_code where l.driver_code = d.driver_code

insert into @result
select
	print_date = print_date,
	ship_date = ship_date,
	check_number = t.check_number,
	send_station = send_station,
	send_area = send_area,
	receive_area = receive_area,
	work_station = work_station,
	arrive_station = arrive_station,
	area_arrive_code =area_arrive_code,
	send_contact = send_contact,
	supplier_code = supplier_code,
	pieces = pieces,
	collection_money = collection_money,
	send_item = send_item,
	arrive_date = arrive_date,
	driver_code = driver_code,
	scan_datetime = scan_datetime,
	scan_item = scan_item
from @temp t left join @tempLog l on t.check_number = l.check_number

select 
	print_date,
	ship_date,
	check_number,
	send_station,
	work_station,
	arrive_station,
	send_contact,
	send_area,
	receive_area,
	pieces,
	collection_money,
	send_item,
	driver_code,
	scan_datetime,
	supplier_code,
	area_arrive_code,
	arrive_date
from @result 

END