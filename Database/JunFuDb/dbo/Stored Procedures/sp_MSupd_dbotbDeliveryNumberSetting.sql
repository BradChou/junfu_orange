﻿create procedure [sp_MSupd_dbotbDeliveryNumberSetting]
		@c1 int = NULL,
		@c2 nvarchar(20) = NULL,
		@c3 bigint = NULL,
		@c4 bigint = NULL,
		@c5 bigint = NULL,
		@c6 nvarchar(10) = NULL,
		@c7 datetime = NULL,
		@c8 int = NULL,
		@c9 int = NULL,
		@pkc1 int = NULL,
		@bitmap binary(2)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[tbDeliveryNumberSetting] set
		[customer_code] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [customer_code] end,
		[begin_number] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [begin_number] end,
		[end_number] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [end_number] end,
		[current_number] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [current_number] end,
		[cuser] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [cuser] end,
		[cdate] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [cdate] end,
		[IsActive] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [IsActive] end,
		[num] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [num] end
	where [seq] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[seq] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbDeliveryNumberSetting]', @param2=@primarykey_text, @param3=13233
		End
end