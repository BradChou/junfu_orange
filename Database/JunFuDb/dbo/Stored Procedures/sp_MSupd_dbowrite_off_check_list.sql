﻿create procedure [sp_MSupd_dbowrite_off_check_list]
		@c1 int = NULL,
		@c2 nvarchar(10) = NULL,
		@c3 nvarchar(5) = NULL,
		@pkc1 int = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
if (substring(@bitmap,1,1) & 1 = 1)
begin 
update [dbo].[write_off_check_list] set
		[id] = case substring(@bitmap,1,1) & 1 when 1 then @c1 else [id] end,
		[information] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [information] end,
		[type] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [type] end
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[write_off_check_list]', @param2=@primarykey_text, @param3=13233
		End
end  
else
begin 
update [dbo].[write_off_check_list] set
		[information] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [information] end,
		[type] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [type] end
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[write_off_check_list]', @param2=@primarykey_text, @param3=13233
		End
end 
end