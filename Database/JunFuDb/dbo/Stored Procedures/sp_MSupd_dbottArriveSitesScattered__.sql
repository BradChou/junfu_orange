﻿create procedure [sp_MSupd_dbottArriveSitesScattered__]
		@c1 numeric(18,0) = NULL,
		@c2 nvarchar(10) = NULL,
		@c3 nvarchar(10) = NULL,
		@c4 nvarchar(3) = NULL,
		@c5 nvarchar(3) = NULL,
		@c6 nvarchar(3) = NULL,
		@c7 nvarchar(10) = NULL,
		@c8 nvarchar(10) = NULL,
		@pkc1 numeric(18,0) = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttArriveSitesScattered__] set
		[post_city] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [post_city] end,
		[post_area] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [post_area] end,
		[zip] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [zip] end,
		[supplier_code] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [supplier_code] end,
		[area_arrive_code_] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [area_arrive_code_] end,
		[station_code] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [station_code] end,
		[area_arrive_code] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [area_arrive_code] end
	where [seq] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[seq] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttArriveSitesScattered__]', @param2=@primarykey_text, @param3=13233
		End
end