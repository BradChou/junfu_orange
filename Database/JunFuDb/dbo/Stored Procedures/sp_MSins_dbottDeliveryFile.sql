﻿create procedure [sp_MSins_dbottDeliveryFile]
    @c1 bigint,
    @c2 bigint,
    @c3 nvarchar(20),
    @c4 nvarchar(2),
    @c5 nvarchar(100),
    @c6 nvarchar(100),
    @c7 datetime,
    @c8 nvarchar(10)
as
begin  
	insert into [dbo].[ttDeliveryFile] (
		[log_id],
		[request_id],
		[check_number],
		[file_type],
		[file_path],
		[file_name],
		[udate],
		[uuser]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8	) 
end