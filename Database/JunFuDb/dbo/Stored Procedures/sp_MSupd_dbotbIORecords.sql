﻿create procedure [sp_MSupd_dbotbIORecords]
		@c1 int = NULL,
		@c2 nvarchar(20) = NULL,
		@c3 datetime = NULL,
		@c4 nvarchar(20) = NULL,
		@c5 datetime = NULL,
		@c6 int = NULL,
		@c7 nvarchar(20) = NULL,
		@c8 nvarchar(50) = NULL,
		@c9 nvarchar(10) = NULL,
		@c10 nvarchar(100) = NULL,
		@c11 int = NULL,
		@c12 int = NULL,
		@c13 int = NULL,
		@c14 datetime = NULL,
		@c15 datetime = NULL,
		@c16 nvarchar(1000) = NULL,
		@c17 bit = NULL,
		@c18 nvarchar(20) = NULL,
		@c19 datetime = NULL,
		@c20 int = NULL,
		@pkc1 int = NULL,
		@bitmap binary(3)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[tbIORecords] set
		[cuser] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [cuser] end,
		[cdate] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [cdate] end,
		[uuser] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [uuser] end,
		[udate] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [udate] end,
		[type] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [type] end,
		[changeTable] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [changeTable] end,
		[fromWhere] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [fromWhere] end,
		[randomCode] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [randomCode] end,
		[memo] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [memo] end,
		[successNum] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [successNum] end,
		[failNum] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [failNum] end,
		[totalNum] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [totalNum] end,
		[startTime] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [startTime] end,
		[endTime] = case substring(@bitmap,2,1) & 64 when 64 then @c15 else [endTime] end,
		[descript] = case substring(@bitmap,2,1) & 128 when 128 then @c16 else [descript] end,
		[result] = case substring(@bitmap,3,1) & 1 when 1 then @c17 else [result] end,
		[customer_code] = case substring(@bitmap,3,1) & 2 when 2 then @c18 else [customer_code] end,
		[print_date] = case substring(@bitmap,3,1) & 4 when 4 then @c19 else [print_date] end,
		[status] = case substring(@bitmap,3,1) & 8 when 8 then @c20 else [status] end
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbIORecords]', @param2=@primarykey_text, @param3=13233
		End
end