﻿create procedure [sp_MSupd_dbottKuoYangScanLog]
		@c1 decimal(18,0) = NULL,
		@c2 nvarchar(20) = NULL,
		@c3 nvarchar(30) = NULL,
		@c4 bit = NULL,
		@c5 datetime = NULL,
		@c6 datetime = NULL,
		@c7 nvarchar(100) = NULL,
		@pkc1 decimal(18,0) = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttKuoYangScanLog] set
		[check_number] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [check_number] end,
		[order_number] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [order_number] end,
		[flag_type] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [flag_type] end,
		[cdate] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [cdate] end,
		[udate] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [udate] end,
		[ImportFile] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [ImportFile] end
	where [log_id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[log_id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttKuoYangScanLog]', @param2=@primarykey_text, @param3=13233
		End
end