﻿create procedure [sp_MSins_dbottInsuranceRecord]
    @c1 bigint,
    @c2 int,
    @c3 int,
    @c4 int,
    @c5 int,
    @c6 bit,
    @c7 nvarchar(50)
as
begin  
	insert into [dbo].[ttInsuranceRecord] (
		[id],
		[Insurance_id],
		[year],
		[month],
		[price],
		[ispay],
		[memo]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7	) 
end