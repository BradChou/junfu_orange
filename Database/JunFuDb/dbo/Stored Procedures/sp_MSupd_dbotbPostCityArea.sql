﻿create procedure [sp_MSupd_dbotbPostCityArea]
		@c1 numeric(18,0) = NULL,
		@c2 nvarchar(10) = NULL,
		@c3 nvarchar(10) = NULL,
		@c4 nvarchar(3) = NULL,
		@pkc1 numeric(18,0) = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[tbPostCityArea] set
		[city] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [city] end,
		[area] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [area] end,
		[zip] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [zip] end
	where [seq] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[seq] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbPostCityArea]', @param2=@primarykey_text, @param3=13233
		End
end