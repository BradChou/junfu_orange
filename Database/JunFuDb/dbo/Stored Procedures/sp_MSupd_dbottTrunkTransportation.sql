﻿create procedure [sp_MSupd_dbottTrunkTransportation]
		@c1 bigint = NULL,
		@c2 varchar(10) = NULL,
		@c3 varchar(10) = NULL,
		@c4 nvarchar(10) = NULL,
		@c5 datetime = NULL,
		@c6 datetime = NULL,
		@c7 datetime = NULL,
		@pkc1 bigint = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttTrunkTransportation] set
		[car_license] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [car_license] end,
		[tel] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [tel] end,
		[driver_name] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [driver_name] end,
		[sdate] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [sdate] end,
		[edate] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [edate] end,
		[cdate] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [cdate] end
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttTrunkTransportation]', @param2=@primarykey_text, @param3=13233
		End
end