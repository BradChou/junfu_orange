﻿create procedure [sp_MSupd_dbotbAccountingSubject]
		@c1 varchar(10) = NULL,
		@c2 nvarchar(20) = NULL,
		@pkc1 varchar(10) = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
if (substring(@bitmap,1,1) & 1 = 1)
begin 
update [dbo].[tbAccountingSubject] set
		[AcntId] = case substring(@bitmap,1,1) & 1 when 1 then @c1 else [AcntId] end,
		[AcntName] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [AcntName] end
	where [AcntId] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[AcntId] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbAccountingSubject]', @param2=@primarykey_text, @param3=13233
		End
end  
else
begin 
update [dbo].[tbAccountingSubject] set
		[AcntName] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [AcntName] end
	where [AcntId] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[AcntId] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbAccountingSubject]', @param2=@primarykey_text, @param3=13233
		End
end 
end