﻿create procedure [sp_MSins_dbottAssetsPrepay]
    @c1 bigint,
    @c2 nvarchar(10),
    @c3 varchar(2),
    @c4 bit
as
begin  
	insert into [dbo].[ttAssetsPrepay] (
		[p_id],
		[car_license],
		[fee_type],
		[prepay]
	) values (
		@c1,
		@c2,
		@c3,
		@c4	) 
end