﻿create procedure [sp_MSupd_dbottInsurance]
		@c1 int = NULL,
		@c2 nvarchar(20) = NULL,
		@c3 bigint = NULL,
		@c4 nvarchar(50) = NULL,
		@c5 nvarchar(20) = NULL,
		@c6 nvarchar(10) = NULL,
		@c7 datetime = NULL,
		@c8 datetime = NULL,
		@c9 int = NULL,
		@c10 int = NULL,
		@c11 int = NULL,
		@c12 int = NULL,
		@c13 int = NULL,
		@c14 nvarchar(50) = NULL,
		@c15 nvarchar(20) = NULL,
		@c16 datetime = NULL,
		@c17 nvarchar(20) = NULL,
		@c18 datetime = NULL,
		@pkc1 int = NULL,
		@bitmap binary(3)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttInsurance] set
		[form_id] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [form_id] end,
		[assets_id] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [assets_id] end,
		[Insurance_name] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [Insurance_name] end,
		[company] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [company] end,
		[kind] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [kind] end,
		[sdtate] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [sdtate] end,
		[edate] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [edate] end,
		[original_price] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [original_price] end,
		[price] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [price] end,
		[amount_paid] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [amount_paid] end,
		[apportion] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [apportion] end,
		[apportion_price] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [apportion_price] end,
		[memo] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [memo] end,
		[cuser] = case substring(@bitmap,2,1) & 64 when 64 then @c15 else [cuser] end,
		[cdate] = case substring(@bitmap,2,1) & 128 when 128 then @c16 else [cdate] end,
		[uuser] = case substring(@bitmap,3,1) & 1 when 1 then @c17 else [uuser] end,
		[udate] = case substring(@bitmap,3,1) & 2 when 2 then @c18 else [udate] end
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttInsurance]', @param2=@primarykey_text, @param3=13233
		End
end