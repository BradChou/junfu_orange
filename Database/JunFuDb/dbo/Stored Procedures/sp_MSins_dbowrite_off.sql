﻿create procedure [sp_MSins_dbowrite_off]
    @c1 varchar(20),
    @c2 int,
    @c3 datetime,
    @c4 nvarchar(5),
    @c5 datetime
as
begin  
	insert into [dbo].[write_off] (
		[check_number],
		[write_off_id],
		[udate],
		[write_off_type],
		[cdate]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5	) 
end