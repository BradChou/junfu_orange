﻿create procedure [sp_MSins_dbotbFunctions]
    @c1 numeric(18,0),
    @c2 varchar(10),
    @c3 nvarchar(30),
    @c4 varchar(10),
    @c5 int,
    @c6 varchar(50),
    @c7 nvarchar(50),
    @c8 varchar(20),
    @c9 datetime,
    @c10 varchar(20),
    @c11 datetime,
    @c12 int,
    @c13 int,
    @c14 bit
as
begin  
	insert into [dbo].[tbFunctions] (
		[func_id],
		[func_code],
		[func_name],
		[upper_level_code],
		[Level],
		[func_link],
		[cssclass],
		[cuser],
		[cdate],
		[uuser],
		[udate],
		[sort],
		[type],
		[visible]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12,
		@c13,
		@c14	) 
end