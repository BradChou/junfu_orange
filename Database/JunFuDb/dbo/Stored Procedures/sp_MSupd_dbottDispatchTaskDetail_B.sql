﻿create procedure [sp_MSupd_dbottDispatchTaskDetail_B]
		@c1 int = NULL,
		@c2 int = NULL,
		@c3 datetime = NULL,
		@c4 nvarchar(10) = NULL,
		@c5 nvarchar(10) = NULL,
		@c6 int = NULL,
		@pkc1 int = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttDispatchTaskDetail_B] set
		[t_id] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [t_id] end,
		[date] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [date] end,
		[warehouse] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [warehouse] end,
		[supplier] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [supplier] end,
		[plates] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [plates] end
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttDispatchTaskDetail_B]', @param2=@primarykey_text, @param3=13233
		End
end