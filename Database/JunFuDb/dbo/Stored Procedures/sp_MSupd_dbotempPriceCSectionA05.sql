﻿create procedure [sp_MSupd_dbotempPriceCSectionA05]
		@c1 numeric(18,0) = NULL,
		@c2 int = NULL,
		@c3 nvarchar(2) = NULL,
		@c4 numeric(18,2) = NULL,
		@c5 numeric(18,2) = NULL,
		@c6 numeric(18,2) = NULL,
		@c7 numeric(18,2) = NULL,
		@c8 numeric(18,2) = NULL,
		@c9 numeric(18,2) = NULL,
		@pkc1 numeric(18,0) = NULL,
		@bitmap binary(2)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[tempPriceCSectionA05] set
		[class_level] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [class_level] end,
		[pricing_code] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [pricing_code] end,
		[plate1_price] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [plate1_price] end,
		[plate2_price] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [plate2_price] end,
		[plate3_price] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [plate3_price] end,
		[plate4_price] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [plate4_price] end,
		[plate5_price] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [plate5_price] end,
		[plate6_price] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [plate6_price] end
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tempPriceCSectionA05]', @param2=@primarykey_text, @param3=13233
		End
end