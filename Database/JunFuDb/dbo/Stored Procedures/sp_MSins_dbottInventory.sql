﻿create procedure [sp_MSins_dbottInventory]
    @c1 int,
    @c2 nvarchar(10),
    @c3 nvarchar(25),
    @c4 datetime,
    @c5 nvarchar(50),
    @c6 datetime
as
begin  
	insert into [dbo].[ttInventory] (
		[Inventory_ID],
		[station_code],
		[driver_code],
		[scanning_dt],
		[check_number],
		[create_dt]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6	) 
end