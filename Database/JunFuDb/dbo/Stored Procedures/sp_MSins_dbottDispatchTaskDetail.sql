﻿create procedure [sp_MSins_dbottDispatchTaskDetail]
    @c1 int,
    @c2 int,
    @c3 bigint
as
begin  
	insert into [dbo].[ttDispatchTaskDetail] (
		[id],
		[t_id],
		[request_id]
	) values (
		@c1,
		@c2,
		@c3	) 
end