﻿create procedure [sp_MSins_dbottPriceBusinessDefineLog]
    @c1 numeric(18,0),
    @c2 nvarchar(12),
    @c3 date,
    @c4 char(1),
    @c5 datetime,
    @c6 nvarchar(20)
as
begin  
	insert into [dbo].[ttPriceBusinessDefineLog] (
		[log_id],
		[customer_code],
		[tariffs_effect_date],
		[tariffs_type],
		[cdate],
		[cuser]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6	) 
end