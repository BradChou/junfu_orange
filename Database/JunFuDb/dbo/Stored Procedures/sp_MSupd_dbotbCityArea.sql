﻿create procedure [sp_MSupd_dbotbCityArea]
		@c1 int = NULL,
		@c2 nvarchar(20) = NULL,
		@c3 nvarchar(3) = NULL,
		@c4 nvarchar(15) = NULL,
		@c5 int = NULL,
		@c6 nvarchar(50) = NULL,
		@c7 int = NULL,
		@c8 datetime = NULL,
		@c9 nvarchar(20) = NULL,
		@pkc1 int = NULL,
		@bitmap binary(2)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[tbCityArea] set
		[CityArea] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [CityArea] end,
		[City] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [City] end,
		[Area] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [Area] end,
		[Zip] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [Zip] end,
		[Note] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [Note] end,
		[IsActive] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [IsActive] end,
		[cdate] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [cdate] end,
		[cuser] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [cuser] end
	where [seq] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[seq] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbCityArea]', @param2=@primarykey_text, @param3=13233
		End
end