﻿create procedure [sp_MSins_dbotbRemoteSections]
    @c1 numeric(18,0),
    @c2 nvarchar(10),
    @c3 nvarchar(10),
    @c4 nvarchar(10),
    @c5 nvarchar(50),
    @c6 nvarchar(50),
    @c7 char(1),
    @c8 int,
    @c9 int,
    @c10 nvarchar(200),
    @c11 numeric(18,2)
as
begin  
	insert into [dbo].[tbRemoteSections] (
		[seq],
		[area],
		[post_city],
		[post_area],
		[area_content],
		[road],
		[number_type],
		[start_no],
		[end_no],
		[delivery_section],
		[append_price]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11	) 
end