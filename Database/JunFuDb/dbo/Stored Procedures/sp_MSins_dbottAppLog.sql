﻿create procedure [sp_MSins_dbottAppLog]
    @c1 numeric(18,0),
    @c2 varchar(30),
    @c3 varchar(40),
    @c4 varchar(10),
    @c5 varchar(300),
    @c6 varchar(2200),
    @c7 datetime
as
begin  
	insert into [dbo].[ttAppLog] (
		[log_id],
		[app_project],
		[app_fun_name],
		[app_type],
		[app_memo],
		[app_log],
		[cdate]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7	) 
end