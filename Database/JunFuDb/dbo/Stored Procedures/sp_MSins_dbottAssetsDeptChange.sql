﻿create procedure [sp_MSins_dbottAssetsDeptChange]
    @c1 int,
    @c2 bigint,
    @c3 nvarchar(10),
    @c4 nvarchar(50),
    @c5 nvarchar(50),
    @c6 nvarchar(50),
    @c7 nvarchar(20),
    @c8 datetime,
    @c9 nvarchar(20),
    @c10 datetime
as
begin  
	insert into [dbo].[ttAssetsDeptChange] (
		[id],
		[a_id],
		[car_license],
		[dept],
		[NewDept],
		[memo],
		[cuser],
		[cdate],
		[uuser],
		[udate]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10	) 
end