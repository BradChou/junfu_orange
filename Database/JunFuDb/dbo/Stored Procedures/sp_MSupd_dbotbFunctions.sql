﻿create procedure [sp_MSupd_dbotbFunctions]
		@c1 numeric(18,0) = NULL,
		@c2 varchar(10) = NULL,
		@c3 nvarchar(30) = NULL,
		@c4 varchar(10) = NULL,
		@c5 int = NULL,
		@c6 varchar(50) = NULL,
		@c7 nvarchar(50) = NULL,
		@c8 varchar(20) = NULL,
		@c9 datetime = NULL,
		@c10 varchar(20) = NULL,
		@c11 datetime = NULL,
		@c12 int = NULL,
		@c13 int = NULL,
		@c14 bit = NULL,
		@pkc1 numeric(18,0) = NULL,
		@bitmap binary(2)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[tbFunctions] set
		[func_code] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [func_code] end,
		[func_name] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [func_name] end,
		[upper_level_code] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [upper_level_code] end,
		[Level] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [Level] end,
		[func_link] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [func_link] end,
		[cssclass] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [cssclass] end,
		[cuser] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [cuser] end,
		[cdate] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [cdate] end,
		[uuser] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [uuser] end,
		[udate] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [udate] end,
		[sort] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [sort] end,
		[type] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [type] end,
		[visible] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [visible] end
	where [func_id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[func_id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbFunctions]', @param2=@primarykey_text, @param3=13233
		End
end