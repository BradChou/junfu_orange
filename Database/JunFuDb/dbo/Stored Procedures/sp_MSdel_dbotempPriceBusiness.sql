﻿create procedure [sp_MSdel_dbotempPriceBusiness]
		@pkc1 numeric(18,0)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [dbo].[tempPriceBusiness] 
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tempPriceBusiness]', @param2=@primarykey_text, @param3=13234
		End
end