﻿create procedure [sp_MSins_dbottPriceClassLog]
    @c1 numeric(18,0),
    @c2 int,
    @c3 numeric(10,2),
    @c4 numeric(10,2),
    @c5 numeric(10,2),
    @c6 numeric(10,2),
    @c7 numeric(10,2),
    @c8 numeric(10,2),
    @c9 numeric(10,2),
    @c10 char(1),
    @c11 datetime,
    @c12 date,
    @c13 datetime,
    @c14 nvarchar(20)
as
begin  
	insert into [dbo].[ttPriceClassLog] (
		[seq],
		[class_level],
		[week1],
		[week2],
		[week3],
		[week4],
		[week5],
		[week6],
		[arrive],
		[notify_flag],
		[notify_time],
		[active_date],
		[cdate],
		[cuser]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12,
		@c13,
		@c14	) 
end