﻿create procedure [sp_MSupd_dbotbMemberCollectionPriceList]
		@c1 int = NULL,
		@c2 nvarchar(20) = NULL,
		@c3 int = NULL,
		@c4 float = NULL,
		@c5 int = NULL,
		@pkc1 int = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[tbMemberCollectionPriceList] set
		[customer_code] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [customer_code] end,
		[Handling_fee] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [Handling_fee] end,
		[order_fee] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [order_fee] end,
		[CollectionPrice] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [CollectionPrice] end
	where [seq] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[seq] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbMemberCollectionPriceList]', @param2=@primarykey_text, @param3=13233
		End
end