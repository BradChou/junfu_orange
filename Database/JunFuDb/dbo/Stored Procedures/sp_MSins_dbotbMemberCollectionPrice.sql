﻿create procedure [sp_MSins_dbotbMemberCollectionPrice]
    @c1 int,
    @c2 int,
    @c3 int
as
begin  
	insert into [dbo].[tbMemberCollectionPrice] (
		[seq],
		[min],
		[max]
	) values (
		@c1,
		@c2,
		@c3	) 
end