﻿create procedure [sp_MSins_dbottPlletReturn]
    @c1 int,
    @c2 bigint,
    @c3 int,
    @c4 datetime,
    @c5 bigint,
    @c6 datetime,
    @c7 nvarchar(10)
as
begin  
	insert into [dbo].[ttPlletReturn] (
		[id],
		[request_id],
		[return_cnt],
		[return_date],
		[return_request_id],
		[udate],
		[uuser]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7	) 
end