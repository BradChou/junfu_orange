﻿create procedure [sp_MSins_dbottAssetsOil_bak]
    @c1 int,
    @c2 int,
    @c3 varchar(2),
    @c4 nchar(10),
    @c5 datetime,
    @c6 datetime,
    @c7 datetime
as
begin  
	insert into [dbo].[ttAssetsOil_bak] (
		[id],
		[a_id],
		[oil],
		[card_id],
		[active_date],
		[stop_date],
		[cdate]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7	) 
end