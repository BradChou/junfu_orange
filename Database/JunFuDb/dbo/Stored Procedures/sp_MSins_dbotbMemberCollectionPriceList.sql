﻿create procedure [sp_MSins_dbotbMemberCollectionPriceList]
    @c1 int,
    @c2 nvarchar(20),
    @c3 int,
    @c4 float,
    @c5 int
as
begin  
	insert into [dbo].[tbMemberCollectionPriceList] (
		[seq],
		[customer_code],
		[Handling_fee],
		[order_fee],
		[CollectionPrice]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5	) 
end