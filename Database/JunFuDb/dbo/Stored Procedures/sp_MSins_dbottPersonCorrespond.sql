﻿create procedure [sp_MSins_dbottPersonCorrespond]
    @c1 int,
    @c2 nvarchar(20),
    @c3 bigint,
    @c4 nvarchar(max),
    @c5 nvarchar(20),
    @c6 datetime,
    @c7 nvarchar(20),
    @c8 datetime
as
begin  
	insert into [dbo].[ttPersonCorrespond] (
		[id],
		[Account_Code],
		[Kinds],
		[Str],
		[cuser],
		[cdate],
		[uuser],
		[udate]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8	) 
end