﻿create procedure [sp_MSupd_dbotcRoadSection]
		@c1 int = NULL,
		@c2 int = NULL,
		@c3 nvarchar(3) = NULL,
		@c4 nvarchar(10) = NULL,
		@c5 nvarchar(10) = NULL,
		@c6 int = NULL,
		@c7 int = NULL,
		@c8 nvarchar(10) = NULL,
		@c9 nvarchar(10) = NULL,
		@c10 int = NULL,
		@c11 int = NULL,
		@c12 int = NULL,
		@c13 int = NULL,
		@c14 int = NULL,
		@c15 nvarchar(10) = NULL,
		@c16 nvarchar(20) = NULL,
		@c17 nvarchar(20) = NULL,
		@c18 nvarchar(20) = NULL,
		@c19 nvarchar(20) = NULL,
		@c20 nvarchar(20) = NULL,
		@c21 nvarchar(20) = NULL,
		@c22 nvarchar(20) = NULL,
		@c23 nvarchar(20) = NULL,
		@c24 nvarchar(20) = NULL,
		@c25 nvarchar(20) = NULL,
		@c26 nvarchar(20) = NULL,
		@c27 nvarchar(20) = NULL,
		@c28 datetime = NULL,
		@c29 nvarchar(20) = NULL,
		@c30 datetime = NULL,
		@c31 nvarchar(20) = NULL,
		@pkc1 int = NULL,
		@bitmap binary(4)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[tcRoadSection] set
		[zip] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [zip] end,
		[city] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [city] end,
		[area] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [area] end,
		[village] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [village] end,
		[neighborS] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [neighborS] end,
		[neighborE] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [neighborE] end,
		[road] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [road] end,
		[section] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [section] end,
		[laneS] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [laneS] end,
		[laneE] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [laneE] end,
		[numbertype] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [numbertype] end,
		[numberS] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [numberS] end,
		[numberE] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [numberE] end,
		[sta_code] = case substring(@bitmap,2,1) & 64 when 64 then @c15 else [sta_code] end,
		[collecting_area] = case substring(@bitmap,2,1) & 128 when 128 then @c16 else [collecting_area] end,
		[collecting_areaR] = case substring(@bitmap,3,1) & 1 when 1 then @c17 else [collecting_areaR] end,
		[arrive_distribution] = case substring(@bitmap,3,1) & 2 when 2 then @c18 else [arrive_distribution] end,
		[arrive_stack] = case substring(@bitmap,3,1) & 4 when 4 then @c19 else [arrive_stack] end,
		[driver] = case substring(@bitmap,3,1) & 8 when 8 then @c20 else [driver] end,
		[emp_code] = case substring(@bitmap,3,1) & 16 when 16 then @c21 else [emp_code] end,
		[MDarrive_distribution] = case substring(@bitmap,3,1) & 32 when 32 then @c22 else [MDarrive_distribution] end,
		[MDarrive_stack] = case substring(@bitmap,3,1) & 64 when 64 then @c23 else [MDarrive_stack] end,
		[DDarrive_distribution] = case substring(@bitmap,3,1) & 128 when 128 then @c24 else [DDarrive_distribution] end,
		[DDarrive_stack] = case substring(@bitmap,4,1) & 1 when 1 then @c25 else [DDarrive_stack] end,
		[HDarrive_distribution] = case substring(@bitmap,4,1) & 2 when 2 then @c26 else [HDarrive_distribution] end,
		[HDarrive_stack] = case substring(@bitmap,4,1) & 4 when 4 then @c27 else [HDarrive_stack] end,
		[Cdate] = case substring(@bitmap,4,1) & 8 when 8 then @c28 else [Cdate] end,
		[Cuser] = case substring(@bitmap,4,1) & 16 when 16 then @c29 else [Cuser] end,
		[Udate] = case substring(@bitmap,4,1) & 32 when 32 then @c30 else [Udate] end,
		[Uuser] = case substring(@bitmap,4,1) & 64 when 64 then @c31 else [Uuser] end
	where [seq] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[seq] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tcRoadSection]', @param2=@primarykey_text, @param3=13233
		End
end