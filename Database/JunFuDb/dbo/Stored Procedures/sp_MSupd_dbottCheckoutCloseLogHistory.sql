﻿create procedure [sp_MSupd_dbottCheckoutCloseLogHistory]
		@c1 numeric(18,0) = NULL,
		@c2 varchar(10) = NULL,
		@c3 varchar(3) = NULL,
		@c4 char(1) = NULL,
		@c5 date = NULL,
		@c6 date = NULL,
		@c7 char(1) = NULL,
		@c8 date = NULL,
		@c9 nvarchar(10) = NULL,
		@pkc1 numeric(18,0) = NULL,
		@bitmap binary(2)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttCheckoutCloseLogHistory] set
		[customer_code] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [customer_code] end,
		[supplier_code] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [supplier_code] end,
		[customer_type] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [customer_type] end,
		[close_begdate] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [close_begdate] end,
		[close_enddate] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [close_enddate] end,
		[close_type] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [close_type] end,
		[cdate] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [cdate] end,
		[cuser] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [cuser] end
	where [log_id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[log_id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttCheckoutCloseLogHistory]', @param2=@primarykey_text, @param3=13233
		End
end