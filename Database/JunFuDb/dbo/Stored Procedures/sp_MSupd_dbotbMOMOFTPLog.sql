﻿create procedure [sp_MSupd_dbotbMOMOFTPLog]
		@c1 bigint = NULL,
		@c2 datetime = NULL,
		@c3 nvarchar(20) = NULL,
		@c4 nvarchar(10) = NULL,
		@c5 nvarchar(10) = NULL,
		@c6 nvarchar(20) = NULL,
		@pkc1 bigint = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[tbMOMOFTPLog] set
		[logdate] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [logdate] end,
		[check_number] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [check_number] end,
		[type] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [type] end,
		[type_option] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [type_option] end,
		[cuser] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [cuser] end
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbMOMOFTPLog]', @param2=@primarykey_text, @param3=13233
		End
end