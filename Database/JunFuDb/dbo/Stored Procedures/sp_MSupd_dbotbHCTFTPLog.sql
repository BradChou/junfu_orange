﻿create procedure [sp_MSupd_dbotbHCTFTPLog]
		@c1 bigint = NULL,
		@c2 bigint = NULL,
		@c3 datetime = NULL,
		@c4 nvarchar(10) = NULL,
		@c5 nvarchar(200) = NULL,
		@c6 nvarchar(20) = NULL,
		@c7 varchar(20) = NULL,
		@pkc1 bigint = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[tbHCTFTPLog] set
		[request_id] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [request_id] end,
		[logdate] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [logdate] end,
		[check_number] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [check_number] end,
		[message] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [message] end,
		[type] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [type] end,
		[cuser] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [cuser] end
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbHCTFTPLog]', @param2=@primarykey_text, @param3=13233
		End
end