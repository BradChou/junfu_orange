﻿create procedure [sp_MSupd_dbotbRemoteSections]
		@c1 numeric(18,0) = NULL,
		@c2 nvarchar(10) = NULL,
		@c3 nvarchar(10) = NULL,
		@c4 nvarchar(10) = NULL,
		@c5 nvarchar(50) = NULL,
		@c6 nvarchar(50) = NULL,
		@c7 char(1) = NULL,
		@c8 int = NULL,
		@c9 int = NULL,
		@c10 nvarchar(200) = NULL,
		@c11 numeric(18,2) = NULL,
		@pkc1 numeric(18,0) = NULL,
		@bitmap binary(2)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[tbRemoteSections] set
		[area] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [area] end,
		[post_city] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [post_city] end,
		[post_area] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [post_area] end,
		[area_content] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [area_content] end,
		[road] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [road] end,
		[number_type] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [number_type] end,
		[start_no] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [start_no] end,
		[end_no] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [end_no] end,
		[delivery_section] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [delivery_section] end,
		[append_price] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [append_price] end
	where [seq] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[seq] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbRemoteSections]', @param2=@primarykey_text, @param3=13233
		End
end