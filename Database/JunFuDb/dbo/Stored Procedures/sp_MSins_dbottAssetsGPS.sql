﻿create procedure [sp_MSins_dbottAssetsGPS]
    @c1 int,
    @c2 int,
    @c3 varchar(2),
    @c4 datetime,
    @c5 datetime
as
begin  
	insert into [dbo].[ttAssetsGPS] (
		[id],
		[a_id],
		[GPS],
		[active_time],
		[cdate]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5	) 
end