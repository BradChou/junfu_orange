﻿create procedure [sp_MSins_dbotcCbmSize]
    @c1 int,
    @c2 nvarchar(10),
    @c3 nvarchar(10)
as
begin  
	insert into [dbo].[tcCbmSize] (
		[id],
		[CbmID],
		[CbmSize]
	) values (
		@c1,
		@c2,
		@c3	) 
end