﻿create procedure [sp_MSins_dbottItemCodesRoad]
    @c1 numeric(18,0),
    @c2 int,
    @c3 nvarchar(50),
    @c4 bit,
    @c5 int,
    @c6 nvarchar(2),
    @c7 nvarchar(255),
    @c8 nvarchar(20),
    @c9 datetime,
    @c10 nvarchar(20),
    @c11 datetime
as
begin  
	insert into [dbo].[ttItemCodesRoad] (
		[seq],
		[FieldArea],
		[code_name],
		[active_flag],
		[DayNight],
		[Temperature],
		[memo],
		[cuser],
		[cdate],
		[uuser],
		[udate]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11	) 
end