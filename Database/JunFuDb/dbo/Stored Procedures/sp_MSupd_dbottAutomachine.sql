﻿create procedure [sp_MSupd_dbottAutomachine]
		@c1 numeric(18,0) = NULL,
		@c2 numeric(20,0) = NULL,
		@c3 nvarchar(20) = NULL,
		@c4 nvarchar(max) = NULL,
		@c5 datetime = NULL,
		@pkc1 numeric(18,0) = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttAutomachine] set
		[request_id] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [request_id] end,
		[check_number] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [check_number] end,
		[pic] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [pic] end,
		[cdate] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [cdate] end
	where [Automachine_id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[Automachine_id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttAutomachine]', @param2=@primarykey_text, @param3=13233
		End
end