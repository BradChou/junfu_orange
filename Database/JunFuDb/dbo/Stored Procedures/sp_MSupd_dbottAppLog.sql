﻿create procedure [sp_MSupd_dbottAppLog]
		@c1 numeric(18,0) = NULL,
		@c2 varchar(30) = NULL,
		@c3 varchar(40) = NULL,
		@c4 varchar(10) = NULL,
		@c5 varchar(300) = NULL,
		@c6 varchar(2200) = NULL,
		@c7 datetime = NULL,
		@pkc1 numeric(18,0) = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttAppLog] set
		[app_project] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [app_project] end,
		[app_fun_name] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [app_fun_name] end,
		[app_type] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [app_type] end,
		[app_memo] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [app_memo] end,
		[app_log] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [app_log] end,
		[cdate] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [cdate] end
	where [log_id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[log_id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttAppLog]', @param2=@primarykey_text, @param3=13233
		End
end