﻿create procedure [sp_MSupd_dbotbEmps]
		@c1 numeric(18,0) = NULL,
		@c2 nvarchar(10) = NULL,
		@c3 nvarchar(20) = NULL,
		@c4 nvarchar(20) = NULL,
		@c5 char(1) = NULL,
		@c6 bit = NULL,
		@c7 nvarchar(20) = NULL,
		@c8 nvarchar(20) = NULL,
		@c9 varchar(10) = NULL,
		@c10 datetime = NULL,
		@c11 nvarchar(20) = NULL,
		@c12 datetime = NULL,
		@c13 nvarchar(20) = NULL,
		@pkc1 numeric(18,0) = NULL,
		@bitmap binary(2)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[tbEmps] set
		[customer_code] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [customer_code] end,
		[emp_code] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [emp_code] end,
		[emp_name] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [emp_name] end,
		[job_type] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [job_type] end,
		[active_flag] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [active_flag] end,
		[account_code] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [account_code] end,
		[driver_code] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [driver_code] end,
		[station] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [station] end,
		[udate] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [udate] end,
		[uuser] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [uuser] end,
		[cdate] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [cdate] end,
		[cuser] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [cuser] end
	where [emp_id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[emp_id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbEmps]', @param2=@primarykey_text, @param3=13233
		End
end