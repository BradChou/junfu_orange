﻿create procedure [sp_MSins_dbotbPriceLessThan]
    @c1 bigint,
    @c2 nvarchar(10),
    @c3 nvarchar(10),
    @c4 int,
    @c5 int,
    @c6 int,
    @c7 nvarchar(10),
    @c8 datetime,
    @c9 nvarchar(20),
    @c10 datetime,
    @c11 nvarchar(12)
as
begin  
	insert into [dbo].[tbPriceLessThan] (
		[id],
		[start_city],
		[end_city],
		[Cbmsize],
		[first_price],
		[add_price],
		[business],
		[cdate],
		[cuser],
		[Enable_date],
		[customer_code]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11	) 
end