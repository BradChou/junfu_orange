﻿create procedure [sp_MSins_dbotbAccountingSubject]
    @c1 varchar(10),
    @c2 nvarchar(20)
as
begin  
	insert into [dbo].[tbAccountingSubject] (
		[AcntId],
		[AcntName]
	) values (
		@c1,
		@c2	) 
end