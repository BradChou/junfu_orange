﻿create procedure [sp_MSins_dbottInsurance]
    @c1 int,
    @c2 nvarchar(20),
    @c3 bigint,
    @c4 nvarchar(50),
    @c5 nvarchar(20),
    @c6 nvarchar(10),
    @c7 datetime,
    @c8 datetime,
    @c9 int,
    @c10 int,
    @c11 int,
    @c12 int,
    @c13 int,
    @c14 nvarchar(50),
    @c15 nvarchar(20),
    @c16 datetime,
    @c17 nvarchar(20),
    @c18 datetime
as
begin  
	insert into [dbo].[ttInsurance] (
		[id],
		[form_id],
		[assets_id],
		[Insurance_name],
		[company],
		[kind],
		[sdtate],
		[edate],
		[original_price],
		[price],
		[amount_paid],
		[apportion],
		[apportion_price],
		[memo],
		[cuser],
		[cdate],
		[uuser],
		[udate]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12,
		@c13,
		@c14,
		@c15,
		@c16,
		@c17,
		@c18	) 
end