﻿create procedure [sp_MSins_dbotbEmps]
    @c1 numeric(18,0),
    @c2 nvarchar(10),
    @c3 nvarchar(20),
    @c4 nvarchar(20),
    @c5 char(1),
    @c6 bit,
    @c7 nvarchar(20),
    @c8 nvarchar(20),
    @c9 varchar(10),
    @c10 datetime,
    @c11 nvarchar(20),
    @c12 datetime,
    @c13 nvarchar(20)
as
begin  
	insert into [dbo].[tbEmps] (
		[emp_id],
		[customer_code],
		[emp_code],
		[emp_name],
		[job_type],
		[active_flag],
		[account_code],
		[driver_code],
		[station],
		[udate],
		[uuser],
		[cdate],
		[cuser]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12,
		@c13	) 
end