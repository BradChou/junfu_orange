﻿create procedure [sp_MSins_dbotbPriceCSectionA05]
    @c1 numeric(18,0),
    @c2 int,
    @c3 nvarchar(2),
    @c4 numeric(18,2),
    @c5 numeric(18,2),
    @c6 numeric(18,2),
    @c7 numeric(18,2),
    @c8 numeric(18,2),
    @c9 numeric(18,2)
as
begin  
	insert into [dbo].[tbPriceCSectionA05] (
		[id],
		[class_level],
		[pricing_code],
		[plate1_price],
		[plate2_price],
		[plate3_price],
		[plate4_price],
		[plate5_price],
		[plate6_price]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9	) 
end