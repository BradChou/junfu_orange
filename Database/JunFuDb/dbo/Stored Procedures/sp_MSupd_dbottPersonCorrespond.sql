﻿create procedure [sp_MSupd_dbottPersonCorrespond]
		@c1 int = NULL,
		@c2 nvarchar(20) = NULL,
		@c3 bigint = NULL,
		@c4 nvarchar(max) = NULL,
		@c5 nvarchar(20) = NULL,
		@c6 datetime = NULL,
		@c7 nvarchar(20) = NULL,
		@c8 datetime = NULL,
		@pkc1 int = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttPersonCorrespond] set
		[Account_Code] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [Account_Code] end,
		[Kinds] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [Kinds] end,
		[Str] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [Str] end,
		[cuser] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [cuser] end,
		[cdate] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [cdate] end,
		[uuser] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [uuser] end,
		[udate] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [udate] end
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttPersonCorrespond]', @param2=@primarykey_text, @param3=13233
		End
end