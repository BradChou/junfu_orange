﻿create procedure [sp_MSupd_dbotbSuppliers]
		@c1 numeric(18,0) = NULL,
		@c2 nvarchar(3) = NULL,
		@c3 nvarchar(10) = NULL,
		@c4 nvarchar(20) = NULL,
		@c5 nvarchar(20) = NULL,
		@c6 nvarchar(8) = NULL,
		@c7 nvarchar(10) = NULL,
		@c8 nvarchar(20) = NULL,
		@c9 nvarchar(20) = NULL,
		@c10 nvarchar(20) = NULL,
		@c11 nvarchar(10) = NULL,
		@c12 nvarchar(10) = NULL,
		@c13 nvarchar(50) = NULL,
		@c14 nvarchar(50) = NULL,
		@c15 nvarchar(8) = NULL,
		@c16 nvarchar(255) = NULL,
		@c17 nvarchar(255) = NULL,
		@c18 nvarchar(255) = NULL,
		@c19 nvarchar(255) = NULL,
		@c20 nvarchar(10) = NULL,
		@c21 nvarchar(20) = NULL,
		@c22 varchar(50) = NULL,
		@c23 nvarchar(10) = NULL,
		@c24 nvarchar(10) = NULL,
		@c25 nvarchar(50) = NULL,
		@c26 varchar(3) = NULL,
		@c27 bit = NULL,
		@c28 bit = NULL,
		@c29 bit = NULL,
		@c30 nvarchar(20) = NULL,
		@c31 datetime = NULL,
		@c32 nvarchar(20) = NULL,
		@c33 datetime = NULL,
		@pkc1 numeric(18,0) = NULL,
		@bitmap binary(5)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[tbSuppliers] set
		[supplier_code] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [supplier_code] end,
		[supplier_no] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [supplier_no] end,
		[supplier_name] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [supplier_name] end,
		[supplier_shortname] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [supplier_shortname] end,
		[uniform_numbers] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [uniform_numbers] end,
		[id_no] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [id_no] end,
		[principal] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [principal] end,
		[contact] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [contact] end,
		[telephone] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [telephone] end,
		[city] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [city] end,
		[area] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [area] end,
		[road] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [road] end,
		[email] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [email] end,
		[receipt_number] = case substring(@bitmap,2,1) & 64 when 64 then @c15 else [receipt_number] end,
		[id_image] = case substring(@bitmap,2,1) & 128 when 128 then @c16 else [id_image] end,
		[driver_image] = case substring(@bitmap,3,1) & 1 when 1 then @c17 else [driver_image] end,
		[account_image] = case substring(@bitmap,3,1) & 2 when 2 then @c18 else [account_image] end,
		[contract_content] = case substring(@bitmap,3,1) & 4 when 4 then @c19 else [contract_content] end,
		[bank_code] = case substring(@bitmap,3,1) & 8 when 8 then @c20 else [bank_code] end,
		[bank_name] = case substring(@bitmap,3,1) & 16 when 16 then @c21 else [bank_name] end,
		[account] = case substring(@bitmap,3,1) & 32 when 32 then @c22 else [account] end,
		[city_receipt] = case substring(@bitmap,3,1) & 64 when 64 then @c23 else [city_receipt] end,
		[area_receipt] = case substring(@bitmap,3,1) & 128 when 128 then @c24 else [area_receipt] end,
		[road_receipt] = case substring(@bitmap,4,1) & 1 when 1 then @c25 else [road_receipt] end,
		[warehouse] = case substring(@bitmap,4,1) & 2 when 2 then @c26 else [warehouse] end,
		[active_flag] = case substring(@bitmap,4,1) & 4 when 4 then @c27 else [active_flag] end,
		[show_trans] = case substring(@bitmap,4,1) & 8 when 8 then @c28 else [show_trans] end,
		[cross_region] = case substring(@bitmap,4,1) & 16 when 16 then @c29 else [cross_region] end,
		[cuser] = case substring(@bitmap,4,1) & 32 when 32 then @c30 else [cuser] end,
		[cdate] = case substring(@bitmap,4,1) & 64 when 64 then @c31 else [cdate] end,
		[uuser] = case substring(@bitmap,4,1) & 128 when 128 then @c32 else [uuser] end,
		[udate] = case substring(@bitmap,5,1) & 1 when 1 then @c33 else [udate] end
	where [supplier_id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[supplier_id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbSuppliers]', @param2=@primarykey_text, @param3=13233
		End
end