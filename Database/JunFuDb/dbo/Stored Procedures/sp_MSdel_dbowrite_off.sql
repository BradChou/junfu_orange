﻿create procedure [sp_MSdel_dbowrite_off]
		@pkc1 varchar(20)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [dbo].[write_off] 
	where [check_number] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[check_number] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[write_off]', @param2=@primarykey_text, @param3=13234
		End
end