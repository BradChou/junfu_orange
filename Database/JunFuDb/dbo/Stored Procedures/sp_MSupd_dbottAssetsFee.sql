﻿create procedure [sp_MSupd_dbottAssetsFee]
		@c1 bigint = NULL,
		@c2 varchar(2) = NULL,
		@c3 nvarchar(10) = NULL,
		@c4 varchar(10) = NULL,
		@c5 datetime = NULL,
		@c6 datetime = NULL,
		@c7 datetime = NULL,
		@c8 varchar(50) = NULL,
		@c9 nvarchar(50) = NULL,
		@c10 varchar(10) = NULL,
		@c11 varchar(20) = NULL,
		@c12 float = NULL,
		@c13 float = NULL,
		@c14 float = NULL,
		@c15 float = NULL,
		@c16 float = NULL,
		@c17 float = NULL,
		@c18 float = NULL,
		@c19 float = NULL,
		@c20 float = NULL,
		@c21 float = NULL,
		@c22 nvarchar(30) = NULL,
		@c23 datetime = NULL,
		@c24 datetime = NULL,
		@c25 varchar(20) = NULL,
		@c26 datetime = NULL,
		@c27 varchar(20) = NULL,
		@pkc1 bigint = NULL,
		@bitmap binary(4)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttAssetsFee] set
		[fee_type] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [fee_type] end,
		[dept_id] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [dept_id] end,
		[a_id] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [a_id] end,
		[date] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [date] end,
		[fee_sdate] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [fee_sdate] end,
		[fee_edate] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [fee_edate] end,
		[oil] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [oil] end,
		[company] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [company] end,
		[items] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [items] end,
		[detail] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [detail] end,
		[milage] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [milage] end,
		[litre] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [litre] end,
		[price] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [price] end,
		[buy_price] = case substring(@bitmap,2,1) & 64 when 64 then @c15 else [buy_price] end,
		[receipt_price] = case substring(@bitmap,2,1) & 128 when 128 then @c16 else [receipt_price] end,
		[price_notax] = case substring(@bitmap,3,1) & 1 when 1 then @c17 else [price_notax] end,
		[buy_price_notax] = case substring(@bitmap,3,1) & 2 when 2 then @c18 else [buy_price_notax] end,
		[receipt_price_notax] = case substring(@bitmap,3,1) & 4 when 4 then @c19 else [receipt_price_notax] end,
		[quant] = case substring(@bitmap,3,1) & 8 when 8 then @c20 else [quant] end,
		[total_price] = case substring(@bitmap,3,1) & 16 when 16 then @c21 else [total_price] end,
		[memo] = case substring(@bitmap,3,1) & 32 when 32 then @c22 else [memo] end,
		[starttime] = case substring(@bitmap,3,1) & 64 when 64 then @c23 else [starttime] end,
		[cdate] = case substring(@bitmap,3,1) & 128 when 128 then @c24 else [cdate] end,
		[cuser] = case substring(@bitmap,4,1) & 1 when 1 then @c25 else [cuser] end,
		[udate] = case substring(@bitmap,4,1) & 2 when 2 then @c26 else [udate] end,
		[uuser] = case substring(@bitmap,4,1) & 4 when 4 then @c27 else [uuser] end
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttAssetsFee]', @param2=@primarykey_text, @param3=13233
		End
end