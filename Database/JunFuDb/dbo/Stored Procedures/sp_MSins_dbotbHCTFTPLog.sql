﻿create procedure [sp_MSins_dbotbHCTFTPLog]
    @c1 bigint,
    @c2 bigint,
    @c3 datetime,
    @c4 nvarchar(10),
    @c5 nvarchar(200),
    @c6 nvarchar(20),
    @c7 varchar(20)
as
begin  
	insert into [dbo].[tbHCTFTPLog] (
		[id],
		[request_id],
		[logdate],
		[check_number],
		[message],
		[type],
		[cuser]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7	) 
end