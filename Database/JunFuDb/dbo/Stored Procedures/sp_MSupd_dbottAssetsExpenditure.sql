﻿create procedure [sp_MSupd_dbottAssetsExpenditure]
		@c1 bigint = NULL,
		@c2 int = NULL,
		@c3 varchar(2) = NULL,
		@c4 nvarchar(50) = NULL,
		@c5 varchar(10) = NULL,
		@c6 nvarchar(7) = NULL,
		@c7 nvarchar(50) = NULL,
		@c8 varchar(2) = NULL,
		@c9 varchar(20) = NULL,
		@c10 datetime = NULL,
		@c11 nvarchar(20) = NULL,
		@c12 nvarchar(20) = NULL,
		@c13 nvarchar(max) = NULL,
		@c14 float = NULL,
		@c15 float = NULL,
		@c16 float = NULL,
		@c17 datetime = NULL,
		@c18 varchar(20) = NULL,
		@c19 datetime = NULL,
		@c20 varchar(20) = NULL,
		@c21 datetime = NULL,
		@c22 varchar(20) = NULL,
		@pkc1 bigint = NULL,
		@bitmap binary(3)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttAssetsExpenditure] set
		[type] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [type] end,
		[fee_type] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [fee_type] end,
		[fee_type_name] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [fee_type_name] end,
		[oil] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [oil] end,
		[fee_date] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [fee_date] end,
		[company] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [company] end,
		[pay_kind] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [pay_kind] end,
		[paper] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [paper] end,
		[pay_date] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [pay_date] end,
		[pay_account] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [pay_account] end,
		[pay_num] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [pay_num] end,
		[memo] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [memo] end,
		[money] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [money] end,
		[price] = case substring(@bitmap,2,1) & 64 when 64 then @c15 else [price] end,
		[tax] = case substring(@bitmap,2,1) & 128 when 128 then @c16 else [tax] end,
		[idate] = case substring(@bitmap,3,1) & 1 when 1 then @c17 else [idate] end,
		[iuser] = case substring(@bitmap,3,1) & 2 when 2 then @c18 else [iuser] end,
		[cdate] = case substring(@bitmap,3,1) & 4 when 4 then @c19 else [cdate] end,
		[cuser] = case substring(@bitmap,3,1) & 8 when 8 then @c20 else [cuser] end,
		[udate] = case substring(@bitmap,3,1) & 16 when 16 then @c21 else [udate] end,
		[uuser] = case substring(@bitmap,3,1) & 32 when 32 then @c22 else [uuser] end
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttAssetsExpenditure]', @param2=@primarykey_text, @param3=13233
		End
end