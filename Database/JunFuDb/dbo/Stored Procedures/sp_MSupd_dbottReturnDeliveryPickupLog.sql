﻿create procedure [sp_MSupd_dbottReturnDeliveryPickupLog]
		@c1 bigint = NULL,
		@c2 nvarchar(20) = NULL,
		@c3 varchar(2) = NULL,
		@c4 datetime = NULL,
		@c5 varchar(20) = NULL,
		@c6 nvarchar(50) = NULL,
		@c7 nvarchar(50) = NULL,
		@c8 datetime = NULL,
		@c9 datetime = NULL,
		@c10 varchar(2) = NULL,
		@pkc1 bigint = NULL,
		@bitmap binary(2)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttReturnDeliveryPickupLog] set
		[check_number] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [check_number] end,
		[scan_item] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [scan_item] end,
		[scan_date] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [scan_date] end,
		[driver_code] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [driver_code] end,
		[sign_form_image] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [sign_form_image] end,
		[sign_field_image] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [sign_field_image] end,
		[next_pickup_time] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [next_pickup_time] end,
		[cdate] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [cdate] end,
		[re_code] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [re_code] end
	where [seq] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[seq] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttReturnDeliveryPickupLog]', @param2=@primarykey_text, @param3=13233
		End
end