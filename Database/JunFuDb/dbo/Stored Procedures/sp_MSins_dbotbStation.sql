﻿create procedure [sp_MSins_dbotbStation]
    @c1 int,
    @c2 nvarchar(20),
    @c3 nvarchar(20),
    @c4 nvarchar(100),
    @c5 bit,
    @c6 nvarchar(20),
    @c7 datetime,
    @c8 nvarchar(20)
as
begin  
	insert into [dbo].[tbStation] (
		[id],
		[station_code],
		[station_scode],
		[station_name],
		[active_flag],
		[BusinessDistrict],
		[udate],
		[uuser]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8	) 
end