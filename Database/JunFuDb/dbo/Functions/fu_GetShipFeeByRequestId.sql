﻿/***************************
*建立時間:  2017/08/23
*建立者  :  Karen
*功能描述:  回傳運價資訊 (帶入託運單號)
*參數說明: 
 傳入參數:  @request_id : 託運單號
 回傳表格:  check_number,supplier_fee,cscetion_fee,status_code,status_msg
*參數說明: 
 回傳參數1: check_number :託運單號
 回傳參數2: supplier_fee :配送費用
 回傳參數3: supplier_unit_fee :配送費用(單價)
 回傳參數4: cscetion_fee :C配運價
 回傳參數5: cscetion_unit_fee :C配運價(單價)
 回傳參數6: status_code  :執行狀態碼 0.未執行 1.正常 -1.錯誤
 回傳參數7: status_msg   :執行結果 or 錯誤訊息(錯誤代碼) 

*使用範例:   SELECT * FROM dbo.fu_GetShipFeeByRequestId(1) fee;	
***************************/

CREATE Function [dbo].[fu_GetShipFeeByRequestId](@request_id numeric(18, 0))

Returns @r 
TABLE(
    check_number NVARCHAR(20) primary key,
	supplier_fee int ,
	supplier_unit_fee int,
    cscetion_fee int ,
	cscetion_unit_fee int ,
	total_fee int ,
    status_code  int ,
	status_msg NVARCHAR(500)
)
AS
BEGIN
	  /** 參數宣告 **/
      DECLARE  @supplier_fee INT          --配送費用
	  DECLARE  @cscetion_fee INT          --C配運價
	  DECLARE  @total_fee     INT         --總費用
	  DECLARE  @status_code  INT          --執行結果代碼 (0:未執行1:正常-1:錯誤)
	  DECLARE  @status_msg NVARCHAR(500)  --執行結果 及 相關錯誤資訊
	  

	  DECLARE @check_number  NVARCHAR(10) --託運單號
	  DECLARE @customer_code NVARCHAR(10) --客代編號
	  DECLARE @supplier_code NVARCHAR(3)  --區配商代碼
	  DECLARE @pricing_type  NVARCHAR(2)  --計價模式(01:論板、02:論件、03論才、04論小板)
	  DECLARE @unit          INT          --單位 (註:依@pricing_type而取不同的欄位)
	  DECLARE @unit_price    INT          --單位價格 (註:取得單位價格，若>=6，則以6計算)
	  DECLARE @unit_price_c  INT          --單位價格-C配 (註:取得單位價格，若>=6，則以6計算)
	  DECLARE @send_city     NVARCHAR(10) --寄件地址-縣市
	  DECLARE @receive_city  NVARCHAR(10) --收件地址-縣市
	  DECLARE @print_date    DATETIME     --列印日期(發送日)
	  DECLARE @customer_type CHAR(1)      --客戶類別(1.區配 2.竹運 3.自營)
	  DECLARE @class_level   INT          --級距
	  DECLARE @tariffs_type  CHAR(1)      --自營-運價類別1:公版 2:自訂
	  DECLARE @tariffs_effect_date DATE   --自營-運價生效日
	  DECLARE @isHCT         BIT          --是否為竹運單
	  DECLARE @remote_fee    INT          --偏遠加價 
	  DECLARE @individual_fee INT         --獨立計價(不採運價表計算託運單價格)
	  DECLARE @turn_board_fee   INT       --翻板
	  DECLARE @upstairs_fee   INT         --上樓
	  DECLARE @difficult_fee  INT         --困配
	  

	   /** 初始值宣告 **/
	  SET @supplier_fee = 0
	  SET @cscetion_fee = 0
	  SET @status_code = 0
	  SET @status_msg = '尚未執行'	

	 /** 必要參數取得 **/
	  SELECT @check_number =check_number
	        ,@customer_code = customer_code
	        ,@pricing_type = pricing_type 
	        ,@unit = CASE　pricing_type WHEN '02' THEN pieces WHEN '03' THEN cbm ELSE plates END
			,@send_city = REPLACE(send_city,'台','臺')
			,@receive_city = REPLACE(receive_city,'台','臺')
			,@print_date = CONVERT(CHAR(10),print_date,111)
			,@remote_fee = ISNULL(remote_fee ,0) 
			,@turn_board_fee =turn_board_fee
			,@upstairs_fee = upstairs_fee
	        ,@difficult_fee = difficult_fee
			,@isHCT = CASE WHEN supplier_code  = '001' THEN 1 ELSE 0 END
	    FROM dbo.tcDeliveryRequests With(Nolock) WHERE request_id = @request_id	  

		IF (@send_city = '' and @customer_code <> '') BEGIN 
			select @send_city = REPLACE(shipments_city,'台','臺')  from tbCustomers where customer_code =  @customer_code
		END

		IF @isHCT = 0  BEGIN 
			SELECT @customer_type = customer_type
				,@supplier_code = supplier_code
				,@individual_fee = ISNULL(individual_fee,0)
			FROM dbo.tbCustomers  With(Nolock) WHERE customer_code = @customer_code

			IF (@individual_fee = 0 ) 
			BEGIN 
					SELECT TOP 1 @class_level = class_level 
				  FROM dbo.ttPriceClassLog With(Nolock) WHERE @print_date >= CONVERT(CHAR(10),active_date,111) and active_date <=getdate() ORDER BY active_date DESC

				  /** 主要程式(start) **/
				 /* 1.區配 */
				 IF @customer_type = '1'
					BEGIN
					   SELECT @unit_price = 
						 CASE(@unit) WHEN 1 THEN plate1_price WHEN 2 THEN plate2_price WHEN 3 THEN plate3_price WHEN 4 THEN plate4_price 
									 WHEN 5 THEN plate5_price ELSE  plate6_price END
						 FROM　dbo.tbPriceSupplier With(Nolock) 
						WHERE start_city = ISNULL(@send_city,'') AND end_city = ISNULL(@receive_city,'') AND  class_level = @class_level AND　@unit > 0		
					END
				/* 2.竹運 */
				ELSE IF @customer_type = '2'
				   BEGIN
					  SELECT @unit_price = 
						 CASE(@unit) WHEN 1 THEN plate1_price WHEN 2 THEN plate2_price WHEN 3 THEN plate3_price WHEN 4 THEN plate4_price 
									 WHEN 5 THEN plate5_price ELSE  plate6_price END
						 FROM　dbo.tbPriceHCT With(Nolock) 
						WHERE start_city = ISNULL(@send_city,'') AND end_city = ISNULL(@receive_city,'') AND　@unit > 0		
				   END
				/* 3.自營 */
				ELSE IF @customer_type = '3'
				   BEGIN
					  SELECT Top 1 @tariffs_type = tariffs_type,@tariffs_effect_date = CONVERT(CHAR(10),tariffs_effect_date,111) 
						FROM dbo.ttPriceBusinessDefineLog With(Nolock) 
					   WHERE customer_code = @customer_code AND @print_date >= CONVERT(CHAR(10),tariffs_effect_date,111)
						ORDER BY  tariffs_effect_date DESC

					  /* 自營-自訂運費 */
					  IF (@tariffs_type IS NOT NULL AND @tariffs_type = 2)
						 BEGIN
							SELECT @unit_price =
							   CASE(@unit) WHEN 1 THEN plate1_price WHEN 2 THEN plate2_price WHEN 3 THEN plate3_price WHEN 4 THEN plate4_price 
									 WHEN 5 THEN plate5_price ELSE  plate6_price END
							  FROM dbo.tbPriceBusinessDefine With(Nolock)
							 WHERE @print_date >= CONVERT(CHAR(10),active_date,111)
							   and CONVERT(CHAR(10),active_date,111) =@tariffs_effect_date
							   AND customer_code = @customer_code 
							   AND start_city = ISNULL(@send_city,'') AND end_city = ISNULL(@receive_city,'')
						 END

						 /* 自營-運費 (取不到自訂運費，則撈公版) */
						 IF (@unit_price IS NULL OR @unit_price = 0)
							BEGIN
							   SELECT @unit_price =
								 CASE(@unit) WHEN 1 THEN plate1_price WHEN 2 THEN plate2_price WHEN 3 THEN plate3_price WHEN 4 THEN plate4_price 
											 WHEN 5 THEN plate5_price ELSE  plate6_price END
								 FROM dbo.tbPriceBusiness With(Nolock)
								WHERE start_city = ISNULL(@send_city,'') AND end_city = ISNULL(@receive_city,'') 
								--AND  class_level = @class_level 2019/02/20 改不分級距
								AND pricing_code = @pricing_type
							END
				   END

				   /*C配運價*/
				   IF (@supplier_code IS NOT NULL AND @supplier_code = 'A05')
					  BEGIN
						 /* 合發 */
						 SELECT @unit_price_c = 
						   CASE(@unit) WHEN 1 THEN plate1_price WHEN 2 THEN plate2_price WHEN 3 THEN plate3_price WHEN 4 THEN plate4_price 
									   WHEN 5 THEN plate5_price ELSE  plate6_price END
						   FROM dbo.tbPriceCSectionA05  With(Nolock) 
						  WHERE class_level = @class_level AND pricing_code = @pricing_type
					  END
				   ELSE
					  BEGIN
						 SELECT @unit_price_c = 
						   CASE(@unit) WHEN 1 THEN plate1_price WHEN 2 THEN plate2_price WHEN 3 THEN plate3_price WHEN 4 THEN plate4_price 
									   WHEN 5 THEN plate5_price ELSE  plate6_price END
						   FROM dbo.tbPriceCSection With(Nolock) 
						  WHERE class_level = @class_level AND pricing_code = @pricing_type
							AND start_city = ISNULL(@send_city,'') AND end_city = ISNULL(@receive_city,'')
					  END
					 
					/* 計算運價 */
				   IF (@unit_price > 0 AND @unit > 0)
					  BEGIN
						 SELECT @supplier_fee = (@unit_price * @unit) + @remote_fee + @turn_board_fee + @upstairs_fee + @difficult_fee 
							   ,@cscetion_fee = (ISNULL(@unit_price_c,0) * @unit)

						 SET @status_code ='1'
						 SET @status_msg = N'成功'
					  END
				   ELSE
					  BEGIN 
						 SET @status_code ='1'
						 SET @status_msg = N'資料異常'
					  END	
			END ELSE 
			BEGIN
				--獨立計價以key單的金額為主
				SELECT @supplier_fee=supplier_fee + @turn_board_fee + @upstairs_fee + @difficult_fee, @cscetion_fee=csection_fee ,@total_fee = total_fee   FROM dbo.tcDeliveryRequests With(Nolock) WHERE request_id = @request_id
				SET @status_code ='1'
				SET @status_msg = N'成功'	 
			END
			
		END ELSE BEGIN 
			--竹運單依匯入以匯入的金額為主
			SELECT @supplier_fee=supplier_fee, @cscetion_fee=csection_fee ,@total_fee = total_fee   FROM dbo.tcDeliveryRequests With(Nolock) WHERE request_id = @request_id
			SET @status_code ='1'
			SET @status_msg = N'成功'
		END  

	  BEGIN
		 INSERT @r
         SELECT @check_number,ISNULL(@supplier_fee,0),ISNULL(@unit_price,0),ISNULL(@cscetion_fee,0),ISNULL(@unit_price_c,0),ISNULL(@total_fee,0),@status_code,@status_msg 
	  END

	 /** 主要程式(end) **/	


    /**回傳結果**/
    RETURN
END