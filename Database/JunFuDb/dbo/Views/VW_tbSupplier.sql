﻿CREATE VIEW VW_tbSupplier as
SELECT supplier_name,IDNO,supplier_id,headquartersType,headquarters FROM 

(SELECT ROW_NUMBER()OVER (PARTITION BY supplier_name ORDER BY IDNO DESC) as ROW
	,supplier_name,IDNO,supplier_id,headquartersType,headquarters FROM (	
		SELECT DISTINCT
		supplier_id
		,supplier_name 
		,headquartersType
		,headquarters
		,CASE WHEN ISNULL(uniform_numbers ,'') = '' THEN ISNULL(id_no,'') ELSE uniform_numbers 
		END AS [IDNO]
		from tbsuppliers with(nolock)
		WHERE supplier_name is not NULL　AND supplier_name NOT LIKE ('光年%')　and active_flag = '1'
	)a) b WHERE ROW ='1'