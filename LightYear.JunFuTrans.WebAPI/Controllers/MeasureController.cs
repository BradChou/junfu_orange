﻿using JunFuTrans.DA.JunFuTrans.condition;
using JunFuTrans.DA.JunFuTrans.DA;
using LightYear.JunFuTrans.WebAPI.Models;
using LightYear.JunFuTrans.WebAPI.Models.Req;
using LightYear.JunFuTrans.WebAPI.Models.Res;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.WebAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class MeasureController : ControllerBase
    {

        tcDeliveryRequests_DA _tcDeliveryRequests_DA = new tcDeliveryRequests_DA();
        MeasureScanLog_DAL _MeasureScanLog_DAL = new MeasureScanLog_DAL();

        [HttpPost]
        [Route("InsertShipmentSize")]
        public ResData InsertShipmentSize(InsertShipmentSize_Req _Req)
        {
            ResData resData = new ResData();
           // InsertShipmentSize_Res _Res = new InsertShipmentSize_Res();
          //  _Res.isSuccess = false;

            if (_Req == null)
            {
                resData.Status = "10001";
                resData.Message = "資料來源錯誤";
                return resData;

            }
            if (string.IsNullOrEmpty(_Req.DataSource))
            {
                resData.Status = "10001";
                resData.Message = "資料來源錯誤";
                return resData;

            }
            if (string.IsNullOrEmpty(_Req.Status))
            {
                resData.Status = "10002";
                resData.Message = "資料有誤";
                return resData;

            }
            if (_Req.ScanTime == null)
            {
                resData.Status = "10002";
                resData.Message = "資料有誤";
                return resData;

            }
           

            try
            {

                MeasureScanLog_Condition _Condition = new MeasureScanLog_Condition();

                _Condition.DataSource = _Req.DataSource;
                _Condition.Scancode = _Req.Scancode;
                _Condition.Length = _Req.Length;
                _Condition.Width = _Req.Width;
                _Condition.Height = _Req.Height;
                _Condition.Size = _Req.Size;
                _Condition.Status = _Req.Status;
                _Condition.ScanTime = _Req.ScanTime;
                _Condition.Picture = _Req.Picture;
                _Condition.cdate = DateTime.Now;
                _MeasureScanLog_DAL.InsertMeasureScanLog(_Condition);
                resData.Status = "200";
                resData.Message = "成功";
                return resData;
            }
            catch (Exception e)
            {
                resData.Status = "10002";
                resData.Message = "資料有誤";
                return resData;
            }
        }
        [HttpGet]
        [Route("GetShipmentSize")]
        public ResData GetShipmentSize(string check_number, string customerCode)
        {
            ResData resData = new ResData();
            GetShipmentSize_Res _Res = new GetShipmentSize_Res();


            if (string.IsNullOrEmpty(check_number))
            {
                resData.Status = "10005";
                resData.Message = "查無此單";
                return resData;
            }
            if (string.IsNullOrEmpty(customerCode))
            {
                resData.Status = "10004";
                resData.Message = "客代錯誤";
                return resData;
            }
            var check_numberLen = check_number.Length;
            if (check_numberLen >= 15 && check_numberLen <= 12)
            {
                resData.Status = "10005";
                resData.Message = "查無此單";
                return resData;
            }
            var customerCodeLen = customerCode.Length;
            if (customerCodeLen != 11)
            {
                resData.Status = "10004";
                resData.Message = "客代錯誤";
                return resData;
            }
            //check_number 可能會重複 不是唯一值
            var infos = _tcDeliveryRequests_DA.GettcDeliveryRequestsBycheck_number(check_number);
            if (infos == null || infos.Count() == 0)
            {
                resData.Status = "10005";
                resData.Message = "查無此單";
                return resData;
            }

            //避免贓資料重複 所有都要尋找
            infos = infos.Where(x => x.customer_code.Equals(customerCode)).ToList();
            if (infos == null || infos.Count() == 0)
            {
                resData.Status = "10004";
                resData.Message = "客代錯誤";
                return resData;
            }

            var cbmHeight = string.Empty;
            var cbmWidth = string.Empty;
            var cbmLength = string.Empty;
            var cbmCont = string.Empty;

            //有資料就塞 不知道髒資料會塞到哪一欄 所以有值就塞
            foreach (var info in infos)
            {
                if (string.IsNullOrEmpty(cbmHeight))
                    cbmHeight = info.cbmHeight.HasValue ? info.cbmHeight.ToString() : string.Empty;
                if (string.IsNullOrEmpty(cbmWidth))
                    cbmWidth = info.cbmWidth.HasValue ? info.cbmWidth.ToString() : string.Empty;
                if (string.IsNullOrEmpty(cbmLength))
                    cbmLength = info.cbmLength.HasValue ? info.cbmLength.ToString() : string.Empty;
                if (string.IsNullOrEmpty(cbmCont))
                    cbmCont = info.cbmCont.HasValue ? info.cbmCont.ToString() : string.Empty;
            }


            if (string.IsNullOrEmpty(cbmHeight))
            {
                resData.Status = "10001";
                resData.Message = "無丈量資料";
                return resData;
            }
            if (string.IsNullOrEmpty(cbmWidth))
            {
                resData.Status = "10001";
                resData.Message = "無丈量資料";
                return resData;
            }
            if (string.IsNullOrEmpty(cbmLength))
            {
                resData.Status = "10001";
                resData.Message = "無丈量資料";
                return resData;
            }
            if (string.IsNullOrEmpty(cbmCont))
            {
                resData.Status = "10001";
                resData.Message = "無丈量資料";
                return resData;
            }

            _Res.Height = cbmHeight;
            _Res.Length = cbmLength;
            _Res.Width = cbmWidth;
            _Res.Size = cbmCont;

            resData.Status = "200";
            resData.Message = "成功";
            resData.Data = _Res;
            return resData;
        }
    }
}
