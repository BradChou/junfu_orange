﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using LightYear.JunFuTrans.BL.BE.Api;
using Newtonsoft.Json;

namespace LightYear.JunFuTrans.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestPostController : ControllerBase
    {

        public List<ApiScanLogEntity> Post([FromBody] List<ApiScanLogEntity> apiScanLogEntities)
        {
            return apiScanLogEntities;
        }
    }
}
