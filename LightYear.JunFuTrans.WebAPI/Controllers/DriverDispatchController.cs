﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LightYear.JunFuTrans.BL.BE.DriverDispatch;
using LightYear.JunFuTrans.BL.Services.DriverDispatch;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace LightYear.JunFuTrans.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DriverDispatchController : ControllerBase
    {
        IDriverDispatchService DriverDispatchService;
        public DriverDispatchController(IDriverDispatchService driverDispatchService)
        {
            this.DriverDispatchService = driverDispatchService;
        }

        /// <summary>
        /// 取得修改托運單MDSD
        /// </summary>
        /// <param name="scanItem"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateMDSD/")]
        public IActionResult UpdateMDSD([FromBody] JsonUpdateMDSD item)
        {
            Result result = new Result();
            try
            {
                if (String.IsNullOrEmpty(item.cCode))
                {
                    throw new Exception("請輸入客戶碼。");
                }

                if (String.IsNullOrEmpty(item.cNumber))
                {
                    throw new Exception("請輸入條碼。");
                }

                if (String.IsNullOrEmpty(item.MD) && String.IsNullOrEmpty(item.SD))
                {
                    throw new Exception("MD與SD不可皆為空。");
                }


                DriverDispatchService.UpdateMDSD(item.cCode, item.cNumber, item.MD, item.SD);

                result.Code = "200";
                result.Reason = "成功";
                string response = JsonConvert.SerializeObject(result);
                return Content(response);
            }
            catch (Exception e)
            {
                result.Code = "400";
                result.Reason = e.Message;
                string response = JsonConvert.SerializeObject(result);
                return Content(response);
            }
        }

        /// <summary>
        /// 取得集貨狀態列表
        /// </summary>
        /// <param name="scanItem"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetScanItemOption/")]
        public IActionResult GetScanItemOption([FromBody] JsonScanItem scanItem)
        {
            Result result = new Result();

            try
            {
                Regex regex = new Regex("^[1-9]+$");

                if (!regex.IsMatch(scanItem.Scan))
                    throw new Exception("請輸入1至9之間的數字。");

                var returnData = DriverDispatchService.GetScanItemOption(scanItem.Scan)
                            .Select(a => new Option { OptionId = a.CodeId, OptionName = a.CodeName }).ToList();

                result.Code = "200";
                result.Reason = "成功";
                result.Data = returnData;
                string response = JsonConvert.SerializeObject(result);
                return Content(response);
            }
            catch (Exception e)
            {
                result.Code = "400";
                result.Reason = e.Message;
                string response = JsonConvert.SerializeObject(result);
                return Content(response);
            }
        }

        /// <summary>
        /// 取得司機正物流派遣明細
        /// </summary>
        /// <param name="driverCode"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetDispatchPickUpList/")]
        public IActionResult GetDispatchPickUpList([FromBody] JsonDriverCode driverCode)
        {
            Result result = new Result();

            try
            {
                if (String.IsNullOrEmpty(driverCode.Driver))
                    throw new Exception("請輸入參數。");

                var toUpperDriverCode = driverCode.Driver.ToUpper();
                
                var driverSDMD = DriverDispatchService.GetDriverMDSD(toUpperDriverCode);

                string MD = "0";
                string SD = "0";
                List<DriverDispatchEntity> list = new List<DriverDispatchEntity>();

                if (driverSDMD.DriverType.Length <= 0 && driverSDMD.DriverTypeCode.Length <= 0)
                {
                  
                }
                else
                {
                    list = DriverDispatchService.GetDispatchPickUpList(toUpperDriverCode);
                    MD = driverSDMD.DriverType == "MD" ? driverSDMD.DriverTypeCode : "0";
                    SD = driverSDMD.DriverType == "SD" ? driverSDMD.DriverTypeCode : "0";
                }

                object obj = new { MD, SD, list };

                result.Code = "200";
                result.Reason = "成功";
                result.Data = obj;
                string response = JsonConvert.SerializeObject(result);
                return Content(response);
            }
            catch (Exception e)
            {
                result.Reason = e.Message;
                result.Code = "400";
                string response = JsonConvert.SerializeObject(result);
                return Content(response);
            }
        }

        /// <summary>
        /// 取得單次派遣託運單號明細
        /// </summary>
        /// <param name="dispatchJson"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetCheckNumberListWithinDispath/")]
        public IActionResult GetCheckNumberListWithinDispath([FromBody] DispatchJsonEntity dispatchJson)
        {
            Result result = new Result();

            try
            {
                if (String.IsNullOrEmpty(dispatchJson.CustomerCode) || String.IsNullOrEmpty(dispatchJson.SendAddress))
                    throw new Exception("請輸入參數。");

                var returnData = DriverDispatchService.GetCheckNumberListWithinDispath(dispatchJson);
                result.Code = "200";
                result.Reason = "成功";
                result.Data = returnData;
                string response = JsonConvert.SerializeObject(result);
                return Content(response);
            }
            catch (Exception e)
            {
                result.Reason = e.Message;
                result.Code = "400";
                string response = JsonConvert.SerializeObject(result);
                return Content(response);
            }
        }

        /// <summary>
        /// 取得該司機的MD、SD代碼
        /// </summary>
        /// <param name="driverCode"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetDriverMDSD/")]
        public IActionResult GetDriverMDSD([FromBody] JsonDriverCode driverCode)
        {
            Result result = new Result();

            try
            {
                if (String.IsNullOrEmpty(driverCode.Driver))
                    throw new Exception("請輸入參數。");

                var returnData = DriverDispatchService.GetDriverMDSD(driverCode.Driver);

                result.Code = "200";
                result.Reason = "成功";
                result.Data = returnData;
                string response = JsonConvert.SerializeObject(result);
                return Content(response);
            }
            catch (Exception e)
            {
                result.Reason = e.Message;
                result.Code = "400";
                string response = JsonConvert.SerializeObject(result);
                return Content(response);
            }
        }
    }
}