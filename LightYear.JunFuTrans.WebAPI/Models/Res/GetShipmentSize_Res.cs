﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.WebAPI.Models.Res
{
    public class GetShipmentSize_Res
    {
        public string Length { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public string Size { get; set; }

    }
}
