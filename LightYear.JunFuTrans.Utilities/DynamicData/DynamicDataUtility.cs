﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Text;
using LightYear.JunFuTrans.Utilities.DynamicData.Attributes;

namespace LightYear.JunFuTrans.Utilities.DynamicData
{
    /// <summary>
    /// DynamicDataUtility
    /// </summary>
    public class DynamicDataUtility
    {
        /// <summary>
        /// 取得類別 Metadata 資料
        /// </summary>
        /// <param name="t">型別</param>
        /// <returns>類別 Metadata 資料</returns>
        public static Dictionary<string, MetadataInfo> GetClassMetadataInfoList(Type t)
        {
            var properties = new AssociatedMetadataTypeTypeDescriptionProvider(t).GetTypeDescriptor(t).GetProperties();
            Dictionary<string, MetadataInfo> metadataInfoList = new Dictionary<string, MetadataInfo>();
            foreach (PropertyDescriptor property in properties)
            {
                if (property.PropertyType == null)
                {
                    continue;
                }

                if ((property.PropertyType.BaseType != null && property.PropertyType.BaseType.Name == "EntityObject") || property.Name == "EntityKey" || property.Name == "EntityStatus")
                {
                    continue;
                }

                if (property.PropertyType != null && property.PropertyType.Name != null && property.PropertyType.Name.Contains("EntityCollection"))
                {
                    continue;
                }

                MetadataInfo mi = new MetadataInfo()
                {
                    FieldName = property.Name,
                    DisplayNameCustom = property.Attributes.OfType<DisplayNameCustomAttribute>().FirstOrDefault(),
                    Length = property.Attributes.OfType<StringLengthAttribute>().FirstOrDefault(),
                    DisplayFormat = property.Attributes.OfType<DisplayFormatAttribute>().FirstOrDefault(),
                    Range = property.Attributes.OfType<RangeAttribute>().FirstOrDefault(),
                    RegularExpression = property.Attributes.OfType<RegularExpressionAttribute>().FirstOrDefault(),
                    Required = property.Attributes.OfType<RequiredAttribute>().FirstOrDefault(),
                    UIHint = property.Attributes.OfType<UIHintAttribute>().FirstOrDefault(),
                    HiddenInput = property.Attributes.OfType<HiddenInputAttribute>().FirstOrDefault(),
                    PropertyType = property.PropertyType,
                    Definition = property.Attributes.OfType<DefinitionAttribute>().FirstOrDefault(),
                    SearchCriteria = property.Attributes.OfType<SearchCriteriaAttribute>().FirstOrDefault(),
                    DataValue = property.Attributes.OfType<DataValueAttribute>().FirstOrDefault(),
                    UIType = property.Attributes.OfType<UITypeAttribute>().FirstOrDefault(),
                    Query = property.Attributes.OfType<QueryAttribute>().FirstOrDefault()
                };

                metadataInfoList.Add(property.Name, mi);
            }

            return metadataInfoList;
        }

        /// <summary>
        /// 取得屬性 Metadata 資料
        /// </summary>
        /// <param name="t">型別</param>
        /// <param name="fieldName">fieldName</param>
        /// <returns>屬性Metadata 資料</returns>
        public static MetadataInfo GetFiledMetadataInfo(Type t, string fieldName)
        {
            var property = new AssociatedMetadataTypeTypeDescriptionProvider(t).GetTypeDescriptor(t).GetProperties().Find(fieldName, true);
            if (property.PropertyType == null
                || ((property.PropertyType.BaseType != null && property.PropertyType.BaseType.Name == "EntityObject") || property.Name == "EntityKey" || property.Name == "EntityStatus")
                || (property.PropertyType != null && property.PropertyType.Name != null && property.PropertyType.Name.Contains("EntityCollection")))
            {
                return null;
            }

            MetadataInfo metadataInfo = new MetadataInfo()
            {
                FieldName = property.Name,
                DisplayNameCustom = property.Attributes.OfType<DisplayNameCustomAttribute>().FirstOrDefault(),
                Length = property.Attributes.OfType<StringLengthAttribute>().FirstOrDefault(),
                DisplayFormat = property.Attributes.OfType<DisplayFormatAttribute>().FirstOrDefault(),
                Range = property.Attributes.OfType<RangeAttribute>().FirstOrDefault(),
                RegularExpression = property.Attributes.OfType<RegularExpressionAttribute>().FirstOrDefault(),
                Required = property.Attributes.OfType<RequiredAttribute>().FirstOrDefault(),
                UIHint = property.Attributes.OfType<UIHintAttribute>().FirstOrDefault(),
                HiddenInput = property.Attributes.OfType<HiddenInputAttribute>().FirstOrDefault(),
                PropertyType = property.PropertyType,
                Definition = property.Attributes.OfType<DefinitionAttribute>().FirstOrDefault(),
                SearchCriteria = property.Attributes.OfType<SearchCriteriaAttribute>().FirstOrDefault(),
                DataValue = property.Attributes.OfType<DataValueAttribute>().FirstOrDefault(),
                UIType = property.Attributes.OfType<UITypeAttribute>().FirstOrDefault(),
                Query = property.Attributes.OfType<QueryAttribute>().FirstOrDefault()
            };

            return metadataInfo;
        }
    }
}
