﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.Utilities.DynamicData.Attributes
{
    /// <summary>
    /// QueryAttributeOperatorEnum
    /// </summary>
    public enum QueryAttributeOperatorEnum
    {
        /// <summary>
        /// 等同於
        /// </summary>
        EqualTo,

        /// <summary>
        /// 大於
        /// </summary>
        GreaterThan,

        /// <summary>
        /// 大於等於
        /// </summary>
        GreaterThanOrEqualTo,

        /// <summary>
        /// 小於
        /// </summary>
        LessThan,

        /// <summary>
        /// 小於等於
        /// </summary>
        LessThanOrEqualTo,

        /// <summary>
        /// 後模糊
        /// </summary>
        StartsWith,

        /// <summary>
        /// 前模糊
        /// </summary>
        EndsWith


    }
}
