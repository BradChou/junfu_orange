﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.Utilities.DynamicData.Attributes
{
    /// <summary>
    /// UITypeAttribute
    /// </summary>
    [System.AttributeUsage(AttributeTargets.Property)]
    public class UITypeAttribute : System.Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UITypeAttribute"/> class.
        /// </summary>
        /// <param name="type">type</param>
        /// <param name="editable">editable</param>
        public UITypeAttribute(UITypeEnum type, bool editable = true)
        {
            this.Type = type;
            this.Editable = editable;
        }

        /// <summary>
        /// UITypeEnum
        /// </summary>
        public UITypeEnum Type { get; set; }

        /// <summary>
        /// UITypeEnum
        /// </summary>
        public ValidationTypeEnum[] ValidationTypes { get; set; }

        /// <summary>
        /// CustomAttrs 
        /// usage : "attr1=value1^attr2=value2"
        /// ex: "onkeyup='checkXXX(this.value)'^onkeydown='checkXXX(this.value)'"
        /// </summary>
        public string CustomAttrs { get; set; }

        /// <summary>
        /// DefaultValue 
        /// </summary>
        public string DefaultValue { get; set; }

        /// <summary>
        /// 是否必填
        /// </summary>
        public bool IsRequired { get; set; }

        /// <summary>
        /// 是否可修改
        /// </summary>
        public bool Editable { get; set; }

        /// <summary>
        /// 最大長度
        /// </summary>
        public string MaxLength { get; set; }

        /// <summary>
        /// 最小長度
        /// </summary>
        public string MinLength { get; set; }
    }
}
