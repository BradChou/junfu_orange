﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.Utilities.DynamicData.Attributes
{
    /// <summary>
    /// 驗證設定
    /// </summary>
    [System.AttributeUsage(AttributeTargets.Property)]
    public class ValidationAttribute : System.Attribute
    {
        /// <summary>
        /// 驗證類型
        /// </summary>
        public ValidationTypeEnum Type { get; set; }
    }
}
