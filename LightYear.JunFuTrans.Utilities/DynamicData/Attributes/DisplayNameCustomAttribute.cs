﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.Utilities.DynamicData.Attributes
{
    /// <summary>
    /// DisplayNameCustomAttribute
    /// </summary>
    public class DisplayNameCustomAttribute : System.ComponentModel.DisplayNameAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DisplayNameCustomAttribute"/> class.
        /// </summary>
        /// <param name="displayName">displayName</param>
        public DisplayNameCustomAttribute(string displayName)
        {
            this.DisplayNameValue = displayName;
        }

        /// <summary>
        /// todo: resource
        /// </summary>
        //// public String Resource { get; set; }
    }
}
