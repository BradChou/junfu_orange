﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.Utilities.DynamicData.Attributes
{
    /// <summary>
    /// UITypeEnum
    /// </summary>
    public enum UITypeEnum
    {
        /// <summary>
        /// Input
        /// </summary>
        Input,

        /// <summary>
        /// Textarea
        /// </summary>
        Textarea,

        /// <summary>
        /// Date
        /// </summary>
        Date,

        /// <summary>
        /// DateTime
        /// </summary>
        DateTime,

        /// <summary>
        /// DropDownList
        /// </summary>
        DropDownList,

        /// <summary>
        /// CheckboxList
        /// </summary>
        CheckboxList,

        /// <summary>
        /// RadioButtonList
        /// </summary>
        RadioButtonList,

        /// <summary>
        /// Label
        /// </summary>
        Label,

        /// <summary>
        /// Number
        /// </summary>
        Number,

        /// <summary>
        /// Currency
        /// </summary>
        Currency,

        /// <summary>
        /// Percentage
        /// </summary>
        Percentage,

        /// <summary>
        /// Password
        /// </summary>
        Password,

        /// <summary>
        /// Email
        /// </summary>
        Email,

        /// <summary>
        /// Mobile
        /// </summary>
        Mobile,

        /// <summary>
        /// 英數字
        /// </summary>
        Alphanumeric
    }
}
