﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.Utilities.DynamicData.Attributes
{
    /// <summary>
    /// Definition Source
    /// </summary>
    [System.AttributeUsage(AttributeTargets.Property)]
    public class DefinitionAttribute : System.Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DefinitionAttribute"/> class.
        /// </summary>
        public DefinitionAttribute()
        {

        }

        /// <summary>
        /// ControllerName
        /// </summary>
        public string ControllerName { get; set; }

        /// <summary>
        /// ActionName
        /// </summary>
        public string ActionName { get; set; }

        /// <summary>
        /// 定義檔 Table 名稱
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// 定義檔欄位名稱
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// DataValueFieldName
        /// </summary>
        public string DefaultValue { get; set; }

        /// <summary>
        /// 是否有全部選項
        /// </summary>
        public bool HasAllOptionItem { get; set; }

        /// <summary>
        /// 是否有查詢框
        /// </summary>
        public bool HasFilter { get; set; }

        /// <summary>
        /// 是否只取得啟用的項目
        /// </summary>
        public bool IsFilterByEnabled { get; set; }

        /// <summary>
        /// 沒選取時是否有"請選擇字樣"
        /// </summary>
        public bool HasOptionLabel { get; set; }
    }
}
