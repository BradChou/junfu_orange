﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.Utilities.DynamicData.Attributes
{
    /// <summary>
    /// 搜尋屬性
    /// </summary>
    [System.AttributeUsage(AttributeTargets.Property)]
    public class SearchCriteriaAttribute : System.Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SearchCriteriaAttribute"/> class.
        /// </summary>
        public SearchCriteriaAttribute()
        {
            this.GroupAfterHtml = string.Empty;
        }

        /// <summary>
        /// 是否預設顯示
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// 群組顯示
        /// </summary>
        public string GroupName { get; set; }

        /// <summary>
        /// 群組顯示分隔用HTML
        /// </summary>
        public string GroupAfterHtml { get; set; }
    }
}
